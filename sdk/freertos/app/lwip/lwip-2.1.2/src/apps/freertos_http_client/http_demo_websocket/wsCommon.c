/*******************************************************************************
*  Copyright Statement:
*  --------------------
*  This software is protected by Copyright and the information contained
*  herein is confidential. The software may not be copied and the information
*  contained herein may not be used or disclosed except with the written
*  permission of Airoha Technology Corp. (C) 2021
*
*  BY OPENING THIS FILE, BUYER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
*  THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("AIROHA SOFTWARE")
*  RECEIVED FROM AIROHA AND/OR ITS REPRESENTATIVES ARE PROVIDED TO BUYER ON
*  AN "AS-IS" BASIS ONLY. AIROHA EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
*  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
*  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
*  NEITHER DOES AIROHA PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
*  SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
*  SUPPLIED WITH THE AIROHA SOFTWARE, AND BUYER AGREES TO LOOK ONLY TO SUCH
*  THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. AIROHA SHALL ALSO
*  NOT BE RESPONSIBLE FOR ANY AIROHA SOFTWARE RELEASES MADE TO BUYER'S
*  SPECIFICATION OR TO CONFORM TO A PARTICULAR STANDARD OR OPEN FORUM.
*
*  BUYER'S SOLE AND EXCLUSIVE REMEDY AND AIROHA'S ENTIRE AND CUMULATIVE
*  LIABILITY WITH RESPECT TO THE AIROHA SOFTWARE RELEASED HEREUNDER WILL BE,
*  AT AIROHA'S OPTION, TO REVISE OR REPLACE THE AIROHA SOFTWARE AT ISSUE,
*  OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY BUYER TO
*  AIROHA FOR SUCH AIROHA SOFTWARE AT ISSUE.
*
*  THE TRANSACTION CONTEMPLATED HEREUNDER SHALL BE CONSTRUED IN ACCORDANCE
*  WITH THE LAWS OF THE STATE OF CALIFORNIA, USA, EXCLUDING ITS CONFLICT OF
*  LAWS PRINCIPLES.  ANY DISPUTES, CONTROVERSIES OR CLAIMS ARISING THEREOF AND
*  RELATED THERETO SHALL BE SETTLED BY ARBITRATION IN SAN FRANCISCO, CA, UNDER
*  THE RULES OF THE INTERNATIONAL CHAMBER OF COMMERCE (ICC).
*
*******************************************************************************/

/* FILE NAME:   wsCommon.c
 * PURPOSE:
 *      Define webSocket common function.
 *
 * NOTES:
 */

/* INCLUDE FILE DECLARATIONS
 */

#include "wsCommon.h"

/* NAMING CONSTANT DECLARATIONS
*/

#define CRLF        "\r\n"

/* MACRO FUNCTION DECLARATIONS
 */

/* DATA TYPE DECLARATIONS
 */

/* GLOBAL VARIABLE DECLARATIONS
 */
int _rcvLen = 0;

/* change Your CA here. */
static unsigned char CA[] =
"-----BEGIN CERTIFICATE-----"CRLF
"MIIBnzCCAQgCCQC1x1LJh4G1AzANBgkqhkiG9w0BAQUFADAUMRIwEAYDVQQDEwls"CRLF
"b2NhbGhvc3QwHhcNMDkxMTEwMjM0ODQ3WhcNMTkxMTA4MjM0ODQ3WjAUMRIwEAYD"CRLF
"VQQDEwlsb2NhbGhvc3QwgZ8wDQYJKoZIhvcNAQEBBQADgY0AMIGJAoGBAMEl0yfj"CRLF
"7K0Ng2pt51+adRAj4pCdoGOVjx1BmljVnGOMW3OGkHnMw9ajibh1vB6UfHxu463o"CRLF
"J1wLxgxq+Q8y/rPEehAjBCspKNSq+bMvZhD4p8HNYMRrKFfjZzv3ns1IItw46kgT"CRLF
"gDpAl1cMRzVGPXFimu5TnWMOZ3ooyaQ0/xntAgMBAAEwDQYJKoZIhvcNAQEFBQAD"CRLF
"gYEAavHzSWz5umhfb/MnBMa5DL2VNzS+9whmmpsDGEG+uR0kM1W2GQIdVHHJTyFd"CRLF
"aHXzgVJBQcWTwhp84nvHSiQTDBSaT6cQNQpvag/TaED/SEQpm0VqDFwpfFYuufBL"CRLF
"vVNbLkKxbK2XwUvu0RxoLdBMC/89HqrZ0ppiONuQ+X2MtxE="CRLF
"-----END CERTIFICATE-----"CRLF;

/* STATIC VARIABLE DECLARATIONS
 */

/* EXPORTED SUBPROGRAM BODIES
 */
/* FUNCTION NAME: WebSocket_Connect
 * PURPOSE:
 *      Connect to remote server of specific address and port number.
 *
 * INPUT:
 *      ptr_url             - url of server address and port number
 *
 * OUTPUT:
 *      None
 *
 * RETURN:
 *      WS_SUCCESS
 *      WS_FAIL
 *
 * NOTES:
 *      If ptr_url == NULL, will use default server address and port which define
 *      in wsopt.h file.
 */
int32_t
WebSocket_Connect(
    char *ptr_url)
{
    SocketStatus_t socketStatus = SOCKETS_SUCCESS;
    int32_t returnStatus = WS_SUCCESS;
    ServerInfo_t serverInfo;

    memset(&_networkContext, 0, sizeof(NetworkContext_t));
    memset(&_mbedsslParams, 0, sizeof(TlsTransportParams_t));
    _networkContext.pParams = &_mbedsslParams;

    if(WS_FAIL == wsParseUrl(ptr_url))
    {
        WS_PRINT("Parser URL fail, use default setting:\n");
    }
    WS_PRINT("Server  : %s\n", _networkContext.serverAddr);
    WS_PRINT("Port    : %s\n", _networkContext.serverPort);
    WS_PRINT("Resource: %s\n", _networkContext.resource);
    WS_PRINT("Query   : %s\n", _networkContext.strquery);

    serverInfo.pHostName = _networkContext.serverAddr;
    serverInfo.hostNameLength = sizeof(_networkContext.serverAddr) - 1;
    serverInfo.port = atoi(_networkContext.serverPort);

    if(TRUE == _networkContext.useTLS)
    {
        /* mbedTLS information */
        TlsTransportStatus_t mbedtlsStatus;
        NetworkCredentials_t mbedtlsCredentials;

        /* Initialize TLS credentials. */
        (void)memset(&mbedtlsCredentials, 0, sizeof(mbedtlsCredentials) );
        mbedtlsCredentials.pRootCa = CA;
        mbedtlsCredentials.rootCaSize = sizeof(CA);
        /* disable hostname check. */
        mbedtlsCredentials.disableSni = TRUE;

        WS_PRINT("WebSocket connect TLS session ... ");
        mbedtlsStatus = MbedTLS_Connect(&_networkContext,
                                        &serverInfo,
                                        &mbedtlsCredentials,
                                        WS_CONNECT_TIMEOUT,
                                        WS_CONNECT_TIMEOUT);
        if(TLS_TRANSPORT_SUCCESS != mbedtlsStatus)
        {
            returnStatus = WS_FAIL;
        }
    }
    else
    {
        WS_PRINT("WebSocket connect ... ");
        socketStatus = Sockets_Connect(&(_mbedsslParams.tcpSocket),
                                        &serverInfo,
                                        WS_CONNECT_TIMEOUT,
                                        WS_CONNECT_TIMEOUT);
        if(SOCKETS_SUCCESS != socketStatus)
        {
            returnStatus = WS_FAIL;
        }
    }
    if(WS_SUCCESS == returnStatus)
    {
        WS_PRINT("Success.\n");
    }
    else
    {
        WS_PRINT("Failed to connect to %s\n", serverInfo.pHostName);
    }

    return returnStatus;
}

/* FUNCTION NAME: WebSocket_Handshake
 * PURPOSE:
 *      Send websocket handshake request to remote server.
 *
 * INPUT:
 *      None
 *
 * OUTPUT:
 *      None
 *
 * RETURN:
 *      WS_SUCCESS
 *      WS_FAIL
 *
 * NOTES:
 *      None
 */
int32_t
WebSocket_Handshake(
    void)
{
    int32_t returnStatus = WS_FAIL;
    static const char alphanum[] = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklnmopqrstuvwxyz";
    int i  = 0;
    unsigned char nonce[WS_BASE64_LEN] = { 0 };
    size_t len = 0;

    ( void ) memset( _wBuffer, 0, WS_BUFFER_LENGTH );
    ( void ) memset( _rBuffer, 0, WS_BUFFER_LENGTH );
    ( void ) memset( _seckey, 0, WS_SECKEY_LEN );

    for(i = 0; i < 16; i ++)
    {
        nonce[i] = alphanum[(rand() % 16)];
    }
    mbedtls_base64_encode((unsigned char*)_seckey, sizeof(_seckey), &len, nonce, sizeof(nonce));
    len = snprintf(_wBuffer, WS_BUFFER_LENGTH,
            "GET %s%s HTTP/1.1\r\n"
            "Host: %s:%s\r\n"
            "Upgrade: websocket\r\n"
            "Connection: Upgrade\r\n"
            "Sec-WebSocket-Key: %s\r\n"
            "Sec-WebSocket-Version: 13\r\n\r\n"
            , _networkContext.resource
            , _networkContext.strquery
            , _networkContext.serverAddr
            , _networkContext.serverPort
            , _seckey);

    WS_PRINT("Send handshake request ... ");
    returnStatus = wsSend(_wBuffer, len);
    if(WS_FAIL == returnStatus)
    {
        WS_PRINT("fail.\n");
    }
    else
    {
        WS_PRINT("Success.\n");
    }

    return returnStatus;
}

/* FUNCTION NAME: WebSocket_Send
 * PURPOSE:
 *      Send data to remote.
 *
 * INPUT:
 *      code                - Websocket option code.
 *                              enum WS_OPCODE_T
 *      ptr_buf             - A pointer of buffer of data want to send.
 *      len                 - Length of data need to send.
 *
 * OUTPUT:
 *      None
 *
 * RETURN:
 *      WS_SUCCESS
 *      WS_FAIL
 *
 * NOTES:
 *      None
 */
int32_t
WebSocket_Send(
    WS_OPCODE_T code,
    char *ptr_buf,
    int len)
{
    int ret;

    ret = wsCreateFrame(code, _wBuffer, WS_BUFFER_LENGTH, ptr_buf, len);
    if(WS_FAIL != ret)
    {
        ret = wsSend(_wBuffer, ret);
    }

    return ret;
}

/* FUNCTION NAME: WebSocket_Recv
 * PURPOSE:
 *      Receive data from remote.
 *
 * INPUT:
 *      None
 *
 * OUTPUT:
 *      ptr_code            - A pointer of websocket option code.
 *                              enum WS_OPCODE_T
 *      ptr_buf             - A pointer of buffer for data received.
 *      len                 - Size of receive data buffer.
 *
 * RETURN:
 *      WEBSOCKET_E_SOCKET_ERROR or receive length of data.
 *
 * NOTES:
 *      None
 */
int32_t
WebSocket_Recv(
    WS_OPCODE_T *ptr_code,
    char *ptr_buf,
    int len)
{
    int num = 0, datalen = 0, ret = WS_SUCCESS;

    num = wsRecv(&_rBuffer[_rcvLen], WS_BUFFER_LENGTH);
    if(0 >= num)
    {
        switch(errno)
        {
            case ECONNRESET:
            case ECONNABORTED:
            case ENOTCONN:
                _rcvLen = 0;
                return WEBSOCKET_E_SOCKET_ERROR;
        }
        return WEBSOCKET_E_SOCKET_WAIT;
    }
    else
    {
        _rcvLen += num;
        if(num >= WS_HEADER_LEN)
        {
            ret = wsParseFrame(ptr_code, _rBuffer, _rcvLen, ptr_buf, len, &datalen);
            if(WS_FAIL == ret)
            {
                _rcvLen = 0;
                return WEBSOCKET_E_SOCKET_ERROR;
            }
            else if(WS_WAITFRAME == ret)
            {
                /* Wait data complete */
                WS_PRINT("Wait data receive done.\n");
                datalen = 0;
            }
            else if(WS_SUCCESS == ret)
            {
                _rcvLen = 0;
                memset(_rBuffer, 0, WS_BUFFER_LENGTH);
            }
        }
    }
    return datalen;
}

/* FUNCTION NAME: WebSocket_Disconnect
 * PURPOSE:
 *      Disconnect with server.
 *
 * INPUT:
 *      None
 *
 * OUTPUT:
 *      None
 *
 * RETURN:
 *      WS_SUCCESS
 *      WS_FAIL
 *
 * NOTES:
 *      None
 */
int32_t
WebSocket_Disconnect(
    void)
{
    if(TRUE == _networkContext.useTLS)
    {
        WS_PRINT("Mbed Disconnect.\n");
        MbedTLS_Disconnect(&_networkContext);
    }
    else
    {
        if(SOCKETS_SUCCESS != Sockets_Disconnect(_mbedsslParams.tcpSocket))
        {
            return WS_FAIL;
        }
    }

    return WS_SUCCESS;
}
