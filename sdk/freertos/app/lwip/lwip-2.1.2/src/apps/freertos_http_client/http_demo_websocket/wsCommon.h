/*******************************************************************************
*  Copyright Statement:
*  --------------------
*  This software is protected by Copyright and the information contained
*  herein is confidential. The software may not be copied and the information
*  contained herein may not be used or disclosed except with the written
*  permission of Airoha Technology Corp. (C) 2021
*
*  BY OPENING THIS FILE, BUYER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
*  THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("AIROHA SOFTWARE")
*  RECEIVED FROM AIROHA AND/OR ITS REPRESENTATIVES ARE PROVIDED TO BUYER ON
*  AN "AS-IS" BASIS ONLY. AIROHA EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
*  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
*  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
*  NEITHER DOES AIROHA PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
*  SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
*  SUPPLIED WITH THE AIROHA SOFTWARE, AND BUYER AGREES TO LOOK ONLY TO SUCH
*  THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. AIROHA SHALL ALSO
*  NOT BE RESPONSIBLE FOR ANY AIROHA SOFTWARE RELEASES MADE TO BUYER'S
*  SPECIFICATION OR TO CONFORM TO A PARTICULAR STANDARD OR OPEN FORUM.
*
*  BUYER'S SOLE AND EXCLUSIVE REMEDY AND AIROHA'S ENTIRE AND CUMULATIVE
*  LIABILITY WITH RESPECT TO THE AIROHA SOFTWARE RELEASED HEREUNDER WILL BE,
*  AT AIROHA'S OPTION, TO REVISE OR REPLACE THE AIROHA SOFTWARE AT ISSUE,
*  OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY BUYER TO
*  AIROHA FOR SUCH AIROHA SOFTWARE AT ISSUE.
*
*  THE TRANSACTION CONTEMPLATED HEREUNDER SHALL BE CONSTRUED IN ACCORDANCE
*  WITH THE LAWS OF THE STATE OF CALIFORNIA, USA, EXCLUDING ITS CONFLICT OF
*  LAWS PRINCIPLES.  ANY DISPUTES, CONTROVERSIES OR CLAIMS ARISING THEREOF AND
*  RELATED THERETO SHALL BE SETTLED BY ARBITRATION IN SAN FRANCISCO, CA, UNDER
*  THE RULES OF THE INTERNATIONAL CHAMBER OF COMMERCE (ICC).
*
*******************************************************************************/

/* FILE NAME:  wsCommon.h
 * PURPOSE:
 *      Define webSocket common function.
 *
 * NOTES:
 */

#ifndef WSCOMMON_H
#define WSCOMMON_H
/* INCLUDE FILE DECLARATIONS
 */
#include "wsClient.h"

/* NAMING CONSTANT DECLARATIONS
 */
#define WEBSOCKET_E_SOCKET_ERROR    (-1)

/* MACRO FUNCTION DECLARATIONS
 */

/* DATA TYPE DECLARATIONS
 */

/* EXPORTED SUBPROGRAM SPECIFICATIONS
 */
/* FUNCTION NAME: WebSocket_Connect
 * PURPOSE:
 *      Connect to remote server of specific address and port number.
 *
 * INPUT:
 *      ptr_url             - url of server address and port number
 *
 * OUTPUT:
 *      None
 *
 * RETURN:
 *      WS_SUCCESS
 *      WS_FAIL
 *
 * NOTES:
 *      If ptr_url == NULL, will use default server address and port which define
 *      in wsopt.h file.
 */
int32_t
WebSocket_Connect(
    char *ptr_url);

/* FUNCTION NAME: WebSocket_Handshake
 * PURPOSE:
 *      Send websocket handshake request to remote server.
 *
 * INPUT:
 *      None
 *
 * OUTPUT:
 *      None
 *
 * RETURN:
 *      WS_SUCCESS
 *      WS_FAIL
 *
 * NOTES:
 *      None
 */
int32_t
WebSocket_Handshake(
    void);

/* FUNCTION NAME: WebSocket_Send
 * PURPOSE:
 *      Send data to remote.
 *
 * INPUT:
 *      code                - Websocket option code.
 *                              enum WS_OPCODE_T
 *      ptr_buf             - A pointer of buffer of data want to send.
 *      len                 - Length of data need to send.
 *
 * OUTPUT:
 *      None
 *
 * RETURN:
 *      WS_SUCCESS
 *      WS_FAIL
 *
 * NOTES:
 *      None
 */
int32_t
WebSocket_Send(
    WS_OPCODE_T code,
    char *ptr_buf,
    int len);

/* FUNCTION NAME: WebSocket_Recv
 * PURPOSE:
 *      Receive data from remote.
 *
 * INPUT:
 *      None
 *
 * OUTPUT:
 *      ptr_code            - A pointer of websocket option code.
 *                              enum WS_OPCODE_T
 *      ptr_buf             - A pointer of buffer for data received.
 *      len                 - Size of receive data buffer.
 *
 * RETURN:
 *      WEBSOCKET_E_SOCKET_ERROR or receive length of data.
 *
 * NOTES:
 *      None
 */
int32_t
WebSocket_Recv(
    WS_OPCODE_T *ptr_code,
    char *ptr_buf,
    int len);

/* FUNCTION NAME: WebSocket_Disconnect
 * PURPOSE:
 *      Disconnect with server.
 *
 * INPUT:
 *      None
 *
 * OUTPUT:
 *      None
 *
 * RETURN:
 *      WS_SUCCESS
 *      WS_FAIL
 *
 * NOTES:
 *      None
 */
int32_t
WebSocket_Disconnect(
    void);
#endif /* end of WSCOMMON_H */
