/*******************************************************************************
*  Copyright Statement:
*  --------------------
*  This software is protected by Copyright and the information contained
*  herein is confidential. The software may not be copied and the information
*  contained herein may not be used or disclosed except with the written
*  permission of Airoha Technology Corp. (C) 2021
*
*  BY OPENING THIS FILE, BUYER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
*  THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("AIROHA SOFTWARE")
*  RECEIVED FROM AIROHA AND/OR ITS REPRESENTATIVES ARE PROVIDED TO BUYER ON
*  AN "AS-IS" BASIS ONLY. AIROHA EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
*  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
*  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
*  NEITHER DOES AIROHA PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
*  SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
*  SUPPLIED WITH THE AIROHA SOFTWARE, AND BUYER AGREES TO LOOK ONLY TO SUCH
*  THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. AIROHA SHALL ALSO
*  NOT BE RESPONSIBLE FOR ANY AIROHA SOFTWARE RELEASES MADE TO BUYER'S
*  SPECIFICATION OR TO CONFORM TO A PARTICULAR STANDARD OR OPEN FORUM.
*
*  BUYER'S SOLE AND EXCLUSIVE REMEDY AND AIROHA'S ENTIRE AND CUMULATIVE
*  LIABILITY WITH RESPECT TO THE AIROHA SOFTWARE RELEASED HEREUNDER WILL BE,
*  AT AIROHA'S OPTION, TO REVISE OR REPLACE THE AIROHA SOFTWARE AT ISSUE,
*  OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY BUYER TO
*  AIROHA FOR SUCH AIROHA SOFTWARE AT ISSUE.
*
*  THE TRANSACTION CONTEMPLATED HEREUNDER SHALL BE CONSTRUED IN ACCORDANCE
*  WITH THE LAWS OF THE STATE OF CALIFORNIA, USA, EXCLUDING ITS CONFLICT OF
*  LAWS PRINCIPLES.  ANY DISPUTES, CONTROVERSIES OR CLAIMS ARISING THEREOF AND
*  RELATED THERETO SHALL BE SETTLED BY ARBITRATION IN SAN FRANCISCO, CA, UNDER
*  THE RULES OF THE INTERNATIONAL CHAMBER OF COMMERCE (ICC).
*
*******************************************************************************/

/* Standard includes. */
#include "FreeRTOS.h"
#include "task.h"
#include "wsCommon.h"
#include "air_mib.h"

int
http_ws_main(
    void *param)
{
    int len = 0;
    int32_t returnStatus = WS_SUCCESS;
    char dataBuffer[WS_BUFFER_LENGTH] = { 0 };
    WS_OPCODE_T code;

    AIR_MIB_CNT_RX_T rx_mib;
    AIR_MIB_CNT_TX_T tx_mib;
    unsigned int port = 0; /* demo CPU port */

    WS_PRINT("===== WebSocket client start : %s =====\n", (((char*)param)[0] == 0) ? WS_DEFAULT_SERVER : (char*)param);

    if(0 == ((char*)param)[0])
    {
        returnStatus = WebSocket_Connect(NULL);
    }
    else
    {
        returnStatus = WebSocket_Connect((char*)param);
    }
    if( returnStatus == WS_FAIL )
    {
        WebSocket_Disconnect();
        return WS_FAIL;
    }

    if(WS_FAIL == WebSocket_Handshake())
    {
        WebSocket_Disconnect();
        return WS_FAIL;
    }

    for( ; ; )
    {
        len = WebSocket_Recv(&code, dataBuffer, WS_BUFFER_LENGTH);
        if(WEBSOCKET_E_SOCKET_ERROR == len)
        {
            WS_PRINT("Socket close.\n");
            break;
        }
        else if(0 < len)
        {
            if((code & WS_BINARY_FRAME) || (code & WS_TEXT_FRAME))
            {
                if(!strcmp(dataBuffer, "mibCounter"))
                {
                    air_mib_getPortCnt(0, port, &rx_mib, &tx_mib);
                    sprintf(dataBuffer,
                            "\nPort:%d\n"
                            "Tx Unicat Packet: %u\n"
                            "Rx Unicat Packet: %u\n"
                            , port
                            , tx_mib.TUPC
                            , rx_mib.RUPC);
                    if(WS_FAIL == WebSocket_Send(code, dataBuffer, (int)strlen(dataBuffer)))
                    {
                        WS_PRINT("Send data fail.\n");
                        break;
                    }
                }
                else
                {
                    WS_PRINT("code: %x\n", code);
                    WS_PRINT("Data:\n%s\n", dataBuffer);
                }
                memset(dataBuffer, 0, WS_BUFFER_LENGTH);
            }
        }
        vTaskDelay(pdMS_TO_TICKS(500));
    }
    WS_PRINT("Disconnect and close socket.\n");
    WebSocket_Disconnect();

    return WS_SUCCESS;
}

void
HttpWS_TestTask(
    void *pvParameters)
{
    http_ws_main(pvParameters);
    /* Will only get here if a listening socket could not be created. */
    vTaskDelete( NULL );
}
