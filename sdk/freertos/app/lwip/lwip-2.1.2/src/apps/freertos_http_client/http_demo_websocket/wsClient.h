/*******************************************************************************
*  Copyright Statement:
*  --------------------
*  This software is protected by Copyright and the information contained
*  herein is confidential. The software may not be copied and the information
*  contained herein may not be used or disclosed except with the written
*  permission of Airoha Technology Corp. (C) 2021
*
*  BY OPENING THIS FILE, BUYER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
*  THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("AIROHA SOFTWARE")
*  RECEIVED FROM AIROHA AND/OR ITS REPRESENTATIVES ARE PROVIDED TO BUYER ON
*  AN "AS-IS" BASIS ONLY. AIROHA EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
*  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
*  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
*  NEITHER DOES AIROHA PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
*  SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
*  SUPPLIED WITH THE AIROHA SOFTWARE, AND BUYER AGREES TO LOOK ONLY TO SUCH
*  THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. AIROHA SHALL ALSO
*  NOT BE RESPONSIBLE FOR ANY AIROHA SOFTWARE RELEASES MADE TO BUYER'S
*  SPECIFICATION OR TO CONFORM TO A PARTICULAR STANDARD OR OPEN FORUM.
*
*  BUYER'S SOLE AND EXCLUSIVE REMEDY AND AIROHA'S ENTIRE AND CUMULATIVE
*  LIABILITY WITH RESPECT TO THE AIROHA SOFTWARE RELEASED HEREUNDER WILL BE,
*  AT AIROHA'S OPTION, TO REVISE OR REPLACE THE AIROHA SOFTWARE AT ISSUE,
*  OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY BUYER TO
*  AIROHA FOR SUCH AIROHA SOFTWARE AT ISSUE.
*
*  THE TRANSACTION CONTEMPLATED HEREUNDER SHALL BE CONSTRUED IN ACCORDANCE
*  WITH THE LAWS OF THE STATE OF CALIFORNIA, USA, EXCLUDING ITS CONFLICT OF
*  LAWS PRINCIPLES.  ANY DISPUTES, CONTROVERSIES OR CLAIMS ARISING THEREOF AND
*  RELATED THERETO SHALL BE SETTLED BY ARBITRATION IN SAN FRANCISCO, CA, UNDER
*  THE RULES OF THE INTERNATIONAL CHAMBER OF COMMERCE (ICC).
*
*******************************************************************************/

/* FILE NAME:  wsClient.h
 * PURPOSE:
 *      Define webSocket client function.
 *
 * NOTES:
 */

#ifndef WSCLIENT_H
#define WSCLIENT_H

/* INCLUDE FILE DECLARATIONS
 */
#include <assert.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "lwip/err.h"
#include "lwip/sockets.h"
#include "lwip/def.h"
#include "wsopt.h"

/* OpenSSL transport header. */
#include "mbedtls_posix.h"
#include "mbedtls/base64.h"
#include "mbedtls/sha1.h"
#include "sockets_posix.h"

/* Common HTTP demo utilities. */
#include "http_demo_utils.h"

/* NAMING CONSTANT DECLARATIONS
 */

#define WS_ERROR_LOG                (1)

#ifndef TRUE
#define TRUE                        (1)
#endif

#ifndef FALSE
#define FALSE                       (0)
#endif

#define WEBSOCKET_E_SOCKET_ERROR    (-1)
#define WEBSOCKET_E_SOCKET_WAIT     (0)
#define WS_SUCCESS                  (0)
#define WS_FAIL                     (1)
#define WS_WAITFRAME                (2)

#define WS_CONNECT_TIMEOUT          (1000)
#define WS_BUFFER_LENGTH            (512)
#define WS_BASE64_LEN               (16)
#define WS_SHA1_LEN                 (20)
#define WS_MAGIC_KEY_SIZE           (4)

#define WS_HEADER_LEN               (2)
#define WS_FIN_OFFT                 (0x80)
#define WS_RSV1_OFFT                (0x40)
#define WS_RSV2_OFFT                (0x20)
#define WS_RSV3_OFFT                (0x10)
#define WS_CODE_OFFT                (0x7F)
#define WS_MASK_OFFT                (0x80)
#define WS_DLEN_OFFT                (0x7F)

#define WS_EXTENDED_16_MSK          (126)
#define WS_EXTENDED_16_SIZE         (2)
#define WS_EXTENDED_64_MSK          (127)
#define WS_EXTENDED_64_SIZE         (8)

#define WS_GUID_LEN                 (36)
#define WS_SECKEY_LEN               (100)
#define WS_ADDRESS_LEN              (16)
#define WS_STRING_LEN               (20)

#define WS_UPGRADE_KEYWORD          "Upgrade:"
#define WS_CONNECT_KEYWORD          "Connection:"
#define WS_ACCEPT_KEYWORD           "Sec-WebSocket-Accept: "
#define WS_UPGRADE_STR              "Upgrade: websocket\r\n"
#define WS_CONNECT_STR              "Connection: Upgrade\r\n"
/* MACRO FUNCTION DECLARATIONS
 */

#ifdef WS_ERROR_LOG
#define WS_PRINT(...)   printf(__VA_ARGS__)
#else
#define WS_PRINT(...)
#endif


#define htonl64(p) {\
    (char)(((p & ((uint64_t)0xff <<  0)) >>  0) & 0xff), (char)(((p & ((uint64_t)0xff <<  8)) >>  8) & 0xff), \
    (char)(((p & ((uint64_t)0xff << 16)) >> 16) & 0xff), (char)(((p & ((uint64_t)0xff << 24)) >> 24) & 0xff), \
    (char)(((p & ((uint64_t)0xff << 32)) >> 32) & 0xff), (char)(((p & ((uint64_t)0xff << 40)) >> 40) & 0xff), \
    (char)(((p & ((uint64_t)0xff << 48)) >> 48) & 0xff), (char)(((p & ((uint64_t)0xff << 56)) >> 56) & 0xff) }

/* DATA TYPE DECLARATIONS
 */

typedef enum {
    WS_CONTINUE = 0x00,
    WS_TEXT_FRAME = 0x01,
    WS_BINARY_FRAME = 0x02,
    WS_CLOSE = 0x08,
    WS_PING = 0x09,
    WS_PONG = 0x0A,
    WS_TEXT_FRAME_END = 0x81,
    WS_BINARY_FRAME_END = 0x82,
}WS_OPCODE_T;

typedef struct WS_INFO_S{
    bool fin;
    bool rsv1;
    bool rsv2;
    bool rsv3;
    WS_OPCODE_T opcode;
    bool mask;
    unsigned long payload_len;
    unsigned int mask_key[4];
}WS_INFO_T;

/* Each compilation unit must define the NetworkContext struct. */
struct NetworkContext
{
    TlsTransportParams_t *pParams;
    char serverAddr[WS_ADDRESS_LEN];
    char serverPort[WS_STRING_LEN];
    char resource[WS_STRING_LEN];
    char strquery[WS_STRING_LEN];
    bool useTLS;
};

/* EXPORTED SUBPROGRAM SPECIFICATIONS
 */
extern char _wBuffer[WS_BUFFER_LENGTH];
extern char _rBuffer[WS_BUFFER_LENGTH];
extern char _seckey[WS_SECKEY_LEN];
extern NetworkContext_t _networkContext;
extern TlsTransportParams_t _mbedsslParams;

/* FUNCTION NAME: wsSend
 * PURPOSE:
 *      Send data to low level socket function.
 *
 * INPUT:
 *      ptr_buf             - A pointer of buffer of data need to send.
 *      bytesToSend         - Length of send data.
 *
 * OUTPUT:
 *      None
 *
 * RETURN:
 *      WS_SUCCESS
 *      WS_FAIL
 *
 * NOTES:
 *      None
 */
int32_t
wsSend(
    const void *ptr_buf,
    size_t bytesToSend);

/* FUNCTION NAME: wsRecv

 * PURPOSE:
 *      Receive data from low level socket function.
 *
 * INPUT:
 *      len                 - Size of receive buffer.
 *
 * OUTPUT:
 *      ptr_buf             - A pointer of buffer of receive data.
 *
 * RETURN:
 *      -1     : receive fail.
 *      others : receive length.
 *
 * NOTES:
 *      None
 */
int32_t
wsRecv(
    void *ptr_buf,
    size_t len);

/* FUNCTION NAME: wsParseFrame
 * PURPOSE:
 *      Parser packet format of websocket protocol.
 *
 * INPUT:
 *      ptr_buf             - A pointer to the buffer of received data to be parsed.
 *      len                 - Length of receive data.
 *      olen                - Size of buffer of output buffer.
 *
 * OUTPUT:
 *      ptr_obuf            - A pointer to the buffer of parsed data payload.
 *      ptr_rlen            - Length of data of parser data payload.
 *
 * RETURN:
 *      WS_SUCCESS
 *      WS_FAIL
 *
 * NOTES:
 *      None
 */
int
wsParseFrame(
    WS_OPCODE_T *ptr_code,
    char *ptr_buf,
    int len,
    char *ptr_obuf,
    int olen,
    int *ptr_rlen);

/* FUNCTION NAME: wsCreateFrame
 * PURPOSE:
 *      Create websocket packt format.
 *
 * INPUT:
 *      code                - Websocket option code.
 *                              enum WS_OPCODE_T
 *      ptr_src             - A pointer to the buffer of data information which is to be embedded
 *                            in the websocket packet.
 *      slen                - Length of data information.
 *
 * OUTPUT:
 *      ptr_dst             - A pointer to the buffer of the newly created websocket packet.
 *      dlen                - Size of buffer.
 *
 * RETURN:
 *      WS_FAIL or length of packet.
 *
 * NOTES:
 *      None
 */
int
wsCreateFrame(
    WS_OPCODE_T code,
    char *ptr_dst,
    int dlen,
    char *ptr_src,
    int slen);

/* FUNCTION NAME: wsParseUrl
 * PURPOSE:
 *      Parser URL of server address and port number.
 *
 * INPUT:
 *      ptr_url             - A pointer of url address.
 *
 * OUTPUT:
 *      None
 *
 * RETURN:
 *      WS_FAIL
 *      WS_SUCCESS
 *
 * NOTES:
 *      None
 */
int
wsParseUrl(
    char *ptr_url);

#endif /* end of WSCLIENT_H */
