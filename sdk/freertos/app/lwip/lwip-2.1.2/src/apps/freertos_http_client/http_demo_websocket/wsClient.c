/*******************************************************************************
*  Copyright Statement:
*  --------------------
*  This software is protected by Copyright and the information contained
*  herein is confidential. The software may not be copied and the information
*  contained herein may not be used or disclosed except with the written
*  permission of Airoha Technology Corp. (C) 2021
*
*  BY OPENING THIS FILE, BUYER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
*  THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("AIROHA SOFTWARE")
*  RECEIVED FROM AIROHA AND/OR ITS REPRESENTATIVES ARE PROVIDED TO BUYER ON
*  AN "AS-IS" BASIS ONLY. AIROHA EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
*  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
*  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
*  NEITHER DOES AIROHA PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
*  SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
*  SUPPLIED WITH THE AIROHA SOFTWARE, AND BUYER AGREES TO LOOK ONLY TO SUCH
*  THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. AIROHA SHALL ALSO
*  NOT BE RESPONSIBLE FOR ANY AIROHA SOFTWARE RELEASES MADE TO BUYER'S
*  SPECIFICATION OR TO CONFORM TO A PARTICULAR STANDARD OR OPEN FORUM.
*
*  BUYER'S SOLE AND EXCLUSIVE REMEDY AND AIROHA'S ENTIRE AND CUMULATIVE
*  LIABILITY WITH RESPECT TO THE AIROHA SOFTWARE RELEASED HEREUNDER WILL BE,
*  AT AIROHA'S OPTION, TO REVISE OR REPLACE THE AIROHA SOFTWARE AT ISSUE,
*  OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY BUYER TO
*  AIROHA FOR SUCH AIROHA SOFTWARE AT ISSUE.
*
*  THE TRANSACTION CONTEMPLATED HEREUNDER SHALL BE CONSTRUED IN ACCORDANCE
*  WITH THE LAWS OF THE STATE OF CALIFORNIA, USA, EXCLUDING ITS CONFLICT OF
*  LAWS PRINCIPLES.  ANY DISPUTES, CONTROVERSIES OR CLAIMS ARISING THEREOF AND
*  RELATED THERETO SHALL BE SETTLED BY ARBITRATION IN SAN FRANCISCO, CA, UNDER
*  THE RULES OF THE INTERNATIONAL CHAMBER OF COMMERCE (ICC).
*
*******************************************************************************/

/* FILE NAME:   wsClient.c
 * PURPOSE:
 *      Define webSocket client function.
 *
 * NOTES:
 */

/* INCLUDE FILE DECLARATIONS
 */

#include "wsClient.h"

/* NAMING CONSTANT DECLARATIONS
*/

/* MACRO FUNCTION DECLARATIONS
 */

/* DATA TYPE DECLARATIONS
 */

/* GLOBAL VARIABLE DECLARATIONS
 */

NetworkContext_t _networkContext;
TlsTransportParams_t _mbedsslParams;
char _wBuffer[WS_BUFFER_LENGTH] = { 0 };
char _rBuffer[WS_BUFFER_LENGTH] = { 0 };
char _seckey[WS_SECKEY_LEN] = { 0 };

/* LOCAL SUBPROGRAM SPECIFICATIONS
*/

int
_wsCheckHandShake(
    char *ptr_buf,
    int len);

int
_wsParseData(
    WS_OPCODE_T *ptr_code,
    char *ptr_buf,
    int len,
    char *ptr_obuf,
    int olen,
    int *ptr_rlen);

/* STATIC VARIABLE DECLARATIONS
 */

/* LOCAL SUBPROGRAM BODIES
 */
int
_wsCheckHandShake(
    char *ptr_buf,
    int len)
{
    char *ptr = NULL;
    const char *GUID = "258EAFA5-E914-47DA-95CA-C5AB0DC85B11";
    unsigned char tmp[WS_SHA1_LEN + 1] = { 0 };
    size_t rlen = 0;
    unsigned char ret[WS_SECKEY_LEN] = { 0 };

    ptr = strstr(ptr_buf, WS_UPGRADE_KEYWORD);
    if(NULL == ptr)
    {
        WS_PRINT("Check Upgrade format fail.\n");
        return WS_FAIL;
    }
    if(0 != strncmp(ptr, WS_UPGRADE_STR, strlen(WS_UPGRADE_STR)))
    {
        WS_PRINT("Error upgrade value\n");
        return WS_FAIL;
    }

    ptr = strstr(ptr_buf, WS_CONNECT_KEYWORD);
    if(NULL == ptr)
    {
        WS_PRINT("Check Connection format fail.\n");
        return WS_FAIL;
    }
    if(0 != strncmp(ptr, WS_CONNECT_STR, strlen(WS_CONNECT_STR)))
    {
        WS_PRINT("Error connection value\n");
        return WS_FAIL;
    }

    ptr = strstr(ptr_buf, WS_ACCEPT_KEYWORD);
    if(NULL == ptr)
    {
        WS_PRINT("Check Accept format fail.\n");
        return WS_FAIL;
    }

    memcpy(&_seckey[strlen(_seckey)], GUID, WS_GUID_LEN);
    mbedtls_sha1((unsigned char*)_seckey, strlen(_seckey), tmp);
    tmp[WS_SHA1_LEN] = 0x0;
    mbedtls_base64_encode(ret, sizeof(ret), &rlen, (unsigned char*)tmp, strlen((char*)tmp));
    if(0 != memcmp((ptr + strlen(WS_ACCEPT_KEYWORD)), ret, rlen))
    {
        WS_PRINT("Security key compare fail.\n");
        return WS_FAIL;
    }

    return WS_SUCCESS;
}

int
_wsParseData(
    WS_OPCODE_T *ptr_code,
    char *ptr_buf,
    int len,
    char *ptr_obuf,
    int olen,
    int *ptr_rlen)
{
    int ret = WS_SUCCESS;
    WS_INFO_T wsInfo;
    unsigned long long dataLen = 0;
    int i = 0, offt = WS_HEADER_LEN, useLen = 0, frameLen = 0;

    *ptr_rlen = 0;
    do
    {
        /* Parser header */
        wsInfo.fin = ((ptr_buf[useLen] & WS_FIN_OFFT) == WS_FIN_OFFT ? TRUE : FALSE);
        wsInfo.rsv1 = ((ptr_buf[useLen] & WS_RSV1_OFFT) == WS_RSV1_OFFT ? TRUE : FALSE);
        wsInfo.rsv2 = ((ptr_buf[useLen] & WS_RSV2_OFFT) == WS_RSV2_OFFT ? TRUE : FALSE);
        wsInfo.rsv3 = ((ptr_buf[useLen] & WS_RSV3_OFFT) == WS_RSV3_OFFT ? TRUE : FALSE);
        wsInfo.opcode = (ptr_buf[useLen] & WS_CODE_OFFT);
        wsInfo.mask = ((ptr_buf[useLen + 1] & WS_MASK_OFFT) == WS_MASK_OFFT ? TRUE : FALSE);
        wsInfo.payload_len = (ptr_buf[useLen + 1] & WS_DLEN_OFFT);

        if(TRUE == wsInfo.mask)
        {
            WS_PRINT("Error: Server should not send mask data.\n");
            return WS_FAIL;
        }

        dataLen = wsInfo.payload_len;

        /* Parser extended length */
        if(WS_EXTENDED_16_MSK == wsInfo.payload_len)
        {
            if(len < (WS_HEADER_LEN + WS_EXTENDED_16_SIZE))
            {
                return WS_WAITFRAME;
            }

            dataLen = 0;
            for(i = 0; i < WS_EXTENDED_16_SIZE; i ++)
            {
                dataLen |= ((unsigned char)ptr_buf[useLen + (WS_HEADER_LEN + i)] << (8 - (8 * i)));
            }
            offt += WS_EXTENDED_16_SIZE;
        }
        else if(WS_EXTENDED_64_MSK == wsInfo.payload_len)
        {
            if(len < (WS_HEADER_LEN + WS_EXTENDED_64_SIZE))
            {
                return WS_WAITFRAME;
            }

            dataLen = 0;
            for(i = 0; i < WS_EXTENDED_64_SIZE; i ++)
            {
                dataLen |= ((unsigned char)ptr_buf[useLen + (WS_HEADER_LEN + i)] << (56 - (8 * i)));
            }
            offt += WS_EXTENDED_16_SIZE;
        }

        frameLen = offt + dataLen;
        if(len < (useLen + frameLen))
        {
            return WS_WAITFRAME;
        }

        /* Parser data type */
        if((TRUE == wsInfo.fin) && (WS_TEXT_FRAME == wsInfo.opcode))
        {
            if(olen < (*ptr_rlen + dataLen))
            {
                return WS_FAIL;
            }
            *ptr_code = WS_TEXT_FRAME_END;
            memcpy(&ptr_obuf[*ptr_rlen], &ptr_buf[useLen + offt], dataLen);
            *ptr_rlen += dataLen;
        }
        else if((TRUE == wsInfo.fin) && (WS_BINARY_FRAME == wsInfo.opcode))
        {
            if(olen < (*ptr_rlen + dataLen))
            {
                return WS_FAIL;
            }
            *ptr_code = WS_BINARY_FRAME_END;
            memcpy(&ptr_obuf[*ptr_rlen], &ptr_buf[useLen + offt], dataLen);
            *ptr_rlen += dataLen;
        }
        else if(WS_CONTINUE == wsInfo.opcode)
        {
            /* Nothing to do */
            *ptr_code = WS_CONTINUE;
            return WS_WAITFRAME;
        }
        else if(WS_PING == wsInfo.opcode)
        {
            if(TRUE != wsInfo.fin)
            {
                WS_PRINT("Error: Control message must not be fragmented\n");
                return WS_FAIL;
            }
            dataLen = wsCreateFrame(WS_PONG, _wBuffer, WS_BUFFER_LENGTH, &ptr_buf[useLen + offt], dataLen);
            wsSend(_wBuffer, dataLen);
        }
        else if(WS_PONG == wsInfo.opcode)
        {
            /* Nothing to do */
        }
        else if(WS_CLOSE == wsInfo.opcode)
        {
            /* Return fail to disconnected */
            ret = WS_FAIL;
        }
        else
        {
            WS_PRINT("Error: Invalid opcode\n");
            ret = WS_FAIL;
        }

        useLen += frameLen;
    }
    while(useLen < len);

    return ret;
}

/* EXPORTED SUBPROGRAM BODIES
 */

/* FUNCTION NAME: wsSend
 * PURPOSE:
 *      Send data to low level socket function.
 *
 * INPUT:
 *      ptr_buf             - A pointer of buffer of data need to send.
 *      bytesToSend         - Length of send data.
 *
 * OUTPUT:
 *      None
 *
 * RETURN:
 *      WS_SUCCESS
 *      WS_FAIL
 *
 * NOTES:
 *      None
 */
int32_t
wsSend(
    const void *ptr_buf,
    size_t bytesToSend)
{
    int ret = WS_SUCCESS;
    int len = 0;

    if(TRUE == _networkContext.useTLS)
    {
        len = MbedTLS_Send(&_networkContext, ptr_buf, bytesToSend);
    }
    else
    {
        len = send(_mbedsslParams.tcpSocket, ptr_buf, bytesToSend, 0);
    }
    if(len != bytesToSend)
    {
        WS_PRINT("Need send [%d], Send byte [%d]\n", (int)bytesToSend, len);
        ret = WS_FAIL;
    }
    else
    {
        ret = WS_SUCCESS;
    }

    return ret;
}

/* FUNCTION NAME: wsRecv
 * PURPOSE:
 *      Receive data from low level socket function.
 *
 * INPUT:
 *      len                 - Size of receive buffer.
 *
 * OUTPUT:
 *      ptr_buf             - A pointer of buffer of receive data.
 *
 * RETURN:
 *      -1     : receive fail.
 *      others : receive length.
 *
 * NOTES:
 *      None
 */
int32_t
wsRecv(
    void *ptr_buf,
    size_t len)
{
    if(TRUE == _networkContext.useTLS)
    {
        return MbedTLS_Recv(&_networkContext, ptr_buf, len);
    }
    else
    {
        return recv(_mbedsslParams.tcpSocket, ptr_buf, len, MSG_DONTWAIT);
    }

    return 0;
}

/* FUNCTION NAME: wsParseFrame
 * PURPOSE:
 *      Parser packet format of websocket protocol.
 *
 * INPUT:
 *      ptr_buf             - A pointer to the buffer of received data to be parsed.
 *      len                 - Length of receive data.
 *      olen                - Size of buffer of output buffer.
 *
 * OUTPUT:
 *      ptr_obuf            - A pointer to the buffer of parsed data payload.
 *      ptr_rlen            - Length of data of parser data payload.
 *
 * RETURN:
 *      WS_SUCCESS
 *      WS_FAIL
 *
 * NOTES:
 *      None
 */
int
wsParseFrame(
    WS_OPCODE_T *ptr_code,
    char *ptr_buf,
    int len,
    char *ptr_obuf,
    int olen,
    int *ptr_rlen)
{
    int useLen = 0;
    int ret = WS_SUCCESS;
    char *offt = NULL;

    do
    {
        if('H' == ptr_buf[useLen])
        {
            offt = strstr(&ptr_buf[useLen], "\r\n\r\n");
            if(NULL == offt)
            {
                ret = WS_WAITFRAME;
                break;
            }
            else
            {
                if((0 != strncmp(&ptr_buf[useLen], "HTTP/1.1 101", 12)) && (0 != strncmp(&ptr_buf[useLen], "HTTP/1.0 101", 12)))
                {
                    WS_PRINT("Error: handshake error\n");
                    ret = WS_FAIL;
                    break;
                }
                else
                {
                    ret = _wsCheckHandShake(&ptr_buf[useLen], (len - useLen));
                    useLen += ((int)(offt - &ptr_buf[useLen]) + 4);
                    *ptr_rlen = 0;
                }
            }
        }
        else
        {
            ret = _wsParseData(ptr_code, &ptr_buf[useLen], (len - useLen), ptr_obuf, olen, ptr_rlen);
            useLen = len;
        }
    }
    while(useLen < len);

    return ret;
}

/* FUNCTION NAME: wsCreateFrame
 * PURPOSE:
 *      Create websocket packt format.
 *
 * INPUT:
 *      code                - Websocket option code.
 *                              enum WS_OPCODE_T
 *      ptr_src             - A pointer to the buffer of data information which is to be embedded
 *                            in the websocket packet.
 *      slen                - Length of data information.
 *
 * OUTPUT:
 *      ptr_dst             - A pointer to the buffer of the newly created websocket packet.
 *      dlen                - Size of buffer.
 *
 * RETURN:
 *      WS_FAIL or length of packet.
 *
 * NOTES:
 *      None
 */
int
wsCreateFrame(
    WS_OPCODE_T code,
    char *ptr_dst,
    int dlen,
    char *ptr_src,
    int slen)
{
    unsigned char magic[WS_MAGIC_KEY_SIZE] = { 0 };
    int framelen = 0, offt = 0, i = 0;

    memset(ptr_dst, 0, dlen);

    for(i = 0; i < WS_MAGIC_KEY_SIZE; i ++)
    {
        magic[i] = (rand() % 0xFF);
    }

    framelen = WS_HEADER_LEN + WS_MAGIC_KEY_SIZE;
    if(dlen < framelen)
    {
        WS_PRINT("Error: output buffer too small.\n");
        return WS_FAIL;
    }

    /* Set header data */
    ptr_dst[offt] = (code | WS_FIN_OFFT);
    offt ++;

    if(slen < WS_EXTENDED_16_MSK)
    {
        ptr_dst[offt] = (slen | WS_MASK_OFFT);
        offt ++;

        for(i = 0; i < WS_MAGIC_KEY_SIZE; i ++)
        {
            ptr_dst[offt + i] = magic[i];
        }
    }
    else if(slen > 0xFFFF && slen < 0xFFFFFFFFFFFFFFFF)
    {
        char len64[WS_EXTENDED_64_SIZE] = htonl64(slen);

        framelen += WS_EXTENDED_64_SIZE;
        if(dlen < framelen)
        {
            WS_PRINT("Error: output buffer too small.\n");
            return WS_FAIL;
        }

        ptr_dst[offt] = (WS_EXTENDED_64_MSK | WS_MASK_OFFT);
        offt ++;

        memcpy(ptr_dst + offt, len64, WS_EXTENDED_64_SIZE);
        offt += WS_EXTENDED_64_SIZE;
    }
    else if(slen > 125 && slen <= 0xFFFF)
    {
        unsigned short len16 = htons(slen);

        framelen += WS_EXTENDED_16_SIZE;
        if(dlen < framelen)
        {
            WS_PRINT("Error: output buffer too small.\n");
            return WS_FAIL;
        }

        ptr_dst[offt] = (WS_EXTENDED_16_MSK | WS_MASK_OFFT);
        offt ++;

        memcpy(ptr_dst + WS_HEADER_LEN, (char*)&len16, WS_EXTENDED_16_SIZE);
        offt += WS_EXTENDED_16_SIZE;
    }
    else
    {
        WS_PRINT("Error: frame too large");
        return WS_FAIL;
    }

    for(i = 0; i < WS_MAGIC_KEY_SIZE; i ++, offt ++)
    {
        ptr_dst[offt] = magic[i];
    }

    framelen += slen;
    if(dlen < framelen)
    {
        WS_PRINT("Error: output buffer too small.\n");
        return WS_FAIL;
    }

    memcpy(&ptr_dst[offt], ptr_src, slen);
    for(i = 0; i < slen; i ++)
    {
        ptr_dst[offt + i] ^= (magic[i % WS_MAGIC_KEY_SIZE] & 0xFF);
    }

    return framelen;
}

/* FUNCTION NAME: wsParseUrl
 * PURPOSE:
 *      Parser URL of server address and port number.
 *
 * INPUT:
 *      ptr_url             - A pointer of url address.
 *
 * OUTPUT:
 *      None
 *
 * RETURN:
 *      WS_FAIL
 *      WS_SUCCESS
 *
 * NOTES:
 *      None
 */
int
wsParseUrl(
    char *ptr_url)
{

    _networkContext.useTLS = FALSE;
    if(4 == sscanf(ptr_url, "ws://%[^:]:%[^/]%[^?]%s"
                            ,_networkContext.serverAddr
                            ,_networkContext.serverPort
                            ,_networkContext.resource
                            ,_networkContext.strquery))
    {
        return WS_SUCCESS;
    }
    else if(3 == sscanf(ptr_url, "ws://%[^:]:%[^/]%s"
                            ,_networkContext.serverAddr
                            ,_networkContext.serverPort
                            ,_networkContext.resource))
    {
        memset(_networkContext.strquery, 0, WS_STRING_LEN);

        return WS_SUCCESS;
    }
    else if(2 == sscanf(ptr_url, "ws://%[^:]:%[^/]%s"
                            ,_networkContext.serverAddr
                            ,_networkContext.serverPort
                            ,_networkContext.resource))
    {
        strcpy(_networkContext.resource, WS_DEFAULT_RESOURCE);
        memset(_networkContext.strquery, 0, WS_STRING_LEN);

        return WS_SUCCESS;
    }
    else if(2 == sscanf(ptr_url, "ws://%[^/]%s"
                            ,_networkContext.serverAddr
                            ,_networkContext.resource))
    {
        strcpy(_networkContext.serverPort, WS_DEFAULT_PORT);
        memset(_networkContext.strquery, 0, WS_STRING_LEN);

        return WS_SUCCESS;
    }
    else if(1 == sscanf(ptr_url, "ws://%[^/]"
                            ,_networkContext.serverAddr))
    {
        strcpy(_networkContext.serverPort, WS_DEFAULT_PORT);
        strcpy(_networkContext.resource, WS_DEFAULT_RESOURCE);
        memset(_networkContext.strquery, 0, WS_STRING_LEN);

        return WS_SUCCESS;
    }
    else if(4 == sscanf(ptr_url, "wss://%[^:]:%[^/]%[^?]%s"
                            ,_networkContext.serverAddr
                            ,_networkContext.serverPort
                            ,_networkContext.resource
                            ,_networkContext.strquery))
    {
        _networkContext.useTLS = TRUE;
        return WS_SUCCESS;
    }
    else if(3 == sscanf(ptr_url, "wss://%[^:]:%[^/]%s"
                            ,_networkContext.serverAddr
                            ,_networkContext.serverPort
                            ,_networkContext.resource))
    {
        memset(_networkContext.strquery, 0, WS_STRING_LEN);
        _networkContext.useTLS = TRUE;

        return WS_SUCCESS;
    }
    else if(2 == sscanf(ptr_url, "wss://%[^:]:%[^/]%s"
                            ,_networkContext.serverAddr
                            ,_networkContext.serverPort
                            ,_networkContext.resource))
    {
        strcpy(_networkContext.resource, WS_DEFAULT_RESOURCE);
        memset(_networkContext.strquery, 0, WS_STRING_LEN);
        _networkContext.useTLS = TRUE;

        return WS_SUCCESS;
    }
    else if(2 == sscanf(ptr_url, "wss://%[^/]%s"
                            ,_networkContext.serverAddr
                            ,_networkContext.resource))
    {
        strcpy(_networkContext.serverPort, WS_DEFAULT_PORT);
        memset(_networkContext.strquery, 0, WS_STRING_LEN);
        _networkContext.useTLS = TRUE;

        return WS_SUCCESS;
    }
    else if(1 == sscanf(ptr_url, "wss://%[^/]"
                            ,_networkContext.serverAddr))
    {
        strcpy(_networkContext.serverPort, WS_DEFAULT_PORT);
        strcpy(_networkContext.resource, WS_DEFAULT_RESOURCE);
        memset(_networkContext.strquery, 0, WS_STRING_LEN);
        _networkContext.useTLS = TRUE;

        return WS_SUCCESS;
    }
    else
    {
        strcpy(_networkContext.serverAddr, WS_DEFAULT_SERVER);
        strcpy(_networkContext.serverPort, WS_DEFAULT_PORT);
        strcpy(_networkContext.resource, WS_DEFAULT_RESOURCE);
        memset(_networkContext.strquery, 0, WS_STRING_LEN);
    }

    return WS_FAIL;
}

