/* FILE NAME:  air_app_sfp.c
 * PURPOSE:
 *      SFP application.
 * NOTES:
 *
 */

/* INCLUDE FILE DECLARATIONS
 */
#include "FreeRTOS.h"
#include "task.h"
#include "air_i2c.h"
#include "air_port.h"
#include "hwcfg_util.h"
#include "pp_def.h"
#include "air_app_sfp.h"
#include "air_gpio.h"
#include "air_sif.h"

/* NAMING CONSTANT DECLARATIONS
 */

/* MACRO FUNCTION DECLARATIONS
 */
#define AIR_APP_SFP_DEBUG_DETAIL(fmt,...) do                           \
{                                                                      \
    if (g_debug_detail)                                                \
    {                                                                  \
        printf(fmt, ##__VA_ARGS__);                                    \
    }                                                                  \
}while (0)

/* DATA TYPE DECLARATIONS
 */

/* GLOBAL VARIABLE DECLARATIONS
 */
TaskHandle_t air_app_sfp_taskHandle;
static int g_debug_detail = 0;

static int i2c_channel = EN8853_SFP_I2C_CHANNEL;
static int sfp_control = AIR_APP_SFP_CONTROL_PIN_I2C;
static AIR_APP_SFP_T sfp[] =
{
/*   AIR_PORT        MOD_ABS        TX_DISABLE        RX_LOSS       TX_FAULT        MUX_CHANNEL        STATE                   */
    {25,             2,             3,                1,            0,              0x4,               AIR_APP_SFP_STATE_ABSENT},
    {26,             6,             7,                5,            4,              0x5,               AIR_APP_SFP_STATE_ABSENT},
    {27,             14,            15,               13,           12,             0x6,               AIR_APP_SFP_STATE_ABSENT},
    {28,             10,            11,               9,            8,              0x7,               AIR_APP_SFP_STATE_ABSENT}
};

/* EXPORTED SUBPROGRAM BODIES
 */

/* LOCAL SUBPROGRAM BODIES
 */

/* FUNCTION NAME:   _air_app_sfp_initI2cConfig
 * PURPOSE:
 *      Initialize I2C related configuration.
 * INPUT:
 *      None
 * OUTPUT:
 *      None
 * RETURN:
 *      None
 * NOTES:
 *
 */
static int _air_app_sfp_initI2cConfig(void)
{
    AIR_SIF_INFO_T sif_info;
    AIR_SIF_PARAM_T sif_param;
    int i, rc = E_OK;

    sif_info.channel = i2c_channel;
    sif_info.slave_id = PCA9535_I2C_ADDRESS;
    sif_param.addr_len = 1;
    sif_param.addr = PCA9535_CONTROL_ADDRESS;
    sif_param.data_len = 2;
    sif_param.info.data = 0;
    /* set intput pin */
    for (i = 0; i < sizeof(sfp) / sizeof(AIR_APP_SFP_T); i++)
    {
        sif_param.info.data |= 1 << sfp[i].abs;
        sif_param.info.data |= 1 << sfp[i].rx_loss;
        sif_param.info.data |= 1 << sfp[i].tx_fault;
    }
    rc = air_sif_write(0, &sif_info, &sif_param);
    if (E_OK != rc)
    {
        printf("[SFP_APP]: Failed to set io expender input pin to %x.\n", sif_param.info.data);
    }
    return rc;
}

/* FUNCTION NAME:   _air_app_sfp_getPin
 * PURPOSE:
 *      Get I2C pin value.
 * INPUT:
 *      None
 * OUTPUT:
 *      None
 * RETURN:
 *      None
 * NOTES:
 *
 */
static int _air_app_sfp_getPin(int pin, int *ptr_value)
{
    AIR_SIF_INFO_T sif_info;
    AIR_SIF_PARAM_T sif_param;
    int rc = E_OK;

    sif_info.channel = i2c_channel;
    sif_info.slave_id = PCA9535_I2C_ADDRESS;
    sif_param.addr_len = 1;
    sif_param.addr = PCA9535_INPUT_ADDRESS;
    sif_param.data_len = 2;
    sif_param.info.data = 0;

    rc = air_sif_read(0, &sif_info, &sif_param);
    if (E_OK == rc)
    {
        *ptr_value = (sif_param.info.data >> pin) & 1;
    }
    else
    {
        AIR_APP_SFP_DEBUG_DETAIL("[SFP_APP]: Failed to get I2C io expender input pin %d value.\n", pin);
    }
    return rc;
}

/* FUNCTION NAME:   _air_app_sfp_setPin
 * PURPOSE:
 *      Set I2C pin value.
 * INPUT:
 *      None
 * OUTPUT:
 *      None
 * RETURN:
 *      None
 * NOTES:
 *
 */
static int _air_app_sfp_setPin(int pin, int value)
{
    AIR_SIF_INFO_T sif_info;
    AIR_SIF_PARAM_T sif_param;
    int rc = E_OK;

    sif_info.channel = i2c_channel;
    sif_info.slave_id = PCA9535_I2C_ADDRESS;
    sif_param.addr_len = 1;
    sif_param.addr = PCA9535_OUTPUT_ADDRESS;
    sif_param.data_len = 2;
    sif_param.info.data = 0;

    rc = air_sif_read(0, &sif_info, &sif_param);
    if (E_OK == rc)
    {
        if (value)
        {
            sif_param.info.data |= (1 << pin);
        }
        else
        {
            sif_param.info.data &= ~(1 << pin);
        }
        rc = air_sif_write(0, &sif_info, &sif_param);
    }

    if (E_OK != rc)
    {
        AIR_APP_SFP_DEBUG_DETAIL("[SFP_APP]: Failed to set I2C io expender output pin %d value %d.\n", pin, value);
    }
    return rc;
}

/* FUNCTION NAME:   _air_app_sfp_checkMediaType
 * PURPOSE:
 *      Check SFP media type.
 * INPUT:
 *      channel    --  Channel of the SFP multiplexer
 * OUTPUT:
 *      None
 * RETURN:
 *      None
 * NOTES:
 *
 */
static int _air_app_sfp_checkMediaType(int channel)
{
    int rc;
    AIR_SIF_INFO_T sif_info;
    AIR_SIF_PARAM_T sif_param;
    int ether_code, fiber_code;

    sif_info.channel = i2c_channel;
    sif_info.slave_id = PCA954XA_I2C_ADDRESS;
    sif_param.addr_len = 1;
    sif_param.addr = 0;
    sif_param.data_len = 1;
    sif_param.info.data = channel;
    rc = air_sif_write(0, &sif_info, &sif_param);
    if (E_OK != rc)
    {
        AIR_APP_SFP_DEBUG_DETAIL("[SFP_APP]: Failed to change mux channel to %d.\n", channel);
    }

    sif_info.slave_id = SFP_A0H_ADDRESS;
    sif_param.addr = SFP_COMPLIANCE_CODE_FIBER_TYPE_BASE;
    sif_param.data_len = 4;
    sif_param.info.data = 0;
    rc = air_sif_read(0, &sif_info, &sif_param);
    if (E_OK != rc)
    {
        AIR_APP_SFP_DEBUG_DETAIL("[SFP_APP]: Failed to read Transceiver Compliance Codes on mux channel %d.\n", channel);
        return AIR_APP_SFP_TYPE_LAST;
    }
    fiber_code = sif_param.info.data;
    sif_param.addr = SFP_ETHERNET_COMPLIANCE_CODE_BASE;
    sif_param.data_len = 1;
    sif_param.info.data = 0;
    rc = air_sif_read(0, &sif_info, &sif_param);
    if (E_OK != rc)
    {
        AIR_APP_SFP_DEBUG_DETAIL("[SFP_APP]: Failed to read Transceiver Compliance Codes on mux channel %d.\n", channel);
        return AIR_APP_SFP_TYPE_LAST;
    }
    ether_code = sif_param.info.data;
    if ((SFP_COMPLIANCE_CODE_FIBER_TYPE_MASK & fiber_code) || (SFP_ETHERNET_COMPLIANCE_CODE_1000BASET != ether_code))
    {
        return AIR_APP_SFP_TYPE_FIBER;
    }
    else
    {
        return AIR_APP_SFP_TYPE_COPPER;
    }
}

/* FUNCTION NAME:   _air_app_sfp_isStateChange
 * PURPOSE:
 *      Handle SFP state change.
 * INPUT:
 *      p_sfp    --  Pointer for SFP node
 * OUTPUT:
 *      None
 * RETURN:
 *      TRUE
 *      FALSE
 * NOTES:
 *
 */
static int _air_app_sfp_isStateChange(AIR_APP_SFP_T *p_sfp)
{
    int rc = 0;
    unsigned int data = 0;

    if (AIR_APP_SFP_CONTROL_PIN_GPIO == sfp_control)
    {
        rc = air_gpio_getValue(p_sfp->abs, &data);
    }
    else if (AIR_APP_SFP_CONTROL_PIN_I2C == sfp_control)
    {
        rc = _air_app_sfp_getPin(p_sfp->abs, &data);
    }
    else
    {
        return FALSE;
    }

    if (E_OK == rc)
    {
        if ((AIR_APP_SFP_STATE_ABSENT == p_sfp->state)  && (SFP_PIN_LOW == data))
        {
            printf("[SFP_APP]: Port:%d receives MOD_ABS low. State change ABSENT --> PRESENT\n", p_sfp->port);
            p_sfp->state = AIR_APP_SFP_STATE_PRESENT;
        }
        else if ((p_sfp->state > AIR_APP_SFP_STATE_ABSENT) && (SFP_PIN_HIGH == data))
        {
            printf("[SFP_APP]: Port:%d receives MOD_ABS high. State change to ABSENT\n", p_sfp->port);
            p_sfp->state = AIR_APP_SFP_STATE_ABSENT;
        }
        else if (AIR_APP_SFP_STATE_PRESENT == p_sfp->state)
        {
            if (AIR_APP_SFP_TYPE_LAST == _air_app_sfp_checkMediaType(p_sfp->channel))
            {
                printf("[SFP_APP]: Port:%d failed on media type checking. State change PERSENT --> INDETERMINATE\n", p_sfp->port);
                p_sfp->state = AIR_APP_SFP_STATE_INDETERMINATE;
            }
            else
            {
                printf("[SFP_APP]: Port:%d passed on media type checking. State change PERSENT --> ACTIVE\n", p_sfp->port);
                p_sfp->state = AIR_APP_SFP_STATE_ACTIVE;
            }
        }
        else if (AIR_APP_SFP_STATE_ACTIVE == p_sfp->state)
        {
            if (AIR_APP_SFP_CONTROL_PIN_GPIO == sfp_control)
            {
                rc = air_gpio_getValue(p_sfp->rx_loss, &data);
            }
            else
            {
                rc = _air_app_sfp_getPin(p_sfp->rx_loss, &data);
            }

            if (E_OK == rc)
            {
                if (SFP_PIN_HIGH == data)
                {
                    printf("[SFP_APP]: Port:%d receives RX_LOSS signal. State change ACTIVE --> INDETERMINATE\n", p_sfp->port);
                    p_sfp->state = AIR_APP_SFP_STATE_INDETERMINATE;
                }
                else
                {
                    return FALSE;
                }
            }
            else
            {
                AIR_APP_SFP_DEBUG_DETAIL("[SFP_APP]: Port:%d failed to get RX_LOS state.\n", p_sfp->port);
            }
        }
        else if (AIR_APP_SFP_STATE_INDETERMINATE == p_sfp->state)
        {
            if (AIR_APP_SFP_CONTROL_PIN_GPIO == sfp_control)
            {
                rc = air_gpio_getValue(p_sfp->rx_loss, &data);
            }
            else
            {
                rc = _air_app_sfp_getPin(p_sfp->rx_loss, &data);
            }

            if (E_OK == rc)
            {
                if (SFP_PIN_LOW == data)
                {
                    printf("[SFP_APP]: Port:%d RX_LOSS signal cleared. State change INDETERMINATE --> PRESENT\n", p_sfp->port);
                    p_sfp->state = AIR_APP_SFP_STATE_PRESENT;
                }
                else
                {
                    return FALSE;
                }
            }
            else
            {
                AIR_APP_SFP_DEBUG_DETAIL("[SFP_APP]: Port:%d failed to get RX_LOS state.\n", p_sfp->port);
            }
        }
        else
        {
            return FALSE;
        }
        return TRUE;
    }
    AIR_APP_SFP_DEBUG_DETAIL("[SFP_APP]: Port:%d failed to get MOD_ABS state.\n", p_sfp->port);
    return FALSE;
}

/* FUNCTION NAME:   _air_app_sfp_doStateChangeAction
 * PURPOSE:
 *      Handle SFP state change.
 * INPUT:
 *      p_sfp    --  Pointer for SFP node
 * OUTPUT:
 *      None
 * RETURN:
 *      None
 * NOTES:
 *
 */
static void _air_app_sfp_doStateChangeAction(AIR_APP_SFP_T *p_sfp)
{
    int rc, i, chk = 0;

    switch(p_sfp->state)
    {
        case AIR_APP_SFP_STATE_ABSENT:
            rc = air_port_setComboMode(0, p_sfp->port, AIR_PORT_COMBO_MODE_PHY);
            if (E_OK != rc)
            {
                AIR_APP_SFP_DEBUG_DETAIL("[SFP_APP]: Port:%d failed to change to PHY mode.\n", p_sfp->port);
            }
            if (AIR_APP_SFP_CONTROL_PIN_I2C == sfp_control)
            {
                rc = _air_app_sfp_setPin(p_sfp->tx_disable, SFP_PIN_HIGH);
            }
            else
            {
                rc = air_gpio_setValue(p_sfp->tx_disable, SFP_PIN_HIGH);
            }
            if (E_OK != rc)
            {
                AIR_APP_SFP_DEBUG_DETAIL("[SFP_APP]: Port:%d failed to set tx-disable pin high.\n", p_sfp->port);
            }
            rc = air_port_setSerdesMode(0, p_sfp->port, AIR_PORT_SERDES_MODE_SGMII);
            if (E_OK != rc)
            {
                AIR_APP_SFP_DEBUG_DETAIL("[SFP_APP]: Port:%d failed to set SERDES to SGMII.\n", p_sfp->port);
            }
            break;
        case AIR_APP_SFP_STATE_PRESENT:
            rc = air_port_setComboMode(0, p_sfp->port, AIR_PORT_COMBO_MODE_SERDES);
            if (E_OK != rc)
            {
                AIR_APP_SFP_DEBUG_DETAIL("[SFP_APP]: Port:%d failed to change to SERDES mode.\n", p_sfp->port);
            }
            if (AIR_APP_SFP_CONTROL_PIN_I2C == sfp_control)
            {
                rc = _air_app_sfp_setPin(p_sfp->tx_disable, SFP_PIN_LOW);
            }
            else
            {
                rc = air_gpio_setValue(p_sfp->tx_disable, SFP_PIN_LOW);
            }
            if (E_OK != rc)
            {
                AIR_APP_SFP_DEBUG_DETAIL("[SFP_APP]: Port:%d failed to set tx-disable pin low.\n", p_sfp->port);
            }
            break;
        case AIR_APP_SFP_STATE_ACTIVE:
            if (AIR_APP_SFP_TYPE_FIBER == _air_app_sfp_checkMediaType(p_sfp->channel))
            {
                rc = air_port_setSerdesMode(0, p_sfp->port, AIR_PORT_SERDES_MODE_1000BASE_X);
                if (E_OK != rc)
                {
                    AIR_APP_SFP_DEBUG_DETAIL("[SFP_APP]: Port:%d failed to set SERDES to 1000BASE-X.\n", p_sfp->port);
                }
            }
            break;
        case AIR_APP_SFP_STATE_INDETERMINATE:
            break;
        default:
            break;
    }
    return;
}

/* FUNCTION NAME:   _air_app_sfp_runAppTask
 * PURPOSE:
 *      SFP application task.
 * INPUT:
 *      None
 * OUTPUT:
 *      None
 * RETURN:
 *      None

 * NOTES:
 *
 */
static void _air_app_sfp_runAppTask(void)
{
    int rc = E_OK, i;

    if (AIR_APP_SFP_CONTROL_PIN_I2C == sfp_control)
    {
        rc = _air_app_sfp_initI2cConfig();
    }

    if (E_OK != rc)
    {
        printf("[SFP_APP]: Task init failed.\n");
        vTaskDelete(air_app_sfp_taskHandle);
        return;
    }

    while(1)
    {
        for (i = 0; i < sizeof(sfp) / sizeof(AIR_APP_SFP_T); i++)
        {
            if (_air_app_sfp_isStateChange(&sfp[i]))
            {
                _air_app_sfp_doStateChangeAction(&sfp[i]);
            }
            vTaskDelay(pdMS_TO_TICKS(SFP_DEFAULT_DELAY_MS));
        }
    }
}

/* FUNCTION NAME:   air_app_sfp_createSfpAppTask
 * PURPOSE:
 *      Create SFP application task.
 * INPUT:
 *      None
 * OUTPUT:
 *      None
 * RETURN:
 *      None

 * NOTES:
 *
 */
void air_app_sfp_createSfpAppTask(void)
{
    xTaskCreate(_air_app_sfp_runAppTask,        /* The function that implements the task. */
                "APP_SFP",                      /* The text name assigned to the task - for debug only as it is not used by the kernel. */
                320,                            /* The size of the stack to allocate to the task. */
                NULL,                           /* The parameter passed to the task - not used in this simple case. */
                2,                              /* The priority assigned to the task. */
                &air_app_sfp_taskHandle);
    return;
}
