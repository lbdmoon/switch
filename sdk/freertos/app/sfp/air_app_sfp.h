#ifndef AIR_APP_SFP_H
#define AIR_APP_SFP_H

/* INCLUDE FILE DECLARTIONS
*/

/* NAMING CONSTANT DECLARATIONS
*/
#define AIR_APP_SFP_CONTROL_PIN_I2C            (0)
#define AIR_APP_SFP_CONTROL_PIN_GPIO           (1)
#define SFP_DEFAULT_DELAY_MS                   (100)
#define SFP_A0H_ADDRESS                        (0x50)
#define SFP_A2H_ADDRESS                        (0x52)
#define SFP_ETHERNET_COMPLIANCE_CODE_BASE      (0x06)
#define SFP_ETHERNET_COMPLIANCE_CODE_1000BASET (0x08)
#define SFP_COMPLIANCE_CODE_FIBER_TYPE_BASE    (0x07)
#define SFP_COMPLIANCE_CODE_FIBER_TYPE_MASK    (0xFFFFF0FF)
#define SFP_PIN_HIGH                           (1)
#define SFP_PIN_LOW                            (0)

#define EN8853_SFP_I2C_CHANNEL                 (0x0)
#define EN8851_SFP_I2C_CHANNEL                 (0x1)
#define PCA954XA_I2C_ADDRESS                   (0x70)
#define PCA9535_I2C_ADDRESS                    (0x21)
#define PCA9535_INPUT_ADDRESS                  (0x0)
#define PCA9535_OUTPUT_ADDRESS                 (0x2)
#define PCA9535_CONTROL_ADDRESS                (0x6)

/* DATA TYPE DECLARATIONS
 */
typedef enum
{
    AIR_APP_SFP_STATE_ABSENT,
    AIR_APP_SFP_STATE_PRESENT,
    AIR_APP_SFP_STATE_ACTIVE,
    AIR_APP_SFP_STATE_INDETERMINATE,
    AIR_APP_SFP_STATE_LAST
} AIR_APP_SFP_STATE_T;

typedef enum
{
    AIR_APP_SFP_TYPE_COPPER,
    AIR_APP_SFP_TYPE_FIBER,
    AIR_APP_SFP_TYPE_LAST
} AIR_APP_SFP_TYPE_T;

typedef struct
{
    int port;
    int abs;
    int tx_disable;
    int rx_loss;
    int tx_fault;
    int channel;
    AIR_APP_SFP_STATE_T state;
} AIR_APP_SFP_T;

void air_app_sfp_createSfpAppTask(void);

#endif
