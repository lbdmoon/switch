/*******************************************************************************
*  Copyright Statement:
*  --------------------
*  This software is protected by Copyright and the information contained
*  herein is confidential. The software may not be copied and the information
*  contained herein may not be used or disclosed except with the written
*  permission of Airoha Technology Corp. (C) 2021
*
*  BY OPENING THIS FILE, BUYER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
*  THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("AIROHA SOFTWARE")
*  RECEIVED FROM AIROHA AND/OR ITS REPRESENTATIVES ARE PROVIDED TO BUYER ON
*  AN "AS-IS" BASIS ONLY. AIROHA EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
*  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
*  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
*  NEITHER DOES AIROHA PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
*  SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
*  SUPPLIED WITH THE AIROHA SOFTWARE, AND BUYER AGREES TO LOOK ONLY TO SUCH
*  THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. AIROHA SHALL ALSO
*  NOT BE RESPONSIBLE FOR ANY AIROHA SOFTWARE RELEASES MADE TO BUYER'S
*  SPECIFICATION OR TO CONFORM TO A PARTICULAR STANDARD OR OPEN FORUM.
*
*  BUYER'S SOLE AND EXCLUSIVE REMEDY AND AIROHA'S ENTIRE AND CUMULATIVE
*  LIABILITY WITH RESPECT TO THE AIROHA SOFTWARE RELEASED HEREUNDER WILL BE,
*  AT AIROHA'S OPTION, TO REVISE OR REPLACE THE AIROHA SOFTWARE AT ISSUE,
*  OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY BUYER TO
*  AIROHA FOR SUCH AIROHA SOFTWARE AT ISSUE.
*
*  THE TRANSACTION CONTEMPLATED HEREUNDER SHALL BE CONSTRUED IN ACCORDANCE
*  WITH THE LAWS OF THE STATE OF CALIFORNIA, USA, EXCLUDING ITS CONFLICT OF
*  LAWS PRINCIPLES.  ANY DISPUTES, CONTROVERSIES OR CLAIMS ARISING THEREOF AND
*  RELATED THERETO SHALL BE SETTLED BY ARBITRATION IN SAN FRANCISCO, CA, UNDER
*  THE RULES OF THE INTERNATIONAL CHAMBER OF COMMERCE (ICC).
*
*******************************************************************************/

/* FILE NAME:  customer_system.c
 * PURPOSE:
 *  Specify customer system configuration.
 *
 * NOTES:
 *
 */

/* INCLUDE FILE DECLARTIONS
*/
#include "FreeRTOS.h"
#include "task.h"
#include "customer_system.h"
#include "hwcfg_util.h"
#include "util.h"
#include "air_chipscu.h"
#include "air_perif.h"
#include "air_gpio.h"
#include "air_error.h"
#include "air_init.h"
#include "air_port.h"
#include "air_sif.h"
#include "air_swc.h"
#include <platform.h>
#include <osal/osal_type.h>
#include <osal/osal_lib.h>

#if 1		/* wy,debug */
/* Standard includes. */
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <machine/endian.h>

/* Kernel includes. */
#include <FreeRTOS.h>
#include "FreeRTOSConfig.h"
#include "task.h"
#include "timers.h"
#include "queue.h"
#include "osal.h"

/* module includes */
#include <poe.h>
#include <os_func.h>
#include <air_error.h>
#include <air_sif.h>
#include <air_i2c.h>
#include <i2c_cmd.h>
#include <pp_def.h>
#include <air_l2.h>
#include <test.h>
#include <product.h>
#include "product_config.h"

#define DEFAULT_DIP_STATUS				(0xff)
#endif

/* MACRO FUNCTION DECLARATIONS
*/
#define SET_GPIO_OUTPUT(x, rc)    do                              \
    {                                                             \
        rc |= air_gpio_setValue(x, GPIO_PIN_HIGH);                \
        rc |= air_gpio_setOutputEnable(x, ENABLE_PIN_OE);         \
    }while(0)

/* LOCAL SUBPROGRAM SPECIFICATIONS
*/
unsigned char g_is_master = 0;
unsigned char g_slave_detected = 0;
unsigned char g_pse_watchdog_en = 0;
TaskHandle_t user_taskHandle = NULL;
TaskHandle_t pse_taskHandle = NULL;
#ifdef SKU_2150
TaskHandle_t sfp_taskHandle = NULL;
#endif
unsigned int g_dip_state = DEFAULT_DIP_STATUS;
unsigned int g_prev_dip_state = DEFAULT_DIP_STATUS;

/* EXPORTED SUBPROGRAM BODIES*/
extern int
hal_pearl_mdio_writeC22(
    const unsigned int    unit,
    const unsigned short    bus_id,
    const unsigned short    phy_addr,
    const unsigned short    reg_addr,
    const unsigned short    reg_data);

extern int
hal_pearl_mdio_readC22(
    const unsigned int    unit,
    const unsigned short    bus_id,
    const unsigned short    phy_addr,
    const unsigned short    reg_addr,
    unsigned short          *ptr_reg_data);

extern int
hal_pearl_mdio_writeC45(
    const unsigned int    unit,
    const unsigned short    bus_id,
    const unsigned short    phy_addr,
    const unsigned short    dev_type,
    const unsigned short    reg_addr,
    const unsigned short    reg_data);

extern int
hal_pearl_mdio_readC45(
    const unsigned int    unit,
    const unsigned short    bus_id,
    const unsigned short    phy_addr,
    const unsigned short    dev_type,
    const unsigned short    reg_addr,
    unsigned short          *ptr_reg_data);

#if 0
void an8801sb_force_led_on(void)
{
    unsigned int data;
    
    data = an8801sb_phy_readBuckPbus(0x19, 0x10000070);
    data |= (0x1 << 1);
    an8801sb_phy_writeBuckPbus(0x19, 0x10000070, data);

    data = an8801sb_phy_readBuckPbus(0x19, 0x1000a304);
    data &= ~(0x1 << 1);
    an8801sb_phy_writeBuckPbus(0x19, 0x1000a304, data);
}

void an8801sb_force_led_off(void)
{
    unsigned int data;
    
    data = an8801sb_phy_readBuckPbus(0x19, 0x10000070);
    data |= (0x1 << 1);
    an8801sb_phy_writeBuckPbus(0x19, 0x10000070, data);

    data = an8801sb_phy_readBuckPbus(0x19, 0x1000a304);
    data |= (0x1 << 1);
    an8801sb_phy_writeBuckPbus(0x19, 0x1000a304, data);
}

void an8801sb_led_enable(void)
{
    unsigned int data;
    
    data = an8801sb_phy_readBuckPbus(0x19, 0x10000070);
    data &= ~(0x1 << 1);
    an8801sb_phy_writeBuckPbus(0x19, 0x10000070, data);
}
#endif

#ifdef SKU_2150
extern BOOL_T sfp_poll_en;
extern void sfp_led_turnLedOff(void);
extern void sfp_led_turnLedOn(void);
#endif

AIR_ERROR_NO_T _sif_read_slave_PSE_reg(unsigned char addr, unsigned char reg, 
													unsigned char *p_data)
{
    AIR_ERROR_NO_T   rc = AIR_E_OK;
    AIR_SIF_INFO_T   sif_info;
    AIR_SIF_PARAM_T  sif_param;
    
    sif_info.channel = 0;
    sif_info.slave_id = addr;

    sif_param.addr_len = 1;
    sif_param.addr = reg;
    sif_param.data_len = 1;
    sif_param.info.data = (UI32_T) *p_data;

    rc = air_sif_read(0, &sif_info, &sif_param);
    if (AIR_E_OK == rc)
    {
        *p_data = sif_param.info.data;
    }
    return rc;
}

AIR_ERROR_NO_T _sif_write_slave_PSE_reg(unsigned char addr, unsigned char reg, 
													unsigned char data)
{
    AIR_ERROR_NO_T   rc = AIR_E_OK;
    AIR_SIF_INFO_T   sif_info;
    AIR_SIF_PARAM_T  sif_param;
    
    sif_info.channel = 0;
    sif_info.slave_id = addr;

    sif_param.addr_len = 1;
    sif_param.addr = reg;
    sif_param.data_len = 1;
    sif_param.info.data = (UI32_T)data;
    //sif_param.info.data = ((UI32_T)data) << 24;

    rc = air_sif_write(0, &sif_info, &sif_param);
    return rc;
}

AIR_ERROR_NO_T _sif_read_slave_8855M_reg(unsigned int reg, unsigned int *p_data)
{
    AIR_ERROR_NO_T   rc = AIR_E_OK;
    AIR_SIF_INFO_T   sif_info;
    AIR_SIF_PARAM_T  sif_param;
    
    sif_info.channel = 0;
    sif_info.slave_id = 0x30;

    sif_param.addr_len = 4;
    sif_param.addr = reg;
    sif_param.data_len = 4;
    sif_param.info.data = (UI32_T) *p_data;

    rc = air_sif_read(0, &sif_info, &sif_param);
    if (AIR_E_OK == rc)
    {
        *p_data = sif_param.info.data;
    }
    return rc;
}

AIR_ERROR_NO_T _sif_write_slave_8855M_reg(unsigned int reg, unsigned int data)
{
    AIR_ERROR_NO_T   rc = AIR_E_OK;
    AIR_SIF_INFO_T   sif_info;
    AIR_SIF_PARAM_T  sif_param;
    
    sif_info.channel = 0;
    sif_info.slave_id = 0x30;

    sif_param.addr_len = 4;
    sif_param.addr = reg;
    sif_param.data_len = 4;
    sif_param.info.data = data;

    rc = air_sif_write(0, &sif_info, &sif_param);
    return rc;
}

unsigned int _get_master_8855M_port_link(unsigned int port)
{
    unsigned int data = 0;

    data = io_read32(0x10210010 + port * 0x200);
    return (data & (1 << 24));
}

#ifdef SKU_2151
unsigned int _get_slave_8855M_port_link(unsigned int port)
{
    unsigned int data = 0;

    _sif_read_slave_8855M_reg(0x10210010 + port * 0x200, &data);
    return (data & (1 << 24));
}
#endif

unsigned int _get_master_8855M_port_rx_pkt(unsigned int port)
{
    unsigned int rupc = 0, rmpc = 0, rbpc = 0;

    rupc = io_read32(0x10214088 + port * 0x200); //RX Unicase pkt cnt
    rmpc = io_read32(0x1021408c + port * 0x200); //RX Multicast pkt cnt
    rbpc = io_read32(0x10214090 + port * 0x200); //RX Broadcast pkt cnt
    //printf("Master Port %u RUPC %u RMPC %u RBPC %u\n", port, rupc, rmpc, rbpc);

    return (rupc + rmpc + rbpc);
}

#ifdef SKU_2151
unsigned int _get_slave_8855M_port_rx_pkt(unsigned int port)
{
    unsigned int rupc = 0, rmpc = 0, rbpc = 0;

    _sif_read_slave_8855M_reg(0x10214088 + port * 0x200, &rupc); //RX Unicase pkt cnt
    _sif_read_slave_8855M_reg(0x1021408c + port * 0x200, &rmpc); //RX Multicast pkt cnt
    _sif_read_slave_8855M_reg(0x10214090 + port * 0x200, &rbpc); //RX Broadcast pkt cnt
    //printf("Slave Port %u RUPC %u RMPC %u RBPC %u\n", port, rupc, rmpc, rbpc);

    return (rupc + rmpc + rbpc);
}
#endif

static unsigned int _get_dip_switch_state(void)
{
    unsigned int data, ret;
    
#if defined(SKU_2151) || defined(SKU_2150)
#if 0	/* wy,to check gpio1 value for testing on PCB2151 */
	unsigned int xx;
	/* gpio1 work as gpio mode */
    data = io_read32(0x1000007c);
    data |= (1 << 1);
    io_write32(0x1000007c, data);

    //set gpio1 input mode
    data = io_read32(0x1000a300);
    data &= ~(0x3 << (1*2));
    io_write32(0x1000a300, data);
    
    //get gpio1 input value
    xx = io_read32(0x1000a304);
#endif

    if (g_is_master)
    {
	#if defined(UART_ENABLE) && (UART_ENABLE)
		//testing dip by writing reg 0x1000501c
        ret = io_read32(0x1000501c);

	#else
		data = io_read32(0x1000a304);
		ret = ((data & (1 << 1)) >> 1) | ((data & (1 << 19)) >> 18);        
	#endif
    }
    else
    {
        ret = io_read32(0x1000501c);
    }
#endif

#if 0	/* wy,delete it */
#ifdef SKU_2150
    //data = io_read32(0x1000a304);
    //ret = ((data & (1 << 1)) >> 1) | ((data & (1 << 19)) >> 18);

    //testing dip by writing reg 0x1000501c
    ret = io_read32(0x1000501c);
#endif
#endif

#ifdef SKU_2149
	if(g_is_master)
	{
		data = io_read32(0x1000a304);
		ret = ((data & (1 << 19)) >> 19) | ((data & (1 << 20)) >> 19);
	}
	else
	{
		ret = io_read32(0x1000501c);
	}
    //testing dip by writing reg 0x1000501c
    //ret = io_read32(0x1000501c);
#endif
    
    return ret;
}

static void _enable_PSE_watchdog(void)
{
    printf("_enable_PSE_watchdog!\n");
    g_pse_watchdog_en = 1;
}

static void _disable_PSE_watchdog(void)
{
    printf("_disable_PSE_watchdog!\n");
    g_pse_watchdog_en = 0;
}

static void _enable_10M_longreach(void)
{
    int phyaddr, port;
    
    printf("_enable_10M_longreach!\n");
#ifdef SKU_2151
    if (g_is_master)
    {
        for (port = 1; port < 6; port++)
        {
            air_port_setPhyOpMode(0, port, AIR_PORT_OP_MODE_LONG_REACH);
        }
        for (phyaddr = 0; phyaddr < 5; phyaddr++)
        {
            cl22_write(phyaddr, 0, 4, 0xc61);
            cl22_write(phyaddr, 0, 9, 0x0);
            cl22_write(phyaddr, 0, 0, 0x1200);
        }
    }
    else
    {
        for (port = 1; port < 4; port++)
        {
            air_port_setPhyOpMode(0, port, AIR_PORT_OP_MODE_LONG_REACH);
        }
        for (phyaddr = 0; phyaddr < 3; phyaddr++)
        {
            cl22_write(phyaddr, 0, 4, 0xc61);
            cl22_write(phyaddr, 0, 9, 0x0);
            cl22_write(phyaddr, 0, 0, 0x1200);
        }
    }
#else
    for (port = 1; port <= 4; port++)
    {
        air_port_setPhyOpMode(0, port, AIR_PORT_OP_MODE_LONG_REACH);
    }
    for (phyaddr = 0; phyaddr <= 3; phyaddr++)
    {
        cl22_write(phyaddr, 0, 4, 0xc61);
        cl22_write(phyaddr, 0, 9, 0x0);
        cl22_write(phyaddr, 0, 0, 0x1200);
    }
#endif
}

static void _disable_10M_longreach(void)
{
    int phyaddr, port;
    
    printf("_disable_10M_longreach!\n");
#ifdef SKU_2151
    if (g_is_master)
    {
        for (port = 1; port < 6; port++)
        {
            air_port_setPhyOpMode(0, port, AIR_PORT_OP_MODE_NORMAL);
        }
        for (phyaddr = 0; phyaddr < 5; phyaddr++)
        {
            cl22_write(phyaddr, 0, 4, 0xde1);
            cl22_write(phyaddr, 0, 9, 0x200);
            cl22_write(phyaddr, 0, 0, 0x1200);
        }
    }
    else
    {
        for (port = 1; port < 4; port++)
        {
            air_port_setPhyOpMode(0, port, AIR_PORT_OP_MODE_NORMAL);
        }
        for (phyaddr = 0; phyaddr < 3; phyaddr++)
        {
            cl22_write(phyaddr, 0, 4, 0xde1);
            cl22_write(phyaddr, 0, 9, 0x200);
            cl22_write(phyaddr, 0, 0, 0x1200);
        }
    }
#else
    for (port = 1; port <= 4; port++)
    {
        air_port_setPhyOpMode(0, port, AIR_PORT_OP_MODE_NORMAL);
    }
    for (phyaddr = 0; phyaddr <= 3; phyaddr++)
    {
        cl22_write(phyaddr, 0, 4, 0xde1);
        cl22_write(phyaddr, 0, 9, 0x200);
        cl22_write(phyaddr, 0, 0, 0x1200);
    }
#endif
}

static void _switch_to_normal_mode()
{
    printf("Switching to normal mode!\n");
    if (DIP_STATE_10M == g_prev_dip_state)
    {
        _disable_10M_longreach();
    }
#ifdef SKU_2151
    if (g_is_master)
#endif
    {
        _disable_PSE_watchdog();
    }
}

static void _switch_to_PSE_watchdog_mode()
{
    printf("Switching to PSE watchdog mode!\n");
    if (DIP_STATE_10M == g_prev_dip_state)
    {
        _disable_10M_longreach();
    }
#ifdef SKU_2151
    if (g_is_master)
#endif
    {
        _enable_PSE_watchdog();
    }
}

static void _switch_to_10M_longreach_mode()
{
    printf("Switching to 10M long reach mode!\n");
#ifdef SKU_2151
    if (g_is_master)
#endif
    {
        _disable_PSE_watchdog();
    }
    _enable_10M_longreach();
}

static void _process_dip_led_indication(int dip_state)
{
    int blink_time, i, phy_addr;

#if defined(SKU_2151) || defined(SKU_2150)

#else
    if (DEFAULT_DIP_STATUS == g_prev_dip_state)
        return;
#endif

    if (DIP_STATE_10M == dip_state)
        blink_time = 3;
    else if (DIP_STATE_PSE_WATCHDOG == dip_state)
        blink_time = 2;
    else
        blink_time = 1;

#if defined(LED_OP_IN_CRITICAL) && LED_OP_IN_CRITICAL
	os_enter_critical();
#endif

    for (i = 0; i < blink_time; i++)
    {
        //force led on
        hal_pearl_mdio_writeC45(0, 0, 0x0, 0x1F, 0x24, 0x8040);
        hal_pearl_mdio_writeC45(0, 0, 0x0, 0x1F, 0x25, 0x0);
#ifdef SKU_2149
        hal_pearl_mdio_writeC45(0, 0, 0x19, 0x1F, 0x24, 0x8040);
        hal_pearl_mdio_writeC45(0, 0, 0x19, 0x1F, 0x25, 0x0);
#endif
#ifdef SKU_2150
        sfp_poll_en = FALSE;
        sfp_led_turnLedOn();
#endif

#if defined(LED_OP_IN_CRITICAL) && LED_OP_IN_CRITICAL
		delay1ms(1000);
#else
        vTaskDelay(pdMS_TO_TICKS(1000));   //in unit of ms
#endif

        //force led off
        hal_pearl_mdio_writeC45(0, 0, 0x0, 0x1F, 0x24, 0x8000);
        hal_pearl_mdio_writeC45(0, 0, 0x0, 0x1F, 0x25, 0x0);
#ifdef SKU_2149
        hal_pearl_mdio_writeC45(0, 0, 0x19, 0x1F, 0x24, 0x8000);
        hal_pearl_mdio_writeC45(0, 0, 0x19, 0x1F, 0x25, 0x0);
#endif
#ifdef SKU_2150
        sfp_led_turnLedOff();
#endif

#if defined(LED_OP_IN_CRITICAL) && LED_OP_IN_CRITICAL
		delay1ms(1000);
#else
        vTaskDelay(pdMS_TO_TICKS(1000));   //in unit of ms
#endif
    }

#if defined(LED_OP_IN_CRITICAL) && LED_OP_IN_CRITICAL
	os_exit_critical();
#endif

    //led recover
    hal_pearl_mdio_writeC45(0, 0, 0x0, 0x1F, 0x24, 0x8007);
    hal_pearl_mdio_writeC45(0, 0, 0x0, 0x1F, 0x25, 0x3f);
#ifdef SKU_2149
    hal_pearl_mdio_writeC45(0, 0, 0x19, 0x1F, 0x24, 0x8007);
    hal_pearl_mdio_writeC45(0, 0, 0x19, 0x1F, 0x25, 0x3f);
#endif
#ifdef SKU_2150
    sfp_poll_en = TRUE;
#endif
}

static void _process_dip_switch_state(int dip_state)
{
    _process_dip_led_indication(dip_state);
    switch(dip_state)
    {
        case DIP_STATE_NORMAL:
            _switch_to_normal_mode();
            break;
        case DIP_STATE_PSE_WATCHDOG:
            _switch_to_PSE_watchdog_mode();
            break;
        case DIP_STATE_10M:
            _switch_to_10M_longreach_mode();
            break;
        default:
            break;
    };
}

#ifdef SKU_2151
static void _process_reg_io_from_slave(void)
{
    unsigned int reg, data;
    
    if (!g_slave_detected)
    {
        return;
    }

    _sif_read_slave_8855M_reg(0x10005010, &reg);
    _sif_read_slave_8855M_reg(0x10005014, &data);
    
    if (reg != 0)
    {
        if (data != 0)
        {
            io_write32(reg, data);
        }
        else
        {
            data = io_read32(reg);
            _sif_write_slave_8855M_reg(0x10005014, data);
            
        }
        _sif_write_slave_8855M_reg(0x10005010, 0);
    }
    
}

unsigned int g_master_link_pre = 0;
unsigned int g_master_link_cur = 0;
unsigned int g_slave_link_pre = 0;
unsigned int g_slave_link_cur = 0;
static void _cascade_process_mac_table(void)
{
    int port;
    BOOL_T flush_master = FALSE, flush_slave = FALSE;

    //update master port status
    for (port = 0; port < 5; port++)
    {
        if (_get_master_8855M_port_link(port))
        {
            g_master_link_cur |= (1 << port);
        }
        else
        {
            g_master_link_cur &= ~(1 << port);
            if (g_master_link_pre & (1 << port))
            {
                
flush_slave = TRUE;
            }
        }
    }
    
    //update slave port status
    for (port = 0; port < 5; port++)
    {
        if (_get_slave_8855M_port_link(port))
        {
            g_slave_link_cur |= (1 << port);
        }
        else
        {
            g_slave_link_cur &= ~(1 << port);
            if (g_slave_link_pre & (1 << port))
            {
                
flush_master = TRUE;
            }
        }
    }

    if (flush_master)
    {
        io_write32(0x10200328, 0x20);
        io_write32(0x10200300, 0x80000602);
    }

    if (flush_slave)
    {
        _sif_write_slave_8855M_reg(0x10200328, 0x20);
        _sif_write_slave_8855M_reg(0x10200300, 0x80000602);
    }

    g_master_link_pre = g_master_link_cur;
    g_slave_link_pre = g_slave_link_cur;
}
#endif

void _an_collision_workaround(void)
{
    unsigned int data = 0, pidx = 0, value = 0, link_status = 0;
    unsigned short phy_data = 0;
    
    for (pidx = 0; pidx < 5; pidx++)
    {
        link_status = io_read32(0x10210010 + pidx * 0x200);
        if (link_status & (1<< 24))
        {
            continue;
        }
        
        data = cl22_read(pidx, 0, 0x0);
        if(data & (0x1 << 11))
        {
            cl22_write(pidx, 0, 0x0, 0x1200);
            continue;
        }

        data = cl22_read(pidx, 0, 0xA);
        if (data & (1 << 15))
        {
            value = get_random(pidx) % 200;
            cl22_write(pidx, 0, 0x0, 0x1840);
            vTaskDelay(pdMS_TO_TICKS(value));   //in unit of ms
            cl22_write(pidx, 0, 0x0, 0x1200);
        }
    }
    
    #ifdef SKU_2149
    link_status = io_read32(0x10210010 + 5 * 0x200);
    if (!(link_status & (1<< 24)))
    {
        hal_pearl_mdio_readC22(0, 0, 0x19, 0, &phy_data);
        if(phy_data & (0x1 << 11))
        {
            hal_pearl_mdio_writeC22(0, 0, 0x19, 0, 0x1200);
        }
        else
        {
            hal_pearl_mdio_readC22(0, 0, 0x19, 0xA, &phy_data);
            if(phy_data & (0x1 << 15))
            {
                value = rand() % 200;
                hal_pearl_mdio_writeC22(0, 0, 0x19, 0, 0x1840);
                vTaskDelay(pdMS_TO_TICKS(value));   //in unit of ms
                hal_pearl_mdio_writeC22(0, 0, 0x19, 0, 0x1200);
            }
        }
    }
    #endif
}

#ifdef SKU_2149
void an8801sb_serdes_link_change_monitor(void)
{
    unsigned short phy_data = 0, an_lp = 0;
    unsigned int data = 0, data1 = 0, data2 = 0;
    
    hal_pearl_mdio_readC22(0, 0, 0x19, 1, &phy_data); //fake read??
    hal_pearl_mdio_readC22(0, 0, 0x19, 1, &phy_data);
    data = io_read32(0x10220a00);
    if ((phy_data & 0x4) && (data & 0x2)) //gphy link & serdes force sync off
    {
        hal_pearl_mdio_readC22(0, 0, 0x19, 5, &an_lp);
        data1 = io_read32(0x10224514);
        data2 = data1;
        if (an_lp & 0x0400)
        {
            data1 |= (0x6);    //fc tx
        }
        else if (an_lp & 0x0800)
        {
            data1 |= (0x2);    //fc rx
        }
        else
        {
            data1 &= ~(0x6);   //fc off
        }

        if (data2 != data1)
        {
            io_write32(0x10224018, 2);
            io_write32(0x10224514, data1);
            io_write32(0x10224018, 0);
        }
        
        //release force sync off
        data &= ~0x6;
        io_write32(0x10220a00, data);
        io_write32(0x10220000, 0x9140);
    }
    if ((phy_data & 0x4) && !(data & 0x2)) //gphy link & serdes not force sync off
    {
        data1 = io_read32(0x10210a10);
        if (!(data1 & (1 << 24)))
        {
            io_write32(0x10220000, 0x9140);
        }
    }
    else if (!(phy_data & 0x4) && !(data & 0x2))//gphy link down  & serdes not force syncd off
    {
        //force serdes down & set sideband
        io_write32(0x10224018, 2);
        io_write32(0x10224514, 0x01010101);
        io_write32(0x10224018, 0);

        //set force sync off
        data |= 0x2;
        io_write32(0x10220a00, data);
    }
}
#endif

#ifdef SKU_2151
#if defined(SLAVE_TEST_GPIO) && (SLAVE_TEST_GPIO)	/* make a GPIO testing to test GPIO[8:6]  */
void
slave_test_gpio_init(void)
{
	int i, idx, en;
	int tgidx[] = SLAVE_TEST_GPIO_ARRAY;

	en = 1;
	for(i = 0; i < NUMOFARRAY(tgidx); i++)
	{
		idx = tgidx[i];

		gpio_pin_set_func_gpio(idx, en);
		gpio_pin_set_direction(idx, GPIO_PIN_DIRECTION_INPUT);
	}
}

void
slave_test_gpio_getting(void)
{
	int i, idx, slen;
	int tgidx[] = SLAVE_TEST_GPIO_ARRAY;
	char tbuf[128];
	uint32_t val;
	uint32_t now_tick = time_current_tick();
	static uint32_t next_tick = 0;

	if(tick_after_eq(now_tick, next_tick))
	{
		slen = 0;
		slen += snprintf(tbuf + slen, sizeof(tbuf) - slen, "GPIO Index:");
		for(i = 0; i < NUMOFARRAY(tgidx); i++)
		{
			idx = tgidx[i];
			slen += snprintf(tbuf + slen, sizeof(tbuf) - slen, "\t%d", idx);
		}
		slen += snprintf(tbuf + slen, sizeof(tbuf) - slen, "\n");

		slen += snprintf(tbuf + slen, sizeof(tbuf) - slen, "GPIO Value:");
		for(i = 0; i < NUMOFARRAY(tgidx); i++)
		{
			idx = tgidx[i];

			gpio_pin_getting(idx, &val);
			//slen += snprintf(tbuf + slen, sizeof(tbuf) - slen, "GPIO[%02d]:%d\n", idx, val);
			slen += snprintf(tbuf + slen, sizeof(tbuf) - slen, "\t%d", val);
		}
		slen += snprintf(tbuf + slen, sizeof(tbuf) - slen, "\n");

		tbuf[sizeof(tbuf) - 1] = '\0';
		DBG_PRINT("%s\n", tbuf);

		/* update the next tick */
		next_tick = time_current_tick() + pdMS_TO_TICKS(SLAVE_TEST_GPIO_INTERVAL_MS);
	}
}
#endif
#endif

static void _master_chip_loop(void)
{
    unsigned int dip_state, data = 0;
	static uint8_t cnt = 0;
    
    while(1)
    {
        vTaskDelay(pdMS_TO_TICKS(1000));   //in unit of ms
        //printf("_master_chip_loop!\n");

        show_all_task_stack_info();

        if (!g_slave_detected)
        {
            _sif_read_slave_8855M_reg(0x10005000, &data);
            if (data == 0x8855)
            {
                printf("Slave 8855M detected through I2C!\n");
                g_slave_detected = 1;
            }
			else
			{
				if(!(cnt % 60))
				{
					printf("Slave 8855M has not detected through I2C!,times %d\n", cnt);
				}
				cnt++;
                //g_slave_detected = 1;
			}
        }

		dip_state = _get_dip_switch_state();

#if 0
		DBG_PRINT("dip_state %d,g_dip_state:%d,g_prev_dip_state:%d\n", 
			dip_state, g_dip_state, g_prev_dip_state);
#endif

		if (dip_state != g_dip_state)
		{
			printf("DIP switch state changed from %d to %d\n", g_dip_state, dip_state);
			g_prev_dip_state = g_dip_state;
			g_dip_state = dip_state;

			_sif_write_slave_8855M_reg(0x1000501c, dip_state); //write dip mode to slave AN8855M

			_process_dip_switch_state(dip_state);
		}
        _an_collision_workaround();

#ifdef SKU_2149
        an8801sb_serdes_link_change_monitor();
#endif

#ifdef SKU_2151
        _cascade_process_mac_table();
        //for debug
        //_process_reg_io_from_slave();
#endif
    }
}

static void _slave_chip_loop(void)
{
    unsigned int dip_state, data = 0;

#ifdef SKU_2151
#if defined(SLAVE_TEST_GPIO) && (SLAVE_TEST_GPIO)
	DBG_PRINT("Show register before slave gpio testing\n");
	reg_show(0x10000000, 0x100);

	slave_test_gpio_init();

	DBG_PRINT("Show register after slave gpio testing\n");
	reg_show(0x10000000, 0x100);
#endif
#endif
	
    while(1)
    {
        vTaskDelay(pdMS_TO_TICKS(50));   //in unit of ms

        dip_state = _get_dip_switch_state();
        if (dip_state != g_dip_state)
        {
            printf("DIP switch state changed from %d to %d\n", g_dip_state, dip_state);
            g_prev_dip_state = g_dip_state;
            g_dip_state = dip_state;
            _process_dip_switch_state(dip_state);
        }
        
        _an_collision_workaround();

#ifdef SKU_2151
#if defined(SLAVE_TEST_GPIO) && (SLAVE_TEST_GPIO)
		slave_test_gpio_getting();
#endif
#endif
    }
}

static void _pse_runAppTask(void)
{
	static uint8_t pre_pse_wd_en = 0;

	vTaskDelay(pdMS_TO_TICKS(2000));

	module_product_init();
	check_pse_reseted(0);
	poe_chip_init_all(&POE_CHIP[0], POE_CHIP_TOTAL, 
		POE_CHIP_MODE, POE_PWR_MGMT_IN_HW);

	POE_WatchDog_Init();
	poe_priority_dynaimc_pwr_limitation_init();

	test_sif_task_init();

    while(1)
    {
        vTaskDelay(pdMS_TO_TICKS(1000));   //in unit of ms

		check_pse_reseted(1);

		if(pre_pse_wd_en != g_pse_watchdog_en)
		{
			if(g_pse_watchdog_en)
			{/* do PSE watchdog initialization when PSE WD from disable to enable */
				POE_WatchDog_Init();
			}
			pre_pse_wd_en = g_pse_watchdog_en;
		}
		
        if(g_pse_watchdog_en)
        {
            //TODO(Netcore)
            POE_WatchDog_Mode();
        }

#if 1
		poe_overload_thread();
		poe_power_on_thread();
#else
		dynamic_poe_pwr_limitation_demo(NULL);
#endif

#if 0	/* wy,debug */
	test_code();
#endif
    }
}

static void _user_runAppTask(void)
{
    if (g_is_master)
    {
        _master_chip_loop();
    }
    else
    {
        _slave_chip_loop();
    }
}

#ifdef SKU_2150
AIR_PORT_SERDES_MODE_T g_cur_mode = AIR_PORT_SERDES_MODE_1000BASE_X;
static void _sfp_runAppTask(void)
{
    unsigned int cnt = 0, data = 0, data1 = 0, data2 = 0;
    AIR_PORT_STATUS_T status = {0};
    AIR_PORT_SERDES_MODE_T mode;

    while(1)
    {
        vTaskDelay(pdMS_TO_TICKS(500+rand()%2500));   //in unit of ms

        air_port_getPortStatus(0, 6, &status);
        if (!(status.flags & AIR_PORT_STATUS_FLAGS_LINK_UP))
        {
            data = io_read32(0x10210a00);
            if (data & (1 << 24))
            {
                data &= ~(1 << 24);
                io_write32(0x10210a00, data);
            }
            
            mode = (cnt % 2) ? AIR_PORT_SERDES_MODE_HSGMII : AIR_PORT_SERDES_MODE_1000BASE_X;
            air_port_setSerdesMode(0, 6, mode);
            g_cur_mode = mode;
        }
        else if (AIR_PORT_SERDES_MODE_1000BASE_X == g_cur_mode)
        {
            data = io_read32(0x10210a00);
            if (data != 0xa3159030)
            {
                io_write32(0x10210a00, 0xa2159030);
                vTaskDelay(pdMS_TO_TICKS(5));   //in unit of ms
                io_write32(0x10210a00, 0xa3159030);
            }
        }
        else if (AIR_PORT_SERDES_MODE_HSGMII == g_cur_mode)
        {
            data = io_read32(0x10210a00);
            if (data != 0xb3159030)
            {
                io_write32(0x10210a00, 0xb2159030);
                vTaskDelay(pdMS_TO_TICKS(5));   //in unit of ms
                io_write32(0x10210a00, 0xb3159030);
            }
        }
        cnt++;
    }
}

#endif

void customer_createUserTask(void)
{
    xTaskCreate(_user_runAppTask,           /* The function that implements the task. */
                "USER",                     /* The text name assigned to the task - for debug only as it is not used by the kernel. */
                SIZE_OF_STACK_TASK_USER,    /* The size of the stack to allocate to the task. */
                NULL,                       /* The parameter passed to the task - not used in this simple case. */
                2,                          /* The priority assigned to the task. */
                &user_taskHandle);
    return;
}

void customer_createPSETask(void)
{
    xTaskCreate(_pse_runAppTask,           /* The function that implements the task. */
                "PSE",                     /* The text name assigned to the task - for debug only as it is not used by the kernel. */
                SIZE_OF_STACK_TASK_PSE,    /* The size of the stack to allocate to the task. */
                NULL,                      /* The parameter passed to the task - not used in this simple case. */
                2,                         /* The priority assigned to the task. */
                &pse_taskHandle);
    return;
}

#ifdef SKU_2150
void customer_createSFPTask(void)
{
    xTaskCreate(_sfp_runAppTask,           /* The function that implements the task. */
                "SFP",                     /* The text name assigned to the task - for debug only as it is not used by the kernel. */
                SIZE_OF_STACK_TASK_SFP,   /* The size of the stack to allocate to the task. */
                NULL,                      /* The parameter passed to the task - not used in this simple case. */
                2,                         /* The priority assigned to the task. */
                &sfp_taskHandle);
    return;
}
#endif

int customer_system_init(void)
{
    UI32_T  data = 0;
    unsigned int dip_state, blink_time = 0, i;

    //show chip hwtrap setting
    data = io_read32(0x10000094);
    printf("Chip hwtrap setting: 0x%x\n", data);

    //Identify Master/Slave 8855M by GPIO13, master GPIO13 low, slave GPIO13 high
    //enable gpio13 gpio function
    data = io_read32(0x1000007c);
    data |= (1 << 13);
    io_write32(0x1000007c, data);

    //set gpio13 input mode
    data = io_read32(0x1000a300);
    data &= ~(0x3 << (13*2));
    io_write32(0x1000a300, data);
    
    //get gpio13 input value
    data = io_read32(0x1000a304);
    if (data & (1 << 13))
    {
        printf("Current chip is slave!\n");
        g_is_master = 0;
    }
    else
    {
        printf("Current chip is master!\n");
        g_is_master = 1;
    }

#if 1		/* wy,debug dip default mode */
	if(!g_is_master)
	{
		io_write32(0x1000501c, DEFAULT_DIP_STATUS);
	}
#endif

#ifdef SKU_2151
	if(g_is_master)
	{
		//turn on sysled
		//enable gpio6 gpio function
		data = io_read32(0x1000007c);
		data |= (1 << 6);
		io_write32(0x1000007c, data);

		//set gpio6 output mode
		data = io_read32(0x1000a300);
		data &= ~(0x3 << (6*2));
		data |= (0x1 << (6*2));
		io_write32(0x1000a300, data);

		//set gpio6 output enable
		data = io_read32(0x1000a314);
		data |= (0x1 << 6);
		io_write32(0x1000a314, data);

		//set gpio6 low
		data = io_read32(0x1000a304);
		data &= ~(0x1 << 6);
		io_write32(0x1000a304, data);
	}
#endif

//#ifdef SKU_2149
#if 0
    //reset 8801
    //enable gpio6 gpio function
    data = io_read32(0x1000007c);
    data |= (1 << 6);
    io_write32(0x1000007c, data);
    
    //set gpio6 output mode
    data = io_read32(0x1000a300);
    data &= ~(0x3 << (6*2));
    data |= (0x1 << (6*2));
    io_write32(0x1000a300, data);
    
    //set gpio6 output enable
    data = io_read32(0x1000a314);
    data |= (0x1 << 6);
    io_write32(0x1000a314, data);
    
    //set gpio6 low
    data = io_read32(0x1000a304);
    data &= ~(0x1 << 6);
    io_write32(0x1000a304, data);
    
    delay1ms(50);
    
    //set gpio6 high
    data = io_read32(0x1000a304);
    data |= (0x1 << 6);
    io_write32(0x1000a304, data);

    //reset SLED
    //enable gpio5 gpio function
    data = io_read32(0x1000007c);
    data |= (1 << 5);
    io_write32(0x1000007c, data);

    //set gpio5 output mode
    data = io_read32(0x1000a300);
    data &= ~(0x3 << (5*2));
    data |= (0x1 << (5*2));
    io_write32(0x1000a300, data);

    //set gpio5 output enable
    data = io_read32(0x1000a314);
    data |= (0x1 << 5);
    io_write32(0x1000a314, data);

    //set gpio5 low
    data = io_read32(0x1000a304);
    data &= ~(0x1 << 5);
    io_write32(0x1000a304, data);
#endif

    printf("Setting up DIP switch...\n");
#if defined(SKU_2151) || defined(SKU_2150)
    //enable gpio1/19 gpio function, normal mode, 10M mode, poe watchdog mode
    data = io_read32(0x1000007c);
#if defined(UART_ENABLE) && (UART_ENABLE)
	data |= (1 << 19);	//gpio 1 is uart, skip gpio1 when debugging with uart
	data &= ~(1 << 1);
#elif defined(REMOTE_UART_ENABLE) && (REMOTE_UART_ENABLE)
	if(g_is_master)
	{/* GPIO1 & GPIO19 work as gpio for customer switch at first AN8855M */
		data |= ((1 << 1) | (1 << 19));
	}
	else
	{
		data |= (1 << 19);	//gpio 1 is uart, skip gpio1 when debugging with uart
		data &= ~(1 << 1);
	}
#else
	data |= ((1 << 1) | (1 << 19));
#endif
    io_write32(0x1000007c, data);

#if defined(UART_ENABLE) && (UART_ENABLE)

#elif defined(REMOTE_UART_ENABLE) && (REMOTE_UART_ENABLE)
	if(g_is_master)
	{
	    //enable gpio1 input mode, gpio 1 is uart, skip gpio1 when debugging with uart
	    data = io_read32(0x1000a300);
	    data &= ~(0x3 << (1*2));
	    io_write32(0x1000a300, data);

		//disable gpio1 output enable
	    data = io_read32(0x1000a314);
	    data &= ~(1 << 1);
	    io_write32(0x1000a314, data);
	}
#else
    //enable gpio1 input mode, gpio 1 is uart, skip gpio1 when debugging with uart
    data = io_read32(0x1000a300);
    data &= ~(0x3 << (1*2));
    io_write32(0x1000a300, data);

	//disable gpio1 output enable
    data = io_read32(0x1000a314);
    data &= ~(1 << 1);
    io_write32(0x1000a314, data);
#endif

    //enable gpio19 input mode
    data = io_read32(0x1000a320);
    data &= ~(0x3 << ((19 - 16)*2));
    io_write32(0x1000a320, data);

    dip_state = _get_dip_switch_state();
    printf("DIP switch state %d\n", dip_state);

    if (dip_state == DIP_STATE_NORMAL)
    {
        blink_time = 1;
    }
    else if (dip_state == DIP_STATE_PSE_WATCHDOG)
    {
        blink_time = 2;
    }
    else
    {
        blink_time = 3;
    }

    //use gpio for led open show, turn on for 1sec then turn off for 1sec
    data = io_read32(0x1000007c);
#if defined(SKU_2151)
    data |= ((1 << 0) | (1 << 2) | (1 << 3) | (1 << 4) | (1 << 5));
#else
    data |= ((1 << 0) | (1 << 2) | (1 << 3) | (1 << 4) | (1 << 5) | (1 << 6));
#endif
    io_write32(0x1000007c, data);

    data = io_read32(0x1000a300);
#if defined(SKU_2151)
    data &= ~((0x3 << (0*2)) | (0x3 << (2*2)) | (0x3 << (3*2)) | (0x3 << (4*2)) | (0x3 << (5*2)));
    data |= ((0x1 << (0*2)) | (0x1 << (2*2)) | (0x1 << (3*2)) | (0x1 << (4*2)) | (0x1 << (5*2)));
#else
    data &= ~((0x3 << (0*2)) | (0x3 << (2*2)) | (0x3 << (3*2)) | (0x3 << (4*2)) | (0x3 << (5*2)) | (0x3 << (6*2)));
    data |= ((0x1 << (0*2)) | (0x1 << (2*2)) | (0x1 << (3*2)) | (0x1 << (4*2)) | (0x1 << (5*2)) | (0x1 << (6*2)));
#endif
    io_write32(0x1000a300, data);

    data = io_read32(0x1000a314);
#if defined(SKU_2151)
    data |= ((0x1 << (0)) | (0x1 << (2)) | (0x1 << (3)) | (0x1 << (4)) | (0x1 << (5)));
#else
    data |= ((0x1 << (0)) | (0x1 << (2)) | (0x1 << (3)) | (0x1 << (4)) | (0x1 << (5)) | (0x1 << (6)));
#endif
    io_write32(0x1000a314, data);

#if 0	/* wy,disable it for first led action */
    for (i = 0; i < blink_time; i++)
    {
        data = io_read32(0x1000a304);
#if defined(PCB2151_LED_POLARITY_SWITCH) && PCB2151_LED_POLARITY_SWITCH
        data |= ((0x1 << (2)) | (0x1 << (3)) | (0x1 << (5)));
#if defined(SKU_2151)
        data &= ~((0x1 << (0)) | (0x1 << (4)));
#else
        data &= ~((0x1 << (0)) | (0x1 << (4)) | (0x1 << (6)));
#endif
#else
        data |= ((0x1 << (0)) | (0x1 << (2)) | (0x1 << (3)) | (0x1 << (5)));
#if defined(SKU_2151)
        data &= ~(0x1 << (4));
#else
        data &= ~((0x1 << (4)) | (0x1 << (6)));
#endif
#endif
        io_write32(0x1000a304, data);

        delay1ms(1000);

        data = io_read32(0x1000a304);
#if defined(PCB2151_LED_POLARITY_SWITCH) && PCB2151_LED_POLARITY_SWITCH
        data &= ~((0x1 << (2)) | (0x1 << (3)) | (0x1 << (5)));
#if defined(SKU_2151)
        data |= ((0x1 << (0)) | (0x1 << (4)));
#else
        data |= ((0x1 << (0)) | (0x1 << (4)) | (0x1 << (6)));
#endif
#else
        data &= ~((0x1 << (0)) | (0x1 << (2)) | (0x1 << (3)) | (0x1 << (5)));
#if defined(SKU_2151)
        data |= (0x1 << (4));
#else
        data |= ((0x1 << (4)) | (0x1 << (6)));
#endif
#endif
        io_write32(0x1000a304, data);
        
        delay1ms(1000);
    }
#endif

    data = io_read32(0x1000007c);
    data &= ~((1 << 0) | (1 << 2) | (1 << 3) | (1 << 4) | (1 << 5));
    io_write32(0x1000007c, data);

    //set up gpio for port led
    io_write32(0x10000058, 0x0c100000);
    io_write32(0x1000005c, 0x00000408);
    
    data = io_read32(0x10000010);
#if defined(PCB2151_LED_POLARITY_SWITCH) && PCB2151_LED_POLARITY_SWITCH
	data |= ((1 << 2) | (1 << 3) | (1 << 5));
#else
    data |= ((1 << 0) | (1 << 2) | (1 << 3) | (1 << 5));
#endif
    io_write32(0x10000010, data);
    
    data = io_read32(0x10000054);
#if defined(UART_ENABLE) && (UART_ENABLE)
    data |= ((1 << 2) | (1 << 3) | (1 << 4) | (1 << 5));    //gpio 0 is uart, skip gpio0 when debugging with uart
#elif defined(REMOTE_UART_ENABLE) && (REMOTE_UART_ENABLE)
	if(g_is_master)
	{
		data |= ((1 << 0) | (1 << 2) | (1 << 3) | (1 << 4) | (1 << 5));
	}
	else
	{
	#if defined(SKU_2151)
		/* On board 2151,slave should make gpio 0 for port led */
		data |= ((1 << 0) | (1 << 2) | (1 << 3) | (1 << 4) | (1 << 5));
	#else
		data |= ((1 << 2) | (1 << 3) | (1 << 4) | (1 << 5));	//gpio 0 is uart, skip gpio0 when debugging with uart
	#endif
	}
#else
    data |= ((1 << 0) | (1 << 2) | (1 << 3) | (1 << 4) | (1 << 5));
#endif
    io_write32(0x10000054, data);
#endif	/* end of #if defined(SKU_2151) || defined(SKU_2150) */

    return E_OK;
}

int customer_system_post_init(void)
{
    UI32_T  data = 0, port;
    unsigned int dip_state, blink_time = 0, i;

    air_swc_setJumboSize(0, AIR_SWC_JUMBO_SIZE_9216);

    if (!g_is_master)
    {
        printf("Setting up I2C slave...\n");
        
        //disable gpio19/20 gpio function
        data = io_read32(0x1000007c);
        data &= ~((1 << 19) | (1 << 20));
        io_write32(0x1000007c, data);

        //enable i2c slave on gpio19/20
        data = io_read32(0x1000006c);
        data |= 0x1;
        io_write32(0x1000006c, data);
    }
    else
    {
        printf("Setting up I2C master...\n");

        //disable gpio7/8 gpio function
        data = io_read32(0x1000007c);
        data &= ~((1 << 7) | (1 << 8));
        io_write32(0x1000007c, data);

#if defined(SKU_2151) || defined(SKU_2150)
        //enable i2c master on gpio7/8
        data = io_read32(0x1000006c);
        data |= (0x1 << 2);
        io_write32(0x1000006c, data);
#endif

#ifdef SKU_2149
	#if defined(UART_ENABLE) && (UART_ENABLE)

	#else
        //enable i2c master on gpio0/1, gpio 1 is uart, skip gpio1 when debugging with uart
		data = io_read32(0x1000006c);
		data |= (0x1 << 3);
		io_write32(0x1000006c, data);

		//disable uart accroding to YaoMin
		data = io_read32(0x10000074);
		data = 0;
		io_write32(0x10000074, data);
	#endif
#endif

        printf("Setting up DIP switch...\n");
#if defined(SKU_2151) || defined(SKU_2150)
        //enable gpio1/19 gpio function, normal mode, 10M mode, poe watchdog mode
        data = io_read32(0x1000007c);
	#if defined(UART_ENABLE) && (UART_ENABLE)
        data |= (1 << 19);   //gpio 1 is uart, skip gpio1 when debugging with uart
	#else
        data |= ((1 << 1) | (1 << 19));
	#endif
        io_write32(0x1000007c, data);

	#if defined(UART_ENABLE) && (UART_ENABLE)

	#else
        //enable gpio1 input mode, gpio 1 is uart, skip gpio1 when debugging with uart
        data = io_read32(0x1000a300);
        data &= ~(0x3 << (1*2));
        io_write32(0x1000a300, data);

		//turn the gpio1 output enable to disable
        data = io_read32(0x1000a314);
        data &= ~(1 << 1);
        io_write32(0x1000a314, data);
	#endif

        //enable gpio19 input mode
        data = io_read32(0x1000a320);
        data &= ~(0x3 << ((19 - 16)*2));
        io_write32(0x1000a320, data);
#else
        //enable gpio19/20 gpio function, normal mode, 10M mode, poe watchdog mode
        data = io_read32(0x1000007c);
        data |= ((1 << 19) | (1 << 20));
        io_write32(0x1000007c, data);

        //enable gpio19 input mode
        data = io_read32(0x1000a320);
        data &= ~(0x3 << ((19 - 16)*2));
        io_write32(0x1000a320, data);

        //enable gpio20 input mode
        data = io_read32(0x1000a320);
        data &= ~(0x3 << ((20 - 16)*2));
        io_write32(0x1000a320, data);
#endif
        printf("Init PSE...\n");

#if defined(SKU_2151) || defined(SKU_2150)
		/* reset PSE on 2150/2151 */
        //enable gpio20 gpio function
        data = io_read32(0x1000007c);
        data |= (1 << 20);
        io_write32(0x1000007c, data);

        //set gpio20 output mode
        data = io_read32(0x1000a320);
        data &= ~(0x3 << ((20 - 16)*2));
        data |= (0x1 << ((20 - 16)*2));
        io_write32(0x1000a320, data);

        //set gpio20 output enable
        data = io_read32(0x1000a314);
        data |= (0x1 << 20);
        io_write32(0x1000a314, data);

        //set gpio20 low
        data = io_read32(0x1000a304);
        data &= ~(0x1 << 20);
        io_write32(0x1000a304, data);

        //delay 100ms
        delay1ms(100);

        //set gpio20 high
        data = io_read32(0x1000a304);
        data |= (0x1 << 20);
        io_write32(0x1000a304, data);
#else
		/* reset PSE on 2149 */
        //enable gpio4 gpio function
        data = io_read32(0x1000007c);
        data |= (1 << 4);
        io_write32(0x1000007c, data);

        //set gpio4 output mode
        data = io_read32(0x1000a300);
        data &= ~(0x3 << (4*2));
        data |= (0x1 << (4*2));
        io_write32(0x1000a300, data);

        //set gpio4 output enable
        data = io_read32(0x1000a314);
        data |= (0x1 << 4);
        io_write32(0x1000a314, data);

        //set gpio4 low
        data = io_read32(0x1000a304);
        data &= ~(0x1 << 4);
        io_write32(0x1000a304, data);

        //delay 100ms
        delay1ms(100);

        //set gpio4 high
        data = io_read32(0x1000a304);
        data |= (0x1 << 4);
        io_write32(0x1000a304, data);
#endif

        //TODO(Netcore): init PSE setting
    }

    customer_createUserTask();
    if (g_is_master)
    {
        customer_createPSETask();
    }

#ifdef SKU_2150
    customer_createSFPTask();
    air_port_setSerdesMode(0, 6, AIR_PORT_SERDES_MODE_1000BASE_X);
    io_write32(0x10210a00, 0xa2159030);
#endif

#ifdef SKU_2151
    io_write32(0x10224018, 2);
    io_write32(0x10224514, 0x01010107);
    io_write32(0x10224018, 0);
#endif

#if 1	/* wy,debug */
	customer_createMiscHdlTask();
#endif

#ifdef SKU_2149
    dip_state = _get_dip_switch_state();
    printf("DIP switch state %d\n", dip_state);

    if (dip_state == DIP_STATE_NORMAL)
    {
        blink_time = 1;
    }
    else if (dip_state == DIP_STATE_PSE_WATCHDOG)
    {
        blink_time = 2;
    }
    else
    {
        blink_time = 3;
    }
    
    hal_pearl_mdio_writeC45(0, 0, 0x0, 0x1F, 0x24, 0x8000);
    hal_pearl_mdio_writeC45(0, 0, 0x0, 0x1F, 0x25, 0x0);
    hal_pearl_mdio_writeC45(0, 0, 0x19, 0x1F, 0x24, 0x8000);
    hal_pearl_mdio_writeC45(0, 0, 0x19, 0x1F, 0x25, 0x0);

    for (i = 0; i < blink_time; i++)
    {
        data = an8801sb_phy_readBuckPbus(0x19, 0x1000a304);
        data &= ~(0x1 << 1);
        an8801sb_phy_writeBuckPbus(0x19, 0x1000a304, data);

        //set gpio5 low
        data = io_read32(0x1000a304);
        data &= ~(0x1 << 5);
        io_write32(0x1000a304, data);

        delay1ms(1000);

        data = an8801sb_phy_readBuckPbus(0x19, 0x1000a304);
        data |= (0x1 << 1);
        an8801sb_phy_writeBuckPbus(0x19, 0x1000a304, data);

        //set gpio5 high
        data = io_read32(0x1000a304);
        data |= (0x1 << 5);
        io_write32(0x1000a304, data);

        delay1ms(1000);
    }

    data = an8801sb_phy_readBuckPbus(0x19, 0x10000070);
    data &= ~(0x1 << 1);
    an8801sb_phy_writeBuckPbus(0x19, 0x10000070, data);
    
    hal_pearl_mdio_writeC45(0, 0, 0x0, 0x1F, 0x24, 0x8007);
    hal_pearl_mdio_writeC45(0, 0, 0x0, 0x1F, 0x25, 0x3f);
    hal_pearl_mdio_writeC45(0, 0, 0x19, 0x1F, 0x24, 0x8007);
    hal_pearl_mdio_writeC45(0, 0, 0x19, 0x1F, 0x25, 0x3f);
    
    data = an8801sb_phy_readBuckPbus(0x19, 0x10000094);
    printf("AN8801SB HWTRAP=0x%x\n", data);
#endif

    //io_write32(0x102000bc, 0xffffffff);

#if 0	/* disabled by wy */
    data = io_read32(0x1000007c);
    data &= ~((1 << 19) | (1 << 20));
    io_write32(0x1000007c, data);
#endif

#if 0	/* disabled by wy,accroding to YaoMin, the bit0 of 0x10000070 set 1 means SMI Slave */
#if 1
	data = io_read32(0x10000070);
	data |= 0x1;
    io_write32(0x10000070, data);
#else
	io_write32(0x10000070, 0x1);
#endif
#endif

    return E_OK;
}

