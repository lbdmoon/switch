
/* Standard includes. */
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>

/* Kernel includes. */
#include <FreeRTOS.h>
#include "FreeRTOSConfig.h"
#include "task.h"
#include "timers.h"
#include "queue.h"
#include "osal.h"

/* Module includes */
#if 1		/* wy,for AN8855M's I2C operation */
#include <air_sif.h>
#else
#include <air_i2c.h>
#include <i2c_cmd.h>
#include <pp_def.h>
#include <air_l2.h>
#endif

/* Function includes */
#include <pp_def.h>
#include <list_linux.h>
#include <os_func.h>
#include <i2c.h>
#include <poe.h>
#include <test.h>


#if 1		/* wy,for AN8855M's I2C operation */
int
i2c_init(uint8_t unit, uint8_t chn)
{
	int rc = E_OK;
	return rc;
}

int
i2c_reg8_val8_read(uint8_t unit, uint8_t chn, uint8_t dev, uint8_t reg, uint8_t *val, 
						uint8_t err_repeat)
{
	AIR_ERROR_NO_T rc = AIR_E_OK;
	uint8_t i;
	UI32_T addrlen = 1, rxlen = 1;

	AIR_SIF_INFO_T   sif_info;
	AIR_SIF_PARAM_T  sif_param;

	for(i = 0; i < err_repeat; i++)
	{
		sif_info.channel = chn;
		sif_info.slave_id = dev;

		sif_param.addr_len = addrlen;
		sif_param.addr = (UI32_T)reg;
		sif_param.data_len = rxlen;
		sif_param.info.data = 0;

		rc = air_sif_read(unit, &sif_info, &sif_param);

		switch(rc)
		{
			case AIR_E_OK:
				*val = (uint8_t)sif_param.info.data;
#if 0
				DEBUG_MSG("data:0x%x ptr_data:%p val:0x%x\n", 
					sif_param.info.data, sif_param.info.ptr_data, *val);
#endif
				if(i > 0)
				{
					DEBUG_MSG("***OK***, read i2c unit:%d chn:%d dev:0x%x reg:0x%x val:0x%x "
						"success!!!return %d,count:%d\n", 
						unit, chn, dev, reg, *val,
						rc, i);
				}
				return rc;
				break;

			default:		
				DEBUG_MSG("***Error***, read i2c unit:%d chn:%d dev:0x%x reg:0x%x "
					"fail!!!return %d,count:%d\n", 
					unit, chn, dev, reg, 
					rc, i);
				continue;
				break;
		}
	}

	return rc;
}

int
i2c_reg8_val8_write(uint8_t unit, uint8_t chn, uint8_t dev, uint8_t reg, uint8_t val,
						uint8_t err_repeat)
{
	AIR_ERROR_NO_T rc = AIR_E_OK;
	uint8_t i;
	UI32_T addrlen = 1, rxlen = 1;

	AIR_SIF_INFO_T   sif_info;
	AIR_SIF_PARAM_T  sif_param;

	for(i = 0; i < err_repeat; i++)
	{
		sif_info.channel = chn;
		sif_info.slave_id = dev;

		sif_param.addr_len = addrlen;
		sif_param.addr = (UI32_T)reg;
		sif_param.data_len = rxlen;
		sif_param.info.data = (UI32_T)val;

		rc = air_sif_write(unit, &sif_info, &sif_param);

		switch(rc)
		{
			case AIR_E_OK:
				if(i > 0)
				{
					DEBUG_MSG("***OK***, write i2c unit:%d chn:%d dev:0x%x reg:0x%x val:0x%x "
						"success!!!return %d,count:%d\n", 
						unit, chn, dev, reg, val,
						rc, i);
				}
				return rc;
				break;

			default:		
				DEBUG_MSG("***Error***, write i2c unit:%d chn:%d dev:0x%x reg:0x%x "
					"fail!!!return %d,count:%d\n", 
					unit, chn, dev, reg, 
					rc, i);
				continue;
				break;
		}
	}

	return rc;
}

int
i2c_reg8_val8_modify(uint8_t unit, uint8_t chn, uint8_t dev, 
							uint8_t reg, uint8_t set, uint8_t msk)
{
	int ret;
	uint8_t val;

	if((ret = i2c_reg8_val8_read(unit, chn, dev, reg, &val, I2C_OP_ERR_REPEAT)) != E_OK)
	{
		DEBUG_MSG("uint:0x%x chn:0x%x dev:0x%x reg:0x%x val:0x%x error return %d\n",
			unit, chn, dev, reg, val, ret);

		return -1;
	}

	val &= ~msk;
	val |= (set & msk);

	if((ret = i2c_reg8_val8_write(unit, chn, dev, reg, val, I2C_OP_ERR_REPEAT)) != E_OK)
	{
		DEBUG_MSG("uint:0x%x chn:0x%x dev:0x%x reg:0x%x val:0x%x error return %d\n",
			unit, chn, dev, reg, val, ret);

		return -2;
	}

	return 0;
}
#else
int
i2c_init(uint8_t unit, uint8_t chn)
{
	int rc = air_i2c_open(unit, chn);
	if(E_OK != rc)
	{
		DEBUG_MSG("***Error***, Open the I2C device-%d on UNIT %d failed !\n", chn, unit);
	}
	else
	{
		DEBUG_MSG("Open the I2C device-%d on UNIT %d success!\n", chn, unit);

	}
	return rc;
}

int
i2c_reg8_val8_read(uint8_t unit, uint8_t chn, uint8_t dev, uint8_t reg, uint8_t *val, 
						uint8_t err_repeat)
{
	int rc = E_OK, i;
	uint8_t addr[1], rxData[1];
	uint32_t addrlen = 1, rxlen = 1;
	AIR_I2C_Master_T i2c_parm;

	for(i = 0; i < err_repeat; i++)
	{
		addr[0] = reg;

		i2c_parm.deviceID = dev;
		i2c_parm.regAddrLen = addrlen;
		i2c_parm.regAddr = addr;
		i2c_parm.dataLen = rxlen;
		i2c_parm.buffPtr = rxData;

		rc = air_i2c_read(unit, chn , &i2c_parm);

		switch(rc)
		{
			case E_OK:
				*val = i2c_parm.buffPtr[0];
				if(i > 0)
				{
					DEBUG_MSG("***OK***, read i2c success!!!return %d,count:%d\n", rc, i);
				}
				return rc;
				break;

			case E_NOT_INITED:
				DEBUG_MSG("***Error***, i2c not open !\n");
				return rc;
				break;

			default:		
				DEBUG_MSG("***Error***, read i2c fail!!!return %d,count:%d\n", rc, i);
				continue;
				break;
		}
	}

	return rc;
}

int
i2c_reg8_val8_write(uint8_t unit, uint8_t chn, uint8_t dev, uint8_t reg, uint8_t val,
						uint8_t err_repeat)
{
	int rc = E_OK, i;
	uint8_t addr[1], txData[1];
	uint32_t addrlen = 1, txlen = 1;
	AIR_I2C_Master_T i2c_parm;

	for(i = 0; i < err_repeat; i++)
	{
		addr[0] = reg;
		txData[0] = val;

		i2c_parm.deviceID = dev;
		i2c_parm.regAddrLen = addrlen;
		i2c_parm.regAddr = addr;
		i2c_parm.dataLen = txlen;
		i2c_parm.buffPtr = txData;

		rc = air_i2c_write(unit, chn , &i2c_parm);

		switch(rc)
		{
			case E_OK:
				if(i > 0)
				{
					DEBUG_MSG("***OK***, write i2c success!!!return %d,count:%d\n", rc, i);
				}
				return rc;
				break;

			case E_NOT_INITED:
				DEBUG_MSG("***Error***, i2c not open !\n");
				return rc;
				break;

			default:		
				DEBUG_MSG("***Error***, write i2c fail!!!return %d,count:%d\n", rc, i);
				continue;
				break;
		}
	}

	return rc;
}

int
i2c_reg8_val8_modify(uint8_t unit, uint8_t chn, uint8_t dev, 
							uint8_t reg, uint8_t set, uint8_t msk)
{
	int ret;
	uint8_t val;

	if((ret = i2c_reg8_val8_read(unit, chn, dev, reg, &val, I2C_OP_ERR_REPEAT)) != E_OK)
	{
		DEBUG_MSG("uint:0x%x chn:0x%x dev:0x%x reg:0x%x val:0x%x error return %d\n",
			unit, chn, dev, reg, val, ret);

		return -1;
	}

	val &= ~msk;
	val |= (set & msk);

	if((ret = i2c_reg8_val8_write(unit, chn, dev, reg, val, I2C_OP_ERR_REPEAT)) != E_OK)
	{
		DEBUG_MSG("uint:0x%x chn:0x%x dev:0x%x reg:0x%x val:0x%x error return %d\n",
			unit, chn, dev, reg, val, ret);

		return -2;
	}

	return 0;
}
#endif

