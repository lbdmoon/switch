
/* Standard includes. */
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

/* Kernel includes. */
#include <FreeRTOS.h>
#include "FreeRTOSConfig.h"
#include "task.h"
#include "timers.h"
#include "queue.h"
#include "osal.h"

/* module includes */
#include <pp_def.h>
#include <air_l2.h>
#include <air_sif.h>
#include <air_chipscu.h>
#include <air_gpio.h>
#include <air_port.h>
#include <util.h>

#include <list_linux.h>
#include <os_func.h>
#include <i2c.h>
#include <poe.h>
#include <test.h>

static poe_id_t poeid[POE_I2C_MAX_DEV] = {[0] = {.valid = 0,},};
static poe_id_mgmt_t pim = {0,};

static cmdline_func_t cf[] = 
{
	{.cmd = TEST_DEBUG, .num_args = 1, .f= do_debug,},
	{.cmd = TEST_SWITCH_MAC, .num_args = 0, .f= do_switch_mac,},
	{.cmd = TEST_POE, .num_args = 3, .f= do_poe,},
	{.cmd = TEST_FAN, .num_args = 2, .f= do_fan,},
	{.cmd = TEST_FUNC, .num_args = 1, .f= do_func,},
	{.cmd = TEST_LED, .num_args = 2, .f= do_led,},
	{.cmd = TEST_KEY, .num_args = 1, .f= do_key,},
};

uint32_t debug_code_mask = BITMSK(DBG_REMOTE_PRINT_ENABLE) | 
	BITMSK(DBG_REMOTE_PRINT_DBG_INFO_ENABLE);

//static TimerHandle_t poe_timer_hdl = NULL;
//static uint8_t poe_mgmt_val = 0;

static TimerHandle_t poe_status_timer_hdl = NULL;
uint8_t poe_status_routing_msk = 0;
poe_status_total_t poe_status_total[POE_I2C_MAX_DEV];

static TimerHandle_t led_timer_hdl = NULL;
static led_set_status_e led_st[MAX_NUM_GPIO_LED] = {LED_SET_OFF,};

static TimerHandle_t key_timer_hdl;

int
set_pse_reset_flag(uint8_t cidx)
{
	poe_id_t *pid;
	uint8_t val;
	uint8_t i;

	if(!(cidx < POE_CHIP_TOTAL && cidx < POE_MAX_CURRENT_CHIP))
	{
		DBG_PRINT("%s:%d cidx %d invalid\n", __FUNCTION__, __LINE__, cidx);
		return -1;
	}

	pid = &POE_CHIP[cidx];

	for(i = 0; i < 3; i++)
	{
		poe_reg_write(pid, POE_T760X_RESERVED_REG, POE_CK_RST_VAL);
		poe_reg_read(pid, POE_T760X_RESERVED_REG, &val);

		if(val == POE_CK_RST_VAL)
		{
			DBG_PRINT("It's success to setting reset flag to PSE\n");
			return 0;
		}
	}

	DBG_PRINT("It's failure to setting reset flag to PSE\n");
	return -2;
}

uint32_t
check_pse_reseted(uint8_t en_reinit)
{
	uint8_t val, tmp[3];
	uint8_t cidx;
	poe_id_t *pid;
	uint32_t msk = 0;

	for(cidx = 0; cidx < POE_CHIP_TOTAL && cidx < POE_MAX_CURRENT_CHIP; cidx++)
	{
		pid = &POE_CHIP[cidx];
		poe_reg_read(pid, POE_T760X_RESERVED_REG, &tmp[0]);
		poe_reg_read(pid, POE_T760X_RESERVED_REG, &tmp[1]);
		poe_reg_read(pid, POE_T760X_RESERVED_REG, &tmp[2]);

		val = tmp[0] | tmp[1] | tmp[2];

		if(val != POE_CK_RST_VAL)
		{
			DBG_PRINT("\n\n");
			DBG_PRINT("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n");
			DBG_PRINT("PSE Chip index %d has been reseted,"
						"current value 0x%x[%02x %02x %02x]\n",
						cidx, val, tmp[0], tmp[1], tmp[2]);
			DBG_PRINT("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n");
			DBG_PRINT("\n\n");
			msk |= BITMSK(cidx);

			set_pse_reset_flag(cidx);

			if(en_reinit)
			{
				DBG_PRINT("Reinit POE chip index %d\n", cidx);
				poe_chip_init_all(pid, 1, POE_CHIP_MODE, POE_PWR_MGMT_IN_HW);
			}
		}
		else
		{
			DBG_PRINT("PSE Chip index %d hasn't been reseted,"
					"current value 0x%x[%02x %02x %02x]\n", 
					cidx, val, tmp[0], tmp[1], tmp[2]);
		}
	}

	return msk;
}

int
poe_id_init(uint32_t idx, uint8_t unit, uint8_t chn, uint8_t dev)
{
	//uint8_t i;
	uint8_t index = idx % NUMOFARRAY(poeid);
	poe_id_t *pid = &poeid[index];
	//poe_port_t *ppp;

	DEBUG_MSG("On chip idx %ld,addr is 0x%x\n", idx, pid->dev);
	
	pid->unit = unit;
	pid->chn = chn;
	pid->dev = dev;
	pid->valid = 1;
	pid->base_port = 0;
	pid->total_port = 0;
	pid->chip_type = 0;
#if 0
	for(i = 0; i < NUMOFARRAY(pid->port); i++)
	{
		ppp = &pid->port[i];

		ppp->chip = index;
		ppp->port = i;
		ppp->pwr_enabled_cnt = 0;
		ppp->pwr_up_success = 0;

		ppp->dtc = 0;
		ppp->cls = 0;
		ppp->st_en = 0;
		ppp->st_gd = 0;
	}
#endif
	return 0;
}

void
print_mac(AIR_MAC_ENTRY_T *p, uint32_t num)
{
	uint32_t i;

	for(i = 0; i < num; i++, p++)
	{
		osal_printf("%02x:%02x:%02x:%02x:%02x:%02x cvid:%d fid:%d flags:0x%x "
			"port_bitmap:0x%x sa_fwd:%d timer:%d\n", 
			p->mac[0], p->mac[1], p->mac[2], p->mac[3], p->mac[4], p->mac[5],
			p->cvid, p->fid, p->flags, p->port_bitmap[0], p->sa_fwd, p->timer);
	}
}

int
do_switch_mac(int argc, char *argv[])
{
	uint8_t cnt;
	UI32_T num;
	uint32_t unit, total;
	AIR_ERROR_NO_T ret;
	AIR_MAC_ENTRY_T *p;

	unit = 0;
	num = 0;
	total = 0;

	ret = air_l2_getMacBucketSize(unit, &num);
	osal_printf("air_l2_getMacBucketSize(%d, %d) return %d,\n", unit, num, ret);
	if(AIR_E_OK != ret)
	{
		osal_printf("Error on air_l2_getMacBucketSize\n");
		return -1;
	}

	p = osal_alloc(sizeof(*p) * num, "testing");
	if(!p)
	{
		osal_printf("Error on osal_alloc\n");
		return -2;
	}

	osal_memset(p, 0, sizeof(*p) * num);
	ret = air_l2_getMacAddr(unit, &cnt, p);
	osal_printf("air_l2_getMacAddr(%d, %d, %p) return %d,\n", unit, cnt, p, ret);
	if(AIR_E_OK != ret)
	{
		osal_printf("Error on air_l2_getMacAddr\n");
		osal_free(p);
		return -3;
	}
	else
	{
		total += cnt;
		print_mac(p, cnt);
	}

	do
	{
		osal_memset(p, 0, sizeof(*p) * num);
		ret = air_l2_getNextMacAddr(unit, &cnt, p);
		osal_printf("air_l2_getNextMacAddr(%d, %d, %p) return %d,\n", unit, cnt, p, ret);
		if(AIR_E_OK != ret)
		{
			osal_printf("Error on air_l2_getNextMacAddr\n");
			break;
		}
		else
		{
			total += cnt;
			print_mac(p, cnt);
		}
	}while(1);

	osal_printf("total is %d,done\n", total);
	osal_free(p);
	return 0;
}

int
get_argcv_from_str(char *input, char **argv, uint32_t argvnum)
{
	int i, argc;
	char *p;
	char *arg_begin;
	int iptlen;

	if(!input || !argv || !argvnum)
	{
		return -1;
	}

	iptlen = osal_strlen(input);
	for(i = 0, argc = 0, p = input, arg_begin = NULL; 
		i < iptlen; 
		i++, p++)
	{
		if(isspace((int)*p))
		{
			*p = '\0';
			if(!arg_begin)
			{
				continue;
			}
			else
			{/* end of argument occur */
				arg_begin = 0;
			}
		}
		else
		{
			if(!arg_begin)
			{/* begin of argument occur */
				arg_begin = p;
				if(argc < argvnum)
				{
					argv[argc++] = arg_begin;
				}
			}
			else
			{
				continue;
			}
		}
	}

	return argc;
}

void
show_perpheral_reg(uint32_t start, uint32_t size)
{
	uint32_t addr = start;
	uint32_t idx, val;
	char tbuf[64];
	int slen;

	for(idx = 0, addr = start, slen = 0; addr < start + size; addr += 4, idx++)
	{
		val = io_read32(addr);

		if((idx % 4) == 0)
		{
			slen += snprintf(tbuf + slen, sizeof(tbuf) - slen, "%08lx:", addr);
		}

		slen += snprintf(tbuf + slen, sizeof(tbuf) - slen, " %08lx", val);

		if((idx % 4) == 3)
		{
			tbuf[sizeof(tbuf) - 1] = '\0';
			DBG_PRINT("%s\n", tbuf);
			slen = 0;
		}
	}

	if(slen)
	{
		tbuf[sizeof(tbuf) - 1] = '\0';
		DBG_PRINT("%s\n", tbuf);
		slen = 0;
	}
}

void
show_mem(uint32_t start, uint32_t size)
{
	uint32_t addr = start;
	uint32_t idx, val;
	char tbuf[64];
	int slen;

	for(idx = 0, addr = start, slen = 0; addr < start + size; addr += 4, idx++)
	{
		val = *(volatile uint32_t*)(addr);

		if((idx % 4) == 0)
		{
			slen += snprintf(tbuf + slen, sizeof(tbuf) - slen, "%08lx:", addr);
		}

		slen += snprintf(tbuf + slen, sizeof(tbuf) - slen, " %08lx", val);

		if((idx % 4) == 3)
		{
			tbuf[sizeof(tbuf) - 1] = '\0';
			DBG_PRINT("%s\n", tbuf);
			slen = 0;
		}
	}

	if(slen)
	{
		tbuf[sizeof(tbuf) - 1] = '\0';
		DBG_PRINT("%s\n", tbuf);
		slen = 0;
	}
}

void
waiting_for_wd(void)
{
	osal_printf("Current tick %lu,waiting for watchdog trigger\n", time_current_tick());

	taskENTER_CRITICAL();
	/* Set ul to a non-zero value using the debugger to step out of this
	function. */
	while(1)
	{
		portNOP();
	}
	taskEXIT_CRITICAL();
}

int
tm_show_status(uint8_t index)
{
	int ret;
	uint8_t val;
	uint8_t cid, ver;
	uint8_t idx, t[4], cls4p[4];
	uint32_t c, v, cua, vmv, pmw;
	char *str_mode[4] = 
		{
			[MODE_SHUTDOWN] = "Shutdown",
			[MODE_MANUAL] = "Manual",
			[MODE_SEMIAUTO] = "SemiAutomatic",
			[MODE_AUTO] = "Automatic",
		};
	char *str_class[8] = 
		{
			[0] = "no class",
			[1] = "class 1",
			[2] = "class 2",
			[3] = "class 3",
			[4] = "class 4",
			[5] = "class 4+",
			[6] = "class 0",
			[7] = "current limit",
		};
	char *str_detection[8] = 
		{
			[0] = "no detection",
			[1] = "DCP",
			[2] = "high capacitance",
			[3] = "low resistance",
			[4] = "detection ok",
			[5] = "high resistance",
			[6] = "open",
			[7] = "DCN",
		};
	char *str_chip_type[2] = 
		{
			[POE_CHIP_TYPE_T7604R] = "TMI7604R",
			[POE_CHIP_TYPE_T7608R] = "TMI7608R",
		};
	poe_chip_type_e pct;
	poe_id_mgmt_t *ppim = &pim;
	poe_id_t *id = &ppim->pid[index % ppim->num];

	if(!id->valid)
	{
		osal_printf("Initialize POE first\n");
		return -1;
	}

	DEBUG_MSG("On chip idx %d,addr is 0x%x\n", index, id->dev);

	if((ret = i2c_reg8_val8_read(id->unit, id->chn, id->dev, POE_T760X_ID_REG, &val, 
							I2C_OP_ERR_REPEAT)) != E_OK)
	{
		DEBUG_MSG("uint:0x%x chn:0x%x dev:0x%x reg:0x%x val:0x%x error return %d\n",
			id->unit, id->chn, id->dev, POE_T760X_ID_REG, val, ret);

		return -1;
	}

	cid = (val >> 3) & 0x1f;
	ver = val & 0x7;
	pct = (cid == POE_CHIP_ID) ? POE_I2C_CHIP_TYPE(id->dev) : POE_CHIP_TYPE_MAX;
	osal_printf("Chip ID:0x%x version:0x%x,poe chip type %d,chip is %s\n", 
		cid, ver, pct,
		pct < POE_CHIP_TYPE_MAX ? str_chip_type[pct % NUMOFARRAY(str_chip_type)] : "unknown");

	if((ret = i2c_reg8_val8_read(id->unit, id->chn, id->dev, POE_T760X_UNDER_V_REG, &val, 
							I2C_OP_ERR_REPEAT)) != E_OK)
	{
		DEBUG_MSG("uint:0x%x chn:0x%x dev:0x%x reg:0x%x val:0x%x error return %d\n",
			id->unit, id->chn, id->dev, POE_T760X_UNDER_V_REG, val, ret);

		return -2;
	}

	if((ret = i2c_reg8_val8_read(id->unit, id->chn, id->dev, 
		POE_T760X_CHIP_VOLT_L_REG, &t[0], I2C_OP_ERR_REPEAT)) != E_OK)
	{
		DEBUG_MSG("uint:0x%x chn:0x%x dev:0x%x reg:0x%x val:0x%x error return %d\n",
			id->unit, id->chn, id->dev, POE_T760X_CHIP_VOLT_L_REG, val, ret);
	
		return -9;
	}
	
	if((ret = i2c_reg8_val8_read(id->unit, id->chn, id->dev, 
		POE_T760X_CHIP_VOLT_M_REG, &t[1], I2C_OP_ERR_REPEAT)) != E_OK)
	{
		DEBUG_MSG("uint:0x%x chn:0x%x dev:0x%x reg:0x%x val:0x%x error return %d\n",
			id->unit, id->chn, id->dev, POE_T760X_CHIP_VOLT_M_REG, val, ret);
	
		return -10;
	}
	
	v = (uint32_t)t[0] | ((uint32_t)(t[1] & 0x1f) << 8);
	vmv = v * 1245 / 32 / 10;

	osal_printf("Input power V54 is %s,chip voltage is %d mv(0x%02x 0x%02x %d) or %d mv\n", 
		(val & (1 << 5)) ? "less than 40v" : "normal", vmv, t[1], t[0], v, 
		((uint32_t)t[0] | ((uint32_t)t[1] << 8)) * 1245 / 32 / 10);

	if((ret = i2c_reg8_val8_read(id->unit, id->chn, id->dev, POE_T760X_MODE_REG, &val, 
							I2C_OP_ERR_REPEAT)) != E_OK)
	{
		DEBUG_MSG("uint:0x%x chn:0x%x dev:0x%x reg:0x%x val:0x%x error return %d\n",
			id->unit, id->chn, id->dev, POE_T760X_MODE_REG, val, ret);

		return -3;
	}

	osal_printf("Port Mode:\n");
	for(idx = 0; idx <= 3; idx++)
	{
		t[0] = (val >> 2 * idx) & 0x3;
		osal_printf("\tPort[%d] %d %s\n", idx, t[0], str_mode[t[0]]);
	}

	if((ret = i2c_reg8_val8_read(id->unit, id->chn, id->dev, POE_T760X_PWR_EN_REG, &t[0], 
							I2C_OP_ERR_REPEAT)) != E_OK)
	{
		DEBUG_MSG("uint:0x%x chn:0x%x dev:0x%x reg:0x%x val:0x%x error return %d\n",
			id->unit, id->chn, id->dev, POE_T760X_PWR_EN_REG, val, ret);

		return -4;
	}

	if((ret = i2c_reg8_val8_read(id->unit, id->chn, id->dev, POE_T760X_PWR_GOOD_REG, &t[1], 
							I2C_OP_ERR_REPEAT)) != E_OK)
	{
		DEBUG_MSG("uint:0x%x chn:0x%x dev:0x%x reg:0x%x val:0x%x error return %d\n",
			id->unit, id->chn, id->dev, POE_T760X_PWR_GOOD_REG, val, ret);

		return -5;
	}

	osal_printf("Port Status 1:\n");
	for(idx = 0; idx <= 3; idx++)
	{
		if((ret = i2c_reg8_val8_read(id->unit, id->chn, id->dev, 
			POE_T760X_PORT_STAUTS_REG(idx), &val, I2C_OP_ERR_REPEAT)) != E_OK)
		{
			DEBUG_MSG("uint:0x%x chn:0x%x dev:0x%x reg:0x%x val:0x%x error return %d\n",
				id->unit, id->chn, id->dev, POE_T760X_PORT_STAUTS_REG(idx), val, ret);

			return -6;
		}
		t[2] = (val >> 4) & 0x7;
		t[3] = (val >> 0) & 0x7;
		osal_printf("\tPort[%d]:class %d[%s],detection %d[%s],power %s and %s\n", 
			idx, t[2], str_class[t[2]], t[3], str_detection[t[3]],
			(t[0] & (1 << idx)) ? "on" : "off",
			(t[1] & (1 << idx)) ? "good" : "bad");

		cls4p[idx] = ((t[2] == 5) ? 1 : 0);
	}

	osal_printf("Port Status 2:\n");
	for(idx = 0; idx <= 3; idx++)
	{
		if((ret = i2c_reg8_val8_read(id->unit, id->chn, id->dev, 
			POE_T760X_PORT_CURR_L_REG(idx), &t[0], I2C_OP_ERR_REPEAT)) != E_OK)
		{
			DEBUG_MSG("uint:0x%x chn:0x%x dev:0x%x reg:0x%x val:0x%x error return %d\n",
				id->unit, id->chn, id->dev, POE_T760X_PORT_CURR_L_REG(idx), val, ret);

			return -7;
		}

		if((ret = i2c_reg8_val8_read(id->unit, id->chn, id->dev, 
			POE_T760X_PORT_CURR_M_REG(idx), &t[1], I2C_OP_ERR_REPEAT)) != E_OK)
		{
			DEBUG_MSG("uint:0x%x chn:0x%x dev:0x%x reg:0x%x val:0x%x error return %d\n",
				id->unit, id->chn, id->dev, POE_T760X_PORT_CURR_M_REG(idx), val, ret);

			return -8;
		}

		c = (uint32_t)t[0] | ((uint32_t)t[1] << 8);
		cua = c * 1956 / 16;
		cua = (cls4p[idx] ? cua * 2 : cua);

		if((ret = i2c_reg8_val8_read(id->unit, id->chn, id->dev, 
			POE_T760X_PORT_VOLT_L_REG(idx), &t[2], I2C_OP_ERR_REPEAT)) != E_OK)
		{
			DEBUG_MSG("uint:0x%x chn:0x%x dev:0x%x reg:0x%x val:0x%x error return %d\n",
				id->unit, id->chn, id->dev, POE_T760X_PORT_VOLT_L_REG(idx), val, ret);

			return -9;
		}

		if((ret = i2c_reg8_val8_read(id->unit, id->chn, id->dev, 
			POE_T760X_PORT_VOLT_M_REG(idx), &t[3], I2C_OP_ERR_REPEAT)) != E_OK)
		{
			DEBUG_MSG("uint:0x%x chn:0x%x dev:0x%x reg:0x%x val:0x%x error return %d\n",
				id->unit, id->chn, id->dev, POE_T760X_PORT_VOLT_M_REG(idx), val, ret);

			return -10;
		}

		v = (uint32_t)t[2] | ((uint32_t)t[3] << 8);
		vmv = v * 1265 / 32 / 10;
		pmw = ((cua / 1000) * vmv) /1000;

		osal_printf("\tPort[%d]:current %d uA(0x%02x 0x%02x %d),voltage %d mV(0x%02x 0x%02x %d) "
			"power %d mW\n", 
			idx, cua, t[1], t[0], c, vmv, t[3], t[2], v, pmw);
	}

	return 0;
}

int
tm_chip_reg(poe_id_t *id, uint8_t reg, uint8_t val, uint8_t set)
{
	int ret;
	uint8_t idx, start, end, cnt, tmp = -1;
	uint8_t buf[POE_T760X_MAX_REG];

	//DEBUG_MSG("reg:0x%x -1:0x%x (uint8_t)-1:0x%x\n", reg, -1, (uint8_t)-1);

	if(!id->valid)
	{
		printf("Initialize POE first\n");
		return -1;
	}

	DEBUG_MSG("On chip idx %d,addr is 0x%x\n", id->chip_idx, id->dev);

	if(set)
	{/* set value to reg */
		for(cnt = 0; cnt < 3; cnt++)
		{
			if((ret = i2c_reg8_val8_write(id->unit, id->chn, id->dev, reg, val, 
									I2C_OP_ERR_REPEAT)) != E_OK)
			{
				DEBUG_MSG("uint:0x%x chn:0x%x dev:0x%x reg:0x%x val:0x%x "
					"error return %d,count is %d\n",
					id->unit, id->chn, id->dev, reg, val, ret, cnt);
				continue;
			}
			else
			{
				if(cnt)
				{
					DEBUG_MSG("uint:0x%x chn:0x%x dev:0x%x reg:0x%x val:0x%x "
						"ok return %d,count is %d\n",
						id->unit, id->chn, id->dev, reg, val, ret, cnt);
				}
				break;
			}
		}
	}
	else
	{/* show value of reg */
		if(reg != tmp)
		{
			start = reg;
			end = reg + 1;
			//DEBUG_MSG("Do special reg:0x%x\n", reg);
		}
		else
		{
			start = 0;
			end = POE_T760X_MAX_REG;
			//DEBUG_MSG("Do rang reg from:0x%x to:0x%x\n", start, end);
		}

		for(idx = start; idx < end; idx++)
		{
			for(cnt = 0; cnt < 3; cnt++)
			{
				if((ret = i2c_reg8_val8_read(id->unit, id->chn, id->dev, idx, &buf[idx], 
										I2C_OP_ERR_REPEAT)) != E_OK)
				{
					DEBUG_MSG("uint:0x%x chn:0x%x dev:0x%x reg:0x%x val:0x%x "
						"error return %d,count is %d\n",
						id->unit, id->chn, id->dev, idx, buf[idx], ret, cnt);
					continue;
				}
				else
				{
					if(cnt)
					{
						DEBUG_MSG("uint:0x%x chn:0x%x dev:0x%x reg:0x%x val:0x%x "
							"ok return %d,count is %d\n",
							id->unit, id->chn, id->dev, idx, buf[idx], ret, cnt);
					}
					break;
				}
			}
		}

		if(reg != tmp)
		{
			osal_printf("uint:0x%x chn:0x%x dev:0x%x reg:0x%x val:0x%x\n",
					id->unit, id->chn, id->dev, start, buf[start]);
			return 0;
		}
		else
		{
			osal_printf("Show all register:\n");
			for(idx = start; idx < end; idx += 0x10)
			{
				printf("0x%02x:  "
					"%02x %02x %02x %02x  %02x %02x %02x %02x    "
					"%02x %02x %02x %02x  %02x %02x %02x %02x\n",
					idx, 
					buf[idx + 0], buf[idx + 1], buf[idx + 2], buf[idx + 3], 
					buf[idx + 4], buf[idx + 5], buf[idx + 6], buf[idx + 7], 
					buf[idx + 8], buf[idx + 9], buf[idx + 0xa], buf[idx + 0xb], 
					buf[idx + 0xc], buf[idx + 0xd], buf[idx + 0xe], buf[idx + 0xf]
					);
			}
		}
	}
	return 0;
}

int
tm_reg(uint8_t index, uint8_t reg, uint8_t val, uint8_t set)
{
	int ret;
	uint8_t idx, start, end, cnt, tmp = -1;
	uint8_t buf[POE_T760X_MAX_REG];
	poe_id_mgmt_t *ppim = &pim;	
	poe_id_t *id = &ppim->pid[index % ppim->num];

	//DEBUG_MSG("reg:0x%x -1:0x%x (uint8_t)-1:0x%x\n", reg, -1, (uint8_t)-1);

	if(!id->valid)
	{
		printf("Initialize POE first\n");
		return -1;
	}

	DEBUG_MSG("On chip idx %d,addr is 0x%x\n", index, id->dev);

	if(set)
	{/* set value to reg */
		for(cnt = 0; cnt < 3; cnt++)
		{
			if((ret = i2c_reg8_val8_write(id->unit, id->chn, id->dev, reg, val, 
									I2C_OP_ERR_REPEAT)) != E_OK)
			{
				DEBUG_MSG("uint:0x%x chn:0x%x dev:0x%x reg:0x%x val:0x%x "
					"error return %d,count is %d\n",
					id->unit, id->chn, id->dev, reg, val, ret, cnt);
				continue;
			}
			else
			{
				if(cnt)
				{
					DEBUG_MSG("uint:0x%x chn:0x%x dev:0x%x reg:0x%x val:0x%x "
						"ok return %d,count is %d\n",
						id->unit, id->chn, id->dev, reg, val, ret, cnt);
				}
				break;
			}
		}
	}
	else
	{/* show value of reg */
		if(reg != tmp)
		{
			start = reg;
			end = reg + 1;
			//DEBUG_MSG("Do special reg:0x%x\n", reg);
		}
		else
		{
			start = 0;
			end = POE_T760X_MAX_REG;
			//DEBUG_MSG("Do rang reg from:0x%x to:0x%x\n", start, end);
		}

		for(idx = start; idx < end; idx++)
		{
			for(cnt = 0; cnt < 3; cnt++)
			{
				if((ret = i2c_reg8_val8_read(id->unit, id->chn, id->dev, idx, &buf[idx], 
										I2C_OP_ERR_REPEAT)) != E_OK)
				{
					DEBUG_MSG("uint:0x%x chn:0x%x dev:0x%x reg:0x%x val:0x%x "
						"error return %d,count is %d\n",
						id->unit, id->chn, id->dev, idx, buf[idx], ret, cnt);
					continue;
				}
				else
				{
					if(cnt)
					{
						DEBUG_MSG("uint:0x%x chn:0x%x dev:0x%x reg:0x%x val:0x%x "
							"ok return %d,count is %d\n",
							id->unit, id->chn, id->dev, idx, buf[idx], ret, cnt);
					}
					break;
				}
			}
		}

		if(reg != tmp)
		{
			osal_printf("uint:0x%x chn:0x%x dev:0x%x reg:0x%x val:0x%x\n",
					id->unit, id->chn, id->dev, start, buf[start]);
			return 0;
		}
		else
		{
			osal_printf("Show all register:\n");
			for(idx = start; idx < end; idx += 0x10)
			{
				printf("0x%02x:  "
					"%02x %02x %02x %02x  %02x %02x %02x %02x    "
					"%02x %02x %02x %02x  %02x %02x %02x %02x\n",
					idx, 
					buf[idx + 0], buf[idx + 1], buf[idx + 2], buf[idx + 3], 
					buf[idx + 4], buf[idx + 5], buf[idx + 6], buf[idx + 7], 
					buf[idx + 8], buf[idx + 9], buf[idx + 0xa], buf[idx + 0xb], 
					buf[idx + 0xc], buf[idx + 0xd], buf[idx + 0xe], buf[idx + 0xf]
					);
			}
		}
	}
	return 0;
}

int
tm_power_supply(uint8_t index, uint8_t port, uint8_t en)
{
	int ret;
	uint8_t set, msk;
	poe_id_t *id = &poeid[index % NUMOFARRAY(poeid)];

	if(!id->valid)
	{
		osal_printf("Initialize POE first\n");
		return -1;
	}

	DEBUG_MSG("On chip idx %d,addr is 0x%x\n", index, id->dev);

	set = en ? (MODE_AUTO << (port * 2)) : (MODE_SHUTDOWN << (port * 2));
	msk = MODE_MASK << (port * 2);
	if((ret = i2c_reg8_val8_modify(id->unit, id->chn, id->dev, POE_T760X_MODE_REG, 
							set, msk)) != E_OK)
	{
		DEBUG_MSG("uint:0x%x chn:0x%x dev:0x%x reg:0x%x set:0x%x msk:0x%x error return %d\n",
			id->unit, id->chn, id->dev, POE_T760X_MODE_REG, set, msk, ret);

		return -1;
	}

	set = en ? (1 << port) : (0 << port);
	msk = (1 << port);
	if((ret = i2c_reg8_val8_modify(id->unit, id->chn, id->dev, POE_T760X_DET_EN_REG, 
							set, msk)) != E_OK)
	{
		DEBUG_MSG("uint:0x%x chn:0x%x dev:0x%x reg:0x%x set:0x%x msk:0x%x error return %d\n",
			id->unit, id->chn, id->dev, POE_T760X_DET_EN_REG, set, msk, ret);

		return -2;
	}

	set = en ? (1 << port) : (0 << port);
	msk = (1 << port);
	if((ret = i2c_reg8_val8_modify(id->unit, id->chn, id->dev, POE_T760X_CLS_EN_REG, 
							set, msk)) != E_OK)
	{
		DEBUG_MSG("uint:0x%x chn:0x%x dev:0x%x reg:0x%x set:0x%x msk:0x%x error return %d\n",
			id->unit, id->chn, id->dev, POE_T760X_CLS_EN_REG, set, msk, ret);

		return -3;
	}

	return 0;
}

int
do_debug(int argc, char *argv[])
{
	return debug_func(argc, argv);
}

int
do_poe_tm(int argc, char *argv[])
{
	uint32_t a[10];
	uint8_t loop = 0, idx;
	poe_id_mgmt_t *ppim = &pim;

	ppim->pid = &POE_CHIP[0];
	ppim->num = POE_CHIP_TOTAL;

	if(argc < 2)
	{
		osal_printf("Parameters is not enough\n");
		return -1;
	}

	if(!strncasecmp(argv[1], POE_INIT, sizeof(POE_INIT)))
	{
		if(argc < 6)
		{
			printf("Parameters is not enough\n");
			return -1;
		}

		a[0] = strtoul(argv[2], NULL, 0);
		a[1] = strtoul(argv[3], NULL, 0);
		a[2] = strtoul(argv[4], NULL, 0);
		a[3] = strtoul(argv[5], NULL, 0);

		poe_id_init(a[0], a[1], a[2], a[3]);

		DEBUG_MSG("Init args idx:0x%lx unit:0x%lx channel:0x%lx dev addr:0x%lx\n", 
			a[0], a[1], a[2], a[3]);
	}
	else if(!strncasecmp(argv[1], POE_INFO, sizeof(POE_INFO)))
	{
		if(argc < 3)
		{
			printf("Parameters is not enough\n");
			return -1;
		}

		a[0] = strtoul(argv[2], NULL, 0);

		tm_show_status(a[0]);
	}
	else if(!strncasecmp(argv[1], POE_REG, sizeof(POE_REG)))
	{
		if(argc < 3)
		{
			loop = 1;
			a[0] = 0;
			a[1] = -1;
			a[2] = 0;
			a[3] = 0;		/* showing */
		}
		else if(argc < 4)
		{
			a[0] = strtoul(argv[2], NULL, 0);
			a[1] = -1;
			a[2] = 0;
			a[3] = 0;		/* showing */
		}
		else if(argc < 5)
		{
			a[0] = strtoul(argv[2], NULL, 0);
			a[1] = strtoul(argv[3], NULL, 0);
			a[2] = 0;
			a[3] = 0;		/* showing */
		}
		else if(argc < 6)
		{
			a[0] = strtoul(argv[2], NULL, 0);
			a[1] = strtoul(argv[3], NULL, 0);
			a[2] = strtoul(argv[4], NULL, 0);
			a[3] = 1;		/* setting */
		}
		else
		{
			loop = 1;
			a[0] = 0;
			a[1] = -1;
			a[2] = 0;
			a[3] = 0;		/* showing */
		}

		idx = 0;
		do
		{
			tm_reg(a[0], a[1], a[2], a[3]);
			idx++;
		}while(loop && idx < ppim->num && (a[0] = idx));
	}
	else if(!strncasecmp(argv[1], POE_PWR, sizeof(POE_PWR)))
	{
		if(argc < 5)
		{
			osal_printf("Parameters is not enough\n");
			return -1;
		}
		a[0] = strtoul(argv[2], NULL, 0);
		a[1] = strtoul(argv[3], NULL, 0);
		a[2] = strtoul(argv[4], NULL, 0);

		tm_power_supply(a[0], a[1], a[2]);
	}
	else if(!strncasecmp(argv[1], POE_MGMT, sizeof(POE_MGMT)))
	{
		if(argc < 3)
		{
			osal_printf("Parameters is not enough\n");
			return -1;
		}
		
		a[0] = strtoul(argv[2], NULL, 0);

		poe_pwr_mgmt_routing(a[0]);
	}
	else if(!strncasecmp(argv[1], POE_STATUS, sizeof(POE_STATUS)))
	{
		if(argc < 3)
		{
			osal_printf("Parameters is not enough\n");
			return -1;
		}
		
		a[0] = strtoul(argv[2], NULL, 0);

		poe_status_routing(a[0]);
	}

	return 0;
}

int
do_poe(int argc, char *argv[])
{
	static char initialized = 0;

	if(!initialized)
	{
		i2c_init(0, 0);
		i2c_init(0, 1);
		module_product_init();
		initialized = 1;
	}

	return do_poe_tm(argc, argv);
}

int
do_fan(int argc, char *argv[])
{
	return fan_func(argc, argv);
}

int
do_func(int argc, char *argv[])
{
	return test_func(argc, argv);
}

int
do_led(int argc, char *argv[])
{
	return led_func(argc, argv);
}

int
do_key(int argc, char *argv[])
{
	return key_func(argc, argv);
}

void
testing_help(char *cmd)
{
	osal_printf("%s %s %s %s %s\n", cmd, TEST_DEBUG, "[set|get]", "[debug mask setting]",  "#Do Debug operation");
	osal_printf("%s %s %s\n", cmd, TEST_SWITCH_MAC, "#Do MAC getting");
	osal_printf("%s %s %s %s %s\n", cmd, TEST_FAN, "[fan index]", "[0:off 1:on level1 2:on level2]", "#Do FAN setting");
	osal_printf("%s %s %s %s\n", cmd, TEST_FUNC, "[testing functrion index from 0-3 for POE Power ON/OFF,4 for register showing,5 for memory showing,6 for task and stack and heap showing,7 for waiting watchdog trigger]", "#Do function testing");
	osal_printf("%s %s %s %s \n", cmd, TEST_KEY, "[status 0:stop 1:start]",  "#Do Key testing");
	osal_printf("%s %s %s %s %s\n", cmd, TEST_LED, "[led index(0-1 for gpio;2-29 for port;big than 29 for all)]", "[0:off 1:on 2:flash 3:stop led timer 4:normal]", "#Do LED setting");
	osal_printf("%s %s %s %s %s %s %s %s\n", cmd, TEST_POE, POE_INIT, "[chip index]", "[unit]", "[channel]", "[dev_addr]", "#Initialize POE CHIP info");
	osal_printf("%s %s %s %s %s\n", cmd, TEST_POE, POE_INFO, "[chip index]", "#Show POE CHIP information");
	osal_printf("%s %s %s %s %s\n", cmd, TEST_POE, POE_REG, "[chip index]", "#Show POE ALL register");
	osal_printf("%s %s %s %s %s %s\n", cmd, TEST_POE, POE_REG, "[chip index]", "[reg]", "#Show POE special register");
	osal_printf("%s %s %s %s %s %s %s\n", cmd, TEST_POE, POE_PWR, "[chip index]", "[port]", "[en:1|dis:0]", "#Enable or disable power supply at special port");
	osal_printf("%s %s %s %s %s\n", cmd, TEST_POE, POE_MGMT, "[en:1|dis:0]", "#Enable or disable the POE management of power up function");
	osal_printf("%s %s %s %s %s\n", cmd, TEST_POE, POE_STATUS, 
		"bit0[poe virtual interrupt enable:1|disable:0] bit1[poe id test:1|normal:0] bit2[timer debug enable:1|disable:0] bit3[status print enable:1|disable:0] bit4[OS debug enable:1|disable:0]", 
		"#Enable or disable the POE status polling");
}

portBASE_TYPE 
testing(signed char *buf, size_t len, const signed char * input)
{
	int ret;
	int argc, i;
	char *argv[MAX_NUM_OF_ARGS];
	cmdline_func_t *pcf;
	char tbuf[MAX_ORG_ARG_BUF];

	osal_strncpy(tbuf, (char*)input, sizeof(tbuf));
	tbuf[sizeof(tbuf) - 1] = '\0';

	if((argc = get_argcv_from_str(tbuf, argv, NUMOFARRAY(argv))) <= 0)
	{
		DEBUG_MSG("Error:get_argcv_from_str return %d\n", argc);
	}
	else
	{
		for(i = 0; i < argc; i++)
		{
			DEBUG_MSG("[%02d]:%s\n", i, argv[i]);
		}
	}

	if(argc < 2)
	{
		osal_printf("Parameters is not enough\n");
		testing_help(argv[0]);
		return pdFALSE;
	}

	ret = -1;
	for(i = 0, pcf = &cf[0]; i < NUMOFARRAY(cf); i++, pcf++)
	{
		if(!strncasecmp(argv[1], pcf->cmd, osal_strlen(pcf->cmd)))
		{
			ret = pcf->f(argc - 1, &argv[1]);
			if(ret < 0)
			{
				testing_help(argv[0]);
			}
			return pdFALSE;
		}
	}

	osal_printf("Unknow function %s\n", argv[1]);
    return pdFALSE;
}

void
debug_set(uint32_t code)
{
	osal_printf("Set debug code mask from 0x%08x to 0x%08x\n", debug_code_mask, code);
	debug_code_mask = code;
}

uint32_t
debug_get(void)
{
	int i;
	char *str[DBG_MAX] = 
	{
		[DBG_SAMPLING] = "debug sampling",
		[DBG_STATISTICS] = "debug statistics",
		[DBG_CALENDAR] = "debug calendar",
		[DBG_POE] = "debug POE",

		[DBG_REMOTE_PRINT_ENABLE] = "debug task",
		[DBG_REMOTE_PRINT_DBG_INFO_ENABLE] = "debug timer",
	};

	for(i = 0; i < NUMOFARRAY(str); i++)
	{
		osal_printf("Bit[%d] for %s\n", i, str[i]);
	}

	osal_printf("Get debug code mask is 0x%08x\n", debug_code_mask);
	return debug_code_mask;
}

int
debug_func(int argc, char *argv[])
{
	uint32_t a[1];

	if(argc < 2)
	{
debug_parameter_error:
		osal_printf("Parameters is not enough\n");
		return -1;
	}

	if(!strncasecmp(argv[1], DEBUG_SET_MASK, osal_strlen(DEBUG_SET_MASK)))
	{
		if(argc < 3)
		{
			goto debug_parameter_error;
		}

		a[0] = strtoul(argv[2], NULL, 0);
		debug_set(a[0]);
	}
	else if(!strncasecmp(argv[1], DEBUG_GET_MASK, osal_strlen(DEBUG_GET_MASK)))
	{
		debug_get();
	}
	else
	{
		osal_printf("Unknown command:%s\n", argv[1]);
	}

	return 0;
}


int
poe_detect_chip_type(poe_id_t *pid, uint8_t chip_idx, uint8_t base_port)
{
	int ret;
	uint8_t val, cid, ver;
	poe_chip_type_e pct;
	char *str_chip_type[2] = 
		{
			[POE_CHIP_TYPE_T7604R] = "TMI7604R",
			[POE_CHIP_TYPE_T7608R] = "TMI7608R",
		};

	if(!pid->valid)
	{
		DBG_PRINT("Error:Initialize POE first\n");
		return -1;
	}

	if((ret = poe_reg_read(pid, POE_T760X_ID_REG, &val)) != E_OK)
	{
		DEBUG_MSG("uint:0x%x chn:0x%x dev:0x%x reg:0x%x val:0x%x error return %d\n",
			pid->unit, pid->chn, pid->dev, POE_T760X_ID_REG, val, ret);

		return -2;
	}

	cid = (val >> 3) & 0x1f;
	ver = val & 0x7;
	pct = (cid == POE_CHIP_ID) ? POE_I2C_CHIP_TYPE(pid->dev) : POE_CHIP_TYPE_MAX;
	DEBUG_MSG("Chip ID:0x%x version:0x%x,poe chip type %d,chip is %s\n", 
		cid, ver, pct,
		pct < POE_CHIP_TYPE_MAX ? str_chip_type[pct % NUMOFARRAY(str_chip_type)] : "unknown");

	pid->chip_type = pct;
	switch(pid->chip_type)
	{
		case POE_CHIP_TYPE_T7604R:
			pid->total_port = MAX_PORT_T7604;
			break;

		case POE_CHIP_TYPE_T7608R:
			pid->total_port = MAX_PORT_T7608;
			break;

		default:
			//pid->total_port = -1;
			return -3;
			break;
	}

	pid->chip_idx = chip_idx;
	pid->base_port = base_port;

	return 0;
}

int
poe_detect_chip_all(poe_id_t *pid_start, uint8_t num_pid)
{
	int ret;
	uint8_t i;
	poe_id_t *pid;
	uint8_t total_port;
	int cnt;

	for(i = 0, pid = pid_start, total_port = 0, cnt = 0; i < num_pid; i++, pid++)
	{
		if(!pid->valid)
		{
			continue;
		}

		i2c_init(pid->unit, pid->chn);

		DBG_PRINT("Do detect POE chip index %d\n", i);

		if((ret = poe_detect_chip_type(pid, i, total_port)))
		{
			DBG_PRINT("Error:detect POE chip error on index %d,%d returned\n", i, ret);
			continue;
		}
		total_port += pid->total_port;
		cnt++;
	}

	return cnt;
}

int
poe_chip_init_all(poe_id_t *pid_start, uint8_t num_pid, uint8_t mode, uint8_t hw_pwr_mg)
{
	uint8_t val;
	uint32_t i;
	poe_id_t *pid;

	for(i = 0, pid = pid_start; i < num_pid; i++, pid++)
	{
		if(!pid->valid)
		{
			continue;
		}

		i2c_init(pid->unit, pid->chn);

		DBG_PRINT("Do initialize POE chip index %d\n", i);
		DBG_PRINT("Reg info before initialization\n");
		tm_chip_reg(pid, -1, 0, 0);

		val = (mode << 0) | (mode << 2) | 
			(mode << 4) | (mode << 6);
		poe_reg_write(pid, POE_T760X_MODE_A_REG, val);

		if(pid->chip_type == POE_CHIP_TYPE_T7608R)
		{
			poe_reg_write(pid, POE_T760X_MODE_B_REG, val);
		}

		val = (pid->chip_type == POE_CHIP_TYPE_T7608R) ? 0xff : 0x0f;

		if(mode != MODE_AUTO)
		{
			poe_reg_write(pid, POE_T760X_PWR_OFF_REG, val);
		}
		
		poe_reg_write(pid, POE_T760X_DCD_EN_REG, val);
		poe_reg_write(pid, POE_T760X_DET_EN_REG, val);
		poe_reg_write(pid, POE_T760X_CLS_EN_REG, val);
		poe_reg_write(pid, POE_T760X_MIDSPAN_EN_REG, val);

		poe_reg_write(pid, POE_T760X_EN_CLS4P_REG, val);

		poe_reg_read(pid, POE_T760X_PWR_CFG_REG, &val);
		val &= ~(PWR_CUT_MODE_MASK << 6);
		val |= (PWR_CUT_MODE_BY_LAST_PWR_ON << 6);
		val = hw_pwr_mg ? (val | BITMSK(0)) : (val &  ~ BITMSK(0));
		poe_reg_write(pid, POE_T760X_PWR_CFG_REG, val);

		DBG_PRINT("Reg info after initialization\n");
		tm_chip_reg(pid, -1, 0, 0);
	}

	return 0;
}

int
poe_init_all(uint8_t mode)
{
	poe_id_mgmt_t *ppim = &pim;
	int good;

	if((good = poe_detect_chip_all(&ppim->pid[0], ppim->num)) != ppim->num)
	{
		DBG_PRINT("\nError:!!! not all POE Chip have been detected,"
			"Good POE Chip is %d but all POE Chip is %d\n\n", 
			good, ppim->num);
	}

	poe_chip_init_all(&ppim->pid[0], ppim->num, mode, POE_PWR_MGMT_IN_HW);

	return 0;
}

#if 0
int
poe_pwr_mgmt_pwr_up(poe_port_t **ppp_ret)
{
	uint8_t val;
	uint32_t i, p;
	poe_id_t *pid;
	poe_port_t *ppp;

	for(i = 0, pid = &POE_CHIP[0]; i < POE_CHIP_TOTAL; i++, pid++)
	{
		if(!pid->valid)
		{
			continue;
		}
		
		//DBG_PRINT("Do initialize POE chip index %d\n", i);
		//tm_reg(i, -1, 0, 0);

		for(p = 0; p < pid->total_port; p++)
		{
			ppp = &pid->port[p];

			poe_reg_read(pid, POE_T760X_PORT_STAUTS_REG(ppp->port), &val);
			ppp->cls = (val >> 4) & 0x7;
			ppp->dtc = (val >> 0) & 0x7;

			if(ppp->dtc != DETECT_OK)
			{/* clear counter of power enabled when detection failure */
				ppp->pwr_enabled_cnt = 0;
				ppp->pwr_up_success = 0;
				continue;
			}

			poe_reg_read(pid, POE_T760X_PWR_EN_REG, &ppp->st_en);
			poe_reg_read(pid, POE_T760X_PWR_GOOD_REG, &ppp->st_gd);

			if(ppp->st_en & ppp->st_gd & (1 << ppp->port))
			{/* There is success power up for port */
				if(!ppp->pwr_up_success)
				{
					DBG_PRINT("chip index %d port %d,power up success\n", i, ppp->port);
				}
			
				/* clear counter of power enabled when successful on power up  */
				ppp->pwr_enabled_cnt = 0;
				ppp->pwr_up_success = 1;
				continue;
			}
			else
			{
				ppp->pwr_up_success = 0;
				if(ppp->pwr_enabled_cnt > 6)
				{/* power enabled but no good */
					 continue;
				}
			
				val = (1 << ppp->port);
				poe_reg_write(pid, POE_T760X_PWR_ON_REG, val);

				ppp->pwr_enabled_cnt++;
				*ppp_ret = ppp;

				DBG_PRINT("chip index %d port %d,detect %d class %d,"
					"status mask[enable:0x%x,good:0x%x\n", 
					i, ppp->port, ppp->dtc, ppp->cls, ppp->st_en, ppp->st_gd);

				return 0;
			}
		}
	}

	*ppp_ret = NULL;
	return -1;
}

void 
poe_pwr_mgmt_timer_cb(TimerHandle_t xTimer)
{
	int ret;
	poe_port_t *ppp;
	uint64_t ms;
	uint32_t ticks;

	if(poe_mgmt_val == 1)
	{
		ms = time_current_ms();
		ticks = time_current_tick();

		DBG_PRINT("On %ld ms,ticks %d\n", ms, ticks);
	}

	if(!(ret = poe_pwr_mgmt_pwr_up(&ppp)))
	{
		DBG_PRINT("POE power up chip %d port %d,count %d,success %d\n", 
			ppp->chip, ppp->port, ppp->pwr_enabled_cnt, ppp->pwr_up_success);
	}
}

void
poe_pwr_mgmt_timer_init(void)
{
	uint32_t waitms = 0;

	if(poe_timer_hdl)
	{
		DBG_PRINT("Error:timer already initialized\n");
		return;
	}

	poe_timer_hdl = xTimerCreate(POE_TIMER_NAME, 
				pdMS_TO_TICKS(POE_TIMER_INTERVAL_MS), 
				pdTRUE, 
				NULL, 
				poe_pwr_mgmt_timer_cb);

	if(poe_timer_hdl)
	{
		xTimerStart(poe_timer_hdl, pdMS_TO_TICKS(waitms));
		DBG_PRINT("POE timer started\n");
	}
}

void
poe_pwr_mgmt_timer_stop(void)
{
	uint32_t waitms = 0;

	if(poe_timer_hdl)
	{
		xTimerStop(poe_timer_hdl, pdMS_TO_TICKS(waitms));
		xTimerDelete(poe_timer_hdl, pdMS_TO_TICKS(waitms));
		poe_timer_hdl = NULL;
		DBG_PRINT("POE timer stopped\n");
	}
}
#endif

void
poe_pwr_mgmt_routing(uint8_t en)
{
#if 0
	poe_mgmt_val = en;

	if(en)
	{
		poe_chip_init_all(MODE_SEMIAUTO);
		poe_pwr_mgmt_timer_init();
	}
	else
	{
		poe_pwr_mgmt_timer_stop();
	}
#endif
}

void
timer_debug_start(char *file, int line, const char *func)
{
	uint64_t ms;
	uint32_t ticks;

	if(poe_status_routing_msk & POE_STATUS_TIMER_DBG_EN)
	{
		ms = time_current_ms();
		ticks = time_current_tick();

		DBG_PRINT("\n[[[========== [%s:%d:%s] ==========\n", file, line, func);
		DBG_PRINT("Start call timer at %lld ms,ticks %d\n", ms, ticks);
	}
}

void
timer_debug_quit(char *file, int line, const char *func)
{
	uint64_t ms;
	uint32_t ticks;

	if(poe_status_routing_msk & POE_STATUS_TIMER_DBG_EN)
	{
		ms = time_current_ms();
		ticks = time_current_tick();

		DBG_PRINT("Quit call timer at %lld ms,ticks %d\n", ms, ticks);
		DBG_PRINT("========== [%s:%d:%s] ==========]]]\n\n", file, line, func);
	}
}

void
freertos_debug(char *file, int line, const char *func)
{
	if(poe_status_routing_msk & POE_STATUS_FREERTOS_DBG_EN)
	{
		/* debug info */
		dbg_task_msg_print();
	}
}

void
poe_status_occured_update(poe_id_t *pid, poe_status_occured_t *ppso)
{
	if(!pid || !ppso)
	{
		DBG_PRINT("Error:parameter pid %p or ppso %p invalid\n", pid, ppso);
		return;
	}

	poe_reg_read(pid, POE_T760X_EVENT_PWR_EN_B_REG, &ppso->pwr_en_changed);
	poe_reg_read(pid, POE_T760X_EVENT_PWR_GD_B_REG, &ppso->pwr_good_changed);
	poe_reg_read(pid, POE_T760X_EVENT_DET_DN_B_REG, &ppso->detect_down);
	poe_reg_read(pid, POE_T760X_EVENT_CLS_DN_B_REG, &ppso->class_down);

	poe_reg_read(pid, POE_T760X_EVENT_OVER_CURRENT_B_REG, &ppso->over_current_pwr_on_occur);
	poe_reg_read(pid, POE_T760X_EVENT_PWR_ON_LIMITING_B_REG, &ppso->current_limit_pwr_on_occur);
	poe_reg_read(pid, POE_T760X_EVENT_PWR_UP_LIMITING_B_REG, &ppso->current_limit_pwr_up_occur);
	poe_reg_read(pid, POE_T760X_EVENT_DISCONNECT_B_REG, &ppso->disconnect_occur);

	poe_reg_read(pid, POE_T760X_EVENT_SUPPLY_B_REG, &ppso->misc);
}

void
poe_status_occured_show(poe_status_occured_t *ppso)
{
	uint8_t i, first;
	char *misc_str[8] = 
	{
		[0] = "Reserved",
		[1] = "Reserved",
		[2] = "Request current over remnant",
		[3] = "Total consumed current over MAX",
		[4] = "V54 too low",
		[5] = "V3.3 too low",
		[6] = "FET Bad",
		[7] = "Recover success from temperature over 150",
	};

	if(!ppso)
	{
		DBG_PRINT("Error:parameter ppso %p invalid\n", ppso);
		return;
	}

	POE_STATUS_ROUTING_PRINT("power enable changed:0x%x\n", ppso->pwr_en_changed);
	POE_STATUS_ROUTING_PRINT("power good changed:0x%x\n", ppso->pwr_good_changed);
	POE_STATUS_ROUTING_PRINT("detect down occured:0x%x\n", ppso->detect_down);
	POE_STATUS_ROUTING_PRINT("class down occured:0x%x\n", ppso->class_down);

	POE_STATUS_ROUTING_PRINT("over current at power on occured:0x%x\n", ppso->over_current_pwr_on_occur);
	POE_STATUS_ROUTING_PRINT("current limit at power on occured:0x%x\n", ppso->current_limit_pwr_on_occur);
	POE_STATUS_ROUTING_PRINT("current limit at power up occured:0x%x\n", ppso->current_limit_pwr_up_occur);
	POE_STATUS_ROUTING_PRINT("disconnect occured:0x%x\n", ppso->disconnect_occur);

	for(i = 0, first = 1; i < NUMOFARRAY(misc_str); i++)
	{
		if(ppso->misc & (1 << i))
		{
			if(first)
			{
				POE_STATUS_ROUTING_PRINT("Misc occured:\n");
				first = 0;
			}
			POE_STATUS_ROUTING_PRINT("\t%s\n", misc_str[i]);
		}
	}
}

void
poe_status_now_update(poe_id_t *pid, poe_status_now_t *ppsn)
{
	uint8_t i;
	uint8_t t[2];

	if(!pid || !ppsn)
	{
		DBG_PRINT("Error:parameter pid %p or ppsn %p invalid\n", pid, ppsn);
		return;
	}

	poe_reg_read(pid, POE_T760X_PWR_EN_REG, &ppsn->pwr_en);
	poe_reg_read(pid, POE_T760X_PWR_GOOD_REG, &ppsn->pwr_good);
	for(i = 0; i < POE_MAX_PORT_PER_CHIP; i++)
	{
		poe_reg_read(pid, POE_T760X_PORT_STAUTS_REG(i), &t[0]);
		ppsn->detect[i] = (t[0] >> 0) & 0x7;
		ppsn->class[i] = (t[0] >> 4) & 0x7;
	}

	poe_get_input_voltage_chip(pid, &ppsn->input_vltg);
	poe_get_icut_chip(pid, &ppsn->icut);

	ppsn->fet_bad = 0;
	for(i = 0; i < POE_MAX_PORT_PER_CHIP; i++)
	{
		poe_reg_read(pid, POE_T760X_PORT_HIGH_PWR_ST_REG(i), &t[0]);
		ppsn->fet_bad |= ((t[0] & FET_BAD) ? (1 << i) : 0);
	}

	poe_get_max_current_pwr_chip(pid, &ppsn->max_available_current_total_ua, 
		&ppsn->max_available_power_total_mw);
	poe_get_total_consumed_current_chip(pid, &ppsn->total_consumed_current_ua);
	poe_get_total_consumed_pwr_chip(pid, &ppsn->total_consumed_power_mw);
	poe_get_total_remnant_current_chip(pid, &ppsn->total_remnant_available_current_ua);
	poe_get_total_remnant_pwr_chip(pid, &ppsn->total_remnant_available_power_mw);
	poe_get_pd_req_over_remnant_chip(pid, &ppsn->pd_req_over_remnant);
	poe_get_cvp_chip(pid, &ppsn->cvp[0]);
	poe_get_temp_chip(pid, &ppsn->temp[0]);
}

void
poe_status_now_show(poe_status_now_t *ppsn)
{
	uint8_t i, first;

	if(!ppsn)
	{
		DBG_PRINT("Error:parameter ppsn %p invalid\n", ppsn);
		return;
	}

	POE_STATUS_ROUTING_PRINT("power enable status:0x%x\n", ppsn->pwr_en);
	POE_STATUS_ROUTING_PRINT("power good status:0x%x\n", ppsn->pwr_good);
	for(i = 0, first = 1; i < POE_MAX_PORT_PER_CHIP; i++)
	{
		if(first)
		{
			POE_STATUS_ROUTING_PRINT("detect:");
			first = 0;
		}
		POE_STATUS_ROUTING_PRINT("\t%d", ppsn->detect[i]);
	}
	POE_STATUS_ROUTING_PRINT("\n");
	for(i = 0, first = 1; i < POE_MAX_PORT_PER_CHIP; i++)
	{
		if(first)
		{
			POE_STATUS_ROUTING_PRINT("class:");
			first = 0;
		}
		POE_STATUS_ROUTING_PRINT("\t%d", ppsn->class[i]);
	}
	POE_STATUS_ROUTING_PRINT("\n");

	POE_STATUS_ROUTING_PRINT("chip input voltage:%s %ldmv\n", 
			ppsn->input_vltg.input_v_low ? "Low" : "Normal", 
			ppsn->input_vltg.input_vmv);

	for(i = 0, first = 1; i < POE_MAX_PORT_PER_CHIP; i++)
	{
		if(first)
		{
			POE_STATUS_ROUTING_PRINT("icut:");
			first = 0;
		}
		POE_STATUS_ROUTING_PRINT("\t%ld", ppsn->icut.ua[i]);
	}
	POE_STATUS_ROUTING_PRINT("\n");

	POE_STATUS_ROUTING_PRINT("FET Bad:0x%x\n", ppsn->fet_bad);
	POE_STATUS_ROUTING_PRINT("Max available current total:%ldua\n", ppsn->max_available_current_total_ua);
	POE_STATUS_ROUTING_PRINT("Max available power total:%ldmw\n", ppsn->max_available_power_total_mw);
	POE_STATUS_ROUTING_PRINT("total consumed current:%ldua\n", ppsn->total_consumed_current_ua);
	POE_STATUS_ROUTING_PRINT("total consumed power:%ldmw\n", ppsn->total_consumed_power_mw);
	POE_STATUS_ROUTING_PRINT("total remnant current:%ldua\n", ppsn->total_remnant_available_current_ua);
	POE_STATUS_ROUTING_PRINT("total remnant power:%ldmw\n", ppsn->total_remnant_available_power_mw);
	POE_STATUS_ROUTING_PRINT("POE device request current over remnant:port %d,%ldua\n", 
		ppsn->pd_req_over_remnant.port, ppsn->pd_req_over_remnant.current);

	for(i = 0, first = 1; i < POE_MAX_PORT_PER_CHIP; i++)
	{
		if(first)
		{
			POE_STATUS_ROUTING_PRINT("\tport\tcurrent\tvoltage\tpower\tthermal:\n");
			first = 0;
		}
		POE_STATUS_ROUTING_PRINT("\t%d\t%ld\t%ld\t%ld\t%d.%d\n", 
				i, ppsn->cvp[i].cua, ppsn->cvp[i].vmv, ppsn->cvp[i].pmw, 
				ppsn->temp[i] / 100, ppsn->temp[i] % 100);
	}
}

void
poe_status_func(void)
{
	uint8_t i;
	poe_id_t *pid;
	poe_status_total_t pst, *ppst = &pst;
	poe_id_mgmt_t *ppim = &pim;

	for(i = 0, pid = &ppim->pid[0]; i < ppim->num; i++, pid++)
	{
		if(!pid->valid)
		{
			continue;
		}

		if(CK_POE_ST_RT(POE_STATUS_PRINT_EN))
		{
			poe_status_occured_update(pid, &ppst->occured);
			poe_status_now_update(pid, &ppst->now);

			//POE_STATUS_ROUTING_PRINT("\n");
			POE_STATUS_ROUTING_PRINT("Chip index %d,device 0x%x\n", i, pid->dev);
			POE_STATUS_ROUTING_PRINT("Size of poe_status_total_t is %ld\n", sizeof(poe_status_total_t));

			poe_status_occured_show(&ppst->occured);
			poe_status_now_show(&ppst->now);
		}
	}
}

void 
poe_status_timer_cb(TimerHandle_t xTimer)
{
	TIMER_DEBUG_START();
	FREERTOS_DEBUG();

	poe_status_func();

	FREERTOS_DEBUG();
	TIMER_DEBUG_QUIT();
}

void
poe_status_timer_init(void)
{
	uint32_t waitms = 0;

	if(poe_status_timer_hdl)
	{
		DBG_PRINT("Error:status timer already initialized\n");
		return;
	}

	poe_status_timer_hdl = xTimerCreate(POE_STATUS_TIMER_NAME, 
				pdMS_TO_TICKS(POE_STATUS_TIMER_INTERVAL_MS), 
				pdTRUE, 
				NULL, 
				poe_status_timer_cb);

	if(poe_status_timer_hdl)
	{
		xTimerStart(poe_status_timer_hdl, pdMS_TO_TICKS(waitms));
		DBG_PRINT("POE status timer started\n");
	}
	else
	{
		DBG_PRINT("Create POE status timer fail\n");
	}
}

void
poe_status_timer_stop(void)
{
	uint32_t waitms = 0;

	if(poe_status_timer_hdl)
	{
		xTimerStop(poe_status_timer_hdl, pdMS_TO_TICKS(waitms));
		xTimerDelete(poe_status_timer_hdl, pdMS_TO_TICKS(waitms));
		poe_status_timer_hdl = NULL;
		DBG_PRINT("POE status timer stopped\n");
	}
}

/*
	Function Name poe_status_routing
	Descript:for virtual interrupt and show POE status in period
	Argument poe_status_routing_e en:
		bit0:[poe virtual interrupt enable:1|disable:0] 
		bit1:[poe id test:1|normal:0] 
		bit2:[timer debug enable:1|disable:0] 
		bit3:[status print enable:1|disable:0] 
		bit4:[OS debug enable:1|disable:0]
*/
void
poe_status_routing(poe_status_routing_e en)
{
	poe_id_mgmt_t *ppim = &pim;
	static uint8_t status_on = 0;

	poe_status_routing_msk = en;

	if(en & POE_STATUS_POE_ID_TEST)
	{
		ppim->pid = &poeid[0];
		ppim->num = NUMOFARRAY(poeid);
	}
	else
	{
		ppim->pid = &POE_CHIP[0];
		ppim->num = POE_CHIP_TOTAL;
	}

	if(en & POE_STATUS_ON)
	{
		if(!status_on)
		{
			DBG_PRINT("Turn ON the POE function\n");

			poe_init_all(MODE_AUTO);
			poe_status_timer_init();
			status_on = 1;
		}
		else
		{
			DBG_PRINT("POE function already ON\n");
		}
	}
	else
	{
		if(status_on)
		{
			DBG_PRINT("Turn OFF the POE function\n");

			poe_status_timer_stop();
			status_on = 0;
		}
		else
		{
			DBG_PRINT("POE function already OFF\n");
		}
	}
}

void
fan_gpio_set(uint8_t gpio_pin, uint8_t high)
{
	int rc;

	rc = air_gpio_setValue(gpio_pin, !!high);
	if(E_OK != rc)
	{
		printf("***Error***, set pin %u state error\n", gpio_pin);
	}
}

int
fan_setting(uint8_t idx, fan_set_status_e status)
{
	fan_gpio_t fg[MAX_NUM_FAN] = 
	{
		[0] = {.gpio_on = FAN_1_GPIO_ON_OFF, .gpio_lvl = FAN_1_GPIO_LEVEL,},
		[1] = {.gpio_on = FAN_2_GPIO_ON_OFF, .gpio_lvl = FAN_2_GPIO_LEVEL,},
	};
	fan_gpio_t *pfg;

	if(idx >= MAX_NUM_FAN)
	{
		DBG_PRINT("Error:idx is %d,but MAX number of fan are %d\n", idx, MAX_NUM_FAN);
		return -1;
	}

	pfg = &fg[idx];
	switch(status)
	{
		case FAN_SET_OFF:
			fan_gpio_set(pfg->gpio_on, FAN_GPIO_SET_OFF);
			DBG_PRINT("Set fan %d %s success\n", idx, "OFF");
			break;

		case FAN_SET_ON_LVL_1:
			fan_gpio_set(pfg->gpio_on, FAN_GPIO_SET_ON);
			fan_gpio_set(pfg->gpio_lvl, FAN_GPIO_SET_ON_LVL_1);
			DBG_PRINT("Set fan %d %s success\n", idx, "ON level 1");
			break;

		case FAN_SET_ON_LVL_2:
			fan_gpio_set(pfg->gpio_on, FAN_GPIO_SET_ON);
			fan_gpio_set(pfg->gpio_lvl, FAN_GPIO_SET_ON_LVL_2);
			DBG_PRINT("Set fan %d %s success\n", idx, "ON level 2");
			break;

		default:
			DBG_PRINT("Error:unknown command %d\n", idx, status);
			break;
	}

	return 0;
}

int
fan_init(void)
{
	uint8_t i;
	uint8_t unit;
	int rc;
	uint8_t function[4] = 
	{
		AIR_CHIPSCU_IOMUX_FORCE_GPIO5_MODE,
		AIR_CHIPSCU_IOMUX_FORCE_GPIO13_MODE,
		AIR_CHIPSCU_IOMUX_FORCE_GPIO15_MODE,
		AIR_CHIPSCU_IOMUX_FORCE_GPIO17_MODE,
	};
	uint8_t pin[4] = 
	{
		FAN_1_GPIO_ON_OFF,
		FAN_1_GPIO_LEVEL,
		FAN_2_GPIO_ON_OFF,
		FAN_2_GPIO_LEVEL,
	};

	unit = 0;
	for(i = 0; i < NUMOFARRAY(function); i++)
	{
	    rc = air_chipscu_setIomuxFuncState(unit, function[i], 1);
		if(E_OK != rc)
		{
			DBG_PRINT("***Error***, set function %u state error\n", function[i]);
			return -1;
		}

		rc = air_gpio_setDirection(pin[i], AIR_GPIO_DIRECTION_OUTPUT);
		if(E_OK != rc)
		{
			DBG_PRINT("***Error***, set pin %u direction error\n", pin[i]);
			return -2;
		}

		rc = air_gpio_setOutputEnable(pin[i], 1);
		if(E_OK != rc)
		{
			osal_printf("***Error***, set pin %u output enable state error\n", pin[i]);
			return -3;
		}
	}

	return 0;
}

int
fan_func(int argc, char *argv[])
{
	uint8_t idx;
	fan_set_status_e status;
	uint32_t a[2];
	static uint8_t inited = 0;

	if(argc < 3)
	{
		osal_printf("Parameters is not enough\n");
		return -1;
	}

	if(!inited)
	{
		fan_init();
		inited = 1;
	}
	
	a[0] = strtoul(argv[1], NULL, 0);
	a[1] = strtoul(argv[2], NULL, 0);

	idx = a[0];
	status = a[1];

	return fan_setting(idx, status);
}

int
test_func(int argc, char *argv[])
{
	uint32_t a[3];
	int ret;
	uint8_t idx;

	if(argc < 2)
	{
		osal_printf("Parameters is not enough\n");
		return -1;
	}

	a[0] = strtoul(argv[1], NULL, 0);

	switch(a[0])
	{
		case 0:
			ret = poe_power_off_btn_trigger_non_auto_all();
			DEBUG_MSG("call poe_power_off_btn_trigger_non_auto_all() return %d\n", ret);
			break;

		case 1:
			ret = poe_power_on_btn_trigger_non_auto_all();
			DEBUG_MSG("call poe_power_on_btn_trigger_non_auto_all() return %d\n", ret);
			break;

		case 2:
			for(idx = 1; idx <= 24; idx++)
			{
				ret = poe_power_off_btn_trigger_non_auto_user_port(idx);
				DEBUG_MSG("call poe_power_off_btn_trigger_non_auto_user_port(%d) "
							"return %d\n", idx, ret);
			}
			break;

		case 3:
			for(idx = 1; idx <= 24; idx++)
			{
				ret = poe_power_on_btn_trigger_non_auto_user_port(idx);
				DEBUG_MSG("call poe_power_on_btn_trigger_non_auto_user_port(%d) "
							"return %d\n", idx, ret);
			}
			break;

		case 4:
			if(argc < 4)
			{
				osal_printf("Parameters is not enough\n");
				return -1;
			}

			a[1] = strtoul(argv[2], NULL, 0);
			a[2] = strtoul(argv[3], NULL, 0);

			show_perpheral_reg(a[1], a[2]);
			break;

		case 5:
			if(argc < 4)
			{
				osal_printf("Parameters is not enough\n");
				return -1;
			}

			a[1] = strtoul(argv[2], NULL, 0);
			a[2] = strtoul(argv[3], NULL, 0);

			show_mem(a[1], a[2]);
			break;

		case 6:
			os_print_critical();
			dbg_task_msg_print();
			dbg_task_print();
			dbg_task_runtime_print();
			break;

		case 7:
			waiting_for_wd();
			break;

		case 8:
			DUMP_STACK();
			break;

		default:
			osal_printf("Unknown parameter %d\n", a[0]);
			break;
	}

	return 0;
}

#if defined(KEY_GPIO_API_MODE) && KEY_GPIO_API_MODE
int
key_gpio_init(void)
{
	uint8_t i;
	uint8_t unit;
	int rc;
	uint8_t function[] = 
	{
		KEY_1_GPIO_IOMUX,
		KEY_2_GPIO_IOMUX,
	};
	uint8_t pin[] = 
	{
		KEY_1_GPIO_ID,
		KEY_2_GPIO_ID,
	};

	unit = 0;
	for(i = 0; i < NUMOFARRAY(function); i++)
	{
	    rc = air_chipscu_setIomuxFuncState(unit, function[i], 1);
		if(E_OK != rc)
		{
			DBG_PRINT("***Error***, set function %u state error\n", function[i]);
			return -1;
		}

		rc = air_gpio_setDirection(pin[i], AIR_GPIO_DIRECTION_INPUT);
		if(E_OK != rc)
		{
			DBG_PRINT("***Error***, set pin %u direction error\n", pin[i]);
			return -2;
		}
	}

	return 0;	
}

void
key_gpio_get(uint8_t gpio_pin, uint8_t *status)
{
	int rc;
	int val;

	rc = air_gpio_getValue(gpio_pin, &val);
	if(E_OK != rc)
	{
		printf("***Error***, get pin %u state error\n", gpio_pin);
	}
	*status = !!val;
}
#else
int
key_get_gpio_reg(UI32_T gid, gpio_reg_t *pgr)
{
	if(gid > 20 || !pgr)
	{
		return -1;
	}

	pgr->reg_mux = 0x1000007c;
	pgr->reg_get = 0x1000a304;

	if(gid < 16)
	{
		pgr->reg_mode = 0x1000a300;
		pgr->idx_mode = gid;
	}
	else
	{
		pgr->reg_mode = 0x1000a320;
		pgr->idx_mode = gid % 16;
	}

	return 0;
}

void
key_set_gpio_input_mode(UI32_T gid)
{
	UI32_T data = 0;
	gpio_reg_t gr, *pgr = &gr;

	key_get_gpio_reg(gid, pgr);

	data = io_read32(pgr->reg_mux);
	data |= (1 << gid);
	io_write32(pgr->reg_mux, data);

	data = io_read32(pgr->reg_mode);
	data &= ~(0x3 << (pgr->idx_mode * 2));
	io_write32(pgr->reg_mode, data);
}

int
key_gpio_init(void)
{
	uint8_t i;
	uint8_t pin[] = 
	{
		KEY_1_GPIO_ID,
		KEY_2_GPIO_ID,
	};

	for(i = 0; i < NUMOFARRAY(pin); i++)
	{
		key_set_gpio_input_mode((UI32_T)pin[i]);
	}

	return 0;	
}

void
key_gpio_get(uint8_t gpio_pin, uint8_t *status)
{
	UI32_T data = 0;
	gpio_reg_t gr, *pgr = &gr;

	key_get_gpio_reg((UI32_T)gpio_pin, pgr);

	data = io_read32(pgr->reg_get);
	*status = !!(data & (1 << gpio_pin));
}
#endif
int
key_init(void)
{
	return key_gpio_init();
}

uint8_t
key_get_status(void)
{
	uint8_t st[2];

	key_gpio_get(KEY_1_GPIO_ID, &st[0]);
	key_gpio_get(KEY_2_GPIO_ID, &st[1]);
	return (st[1] << 1) | (st[0] << 0);
}

void 
key_timer_cb(TimerHandle_t xTimer)
{
	osal_printf("Key status:0x%x\n", key_get_status());
}

void
key_timer_init(void)
{
	uint32_t waitms = 0;

	if(key_timer_hdl)
	{
		DBG_PRINT("Error:key timer already initialized\n");
		return;
	}

	key_timer_hdl = xTimerCreate(KEY_TIMER_NAME, 
				pdMS_TO_TICKS(KEY_TIMER_INTERVAL_MS), 
				pdTRUE, 
				NULL, 
				key_timer_cb);

	if(key_timer_hdl)
	{
		xTimerStart(key_timer_hdl, pdMS_TO_TICKS(waitms));
		DBG_PRINT("KEY timer started\n");
	}
}

void
key_timer_stop(void)
{
	uint32_t waitms = 0;

	if(key_timer_hdl)
	{
		xTimerStop(key_timer_hdl, pdMS_TO_TICKS(waitms));
		xTimerDelete(key_timer_hdl, pdMS_TO_TICKS(waitms));
		key_timer_hdl = NULL;
		DBG_PRINT("key timer stopped\n");
	}
}

int
key_func(int argc, char *argv[])
{
	uint32_t a[1];
	static uint8_t inited = 0;

	if(argc < 2)
	{
		osal_printf("Parameters is not enough\n");
		return -1;
	}

	if(!inited)
	{
		key_gpio_init();
		inited = 1;
	}
	
	a[0] = strtoul(argv[1], NULL, 0);

	if(a[0])
	{
		key_timer_init();
	}
	else
	{
		key_timer_stop();
	}

	return 0;
}

void
led_port_setting(uint8_t port, led_set_status_e st)
{
	uint8_t unit, led_id;
	AIR_PORT_PHY_LED_CTRL_MODE_T mode;
	AIR_PORT_PHY_LED_STATE_T state;
	AIR_PORT_PHY_LED_PATT_T pattern;

	unit = 0;
	led_id = 0;
	pattern = AIR_PORT_PHY_LED_PATT_HZ_HALF;
	switch(st)
	{
		case LED_SET_OFF:
			state = AIR_PORT_PHY_LED_STATE_OFF;
			mode = AIR_PORT_PHY_LED_CTRL_MODE_FORCE;
			break;

		case LED_SET_ON:
			state = AIR_PORT_PHY_LED_STATE_ON;
			mode = AIR_PORT_PHY_LED_CTRL_MODE_FORCE;
			break;

		case LED_SET_FLASH:
			state = AIR_PORT_PHY_LED_STATE_FORCE_PATT;
			mode = AIR_PORT_PHY_LED_CTRL_MODE_FORCE;
			break;

		case LED_SET_NORMAL:
			state = AIR_PORT_PHY_LED_STATE_LAST;
			mode = AIR_PORT_PHY_LED_CTRL_MODE_PHY;
			break;

		default:
			DBG_PRINT("Error:unknown command %d\n", st);
			return;
			break;
	}

	air_port_setPhyLedCtrlMode((UI32_T)unit, (UI32_T)port, (UI32_T)led_id, mode);
	air_port_setPhyLedForceState((UI32_T)unit, (UI32_T)port, (UI32_T)led_id, state);
	air_port_setPhyLedForcePattCfg((UI32_T)unit, (UI32_T)port, (UI32_T)led_id, pattern);
	DBG_PRINT("Set port %d led status %d success\n", port, st);
}

void 
led_timer_cb(TimerHandle_t xTimer)
{
	uint8_t i;
	led_set_status_e *plss;
	static led_set_status_e lst = LED_SET_OFF;
	
	TIMER_DEBUG_START();
	FREERTOS_DEBUG();

	osal_printf("Key status:%d\n", key_get_status());

	for(i = 0, plss = &led_st[0]; i < NUMOFARRAY(led_st); i++, plss++)
	{
		//DBG_PRINT("%s led[%d] status %d\n", __FUNCTION__, i, *plss);
	
		if(*plss == LED_SET_FLASH)
		{
			led_hw_setting(i, lst);
		}
	}
	lst = ((lst == LED_SET_OFF) ? LED_SET_ON : LED_SET_OFF);

	FREERTOS_DEBUG();
	TIMER_DEBUG_QUIT();
}

void
led_timer_init(void)
{
	uint32_t waitms = 0;

	if(led_timer_hdl)
	{
		DBG_PRINT("Error:led timer already initialized\n");
		return;
	}

	led_timer_hdl = xTimerCreate(LED_TIMER_NAME, 
				pdMS_TO_TICKS(LED_TIMER_INTERVAL_MS), 
				pdTRUE, 
				NULL, 
				led_timer_cb);

	if(led_timer_hdl)
	{
		xTimerStart(led_timer_hdl, pdMS_TO_TICKS(waitms));
		DBG_PRINT("LED timer started\n");
	}
}

void
led_timer_stop(void)
{
	uint32_t waitms = 0;

	if(led_timer_hdl)
	{
		xTimerStop(led_timer_hdl, pdMS_TO_TICKS(waitms));
		xTimerDelete(led_timer_hdl, pdMS_TO_TICKS(waitms));
		led_timer_hdl = NULL;
		DBG_PRINT("led timer stopped\n");
	}
}

void
led_gpio_set(uint8_t gpio_pin, uint8_t high)
{
	fan_gpio_set(gpio_pin, high);
}

int
led_hw_setting(uint8_t idx, led_set_status_e status)
{
	led_gpio_t lg[MAX_NUM_GPIO_LED] = 
	{
		[0] = {.gpio = LED_1_GPIO_ON_OFF,},
		[1] = {.gpio = LED_2_GPIO_ON_OFF,},
	};
	led_gpio_t *plg;

	if(idx >= MAX_NUM_GPIO_LED)
	{
		DBG_PRINT("Error:idx is %d,but MAX number of led are %d\n", idx, MAX_NUM_GPIO_LED);
		return -1;
	}

	plg = &lg[idx];
	switch(status)
	{
		case LED_SET_OFF:
			led_gpio_set(plg->gpio, LED_GPIO_SET_OFF);
			DBG_PRINT("Set led %d %s success\n", idx, "OFF");
			break;

		case LED_SET_ON:
			led_gpio_set(plg->gpio, LED_GPIO_SET_ON);
			DBG_PRINT("Set led %d %s success\n", idx, "ON");
			break;

		default:
			DBG_PRINT("Error:unknown command %d\n", status);
			break;
	}

	return 0;
}

int
led_sw_setting(uint8_t idx, led_set_status_e status)
{
	if(idx >= MAX_NUM_GPIO_LED)
	{
		DBG_PRINT("Error:idx is %d,but MAX number of led are %d\n", idx, MAX_NUM_GPIO_LED);
		return -1;
	}

	switch(status)
	{
		case LED_SET_OFF:
		case LED_SET_ON:
			led_st[idx] = status;
			led_hw_setting(idx, status);
			break;

		case LED_SET_FLASH:
			led_st[idx] = status;
			led_timer_init();
			DBG_PRINT("Set led %d %s success\n", idx, "FLASH");
			break;

		case LED_SET_TIMER_STOP:
			led_st[idx] = status;
			led_timer_stop();
			DBG_PRINT("Set led %d %s success\n", idx, "Timer Stop");
			break;

		default:
			DBG_PRINT("Error:unknown command %d\n", status);
			break;
	}

	return 0;
}

int
led_init(void)
{
	uint8_t i;
	uint8_t unit;
	int rc;
	uint8_t function[] = 
	{
		AIR_CHIPSCU_IOMUX_FORCE_GPIO0_MODE,
		AIR_CHIPSCU_IOMUX_FORCE_GPIO6_MODE,
	};
	uint8_t pin[] = 
	{
		LED_1_GPIO_ON_OFF,
		LED_2_GPIO_ON_OFF,
	};

	unit = 0;
	for(i = 0; i < NUMOFARRAY(function); i++)
	{
	    rc = air_chipscu_setIomuxFuncState(unit, function[i], 1);
		if(E_OK != rc)
		{
			DBG_PRINT("***Error***, set function %u state error\n", function[i]);
			return -1;
		}

		rc = air_gpio_setDirection(pin[i], AIR_GPIO_DIRECTION_OUTPUT);
		if(E_OK != rc)
		{
			DBG_PRINT("***Error***, set pin %u direction error\n", pin[i]);
			return -2;
		}

		rc = air_gpio_setOutputEnable(pin[i], 1);
		if(E_OK != rc)
		{
			osal_printf("***Error***, set pin %u output enable state error\n", pin[i]);
			return -3;
		}
	}

	return 0;
}

int
led_func(int argc, char *argv[])
{
	uint8_t idx, port, i;
	led_set_status_e status;
	uint32_t a[2];
	static uint8_t inited = 0;

	if(argc < 3)
	{
		osal_printf("Parameters is not enough\n");
		return -1;
	}

	if(!inited)
	{
		led_init();
		inited = 1;
	}
	
	a[0] = strtoul(argv[1], NULL, 0);
	a[1] = strtoul(argv[2], NULL, 0);

	idx = a[0];
	status = a[1];

	if(idx < MAX_NUM_GPIO_LED)
	{/* gpio led */
		led_sw_setting(idx, status);
	}
	else if(idx < MAX_NUM_LED)
	{
		port = GET_SWITCH_PORT_LED(idx);
		led_port_setting(port, status);
	}
	else
	{
		for(i = 0; i < MAX_NUM_GPIO_LED; i++)
		{
			led_sw_setting(i, status);
		}

		for(i = MAX_NUM_GPIO_LED; i < MAX_NUM_LED; i++)
		{
			port = GET_SWITCH_PORT_LED(i);
			led_port_setting(port, status);
		}		
	}

	return 0;
}


