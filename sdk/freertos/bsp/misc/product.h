
#ifndef __PRODUCT_H__
#define __PRODUCT_H__

/* This is for TM7604|TM7608,max poe power less than limitation */
#define MAX_POE_POWER_PLUS_W			(0)

#define SIF_TESTING_ENABLE				(0)

#define LED_OP_IN_CRITICAL				(1)

#if defined(SKU_2149)
#define MAX_POE_POWER_MW					(((65) + MAX_POE_POWER_PLUS_W) * 1000)
#define USABLE_POE_POWER_MW					(((60) + MAX_POE_POWER_PLUS_W) * 1000)
#define POE_PWR_CALIBRATION_MW_PER_10W		(4 * 100)				/* 400mw */
#elif defined(SKU_2150)
#define MAX_POE_POWER_MW					(((65) + MAX_POE_POWER_PLUS_W) * 1000)
#define USABLE_POE_POWER_MW					(((60) + MAX_POE_POWER_PLUS_W) * 1000)
#define POE_PWR_CALIBRATION_MW_PER_10W		(4 * 100)				/* 400mw */
#elif defined(SKU_2151)
#if 0
#define MAX_POE_POWER_MW					(((9) + MAX_POE_POWER_PLUS_W) * 1000)
#define USABLE_POE_POWER_MW					(((5) + MAX_POE_POWER_PLUS_W) * 1000)
#else
#define MAX_POE_POWER_MW					(((120) + MAX_POE_POWER_PLUS_W) * 1000)
#define USABLE_POE_POWER_MW					(((115) + MAX_POE_POWER_PLUS_W) * 1000)
#endif
#define POE_PWR_CALIBRATION_MW_PER_10W		(0 * 100)				/* 000mw */
#else
#error "Must define 2149 or 2150 or 2151 !!!"
#endif

#define POE_INTV_BTEN_DN_2_UP_IN_RST_MS		(500)	/* Interval ms between 
													power down to up in reseting */

#define POE_PWR_10W_MW						(10 * 1000)				/* 10000mw */

#define POE_OVERLOAD_INTERVAL_MS			(1 * configTICK_RATE_HZ)	/* 1s */
#define POE_PWR_ON_INTERVAL_MS				(1 * configTICK_RATE_HZ)	/* 1s */
#define POE_COUPLED_PWR_ON_INTERVAL_MS		(3 * configTICK_RATE_HZ)	/* 3s */

#define POE_POWER_UPDATE_INTERVAL_MS		(2 * configTICK_RATE_HZ)	/* 2s */

#if defined(SIF_TESTING_ENABLE) && SIF_TESTING_ENABLE
#define TEST_SIF_TASK_MAX_NUM				(10)
#define TEST_SIF_TASK_SIZE_STACK			(512)
#define TEST_SIF_TASK_PERIOD_MS				(20)						/* 50ms */
#define TEST_SIF_TASK_NAME					"Test SIF"
#define TEST_SIF_TASK_PRIORITY				(2)//(configMAX_PRIORITIES - 1)
#else
#define TEST_SIF_TASK_MAX_NUM				(0)
#endif

extern uint32_t LMA_BEGIN_FLASH;
extern uint32_t LMA_END_FLASH;
extern unsigned char g_slave_detected;

extern TaskHandle_t user_taskHandle;
extern TaskHandle_t pse_taskHandle;
#ifdef SKU_2150
extern TaskHandle_t sfp_taskHandle;
#endif
extern TaskHandle_t mischdl_taskHandle;
#if defined(SIF_TESTING_ENABLE) && SIF_TESTING_ENABLE
extern TaskHandle_t test_sif_task_hdl[TEST_SIF_TASK_MAX_NUM];
#endif

#if defined(SKU_2150) || defined(SKU_2151)
#define PCB2151_LED_POLARITY_SWITCH		(1)
#define PCB2151_KEY_POLARITY_SWITCH		(1)
#endif

/*
	PCB 2150/2151 master mode,function of uart Rx(GPIO0) is Port0 LED0 mode or uart Rx mode,it's control by MACRO UART_ENABLE;
	PCB 2150/2151 master mode,function of uart Tx(GPIO1) is GPIO mode or uart Tx mode,it's control by MACRO UART_ENABLE;
	PCB 2150/2151 slave mode,function of uart Rx(GPIO0) is Port0 LED0 mode or uart Rx mode,it's control by MACRO UART_ENABLE;
	PCB 2150/2151 slave mode,function of uart Tx(GPIO1) isn't control by MACRO UART_ENABLE,still as uart Tx mode

	PCB 2149 master mode,function of uart Rx(GPIO0) is I2C master SCL mode or uart Rx mode,it's control by MACRO UART_ENABLE;
	PCB 2149 master mode,function of uart Tx(GPIO1) is I2C master SDA mode or uart Tx mode,it's control by MACRO UART_ENABLE;
	PCB 2149 slave mode,function of uart Rx(GPIO0) isn't control by MACRO UART_ENABLE,still as uart Rx mode;
	PCB 2149 slave mode,function of uart Tx(GPIO1) isn't control by MACRO UART_ENABLE,still as uart Tx mode;
*/

#define UART_ENABLE						(0)
#define CONVERSATION_ENABLE				(0)
#define CONVERSATION_TESTING_ENABLE		(0)
#define REMOTE_UART_ENABLE				(1)
#define REMOTE_UART_TESTING_ENABLE		(1)

#define SLAVE_TEST_GPIO					(0)
#if defined(SLAVE_TEST_GPIO) && SLAVE_TEST_GPIO
#define SLAVE_TEST_GPIO_INTERVAL_MS		(1 * configTICK_RATE_HZ)
#define SLAVE_TEST_GPIO_ARRAY			{0, 6, 7, 8}
#endif

#if defined(CONVERSATION_ENABLE) && CONVERSATION_ENABLE
#define INTERVIEW_ADDR		(0x1000501c)
#endif

#define MAGIC_ID_MSG		(0x12438756)
#define MAX_SZ_MSG			(64 + 32)

typedef enum
{
	MSG_TYPE_UART = 0,
	MSG_TYPE_MAX,
}msg_type_e;

typedef struct
{
	uint32_t mgc;	/* Magic Number for ID */
	uint8_t msgt;	/* Type of Message,accroding to msg_type_e */
	uint8_t idx;	/* Index of message */
	uint8_t sz;		/* Size of data buffer */
	uint8_t reserved;
	uint8_t buf[MAX_SZ_MSG];	/* Data buffer */
}interaction_t;

#if defined(REMOTE_UART_ENABLE) && REMOTE_UART_ENABLE
#define MAX_REMOTE_UART_MSG_NUMBER				(15)
interaction_t rmt_uart[MAX_REMOTE_UART_MSG_NUMBER];
#endif

typedef struct
{
	void *start, *end;
	uint32_t size;
}img_info_t;

/* NAMING CONSTANT DECLARATIONS
*/
#if defined(SKU_2149)
#define DIP_STATE_NORMAL        2
#define DIP_STATE_PSE_WATCHDOG  0
#define DIP_STATE_10M           1
#elif defined(SKU_2150) || defined(SKU_2151)
	#if defined(PCB2151_KEY_POLARITY_SWITCH) && PCB2151_KEY_POLARITY_SWITCH
		#if 1
		/* product defination */
		#define DIP_STATE_NORMAL		(1 ^ 3)
		#define DIP_STATE_PSE_WATCHDOG	(0 ^ 3)
		#define DIP_STATE_10M			(2 ^ 3)
		#else
		/* old board defination */
		#define DIP_STATE_NORMAL        (0 ^ 3)
		#define DIP_STATE_PSE_WATCHDOG  (1 ^ 3)
		#define DIP_STATE_10M           (2 ^ 3)
		#endif
	#else
	#define DIP_STATE_NORMAL        0
	#define DIP_STATE_PSE_WATCHDOG  1
	#define DIP_STATE_10M           2
	#endif
#else
#error "Must define 2149 or 2150 or 2151 !!!"
#endif

#if 1	/* code from yek */

#define NUM_OF_CHIP				(1)
#ifdef SKU_2151
#define NUM_OF_PORT				(8)
#else
#define NUM_OF_PORT				(4)
#endif

#define TIME_UNIT				(60 * configTICK_RATE_HZ)	/* TIME_UNIT is 1m,it has 60s,it has 60 * HZ_per_tick */
#define MAX_WAITING_AFTER_RESET	(10 * configTICK_RATE_HZ)	/* 10s */

enum PoE_Status
{
	NO_POWER = 0,
	PowerOn_Link_Down,
	PowerOn_Link_Up,
	RESET,
};

typedef struct
{
	uint32_t tick;
	uint8_t poe_en_good[NUM_OF_CHIP];
}pse_wd_spcl_args_t;
#endif

typedef enum
{
	GPIO_PIN_DIRECTION_INPUT = 0,
	GPIO_PIN_DIRECTION_OUTPUT,
	GPIO_PIN_DIRECTION_MAX,
}GPIO_PIN_DIRECTION_E;

void serial_outc(char c);
unsigned int crc32buf(unsigned char *buf, unsigned int len);
AIR_ERROR_NO_T _sif_read_slave_8855M_block
	(uint32_t start, void *p_data, uint32_t size/* size bigger than 4B */);
AIR_ERROR_NO_T _sif_write_slave_8855M_block
	(uint32_t start, void *p_data, uint32_t size/* size bigger than 4B */);
uint32_t get_img_info(img_info_t *pii);
uint32_t get_img_crc(void);
int system_inspection(void);
void test_code(void);
void serial_puts(char *s);
void serial_put_hex_4(uint8_t val);
void serial_put_hex_32(uint32_t val);
int remote_uart_master_tx(char *str, uint8_t sz/* size include '\0' */);
int remote_uart_slave_rx(char **ppstr, uint8_t *psz/* size include '\0' */, 
	uint8_t *pidx , uint8_t *poidx);
void remote_uart_print(const char *ptr_fmt, ...);
void remote_uart_master_test(void);
void remote_uart_slave_test(void);
void conversation(void);
int dynamic_poe_pwr_limitation_demo(poe_hw_port_t *pphp);
void poe_priority_dynaimc_pwr_limitation_init(void);
uint32_t poe_pwr_calibration(uint32_t pmw_orig);
void poe_overload_thread(void);
void poe_power_on_thread(void);
uint8_t Check_POE_Status(uint8_t poe_idx, uint8_t port
#if 1		/* wy,debug */
	, pse_wd_spcl_args_t *ppwsa
#endif
);
uint8_t Read_Link_Status(uint8_t poe_idx, uint8_t port);
uint32_t Read_Port_Pack(uint8_t poe_idx, uint8_t port);
int POE_reset(uint8_t poe_idx, uint8_t port);
void POE_WatchDog_Init(void);
void POE_WatchDog(uint8_t Smi, uint8_t Port
#if 1		/* wy,debug */
	, pse_wd_spcl_args_t *ppwsa
#endif
);
void POE_WatchDog_Mode(void);
void _MiscHdl_AppTask(void *p);
void customer_createMiscHdlTask(void);
void misc_hdl_master(void);
void misc_hdl_slave(void);
void dump_heap_info(void);
int gpio_pin_set_func_gpio(int gidx, int en);
int gpio_pin_set_direction(int gidx, uint8_t dir);
int gpio_pin_setting(int gidx, uint32_t val);
int gpio_pin_getting(int gidx, uint32_t *val);
void reg_show(uint32_t start, uint32_t size);
void test_sif_loop(void);
#if defined(SIF_TESTING_ENABLE) && SIF_TESTING_ENABLE
void test_sif_task_app(void *p);
void test_sif_task_create();
void test_sif_task_init(void);
#else
void test_sif_task_init(void);
#endif
void show_task_stack_detail(TaskHandle_t ptsk);
void show_task_stack_info(TaskHandle_t ptsk);
void show_all_task_stack_info(void);
AIR_ERROR_NO_T _sif_read_slave_PSE_reg
	(unsigned char addr, unsigned char reg, unsigned char *p_data);
AIR_ERROR_NO_T _sif_write_slave_PSE_reg
	(unsigned char addr, unsigned char reg, unsigned char data);
AIR_ERROR_NO_T _sif_read_slave_8855M_reg(unsigned int reg, unsigned int *p_data);
AIR_ERROR_NO_T _sif_write_slave_8855M_reg(unsigned int reg, unsigned int data);
unsigned int _get_master_8855M_port_link(unsigned int port);
#ifdef SKU_2151
unsigned int _get_slave_8855M_port_link(unsigned int port);
#endif
unsigned int _get_master_8855M_port_rx_pkt(unsigned int port);
#ifdef SKU_2151
unsigned int _get_slave_8855M_port_rx_pkt(unsigned int port);
#endif

size_t xPortGetHeapUsableBytes();

#endif

