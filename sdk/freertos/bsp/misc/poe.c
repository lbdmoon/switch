
/* Standard includes. */
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>

/* Kernel includes. */
#include <FreeRTOS.h>
#include "FreeRTOSConfig.h"
#include "task.h"
#include "timers.h"
#include "queue.h"
#include "osal.h"

/* module includes */
#include <os_func.h>
#include <air_i2c.h>
#include <i2c_cmd.h>
#include <pp_def.h>
#include <air_l2.h>

#include <list_linux.h>
#include <os_func.h>
#include <i2c.h>
#include <poe.h>
#include <test.h>


#if 0
poe_id_t poe_id_all[POE_MAX_CURRENT_CHIP] = 
{
	[0] = {
			.valid = 1,
			.unit = 0,
			.chn = 0,
			.dev = 0x73,			/* PCB U3 */
			.chip_idx = 0,
			.base_port = 0 * POE_MAX_PORT_PER_CHIP,
		},
	[1] = {
			.valid = 1,
			.unit = 0,
			.chn = 0,
			.dev = 0x72,			/* PCB U2 */
			.chip_idx = 1,
			.base_port = 1 * POE_MAX_PORT_PER_CHIP,
		},
	[2] = {
			.valid = 1,
			.unit = 0,
			.chn = 0,
			.dev = 0x74,			/* PCB U4 */
			.chip_idx = 2,
			.base_port = 1 * POE_MAX_PORT_PER_CHIP,
		},
};
#endif
static poe_info_t pci;

/* poe port form 0 to 23 */
static uint8_t poe_port_idx_to_user_idx[24] = 
{
/*
	0	1	2	3		4	5	6	7
*/
	5,	6,	7,	8,		3,	4, 	1,	2,
	13,	14,	15,	16,		11,	12,	9,	10,
	21,	22,	23,	24,		19,	20,	17,	18,
};

/* user port form 1 to 24 */
static uint8_t user_port_idx_to_poe_idx[25] = 
{
	0xff /* CPU port has not POE function */,
/*
	0	1	2	3		4	5	6	7
*/
	6,	7,	4,	5,		0,	1,	2,	3,
	14,	15,	12,	13,		8,	9,	10,	11,
	22,	23,	20,	21,		16,	17,	18,	19,
};

static int_special_hdl_t ishdl[] = 
{
	[INT_PWR_EN] = 
		{
			.int_name = "Power Enable changed",
			.int_type = INT_PWR_EN,
			.int_type_msk = BIT(INT_PWR_EN),
			.reg_int = POE_T760X_EVENT_PWR_EN_B_REG,
			.reg_special = POE_T760X_PWR_EN_REG,
			.int_hdl = poe_int_hdl_pwr_en_changed,
		},

	[INT_PWR_GD] = 
		{
			.int_name = "Power Good changed",
			.int_type = INT_PWR_GD,
			.int_type_msk = BIT(INT_PWR_GD),
			.reg_int = POE_T760X_EVENT_PWR_GD_B_REG,
			.reg_special = POE_T760X_PWR_GOOD_REG,
			.int_hdl = poe_int_hdl_pwr_good_changed,
		},

	[INT_DISCONNECT] = 
		{
			.int_name = "Disconnect",
			.int_type = INT_DISCONNECT,
			.int_type_msk = BIT(INT_DISCONNECT),
			.reg_int = POE_T760X_EVENT_DISCONNECT_B_REG,
			.reg_special = 0,
			.int_hdl = poe_int_hdl_disconnect,
		},
		
	[INT_DET_DN] = 
		{
			.int_name = "Detection done",
			.int_type = INT_DET_DN,
			.int_type_msk = BIT(INT_DET_DN),
			.reg_int = POE_T760X_EVENT_DET_DN_B_REG,
			.reg_special = 0,
			.int_hdl = poe_int_hdl_detect_done,
		},

	[INT_CLS_DN] = 
		{
			.int_name = "Classification done",
			.int_type = INT_CLS_DN,
			.int_type_msk = BIT(INT_CLS_DN),
			.reg_int = POE_T760X_EVENT_CLS_DN_B_REG,
			.reg_special = 0,
			.int_hdl = poe_int_hdl_class_done,
		},

	[INT_OVER_CURRENT] = 
		{
			.int_name = "Over Current after Power ON",
			.int_type = INT_OVER_CURRENT,
			.int_type_msk = BIT(INT_OVER_CURRENT),
			.reg_int = POE_T760X_EVENT_OVER_CURRENT_B_REG,
			.reg_special = 0,
			.int_hdl = poe_int_hdl_over_current_after_power_on,
		},

	[INT_PWR_UP_LIMITING] = 
		{
			.int_name = "Limit Current on Power UP",
			.int_type = INT_PWR_UP_LIMITING,
			.int_type_msk = BIT(INT_PWR_UP_LIMITING),
			.reg_int = POE_T760X_EVENT_PWR_UP_LIMITING_B_REG,
			.reg_special = 0,
			.int_hdl = poe_int_hdl_limit_current_on_power_up,
		},

	[INT_SUPPLY_BITS] = 
		{
			.int_name = "Limit Current on Power UP",
			.int_type = INT_SUPPLY_BITS,
			.int_type_msk = BIT(INT_SUPPLY_BITS),
			.reg_int = POE_T760X_EVENT_SUPPLY_B_REG,
			.reg_special = 0,
			.int_hdl = poe_int_hdl_supply,
		},

	[INT_PWR_ON_LIMITING] = 
		{
			.int_name = "Limit Current on Power ON",
			.int_type = INT_PWR_ON_LIMITING,
			//.int_type_msk = BIT(INT_PWR_ON_LIMITING),
			.int_type_msk = BIT(INT_PWR_UP_LIMITING) | BIT(INT_OVER_CURRENT),
			.reg_int = POE_T760X_EVENT_PWR_ON_LIMITING_B_REG,
			.reg_special = 0,
			.int_hdl = poe_int_hdl_limit_current_on_power_on,
		},
};

int8_t
port_user_to_poe_flat(uint8_t user_port)
{
	uint8_t type;

	if(user_port < POE_USER_PORT_BEGIN || user_port > POE_USER_PORT_END)
	{/* valid user port range from 1 to 24 */
		DEBUG_MSG("Error:parameter user_port:%d in %s are invalid,valid range[%d:%d]\n", 
			user_port, __FUNCTION__, POE_USER_PORT_BEGIN, POE_USER_PORT_END);
		return -1;
	}

	type = POE_USER_PORT_MAP;
	if(type == POE_PORT_MAP_DIRECTLY)
	{
		return (int8_t)(user_port - POE_USER_PORT_BEGIN + POE_FLAT_PORT_BEGIN);
	}
	else if(type == POE_PORT_MAP_PCB2142)
	{
		return (int8_t)user_port_idx_to_poe_idx[user_port];
	}
	else
	{
		return -1;
	}
}

int8_t
port_poe_flat_to_user(uint8_t poe_flat_port)
{
	uint8_t type;

	if(poe_flat_port < POE_FLAT_PORT_BEGIN || poe_flat_port > POE_FLAT_PORT_END)
	{/* valid poe flat port range from 0 to 23 */
		DEBUG_MSG("Error:parameter poe_flat_port:%d in %s are invalid,valid range[%d:%d]\n", 
			poe_flat_port, __FUNCTION__, POE_FLAT_PORT_BEGIN, POE_FLAT_PORT_END);
		return -1;
	}

	type = POE_USER_PORT_MAP;
	if(type == POE_PORT_MAP_DIRECTLY)
	{
		return (int8_t)(poe_flat_port - POE_FLAT_PORT_BEGIN + POE_USER_PORT_BEGIN);
	}
	else if(type == POE_PORT_MAP_PCB2142)
	{
		return (int8_t)poe_port_idx_to_user_idx[poe_flat_port];
	}
	else
	{
		return -1;
	}
}

int8_t
port_poe_flat_to_poe_hw(uint8_t poe_flat_port, poe_hw_port_t *ppcp)
{
	int i;
	poe_id_t *pid;
	uint8_t tmp;

	if(poe_flat_port < POE_FLAT_PORT_BEGIN || poe_flat_port > POE_FLAT_PORT_END)
	{/* valid poe flat port range from 0 to 23 */
		DEBUG_MSG("Error:parameter poe_flat_port:%d in %s are invalid,valid range[%d:%d]\n", 
			poe_flat_port, __FUNCTION__, POE_FLAT_PORT_BEGIN, POE_FLAT_PORT_END);
		return -1;
	}

	if(!ppcp)
	{
		DEBUG_MSG("Error:parameter ppcp:%p in %s are invalid\n", ppcp, __FUNCTION__);
		return -2;
	}

	tmp = poe_flat_port;
	for(i = 0, pid = &POE_CHIP[0]; i < POE_CHIP_TOTAL; i++, pid++)
	{
		if(!pid->valid)
		{
			continue;
		}

		if(tmp < pid->total_port)
		{
			ppcp->chip_idx = i;
			ppcp->port_idx = tmp;
			ppcp->pid = pid;
			return 0;
		}
		else
		{
			tmp -= pid->total_port;
			continue;
		}
	}

	return -3;
}

int8_t
port_poe_hw_to_poe_flat(poe_hw_port_t *ppcp)
{
	int i;
	poe_id_t *pid;
	uint8_t tmp;

	if(!ppcp)
	{
		DEBUG_MSG("Error:parameter ppcp:%p in %s are invalid\n", ppcp, __FUNCTION__);
		return -1;
	}

	if(ppcp->chip_idx >= POE_CHIP_TOTAL || 
		ppcp->port_idx >= POE_MAX_PORT_PER_CHIP ||
		ppcp->port_idx >= ppcp->pid->total_port ||
		ppcp->pid != &POE_CHIP[ppcp->chip_idx])
	{
		DEBUG_MSG("Error:parameter ppcp->chip_idx:%d or ppcp->port_idx:%d "
			"or ppcp->pid:%p in %s are invalid;POE_CHIP_TOTAL:%d POE_MAX_PORT_PER_CHIP:%d "
			"ppcp->pid->total_port:%d &POE_CHIP[%d]:%p\n", 
			ppcp->chip_idx, ppcp->port_idx, ppcp->pid, __FUNCTION__,
			POE_CHIP_TOTAL, POE_MAX_PORT_PER_CHIP, 
			ppcp->pid->total_port, ppcp->chip_idx, &POE_CHIP[ppcp->chip_idx]);

		return -2;
	}

	tmp = 0;
	for(i = 0, pid = &POE_CHIP[0]; i < POE_CHIP_TOTAL; i++, pid++)
	{
		if(!pid->valid)
		{
			continue;
		}

		if(i < ppcp->chip_idx)
		{
			tmp += pid->total_port;
			continue;
		}
		else
		{
			tmp += ppcp->port_idx;
			return (int8_t)tmp;
		}
	}

	return -3;
}

int8_t
port_user_to_poe_chip(uint8_t user_port, poe_hw_port_t *ppcp)
{
	int8_t poe_flat_port;

	if((poe_flat_port = port_user_to_poe_flat(user_port)) < 0)
	{
		return -1;
	}

	if(port_poe_flat_to_poe_hw(poe_flat_port, ppcp) < 0)
	{
		return -2;
	}

	return 0;
}

int8_t
port_poe_chip_to_user(poe_hw_port_t *ppcp)
{
	int8_t poe_flat_port;

	if((poe_flat_port = port_poe_hw_to_poe_flat(ppcp)) < 0)
	{
		return -1;
	}

	return port_poe_flat_to_user(poe_flat_port);
}

int
poe_reg_read(poe_id_t *p, uint8_t reg, uint8_t *val)
{
	int ret;

	if(!p || !p->valid || !val)
	{
		DEBUG_MSG("Error:parameter in %s are invalid\n", __FUNCTION__);
		return -1;
	}

	if((ret = i2c_reg8_val8_read(p->unit, p->chn, p->dev, 
				reg, val, I2C_OP_ERR_REPEAT)) != E_OK)
	{
		DEBUG_MSG("uint:0x%x chn:0x%x dev:0x%x reg:0x%x val:0x%x error return %d\n",
			p->unit, p->chn, p->dev, reg, *val, ret);

		return -2;
	}

	return 0;
}

int
poe_reg_write(poe_id_t *p, uint8_t reg, uint8_t val)
{
	int ret;

	if(!p || !p->valid)
	{
		DEBUG_MSG("Error:parameter in %s are invalid\n", __FUNCTION__);
		return -1;
	}

	if((ret = i2c_reg8_val8_write(p->unit, p->chn, p->dev, 
				reg, val, I2C_OP_ERR_REPEAT)) != E_OK)
	{
		DEBUG_MSG("uint:0x%x chn:0x%x dev:0x%x reg:0x%x val:0x%x error return %d\n",
			p->unit, p->chn, p->dev, reg, val, ret);

		return -2;
	}

	return 0;
}

int
poe_reg_modify(poe_id_t *p, uint8_t reg, uint8_t set, uint8_t msk)
{
	int ret;

	if(!p || !p->valid)
	{
		DEBUG_MSG("Error:parameter in %s are invalid\n", __FUNCTION__);
		return -1;
	}

	if((ret = i2c_reg8_val8_modify(p->unit, p->chn, p->dev, 
			reg, set, msk)) != E_OK)
	{
		DEBUG_MSG("uint:0x%x chn:0x%x dev:0x%x reg:0x%x set:0x%x msk:0x%x error return %d\n",
			p->unit, p->chn, p->dev, reg, set, msk, ret);

		return -2;
	}

	return 0;
}

#if 0
int
poe_chip_id_init_default(void)
{
	poe_id_t p;
	uint8_t u, c, d;

	if(NUMOFARRAY(poe_id_all) < POE_MAX_CHIP_PER_SWITCH)
	{
		return -1;
	}

	for(u = 0; u < POE_I2C_MAX_UNIT; u++)
	{
		for(c = 0; c < POE_I2C_MAX_CHN; c++)
		{
			for(d = 0; d < POE_I2C_MAX_DEV; d++)
			{
				p = &poe_id_all[u][c][d];

				p->unit = u;
				p->chn = c;
				p->dev = POE_I2C_DEV_START + d;
				p->valid = 1;
			}
		}
	}
	return 0;
}
#endif

int
poe_detect_success(uint8_t id)
{
	return id == DETECT_OK ? E_OK : -1;
}

char*
poe_detect_id_2_str(uint8_t id)
{
	char *str_detection[8] = 
		{
			[0] = "no detection",
			[1] = "DCP",
			[2] = "high capacitance",
			[3] = "low resistance",
			[4] = "detection ok",
			[5] = "high resistance",
			[6] = "open",
			[7] = "DCN",
		};

	return str_detection[id % NUMOFARRAY(str_detection)];
}

int
poe_class_success(uint8_t id)
{
	int ret;

	if(id >= CLASS_LEVEL_1 && id <= CLASS_LEVEL_0)
	{
		ret = E_OK;
	}
	else
	{
		ret = -1;
	}

	return ret;
}

char*
poe_class_id_2_str(uint8_t id)
{
	char *str_class[8] = 
		{
			[0] = "no class",
			[1] = "class 1",
			[2] = "class 2",
			[3] = "class 3",
			[4] = "class 4",
			[5] = "class 4+",
			[6] = "class 0",
			[7] = "current limit",
		};

	return str_class[id % NUMOFARRAY(str_class)];
}

int
poe_get_id_chip(poe_id_t *p, uint8_t *id, uint8_t *ver)
{
	int ret;
	uint8_t val;

	if(!p || !p->valid || !id || !ver)
	{
		DEBUG_MSG("Error:parameter in %s are invalid\n", __FUNCTION__);
		return -1;
	}

	if((ret = poe_reg_read(p, POE_T760X_ID_REG, &val)) != E_OK)
	{
		DEBUG_MSG("Error:%s reg:0x%x val:0x%x error return %d\n",
			"poe_reg_read", POE_T760X_ID_REG, val, ret);

		return -1;
	}

	*id = (val >> 3) & 0x1f;
	*ver = val & 0x7;

	return 0;
}

int
poe_pwr_cut_get_mode_chip(poe_id_t *p, uint8_t *mode)
{
	int ret;
	uint8_t val;

	if(!p || !mode)
	{
		DEBUG_MSG("Error:parameter in %s are invalid\n", __FUNCTION__);
		return -1;
	}

	if((ret = poe_reg_read(p, POE_T760X_PWR_CFG_REG, &val)) != E_OK)
	{
		DEBUG_MSG("Error:%s poe reg:0x%x val:0x%x error return %d\n",
			"poe_reg_read", POE_T760X_PWR_CFG_REG, val, ret);
	
		return -2;
	}

	*mode = (val >> 6) & PWR_CUT_MODE_MASK;

	return 0;
}

int
poe_pwr_cut_set_mode_chip(poe_id_t *p, uint8_t mode)
{
	int ret;
	uint8_t set, msk;

	if(!p || mode >= PWR_CUT_MODE_MAX)
	{
		DEBUG_MSG("Error:parameter in %s are invalid\n", __FUNCTION__);
		return -1;
	}

	set = mode << 6;
	msk = PWR_CUT_MODE_MASK << 6;	
	if((ret = poe_reg_modify(p, POE_T760X_PWR_CFG_REG, set, msk)) != E_OK)
	{
		DEBUG_MSG("Error:%s poe reg:0x%x set:0x%x mask:0x%x error return %d\n",
			"poe_reg_modify", POE_T760X_PWR_CFG_REG, set, msk, ret);
	
		return -2;
	}

	return 0;
}

int
poe_get_input_voltage_chip(poe_id_t *p, poe_chip_input_vltg_t *pciv)
{
	int ret;
	uint8_t t[2];
	uint8_t val;
#if 1
	uint32_t v, vmv;
#endif

	if(!p || !pciv)
	{
		DEBUG_MSG("Error:parameter in %s are invalid\n", __FUNCTION__);
		return -1;
	}

	if((ret = poe_reg_read(p, POE_T760X_UNDER_V_REG, &val)) != E_OK)
	{
		DEBUG_MSG("Error:%s reg:0x%x val:0x%x error return %d\n",
			"poe_reg_read", POE_T760X_UNDER_V_REG, val, ret);

		return -2;
	}

	if((ret = poe_reg_read(p, POE_T760X_CHIP_VOLT_L_REG, &t[0])) != E_OK)
	{
		DEBUG_MSG("Error:%s reg:0x%x val:0x%x error return %d\n",
			"poe_reg_read", POE_T760X_CHIP_VOLT_L_REG, t[0], ret);

		return -3;
	}

	if((ret = poe_reg_read(p, POE_T760X_CHIP_VOLT_M_REG, &t[1])) != E_OK)
	{
		DEBUG_MSG("Error:%s reg:0x%x val:0x%x error return %d\n",
			"poe_reg_read", POE_T760X_CHIP_VOLT_M_REG, t[1], ret);

		return -4;
	}
	
#if 1
	v = (uint32_t)t[0] | ((uint32_t)(t[1] & 0x1f) << 8);
	vmv = v * 1245 / 32 / 10;

	DBG_POE_MSG("Input power V54 is %s,chip voltage is %d mv(0x%02x 0x%02x %d) or %d mv\n", 
		(val & (1 << 5)) ? "less than 40v" : "normal", vmv, t[1], t[0], v, 
		((uint32_t)t[0] | ((uint32_t)t[1] << 8)) * 1245 / 32 / 10);
#endif

	pciv->input_v_low = !!(val & (1 << 5));
	pciv->input_vmv = ((uint32_t)t[0] | ((uint32_t)t[1] << 8)) * 1245 / 32 / 10;

	return 0;
}

int
poe_get_max_current_pwr_chip(poe_id_t *p, uint32_t *pua, uint32_t *pmw)
{
	int ret;
	uint8_t t[2];
	uint8_t val, reg[2];
	uint32_t v, imaxua, pmaxmw, constval;
	poe_chip_input_vltg_t civ, *pciv = &civ;

	if(!p || !pua || !pmw)
	{
		DEBUG_MSG("Error:parameter in %s are invalid\n", __FUNCTION__);
		return -1;
	}

	if((ret = poe_get_input_voltage_chip(p, pciv)) != 0)
	{
		DEBUG_MSG("Error:%s error return %d\n",
			"poe_chip_get_input_voltage", ret);

		return -2;
	}

	if((ret = poe_reg_read(p, POE_T760X_PWR_CFG_REG, &val)) != E_OK)
	{
		DEBUG_MSG("Error:%s reg:0x%x val:0x%x error return %d\n",
			"poe_reg_read", POE_T760X_PWR_CFG_REG, val, ret);

		return -3;
	}

	if(!(val & (1 << 1)))
	{/* PMAX 0 */
		//constval = 78;
		constval = 1956;
		reg[0] = POE_T760X_PMAX_MAX_CUR_L_REG;
		reg[1] = POE_T760X_PMAX_MAX_CUR_H_REG;
	}
	else
	{/* PMAX 1 */
		constval = 1956;
		reg[0] = POE_T760X_MAX_CUR_L_REG;
		reg[1] = POE_T760X_MAX_CUR_H_REG;
	}

	if((ret = poe_reg_read(p, reg[0], &t[0])) != E_OK)
	{
		DEBUG_MSG("Error:%s reg:0x%x val:0x%x error return %d\n",
			"poe_reg_read", reg[0], t[0], ret);
	
		return -4;
	}
	
	if((ret = poe_reg_read(p, reg[1], &t[1])) != E_OK)
	{
		DEBUG_MSG("Error:%s reg:0x%x val:0x%x error return %d\n",
			"poe_reg_read", reg[1], t[1], ret);
	
		return -5;
	}
	
	v = ((uint32_t)t[0] | (((uint32_t)t[1] & 0xf) << 8));
	imaxua = constval * v;
	pmaxmw = ((imaxua / 1000) * pciv->input_vmv) / 1000;

	*pua = imaxua;
	*pmw = pmaxmw;

	return 0;
}

int
poe_set_max_current_chip(poe_id_t *p, uint32_t imaxua)
{
	int ret;
	uint8_t t[2];
	uint8_t val, reg[2];
	uint32_t v, constval;

	if(!p || !imaxua)
	{
		DEBUG_MSG("Error:parameter in %s are invalid\n", __FUNCTION__);
		return -1;
	}

	/* PMAX 1 */
	constval = 1956;
	reg[0] = POE_T760X_MAX_CUR_L_REG;
	reg[1] = POE_T760X_MAX_CUR_H_REG;

	v = imaxua / constval;
	t[0] = v & 0xff;
	t[1] = (v >> 8) & 0xf;

	if((ret = poe_reg_write(p, reg[0], t[0])) != E_OK)
	{
		DEBUG_MSG("Error:%s reg:0x%x val:0x%x error return %d\n",
			"poe_reg_write", reg[0], t[0], ret);
	
		return -2;
	}
	
	if((ret = poe_reg_write(p, reg[1], t[1])) != E_OK)
	{
		DEBUG_MSG("Error:%s reg:0x%x val:0x%x error return %d\n",
			"poe_reg_write", reg[1], t[1], ret);
	
		return -3;
	}

	if((ret = poe_reg_read(p, POE_T760X_PWR_CFG_REG, &val)) != E_OK)
	{
		DEBUG_MSG("Error:%s reg:0x%x val:0x%x error return %d\n",
			"poe_reg_read", POE_T760X_PWR_CFG_REG, val, ret);

		return -4;
	}
	/* PMAX 1 */
	val |= (1 << 1);
	if((ret = poe_reg_write(p, POE_T760X_PWR_CFG_REG, val)) != E_OK)
	{
		DEBUG_MSG("Error:%s reg:0x%x val:0x%x error return %d\n",
			"poe_reg_write", POE_T760X_PWR_CFG_REG, val, ret);

		return -4;
	}
	
	return 0;
}

int
poe_set_max_current_all(uint32_t imaxua)
{
	int i;
	poe_id_t *pid;

	for(i = 0, pid = &POE_CHIP[0]; i < POE_CHIP_TOTAL; i++, pid++)
	{
		if(!pid->valid)
		{
			continue;
		}
		poe_set_max_current_chip(pid, imaxua);
	}

	return 0;
}

int
poe_set_max_pwr_chip(poe_id_t *p, uint32_t pmaxmw)
{
	int ret;
	uint32_t imaxua;
	poe_chip_input_vltg_t civ, *pciv = &civ;

	if(!p || !p->valid || !pmaxmw)
	{
		DEBUG_MSG("Error:parameter in %s are invalid\n", __FUNCTION__);
		return -1;
	}

	if((ret = poe_get_input_voltage_chip(p, pciv)) != 0)
	{
		DEBUG_MSG("Error:%s error return %d\n",
			"poe_chip_get_input_voltage", ret);

		return -2;
	}

	//imaxua = pmaxmw / pciv->input_vmv * 1000;
	imaxua = (pmaxmw * 1000) / pciv->input_vmv * 1000;	/* mw/mv * 1000 * 1000 == ua */

	return poe_set_max_current_chip(p, imaxua);
}

int
poe_set_max_pwr_all(uint32_t pmaxmw)
{
	int i;
	poe_id_t *pid;

	for(i = 0, pid = &POE_CHIP[0]; i < POE_CHIP_TOTAL; i++, pid++)
	{
		if(!pid->valid)
		{
			continue;
		}
		poe_set_max_pwr_chip(pid, pmaxmw);
	}

	return 0;
}

int
poe_set_chip_max_pwr_by_port_voltage_hw_port(
		poe_hw_port_t *pphp, uint32_t pmaxmw)
{
	int ret;
	uint32_t imaxua;
	poe_port_cvp_t ppc, *pppc = &ppc;

	if(!pphp || pphp->chip_idx >= POE_CHIP_TOTAL ||
		pphp->pid != &POE_CHIP[pphp->chip_idx] || 
		pphp->port_idx >= pphp->pid->total_port || !pmaxmw)
	{
		DBG_PRINT("Error:parameter in %s are invalid\n", __FUNCTION__);
		return -1;
	}

	if((ret = poe_get_cvp_hw_port(pphp, pppc)) != 0)
	{
		DBG_PRINT("Error:%s error return %d\n",
			"poe_get_cvp_hw_port", ret);

		return -2;
	}

	imaxua = (pmaxmw * 1000) / pppc->vmv * 1000;	/* mw/mv * 1000 * 1000 == ua */

	DBG_PRINT("POE chip index:%d port:%d,Cua:%dua Vmv:%d Pmw:%dmw\n", 
		pphp->chip_idx, pphp->port_idx,
		pppc->cua, pppc->vmv, pppc->pmw
		);
	DBG_PRINT("Set Current Max is %dua to POE chip index %d\n", imaxua, pphp->chip_idx);

	return poe_set_max_current_chip(pphp->pid, imaxua);
}

int
poe_set_chip_max_pwr_by_port_voltage_all(
		poe_hw_port_t *pphp, uint32_t pmaxmw)
{
	int i;
	poe_id_t *pid;
	int ret;
	uint32_t imaxua;
	poe_port_cvp_t ppc, *pppc = &ppc;
	static uint32_t cnt = (uint32_t)-1;

	cnt++;

	if(!pphp || pphp->chip_idx >= POE_CHIP_TOTAL ||
		pphp->pid != &POE_CHIP[pphp->chip_idx] || 
		pphp->port_idx >= pphp->pid->total_port || !pmaxmw)
	{
		DBG_PRINT("Error:parameter in %s are invalid\n", __FUNCTION__);
		return -1;
	}

	if(!(cnt % 10))
	{
		DBG_PRINT("POE MAX PWR %dmw\n", pmaxmw);
	}

	if((ret = poe_get_cvp_hw_port(pphp, pppc)) != 0)
	{
		DBG_PRINT("Error:%s error return %d\n",
			"poe_get_cvp_hw_port", ret);

		return -2;
	}

	imaxua = (pmaxmw * 1000) / pppc->vmv * 1000;	/* mw/mv * 1000 * 1000 == ua */

	if(!(cnt % 10))
	{
		DBG_PRINT("POE chip index:%d port:%d,C:%dua V:%dmv P:%dmw\n", 
			pphp->chip_idx, pphp->port_idx,
			pppc->cua, pppc->vmv, pppc->pmw
			);
	}

	for(i = 0, pid = &POE_CHIP[0]; i < POE_CHIP_TOTAL; i++, pid++)
	{
		if(!pid->valid)
		{
			continue;
		}
		poe_set_max_current_chip(pid, imaxua);

		if(!(cnt % 10))
		{
			DBG_PRINT("Set Current Max is %dua to POE chip index %d\n", imaxua, pid->chip_idx);
		}
	}

	if(!(cnt % 10))
	{
		DBG_PRINT("Set Current Max done\n");
	}

	return 0;
}

int
poe_set_chip_max_pwr_by_port_voltage_routing(uint32_t pmaxmw)
{
	int i, j;
	uint8_t en_good[POE_MAX_CURRENT_CHIP];
	poe_hw_port_t php, *pphp = &php;

	memset(&en_good[0], 0, sizeof(en_good));
	poe_get_pwr_en_good_all(&en_good[0], NUMOFARRAY(en_good));

	for(i = 0; i < NUMOFARRAY(en_good); i++)
	{
		if(!en_good[i])
		{
			continue;
		}

		for(j = 0; j < 8; j++)
		{
			if(en_good[i] & BITMSK(j))
			{
				pphp->chip_idx = i;
				pphp->port_idx = j;
				pphp->pid = &POE_CHIP[i];

				poe_set_chip_max_pwr_by_port_voltage_all(pphp, pmaxmw);
				return 0;
			}
		}
	}

	return 0;
}

int
poe_get_total_consumed_current_chip(poe_id_t *p, uint32_t *pua)
{
	int ret;
	uint8_t t[2];
	uint32_t v, constval = 1956;

	if(!p || !pua)
	{
		DEBUG_MSG("Error:parameter in %s are invalid\n", __FUNCTION__);
		return -1;
	}

	if((ret = poe_reg_read(p, POE_T760X_TOTAL_CONSUMED_I_L_REG, &t[0])) != E_OK)
	{
		DEBUG_MSG("Error:%s reg:0x%x val:0x%x error return %d\n",
			"poe_reg_read", POE_T760X_TOTAL_CONSUMED_I_L_REG, t[0], ret);

		return -2;
	}

	if((ret = poe_reg_read(p, POE_T760X_TOTAL_CONSUMED_I_H_REG, &t[1])) != E_OK)
	{
		DEBUG_MSG("Error:%s reg:0x%x val:0x%x error return %d\n",
			"poe_reg_read", POE_T760X_TOTAL_CONSUMED_I_H_REG, t[1], ret);

		return -3;
	}

	v = (uint32_t)t[0] | (((uint32_t)t[1] & 0xf) << 8);
	*pua = constval * v;

	return 0;
}

int
poe_get_total_remnant_current_chip(poe_id_t *p, uint32_t *pua)
{
	int ret;
	uint8_t t[2];
	uint32_t v, constval = 1956;

	if(!p || !pua)
	{
		DEBUG_MSG("Error:parameter in %s are invalid\n", __FUNCTION__);
		return -1;
	}

	if((ret = poe_reg_read(p, POE_T760X_TOTAL_REMNANT_I_L_REG, &t[0])) != E_OK)
	{
		DEBUG_MSG("Error:%s reg:0x%x val:0x%x error return %d\n",
			"poe_reg_read", POE_T760X_TOTAL_REMNANT_I_L_REG, t[0], ret);

		return -2;
	}

	if((ret = poe_reg_read(p, POE_T760X_TOTAL_REMNANT_I_H_REG, &t[1])) != E_OK)
	{
		DEBUG_MSG("Error:%s reg:0x%x val:0x%x error return %d\n",
			"poe_reg_read", POE_T760X_TOTAL_REMNANT_I_H_REG, t[1], ret);

		return -3;
	}

	v = (uint32_t)t[0] | (((uint32_t)t[1] & 0xf) << 8);
	*pua = constval * v;

	return 0;
}

uint32_t dbg_input_vmv, dbg_cua;

int
poe_get_total_consumed_pwr_chip(poe_id_t *p, uint32_t *ppmw)
{
	int ret;
	poe_chip_input_vltg_t civ, *pciv = &civ;
	uint32_t cua;

	if(!p || !ppmw)
	{
		DEBUG_MSG("Error:parameter in %s are invalid\n", __FUNCTION__);
		return -1;
	}

	if((ret = poe_get_input_voltage_chip(p, pciv)) != 0)
	{
		DEBUG_MSG("Error:%s error return %d\n",
			"poe_chip_get_input_voltage", ret);

		return -2;
	}

	if((ret = poe_get_total_consumed_current_chip(p, &cua)) != 0)
	{
		DEBUG_MSG("Error:%s error return %d\n",
			"poe_get_total_consumed_current_chip", ret);

		return -3;
	}

	*ppmw = ((cua / 1000) * pciv->input_vmv) /1000;

#if 1	/* wy,debug */
	dbg_input_vmv = pciv->input_vmv;
	dbg_cua = cua;
#endif

	return 0;
}

int
poe_get_total_consumed_pwr_all(uint32_t *ppmw)
{
	int i, ret;
	poe_id_t *pid;
	uint32_t pmw;

	*ppmw = 0;
	for(i = 0, pid = &POE_CHIP[0]; i < POE_CHIP_TOTAL; i++, pid++)
	{
		if(!pid->valid)
		{
			continue;
		}

		if((ret = poe_get_total_consumed_pwr_chip(pid, &pmw)) != 0)
		{
			DEBUG_MSG("Error:%s error return %d\n",
				"poe_get_total_consumed_pwr_chip", ret);

			continue;
		}

		*ppmw += pmw;
	}

	return 0;
}

int
poe_get_total_remnant_pwr_chip(poe_id_t *p, uint32_t *ppmw)
{
	int ret;
	poe_chip_input_vltg_t civ, *pciv = &civ;
	uint32_t cua;

	if(!p || !ppmw)
	{
		DEBUG_MSG("Error:parameter in %s are invalid\n", __FUNCTION__);
		return -1;
	}

	if((ret = poe_get_input_voltage_chip(p, pciv)) != 0)
	{
		DEBUG_MSG("Error:%s error return %d\n",
			"poe_chip_get_input_voltage", ret);

		return -2;
	}

	if((ret = poe_get_total_remnant_current_chip(p, &cua)) != 0)
	{
		DEBUG_MSG("Error:%s error return %d\n",
			"poe_get_total_remnant_current_chip", ret);

		return -3;
	}

	*ppmw = ((cua / 1000) * pciv->input_vmv) /1000;

	return 0;
}

int
poe_get_total_remnant_pwr_all(uint32_t *ppmw)
{
	int i, ret;
	poe_id_t *pid;
	uint32_t pmw;

	*ppmw = 0;
	for(i = 0, pid = &POE_CHIP[0]; i < POE_CHIP_TOTAL; i++, pid++)
	{
		if(!pid->valid)
		{
			continue;
		}

		if((ret = poe_get_total_remnant_pwr_chip(pid, &pmw)) != 0)
		{
			DEBUG_MSG("Error:%s error return %d\n",
				"poe_get_total_remnant_pwr_chip", ret);

			continue;
		}

		*ppmw += pmw;
	}

	return 0;
}

int
poe_get_pd_req_over_remnant_chip(poe_id_t *p, poe_pd_req_current_t *ppprc)
{
	int ret;
	uint8_t t[2];

	if(!p || !ppprc)
	{
		DEBUG_MSG("Error:parameter in %s are invalid\n", __FUNCTION__);
		return -1;
	}

	if((ret = poe_reg_read(p, POE_T760X_PD_REQ_I_L_REG, &t[0])) != E_OK)
	{
		DEBUG_MSG("Error:%s reg:0x%x val:0x%x error return %d\n",
			"poe_reg_read", POE_T760X_PD_REQ_I_L_REG, t[0], ret);

		return -2;
	}

	if((ret = poe_reg_read(p, POE_T760X_PD_REQ_I_H_REG, &t[1])) != E_OK)
	{
		DEBUG_MSG("Error:%s reg:0x%x val:0x%x error return %d\n",
			"poe_reg_read", POE_T760X_PD_REQ_I_H_REG, t[1], ret);

		return -3;
	}

	ppprc->port = (t[1] >> 4) & 0x7;
	ppprc->current = (uint32_t)t[0] | (((uint32_t)t[1] & 0x1) << 8);

	return 0;
}

int
poe_get_icut_chip(poe_id_t *p, poe_chip_icut_t *picut)
{
	int ret, i;
	uint8_t t[2], cls;
	uint32_t multiple, i_base;

	if(!p || !picut)
	{
		DEBUG_MSG("Error:parameter in %s are invalid\n", __FUNCTION__);
		return -1;
	}

	for(i = 0; i < p->total_port; i++)
	{
		if((ret = poe_reg_read(p, POE_T760X_PORT_ICUT_REG(i), &t[0])) != E_OK)
		{
			DEBUG_MSG("Error:%s reg:0x%x val:0x%x error return %d\n",
				"poe_reg_read", POE_T760X_PORT_ICUT_REG(i), t[0], ret);

			return -2;
		}

		if(!(t[0] & 0x3f))
		{
			picut->ua[i] = 0;
			continue;
		}

		if((ret = poe_reg_read(p, POE_T760X_PORT_STAUTS_REG(i), &t[1])) != E_OK)
		{
			DEBUG_MSG("Error:%s reg:0x%x val:0x%x error return %d\n",
				"poe_reg_read", POE_T760X_PORT_STAUTS_REG(i), t[1], ret);

			return -3;
		}

		if(!(t[0] & (1 << 6)))
		{
			multiple = 2;
		}
		else
		{
			multiple = 1;
		}

		cls = (t[1] >> 4) & 0x7;
		if(cls == CLASS_LEVEL_4PLUS)
		{
			i_base = 37500;
		}
		else
		{
			i_base = 18750;
		}

		picut->ua[i] = (uint32_t)(t[0] & 0x3f) * i_base * multiple;
	}

	return 0;
}

int
poe_get_icut_hw_port(poe_hw_port_t *ppcp, uint32_t *picut)
{
	int ret;
	uint8_t t[1], cls;
	uint32_t multiple, i_base;

	if(!ppcp || ppcp->chip_idx >= POE_CHIP_TOTAL ||
		ppcp->pid != &POE_CHIP[ppcp->chip_idx] || 
		ppcp->port_idx >= ppcp->pid->total_port || !picut)
	{
		DEBUG_MSG("Error:parameter in %s are invalid\n", __FUNCTION__);
		return -1;
	}

	if((ret = poe_reg_read(ppcp->pid, POE_T760X_PORT_ICUT_REG(ppcp->port_idx), &t[0])) != E_OK)
	{
		DEBUG_MSG("Error:%s reg:0x%x val:0x%x error return %d\n",
			"poe_reg_read", POE_T760X_PORT_ICUT_REG(ppcp->port_idx), t[0], ret);

		return -2;
	}

	if(!(t[0] & 0x3f))
	{
		*picut = 0;
		return 0;
	}

	if(poe_get_cls_hw_port(ppcp, &cls) < 0)
	{
		DEBUG_MSG("Error:%s poe_port_get_cls_hw_port fail\n", __FUNCTION__);
		return -3;
	}

	if(!(t[0] & (1 << 6)))
	{
		multiple = 2;
	}
	else
	{
		multiple = 1;
	}

	if(cls == CLASS_LEVEL_4PLUS)
	{
		i_base = 37500;
	}
	else
	{
		i_base = 18750;
	}

	*picut = (uint32_t)(t[0] & 0x3f) * i_base * multiple;

	return 0;
}

int
poe_get_icut_user_port(uint8_t user_port, uint32_t *picut)
{
	poe_hw_port_t pcp, *ppcp = &pcp;

	if(!picut)
	{
		DEBUG_MSG("Error:parameter in %s are invalid\n", __FUNCTION__);
		return -1;
	}

	if(port_user_to_poe_chip(user_port, ppcp) < 0)
	{
		DEBUG_MSG("Error:user port to poe chip fail\n");
		return -2;
	}

	return poe_get_icut_hw_port(ppcp, picut);
}

int
poe_get_cvp_chip(poe_id_t *p, poe_port_cvp_t *ppc)
{
	int ret, i;
	uint8_t t[4], cls;
	uint32_t c, v, cua, vmv, pmw;

	if(!p || !ppc)
	{
		DEBUG_MSG("Error:parameters are invalid\n");
		return -1;
	}

	if(!p->valid)
	{
		DEBUG_MSG("Error:poe id is invalid\n");
		return -2;
	}

	for(i = 0; i < p->total_port; i++)
	{	
		if((ret = poe_reg_read(p, POE_T760X_PORT_STAUTS_REG(i), &t[0])) != E_OK)
		{
			DEBUG_MSG("Error:%s reg:0x%x val:0x%x error return %d\n",
				"poe_reg_read", POE_T760X_PORT_STAUTS_REG(i), t[0], ret);

			return -3;
		}
		cls = (t[0] >> 4) & 0x7;
	
		if((ret = poe_reg_read(p, POE_T760X_PORT_CURR_L_REG(i), &t[0])) != E_OK)
		{
			DEBUG_MSG("Error:%s reg:0x%x val:0x%x error return %d\n",
				"poe_reg_read", POE_T760X_PORT_CURR_L_REG(i), t[0], ret);

			return -4;
		}

		if((ret = poe_reg_read(p, POE_T760X_PORT_CURR_M_REG(i), &t[1])) != E_OK)
		{
			DEBUG_MSG("Error:%s reg:0x%x val:0x%x error return %d\n",
				"poe_reg_read", POE_T760X_PORT_CURR_M_REG(i), t[1], ret);

			return -5;
		}

		c = (uint32_t)t[0] | ((uint32_t)t[1] << 8);
		cua = c * 1956 / 16;
		cua = (cls == CLASS_LEVEL_4PLUS ? cua * 2 : cua);

		if((ret = poe_reg_read(p, POE_T760X_PORT_VOLT_L_REG(i), &t[2])) != E_OK)
		{
			DEBUG_MSG("Error:%s reg:0x%x val:0x%x error return %d\n",
				"poe_reg_read", POE_T760X_PORT_VOLT_L_REG(i), t[2], ret);

			return -6;
		}

		if((ret = poe_reg_read(p, POE_T760X_PORT_VOLT_M_REG(i), &t[3])) != E_OK)
		{
			DEBUG_MSG("Error:%s reg:0x%x val:0x%x error return %d\n",
				"poe_reg_read", POE_T760X_PORT_VOLT_M_REG(i), t[3], ret);

			return -7;
		}

		v = (uint32_t)t[2] | ((uint32_t)t[3] << 8);
		vmv = v * 1265 / 32 / 10;
		pmw = ((cua / 1000) * vmv) /1000;

#if 0
		DEBUG_MSG("\tChip[%d] Port[%d]:current %d uA(0x%02x 0x%02x %d),voltage %d mV(0x%02x 0x%02x %d) "
			"power %d mW\n", 
			p->chip_idx, i, cua, t[1], t[0], c, vmv, t[3], t[2], v, pmw);
#endif

		ppc[i].cua = cua;
		ppc[i].vmv = vmv;
		ppc[i].pmw = pmw;
	}

	return 0;
}

int
poe_get_cvp_hw_port(poe_hw_port_t *pphp, poe_port_cvp_t *ppc)
{
	int ret;
	uint8_t t[4], cls;
	uint32_t c, v, cua, vmv, pmw;

	if(!pphp || pphp->chip_idx >= POE_CHIP_TOTAL ||
		pphp->pid != &POE_CHIP[pphp->chip_idx] || 
		pphp->port_idx >= pphp->pid->total_port || !ppc)
	{
		DEBUG_MSG("Error:parameter in %s are invalid\n", __FUNCTION__);
		return -1;
	}

	if((ret = poe_get_cls_hw_port(pphp, &cls)) != 0)
	{
		DEBUG_MSG("Error:%s port:%d error return %d\n",
			"poe_port_get_cls", pphp->port_idx, ret);
	
		return -2;
	}
	DBG_POE_MSG("Get class %d success from chip idx %d port idx %d\n", 
		cls, pphp->chip_idx, pphp->port_idx);

	if((ret = poe_reg_read(pphp->pid, POE_T760X_PORT_CURR_L_REG(pphp->port_idx), &t[0])) != E_OK)
	{
		DEBUG_MSG("Error:%s reg:0x%x val:0x%x error return %d\n",
			"poe_reg_read", POE_T760X_PORT_CURR_L_REG(pphp->port_idx), t[0], ret);

		return -3;
	}

	if((ret = poe_reg_read(pphp->pid, POE_T760X_PORT_CURR_M_REG(pphp->port_idx), &t[1])) != E_OK)
	{
		DEBUG_MSG("Error:%s reg:0x%x val:0x%x error return %d\n",
			"poe_reg_read", POE_T760X_PORT_CURR_M_REG(pphp->port_idx), t[1], ret);

		return -4;
	}

	c = (uint32_t)t[0] | ((uint32_t)t[1] << 8);
	cua = c * 1956 / 16;
	cua = (cls == CLASS_LEVEL_4PLUS ? cua * 2 : cua);

	if((ret = poe_reg_read(pphp->pid, POE_T760X_PORT_VOLT_L_REG(pphp->port_idx), &t[2])) != E_OK)
	{
		DEBUG_MSG("Error:%s reg:0x%x val:0x%x error return %d\n",
			"poe_reg_read", POE_T760X_PORT_VOLT_L_REG(pphp->port_idx), t[2], ret);

		return -5;
	}

	if((ret = poe_reg_read(pphp->pid, POE_T760X_PORT_VOLT_M_REG(pphp->port_idx), &t[3])) != E_OK)
	{
		DEBUG_MSG("Error:%s reg:0x%x val:0x%x error return %d\n",
			"poe_reg_read", POE_T760X_PORT_VOLT_M_REG(pphp->port_idx), t[3], ret);

		return -6;
	}

	v = (uint32_t)t[2] | ((uint32_t)t[3] << 8);
	vmv = v * 1265 / 32 / 10;
	pmw = ((cua / 1000) * vmv) /1000;

#if 0
	DEBUG_MSG("\tChip[%d] Port[%d]:current %d uA(0x%02x 0x%02x %d),voltage %d mV(0x%02x 0x%02x %d) "
		"power %d mW\n", 
		pphp->chip_idx, pphp->port_idx, cua, t[1], t[0], c, vmv, t[3], t[2], v, pmw);
#endif

	ppc->cua = cua;
	ppc->vmv = vmv;
	ppc->pmw = pmw;

	DBG_POE_MSG("%s success on chip %d port %d,"
		"C:%dua,V:%dmv,P:%dmw;t[0x%x:0x%x:0x%x:0x%x]\n", 
		__FUNCTION__, 
		pphp->chip_idx, pphp->port_idx, 
		ppc->cua, ppc->vmv, ppc->pmw, 
		t[0], t[1], t[2], t[3]);

	return 0;
}

int
poe_get_cvp_user_port(uint8_t user_port, poe_port_cvp_t *ppc)
{
	poe_hw_port_t php, *pphp = &php;

	if(!ppc)
	{
		DEBUG_MSG("Error:parameter in %s are invalid\n", __FUNCTION__);
		return -1;
	}

	if(port_user_to_poe_chip(user_port, pphp) < 0)
	{
		DEBUG_MSG("Error:user port to poe chip fail\n");
		return -2;
	}

	return poe_get_cvp_hw_port(pphp, ppc);
}

int
poe_get_temp_chip(poe_id_t *p, uint16_t *ptemp)
{
	int ret;
	uint8_t t[2];
	uint8_t pot;
	uint32_t tmp;

	if(!p || !ptemp)
	{
		DEBUG_MSG("Error:parameters are invalid\n");
		return -1;
	}

	if(!p->valid)
	{
		DEBUG_MSG("Error:poe id is invalid\n");
		return -2;
	}

	for(pot = 0; pot < p->total_port; pot++)
	{
		if((ret = poe_reg_read(p, POE_T760X_PORT_TEMP_L_REG(pot), &t[0])) != E_OK)
		{
			DEBUG_MSG("Error:%s reg:0x%x val:0x%x error return %d\n",
				"poe_reg_read", POE_T760X_PORT_TEMP_L_REG(pot), t[0], ret);

			return -3;
		}

		if((ret = poe_reg_read(p, POE_T760X_PORT_TEMP_M_REG(pot), &t[1])) != E_OK)
		{
			DEBUG_MSG("Error:%s reg:0x%x val:0x%x error return %d\n",
				"poe_reg_read", POE_T760X_PORT_TEMP_M_REG(pot), t[1], ret);

			return -4;
		}

		tmp = (uint32_t)t[0] | ((uint32_t)t[1] << 8);
		if(!tmp)
		{
			ptemp[pot] = 0;
		}
		else
		{
			ptemp[pot] = (tmp - 280) * 10000 * 100 / 10876;
		}
	}

	return 0;
}

int
poe_get_temp_hw_port(poe_hw_port_t *pphp, uint16_t *ptemp)
{
	int ret;
	uint8_t t[2];
	uint32_t tmp;

	if(!pphp || pphp->chip_idx >= POE_CHIP_TOTAL ||
		pphp->pid != &POE_CHIP[pphp->chip_idx] || 
		pphp->port_idx >= pphp->pid->total_port || !ptemp)
	{
		DEBUG_MSG("Error:parameter in %s are invalid\n", __FUNCTION__);
		return -1;
	}

	if((ret = poe_reg_read(pphp->pid, POE_T760X_PORT_TEMP_L_REG(pphp->port_idx), &t[0])) != E_OK)
	{
		DEBUG_MSG("Error:%s reg:0x%x val:0x%x error return %d\n",
			"poe_reg_read", POE_T760X_PORT_TEMP_L_REG(pphp->port_idx), t[0], ret);

		return -2;
	}

	if((ret = poe_reg_read(pphp->pid, POE_T760X_PORT_TEMP_M_REG(pphp->port_idx), &t[1])) != E_OK)
	{
		DEBUG_MSG("Error:%s reg:0x%x val:0x%x error return %d\n",
			"poe_reg_read", POE_T760X_PORT_TEMP_M_REG(pphp->port_idx), t[1], ret);

		return -3;
	}

	tmp = (uint32_t)t[0] | ((uint32_t)t[1] << 8);
	if(!tmp)
	{
		*ptemp = 0;
	}
	else
	{
		*ptemp = (tmp - 280) * 10000 * 100 / 10876;
	}

	return 0;
}

int
poe_get_temp_user_port(uint8_t user_port, uint16_t *ptemp)
{
	poe_hw_port_t php, *pphp = &php;

	if(!ptemp)
	{
		DEBUG_MSG("Error:parameter in %s are invalid\n", __FUNCTION__);
		return -1;
	}

	if(port_user_to_poe_chip(user_port, pphp) < 0)
	{
		DEBUG_MSG("Error:user port to poe chip fail\n");
		return -2;
	}

	return poe_get_temp_hw_port(pphp, ptemp);
}

int
poe_pwr_routing(void)
{
	poe_info_t *ppci = &pci;
	poe_port_info_t *pi, *n;

	list_for_each_entry_safe(pi, n, &ppci->priority_hdr, priority_nod)
	{
		if(pi->status == POE_PORT_STATUS_CLASSED)
		{
			poe_port_power_on_btn_trigger(pi->port);
		}
	}
	return 0;
}

int
poe_info_init(void)
{
	poe_info_t *ppci = &pci;
	poe_port_info_t *pi;
	uint8_t idx;

	ppci->cls_done = 0;
	INIT_LIST_HEAD(&ppci->priority_hdr);

	for(idx = 0, pi = &ppci->ppit[0]; idx < NUMOFARRAY(ppci->ppit); idx++, pi++)
	{
		pi->port = idx;
		pi->priority_val = POE_PRIORITY_DEFAULT + idx;
		pi->status = POE_PORT_STATUS_INACTIVE;
		INIT_LIST_HEAD(&pi->priority_nod);

		if(POE_PORT_2_CHIP_IDX(pi->port) < POE_CHIP_TOTAL)
		{
			pi->pid = &POE_CHIP[POE_PORT_2_CHIP_IDX(pi->port)];
		}
		else
		{
			DEBUG_MSG("Error:POE port %d overflow the total chip %d\n", 
				pi->port, (int)POE_CHIP_TOTAL);
			pi->pid = NULL;
		}

		list_add_tail(&pi->priority_nod, &ppci->priority_hdr);
	}

	return 0;
}

int
poe_info_set_status(uint8_t port, uint8_t status)
{
	poe_info_t *ppci = &pci;
	poe_port_info_t *pi;

	if(port >= NUMOFARRAY(ppci->ppit) || status >= POE_PORT_STATUS_MAX)
	{
		return -1;
	}

	pi = &ppci->ppit[port];
	pi->status = status;

	return 0;
}

int
poe_port_info_get_status(uint8_t port, uint8_t *status)
{
	poe_info_t *ppci = &pci;
	poe_port_info_t *pi;

	if(port >= NUMOFARRAY(ppci->ppit) || !status)
	{
		return -1;
	}

	pi = &ppci->ppit[port];
	*status = pi->status;

	return 0;
}

int
poe_get_cls_hw_port(poe_hw_port_t *pphp, uint8_t *cls)
{
	int ret;
	uint8_t val;

	if(!pphp || pphp->chip_idx >= POE_CHIP_TOTAL ||
		pphp->pid != &POE_CHIP[pphp->chip_idx] || 
		pphp->port_idx >= pphp->pid->total_port || !cls)
	{
		DEBUG_MSG("Error:parameter in %s are invalid\n", __FUNCTION__);
		return -1;
	}

	if((ret = poe_reg_read(pphp->pid, POE_T760X_PORT_STAUTS_REG(pphp->port_idx), &val)) != E_OK)
	{
		DEBUG_MSG("Error:%s poe index:%d port:%d reg:0x%x val:0x%x error return %d\n",
			"poe_reg_read", pphp->pid->chip_idx, pphp->port_idx, 
			POE_T760X_PORT_STAUTS_REG(pphp->port_idx), val, ret);
	
		return -2;
	}

	*cls = (val >> 4) & 0x7;

	DBG_POE_MSG("Get val %d success from chip idx %d port idx %d reg 0x%x\n", 
		val, pphp->chip_idx, pphp->port_idx, 
		POE_T760X_PORT_STAUTS_REG(pphp->port_idx), cls);

	return 0;
}

int
poe_get_cls_user_port(uint8_t user_port, uint8_t *cls)
{
	poe_hw_port_t php, *pphp = &php;

	if(!cls)
	{
		DEBUG_MSG("Error:parameter in %s are invalid\n", __FUNCTION__);
		return -1;
	}

	if(port_user_to_poe_chip(user_port, pphp) < 0)
	{
		DEBUG_MSG("Error:user port to poe chip fail\n");
		return -2; 
	}

	return poe_get_cls_hw_port(pphp, cls);
}

int
poe_get_det_hw_port(poe_hw_port_t *pphp, uint8_t *det)
{
	int ret;
	uint8_t val;

	if(!pphp || pphp->chip_idx >= POE_CHIP_TOTAL ||
		pphp->pid != &POE_CHIP[pphp->chip_idx] || 
		pphp->port_idx >= pphp->pid->total_port || !det)
	{
		DEBUG_MSG("Error:parameter in %s are invalid\n", __FUNCTION__);
		return -1;
	}

	if((ret = poe_reg_read(pphp->pid, POE_T760X_PORT_STAUTS_REG(pphp->port_idx), &val)) != E_OK)
	{
		DEBUG_MSG("Error:%s poe index:%d port:%d reg:0x%x val:0x%x error return %d\n",
			"poe_reg_read", pphp->pid->chip_idx, pphp->port_idx, 
			POE_T760X_PORT_STAUTS_REG(pphp->port_idx), val, ret);
	
		return -2;
	}

	*det = (val >> 0) & 0x7;

	return 0;
}

int
poe_get_det_user_port(uint8_t user_port, uint8_t *det)
{
	poe_hw_port_t php, *pphp = &php;

	if(!det)
	{
		DEBUG_MSG("Error:parameter in %s are invalid\n", __FUNCTION__);
		return -1;
	}

	if(port_user_to_poe_chip(user_port, pphp) < 0)
	{
		DEBUG_MSG("Error:user port to poe chip fail\n");
		return -2; 
	}

	return poe_get_det_hw_port(pphp, det);
}

int
poe_get_det_cls_hw_port(poe_hw_port_t *pphp, uint8_t *det, uint8_t *cls)
{
	int ret;
	uint8_t val;

	if(!pphp || pphp->chip_idx >= POE_CHIP_TOTAL ||
		pphp->pid != &POE_CHIP[pphp->chip_idx] || 
		pphp->port_idx >= pphp->pid->total_port || !det)
	{
		DEBUG_MSG("Error:parameter in %s are invalid\n", __FUNCTION__);
		return -1;
	}

	if((ret = poe_reg_read(pphp->pid, POE_T760X_PORT_STAUTS_REG(pphp->port_idx), &val)) != E_OK)
	{
		DEBUG_MSG("Error:%s poe index:%d port:%d reg:0x%x val:0x%x error return %d\n",
			"poe_reg_read", pphp->pid->chip_idx, pphp->port_idx, 
			POE_T760X_PORT_STAUTS_REG(pphp->port_idx), val, ret);
	
		return -2;
	}

	*det = (val >> 0) & 0x7;
	*cls = (val >> 4) & 0x7;

	return 0;
}

int
poe_get_det_cls_user_port(uint8_t user_port, uint8_t *det, uint8_t *cls)
{
	poe_hw_port_t php, *pphp = &php;

	if(!det)
	{
		DEBUG_MSG("Error:parameter in %s are invalid\n", __FUNCTION__);
		return -1;
	}

	if(port_user_to_poe_chip(user_port, pphp) < 0)
	{
		DEBUG_MSG("Error:user port to poe chip fail\n");
		return -2; 
	}

	return poe_get_det_cls_hw_port(pphp, det, cls);
}

int
poe_get_pwr_en_chip(poe_id_t *p, uint8_t *en)
{
	int ret;
	uint8_t val;

	if(!p || !en)
	{
		DEBUG_MSG("Error:parameters are invalid\n");
		return -1;
	}

	if(!p->valid)
	{
		DEBUG_MSG("Error:poe id is invalid\n");
		return -2;
	}

	if((ret = poe_reg_read(p, POE_T760X_PWR_EN_REG, &val)) != E_OK)
	{
		DEBUG_MSG("Error:%s poe index:%d reg:0x%x val:0x%x error return %d\n",
			"poe_reg_read", p->chip_idx, POE_T760X_PWR_EN_REG, val, ret);
	
		return -3;
	}

	*en = val;

	return 0;
}

int
poe_get_pwr_en_all(uint8_t *en, uint8_t num)
{
	int i, j;
	poe_id_t *pid;

	for(i = 0, j = 0, pid = &POE_CHIP[0]; i < POE_CHIP_TOTAL && j < num; i++, pid++)
	{
		if(!pid->valid)
		{
			continue;
		}
		poe_get_pwr_en_chip(pid, &en[j++]);
	}

	return 0;
}

int
poe_get_pwr_en_hw_port(poe_hw_port_t *pphp, uint8_t *en)
{
	int ret;
	uint8_t val;

	if(!pphp || pphp->chip_idx >= POE_CHIP_TOTAL ||
		pphp->pid != &POE_CHIP[pphp->chip_idx] || 
		pphp->port_idx >= pphp->pid->total_port || !en)
	{
		DEBUG_MSG("Error:parameter in %s are invalid\n", __FUNCTION__);
		return -1;
	}

	if((ret = poe_reg_read(pphp->pid, POE_T760X_PWR_EN_REG, &val)) != E_OK)
	{
		DEBUG_MSG("Error:%s poe index:%d port:%d reg:0x%x val:0x%x error return %d\n",
			"poe_reg_read", pphp->pid->chip_idx, pphp->port_idx, 
			POE_T760X_PWR_EN_REG, val, ret);
	
		return -2;
	}

	*en = !!(val & (1 << pphp->port_idx));

	return 0;
}

int
poe_get_pwr_en_user_port(uint8_t user_port, uint8_t *en)
{
	poe_hw_port_t php, *pphp = &php;

	if(!en)
	{
		DEBUG_MSG("Error:parameter in %s are invalid\n", __FUNCTION__);
		return -1;
	}

	if(port_user_to_poe_chip(user_port, pphp) < 0)
	{
		DEBUG_MSG("Error:user port to poe chip fail\n");
		return -2; 
	}

	return poe_get_pwr_en_hw_port(pphp, en);
}

int
poe_get_pwr_good_chip(poe_id_t *p, uint8_t *good)
{
	int ret;
	uint8_t val;

	if(!p || !good)
	{
		DEBUG_MSG("Error:parameters are invalid\n");
		return -1;
	}

	if(!p->valid)
	{
		DEBUG_MSG("Error:poe id is invalid\n");
		return -2;
	}

	if((ret = poe_reg_read(p, POE_T760X_PWR_GOOD_REG, &val)) != E_OK)
	{
		DEBUG_MSG("Error:%s poe index:%d reg:0x%x val:0x%x error return %d\n",
			"poe_reg_read", p->chip_idx, POE_T760X_PWR_GOOD_REG, val, ret);
	
		return -3;
	}

	*good = val;

	return 0;
}

int
poe_get_pwr_good_all(uint8_t *good, uint8_t num)
{
	int i, j;
	poe_id_t *pid;

	for(i = 0, j = 0, pid = &POE_CHIP[0]; i < POE_CHIP_TOTAL && j < num; i++, pid++)
	{
		if(!pid->valid)
		{
			continue;
		}
		poe_get_pwr_good_chip(pid, &good[j++]);
	}

	return 0;
}

int
poe_get_pwr_good_hw_port(poe_hw_port_t *pphp, uint8_t *good)
{
	int ret;
	uint8_t val;

	if(!pphp || pphp->chip_idx >= POE_CHIP_TOTAL ||
		pphp->pid != &POE_CHIP[pphp->chip_idx] || 
		pphp->port_idx >= pphp->pid->total_port || !good)
	{
		DEBUG_MSG("Error:parameter in %s are invalid\n", __FUNCTION__);
		return -1;
	}

	if((ret = poe_reg_read(pphp->pid, POE_T760X_PWR_GOOD_REG, &val)) != E_OK)
	{
		DEBUG_MSG("Error:%s poe index:%d port:%d reg:0x%x val:0x%x error return %d\n",
			"poe_reg_read", pphp->pid->chip_idx, pphp->port_idx, 
			POE_T760X_PWR_GOOD_REG, val, ret);
	
		return -2;
	}

	*good = !!(val & (1 << pphp->port_idx));

	return 0;
}

int
poe_get_pwr_good_user_port(uint8_t user_port, uint8_t *good)
{
	poe_hw_port_t php, *pphp = &php;

	if(!good)
	{
		DEBUG_MSG("Error:parameter in %s are invalid\n", __FUNCTION__);
		return -1;
	}

	if(port_user_to_poe_chip(user_port, pphp) < 0)
	{
		DEBUG_MSG("Error:user port to poe chip fail\n");
		return -2; 
	}

	return poe_get_pwr_good_hw_port(pphp, good);
}

int
poe_get_pwr_en_good_all(uint8_t *en_good, uint8_t num)
{
	int i, j;
	poe_id_t *pid;
	uint8_t a, b;

	for(i = 0, j = 0, pid = &POE_CHIP[0]; i < POE_CHIP_TOTAL && j < num; i++, pid++)
	{
		if(!pid->valid)
		{
			continue;
		}

		poe_get_pwr_en_chip(pid, &a);
		poe_get_pwr_good_chip(pid, &b);

		en_good[j++] = a & b;
	}

	return 0;
}

int
poe_get_mode_hw_port(poe_hw_port_t *pphp, uint8_t *mode)
{
	int ret;
	uint8_t val, reg;

	if(!pphp || pphp->chip_idx >= POE_CHIP_TOTAL ||
		pphp->pid != &POE_CHIP[pphp->chip_idx] || 
		pphp->port_idx >= pphp->pid->total_port || !mode)
	{
		DEBUG_MSG("Error:parameter in %s are invalid\n", __FUNCTION__);
		return -1;
	}

	if(pphp->port_idx >= 4 && pphp->pid->chip_type == POE_CHIP_TYPE_T7608R)
	{
		reg = POE_T760X_MODE_B_REG;
	}
	else
	{
		reg = POE_T760X_MODE_A_REG;
	}

	if((ret = poe_reg_read(pphp->pid, reg, &val)) != E_OK)
	{
		DEBUG_MSG("Error:%s poe index:%d port:%d reg:0x%x val:0x%x error return %d\n",
			"poe_reg_read", pphp->chip_idx, pphp->port_idx, 
			reg, val, ret);
	
		return -2;
	}

	*mode = (val >> ((pphp->port_idx % 4) * 2)) & MODE_MASK;

	return 0;
}

int
poe_get_mode_user_port(uint8_t user_port, uint8_t *mode)
{
	poe_hw_port_t php, *pphp = &php;

	if(!mode)
	{
		DEBUG_MSG("Error:parameter in %s are invalid\n", __FUNCTION__);
		return -1;
	}

	if(port_user_to_poe_chip(user_port, pphp) < 0)
	{
		DEBUG_MSG("Error:user port to poe chip fail\n");
		return -2;
	}

	return poe_get_mode_hw_port(pphp, mode);
}

int
poe_get_mode_all(uint8_t *mode, uint32_t szmd)
{
	int ret, st;
	uint8_t val;
	uint8_t i;
	poe_id_t *p;

	if(!mode || szmd < POE_CHIP_TOTAL * 2)
	{
		DEBUG_MSG("Error:parameter mode %p incorrect or size of mode %ld less than %d\n", 
			mode, szmd, (int)POE_CHIP_TOTAL * 2);
		return -1;
	}

	for(i = 0, p = &POE_CHIP[0], ret = 0; i < POE_CHIP_TOTAL; i++, p++)
	{
		if(!p->valid)
		{
			continue;
		}

		if((st = poe_reg_read(p, POE_T760X_MODE_A_REG, &val)) != E_OK)
		{
			DEBUG_MSG("Error:%s poe index:%d reg:0x%x val:0x%x error return %d\n",
				"poe_reg_read", i, POE_T760X_MODE_A_REG, val, st);
			ret = -1;
		}
		else
		{
			mode[i] = val;
		}

		if(p->chip_type == POE_CHIP_TYPE_T7608R)
		{
			i++;
			if((st = poe_reg_read(p, POE_T760X_MODE_B_REG, &val)) != E_OK)
			{
				DEBUG_MSG("Error:%s poe index:%d reg:0x%x val:0x%x error return %d\n",
					"poe_reg_read", i, POE_T760X_MODE_B_REG, val, st);
				ret = -1;
			}
			else
			{
				mode[i] = val;
			}
		}
	}

	return ret;
}

int
poe_set_mode_hw_port(poe_hw_port_t *pphp, uint8_t mode)
{
	int ret;
	uint8_t set, msk, reg;

	if(!pphp || pphp->chip_idx >= POE_CHIP_TOTAL ||
		pphp->pid != &POE_CHIP[pphp->chip_idx] || 
		pphp->port_idx >= pphp->pid->total_port || mode >= MODE_MAX)
	{
		DEBUG_MSG("Error:parameter in %s are invalid\n", __FUNCTION__);
		return -1;
	}

	set = mode << (pphp->port_idx * 2);
	msk = MODE_MASK << (pphp->port_idx * 2);

	if(pphp->port_idx >= 4 && pphp->pid->chip_type == POE_CHIP_TYPE_T7608R)
	{
		reg = POE_T760X_MODE_B_REG;
	}
	else
	{
		reg = POE_T760X_MODE_A_REG;
	}

	if((ret = poe_reg_modify(pphp->pid, reg, set, msk)) != E_OK)
	{
		DEBUG_MSG("Error:%s poe index:%d port:%d reg:0x%x set:0x%x mask:0x%x error return %d\n",
			"poe_reg_modify", pphp->chip_idx, pphp->port_idx, 
			reg, set, msk, ret);
	
		return -2;
	}

	return 0;
}

int
poe_set_mode_user_port(uint8_t user_port, uint8_t mode)
{
	poe_hw_port_t php, *pphp = &php;

	if(mode >= MODE_MAX)
	{
		DEBUG_MSG("Error:parameter mode %d incorrect\n", mode);
		return -1;
	}

	if(port_user_to_poe_chip(user_port, pphp) < 0)
	{
		DEBUG_MSG("Error:user port to poe chip fail\n");
		return -2;
	}

	return poe_set_mode_hw_port(pphp, mode);
}

int
poe_set_mode_chip(poe_id_t *pid, uint8_t mode)
{
	int ret, st;
	uint8_t val;

	if(mode >= MODE_MAX)
	{
		DEBUG_MSG("Error:parameter mode %d incorrect\n", mode);
		return -1;
	}

	val = (mode << 0) | (mode << 2) | 
		(mode << 4) | (mode << 6);

	if((st = poe_reg_write(pid, POE_T760X_MODE_A_REG, val)) != E_OK)
	{
		DEBUG_MSG("Error:%s poe index:%d reg:0x%x val:0x%x error return %d\n",
			"poe_reg_write", pid->chip_idx, POE_T760X_MODE_A_REG, val, st);
		ret = -2;
	}
	
	if(pid->chip_type == POE_CHIP_TYPE_T7608R)
	{
		if((st = poe_reg_write(pid, POE_T760X_MODE_B_REG, val)) != E_OK)
		{
			DEBUG_MSG("Error:%s poe index:%d reg:0x%x val:0x%x error return %d\n",
				"poe_reg_write", pid->chip_idx, POE_T760X_MODE_B_REG, val, st);
			ret = -3;
		}
	}

	return ret;
}


int
poe_set_mode_all(uint8_t mode)
{
	int i;
	poe_id_t *pid;

	if(mode >= MODE_MAX)
	{
		DEBUG_MSG("Error:parameter mode %d incorrect\n", mode);
		return -1;
	}

	for(i = 0, pid = &POE_CHIP[0]; i < POE_CHIP_TOTAL; i++, pid++)
	{
		if(!pid->valid)
		{
			continue;
		}
		poe_set_mode_chip(pid, mode);
	}

	return 0;
}

#if 1	/* wy,haven't modified because no more time,next time solve it */
int
poe_port_connecting_check_setting(uint8_t port, uint8_t en)
{
	int ret;
	uint8_t set, msk;
	poe_id_t *p;

	set = en ? (1 << port) : (0 << port);
	msk = (1 << port);

	p = &POE_PORT_2_CHIP(POE_CHIP, port);
	if(!p->valid)
	{
		return -1;
	}

	if((ret = poe_reg_modify(p, POE_T760X_DCD_EN_REG, set, msk)) != E_OK)
	{
		DEBUG_MSG("Error:%s poe index:%d port:%d reg:0x%x set:0x%x mask:0x%x error return %d\n",
			"poe_reg_modify", POE_PORT_2_CHIP_IDX(port), port, 
			POE_T760X_DCD_EN_REG, set, msk, ret);
	
		return -1;
	}

	return 0;
}

int
poe_port_connecting_check_setting_all(uint8_t en)
{
	int ret, st;
	uint8_t set, msk;
	uint8_t i;
	poe_id_t *p;

	for(i = 0, p = &POE_CHIP[0], ret = 0; i < POE_CHIP_TOTAL; i++, p++)
	{
		if(!p->valid)
		{
			continue;
		}

		set = en ? ((1 << p->total_port) - 1) : 0;
		msk = ((1 << p->total_port) - 1);

		if((st = poe_reg_modify(p, POE_T760X_DCD_EN_REG, set, msk)) != E_OK)
		{
			DEBUG_MSG("Error:%s poe index:%d reg:0x%x set:0x%x mask:0x%x error return %d\n",
				"poe_reg_modify", i, POE_T760X_DCD_EN_REG, set, msk, st);
			ret = -1;
		}
	}

	return ret;
}

int
poe_port_detect_setting(uint8_t port, uint8_t en)
{
	int ret;
	uint8_t set, msk;
	poe_id_t *p;

	set = en ? (1 << port) : (0 << port);
	msk = (1 << port);

	p = &POE_PORT_2_CHIP(POE_CHIP, port);
	if(!p->valid)
	{
		return -1;
	}

	if((ret = poe_reg_modify(p, POE_T760X_DET_EN_REG, set, msk)) != E_OK)
	{
		DEBUG_MSG("Error:%s poe index:%d port:%d reg:0x%x set:0x%x mask:0x%x error return %d\n",
			"poe_reg_modify", POE_PORT_2_CHIP_IDX(port), port, 
			POE_T760X_DET_EN_REG, set, msk, ret);
	
		return -1;
	}

	return 0;
}

int
poe_port_detect_setting_all(uint8_t en)
{
	int ret, st;
	uint8_t set, msk;
	uint8_t i;
	poe_id_t *p;

	for(i = 0, p = &POE_CHIP[0], ret = 0; i < POE_CHIP_TOTAL; i++, p++)
	{
		if(!p->valid)
		{
			continue;
		}

		set = en ? ((1 << p->total_port) - 1) : 0;
		msk = ((1 << p->total_port) - 1);

		if((st = poe_reg_modify(p, POE_T760X_DET_EN_REG, set, msk)) != E_OK)
		{
			DEBUG_MSG("Error:%s poe index:%d reg:0x%x set:0x%x mask:0x%x error return %d\n",
				"poe_reg_modify", i, POE_T760X_DET_EN_REG, set, msk, st);
			ret = -1;
		}
	}

	return ret;
}

int
poe_port_class_setting(uint8_t port, uint8_t en)
{
	int ret;
	uint8_t set, msk;
	poe_id_t *p;

	set = en ? (1 << port) : (0 << port);
	msk = (1 << port);

	p = &POE_PORT_2_CHIP(POE_CHIP, port);
	if(!p->valid)
	{
		return -1;
	}

	if((ret = poe_reg_modify(p, POE_T760X_CLS_EN_REG, set, msk)) != E_OK)
	{
		DEBUG_MSG("Error:%s poe index:%d port:%d reg:0x%x set:0x%x mask:0x%x error return %d\n",
			"poe_reg_modify", POE_PORT_2_CHIP_IDX(port), port, 
			POE_T760X_CLS_EN_REG, set, msk, ret);
	
		return -1;
	}

	return 0;
}

int
poe_port_class_setting_all(uint8_t en)
{
	int ret, st;
	uint8_t set, msk;
	uint8_t i;
	poe_id_t *p;

	for(i = 0, p = &POE_CHIP[0], ret = 0; i < POE_CHIP_TOTAL; i++, p++)
	{
		if(!p->valid)
		{
			continue;
		}

		set = en ? ((1 << p->total_port) - 1) : 0;
		msk = ((1 << p->total_port) - 1);

		if((st = poe_reg_modify(p, POE_T760X_CLS_EN_REG, set, msk)) != E_OK)
		{
			DEBUG_MSG("Error:%s poe index:%d reg:0x%x set:0x%x mask:0x%x error return %d\n",
				"poe_reg_modify", i, POE_T760X_CLS_EN_REG, set, msk, st);
			ret = -1;
		}
	}

	return ret;
}

int
poe_port_midspan_setting(uint8_t port, uint8_t en)
{
	int ret;
	uint8_t set, msk;
	poe_id_t *p;

	set = en ? (1 << port) : (0 << port);
	msk = (1 << port);

	p = &POE_PORT_2_CHIP(POE_CHIP, port);
	if(!p->valid)
	{
		return -1;
	}

	if((ret = poe_reg_modify(p, POE_T760X_MIDSPAN_EN_REG, set, msk)) != E_OK)
	{
		DEBUG_MSG("Error:%s poe index:%d port:%d reg:0x%x set:0x%x mask:0x%x error return %d\n",
			"poe_reg_modify", POE_PORT_2_CHIP_IDX(port), port, 
			POE_T760X_MIDSPAN_EN_REG, set, msk, ret);
	
		return -1;
	}

	return 0;
}

int
poe_port_midspan_setting_all(uint8_t en)
{
	int ret, st;
	uint8_t set, msk;
	uint8_t i;
	poe_id_t *p;

	for(i = 0, p = &POE_CHIP[0], ret = 0; i < POE_CHIP_TOTAL; i++, p++)
	{
		if(!p->valid)
		{
			continue;
		}

		set = en ? ((1 << p->total_port) - 1) : 0;
		msk = ((1 << p->total_port) - 1);

		if((st = poe_reg_modify(p, POE_T760X_MIDSPAN_EN_REG, set, msk)) != E_OK)
		{
			DEBUG_MSG("Error:%s poe index:%d reg:0x%x set:0x%x mask:0x%x error return %d\n",
				"poe_reg_modify", i, POE_T760X_MIDSPAN_EN_REG, set, msk, st);
			ret = -1;
		}
	}

	return ret;
}

int
poe_port_detect_btn_trigger(uint8_t port)
{
	int ret;
	uint8_t set, msk;
	poe_id_t *p;
	uint8_t on = 1;

	set = on ? (1 << port) : (0 << port);
	msk = (1 << port);

	p = &POE_PORT_2_CHIP(POE_CHIP, port);
	if(!p->valid)
	{
		return -1;
	}

	if((ret = poe_reg_modify(p, POE_T760X_DET_PB_REG, set, msk)) != E_OK)
	{
		DEBUG_MSG("Error:%s poe index:%d port:%d reg:0x%x set:0x%x mask:0x%x error return %d\n",
			"poe_reg_modify", POE_PORT_2_CHIP_IDX(port), port, 
			POE_T760X_DET_PB_REG, set, msk, ret);
	
		return -1;
	}

	return 0;
}

int
poe_port_detect_btn_trigger_all(void)
{
	int ret, st;
	uint8_t set, msk;
	uint8_t i;
	poe_id_t *p;
	uint8_t on = 1;

	for(i = 0, p = &POE_CHIP[0], ret = 0; i < POE_CHIP_TOTAL; i++, p++)
	{
		if(!p->valid)
		{
			continue;
		}

		set = on ? ((1 << p->total_port) - 1) : 0;
		msk = ((1 << p->total_port) - 1);

		if((st = poe_reg_modify(p, POE_T760X_DET_PB_REG, set, msk)) != E_OK)
		{
			DEBUG_MSG("Error:%s poe index:%d reg:0x%x set:0x%x mask:0x%x error return %d\n",
				"poe_reg_modify", i, POE_T760X_DET_PB_REG, set, msk, st);
			ret = -1;
		}
	}

	return ret;
}

int
poe_port_class_btn_trigger(uint8_t port)
{
	int ret;
	uint8_t set, msk;
	poe_id_t *p;
	uint8_t on = 1;

	set = on ? (1 << port) : (0 << port);
	msk = (1 << port);

	p = &POE_PORT_2_CHIP(POE_CHIP, port);
	if(!p->valid)
	{
		return -1;
	}

	if((ret = poe_reg_modify(p, POE_T760X_CLS_PB_REG, set, msk)) != E_OK)
	{
		DEBUG_MSG("Error:%s poe index:%d port:%d reg:0x%x set:0x%x mask:0x%x error return %d\n",
			"poe_reg_modify", POE_PORT_2_CHIP_IDX(port), port, 
			POE_T760X_CLS_PB_REG, set, msk, ret);
	
		return -1;
	}

	return 0;
}

int
poe_port_class_btn_trigger_all(void)
{
	int ret, st;
	uint8_t set, msk;
	uint8_t i;
	poe_id_t *p;
	uint8_t on = 1;

	for(i = 0, p = &POE_CHIP[0], ret = 0; i < POE_CHIP_TOTAL; i++, p++)
	{
		if(!p->valid)
		{
			continue;
		}
		
		set = on ? ((1 << p->total_port) - 1) : 0;
		msk = ((1 << p->total_port) - 1);

		if((st = poe_reg_modify(p, POE_T760X_CLS_PB_REG, set, msk)) != E_OK)
		{
			DEBUG_MSG("Error:%s poe index:%d reg:0x%x set:0x%x mask:0x%x error return %d\n",
				"poe_reg_modify", i, POE_T760X_CLS_PB_REG, set, msk, st);
			ret = -1;
		}
	}

	return ret;
}

int
poe_port_power_on_btn_trigger(uint8_t port)
{
	int ret;
	uint8_t set, msk;
	poe_id_t *p;
	uint8_t on = 1;

	set = on ? (1 << port) : (0 << port);
	msk = (1 << port);

	p = &POE_PORT_2_CHIP(POE_CHIP, port);
	if(!p->valid)
	{
		return -1;
	}

	if((ret = poe_reg_modify(p, POE_T760X_PWR_ON_REG, set, msk)) != E_OK)
	{
		DEBUG_MSG("Error:%s poe index:%d port:%d reg:0x%x set:0x%x mask:0x%x error return %d\n",
			"poe_reg_modify", POE_PORT_2_CHIP_IDX(port), port, 
			POE_T760X_PWR_ON_REG, set, msk, ret);
	
		return -1;
	}

	return 0;
}

int
poe_port_power_on_btn_trigger_all(void)
{
	int ret, st;
	uint8_t set, msk;
	uint8_t i;
	poe_id_t *p;
	uint8_t on = 1;

	for(i = 0, p = &POE_CHIP[0], ret = 0; i < POE_CHIP_TOTAL; i++, p++)
	{
		if(!p->valid)
		{
			continue;
		}

		set = on ? ((1 << p->total_port) - 1) : 0;
		msk = ((1 << p->total_port) - 1);

		if((st = poe_reg_modify(p, POE_T760X_PWR_ON_REG, set, msk)) != E_OK)
		{
			DEBUG_MSG("Error:%s poe index:%d reg:0x%x set:0x%x mask:0x%x error return %d\n",
				"poe_reg_modify", i, POE_T760X_PWR_ON_REG, set, msk, st);
			ret = -1;
		}
	}

	return ret;
}

int
poe_port_power_off_btn_trigger(uint8_t port)
{
	int ret;
	uint8_t set, msk;
	poe_id_t *p;
	uint8_t on = 1;

	set = on ? (1 << port) : (0 << port);
	msk = (1 << port);

	p = &POE_PORT_2_CHIP(POE_CHIP, port);
	if(!p->valid)
	{
		return -1;
	}

	if((ret = poe_reg_modify(p, POE_T760X_PWR_OFF_REG, set, msk)) != E_OK)
	{
		DEBUG_MSG("Error:%s poe index:%d port:%d reg:0x%x set:0x%x mask:0x%x error return %d\n",
			"poe_reg_modify", POE_PORT_2_CHIP_IDX(port), port, 
			POE_T760X_PWR_OFF_REG, set, msk, ret);
	
		return -1;
	}

	return 0;
}

int
poe_port_power_off_btn_trigger_all(void)
{
	int ret, st;
	uint8_t set, msk;
	uint8_t i;
	poe_id_t *p;
	uint8_t on = 1;

	for(i = 0, p = &POE_CHIP[0], ret = 0; i < POE_CHIP_TOTAL; i++, p++)
	{
		if(!p->valid)
		{
			continue;
		}

		set = on ? ((1 << p->total_port) - 1) : 0;
		msk = ((1 << p->total_port) - 1);

		if((st = poe_reg_modify(p, POE_T760X_PWR_OFF_REG, set, msk)) != E_OK)
		{
			DEBUG_MSG("Error:%s poe index:%d reg:0x%x set:0x%x mask:0x%x error return %d\n",
				"poe_reg_modify", i, POE_T760X_PWR_OFF_REG, set, msk, st);
			ret = -1;
		}
	}

	return ret;
}
#endif

int
poe_reset_btn_trigger_chip(poe_id_t *pid)
{
	int ret;
	uint8_t set;
	uint8_t on = 1;

	if(!pid || !pid->valid)
	{
		DEBUG_MSG("Error:parameter in %s are invalid\n", __FUNCTION__);
		return -1;
	}

	set = on ? ((1 << pid->total_port) - 1) : 0;

	if((ret = poe_reg_write(pid, POE_T760X_RST_PORT_REG, set)) != E_OK)
	{
		DEBUG_MSG("Error:%s reg:0x%x val:0x%x error return %d\n",
			"poe_reg_write", POE_T760X_RST_PORT_REG, set, ret);
	
		return -2;
	}

	return ret;
}

int
poe_reset_btn_trigger_all(void)
{
	int ret, st;
	uint8_t i;
	poe_id_t *pid;

	ret = 0;
	for(i = 0, pid = &POE_CHIP[0], ret = 0; i < POE_CHIP_TOTAL; i++, pid++)
	{
		if(!pid->valid)
		{
			continue;
		}

		if((st = poe_reset_btn_trigger_chip(pid)) != E_OK)
		{
			DEBUG_MSG("Error:%s chip index %d error return %d\n",
				"poe_reset_btn_trigger_chip", pid->chip_idx, st);
			ret |= (1 << pid->chip_idx);
		}
	}

	return ret;
}

int
poe_reset_btn_trigger_hw_port(poe_hw_port_t *pphp)
{
	int ret;
	uint8_t set, msk;
	uint8_t on = 1;

	if(!pphp || pphp->chip_idx >= POE_CHIP_TOTAL ||
		pphp->pid != &POE_CHIP[pphp->chip_idx] || 
		pphp->port_idx >= pphp->pid->total_port)
	{
		DEBUG_MSG("Error:parameter in %s are invalid\n", __FUNCTION__);
		return -1;
	}

	set = on ? (1 << pphp->port_idx) : (0 << pphp->port_idx);
	msk = (1 << pphp->port_idx);

	if((ret = poe_reg_modify(pphp->pid, POE_T760X_RST_PORT_REG, set, msk)) != E_OK)
	{
		DEBUG_MSG("Error:%s poe index:%d port:%d reg:0x%x set:0x%x mask:0x%x error return %d\n",
			"poe_reg_modify", POE_PORT_2_CHIP_IDX(pphp->port_idx), pphp->port_idx, 
			POE_T760X_RST_PORT_REG, set, msk, ret);
	
		return -2;
	}

	return 0;
}

int
poe_reset_btn_trigger_user_port(uint8_t user_port)
{
	poe_hw_port_t php, *pphp = &php;

	if(port_user_to_poe_chip(user_port, pphp) < 0)
	{
		DEBUG_MSG("Error:user port to poe chip fail\n");
		return -1; 
	}

	return poe_reset_btn_trigger_hw_port(pphp);
}

int
poe_recover_from_reset_non_auto_hw_port(poe_hw_port_t *pphp, uint8_t mode)
{
	int ret;
	uint8_t port, reg, set, msk;

	if(!pphp || pphp->chip_idx >= POE_CHIP_TOTAL ||
		pphp->pid != &POE_CHIP[pphp->chip_idx] || 
		pphp->port_idx >= pphp->pid->total_port || mode >= MODE_MAX)
	{
		DEBUG_MSG("Error:parameter in %s are invalid\n", __FUNCTION__);
		return -1;
	}

	port = pphp->port_idx % 4;
	set = (mode & MODE_MASK) << (port * 2);
	msk = MODE_MASK << (port * 2);
	reg = (pphp->port_idx < 4) ? POE_T760X_MODE_A_REG : POE_T760X_MODE_B_REG;

	if((ret = poe_reg_modify(pphp->pid, reg, set, msk)) != E_OK)
	{
		DEBUG_MSG("Error:%s poe index:%d port:%d reg:0x%x set:0x%x mask:0x%x error return %d\n",
			"poe_reg_modify", pphp->chip_idx, pphp->port_idx, reg, set, msk, ret);
	
		return -2;
	}

	set = (1 << pphp->port_idx);
	msk = (1 << pphp->port_idx);

	poe_reg_modify(pphp->pid, POE_T760X_DCD_EN_REG, set, msk);
	poe_reg_modify(pphp->pid, POE_T760X_DET_EN_REG, set, msk);
	poe_reg_modify(pphp->pid, POE_T760X_CLS_EN_REG, set, msk);
	poe_reg_modify(pphp->pid, POE_T760X_MIDSPAN_EN_REG, set, msk);

	if((mode & MODE_MASK) != MODE_AUTO)
	{
		poe_reg_write(pphp->pid, POE_T760X_PWR_ON_REG, set);
	}

	return 0;
}

int
poe_recover_from_reset_non_auto_chip(poe_id_t *pid, uint8_t mode)
{
	uint8_t val;

	if(!pid || !pid->valid || mode >= MODE_MAX)
	{
		DEBUG_MSG("Error:parameter invalid\n");
		return -1;
	}

	val = (mode << 0) | (mode << 2) | 
		(mode << 4) | (mode << 6);
	poe_reg_write(pid, POE_T760X_MODE_A_REG, val);
	if(pid->chip_type == POE_CHIP_TYPE_T7608R)
	{
		poe_reg_write(pid, POE_T760X_MODE_B_REG, val);
	}
	
	val = (pid->chip_type == POE_CHIP_TYPE_T7608R) ? 0xff : 0x0f;

	poe_reg_write(pid, POE_T760X_DCD_EN_REG, val);
	poe_reg_write(pid, POE_T760X_DET_EN_REG, val);
	poe_reg_write(pid, POE_T760X_CLS_EN_REG, val);
	poe_reg_write(pid, POE_T760X_MIDSPAN_EN_REG, val);

	if((mode & MODE_MASK) != MODE_AUTO)
	{
		poe_reg_write(pid, POE_T760X_PWR_ON_REG, val);
	}

	return 0;
}

int
poe_recover_from_reset_non_auto_all(uint8_t mode)
{
	int i;
	poe_id_t *pid;

	for(i = 0, pid = &POE_CHIP[0]; i < POE_CHIP_TOTAL; i++, pid++)
	{
		if(!pid->valid)
		{
			continue;
		}
		poe_recover_from_reset_non_auto_chip(pid, mode);
	}

	return 0;
}

int
poe_recover_only_non_auto_hw_port(poe_hw_port_t *pphp, uint8_t mode)
{
	int ret;
	uint8_t port, reg, set, msk, val;

	if(!pphp || pphp->chip_idx >= POE_CHIP_TOTAL ||
		pphp->pid != &POE_CHIP[pphp->chip_idx] || 
		pphp->port_idx >= pphp->pid->total_port || mode >= MODE_MAX)
	{
		DEBUG_MSG("Error:parameter in %s are invalid\n", __FUNCTION__);
		return -1;
	}

	port = pphp->port_idx % 4;
	set = (mode & MODE_MASK) << (port * 2);
	msk = MODE_MASK << (port * 2);
	reg = (pphp->port_idx < 4) ? POE_T760X_MODE_A_REG : POE_T760X_MODE_B_REG;

	if((ret = poe_reg_modify(pphp->pid, reg, set, msk)) != E_OK)
	{
		DEBUG_MSG("Error:%s poe index:%d port:%d reg:0x%x set:0x%x mask:0x%x error return %d\n",
			"poe_reg_modify", pphp->chip_idx, pphp->port_idx, reg, set, msk, ret);
	
		return -2;
	}

	val = 0xff;
	poe_reg_write(pphp->pid, POE_T760X_CHIP_TDISC_REG, val);

	set = (1 << pphp->port_idx);
	msk = (1 << pphp->port_idx);

	poe_reg_modify(pphp->pid, POE_T760X_DCD_EN_REG, set, msk);
	poe_reg_modify(pphp->pid, POE_T760X_DET_EN_REG, set, msk);
	poe_reg_modify(pphp->pid, POE_T760X_CLS_EN_REG, set, msk);
	poe_reg_modify(pphp->pid, POE_T760X_MIDSPAN_EN_REG, set, msk);

	return 0;
}

int
poe_recover_only_non_auto_chip(poe_id_t *pid, uint8_t mode)
{
	uint8_t val;

	if(!pid || !pid->valid || mode >= MODE_MAX)
	{
		DEBUG_MSG("Error:parameter invalid\n");
		return -1;
	}

	val = (mode << 0) | (mode << 2) | 
		(mode << 4) | (mode << 6);
	poe_reg_write(pid, POE_T760X_MODE_A_REG, val);
	if(pid->chip_type == POE_CHIP_TYPE_T7608R)
	{
		poe_reg_write(pid, POE_T760X_MODE_B_REG, val);
	}
	
	val = (pid->chip_type == POE_CHIP_TYPE_T7608R) ? 0xff : 0x0f;

	poe_reg_write(pid, POE_T760X_DCD_EN_REG, val);
	poe_reg_write(pid, POE_T760X_DET_EN_REG, val);
	poe_reg_write(pid, POE_T760X_CLS_EN_REG, val);
	poe_reg_write(pid, POE_T760X_MIDSPAN_EN_REG, val);

	if((mode & MODE_MASK) != MODE_AUTO)
	{
		poe_reg_write(pid, POE_T760X_PWR_ON_REG, val);
	}

	return 0;
}

int
poe_recover_only_non_auto_all(uint8_t mode)
{
	int i;
	poe_id_t *pid;

	for(i = 0, pid = &POE_CHIP[0]; i < POE_CHIP_TOTAL; i++, pid++)
	{
		if(!pid->valid)
		{
			continue;
		}
		poe_recover_only_non_auto_chip(pid, mode);
	}

	return 0;
}

int
poe_reset_btn_trigger_non_auto_hw_port(poe_hw_port_t *pphp)
{
	int ret;

	if((ret = poe_reset_btn_trigger_hw_port(pphp)) != E_OK)
	{
		DEBUG_MSG("Error:%s chip index %d error return %d\n",
			"poe_reset_btn_trigger_hw_port", pphp->chip_idx, ret);
		return ret;
	}

	return poe_recover_from_reset_non_auto_hw_port(pphp, POE_CHIP_MODE);
}

int
poe_reset_btn_trigger_non_auto_user_port(uint8_t user_port)
{
	poe_hw_port_t php, *pphp = &php;

	if(port_user_to_poe_chip(user_port, pphp) < 0)
	{
		DEBUG_MSG("Error:user port to poe chip fail\n");
		return -1; 
	}

	return poe_reset_btn_trigger_non_auto_hw_port(pphp);
}

int
poe_reset_btn_trigger_non_auto_all(void)
{
	poe_reset_btn_trigger_all();
	poe_recover_from_reset_non_auto_all(POE_CHIP_MODE);

	return 0;
}

int
poe_power_off_btn_trigger_non_auto_hw_port(poe_hw_port_t *pphp)
{
	return poe_reset_btn_trigger_hw_port(pphp);
}

int
poe_power_off_btn_trigger_non_auto_user_port(uint8_t user_port)
{
	return poe_reset_btn_trigger_user_port(user_port);
}

int
poe_power_off_btn_trigger_non_auto_all(void)
{
	return poe_reset_btn_trigger_all();
}

int
poe_power_on_btn_trigger_non_auto_hw_port(poe_hw_port_t *pphp)
{
	return poe_recover_from_reset_non_auto_hw_port(pphp, POE_CHIP_MODE);
}

int
poe_power_on_btn_trigger_non_auto_user_port(uint8_t user_port)
{
	poe_hw_port_t php, *pphp = &php;

	if(port_user_to_poe_chip(user_port, pphp) < 0)
	{
		DEBUG_MSG("Error:user port to poe chip fail\n");
		return -1; 
	}

	return poe_recover_from_reset_non_auto_hw_port(pphp, POE_CHIP_MODE);
}

int
poe_power_on_btn_trigger_non_auto_all(void)
{
	poe_recover_from_reset_non_auto_all(POE_CHIP_MODE);
	return 0;
}

int
poe_port_enable_cls4p_hw_port(poe_hw_port_t *pphp, uint8_t en)
{
	int ret;
	uint8_t set, msk;

	if(!pphp || pphp->chip_idx >= POE_CHIP_TOTAL ||
		pphp->pid != &POE_CHIP[pphp->chip_idx] || 
		pphp->port_idx >= pphp->pid->total_port || en >= MODE_MAX)
	{
		DEBUG_MSG("Error:parameter in %s are invalid\n", __FUNCTION__);
		return -1;
	}

	set = en ? (1 << pphp->port_idx) : (0 << pphp->port_idx);
	msk = (1 << pphp->port_idx);

	if((ret = poe_reg_modify(pphp->pid, POE_T760X_EN_CLS4P_REG, set, msk)) != E_OK)
	{
		DEBUG_MSG("Error:%s poe index:%d port:%d reg:0x%x set:0x%x mask:0x%x error return %d\n",
			"poe_reg_modify", pphp->chip_idx, pphp->port_idx, 
			POE_T760X_EN_CLS4P_REG, set, msk, ret);
	
		return -1;
	}

	return 0;
}

int
poe_port_enable_cls4p_user_port(uint8_t user_port, uint8_t en)
{
	poe_hw_port_t php, *pphp = &php;

	if(port_user_to_poe_chip(user_port, pphp) < 0)
	{
		DEBUG_MSG("Error:user port to poe chip fail\n");
		return -1; 
	}

	return poe_port_enable_cls4p_hw_port(pphp, en);
}

int
poe_port_enable_cls4p_chip(poe_id_t *pid, uint8_t en)
{
	int ret;
	uint8_t val;

	if(!pid || !pid->valid)
	{
		DEBUG_MSG("Error:parameter invalid\n");
		return -1;
	}

	val = en ? 0xff : 0x00;
	val = (pid->chip_type == POE_CHIP_TYPE_T7608R) ? val : (val & 0xf);

	if((ret = poe_reg_write(pid, POE_T760X_EN_CLS4P_REG, val)) != E_OK)
	{
		DEBUG_MSG("Error:%s poe index:%d reg:0x%x val:0x%x error return %d\n",
			"poe_reg_write", pid->chip_idx, POE_T760X_EN_CLS4P_REG, val, ret);
	
		return -1;
	}

	return 0;
}

int
poe_port_enable_cls4p_all(uint8_t en)
{
	int i;
	poe_id_t *pid;

	for(i = 0, pid = &POE_CHIP[0]; i < POE_CHIP_TOTAL; i++, pid++)
	{
		if(!pid->valid)
		{
			continue;
		}
		poe_port_enable_cls4p_chip(pid, en);
	}

	return 0;
}

int
poe_port_init(poe_id_t *p, uint8_t mode)
{
	uint8_t omd, val[POE_MAX_CURRENT_CHIP * 2], port, chip;

	if(mode >= MODE_MAX)
	{
		DEBUG_MSG("Error:port mode %d unknown\n", mode);
		return -1;
	}

	poe_get_mode_all(&val[0], NUMOFARRAY(val));
	for(chip = 0; chip < NUMOFARRAY(val); chip++)
	{
		for(port = 0; port < p->total_port; port++)
		{
			omd = (val[chip] >> (port * 2)) & MODE_MASK;
			DEBUG_MSG("Get poe chip index %d port %d mode is %d\n", chip, port, omd);
		}
	}

	poe_set_mode_all(mode);
	poe_port_connecting_check_setting_all(mode);
	poe_port_detect_setting_all(mode);
	poe_port_class_setting_all(mode);
	poe_port_midspan_setting_all(mode);

	return 0;
}

int
poe_int_en(poe_id_t *p, uint8_t en)
{
	int ret;
	uint8_t msk, set;

	set = en ? INT_MSK_ALL : 0;
	msk = INT_MSK_ALL;
	if((ret = poe_reg_modify(p, POE_T760X_INT_MSK_REG, set, msk)) != E_OK)
	{
		DEBUG_MSG("Error:%s reg:0x%x set:0x%x mask:0x%x error return %d\n",
			"poe_reg_modify", POE_T760X_INT_MSK_REG, set, msk, ret);

		return -1;
	}

	set = en ? INT_EN : 0;
	msk = INT_EN_MSK;
	if((ret = poe_reg_modify(p, POE_T760X_MISC_CFG_REG, set, msk)) != E_OK)
	{
		DEBUG_MSG("Error:%s reg:0x%x set:0x%x mask:0x%x error return %d\n",
			"poe_reg_modify", POE_T760X_MISC_CFG_REG, set, msk, ret);

		return -2;
	}

	return 0;
}

int
poe_int_show(char *int_name, uint8_t ports_msk, uint8_t base_port)
{
	char i, c, out = 0;
	char tbuf[64];

	c = 0;
	c += osal_snprintf(tbuf + c, sizeof(tbuf) - c, "Interrupt [%s] occur on port:", int_name);
	for(i = 0; i < POE_MAX_PORT_PER_CHIP; i++)
	{
		if(ports_msk & BIT(i))
		{
			c += osal_snprintf(tbuf + c, sizeof(tbuf) - c, "%d ", base_port + i);
			out = 1;
		}
	}

	if(out)
	{
		tbuf[sizeof(tbuf) - 1] = '\0';
		osal_printf("%s\n", tbuf);
	}

	return 0;
}

int
poe_int_hdl_pwr_en_changed(poe_id_t *p, int_special_hdl_t *pish)
{
	int ret;
	uint8_t ports_msk, st_msk, i, port;

	if((ret = poe_reg_read(p, pish->reg_int, &ports_msk)) != E_OK)
	{
		DEBUG_MSG("Error:%s reg:0x%x val:0x%x error return %d\n",
			"poe_reg_read", pish->reg_int, ports_msk, ret);

		return -1;
	}

	poe_int_show(pish->int_name, ports_msk, p->base_port);

	if((ret = poe_reg_read(p, pish->reg_special, &st_msk)) != E_OK)
	{
		DEBUG_MSG("Error:%s reg:0x%x val:0x%x error return %d\n",
			"poe_reg_read", pish->reg_special, st_msk, ret);

		return -2;
	}

	for(i = 0; i < POE_MAX_PORT_PER_CHIP; i++)
	{
		if(ports_msk & BIT(i))
		{
			port = p->base_port + i;
			poe_info_set_status(port, (st_msk & BIT(i)) ? 
				POE_PORT_STATUS_POWER_ENABLED : POE_PORT_STATUS_POWER_DISABLED);

			osal_printf("Port %d,Power Enable %s\n", port, (st_msk & BIT(i)) ? "ON" : "OFF");
		}
	}

	return 0;
}

int
poe_int_hdl_pwr_good_changed(poe_id_t *p, int_special_hdl_t *pish)
{
	int ret;
	uint8_t ports_msk, st_msk, i, port;

	if((ret = poe_reg_read(p, pish->reg_int, &ports_msk)) != E_OK)
	{
		DEBUG_MSG("Error:%s reg:0x%x val:0x%x error return %d\n",
			"poe_reg_read", pish->reg_int, ports_msk, ret);

		return -1;
	}

	poe_int_show(pish->int_name, ports_msk, p->base_port);

	if((ret = poe_reg_read(p, pish->reg_special, &st_msk)) != E_OK)
	{
		DEBUG_MSG("Error:%s reg:0x%x val:0x%x error return %d\n",
			"poe_reg_read", pish->reg_special, st_msk, ret);

		return -2;
	}

	for(i = 0; i < POE_MAX_PORT_PER_CHIP; i++)
	{
		if(ports_msk & BIT(i))
		{
			port = p->base_port + i;
			poe_info_set_status(port, (st_msk & BIT(i)) ? 
				POE_PORT_STATUS_POWER_GOOD : POE_PORT_STATUS_POWER_BAD);

			osal_printf("Port %d,Power %s\n", port, (st_msk & BIT(i)) ? "GOOD" : "BAD");
		}
	}

	return 0;
}

int
poe_int_hdl_disconnect(poe_id_t *p, int_special_hdl_t *pish)
{
	int ret;
	uint8_t ports_msk, i, port;

	if((ret = poe_reg_read(p, pish->reg_int, &ports_msk)) != E_OK)
	{
		DEBUG_MSG("Error:%s reg:0x%x val:0x%x error return %d\n",
			"poe_reg_read", pish->reg_int, ports_msk, ret);

		return -1;
	}

	poe_int_show(pish->int_name, ports_msk, p->base_port);

	for(i = 0; i < POE_MAX_PORT_PER_CHIP; i++)
	{
		if(ports_msk & BIT(i))
		{
			port = p->base_port + i;
			poe_info_set_status(port, POE_PORT_STATUS_PWR_DN_BY_DISCONNECTED);
		}
	}

	return 0;
}

int
poe_int_hdl_detect_done(poe_id_t *p, int_special_hdl_t *pish)
{
	int ret;
	uint8_t ports_msk, val, i;
	poe_hw_port_t php, *pphp = &php;

	if((ret = poe_reg_read(p, pish->reg_int, &ports_msk)) != E_OK)
	{
		DEBUG_MSG("Error:%s reg:0x%x val:0x%x error return %d\n",
			"poe_reg_read", pish->reg_int, ports_msk, ret);

		return -1;
	}

	poe_int_show(pish->int_name, ports_msk, p->base_port);

	for(i = 0; i < p->total_port; i++)
	{
		if(ports_msk & BIT(i))
		{
			pphp->chip_idx = p->chip_idx;
			pphp->port_idx = i;
			pphp->pid = p;

			if((ret = poe_get_det_hw_port(pphp, &val)) != 0)
			{
				DEBUG_MSG("Error:%s port:%d error return %d\n",
					"poe_port_get_det", i, ret);

				return -2;
			}

			osal_printf("Port %d,detection %d[%s],\n", p->base_port + i, val, poe_detect_id_2_str(val));
		}
	}

	return 0;
}

int
poe_int_hdl_class_done(poe_id_t *p, int_special_hdl_t *pish)
{
	int ret;
	uint8_t ports_msk, val, i;
	poe_hw_port_t pcp, *ppcp = &pcp;

	if((ret = poe_reg_read(p, pish->reg_int, &ports_msk)) != E_OK)
	{
		DEBUG_MSG("Error:%s reg:0x%x val:0x%x error return %d\n",
			"poe_reg_read", pish->reg_int, ports_msk, ret);

		return -1;
	}

	poe_int_show(pish->int_name, ports_msk, p->base_port);

	for(i = 0; i < POE_MAX_PORT_PER_CHIP; i++)
	{
		if(ports_msk & BIT(i))
		{
			ppcp->pid = p;
			ppcp->chip_idx = p->chip_idx;
			ppcp->port_idx = i;
			if((ret = poe_get_cls_hw_port(ppcp, &val)) != 0)
			{
				DEBUG_MSG("Error:%s port:%d error return %d\n",
					"poe_port_get_cls", i, ret);

				return -2;
			}

			osal_printf("Port %d,classification %d[%s],\n", p->base_port + i, val, poe_class_id_2_str(val));
		}
	}

	return 0;
}

int
poe_int_hdl_over_current_after_power_on(poe_id_t *p, int_special_hdl_t *pish)
{
	int ret;
	uint8_t ports_msk;

	if((ret = poe_reg_read(p, pish->reg_int, &ports_msk)) != E_OK)
	{
		DEBUG_MSG("Error:%s reg:0x%x val:0x%x error return %d\n",
			"poe_reg_read", pish->reg_int, ports_msk, ret);

		return -1;
	}

	poe_int_show(pish->int_name, ports_msk, p->base_port);

	return 0;
}

int
poe_int_hdl_limit_current_on_power_up(poe_id_t *p, int_special_hdl_t *pish)
{
	int ret;
	uint8_t ports_msk;

	if((ret = poe_reg_read(p, pish->reg_int, &ports_msk)) != E_OK)
	{
		DEBUG_MSG("Error:%s reg:0x%x val:0x%x error return %d\n",
			"poe_reg_read", pish->reg_int, ports_msk, ret);

		return -1;
	}

	poe_int_show(pish->int_name, ports_msk, p->base_port);

	return 0;
}

int
poe_int_hdl_supply(poe_id_t *p, int_special_hdl_t *pish)
{
	int ret;
	uint8_t reason, ports_msk, i, val;

	if((ret = poe_reg_read(p, pish->reg_int, &reason)) != E_OK)
	{
		DEBUG_MSG("Error:%s reg:0x%x val:0x%x error return %d\n",
				"poe_reg_read", pish->reg_int, reason, ret);

		return -1;
	}

	osal_printf("Interrupt [%s] occured\n", pish->int_name);

	if(reason & BIT(SUPPLY_REQUEST_CURRENT_OVERFLOW))
	{
		osal_printf("No more current for request of port when power it up\n");
	}
	else if(reason & BIT(SUPPLY_CONSUMPTION_OVER_MAX_CURRENT))
	{
		osal_printf("The total consumption current over the MAX current by setting\n");
	}
	else if(reason & BIT(SUPPLY_CHIP_RESET_FROM_V54_LOW))
	{
		osal_printf("Chip reset ok from V54 low\n");
	}
	else if(reason & BIT(SUPPLY_CHIP_RESET_FROM_V3P3_LOW))
	{
		osal_printf("Chip reset ok from V3P3 low\n");
	}
	else if(reason & BIT(SUPPLY_FET_BAD))
	{
		ports_msk = 0;
		for(i = 0; i < POE_MAX_PORT_PER_CHIP; i++)
		{
			if((ret = poe_reg_read(p, POE_T760X_PORT_HIGH_PWR_ST_REG(i), &val)) != E_OK)
			{
				DEBUG_MSG("Error:%s reg:0x%x val:0x%x error return %d\n",
					"poe_reg_read", POE_T760X_PORT_HIGH_PWR_ST_REG(i), val, ret);

				return -2;
			}
	
			if(val & FET_BAD)
			{
				ports_msk |= (1 << i);
			}
		}
		poe_int_show("FET Bad", ports_msk, p->base_port);
	}
	else if(reason & BIT(SUPPLY_CHIP_RESET_FROM_T150))
	{
		osal_printf("Chip reset ok from Temperature over the 150c\n");
	}

	return 0;
}

int
poe_int_hdl_limit_current_on_power_on(poe_id_t *p, int_special_hdl_t *pish)
{
	int ret;
	uint8_t ports_msk;

	if((ret = poe_reg_read(p, pish->reg_int, &ports_msk)) != E_OK)
	{
		DEBUG_MSG("Error:%s reg:0x%x val:0x%x error return %d\n",
			"poe_reg_read", pish->reg_int, ports_msk, ret);

		return -1;
	}

	poe_int_show(pish->int_name, ports_msk, p->base_port);

	return 0;
}

int
poe_int_hdl(uint8_t intidx)
{
	int ret;
	uint8_t val, i;
	int_special_hdl_t *pish;
	poe_id_t *p;

	if(intidx >= POE_CHIP_TOTAL)
	{
		DEBUG_MSG("Error:index of interrupt %d overflow the chip index %d\n", 
			intidx, POE_MAX_CURRENT_CHIP);
		return -1;
	}
	p = &POE_CHIP[intidx];

	/* Get interrupt status register */
	if((ret = poe_reg_read(p, POE_T760X_INT_REG, &val)) != E_OK)
	{
		DEBUG_MSG("Error:%s reg:0x%x val:0x%x error return %d\n",
			"poe_reg_read", POE_T760X_INT_REG, val, ret);

		return -2;
	}

	if(!val)
	{
		DEBUG_MSG("POE interrupt has no occured on chip index %d\n", intidx);
		return 0;
	}

	for(i = 0, pish = &ishdl[0]; i < NUMOFARRAY(ishdl); i++, pish++)
	{
		if(val & pish->int_type_msk)
		{
			if(pish->int_hdl)
			{
				ret = pish->int_hdl(p, pish);
				DEBUG_MSG("Do interrupt [%s] on chip index %d return %s[%d]\n", 
					pish->int_name, intidx, ret ? "failure" : "success", ret);
			}
		}
	}

	return 0;
}

int
poe_init(void)
{
	uint8_t i, id, ver;

	poe_info_init();

	for(i = 0; i < POE_CHIP_TOTAL; i++)
	{
		poe_get_id_chip(&POE_CHIP[i], &id, &ver);
		printf("Chip[%d] ID:0x%x version:0x%x,chip is %s\n", 
			i, id, ver, id == 0x1a ? "TM7604R" : "unknown");

		poe_int_en(&POE_CHIP[i], 1);	/* Enable interrupt */
		poe_port_init(&POE_CHIP[i], MODE_SEMIAUTO);		/* For priority in software */
	}

	return 0;
}

int
poe_init_auto_mode(void)
{
	poe_status_routing_e en_mode = 0;

#if 0
	/* debug message enabled */
	en_mode = POE_STATUS_ON | POE_STATUS_TIMER_DBG_EN | 
			POE_STATUS_PRINT_EN | POE_STATUS_FREERTOS_DBG_EN;
#else
	/* debug message disabled */
	en_mode = POE_STATUS_ON;
#endif
	poe_status_routing(en_mode);
	return 0;
}

void 
poe_task( void *pvParameters )
{
	uint8_t i;
	/* do I2C init first */
	i2c_init(0, 0);
	i2c_init(0, 1);

	/* initialize poe device */
	poe_init();

	while(1)
	{
#if defined(POE_INTERRUPT_ENABLE) && !POE_INTERRUPT_ENABLE
		for(i = 0; i < POE_CHIP_TOTAL; i++)
		{
			poe_int_hdl(i);
		}
#endif
		poe_pwr_routing();

		os_sleep(200);
	}
}

void 
create_poe_task(void)
{
	xTaskCreate(poe_task,            	/* The function that implements the task. */
		"POE",                          /* The text name assigned to the task - for debug only as it is not used by the kernel. */
		(1024), /* The size of the stack to allocate to the task. */
		NULL,                           /* The parameter passed to the task - not used in this simple case. */
		(tskIDLE_PRIORITY + 3),         /* The priority assigned to the task. */
		NULL );                         /* The task handle is not required, so NULL is passed. */

	return;
}


