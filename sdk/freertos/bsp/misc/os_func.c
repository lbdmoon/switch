
/* Standard includes. */
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <machine/endian.h>

/* Kernel includes. */
#include <FreeRTOS.h>
#include "FreeRTOSConfig.h"
#include "task.h"
#include "timers.h"
#include "queue.h"
#include "osal.h"

/* module includes */
#include <poe.h>
#include <os_func.h>
#include <air_i2c.h>
#include <i2c_cmd.h>
#include <pp_def.h>
#include <air_l2.h>
#include <product.h>
#include <test.h>

#if !defined(SKU_2149) && !defined(SKU_2150) && !defined(SKU_2151)
#error "!!!Must defind 2149 or 2150 or 2151!!!"
#endif

#ifdef SKU_2149
char *__mw_product_str = "AN8855M_5P_1P";
uint8_t __mw_product_id = MW_PRODUCT_ID_AN8855M_5P_1P;

poe_id_t poe_id_all_5p_1p[1] = 
{
	[0] = {
			.valid = 1,
			.unit = 0,
			.chn = 0,
			.dev = 0x50,			/* PCB2149 U2 */
			.chip_idx = 0,
			.base_port = 0 * POE_MAX_PORT_PER_CHIP,
			.chip_type = POE_CHIP_TYPE_T7604R,
			.total_port = MAX_PORT_T7604,
		},
};
//uint8_t poe_id_all_5p_1p_total = NUMOFARRAY(poe_id_all_5p_1p);
#endif

#ifdef SKU_2150
char *__mw_product_str = "AN8855M_5P_1SFP";
uint8_t __mw_product_id = MW_PRODUCT_ID_AN8855M_5P_1SFP;

poe_id_t poe_id_all_5p_1sfp[1] = 
{
	[0] = {
			.valid = 1,
			.unit = 0,
			.chn = 0,
			.dev = 0x54,			/* PCB2150 U2 */
			.chip_idx = 0,
			.base_port = 0 * POE_MAX_PORT_PER_CHIP,
			.chip_type = POE_CHIP_TYPE_T7604R,
			.total_port = MAX_PORT_T7604,
		},
};
//uint8_t poe_id_all_5p_1sfp_total = NUMOFARRAY(poe_id_all_5p_1sfp);
#endif

#ifdef SKU_2151
char *__mw_product_str = "AN8855M_5P_5P";
uint8_t __mw_product_id = MW_PRODUCT_ID_AN8855M_5P_5P;

poe_id_t poe_id_all_5p_5p[1] = 
{
	[0] = {
			.valid = 1,
			.unit = 0,
			.chn = 0,
			.dev = 0x72,			/* PCB2151 U2 */
			.chip_idx = 0,
			.base_port = 0 * POE_MAX_PORT_PER_CHIP,
			.chip_type = POE_CHIP_TYPE_T7608R,
			.total_port = MAX_PORT_T7608,
		},
};
//uint8_t poe_id_all_5p_5p_total = NUMOFARRAY(poe_id_all_5p_5p);
#endif

static TimerHandle_t time_local_timer_hdl;
static time_local_tick_t tlt = {0,};
static board_port_cfg_t *pboard_port_cfg = NULL;
static board_port_cfg_t board_port_cfg[] = 
{
#ifdef SKU_2149
	[0] = {/* default board config */
			.mp_id = MW_PRODUCT_ID_AN8855M_5P_1P,
			.mp_str = "AN8855M_5P_1P",
			.sw = {
					.port_total = 1 + 5 + 1,
					.port_total_cpu = 1,
					.port_total_asic = 5 + 1,
					.port_total_uplink = 1,

					.port_cpu_start_idx = 0,
					.port_cpu_end_idx = 0,
					.port_asic_start_idx = 1,
					.port_asic_end_idx = 5 + 1,
					.port_uplink_start_idx = 6,
					.port_uplink_end_idx = 6,
				},
			.poe = {
					.port_total = 4,
					.port_user_start_idx = 1,
					.port_user_end_idx = 4,
					.port_flat_start_idx = 0,
					.port_flat_end_idx = 3,
					.port_map_type = POE_PORT_MAP_DIRECTLY,
				},
			.poe_chip = &poe_id_all_5p_1p[0],
			.poe_chip_total = NUMOFARRAY(poe_id_all_5p_1p),
			.poe_max_board_pwr_mw = MAX_POE_POWER_MW,
			.poe_usable_board_pwr_mw = USABLE_POE_POWER_MW,
			.poe_pwr_cal_mw = POE_PWR_CALIBRATION_MW_PER_10W,
		},
#endif

#ifdef SKU_2150
	[0] = {
			.mp_id = MW_PRODUCT_ID_AN8855M_5P_1SFP,
			.mp_str = "AN8855M_5P_1SFP",
			.sw = {
					.port_total = 1 + 5 + 1,
					.port_total_cpu = 1,
					.port_total_asic = 5 + 1,
					.port_total_uplink = 1,

					.port_cpu_start_idx = 0,
					.port_cpu_end_idx = 0,
					.port_asic_start_idx = 1,
					.port_asic_end_idx = 5 + 1,
					.port_uplink_start_idx = 6,
					.port_uplink_end_idx = 6,
				},
			.poe = {
					.port_total = 4,
					.port_user_start_idx = 1,
					.port_user_end_idx = 4,
					.port_flat_start_idx = 0,
					.port_flat_end_idx = 3,
					.port_map_type = POE_PORT_MAP_DIRECTLY,
				},
			.poe_chip = &poe_id_all_5p_1sfp[0],
			.poe_chip_total = NUMOFARRAY(poe_id_all_5p_1sfp),
			.poe_max_board_pwr_mw = MAX_POE_POWER_MW,
			.poe_usable_board_pwr_mw = USABLE_POE_POWER_MW,
			.poe_pwr_cal_mw = POE_PWR_CALIBRATION_MW_PER_10W,
		},
#endif

#ifdef SKU_2151
	[0] = {
			.mp_id = MW_PRODUCT_ID_AN8855M_5P_5P,
			.mp_str = "AN8855M_5P_5P",
			.sw = {
					.port_total = 1 + 5 + 5,
					.port_total_cpu = 1,
					.port_total_asic = 5 + 5,
					.port_total_uplink = 2,

					.port_cpu_start_idx = 0,
					.port_cpu_end_idx = 0,
					.port_asic_start_idx = 1,
					.port_asic_end_idx = 5 + 5,
					.port_uplink_start_idx = 9,
					.port_uplink_end_idx = 10,
				},
			.poe = {
					.port_total = 8,
					.port_user_start_idx = 1,
					.port_user_end_idx = 8,
					.port_flat_start_idx = 0,
					.port_flat_end_idx = 7,
					.port_map_type = POE_PORT_MAP_DIRECTLY,
				},
			.poe_chip = &poe_id_all_5p_5p[0],
			.poe_chip_total = NUMOFARRAY(poe_id_all_5p_5p),
			.poe_max_board_pwr_mw = MAX_POE_POWER_MW,
			.poe_usable_board_pwr_mw = USABLE_POE_POWER_MW,
			.poe_pwr_cal_mw = POE_PWR_CALIBRATION_MW_PER_10W,
		},
#endif
};

static critical_t critical = 
{
	.type = 0xff,
	.count = 0,
};

void
os_sleep(uint32_t ms)
{
	vTaskDelay(pdMS_TO_TICKS(ms));
}

int
os_enter_critical(void)
{
	critical_t *pc = &critical;

#if !defined(OS_CRITICAL_TYPE)
#error "must define macro OS_CRITICAL_TYPE"
#endif

	pc->type = OS_CRITICAL_TYPE;

	taskENTER_CRITICAL();
	pc->count++;
	taskEXIT_CRITICAL();

	if(OS_CRITICAL_TYPE == OS_CRITICAL_DISABLE)
	{

	}
	else if(OS_CRITICAL_TYPE == OS_CRITICAL_SCHEDULE_LOCK)
	{
		vTaskSuspendAll();
	}
	else if(OS_CRITICAL_TYPE == OS_CRITICAL_INTERRUPT_LOCK)
	{
		taskENTER_CRITICAL();
	}
	else
	{
		DBG_PRINT("\n\n!!!Error:unknown critical type %d,task %s\n\n\n", 
			OS_CRITICAL_TYPE, dbg_get_current_task_name());
	}

	return 0;
}

int
os_exit_critical(void)
{
	int ret = 0;
	critical_t *pc = &critical;
	int old = pc->count;

#if !defined(OS_CRITICAL_TYPE)
#error "must define macro OS_CRITICAL_TYPE"
#endif

	pc->type = OS_CRITICAL_TYPE;

	taskENTER_CRITICAL();
	pc->count--;
	taskEXIT_CRITICAL();

	if(OS_CRITICAL_TYPE == OS_CRITICAL_DISABLE)
	{

	}
	else if(OS_CRITICAL_TYPE == OS_CRITICAL_SCHEDULE_LOCK)
	{
		ret = (int)xTaskResumeAll();
	}
	else if(OS_CRITICAL_TYPE == OS_CRITICAL_INTERRUPT_LOCK)
	{
		taskEXIT_CRITICAL();
	}
	else
	{
		DBG_PRINT("\n\n!!!Error:unknown critical type %d,task %s\n\n\n", 
			OS_CRITICAL_TYPE, dbg_get_current_task_name());
	}

	if(pc->count < 0)
	{
		DBG_PRINT("\n\n!!!Error:Critical count %d,old is %d,task %s\n\n\n", 
			pc->count, old, dbg_get_current_task_name());
	}

	return ret;
}

void
os_print_critical(void)
{
	critical_t *pc = &critical;

	DBG_PRINT("Critical type is %d,count is %d\n", pc->type, pc->count);
}

int
os_status_critical_disabled(void)
{
#if !defined(OS_CRITICAL_TYPE)
#error "must define macro OS_CRITICAL_TYPE"
#endif
	
	if(OS_CRITICAL_TYPE == OS_CRITICAL_DISABLE)
	{
		return 1;
	}

	return 0;
}

uint64_t 
time_current_ms(void)
{
	return TICKS_TO_pdMS64(xTaskGetTickCount());
}

uint32_t 
time_current_tick(void)
{
	return (uint32_t)xTaskGetTickCount();
}

void 
time_local_timer_cb(TimerHandle_t xTimer)
{
	uint32_t tk;
	time_local_tick_t *ptlt = &tlt;

	TIMER_DEBUG_START();
	FREERTOS_DEBUG();

	if(!ptlt->inited)
	{
		FREERTOS_DEBUG();
		TIMER_DEBUG_QUIT();
		return;
	}

	tk = time_current_tick();

	os_enter_critical();
	if(ptlt->ticks <= tk)
	{
		ptlt->ticks = tk;
	}
	else
	{
		ptlt->ticks = tk;
		ptlt->ticks_carry++;
	}
	os_exit_critical();

	FREERTOS_DEBUG();
	TIMER_DEBUG_QUIT();

	return;
}

int
time_local_timer_init()
{
	time_local_timer_hdl = xTimerCreate(TIME_LOCAL_TIMER_NAME, 
				pdMS_TO_TICKS(TIME_LOCAL_TIMER_MS), 
				pdTRUE, 
				NULL, 
				time_local_timer_cb);

	if(time_local_timer_hdl)
	{
		xTimerStart(time_local_timer_hdl, 0);
		return 0;
	}

	return -1;
}

int
time_local_update(time_network_t *ptn)
{
	time_local_tick_t *ptlt = &tlt;
	uint32_t *pl, *ph;
	char *p = NULL;

	if(!ptn)
	{
		DBG_PRINT("Error:parameter ptn %p invalid\n", ptn);
		return -1;
	}

#if (__BYTE_ORDER == __LITTLE_ENDIAN)
	pl = (uint32_t*)&ptn->time_val;
	ph = (uint32_t*)&ptn->time_val + 1;
	p = "little endian";
#elif (__BYTE_ORDER == __BIG_ENDIAN)
	ph = (uint32_t*)&ptn->time_val;
	pl = (uint32_t*)&ptn->time_val + 1;
	p = "big endian";
#else
#error "No __BYTE_ORDER defined"
#endif
	
	osal_printf("Order(%s).Input time val:0x%08x%08x,time_zone:0x%x\n", 
			p, *ph, *pl, ptn->time_zone);

	os_enter_critical();

	osal_memset(ptlt, 0, sizeof(*ptlt));
	ptlt->tn.time_val = ptn->time_val;
	ptlt->tn.time_zone = ptn->time_zone;
	ptlt->ticks_init = time_current_tick();
	ptlt->ticks = ptlt->ticks_init;
	ptlt->ticks_carry = 0;
	ptlt->inited = 1;
	
	os_exit_critical();

	return 0;
}

void
time_local_init(time_network_t *ptn)
{
	static uint8_t timer_inited = 0;

	if(!ptn)
	{
		DBG_PRINT("Error:parameter ptn %p invalid\n", ptn);
		return;
	}

	time_local_update(ptn);

#if 0	/* wy,debug code */
{
	uint8_t *p;
	DEBUG_MSG("===========================\n");
	DEBUG_MSG("ptn addr:%p time_val addr:%p size of ptn->time_val:%ld size of ptn:%ld\n", 
		ptn, &ptn->time_val, sizeof(ptn->time_val), sizeof(ptn));
	DEBUG_MSG("time_val:%lld(0x%x,%d) time_zone:%d\n", 
		ptn->time_val, (unsigned int)ptn->time_val, 
		(unsigned int)ptn->time_val, ptn->time_zone);
	DEBUG_MSG("time_val:%lld\n", ptn->time_val);
	DEBUG_MSG("time_val:0x%x\n", (unsigned int)ptn->time_val);
	DEBUG_MSG("time_val:%d\n", (unsigned int)ptn->time_val);
	p = (uint8_t*)&ptn->time_val;
	DEBUG_MSG("time_val(%p):  %02x %02x %02x %02x   %02x %02x %02x %02x   %02x %02x %02x %02x\n",
		p, p[0], p[1], p[2], p[3], p[4], p[5], p[6], p[7], p[8], p[9], p[10], p[11]);
	p = (uint8_t*)ptn;
	DEBUG_MSG("ptn(%p):  %02x %02x %02x %02x   %02x %02x %02x %02x   %02x %02x %02x %02x\n",
		p, p[0], p[1], p[2], p[3], p[4], p[5], p[6], p[7], p[8], p[9], p[10], p[11]);
	DEBUG_MSG("===========================\n");
}
#endif

	if(!timer_inited)
	{
		timer_inited = 1;
		if(time_local_timer_init())
		{
			DBG_PRINT("Error:time local timer initialize failure\n");
		}
	}
}

int
time_local_get_time(time_network_t *out)
{
	time_local_tick_t *ptlt = &tlt;
	uint64_t tkc, tki;

	if(!out || !ptlt->inited)
	{
		DBG_PRINT("Error:out %p or initialized %d invalid\n", out, ptlt->inited);
		return -1;
	}

	time_local_timer_cb(NULL);

	tki = (uint64_t)ptlt->ticks_init;
	tkc = (uint64_t)ptlt->ticks + ((uint64_t)ptlt->ticks_carry << 32);

	out->time_val = TICKS_TO_pdS(tkc - tki) + ptlt->tn.time_val;
	out->time_zone = ptlt->tn.time_zone;

#if 0	/* wy,debug code */
{
	uint8_t *p;

	DEBUG_MSG("%s:%d:%s\n", __FILE__, __LINE__, __FUNCTION__);
	DEBUG_MSG("tki:%lld(0x%x,%u)\n", tki, (unsigned int)tki, (unsigned int)tki);
	p = (uint8_t*)&tki;
	DEBUG_MSG("tki:  %02x %02x %02x %02x	%02x %02x %02x %02x\n",
		p[0], p[1], p[2], p[3], p[4], p[5], p[6], p[7]);

	DEBUG_MSG("tkc:%lld(0x%x,%u)\n", tkc, (unsigned int)tkc, (unsigned int)tkc);
	p = (uint8_t*)&tkc;
	DEBUG_MSG("tkc:  %02x %02x %02x %02x	%02x %02x %02x %02x\n",
		p[0], p[1], p[2], p[3], p[4], p[5], p[6], p[7]);

	DEBUG_MSG("ptlt->tn.time_val:%lld(0x%x,%u)\n", ptlt->tn.time_val, (unsigned int)ptlt->tn.time_val, (unsigned int)ptlt->tn.time_val);
	p = (uint8_t*)&ptlt->tn.time_val;
	DEBUG_MSG("ptlt->tn.time_val:  %02x %02x %02x %02x	%02x %02x %02x %02x\n",
		p[0], p[1], p[2], p[3], p[4], p[5], p[6], p[7]);

	DEBUG_MSG("out->time_val:%lld(0x%x,%u)\n", out->time_val, (unsigned int)out->time_val, (unsigned int)out->time_val);
	p = (uint8_t*)&out->time_val;
	DEBUG_MSG("out->time_val:  %02x %02x %02x %02x	%02x %02x %02x %02x\n",
		p[0], p[1], p[2], p[3], p[4], p[5], p[6], p[7]);

}
#endif

	return 0;
}

// 计算闰年
static int 
time_local_is_leap_year(int year)
{
    return (year % 4 == 0 && year % 100 != 0) || (year % 400 == 0);
}

// 根据年、月、日计算星期几（0 = 星期日, 1 = 星期一, ..., 6 = 星期六）  
static int 
time_local_calculateWeekday(int year, int month, int day)
{  
	if(month < 3)
	{  
		month += 12;  
		year--;  
	}  

	int wk = (day + 2 * month + 3 * (month + 1) / 5 + year + year / 4 - year / 100 + year / 400) % 7;  
	return wk;  
}

// 根据秒数和时区计算日期时间
static int 
time_local_time_2_datetime(time_network_t *ptn, datetime_t *dt)
{
    // 一分钟的秒数
    long minute = 60;
    // 一小时的秒数
    long hour = 60 * minute;
    // 一天的秒数
    long day = 24 * hour;
    // 一年的秒数（不考虑闰年）
    long year = 365 * day;

	if(!ptn || !dt)
	{
		DBG_PRINT("Error:ptn % or dt %p invalid\n", ptn, dt);
		return -1;
	}

	uint64_t seconds = ptn->time_val;
	int16_t timezoneOffset = ptn->time_zone;

    // 考虑时区偏移
    seconds += timezoneOffset * hour;

    // 计算年份
    dt->year = 1970;
    while(seconds >= year)
	{
        int daysInYear = time_local_is_leap_year(dt->year) ? 366 : 365;
        seconds -= daysInYear * day;
        dt->year++;
    }

    // 计算月份和日期
    int daysInMonth[] = {0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
    if(time_local_is_leap_year(dt->year))
	{
        daysInMonth[2] = 29; // 闰年二月有29天
    }

    dt->month = 1;
    while(seconds >= daysInMonth[dt->month] * day)
	{
        seconds -= daysInMonth[dt->month] * day;
        dt->month++;
    }

    // 计算日期
    dt->day = seconds / day + 1;
    seconds %= day;

	//计算星期
	dt->week = time_local_calculateWeekday(dt->year, dt->month, dt->day) + 1;

    // 计算小时、分钟
    dt->hour = seconds / hour;
    seconds %= hour;

    dt->minute = seconds / minute;

	dt->seconds = seconds % minute;

    return 0;
}

int
time_local_get_datetime(datetime_t *pdt)
{
	time_network_t tn, *ptn = &tn;

	if(!pdt)
	{
		DBG_PRINT("Error:pdt %p invalid\n", pdt);
		return -1;
	}

	if(time_local_get_time(ptn))
	{
		DBG_PRINT("Error:time_local_get_time failure\n");
		return -2;
	}

	if(time_local_time_2_datetime(ptn, pdt))
	{
		DBG_PRINT("Error:time_local_time_2_datetime failure\n");
		return -3;
	}

	return 0;
}

int
time_datetime_cmp(datetime_t *pdta, datetime_t *pdtb, uint8_t *perrbit)
{
	if(!pdta || !pdtb || !perrbit)
	{
		DBG_PRINT("Error:pdta %p or pdtb %p or perrbit %p invalid\n", pdta, pdtb, perrbit);
		return -1;
	}

	*perrbit = 0x0;
	*perrbit |= ((pdta->year == pdtb->year) ? 0 : BIT(CELL_YEAR));
	*perrbit |= ((pdta->month == pdtb->month) ? 0 : BIT(CELL_MONTH));
	*perrbit |= ((pdta->week == pdtb->week) ? 0 : BIT(CELL_WEEK));
	*perrbit |= ((pdta->day == pdtb->day) ? 0 : BIT(CELL_DAY));
	*perrbit |= ((pdta->hour == pdtb->hour) ? 0 : BIT(CELL_HOUR));
	*perrbit |= ((pdta->minute == pdtb->minute) ? 0 : BIT(CELL_MINUTE));
	*perrbit |= ((pdta->seconds == pdtb->seconds) ? 0 : BIT(CELL_SECONDS));

	if(*perrbit)
	{
		return -2;
	}
	else
	{/* match */
		return 0;
	}
}

char *
module_product_id_get(uint8_t *pdct    /* Accroding to MW_PRODUCT_ID_T */,
							board_port_cfg_t **ppbpc)
{
	uint8_t idx;
	board_port_cfg_t *p;

	if(!__mw_product_str)
	{
		if(pdct)
		{
			*pdct = MW_PRODUCT_ID_LAST;
		}

		if(ppbpc)
		{
			*ppbpc = NULL;
		}

		return NULL;
	}

	if(pdct)
	{
		*pdct = __mw_product_id;
	}

	if(ppbpc)
	{
		for(idx = 0, p = &board_port_cfg[0]; idx < NUMOFARRAY(board_port_cfg); idx++, p++)
		{
			if(p->mp_id == __mw_product_id)
			{
				*ppbpc = p;
				return __mw_product_str;
			}
		}

		//*ppbpc = NULL;
		/* No accurate module product ID, use default configuration */
		*ppbpc = &board_port_cfg[0];
		DBG_PRINT("No accurate module product ID,use default configuration\n");
	}

	return __mw_product_str;
}

void
module_product_print(board_port_cfg_t *pbpc)
{
	board_sw_port_cfg_t *psw;
	board_poe_port_cfg_t *ppoe;
	poe_id_t *pid;
	uint8_t i;
	char *str_chip_type[POE_CHIP_TYPE_MAX] = 
		{
			[POE_CHIP_TYPE_T7604R] = "TMI7604R",
			[POE_CHIP_TYPE_T7608R] = "TMI7608R",
		};

	if(!pbpc)
	{
		return;
	}

	psw = &pbpc->sw;
	ppoe = &pbpc->poe;


#if 1
	DBG_PRINT("Board Module Product ID:%d(%s)\n",
			pbpc->mp_id, pbpc->mp_str
			);
	DBG_PRINT("\tTotal port:%d Total CPU port:%d Total ASIC port:%d Total Uplink port:%d\n",
			psw->port_total, psw->port_total_cpu, psw->port_total_asic, psw->port_total_uplink
			);

	DBG_PRINT("\tStart of CPU port:%d End of CPU port:%d\n"
			"\tStart of ASIC port:%d End of ASIC port:%d\n",
			psw->port_cpu_start_idx, psw->port_cpu_end_idx,
			psw->port_asic_start_idx, psw->port_asic_end_idx
			);
	DBG_PRINT("\tStart of Uplink port:%d End of Uplink port:%d\n",			
			psw->port_uplink_start_idx, psw->port_uplink_end_idx
			);

	DBG_PRINT("\tTotal POE port:%d\n"
			"\tStart of POE User port:%d End of POE User port:%d\n",
			ppoe->port_total, 
			ppoe->port_user_start_idx, ppoe->port_user_end_idx
			);
	DBG_PRINT("\tStart of POE Flat port:%d End of POE Flat port:%d\n", 
			ppoe->port_flat_start_idx, ppoe->port_flat_end_idx
			);
#else
	DBG_PRINT("Board Module Product ID:%d(%s)\n"
			"\tTotal port:%d Total CPU port:%d Total ASIC port:%d Total Uplink port:%d\n",
			pbpc->mp_id, pbpc->mp_str,
			psw->port_total, psw->port_total_cpu, psw->port_total_asic, psw->port_total_uplink
			);

	DBG_PRINT("\tStart of CPU port:%d End of CPU port:%d\n"
			"\tStart of ASIC port:%d End of ASIC port:%d\n"
			"\tStart of Uplink port:%d End of Uplink port:%d\n",
			psw->port_cpu_start_idx, psw->port_cpu_end_idx,
			psw->port_asic_start_idx, psw->port_asic_end_idx,
			psw->port_uplink_start_idx, psw->port_uplink_end_idx
			);

	DBG_PRINT("\tTotal POE port:%d\n"
			"\tStart of POE User port:%d End of POE User port:%d\n"
			"\tStart of POE Flat port:%d End of POE Flat port:%d\n", 
			ppoe->port_total, 
			ppoe->port_user_start_idx, ppoe->port_user_end_idx,
			ppoe->port_flat_start_idx, ppoe->port_flat_end_idx
			);
#endif
	


	for(i = 0; i < pbpc->poe_chip_total; i++)
	{
		pid = &pbpc->poe_chip[i];

		if(pid->valid)
		{
#if 1
			DBG_PRINT("\tPOE Chip[%d] Index:%d Type:%s\n", 
				i, pid->chip_idx, (pid->chip_type < POE_CHIP_TYPE_MAX) ? 
				str_chip_type[pid->chip_type % NUMOFARRAY(str_chip_type)] : "unknown"
				);
			DBG_PRINT("\t\tUnit:%d Channel:%d Device Address:0x%x Base Port:%d Total Port:%d\n", 
				pid->unit, pid->chn, pid->dev, pid->base_port, pid->total_port
				);
#else
			DBG_PRINT("\tPOE Chip[%d] Index:%d Type:%s\n"
				"\t\tUnit:%d Channel:%d Device Address:0x%x Base Port:%d Total Port:%d\n", 
				i, pid->chip_idx, (pid->chip_type < POE_CHIP_TYPE_MAX) ? 
				str_chip_type[pid->chip_type % NUMOFARRAY(str_chip_type)] : "unknown",
				pid->unit, pid->chn, pid->dev, pid->base_port, pid->total_port
				);
#endif
		}
	}
}

void
module_product_init(void)
{
	char *mpstr;
	uint8_t mpid;
	board_port_cfg_t *pbpc = NULL;
	int good;

	if((mpstr = module_product_id_get(&mpid, &pbpc)))
	{
		pboard_port_cfg = pbpc;

		DBG_PRINT("Original Module Product ID:%d \"%s\";Board Config:%p\n", mpid, mpstr, pbpc);
	
		if(pbpc)
		{
			if((good = poe_detect_chip_all(&pbpc->poe_chip[0], pbpc->poe_chip_total)) != pbpc->poe_chip_total)
			{
				DBG_PRINT("\nError:!!! not all POE Chip have been detected,"
					"Good POE Chip is %d but all POE Chip is %d\n\n", 
					good, pbpc->poe_chip_total);
			}
			
			DBG_PRINT("\n==========================================================\n");
			module_product_print(pbpc);
			DBG_PRINT("==========================================================\n\n");
		}
	}
	else
	{
		DBG_PRINT("\n\n\t!!! Error:Getting Board Module Product failure !!!\n\n");
		configASSERT(0);
	}
}

board_port_cfg_t *
module_product_cfg_get(void)
{
	return pboard_port_cfg;
}

uint8_t
module_product_poe_port_map_type(void)
{
	board_port_cfg_t *pbpc = module_product_cfg_get();

	configASSERT(pbpc);
	return pbpc->poe.port_map_type;
}

uint8_t
module_product_poe_port_user_start_idx(void)
{
	board_port_cfg_t *pbpc = module_product_cfg_get();

	configASSERT(pbpc);
	return pbpc->poe.port_user_start_idx;
}

uint8_t
module_product_poe_port_user_end_idx(void)
{
	board_port_cfg_t *pbpc = module_product_cfg_get();

	configASSERT(pbpc);
	return pbpc->poe.port_user_end_idx;
}

uint8_t
module_product_poe_port_flat_start_idx(void)
{
	board_port_cfg_t *pbpc = module_product_cfg_get();

	configASSERT(pbpc);
	return pbpc->poe.port_flat_start_idx;
}

uint8_t
module_product_poe_port_flat_end_idx(void)
{
	board_port_cfg_t *pbpc = module_product_cfg_get();

	configASSERT(pbpc);
	return pbpc->poe.port_flat_end_idx;
}

uint8_t
module_product_poe_port_total(void)
{
	board_port_cfg_t *pbpc = module_product_cfg_get();

	configASSERT(pbpc);
	return pbpc->poe.port_total;
}

poe_id_t*
module_product_poe_chip(void)
{
	board_port_cfg_t *pbpc = module_product_cfg_get();

	configASSERT(pbpc);
	return pbpc->poe_chip;
}

uint8_t
module_product_poe_chip_total(void)
{
	board_port_cfg_t *pbpc = module_product_cfg_get();

	configASSERT(pbpc);
	return pbpc->poe_chip_total;
}

uint32_t
module_product_poe_max_board_pwr_mw(void)
{
	board_port_cfg_t *pbpc = module_product_cfg_get();

	configASSERT(pbpc);
	return pbpc->poe_max_board_pwr_mw;
}

uint32_t
module_product_poe_usable_board_pwr_mw(void)
{
	board_port_cfg_t *pbpc = module_product_cfg_get();

	configASSERT(pbpc);
	return pbpc->poe_usable_board_pwr_mw;
}

uint32_t
module_product_poe_pwr_calibration_mw(void)
{
	board_port_cfg_t *pbpc = module_product_cfg_get();

	configASSERT(pbpc);
	return pbpc->poe_pwr_cal_mw;
}

uint8_t
module_product_switch_port_total(void)
{
	board_port_cfg_t *pbpc = module_product_cfg_get();

	configASSERT(pbpc);
	return pbpc->sw.port_total;
}

uint8_t
module_product_cpu_port_total(void)
{
	board_port_cfg_t *pbpc = module_product_cfg_get();

	configASSERT(pbpc);
	return pbpc->sw.port_total_cpu;
}

uint8_t
module_product_asic_port_total(void)
{
	board_port_cfg_t *pbpc = module_product_cfg_get();

	configASSERT(pbpc);
	return pbpc->sw.port_total_asic;
}

uint8_t
module_product_uplink_port_total(void)
{
	board_port_cfg_t *pbpc = module_product_cfg_get();

	configASSERT(pbpc);
	return pbpc->sw.port_total_uplink;
}

uint8_t
module_product_cpu_port_start(void)
{
	board_port_cfg_t *pbpc = module_product_cfg_get();

	configASSERT(pbpc);
	return pbpc->sw.port_cpu_start_idx;
}

uint8_t
module_product_cpu_port_end(void)
{
	board_port_cfg_t *pbpc = module_product_cfg_get();

	configASSERT(pbpc);
	return pbpc->sw.port_cpu_end_idx;
}

uint8_t
module_product_asic_port_start(void)
{
	board_port_cfg_t *pbpc = module_product_cfg_get();

	configASSERT(pbpc);
	return pbpc->sw.port_asic_start_idx;
}

uint8_t
module_product_asic_port_end(void)
{
	board_port_cfg_t *pbpc = module_product_cfg_get();

	configASSERT(pbpc);
	return pbpc->sw.port_asic_end_idx;
}

uint8_t
module_product_uplink_port_start(void)
{
	board_port_cfg_t *pbpc = module_product_cfg_get();

	configASSERT(pbpc);
	return pbpc->sw.port_uplink_start_idx;
}

uint8_t
module_product_uplink_port_end(void)
{
	board_port_cfg_t *pbpc = module_product_cfg_get();

	configASSERT(pbpc);
	return pbpc->sw.port_uplink_end_idx;
}


