
#ifndef __POE_H__
#define __POE_H__

#include "list_linux.h"

#ifndef BIT
#define BIT(n)						(1 << (n))
#endif

#define POE_INTERRUPT_ENABLE		(0)

#define POE_I2C_UNIT				(0)
#define POE_I2C_CHN					(1)

#define DBG_POE_MSG(x...)	do{if(CK_DBG_EN(DBG_POE)){osal_printf(x);}}while(0)

typedef enum poe_chip_type
{
	POE_CHIP_TYPE_T7604R = 0,
	POE_CHIP_TYPE_T7608R,
	POE_CHIP_TYPE_MAX,
}poe_chip_type_e;

#define POE_I2C_STABLE_ADDR_MASK	(0xf)
#define POE_I2C_STABLE_ADDR_OFFSET	(0x3)
#define POE_I2C_DEV_ADDR_MASK		(0x7)
#define POE_I2C_DEV_ADDR_OFFSET		(0x0)

#define POE_I2C_7604_DEV_ADDR		(0xa)
#define POE_I2C_7608_DEV_ADDR		(0xe)

#define POE_I2C_7604_ADDR(d)		\
	(((POE_I2C_7604_DEV_ADDR & POE_I2C_STABLE_ADDR_MASK) << POE_I2C_STABLE_ADDR_OFFSET) + \
	(((d) & POE_I2C_DEV_ADDR_MASK) << POE_I2C_DEV_ADDR_OFFSET))

#define POE_I2C_7608_ADDR(d)		\
	(((POE_I2C_7608_DEV_ADDR & POE_I2C_STABLE_ADDR_MASK) << POE_I2C_STABLE_ADDR_OFFSET) + \
	(((d) & POE_I2C_DEV_ADDR_MASK) << POE_I2C_DEV_ADDR_OFFSET))

#define POE_I2C_ADDR(type, dev_addr)			\
	(((type) == POE_CHIP_TYPE_T7604R) ? POE_I2C_7604_ADDR(dev_addr) : \
		(((type) == POE_CHIP_TYPE_T7608R) ? POE_I2C_7608_ADDR(dev_addr) : (-1)))

#define POE_I2C_CHIP_TYPE(addr)		\
	((((addr) >> POE_I2C_STABLE_ADDR_OFFSET) & POE_I2C_STABLE_ADDR_MASK) == POE_I2C_7604_DEV_ADDR) ? POE_CHIP_TYPE_T7604R : \
	(((((addr) >> POE_I2C_STABLE_ADDR_OFFSET) & POE_I2C_STABLE_ADDR_MASK) == POE_I2C_7608_DEV_ADDR) ? POE_CHIP_TYPE_T7608R :  POE_CHIP_TYPE_MAX)

#define POE_CHIP_ID					(0x1a)

#define MAX_PORT_T7604				(4)
#define MAX_PORT_T7608				(8)

#define POE_I2C_MAX_UNIT			(1)
#define POE_I2C_MAX_CHN				(2)
#define POE_I2C_MAX_DEV				(1)
#define POE_I2C_DEV_START			(0x50)

#define POE_MAX_PORT_PER_CHIP		((MAX_PORT_T7608 > MAX_PORT_T7604) ? MAX_PORT_T7608 : MAX_PORT_T7604)
#define POE_MAX_CHIP_PER_SWITCH		(POE_I2C_MAX_UNIT * POE_I2C_MAX_CHN * POE_I2C_MAX_DEV)
#define POE_MAX_PORT				(POE_MAX_PORT_PER_CHIP * POE_MAX_CHIP_PER_SWITCH)

#define POE_MAX_CURRENT_CHIP		(4)
#define POE_MAX_CURRENT_PORT		(POE_MAX_CURRENT_CHIP * POE_MAX_PORT_PER_CHIP)

#define POE_PRIORITY_DEFAULT		(0x80)

#define POE_PORT_2_CHIP_IDX(port)	((port) / POE_CHIP->total_port)
#define POE_PORT_2_CHIP_PORT(port)	((port) % POE_CHIP->total_port)
#define POE_PORT_2_CHIP(pid, port)	((pid)[POE_PORT_2_CHIP_IDX(port) % POE_CHIP_TOTAL])

enum
{
	DETECT_NOT_HAPPEN = 0,
	DETECT_DCP,
	DETECT_HIGH_CAP,
	DETECT_R_LOW,
	DETECT_OK,
	DETECT_R_HIGH,
	DETECT_OPEN,
	DETECT_DCN,
	DETECT_MAX,
};

enum
{
	CLASS_NOT_HAPPEN = 0,
	CLASS_LEVEL_1,
	CLASS_LEVEL_2,
	CLASS_LEVEL_3,
	CLASS_LEVEL_4,
	CLASS_LEVEL_4PLUS,
	CLASS_LEVEL_0,
	CLASS_LEVEL_CURRENT_LIMIT,
	CLASS_MAX,
};

enum
{
	MODE_SHUTDOWN = 0,
	MODE_MANUAL,
	MODE_SEMIAUTO,
	MODE_AUTO,
	MODE_MAX,
	MODE_MASK = 0x3,
};

#define POE_CHIP_MODE						(MODE_SEMIAUTO)
#define POE_PWR_MGMT_IN_HW					(0)

enum
{
	INT_PWR_EN = 0,
	INT_PWR_GD,
	INT_DISCONNECT,
	INT_DET_DN,

	INT_CLS_DN,
	INT_OVER_CURRENT,
	INT_PWR_UP_LIMITING,
	INT_SUPPLY_BITS,
	INT_PWR_ON_LIMITING,
};
#define INT_MSK_ALL				\
			(BIT(INT_PWR_EN) | BIT(INT_PWR_GD) | BIT(INT_DISCONNECT) | \
			BIT(INT_DET_DN) | BIT(INT_CLS_DN) | BIT(INT_OVER_CURRENT) | \
			BIT(INT_PWR_UP_LIMITING) | BIT(INT_SUPPLY_BITS))

#define INT_EN									(1 << 7)	/* for reg 0x25 */
#define INT_EN_MSK								(1 << 7)	/* for reg 0x25 */
#define INT_CLR									(1 << 7)	/* for reg 0x2b */
#define INT_PIN_CLR								(1 << 6)	/* for reg 0x2b */

#define FET_BAD									(1 << 1)	/* for reg 0x59,0x5d,
																0x61,0x65 */

#define MISC_STATUS_TSD							(1 << 7)
#define MISC_STATUS_FETBAD						(1 << 6)
#define MISC_STATUS_V3P3_UVLO					(1 << 5)
#define MISC_STATUS_V54_UVLO					(1 << 4)
#define MISC_STATUS_OVP_MAX						(1 << 3)
#define MISC_STATUS_RCS							(1 << 2)

enum
{
	SUPPLY_RESERVED_A = 0,
	SUPPLY_RESERVED_B,
	SUPPLY_REQUEST_CURRENT_OVERFLOW,
	SUPPLY_CONSUMPTION_OVER_MAX_CURRENT,

	SUPPLY_CHIP_RESET_FROM_V54_LOW,
	SUPPLY_CHIP_RESET_FROM_V3P3_LOW,
	SUPPLY_FET_BAD,
	SUPPLY_CHIP_RESET_FROM_T150,
};


#define POE_T760X_INT_REG						0x0
#define POE_T760X_INT_MSK_REG					0x1

#define POE_T760X_EVENT_PWR_EN_A_REG			0x2				/* A is read only */
#define POE_T760X_EVENT_PWR_EN_B_REG			0x3				/* B is clear after read */
#define POE_T760X_EVENT_PWR_GD_A_REG			0x4
#define POE_T760X_EVENT_PWR_GD_B_REG			0x5
#define POE_T760X_EVENT_DET_DN_A_REG			0x6
#define POE_T760X_EVENT_DET_DN_B_REG			0x7
#define POE_T760X_EVENT_CLS_DN_A_REG			0x8
#define POE_T760X_EVENT_CLS_DN_B_REG			0x9
#define POE_T760X_EVENT_OVER_CURRENT_A_REG		0xa
#define POE_T760X_EVENT_OVER_CURRENT_B_REG		0xb
#define POE_T760X_EVENT_DISCONNECT_A_REG		0xc
#define POE_T760X_EVENT_DISCONNECT_B_REG		0xd
#define POE_T760X_EVENT_PWR_UP_LIMITING_A_REG	0xe
#define POE_T760X_EVENT_PWR_UP_LIMITING_B_REG	0xf
#define POE_T760X_EVENT_PWR_ON_LIMITING_A_REG	0x10
#define POE_T760X_EVENT_PWR_ON_LIMITING_B_REG	0x11
#define POE_T760X_EVENT_SUPPLY_A_REG			0x12
#define POE_T760X_EVENT_SUPPLY_B_REG			0x13




#define POE_T760X_BASE_STAUTS_REG				0x14
#define POE_T760X_PORT_STAUTS_REG(p)			(((p) * 1) + (POE_T760X_BASE_STAUTS_REG))


#define POE_T760X_PWR_EN_REG					0x1c
#define POE_T760X_PWR_GOOD_REG					0x1d

#define POE_T760X_MODE_REG						0x1f
#define POE_T760X_MODE_A_REG					POE_T760X_MODE_REG
#define POE_T760X_MODE_B_REG					0x20

#define POE_T760X_DCD_EN_REG					0x21
#define POE_T760X_DET_EN_REG					0x22
#define POE_T760X_CLS_EN_REG					0x23
#define POE_T760X_MIDSPAN_EN_REG				0x24

#define POE_T760X_MISC_CFG_REG					0x25

#define POE_T760X_DET_PB_REG					0x26
#define POE_T760X_CLS_PB_REG					0x27
#define POE_T760X_PWR_ON_REG					0x28
#define POE_T760X_PWR_OFF_REG					0x29
#define POE_T760X_RST_PORT_REG					0x2A
#define POE_T760X_MISC_CFG_2_REG				0x2B


#define POE_T760X_ID_REG						0x2c
#define POE_T760X_EN_CLS4P_REG					0x2d


#define POE_T760X_BASE_CURR_L_REG				0x33
#define POE_T760X_BASE_CURR_M_REG				0x34
#define POE_T760X_BASE_VOLT_L_REG				0x35
#define POE_T760X_BASE_VOLT_M_REG				0x36
#define POE_T760X_PORT_CURR_L_REG(p)			(((p) * 4) + (POE_T760X_BASE_CURR_L_REG))
#define POE_T760X_PORT_CURR_M_REG(p)			(((p) * 4) + (POE_T760X_BASE_CURR_M_REG))
#define POE_T760X_PORT_VOLT_L_REG(p)			(((p) * 4) + (POE_T760X_BASE_VOLT_L_REG))
#define POE_T760X_PORT_VOLT_M_REG(p)			(((p) * 4) + (POE_T760X_BASE_VOLT_M_REG))

#define POE_T760X_UNDER_V_REG					0x53

#define POE_T760X_RESERVED_REG					0x54	  /* all bits readable and writable,
 																													  except bit[3:2] are read only */

#define POE_T760X_BASE_2ND_EVENT_LEVEL_REG		0x56
#define POE_T760X_BASE_ICUT_REG					0x57
#define POE_T760X_BASE_CURRENT_LIMIT_REG		0x58
#define POE_T760X_BASE_HIGH_PWR_ST_REG			0x59
#define POE_T760X_PORT_2ND_EVENT_LEVEL_REG(p)	(((p) * 4) + (POE_T760X_BASE_2ND_EVENT_LEVEL_REG))
#define POE_T760X_PORT_ICUT_REG(p)				(((p) * 4) + (POE_T760X_BASE_ICUT_REG))
#define POE_T760X_PORT_CURRENT_LIMIT_REG(p)		(((p) * 4) + (POE_T760X_BASE_CURRENT_LIMIT_REG))
#define POE_T760X_PORT_HIGH_PWR_ST_REG(p)		(((p) * 4) + (POE_T760X_BASE_HIGH_PWR_ST_REG))

#define POE_T760X_PWR_CFG_REG					0x76
#define POE_T760X_MAX_CUR_L_REG					0x77
#define POE_T760X_MAX_CUR_H_REG					0x78
#define POE_T760X_PMAX_MAX_CUR_L_REG			0x79
#define POE_T760X_PMAX_MAX_CUR_H_REG			0x7a
#define POE_T760X_TOTAL_CONSUMED_I_L_REG		0x7b
#define POE_T760X_TOTAL_CONSUMED_I_H_REG		0x7c
#define POE_T760X_TOTAL_REMNANT_I_L_REG			0x7d
#define POE_T760X_TOTAL_REMNANT_I_H_REG			0x7e
#define POE_T760X_PD_REQ_I_L_REG				0x7f
#define POE_T760X_PD_REQ_I_H_REG				0x80

#define POE_T760X_CHIP_VOLT_L_REG				0x89
#define POE_T760X_CHIP_VOLT_M_REG				0x8a

#define POE_T760X_CHIP_TDISC_REG				0x94

#define POE_T760X_BASE_TEMP_L_REG				0xe0
#define POE_T760X_BASE_TEMP_M_REG				0xe1
#define POE_T760X_PORT_TEMP_L_REG(p)			(((p) * 2) + (POE_T760X_BASE_TEMP_L_REG))
#define POE_T760X_PORT_TEMP_M_REG(p)			(((p) * 2) + (POE_T760X_BASE_TEMP_M_REG))

#define POE_T760X_MAX_REG						0xf0

#define POE_CK_RST_VAL							(0xe3)
 
enum pwr_cut_mode
{
	PWR_CUT_MODE_BY_LAST_PWR_ON = 0,
	PWR_CUT_MODE_BY_FIRST_PWR_ON,
	PWR_CUT_MODE_BY_MIN_PORT_IDX,
	PWR_CUT_MODE_BY_MAX_PORT_IDX,
	PWR_CUT_MODE_MAX,
	PWR_CUT_MODE_MASK = 0x3,
};

typedef enum
{
	POE_PORT_PWR_ST_OFF = 0,
	POE_PORT_PWR_ST_ON,
	POE_PORT_PWR_ST_GOOD,
	POE_PORT_PWR_ST_MAX,
}poe_port_pwr_status_e;

typedef struct
{
	poe_port_pwr_status_e st_pt_pwr;
	uint32_t tick_pwron;
}poe_port_power_status_t;

typedef struct
{
	poe_port_power_status_t ppps[POE_MAX_PORT_PER_CHIP];
}poe_chip_power_status_t;

typedef struct
{
	uint8_t chip;
	uint8_t port;
	uint8_t pwr_enabled_cnt;
	uint8_t pwr_up_success;
	uint8_t dtc;
	uint8_t cls;
	uint8_t st_en;
	uint8_t st_gd;
}poe_port_t;

typedef struct
{
	uint8_t valid;
	uint8_t unit;
	uint8_t chn;
	uint8_t dev;

	uint8_t chip_idx;
	uint8_t chip_type;
	uint8_t base_port;
	uint8_t total_port;

#if 0
	poe_port_t port[POE_MAX_PORT_PER_CHIP];
#endif
}poe_id_t;

typedef struct
{
	uint8_t chip_idx;	/* poe chip index,it's indicated index of poe_id_t */
	uint8_t port_idx;	/* index of port of poe chip,from 0 to POE_MAX_PORT_PER_CHIP */
	poe_id_t *pid;		/* poe_id_t pointer,which point to poe chip with same poe chip index */
}poe_hw_port_t;

typedef struct
{
	poe_id_t *pid;
	uint8_t num;		/* number of pid */
}poe_id_mgmt_t;

typedef struct int_special_hdl
{
	char *int_name;
	uint8_t int_type;
	uint8_t int_type_msk;
	uint8_t reg_int;
	uint8_t reg_special;
	int (*int_hdl)(poe_id_t *p, struct int_special_hdl *pish);
}int_special_hdl_t;

enum poe_port_status_e
{
	POE_PORT_STATUS_INACTIVE = 0,
	POE_PORT_STATUS_ENABLE_DETECT,
	POE_PORT_STATUS_DETECTED,
	POE_PORT_STATUS_ENABLE_CLASSED,
	POE_PORT_STATUS_CLASSED,
	POE_PORT_STATUS_ENABLE_POWER_ON,
	POE_PORT_STATUS_POWER_ENABLED,
	POE_PORT_STATUS_POWER_DISABLED,
	POE_PORT_STATUS_POWER_GOOD,
	POE_PORT_STATUS_POWER_BAD,
	POE_PORT_STATUS_PWR_DN_BY_DISCONNECTED,
	POE_PORT_STATUS_PROBLEM,
	POE_PORT_STATUS_MAX,
};

typedef struct
{
	uint8_t port;					/* Index of poe port,start from 0 */
	uint8_t priority_val;			/* The priority of sequence on power up,0 has high priority */
	uint8_t status;					/* Accroding to enum poe_port_status_e */
	struct list_head priority_nod;	/* Next poe_port_status_t with low priority */
	poe_id_t *pid;					/* indicated the port which poe chip belong to */
}poe_port_info_t;

typedef struct
{
	uint64_t cls_done;
	struct list_head priority_hdr;
	poe_port_info_t ppit[POE_MAX_CURRENT_PORT];
}poe_info_t;

typedef struct
{
	uint8_t input_v_low;
	uint32_t input_vmv;
}poe_chip_input_vltg_t;

typedef struct
{
	uint32_t cua;
	uint32_t vmv;
	uint32_t pmw;
}poe_port_cvp_t;

typedef struct
{
	uint8_t port;
	uint32_t current;
}poe_pd_req_current_t;

typedef struct
{
	uint32_t ua[POE_MAX_PORT_PER_CHIP];
}poe_chip_icut_t;

typedef struct
{
	uint8_t pwr_en_changed;
	uint8_t pwr_good_changed;
	uint8_t detect_down;
	uint8_t class_down;

	uint8_t over_current_pwr_on_occur;
	uint8_t current_limit_pwr_on_occur;
	uint8_t current_limit_pwr_up_occur;
	uint8_t disconnect_occur;

	uint8_t misc;
}poe_status_occured_t;

typedef struct
{
	uint8_t pwr_en;
	uint8_t pwr_good;
	uint8_t detect[POE_MAX_PORT_PER_CHIP];
	uint8_t class[POE_MAX_PORT_PER_CHIP];
	poe_chip_input_vltg_t input_vltg;
	poe_chip_icut_t icut;
	uint8_t fet_bad;
	uint32_t max_available_current_total_ua;
	uint32_t max_available_power_total_mw;
	uint32_t total_consumed_current_ua;
	uint32_t total_consumed_power_mw;
	uint32_t total_remnant_available_current_ua;
	uint32_t total_remnant_available_power_mw;
	poe_pd_req_current_t pd_req_over_remnant;
	poe_port_cvp_t cvp[POE_MAX_PORT_PER_CHIP];
	uint16_t temp[POE_MAX_PORT_PER_CHIP];
}poe_status_now_t;

typedef struct
{
	poe_status_occured_t occured;
	poe_status_now_t now;
}poe_status_total_t;

//extern poe_id_t poe_id_all[POE_MAX_CURRENT_CHIP];
extern uint32_t dbg_input_vmv, dbg_cua;

int8_t port_user_to_poe_flat(uint8_t user_port);
int8_t port_poe_flat_to_user(uint8_t poe_flat_port);
int8_t port_poe_flat_to_poe_hw(uint8_t poe_flat_port, poe_hw_port_t *ppcp);
int8_t port_poe_hw_to_poe_flat(poe_hw_port_t *ppcp);
int8_t port_user_to_poe_chip(uint8_t user_port, poe_hw_port_t *ppcp);
int8_t port_poe_chip_to_user(poe_hw_port_t *ppcp);
int poe_reg_read(poe_id_t *p, uint8_t reg, uint8_t *val);
int poe_reg_write(poe_id_t *p, uint8_t reg, uint8_t val);
int poe_reg_modify(poe_id_t *p, uint8_t reg, uint8_t set, uint8_t msk);
int poe_detect_success(uint8_t id);
char* poe_detect_id_2_str(uint8_t id);
int poe_class_success(uint8_t id);
char* poe_class_id_2_str(uint8_t id);
int poe_get_id_chip(poe_id_t *p, uint8_t *id, uint8_t *ver);
int poe_pwr_cut_get_mode_chip(poe_id_t *p, uint8_t *mode);
int poe_pwr_cut_set_mode_chip(poe_id_t *p, uint8_t mode);
int poe_get_input_voltage_chip(poe_id_t *p, poe_chip_input_vltg_t *pciv);
int poe_get_max_current_pwr_chip(poe_id_t *p, uint32_t *pua, uint32_t *pmw);
int poe_set_max_current_chip(poe_id_t *p, uint32_t imaxua);
int poe_set_max_current_all(uint32_t imaxua);
int poe_set_max_pwr_chip(poe_id_t *p, uint32_t pmaxmw);
int poe_set_max_pwr_all(uint32_t pmaxmw);
int poe_set_chip_max_pwr_by_port_voltage_hw_port(		poe_hw_port_t *pphp, uint32_t pmaxmw);
int poe_set_chip_max_pwr_by_port_voltage_all(		poe_hw_port_t *pphp, uint32_t pmaxmw);
int poe_set_chip_max_pwr_by_port_voltage_routing(uint32_t pmaxmw);
int poe_get_total_consumed_current_chip(poe_id_t *p, uint32_t *pua);
int poe_get_total_remnant_current_chip(poe_id_t *p, uint32_t *pua);
int poe_get_total_consumed_pwr_chip(poe_id_t *p, uint32_t *ppmw);
int poe_get_total_consumed_pwr_all(uint32_t *ppmw);
int poe_get_total_remnant_pwr_chip(poe_id_t *p, uint32_t *ppmw);
int poe_get_total_remnant_pwr_all(uint32_t *ppmw);
int poe_get_pd_req_over_remnant_chip(poe_id_t *p, poe_pd_req_current_t *ppprc);
int poe_get_icut_chip(poe_id_t *p, poe_chip_icut_t *picut);
int poe_get_icut_hw_port(poe_hw_port_t *ppcp, uint32_t *picut);
int poe_get_icut_user_port(uint8_t user_port, uint32_t *picut);
int poe_get_cvp_chip(poe_id_t *p, poe_port_cvp_t *ppc);
int poe_get_cvp_hw_port(poe_hw_port_t *pphp, poe_port_cvp_t *ppc);
int poe_get_cvp_user_port(uint8_t user_port, poe_port_cvp_t *ppc);
int poe_get_temp_chip(poe_id_t *p, uint16_t *ptemp);
int poe_get_temp_hw_port(poe_hw_port_t *pphp, uint16_t *ptemp);
int poe_get_temp_user_port(uint8_t user_port, uint16_t *ptemp);
int poe_pwr_routing(void);
int poe_info_init(void);
int poe_info_set_status(uint8_t port, uint8_t status);
int poe_port_info_get_status(uint8_t port, uint8_t *status);
int poe_get_cls_hw_port(poe_hw_port_t *pphp, uint8_t *cls);
int poe_get_cls_user_port(uint8_t user_port, uint8_t *cls);
int poe_get_det_hw_port(poe_hw_port_t *pphp, uint8_t *det);
int poe_get_det_user_port(uint8_t user_port, uint8_t *det);
int poe_get_det_cls_hw_port(poe_hw_port_t *pphp, uint8_t *det, uint8_t *cls);
int poe_get_det_cls_user_port(uint8_t user_port, uint8_t *det, uint8_t *cls);
int poe_get_pwr_en_chip(poe_id_t *p, uint8_t *en);
int poe_get_pwr_en_all(uint8_t *en, uint8_t num);
int poe_get_pwr_en_hw_port(poe_hw_port_t *pphp, uint8_t *en);
int poe_get_pwr_en_user_port(uint8_t user_port, uint8_t *en);
int poe_get_pwr_good_chip(poe_id_t *p, uint8_t *good);
int poe_get_pwr_good_all(uint8_t *good, uint8_t num);
int poe_get_pwr_good_hw_port(poe_hw_port_t *pphp, uint8_t *good);
int poe_get_pwr_good_user_port(uint8_t user_port, uint8_t *good);
int poe_get_pwr_en_good_all(uint8_t *en_good, uint8_t num);
int poe_get_mode_hw_port(poe_hw_port_t *pphp, uint8_t *mode);
int poe_get_mode_user_port(uint8_t user_port, uint8_t *mode);
int poe_get_mode_all(uint8_t *mode, uint32_t szmd);
int poe_set_mode_hw_port(poe_hw_port_t *pphp, uint8_t mode);
int poe_set_mode_user_port(uint8_t user_port, uint8_t mode);
int poe_set_mode_chip(poe_id_t *pid, uint8_t mode);
int poe_set_mode_all(uint8_t mode);
int poe_port_connecting_check_setting(uint8_t port, uint8_t en);
int poe_port_connecting_check_setting_all(uint8_t en);
int poe_port_detect_setting(uint8_t port, uint8_t en);
int poe_port_detect_setting_all(uint8_t en);
int poe_port_class_setting(uint8_t port, uint8_t en);
int poe_port_class_setting_all(uint8_t en);
int poe_port_midspan_setting(uint8_t port, uint8_t en);
int poe_port_midspan_setting_all(uint8_t en);
int poe_port_detect_btn_trigger(uint8_t port);
int poe_port_detect_btn_trigger_all(void);
int poe_port_class_btn_trigger(uint8_t port);
int poe_port_class_btn_trigger_all(void);
int poe_port_power_on_btn_trigger(uint8_t port);
int poe_port_power_on_btn_trigger_all(void);
int poe_port_power_off_btn_trigger(uint8_t port);
int poe_port_power_off_btn_trigger_all(void);
int poe_reset_btn_trigger_chip(poe_id_t *pid);
int poe_reset_btn_trigger_all(void);
int poe_reset_btn_trigger_hw_port(poe_hw_port_t *pphp);
int poe_reset_btn_trigger_user_port(uint8_t user_port);
int poe_recover_from_reset_non_auto_hw_port(poe_hw_port_t *pphp, uint8_t mode);
int poe_recover_from_reset_non_auto_chip(poe_id_t *pid, uint8_t mode);
int poe_recover_from_reset_non_auto_all(uint8_t mode);
int poe_recover_only_non_auto_hw_port(poe_hw_port_t *pphp, uint8_t mode);
int poe_recover_only_non_auto_chip(poe_id_t *pid, uint8_t mode);
int poe_recover_only_non_auto_all(uint8_t mode);
int poe_reset_btn_trigger_non_auto_hw_port(poe_hw_port_t *pphp);
int poe_reset_btn_trigger_non_auto_user_port(uint8_t user_port);
int poe_reset_btn_trigger_non_auto_all(void);
int poe_power_off_btn_trigger_non_auto_hw_port(poe_hw_port_t *pphp);
int poe_power_off_btn_trigger_non_auto_user_port(uint8_t user_port);
int poe_power_off_btn_trigger_non_auto_all(void);
int poe_power_on_btn_trigger_non_auto_hw_port(poe_hw_port_t *pphp);
int poe_power_on_btn_trigger_non_auto_user_port(uint8_t user_port);
int poe_power_on_btn_trigger_non_auto_all(void);
int poe_port_enable_cls4p_hw_port(poe_hw_port_t *pphp, uint8_t en);
int poe_port_enable_cls4p_user_port(uint8_t user_port, uint8_t en);
int poe_port_enable_cls4p_chip(poe_id_t *pid, uint8_t en);
int poe_port_enable_cls4p_all(uint8_t en);
int poe_port_init(poe_id_t *p, uint8_t mode);
int poe_int_en(poe_id_t *p, uint8_t en);
int poe_int_show(char *int_name, uint8_t ports_msk, uint8_t base_port);
int poe_int_hdl_pwr_en_changed(poe_id_t *p, int_special_hdl_t *pish);
int poe_int_hdl_pwr_good_changed(poe_id_t *p, int_special_hdl_t *pish);
int poe_int_hdl_disconnect(poe_id_t *p, int_special_hdl_t *pish);
int poe_int_hdl_detect_done(poe_id_t *p, int_special_hdl_t *pish);
int poe_int_hdl_class_done(poe_id_t *p, int_special_hdl_t *pish);
int poe_int_hdl_over_current_after_power_on(poe_id_t *p, int_special_hdl_t *pish);
int poe_int_hdl_limit_current_on_power_up(poe_id_t *p, int_special_hdl_t *pish);
int poe_int_hdl_supply(poe_id_t *p, int_special_hdl_t *pish);
int poe_int_hdl_limit_current_on_power_on(poe_id_t *p, int_special_hdl_t *pish);
int poe_int_hdl(uint8_t intidx);
int poe_init(void);
int poe_init_auto_mode(void);
void poe_task( void *pvParameters );
void create_poe_task(void);


#endif

