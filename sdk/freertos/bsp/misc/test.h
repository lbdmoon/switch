
#ifndef __TEST_H__
#define __TEST_H__

#include "poe.h"


#define DEBUG_TEST_ENABLE					(1)

extern unsigned char g_is_master;
void remote_uart_print(const char *ptr_fmt, ...);

#if defined(DEBUG_TEST_ENABLE) && DEBUG_TEST_ENABLE
//#define DEBUG_MSG(x...)		do{printf(x);}while(0)
#define DEBUG_MSG(x...)	\
			do{if(g_is_master){remote_uart_print(x);}else{osal_printf(x);}}while(0)
#else
#define DEBUG_MSG(x...)		do{}while(0)
#endif

#define BITMSK(x)			(1 << (x))

extern uint32_t debug_code_mask;

enum
{
	DBG_SAMPLING = 0,
	DBG_STATISTICS,
	DBG_CALENDAR,
	DBG_POE,
	
	DBG_REMOTE_PRINT_ENABLE,
	DBG_REMOTE_PRINT_DBG_INFO_ENABLE,

	DBG_MAX,
};

#define CK_DBG_EN(x)	(debug_code_mask & BITMSK(x))

extern uint8_t poe_status_routing_msk;
#define CK_POE_ST_RT(x)		(poe_status_routing_msk & (x))
#define POE_STATUS_ROUTING_PRINT(x...)		\
	do{if(CK_POE_ST_RT(POE_STATUS_PRINT_EN)){osal_printf(x);}}while(0)


#define POE_TIMER_NAME						"POE Timer"
#define POE_TIMER_INTERVAL_MS				(500)

#define POE_STATUS_TIMER_NAME				"POE Status Timer"
#define POE_STATUS_TIMER_INTERVAL_MS		(1000)

#define LED_TIMER_NAME						"LED Status Timer"
#define LED_TIMER_INTERVAL_MS				(1000)

#define TEST_DEBUG							"debug"
#define TEST_SWITCH_MAC						"switch_mac"
#define TEST_POE							"poe"
#define TEST_FAN							"fan"
#define TEST_FUNC							"func"
#define TEST_LED							"led"
#define TEST_KEY							"key"
#define TEST_DATE							"date"
#define TEST_STATISTICS						"statistics"
#define TEST_CALENDAR						"calendar"

#define DEBUG_SET_MASK						"set"
#define DEBUG_GET_MASK						"get"

#define POE_INIT							"init"
#define POE_INFO							"info"
#define POE_REG								"reg"
#define POE_PWR								"pwr"
#define POE_MGMT							"mgmt"
#define POE_STATUS							"status"

#define DATE_CMD_INIT						"init"
#define DATE_CMD_SHOW						"show"

#define CALENDAR_CMD_ADD					"add"
#define CALENDAR_CMD_DEL					"del"

#define MAX_NUM_OF_ARGS				(10)
#define MAX_ORG_ARG_BUF				(100)

#define MAX_NUM_FAN					(2)

#define FAN_1_GPIO_ON_OFF			(5)
#define FAN_1_GPIO_LEVEL			(13)
#define FAN_2_GPIO_ON_OFF			(15)
#define FAN_2_GPIO_LEVEL			(17)

enum
{
	FAN_GPIO_SET_ON = 0,
	FAN_GPIO_SET_OFF,
	FAN_GPIO_SET_ON_LVL_1 = 0,
	FAN_GPIO_SET_ON_LVL_2,
};

typedef enum
{
	FAN_SET_OFF = 0,
	FAN_SET_ON_LVL_1,
	FAN_SET_ON_LVL_2,
	FAN_SET_MAX,
}fan_set_status_e;

typedef struct
{
	uint8_t gpio_on;
	uint8_t gpio_lvl;
}fan_gpio_t;

#define MAX_NUM_GPIO_LED			(2)
#define MAX_NUM_LED					(MAX_NUM_GPIO_LED + 28)
#define GET_SWITCH_PORT_LED(x)		((x) -MAX_NUM_GPIO_LED + 1/* switch port start from 1 */)

#define LED_1_GPIO_ON_OFF			(6)
#define LED_2_GPIO_ON_OFF			(0)

enum
{
	LED_GPIO_SET_ON = 0,
	LED_GPIO_SET_OFF,
};

typedef enum
{
	LED_SET_OFF = 0,
	LED_SET_ON,
	LED_SET_FLASH,
	LED_SET_TIMER_STOP,
	LED_SET_NORMAL,
	LED_SET_MAX,
}led_set_status_e;

typedef struct
{
	uint8_t gpio;
}led_gpio_t;

#define KEY_GPIO_API_MODE					(0)

#define KEY_TIMER_NAME						"KEY Status Timer"
#define KEY_TIMER_INTERVAL_MS				(1000)

#define KEY_1_GPIO_ID						(9)//(1)
#define KEY_2_GPIO_ID						(9)

#define KEY_1_GPIO_IOMUX					(AIR_CHIPSCU_IOMUX_FORCE_GPIO9_MODE)//AIR_CHIPSCU_IOMUX_FORCE_GPIO1_MODE
#define KEY_2_GPIO_IOMUX					(AIR_CHIPSCU_IOMUX_FORCE_GPIO9_MODE)

typedef struct
{
	uint32_t reg_mux, reg_mode, reg_get, idx_mode;
}gpio_reg_t;

typedef struct
{
	char *cmd;
	int num_args;
	int (*f)(int argc, char **argv);
}cmdline_func_t;

enum
{
	DATA_TYPE_POINTER = 0,
	DATA_TYPE_VAL_DEC,
	DATA_TYPE_VAL_HEX,
	DATA_TYPE_MAX,
};

typedef struct
{
	uint8_t type;
	union
	{
		uint8_t *p;
		uint32_t val;
	}u;
}format_argv_t;

typedef enum
{
	POE_STATUS_ON = (1 << 0),
	POE_STATUS_POE_ID_TEST = (1 << 1),
	POE_STATUS_TIMER_DBG_EN = (1 << 2),
	POE_STATUS_PRINT_EN = (1 << 3),
	POE_STATUS_FREERTOS_DBG_EN = (1 << 4),
}poe_status_routing_e;

//extern poe_id_t poeid;
//extern poe_id_t *pid;
void show_perpheral_reg(uint32_t start, uint32_t size);
void show_mem(uint32_t start, uint32_t size);
void waiting_for_wd(void);
int set_pse_reset_flag(uint8_t cidx);
uint32_t check_pse_reseted(uint8_t en_reinit);
int tm_chip_reg(poe_id_t *id, uint8_t reg, uint8_t val, uint8_t set);
int debug_func(int argc, char *argv[]);
int do_debug(int argc, char *argv[]); 
int fan_init(void);
int fan_func(int argc, char *argv[]);
int do_fan(int argc, char *argv[]); 
int test_func(int argc, char *argv[]);
int do_func(int argc, char *argv[]); 
int key_init(void);
void key_gpio_get(uint8_t gpio_pin, uint8_t *status);
uint8_t key_get_status(void);
void led_port_setting(uint8_t port, led_set_status_e st);
void led_timer_cb(TimerHandle_t xTimer);
void led_timer_init(void);
void led_timer_stop(void);
void led_gpio_set(uint8_t gpio_pin, uint8_t high);
int led_hw_setting(uint8_t idx, led_set_status_e status);
int led_sw_setting(uint8_t idx, led_set_status_e status);
int led_init(void);
int led_func(int argc, char *argv[]);
int do_led(int argc, char *argv[]);
int key_func(int argc, char *argv[]);
int do_key(int argc, char *argv[]); 
int poe_id_init(uint32_t idx, uint8_t unit, uint8_t chn, uint8_t dev);
int do_switch_mac(int argc, char *argv[]);
int do_poe(int argc, char *argv[]);
int poe_detect_chip_type(poe_id_t *pid, uint8_t chip_idx, uint8_t base_port);
int poe_detect_chip_all(poe_id_t *pid_start, uint8_t num_pid);
int poe_chip_init_all(poe_id_t *pid_start, uint8_t num_pid, uint8_t mode, uint8_t hw_pwr_mg);
int poe_init_all(uint8_t mode);
void poe_pwr_mgmt_routing(uint8_t en);
void timer_debug_start(char *file, int line, const char *func);
void timer_debug_quit(char *file, int line, const char *func);
void freertos_debug(char *file, int line, const char *func);
void poe_status_occured_update(poe_id_t *pid, poe_status_occured_t *ppso);
void poe_status_now_update(poe_id_t *pid, poe_status_now_t *ppsn);

#define TIMER_DEBUG_START()	do{timer_debug_start(__FILE__, __LINE__, __FUNCTION__);}while(0)
#define TIMER_DEBUG_QUIT()	do{timer_debug_quit(__FILE__, __LINE__, __FUNCTION__);}while(0)
#define FREERTOS_DEBUG()	do{freertos_debug(__FILE__, __LINE__, __FUNCTION__);}while(0)

/*
	Function Name poe_status_routing
	Descript:for virtual interrupt and show POE status in period
	Argument poe_status_routing_e en:
		bit0:[poe virtual interrupt enable:1|disable:0] 
		bit1:[poe id test:1|normal:0] 
		bit2:[timer debug enable:1|disable:0] 
		bit3:[status print enable:1|disable:0] 
		bit4:[OS debug enable:1|disable:0]
*/
void poe_status_routing(poe_status_routing_e en);
portBASE_TYPE testing(signed char *buf, size_t len, const signed char * input);
char *dbg_get_current_task_name(void);
void dbg_task_msg_print(void);
void dbg_task_print(void);
void dbg_task_runtime_print(void);

unsigned int io_read32(unsigned int addr);
void io_write32(unsigned int addr, unsigned int vlaue);

void DUMP_STACK(void);

#endif

