
/* Standard includes. */
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <machine/endian.h>

/* Kernel includes. */
#include <FreeRTOS.h>
#include "FreeRTOSConfig.h"
#include "task.h"
#include "timers.h"
#include "queue.h"
#include "osal.h"
#include "cache.h"

/* module includes */
#include <poe.h>
#include <os_func.h>
#include <air_error.h>
#include <air_sif.h>
#include <air_i2c.h>
#include <i2c_cmd.h>
#include <pp_def.h>
#include <air_l2.h>
#include <test.h>
#include <product.h>

TaskHandle_t mischdl_taskHandle = NULL;
static poe_chip_power_status_t poe_chip_power_status[POE_MAX_CURRENT_CHIP];
#if defined(SIF_TESTING_ENABLE) && SIF_TESTING_ENABLE
TaskHandle_t test_sif_task_hdl[TEST_SIF_TASK_MAX_NUM];
#endif

void
show_task_stack_info(TaskHandle_t ptsk)
{
	UBaseType_t usize;
	char *pn;

	if(!ptsk)
	{
		return;
	}

	pn = pcTaskGetName(ptsk);
	usize = uxTaskGetStackHighWaterMark(ptsk);
	printf("Task %s,MAX Stack High Water Mark is %lu\n", pn, usize);
}

void
show_all_task_stack_info(void)
{
	int idx = 0;
#ifdef SKU_2150
	TaskHandle_t tsk[TEST_SIF_TASK_MAX_NUM + 4] = {NULL, };
#else
	TaskHandle_t tsk[TEST_SIF_TASK_MAX_NUM + 3] = {NULL, };
#endif

	tsk[idx++] = user_taskHandle;
	tsk[idx++] = pse_taskHandle;
#ifdef SKU_2150
	tsk[idx++] = sfp_taskHandle;
#endif
	tsk[idx++] = mischdl_taskHandle;
#if defined(SIF_TESTING_ENABLE) && SIF_TESTING_ENABLE
	memcpy(&tsk[idx], &test_sif_task_hdl[0], sizeof(test_sif_task_hdl));
#endif

	for(idx = 0; idx < NUMOFARRAY(tsk); idx++)
	{
		if(tsk[idx])
		{
			show_task_stack_info(tsk[idx]);
		}
	}

	show_task_stack_detail(tsk[NUMOFARRAY(tsk) - 1]);
}

AIR_ERROR_NO_T
_sif_read_slave_8855M_block(
			uint32_t start, void *p_data, uint32_t size/* size bigger than 4B */)
{
	AIR_ERROR_NO_T   rc = AIR_E_OK;
	AIR_SIF_INFO_T   sif_info;
	AIR_SIF_PARAM_T  sif_param;

	if(size <= 4)
	{
		return _sif_read_slave_8855M_reg(start, p_data);
	}

	sif_info.channel = 0;
	sif_info.slave_id = 0x30;

	sif_param.addr_len = 4;
	sif_param.addr = start;
	sif_param.data_len = size;
	sif_param.info.ptr_data = (UI32_T*)p_data;

	rc = air_sif_read(0, &sif_info, &sif_param);
	return rc;
}

AIR_ERROR_NO_T
_sif_write_slave_8855M_block(
			uint32_t start, void *p_data, uint32_t size/* size bigger than 4B */)
{
    AIR_ERROR_NO_T   rc = AIR_E_OK;
    AIR_SIF_INFO_T   sif_info;
    AIR_SIF_PARAM_T  sif_param;

	if(size <= 4)
	{
		return _sif_write_slave_8855M_reg(start, *(uint32_t*)p_data);
	}

    sif_info.channel = 0;
    sif_info.slave_id = 0x30;

    sif_param.addr_len = 4;
    sif_param.addr = start;
    sif_param.data_len = size;
    sif_param.info.ptr_data = (UI32_T*)p_data;

    rc = air_sif_write(0, &sif_info, &sif_param);
    return rc;
}

uint32_t
get_img_info(img_info_t *pii)
{
	if(!pii)
	{
		return (uint32_t)-1;
	}

	pii->start = &LMA_BEGIN_FLASH;
	pii->end = &LMA_END_FLASH;
	pii->size = (uint32_t)pii->end - (uint32_t)pii->start;

	return pii->size;
}

uint32_t
get_img_crc(void)
{
	img_info_t ii, *pii = &ii;
	uint32_t crcv;
	uint8_t *pc;

	get_img_info(pii);
	pc = pii->end;

#if (__BYTE_ORDER == __LITTLE_ENDIAN)
	crcv = ((uint32_t)pc[0] << 0) | ((uint32_t)pc[1] << 8) |
			((uint32_t)pc[2] << 16) | ((uint32_t)pc[3] << 24);
#elif (__BYTE_ORDER == __BIG_ENDIAN)
	crcv = ((uint32_t)pc[0] << 24) | ((uint32_t)pc[1] << 16) |
			((uint32_t)pc[2] << 8) | ((uint32_t)pc[3] << 0);
#else
#error "No __BYTE_ORDER defined"
#endif

	return crcv;
}

int
system_inspection(void)
{
	img_info_t ii, *pii = &ii;
	char *p;
	uint32_t crc_ck, crc_getting;
	uint8_t matched;
	uint32_t data, reg;

	reg = CHIPSCU_SYS_CTRL1;
	data = io_read32(reg);
	OS_PRINT("Reg 0x%lx Value 0x%lx\n", reg, data);

	OS_PRINT("MB %s\n", MKSTR(MODULE_BOARD));
	OS_PRINT("SF %s\n", MKSTR(VERCODE));
	OS_PRINT("ENV %s\n", MKSTR(CMPLENV));

#if (__BYTE_ORDER == __LITTLE_ENDIAN)
	p = "little endian";
#elif (__BYTE_ORDER == __BIG_ENDIAN)
	p = "big endian";
#else
#error "No __BYTE_ORDER defined"
#endif

	OS_PRINT("CPU is %s\n", p);

	get_img_info(pii);
	OS_PRINT("Image start at 0x%p,end at 0x%p,size is 0x%lx\n", pii->start, pii->end, pii->size);

	crc_getting = get_img_crc();
	crc_ck = crc32buf(pii->start, pii->size);
	matched = (crc_getting == crc_ck);
	OS_PRINT("CRC getting 0x%lx calculated 0x%lx,CRC is %s !!!\n", crc_getting, crc_ck,
		matched ? "OK" : "Error");

	if(!matched)
	{
		OS_PRINT("Waiting for restart!!!\n");
		while(1);
	}

	return 0;
}

void
test_code(void)
{
	AIR_ERROR_NO_T aret;
	uint8_t val;
	int i;
	uint32_t link, rx;

	aret = _sif_read_slave_PSE_reg(POE_CHIP->dev, POE_T760X_ID_REG, &val);
	DBG_PRINT("sif read reg 0x%x val 0x%x,return %d\n", POE_T760X_ID_REG, val, aret);

	for(i = 0; i < 12; i++)
	{/* Port 5 & 11 is serdes port */
		if(i < 6)
		{
			link = _get_master_8855M_port_link(i);
			rx = _get_master_8855M_port_rx_pkt(i);
			DBG_PRINT("port %d link %s rx %d\n", i, link ? " up " : "down", rx);
		}
#ifdef SKU_2151
		else
		{
			link = _get_slave_8855M_port_link(i - 6);
			rx = _get_slave_8855M_port_rx_pkt(i - 6);
			DBG_PRINT("port %d link %s rx %d\n", i, link ? " up " : "down", rx);
		}
#endif
	}
}

void
serial_puts(char *s)
{
	while(*s)
	{
		if(*s == '\n')
		{
			serial_outc('\r');
		}
		serial_outc(*s++);
	}
}

void
serial_put_hex_4(uint8_t val)
{
	uint8_t v;

	v = val & 0xf;
	if(v < 0xa)
	{
		v += '0';
	}
	else
	{
		v -= 0xa;
		v += 'a';
	}

	serial_outc((char)v);
}

void
serial_put_hex_32(uint32_t val)
{
	int8_t i;
	uint8_t v;

	serial_outc('[');
	for(i = 7; i >= 0; i--)
	{
		v = (val >> (4 * i)) & 0xf;
		serial_put_hex_4(v);
	}
	serial_outc(']');
}

#if defined(REMOTE_UART_ENABLE) && REMOTE_UART_ENABLE
int
remote_uart_master_tx(char *str, uint8_t sz/* size include '\0' */)
{
	interaction_t *pru;
	static uint8_t idx = 0;
	AIR_ERROR_NO_T ret;
	uint32_t start;

	os_enter_critical();

	pru = &rmt_uart[idx % NUMOFARRAY(rmt_uart)];
	pru->mgc = MAGIC_ID_MSG;
	pru->msgt = MSG_TYPE_UART;
	pru->idx = idx;
	pru->sz = sz > sizeof(pru->buf) ? sizeof(pru->buf) : sz;
	memcpy(&pru->buf[0], str, pru->sz);
	pru->buf[pru->sz - 1] = '\0';

#if 1
	start = (uint32_t)pru;
#else
	start = (uint32_t)&((interaction_t*)0x8000e014)[idx % NUMOFARRAY(rmt_uart)];
#endif

	idx++;

	//if((ret = _sif_write_slave_8855M_block(start, pru, sizeof(*pru))) != AIR_E_OK)
	if((ret = _sif_write_slave_8855M_block(start, pru, offsetof(typeof(*pru), buf) + pru->sz)) != AIR_E_OK)
	{
		osal_printf("Error:_sif_write_slave_8855M_block error return %d\n", ret);
		os_exit_critical();

		return -1;
	}

	os_exit_critical();

	return 0;
}

int
remote_uart_slave_rx(char **ppstr, uint8_t *psz/* size include '\0' */, 
	uint8_t *pidx , uint8_t *poidx)
{
	interaction_t *pru;
	static uint8_t oidx[NUMOFARRAY(rmt_uart)];
	uint8_t i, sz, idx;
	static uint8_t done_idx = NUMOFARRAY(rmt_uart) - 1;
	static uint8_t first = 1;

	if(first)
	{
		first = 0;
		DBG_PRINT("\nrmt_uart start at 0x%p,number is %d,the size of single rmt_uart is %d(0x%x)\n\n", 
			rmt_uart, NUMOFARRAY(rmt_uart), sizeof(rmt_uart[0]), sizeof(rmt_uart[0]));
		delay1ms(100);

		memset(&oidx[0], 0xff, sizeof(oidx));
	}

	for(i = 0, idx = (done_idx + 1) % NUMOFARRAY(rmt_uart), pru = &rmt_uart[idx]; 
		i < NUMOFARRAY(rmt_uart);
		i++, idx = (idx + 1) % NUMOFARRAY(rmt_uart), pru = &rmt_uart[idx])
	{
#ifdef CFG_CACHE_ENABLE
	#if 0
		nds32_dcache_invalidate_range((unsigned long)pru, 
			(unsigned long)((uint8_t*)pru + offsetof(typeof(*pru), buf)));
	#else
		nds32_dcache_invalidate_range((unsigned long)pru, 
			(unsigned long)((uint8_t*)pru + sizeof(*pru)));
	#endif
#endif

		//DBG_PRINT("mgc:0x%x msgt:%d idx:%d sz:%d reserved:%d\n", 
		//	pru->mgc, pru->msgt, pru->idx, pru->sz, pru->reserved);

		if(pru->mgc == MAGIC_ID_MSG && pru->msgt == MSG_TYPE_UART && oidx[idx] != pru->idx)
		{
			oidx[idx] = pru->idx;
			sz = pru->sz > sizeof(pru->buf) ? sizeof(pru->buf) : pru->sz;

#ifdef CFG_CACHE_ENABLE
	#if 0
		#if 0
			nds32_dcache_invalidate_range((unsigned long)&pru->buf[0], 
				(unsigned long)((uint8_t*)&pru->buf[0] + sz));
		#else
			nds32_dcache_invalidate_range((unsigned long)&pru->buf[0], 
							(unsigned long)((uint8_t*)&pru->buf[0] + sizeof(pru->buf)));
		#endif
	#endif
#endif

			pru->buf[sizeof(pru->buf) - 1] = '\0';

			*ppstr = (char*)&pru->buf[0];
			*psz = sz;
			*pidx = pru->idx;
			*poidx = idx;

			done_idx = idx;

			return 0;
		}
	}

	return -1;
}

void
remote_uart_print(const char *ptr_fmt, ...)
{
    va_list ap;
    int len = 0;
    char buf[MAX_SZ_MSG];

    if (NULL != ptr_fmt)
    {
        va_start(ap, ptr_fmt);

        len = vsnprintf(buf, sizeof(buf), ptr_fmt, ap);
		len = (len + 1) < sizeof(buf) ? (len + 1) : sizeof(buf);
		buf[len - 1] = '\0';

        va_end(ap);

		if(g_slave_detected)
		{
			remote_uart_master_tx(buf, len);
		}
		else
		{
			serial_puts(buf);
		}
    }
}

void
remote_uart_master_test(void)
{
	uint32_t ticks = time_current_tick();
#if 0
	char buf[40];
	int ret, sz;

	sz = snprintf(buf, sizeof(buf), "Current tick is %d\n", ticks);
	sz = (sz + 1) < sizeof(buf) ? (sz + 1) : sizeof(buf);
	buf[sz - 1] = '\0';
	ret = remote_uart_master_tx(buf, sz);
#else
	remote_uart_print("Current tick is %d\n", ticks);
#endif
	//DBG_PRINT("%s remote_uart_master_tx(%s) return %d\n", __FUNCTION__, buf, ret);
}

void
remote_uart_slave_test(void)
{
	char *pbuf;
	uint8_t idx, oidx, sz, len;
	int ret;
	char tbuf[64];

	os_enter_critical();
	while((ret = remote_uart_slave_rx(&pbuf, &sz, &idx, &oidx)) == 0)
	{
#if 1
		if((len = strlen(pbuf)) + 1 != sz)
		{
			DBG_PRINT("\nError:remote buffer size is %d,actual size is %d\n\n", sz, len + 1);
		}

		if(CK_DBG_EN(DBG_REMOTE_PRINT_DBG_INFO_ENABLE))
		{
			snprintf(tbuf, sizeof(tbuf), "<M %d|S %d|sz %d> ", idx, oidx, sz);
			tbuf[sizeof(tbuf) - 1] = '\0';
			serial_puts(tbuf);
		}

		if(CK_DBG_EN(DBG_REMOTE_PRINT_ENABLE))
		{
			serial_puts(pbuf);
		}
#else
		osal_printf("<From Master idx %d|Slave idx %d> %s", idx, oidx, pbuf);
#endif
	}
	os_exit_critical();
}
#else
void
remote_uart_print(const char *ptr_fmt, ...)
{
    va_list ap;
    int len = 0;
	//uint32_t v0 = 0;
    char buf[MAX_SZ_MSG];

	//memset(tmp, 0, sizeof(tmp));
	memset(buf, 0, sizeof(buf));

	//serial_put_hex_32(__nds32__mfsr(NDS32_SR_PSW));

    if (NULL != ptr_fmt)
    {
        va_start(ap, ptr_fmt);

        len = vsnprintf(buf, sizeof(buf), ptr_fmt, ap);
		//v0 = len;
		len = (len + 1) < sizeof(buf) ? (len + 1) : sizeof(buf);
		buf[len - 1] = '\0';

        va_end(ap);

		serial_puts(buf);

#if 0
		//v1 = (tmp[0] << 24) | (tmp[1] << 16) | (tmp[2] << 8) | (tmp[3] << 0);
		//v1 = (buf[MAX_SZ_MSG + 0] << 24) | (buf[MAX_SZ_MSG + 1] << 16) | 
		//	(buf[MAX_SZ_MSG + 2] << 8) | (buf[MAX_SZ_MSG + 3] << 0);
		serial_put_hex_32(v0);
		serial_put_hex_32(__nds32__mfsr(NDS32_SR_PSW));
		serial_outc('\r');
		serial_outc('\n');
		serial_put_hex_32((uint32_t)&v0);
		serial_put_hex_32((uint32_t)&buf[0]);
		serial_outc('\r');
		serial_outc('\n');

		show_task_stack_detail(NULL);

		if(v0 > MAX_SZ_MSG)
		{
			serial_puts("-----------------------------------------------\n");
			delay1ms(10 * 1000);
			serial_puts("-----------------------------------------------\n");
		}
#endif
    }
}
#endif

#if defined(CONVERSATION_ENABLE) && CONVERSATION_ENABLE
#if 0	/* wy,debug */
void
conversation(void)
{
	AIR_ERROR_NO_T ret;
	static uint32_t last = 0x5aa555aa;
	uint32_t tmp;

	if(g_is_master)
	{
		if((ret = _sif_read_slave_8855M_reg(INTERVIEW_ADDR, &tmp)) != AIR_E_OK)
		{
			DBG_PRINT("Error:_sif_read_slave_8855M_reg error return %d\n", ret);
		}
		else
		{
			if(tmp != last)
			{
				osal_printf("Slave tell Master:0x%x\n", tmp);
				last = tmp + 1;
				if((ret = _sif_write_slave_8855M_reg(INTERVIEW_ADDR, last)) != AIR_E_OK)
				{
					DBG_PRINT("Error:_sif_write_slave_8855M_reg error return %d\n", ret);
				}
				
			}
		}
	}
	else
	{
		if((tmp = io_read32(INTERVIEW_ADDR)) != last)
		{
			osal_printf("Master tell Slave:0x%x\n", tmp);
			last = tmp + 1;

			io_write32(INTERVIEW_ADDR, last);
		}
	}
}
#else
volatile uint32_t master_tx_slave_rx = 0x80, slave_tx_master_rx = 0xff;
void
conversation(void)
{
	AIR_ERROR_NO_T ret;
	static uint32_t last = 0x5aa555aa;
	static uint8_t first = 1;
	uint32_t tmp;

	if(first)
	{
		first = 0;
		DBG_PRINT("master_tx_slave_rx:%p[value:0x%x],slave_tx_master_rx:%p[value:0x%x]\n", 
			&master_tx_slave_rx, master_tx_slave_rx,
			&slave_tx_master_rx, slave_tx_master_rx);
		delay1ms(100);
	}

	if(g_is_master)
	{
		if((ret = _sif_read_slave_8855M_reg((uint32_t)&slave_tx_master_rx, &tmp)) != AIR_E_OK)
		{
			DBG_PRINT("Error:_sif_read_slave_8855M_reg error return %d\n", ret);
		}
		else
		{
			if(tmp != last)
			{
				osal_printf("Slave tell Master:0x%x\n", tmp);
				last = tmp;

				if((ret = _sif_write_slave_8855M_reg((uint32_t)&master_tx_slave_rx, last + 1)) != AIR_E_OK)
				{
					DBG_PRINT("Error:_sif_write_slave_8855M_reg error return %d\n", ret);
				}
			}
		}
	}
	else
	{
#ifdef CFG_CACHE_ENABLE
		nds32_dcache_invalidate_range((unsigned long)&master_tx_slave_rx, 
			(unsigned long)((uint8_t*)&master_tx_slave_rx + sizeof(master_tx_slave_rx)));
#endif

		if(last != master_tx_slave_rx)
		{
			osal_printf("Master tell Slave:0x%x\n", master_tx_slave_rx);
			last = master_tx_slave_rx;

			slave_tx_master_rx = last + 1;
#ifdef CFG_CACHE_ENABLE
			nds32_dcache_flush_range((unsigned long)&slave_tx_master_rx, 
				(unsigned long)((uint8_t*)&slave_tx_master_rx + sizeof(slave_tx_master_rx)));
#endif
		}
	}
}
#endif
#endif

int
dynamic_poe_pwr_limitation_demo(poe_hw_port_t *pphp)
{
	static uint32_t next_tick = 0;
	uint32_t now_tick;
	int ret;
	static uint32_t cnt = (uint32_t)-1;

	cnt++;

	//DBG_PRINT("%s:%d\n", __FUNCTION__, __LINE__);
#if 0
	if(!pphp)
	{
		return -1;
	}
#endif

	now_tick = time_current_tick();

	//DBG_PRINT("next_tick:%d now_tick:%d\n", next_tick, now_tick);

	if(tick_after_eq(now_tick, next_tick))
	{/* timer expired */
		/* update the next tick */
		next_tick = now_tick + pdMS_TO_TICKS(POE_POWER_UPDATE_INTERVAL_MS);

		if(pphp)
		{
			if(!(cnt % 10))
			{
				DBG_PRINT("%d DPPL in special port mode\n", cnt);
			}

			if((ret = poe_set_chip_max_pwr_by_port_voltage_all(pphp, POE_MAX_BOARD_PWR_MW)) < 0)
			{
				DBG_PRINT("Error:poe_set_chip_max_pwr_by_port_voltage_all return %d\n", ret);
				return -2;
			}
		}
		else
		{
			if(!(cnt % 10))
			{
				DBG_PRINT("%d DPPL in AUTO mode\n", cnt);
			}

			if((ret = poe_set_chip_max_pwr_by_port_voltage_routing(POE_MAX_BOARD_PWR_MW)) < 0)
			{
				DBG_PRINT("Error:poe_set_chip_max_pwr_by_port_voltage_routing return %d\n", ret);
				return -3;
			}
		}
	}

	return 0;
}

void
poe_priority_dynaimc_pwr_limitation_init(void)
{
	uint8_t i, j;
	poe_chip_power_status_t *ppcps;
	poe_port_power_status_t *pppps;

	for(i = 0; i < NUMOFARRAY(poe_chip_power_status); i++)
	{
		ppcps = &poe_chip_power_status[i];

		for(j = 0; j < NUMOFARRAY(ppcps->ppps); j++)
		{
			pppps = &ppcps->ppps[j];
			pppps->st_pt_pwr = POE_PORT_PWR_ST_OFF;
			pppps->tick_pwron = 0;
		}
	}
}

uint32_t
poe_pwr_calibration(uint32_t pmw_orig)
{
	uint32_t mutip;

	mutip = pmw_orig / POE_PWR_10W_MW;

	return pmw_orig - mutip * POE_PWR_CALIBRATION_MW;
}

void
poe_overload_thread(void)
{
	static uint32_t next_tick = 0;
	uint32_t now_tick = time_current_tick();
	uint32_t pmw, pmw_calibration;
	uint8_t en[POE_MAX_CURRENT_CHIP], allen;
	int8_t cidx, pidx, ctotal, ptotal;
	poe_hw_port_t php, *pphp = &php;
	char tbuf[64], slen = 0;
	poe_chip_power_status_t *ppcps;
	poe_port_power_status_t *pppps;

	//serial_puts("OL:1\n");

	if(tick_after_eq(now_tick, next_tick))
	{/* timer expired */
#if defined(POE_REG_DEBUG_SHOW) && POE_REG_DEBUG_SHOW
		for(cidx = 0; cidx < POE_CHIP_TOTAL; cidx++)
		{
			if(!POE_CHIP[cidx].valid)
			{
				continue;
			}
			tm_chip_reg(&POE_CHIP[cidx], -1, 0, 0);
		}
#endif

recheck_power:
		//serial_puts("OL:2\n");
		memset(&en[0], 0, sizeof(en));
		//serial_puts("OL:3\n");
		poe_get_pwr_en_all(&en[0], NUMOFARRAY(en));

		//serial_puts("OL:4\n");
		poe_get_total_consumed_pwr_all(&pmw);
		pmw_calibration = poe_pwr_calibration(pmw);
		show_task_stack_detail(NULL);
		printf("PSW 0x%x\n", __nds32__mfsr(NDS32_SR_PSW));
		DBG_PRINT("%s pwr now is %dmw(%dmw),overload is %dmw,V:%dmv,C:%dua,"
			"EN[%02x %02x %02x %02x]\n", 
			__FUNCTION__, pmw, pmw_calibration, POE_MAX_BOARD_PWR_MW, 
			dbg_input_vmv, dbg_cua, en[0], en[1], en[2], en[3]);

		if(pmw_calibration > POE_MAX_BOARD_PWR_MW)
		{/* power used big than overload */
			//serial_puts("OL:7\n");
			memset(&en[0], 0, sizeof(en));
			//serial_puts("OL:8\n");
			poe_get_pwr_en_all(&en[0], NUMOFARRAY(en));
			//serial_puts("OL:9\n");

			slen = 0;
			allen = 0;
			for(cidx = 0; cidx < POE_CHIP_TOTAL && cidx < POE_MAX_CURRENT_CHIP; cidx++)
			{
				allen |= en[cidx];
				slen += snprintf(tbuf + slen, sizeof(tbuf) - slen, "0x%02x ", en[cidx]);
			}
			tbuf[sizeof(tbuf) - 1] = '\0';
			DBG_PRINT("%s tick %d,power enabled array is %s\n", 
				__FUNCTION__, now_tick, tbuf);

			if(!allen)
			{
				DBG_PRINT("\nError: %s all of power enabled port is %d\n\n", 
					__FUNCTION__, allen);
				goto done;
			}

			ctotal = POE_MAX_CURRENT_CHIP < POE_CHIP_TOTAL ? 
					POE_MAX_CURRENT_CHIP : POE_CHIP_TOTAL;
			ptotal = POE_CHIP[cidx].total_port < POE_MAX_PORT_PER_CHIP ? 
					POE_CHIP[cidx].total_port : POE_MAX_PORT_PER_CHIP;
			for(cidx = ctotal - 1; cidx >= 0; cidx--)
			{
				for(pidx = ptotal - 1; en[cidx] && pidx >= 0; pidx --)
				{
					if(!(en[cidx] & BITMSK(pidx)))
					{/* Already powered down,ignore it */
						continue;
					}

					pphp->chip_idx = cidx;
					pphp->port_idx = pidx;
					pphp->pid = &POE_CHIP[cidx];

					DBG_PRINT("%s --- power OFF on chip %d port %d\n", 
						__FUNCTION__, cidx, pidx);
					poe_power_off_btn_trigger_non_auto_hw_port(pphp);
					poe_recover_only_non_auto_hw_port(pphp, POE_CHIP_MODE);

					ppcps = &poe_chip_power_status[cidx];
					pppps = &ppcps->ppps[pidx];
					pppps->st_pt_pwr = POE_PORT_PWR_ST_OFF;
					pppps->tick_pwron = time_current_tick();

					vTaskDelay(1 * configTICK_RATE_HZ);

					goto recheck_power;
					//goto done;
				}
			}
				
		}
		else
		{/* do nothing */

		}

done:
		//serial_puts("OL:11\n");
		/* update the next tick */
		next_tick = time_current_tick() + pdMS_TO_TICKS(POE_OVERLOAD_INTERVAL_MS);
	}
}

void
poe_power_on_thread(void)
{
	static uint32_t next_tick = 0;
	uint32_t now_tick = time_current_tick();
	uint32_t pmw, pmw_calibration;
	uint8_t en[POE_MAX_CURRENT_CHIP], allen;
	uint8_t det[POE_MAX_CURRENT_CHIP][POE_MAX_PORT_PER_CHIP], *pdet;
	uint8_t cls[POE_MAX_CURRENT_CHIP][POE_MAX_PORT_PER_CHIP], *pcls;
	int8_t cidx, pidx;
	poe_hw_port_t php, *pphp = &php;
	char tbuf[64], slen = 0;
	char tbuf2[64], slen2 = 0;
	poe_chip_power_status_t *ppcps;
	poe_port_power_status_t *pppps;

	//serial_puts("PO:1\n");
	if(tick_after_eq(now_tick, next_tick))
	{/* timer expired */
		/* get the status of power enable */
		//serial_puts("PO:2\n");
		memset(&en[0], 0, sizeof(en));
		//serial_puts("PO:3\n");
		poe_get_pwr_en_all(&en[0], NUMOFARRAY(en));
		//serial_puts("PO:4\n");

		slen = 0;
		allen = 0;
		for(cidx = 0; cidx < POE_CHIP_TOTAL && cidx < POE_MAX_CURRENT_CHIP; cidx++)
		{
			//serial_puts("PO:5\n");
			allen |= en[cidx];
			slen += snprintf(tbuf + slen, sizeof(tbuf) - slen, "0x%02x ", en[cidx]);
		}
		//serial_puts("PO:6\n");
		tbuf[sizeof(tbuf) - 1] = '\0';
		DBG_PRINT("%s tick %d,power enabled array is %s\n", 
			__FUNCTION__, now_tick, tbuf);
		//serial_puts("PO:7\n");

		for(cidx = 0; cidx < POE_CHIP_TOTAL && cidx < POE_MAX_CURRENT_CHIP; cidx++)
		{
			for(pidx = 0; 
				pidx < POE_CHIP[cidx].total_port && pidx < POE_MAX_PORT_PER_CHIP; 
				pidx++)
			{
#if 1
				ppcps = &poe_chip_power_status[cidx];
				pppps = &ppcps->ppps[pidx];

				if((en[cidx] & BITMSK(pidx)) && 
					(pppps->st_pt_pwr != POE_PORT_PWR_ST_ON))
				{
					pppps->st_pt_pwr = POE_PORT_PWR_ST_ON;
					pppps->tick_pwron = time_current_tick();

					DBG_PRINT("\n%s *** power ON occur on chip %d port %d\n\n", 
						__FUNCTION__, cidx, pidx);
				}

				if(!(en[cidx] & BITMSK(pidx)) && 
					(pppps->st_pt_pwr != POE_PORT_PWR_ST_OFF))
				{
					if(tick_after_eq(now_tick, 
						pppps->tick_pwron + POE_COUPLED_PWR_ON_INTERVAL_MS))
					{
						pphp->chip_idx = cidx;
						pphp->port_idx = pidx;
						pphp->pid = &POE_CHIP[cidx];

						poe_power_off_btn_trigger_non_auto_hw_port(pphp);
						poe_recover_only_non_auto_hw_port(pphp, POE_CHIP_MODE);

						pppps->st_pt_pwr = POE_PORT_PWR_ST_OFF;
						pppps->tick_pwron = time_current_tick();

						DBG_PRINT("\n%s *** power OFF occur on chip %d port %d,"
							"power off this port\n\n", 
							__FUNCTION__, cidx, pidx);
					}
					else
					{
						DBG_PRINT("\n%s chip %d port %d power OFF occur but time isn't expired\n\n", 
							__FUNCTION__, cidx, pidx);
					}
				}
#else
				if(en[cidx] & BITMSK(pidx))
				{/* Already powered ON,ignore it */
					continue;
				}

				/* filiter out the port with not power ON */
				ppcps = &poe_chip_power_status[cidx];
				pppps = &ppcps->ppps[pidx];

				if(pppps->st_pt_pwr != POE_PORT_PWR_ST_OFF)
				{/* update the port power status */
					pppps->st_pt_pwr = POE_PORT_PWR_ST_OFF;
					//pppps->tick_pwron = time_current_tick();

					DBG_PRINT("\n%s --- power OFF occur on chip %d port %d\n\n", 
						__FUNCTION__, cidx, pidx);
				}
#endif
			}
		}

		//serial_puts("PO:8\n");

		poe_get_total_consumed_pwr_all(&pmw);
		pmw_calibration = poe_pwr_calibration(pmw);
		DBG_PRINT("%s pwr now is %dmw(%dmw),usable is %dmw\n", 
			__FUNCTION__, pmw, pmw_calibration, POE_USABLE_BOARD_PWR_MW);

		if(pmw_calibration < POE_USABLE_BOARD_PWR_MW)
		{/* power used less than usable */
			/* get the status of detected and classified */
			memset(&det[0], 0, sizeof(det));
			memset(&cls[0], 0, sizeof(cls));
			for(cidx = 0; cidx < POE_CHIP_TOTAL && cidx < POE_MAX_CURRENT_CHIP; cidx++)
			{
				slen = slen2 = 0;
				for(pidx = 0; 
					pidx < POE_CHIP[cidx].total_port && pidx < POE_MAX_PORT_PER_CHIP; 
					pidx++)
				{
					pphp->chip_idx = cidx;
					pphp->port_idx = pidx;
					pphp->pid = &POE_CHIP[cidx];

					pdet = &det[cidx][pidx];
					pcls = &cls[cidx][pidx];

					poe_get_det_cls_hw_port(pphp, pdet, pcls);

					slen += snprintf(tbuf + slen, sizeof(tbuf) - slen, "\t%d", *pdet);
					slen2 += snprintf(tbuf2 + slen2, sizeof(tbuf2) - slen2, "\t%d", *pcls);
				}

				tbuf[sizeof(tbuf) - 1] = '\0';
				tbuf2[sizeof(tbuf2) - 1] = '\0';
				DBG_PRINT("%s chip %d port total %d max %d detect & class:\n", 
					__FUNCTION__, cidx, 
					POE_CHIP[cidx].total_port, POE_MAX_PORT_PER_CHIP);
				DBG_PRINT("detected:%s\n", tbuf);
				DBG_PRINT("classified:%s\n", tbuf2);
			}

			/* check port for power ON */
			for(cidx = 0; cidx < POE_CHIP_TOTAL && cidx < POE_MAX_CURRENT_CHIP; cidx++)
			{
#if 0
				DBG_PRINT("%s chip %d power enabled array 0x%02x\n", 
					__FUNCTION__, cidx, en[cidx]);
#endif

				for(pidx = 0; 
					pidx < POE_CHIP[cidx].total_port && pidx < POE_MAX_PORT_PER_CHIP; 
					pidx++)
				{
					if(en[cidx] & BITMSK(pidx))
					{/* Already powered ON,ignore it */
						continue;
					}

					/* filiter out the port with not power ON */
					pdet = &det[cidx][pidx];
					pcls = &cls[cidx][pidx];

					ppcps = &poe_chip_power_status[cidx];
					pppps = &ppcps->ppps[pidx];

#if 0
					DBG_PRINT("%s port %d,detect %d,class %d\n", 
							__FUNCTION__, pidx, *pdet, *pcls);

					if(poe_detect_success(*pdet) == E_OK && 
						poe_class_success(*pcls) == E_OK)
					{
						DBG_PRINT("%s no power ON on chip %d port %d,status %d tick %d\n", 
							__FUNCTION__, cidx, pidx, pppps->st_pt_pwr, pppps->tick_pwron);
					}
#endif

					if(poe_detect_success(*pdet) == E_OK && 
						poe_class_success(*pcls) == E_OK &&
						pppps->st_pt_pwr == POE_PORT_PWR_ST_OFF &&
						tick_after_eq(now_tick, 
							pppps->tick_pwron + POE_COUPLED_PWR_ON_INTERVAL_MS))
					{/* PD which connected to this port has been detected and classified
						and the interval between coupled power ON after 3s */
						pphp->chip_idx = cidx;
						pphp->port_idx = pidx;
						pphp->pid = &POE_CHIP[cidx];

						DBG_PRINT("\n%s +++ power ON on chip %d port %d\n\n", 
							__FUNCTION__, cidx, pidx);
						poe_power_on_btn_trigger_non_auto_hw_port(pphp);

						pppps->st_pt_pwr = POE_PORT_PWR_ST_ON;
						pppps->tick_pwron = time_current_tick();

						//vTaskDelay(1 * configTICK_RATE_HZ / 10);

						//goto recheck_power;
						goto done;
					}
				}
			}
		}
		else
		{/* do nothing */

		}

done:
		//serial_puts("PO:11\n");
		/* update the next tick */
		next_tick = time_current_tick() + pdMS_TO_TICKS(POE_OVERLOAD_INTERVAL_MS);
	}
}

#if 1	/* code from yek */

uint8_t POE_Status[NUM_OF_CHIP][NUM_OF_PORT]; // POE状态
uint32_t Time[NUM_OF_CHIP][NUM_OF_PORT];
uint32_t Pack_Last[NUM_OF_CHIP][NUM_OF_PORT]; // 上次端口的数据包
//uint32_t Pack_This[NUM_OF_CHIP][NUM_OF_PORT]; // 这次端口的数据包
uint8_t N[NUM_OF_CHIP][NUM_OF_PORT];

#if 0
uint8_t sfp_speed[NUM_OF_SFP] ;
uint32_t sfp_cnt[NUM_OF_SFP];
uint8_t sfp_link_status;

uint32_t SW_MODE=0xffff;
uint32_t SW1 = 0xffff;
uint32_t SW2;
#endif

uint32_t random=0x7e54a67e;	/* 随机数初始值 */

volatile uint32_t count = 0; // 时钟计数

uint32_t PoE_WatchDog_TimeOut[] = {
	((1 << 1) ) * TIME_UNIT, ((1 << 2) ) * TIME_UNIT, ((1 << 3)) * TIME_UNIT, ((1 << 4) ) * TIME_UNIT, ((1 << 5) ) * TIME_UNIT,
	((1 << 6) ) * TIME_UNIT, ((1 << 7) ) * TIME_UNIT, ((1 << 8) ) * TIME_UNIT, ((1 << 9) ) * TIME_UNIT, ((1 << 10) ) * TIME_UNIT};
	
uint8_t
Check_POE_Status(uint8_t poe_idx, uint8_t port
#if 1		/* wy,debug */
	, pse_wd_spcl_args_t *ppwsa
#endif
)
{
	//poe_hw_port_t php, *pphp = &php;
	//uint8_t en, good, pwred, pwred_old;
	uint8_t pwred, pwred_old;
	static uint8_t peg[NUM_OF_CHIP] = {0,};

	if(poe_idx >= NUM_OF_CHIP || port >= NUM_OF_PORT || 
		poe_idx >= NUMOFARRAY(ppwsa->poe_en_good))
	{
		return -1;
	}

#if 1		/* wy,debug */
	pwred = !!(ppwsa->poe_en_good[poe_idx] & BITMSK(port));

	pwred_old = !!(peg[poe_idx] & BITMSK(port));
	if(pwred_old != pwred)
	{
		DBG_PRINT("POE chip %d,port %d power status change from %s to %s !!!\n",
			poe_idx, port, pwred_old ? "ON" : "OFF", pwred ? "ON" : "OFF");

		if(pwred)
		{
			peg[poe_idx] |= BITMSK(port);
		}
		else
		{
			peg[poe_idx] &= ~ BITMSK(port);
		}
	}
#else
	pphp->chip_idx = poe_idx;
	pphp->port_idx = port;
	pphp->pid = &POE_CHIP[poe_idx];

	if(poe_get_pwr_en_hw_port(pphp, &en) < 0)
	{
		DBG_PRINT("Error:get power enable on chip idx %d port %d failed\n", poe_idx, port);
		return -2;
	}

	if(poe_get_pwr_good_hw_port(pphp, &good) < 0)
	{
		DBG_PRINT("Error:get power good on chip idx %d port %d failed\n", poe_idx, port);
		return -3;
	}

	pwred = !!(en & good);
	if(pwred)
	{
		//dynamic_poe_pwr_limitation_demo(pphp);
	}
#endif
	return pwred;
}

uint8_t
Read_Link_Status(uint8_t poe_idx, uint8_t port)
{
	uint8_t master = 0, asic_port = 0;
	uint32_t link;
	uint8_t lst, lst_old;
	static uint8_t link_st[NUM_OF_CHIP] = {0,};

	if(poe_idx >= NUM_OF_CHIP || port >= NUM_OF_PORT)
	{
		return -1;
	}

#ifdef SKU_2151
	if(port < 5)
	{/* Master EN8855 chip has 0-4 port, port 5 of en8855 is serdes port */
		master = 1;
		asic_port = port;
	}
	else
	{
		master = 0;
		asic_port = port - 5;
	}
#else
	master = 1;
	asic_port = port;
#endif

	if(master)
	{
		link = _get_master_8855M_port_link(asic_port);
	}
#ifdef SKU_2151
	else
	{
		link = _get_slave_8855M_port_link(asic_port);
	}
#endif

	lst = !!link;

	lst_old = !!(link_st[poe_idx] & BITMSK(port));
	if(lst_old != lst)
	{
		DBG_PRINT("AN8855M chip %d,port %d link status change from %s to %s !!!\n",
			master ? 0 : 1, asic_port, lst_old ? "UP" : "DOWN", lst ? "UP" : "DOWN");

		if(lst)
		{
			link_st[poe_idx] |= BITMSK(port);
		}
		else
		{
			link_st[poe_idx] &= ~ BITMSK(port);
		}
	}

	return lst;	
}

uint32_t 
Read_Port_Pack(uint8_t poe_idx, uint8_t port)
{
	uint8_t master = 0, asic_port = 0;
	uint32_t rx;
#ifdef SKU_2151
	if(port < 5)
	{/* Master EN8855 chip has 0-4 port, port 5 of en8855 is serdes port */
		master = 1;
		asic_port = port;
	}
	else
	{
		master = 0;
		asic_port = port - 5;
	}
#else
	master = 1;
	asic_port = port;
#endif

	if(master)
	{
		rx = _get_master_8855M_port_rx_pkt(asic_port);
	}
#ifdef SKU_2151
	else
	{
		rx = _get_slave_8855M_port_rx_pkt(asic_port);
	}
#endif

	//DBG_PRINT("chip %d port %d rx %d\n", master ? 0 : 1, asic_port, rx);

	return rx;
}

int 
POE_reset(uint8_t poe_idx, uint8_t port)
{
	poe_hw_port_t php, *pphp = &php;

	if(poe_idx >= NUM_OF_CHIP || port >= NUM_OF_PORT)
	{
		return -1;
	}

	pphp->chip_idx = poe_idx;
	pphp->port_idx = port;
	pphp->pid = &POE_CHIP[poe_idx];

#if 0
	poe_reset_btn_trigger_non_auto_hw_port(pphp);
#else
	poe_reset_btn_trigger_hw_port(pphp);
	delay1ms(POE_INTV_BTEN_DN_2_UP_IN_RST_MS);
	poe_recover_from_reset_non_auto_hw_port(pphp, POE_CHIP_MODE);
#endif

	DBG_PRINT("reset poe idx %d port %d\n", poe_idx, port);

	return 0;
}

/* PoE Watchdog 初始化 */
void POE_WatchDog_Init(void)
{
	int i, j;
	for(i = 0; i < NUM_OF_CHIP; i++)
	{
		for(j = 0; j < NUM_OF_PORT; j++)
		{
			POE_Status[i][j] = NO_POWER;
			Pack_Last[i][j] = 0;
			//Pack_This[i][j] = 0;
			Time[i][j] = 0;
			N[i][j] = 0;
		}
	}
}
/* POE看门狗功能
 *
 * Smi	: Smi总线号
 * Port	: 端口号
 */
void POE_WatchDog(uint8_t Smi, uint8_t Port
#if 1		/* wy,debug */
	, pse_wd_spcl_args_t *ppwsa
#endif
)
{
	uint8_t Smi_Num = Smi;
	uint8_t Port_Num = Port;
	uint32_t cnt;
#if 1		/* wy,debug */
	uint32_t timeout_tick, now_tick, idx;
	static uint32_t timeout_old[NUM_OF_CHIP][NUM_OF_PORT] = {{0,},};
	uint32_t *ptimeout_old;

	if(Smi >= NUM_OF_CHIP || Port >= NUM_OF_PORT)
	{
		return;
	}
	ptimeout_old = &timeout_old[Smi_Num][Port_Num];

	//count = time_current_tick();
	count = ppwsa->tick;
#endif

	if (POE_Status[Smi_Num][Port_Num] != RESET) // 判断POE是否为重启状态
	{
		if (Check_POE_Status(Smi_Num, Port_Num, ppwsa) == 1) // 判断POE是否供电
		{
/*
			DBG_PRINT("Chip %d,Port:%d,old time %d\n", 
					Smi_Num, Port_Num, Time[Smi_Num][Port_Num]);
*/			
			if(Time[Smi_Num][Port_Num] || Read_Link_Status(Smi_Num, Port_Num) == 1 )	//判断端口是否Link Up
			{
				if (!Time[Smi_Num][Port_Num])  // 端口首次link up时开始计时
					Time[Smi_Num][Port_Num] = count;

				cnt = Read_Port_Pack(Smi_Num, Port_Num);
/*
				DBG_PRINT("Chip %d,Port:%d,Rx now %d,old %d,old time %d\n", 
					Smi_Num, Port_Num, cnt, Pack_Last[Smi_Num][Port_Num], 
					Time[Smi_Num][Port_Num]);
*/
				if (cnt != Pack_Last[Smi_Num][Port_Num]) // 这次的数据包和上次的数据包进行比较
				{
					random ^= cnt;
					Pack_Last[Smi_Num][Port_Num] = cnt;
					Time[Smi_Num][Port_Num] = count;

					/* wy,added for reset the index of PoE_WatchDog_TimeOut to start */
					N[Smi_Num][Port_Num] = 0;
				}
				else
				{
#if 1				/* wy,debug */
					now_tick = count;
					idx = N[Smi_Num][Port_Num];
					timeout_tick = Time[Smi_Num][Port_Num];					
					timeout_tick += PoE_WatchDog_TimeOut[idx];

					if(*ptimeout_old != timeout_tick)
					{
						DBG_PRINT("POE Chip %d,Port:%d,now tick %d,"
							"timeout tick from %d to %d,"
							"index %d,timeout stage %d\n", 
							Smi_Num, Port_Num, now_tick, *ptimeout_old, timeout_tick, idx, 
							PoE_WatchDog_TimeOut[idx]);

						*ptimeout_old = timeout_tick;
					}

					if(tick_after_eq(now_tick, timeout_tick))
#else
					if ((count - Time[Smi_Num][Port_Num]) >= (uint32_t)PoE_WatchDog_TimeOut[N[Smi_Num][Port_Num]])
#endif
					{
						DBG_PRINT("!!! tick after occured on chip %d port %d,"
							"now tick %d,timeout tick %d\n", 
							Smi_Num, Port_Num, now_tick, timeout_tick);
						
						POE_Status[Smi_Num][Port_Num] = RESET;
						// uint8_t Senddata[1] = {1 << (Smi_Num * 4 + Port_Num)};
						// I2C_MasterWriteData(I2C,0x2a,Senddata,1);  			//重启POE供电
						POE_reset(Smi_Num, Port_Num);
						delay1ms(500);  /* 避免同时重启，减小对电源的冲击 */
						Time[Smi_Num][Port_Num] = count;

#if 1					/* wy,debug */
						idx = (idx + 1 >= NUMOFARRAY(PoE_WatchDog_TimeOut)) ? 
								NUMOFARRAY(PoE_WatchDog_TimeOut) - 1 : idx + 1;
						N[Smi_Num][Port_Num] = idx;
#else
						if (N[Smi_Num][Port_Num] < sizeof(PoE_WatchDog_TimeOut)/sizeof(uint16_t)-1)
								N[Smi_Num][Port_Num]++;
						else
							return;
#endif
					}
				}
			}
		}
		else
		{
			if(Time[Smi_Num][Port_Num])
			{
				DBG_PRINT("POE no power on Chip %d,Port:%d,reset old time %d\n", 
					Smi_Num, Port_Num, Time[Smi_Num][Port_Num]);
			}
		
			POE_Status[Smi_Num][Port_Num] = NO_POWER;
			N[Smi_Num][Port_Num] = 0;
			Time[Smi_Num][Port_Num] = 0;
		}
	}
	else
	{
#if 1	/* wy,debug */
		now_tick = count;
		timeout_tick = Time[Smi_Num][Port_Num];
		timeout_tick += MAX_WAITING_AFTER_RESET;

/*
		DBG_PRINT("In Reset status,now tick %d,timeout tick %d\n", 
			now_tick, timeout_tick);
*/

		if(tick_after_eq(now_tick, timeout_tick))
#else
		if ((count - Time[Smi_Num][Port_Num]) > 10)
#endif
		{
			if(Time[Smi_Num][Port_Num])
			{
				DBG_PRINT("In Reset status,Chip %d,Port:%d,reset old time %d\n", 
					Smi_Num, Port_Num, Time[Smi_Num][Port_Num]);
			}

			POE_Status[Smi_Num][Port_Num] = NO_POWER;
			Time[Smi_Num][Port_Num] = 0;
		}
	}
}

void
POE_WatchDog_Mode(void)
{
	uint8_t poe_chip_idx, port;
	static uint32_t cnt = 0;
	pse_wd_spcl_args_t pwsa, *ppwsa = &pwsa;

	if(!(cnt % 60))
	{
		DBG_PRINT("%s count %d NUM_OF_CHIP:%d NUM_OF_PORT:%d\n", 
			__FUNCTION__, cnt, NUM_OF_CHIP, NUM_OF_PORT);
	}
	cnt++;

	memset(&ppwsa->poe_en_good[0], 0, sizeof(ppwsa->poe_en_good));
	poe_get_pwr_en_good_all(&ppwsa->poe_en_good[0], NUMOFARRAY(ppwsa->poe_en_good));
	ppwsa->tick = time_current_tick();
	
	for(poe_chip_idx = 0; poe_chip_idx < NUM_OF_CHIP; poe_chip_idx++)
	{
		for(port = 0; port < NUM_OF_PORT; port++)
		{
			POE_WatchDog(poe_chip_idx, port, ppwsa);
		}
	}
}
#endif

void 
_MiscHdl_AppTask(void *p)
{
	if(g_is_master)
	{
		misc_hdl_master();
	}
	else
	{
		misc_hdl_slave();
	}
}

void 
customer_createMiscHdlTask(void)
{
    xTaskCreate(_MiscHdl_AppTask,           /* The function that implements the task. */
                "MiscHdl",                  /* The text name assigned to the task - for debug only as it is not used by the kernel. */
                SIZE_OF_STACK_TASK_MISC,    /* The size of the stack to allocate to the task. */
                NULL,                       /* The parameter passed to the task - not used in this simple case. */
                configMAX_PRIORITIES - 2,   /* The priority assigned to the task. */
                &mischdl_taskHandle);
    return;
}

void
misc_hdl_master(void)
{
	char *p = NULL;

	while(1)
	{
#if defined(CONVERSATION_ENABLE) && CONVERSATION_ENABLE &&	\
	defined(CONVERSATION_TESTING_ENABLE) && CONVERSATION_TESTING_ENABLE
		conversation();
#elif defined(REMOTE_UART_ENABLE) && REMOTE_UART_ENABLE &&	\
	defined(REMOTE_UART_TESTING_ENABLE) && REMOTE_UART_TESTING_ENABLE
		remote_uart_master_test();
#else

#endif
		dump_heap_info();
		if(p)
		{
			osal_free(p);
			p = NULL;
		}
		else
		{
			p = osal_alloc(0x100, "MiscHdl");
		}

		vTaskDelay(pdMS_TO_TICKS(10000));   //in unit of ms
	}
}

void
misc_hdl_slave(void)
{
	while(1)
	{
		//DBG_PRINT("S W tick %d\n", time_current_tick());
	
#if defined(CONVERSATION_ENABLE) && CONVERSATION_ENABLE &&	\
	defined(CONVERSATION_TESTING_ENABLE) && CONVERSATION_TESTING_ENABLE
		conversation();
#elif defined(REMOTE_UART_ENABLE) && REMOTE_UART_ENABLE &&	\
	defined(REMOTE_UART_TESTING_ENABLE) && REMOTE_UART_TESTING_ENABLE
		remote_uart_slave_test();
#elif defined(REMOTE_UART_ENABLE) && REMOTE_UART_ENABLE
		remote_uart_slave_test();
#else

#endif

		//DBG_PRINT("S S tick %d\n", time_current_tick());

		vTaskDelay(pdMS_TO_TICKS(20));   //in unit of ms
	}
}

void
dump_heap_info(void)
{
	size_t size[4];

	size[0] = xPortGetFreeHeapSize();
	//DBG_PRINT("xPortGetFreeHeapSize %d\n", size[0]);

	size[1] = xPortGetMinimumEverFreeHeapSize();
	//DBG_PRINT("xPortGetMinimumEverFreeHeapSize %d\n", size[1]);

	size[2] = xPortGetMallocSize(0);
	//DBG_PRINT("xPortGetMallocSize %d\n", size[2]);

	size[3] = xPortGetHeapUsableBytes();
	///DBG_PRINT("xPortGetHeapUsableBytes %d\n", size[3]);

	DBG_PRINT("%d %d %d %d\n", size[0], size[1], size[2], size[3]);
}

int
gpio_pin_set_func_gpio(int gidx, int en)
{
	uint32_t data, reg;

	if(gidx < 0 || (gidx > 13 && gidx < 19) || gidx > 20)
	{
		DBG_PRINT("%s gpio index %d violation\n", __FUNCTION__, gidx);
		return -1;
	}

	//enable or disable gpio function
	reg = 0x1000007c;
	data = io_read32(reg);
	data = en ? (data | BITMSK(gidx)) : (data & ~ BITMSK(gidx));
	io_write32(reg, data);

	return 0;
}

int
gpio_pin_set_direction(int gidx, uint8_t dir)
{
	uint32_t data, reg;

	if(gidx < 0 || (gidx > 13 && gidx < 19) || gidx > 20 || 
		dir >= GPIO_PIN_DIRECTION_MAX)
	{
		DBG_PRINT("%s gpio index %d or direction %d violation\n", __FUNCTION__, gidx, dir);
		return -1;
	}

	if(gidx < 16)
	{
		reg = 0x1000a300;
	}
	else
	{
		reg = 0x1000a320;
	}

	data = io_read32(reg);
	if(dir == GPIO_PIN_DIRECTION_INPUT)
	{//enable gpio as input
		data &= ~(BITMSK(gidx * 2) | BITMSK(gidx * 2 + 1));
	}
	else
	{//enable gpio as output
		data &= ~(BITMSK(gidx * 2) | BITMSK(gidx * 2 + 1));
		data |= BITMSK(gidx * 2);
	}
    io_write32(reg, data);

	reg = 0x1000a314;
	data = io_read32(reg);
	if(dir == GPIO_PIN_DIRECTION_INPUT)
	{//disable gpio output enable
		data &= ~ BITMSK(gidx);
	}
	else
	{//enable gpio output enable
		data |= BITMSK(gidx);
	}
    io_write32(reg, data);


	return 0;
}

int
gpio_pin_setting(int gidx, uint32_t val)
{
	uint32_t data, reg;

	if(gidx < 0 || (gidx > 13 && gidx < 19) || gidx > 20)
	{
		DBG_PRINT("%s gpio index %d violation\n", __FUNCTION__, gidx);
		return -1;
	}

	reg = 0x1000a304;
	data = io_read32(reg);
	data = val ? (data  | BITMSK(gidx)) : (data  & ~ BITMSK(gidx));
	io_write32(reg, data);

	return 0;
}

int
gpio_pin_getting(int gidx, uint32_t *val)
{
	uint32_t data, reg;

	if(gidx < 0 || (gidx > 13 && gidx < 19) || gidx > 20)
	{
		DBG_PRINT("%s gpio index %d violation\n", __FUNCTION__, gidx);
		return -1;
	}

	reg = 0x1000a304;
	data = io_read32(reg);

	*val = !!(data & BITMSK(gidx));

	return 0;
}

void
reg_show(uint32_t start, uint32_t size)
{
	uint32_t addr = start;
	uint32_t idx, val;
	char tbuf[64];
	int slen;

	for(idx = 0, addr = start, slen = 0; addr < start + size; addr += 4, idx++)
	{
		val = io_read32(addr);

		if((idx % 4) == 0)
		{
			slen += snprintf(tbuf + slen, sizeof(tbuf) - slen, "%08lx:", addr);
		}

		slen += snprintf(tbuf + slen, sizeof(tbuf) - slen, " %08lx", val);

		if((idx % 4) == 3)
		{
			tbuf[sizeof(tbuf) - 1] = '\0';
			DBG_PRINT("%s\n", tbuf);
			slen = 0;
		}
	}

	if(slen)
	{
		tbuf[sizeof(tbuf) - 1] = '\0';
		DBG_PRINT("%s\n", tbuf);
		slen = 0;
	}
}


#if defined(SIF_TESTING_ENABLE) && SIF_TESTING_ENABLE
#if 1
void
test_sif_loop(void)
{
	uint8_t eg[NUM_OF_CHIP];

	while(1)
	{
		vTaskDelay(pdMS_TO_TICKS(TEST_SIF_TASK_PERIOD_MS));

		poe_get_pwr_en_good_all(&eg[0], sizeof(eg));
	}
}
#else
void
test_sif_loop(void)
{
	uint8_t i;
	poe_id_t *pid;
	poe_status_total_t pst, *ppst = &pst;

	while(1)
	{
		vTaskDelay(pdMS_TO_TICKS(TEST_SIF_TASK_PERIOD_MS));

		for(i = 0; i < POE_CHIP_TOTAL; i++)
		{
			pid = &POE_CHIP[i];
			poe_status_occured_update(pid, &ppst->occured);
			poe_status_now_update(pid, &ppst->now);
		}
	}
}
#endif

void 
test_sif_task_app(void *p)
{
	if(!g_is_master)
	{
		return;
	}

	test_sif_loop();
}

void 
test_sif_task_create()
{

    return;
}

void
test_sif_task_init(void)
{
	uint8_t i;
	char tbuf[32];

	if(!g_is_master)
	{
		return;
	}

	for(i = 0; i < TEST_SIF_TASK_MAX_NUM; i++)
	{
		snprintf(tbuf, sizeof(tbuf), "%s[%d]", TEST_SIF_TASK_NAME, i);
		tbuf[sizeof(tbuf) - 1] = '\0';

		xTaskCreate(test_sif_task_app,		/* The function that implements the task. */
				tbuf,						/* The text name assigned to the task - for debug only as it is not used by the kernel. */
				TEST_SIF_TASK_SIZE_STACK,	/* The size of the stack to allocate to the task. */
				NULL,						/* The parameter passed to the task - not used in this simple case. */
				TEST_SIF_TASK_PRIORITY,		/* The priority assigned to the task. */
				&test_sif_task_hdl[i]);

		DBG_PRINT("Create task %s\n", tbuf);
	}
}
#else
void
test_sif_task_init(void)
{

}
#endif

