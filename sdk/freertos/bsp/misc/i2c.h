
#ifndef __I2C_H__
#define __I2C_H__


#define I2C_OP_ERR_REPEAT					(5)


int i2c_init(uint8_t unit, uint8_t chn);
int i2c_reg8_val8_read(uint8_t unit, uint8_t chn, uint8_t dev, uint8_t reg, uint8_t *val, 
						uint8_t err_repeat);
int i2c_reg8_val8_write(uint8_t unit, uint8_t chn, uint8_t dev, uint8_t reg, uint8_t val,
						uint8_t err_repeat);
int i2c_reg8_val8_modify(uint8_t unit, uint8_t chn, uint8_t dev, 
							uint8_t reg, uint8_t set, uint8_t msk);

#endif

