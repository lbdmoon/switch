
#ifndef __PRODUCT_CONFIG_H__
#define __PRODUCT_CONFIG_H__

#define SMI_CLOCK_1_P_5625_MHZ				(1)
#define SPI_FLASH_CLOCK_50MHZ				(1)

#define POE_REG_DEBUG_SHOW					(1)

#define SIZE_OF_STACK_TASK_USER				(1024)
#define SIZE_OF_STACK_TASK_PSE				(1024)
#define SIZE_OF_STACK_TASK_SFP				(1024)
#define SIZE_OF_STACK_TASK_MISC				(1024)

#endif

