
#ifndef __OS_FUNC_H__
#define __OS_FUNC_H__

#include "poe.h"

enum os_critical
{
	OS_CRITICAL_DISABLE = 0,
	OS_CRITICAL_SCHEDULE_LOCK,
	OS_CRITICAL_INTERRUPT_LOCK,
	OS_CRITICAL_MAX,
};

#define OS_CRITICAL_TYPE					(OS_CRITICAL_SCHEDULE_LOCK)

#define NUMOFARRAY(x)						(sizeof(x) / sizeof((x)[0]))

extern unsigned char g_is_master;
void remote_uart_print(const char *ptr_fmt, ...);

#if 1
#ifndef DBG_PRINT
	#define DBG_PRINT(x...)	\
			do{if(g_is_master){remote_uart_print(x);}else{osal_printf(x);}}while(0)
#endif
#else
#ifndef DBG_PRINT
	#define DBG_PRINT(x...)	\
		do{osal_printf(x);}while(0)
#endif
#endif

#ifndef OS_PRINT
	#define OS_PRINT(x...)	\
			do{if(g_is_master){remote_uart_print(x);}else{printf(x);}}while(0)
#endif

#define STRLINK(x)		#x
#define MKSTR(x)		STRLINK(x)

#ifndef offsetof
#define offsetof(TYPE, MEMBER) ((size_t) &((TYPE *)0)->MEMBER)
#endif

#ifndef container_of
#define container_of(ptr, type, member) ({				\
	void *__mptr = (void *)(ptr);					\
	((type *)(__mptr - offsetof(type, member))); })
#endif

/*
 * Check at compile time that something is of a particular type.
 * Always evaluates to 1 so you may use it easily in comparisons.
 */
#define typecheck(type,x) \
		({	type __dummy; \
			typeof(x) __dummy2; \
			(void)(&__dummy == &__dummy2); \
			1; \
		})
		
/*
 * Check at compile time that 'function' is a certain type, or is a pointer
 * to that type (needs to use typedef for the function type.)
 */
#define typecheck_fn(type,function) \
		({	typeof(type) __tmp = function; \
			(void)__tmp; \
		})


#ifndef TICKS_TO_pdMS
	#define TICKS_TO_pdMS(ticks)	\
		((TickType_t)(ticks) * ((TickType_t)1000) / ((TickType_t)configTICK_RATE_HZ))
#endif

#ifndef TICKS_TO_pdS
	#define TICKS_TO_pdS(ticks)	\
		((TickType_t)(ticks) / ((TickType_t)configTICK_RATE_HZ))
#endif

#ifndef TICKS_TO_pdMS64
	#define TICKS_TO_pdMS64(ticks)	\
		((uint64_t)(ticks) * ((uint64_t)1000) / ((uint64_t)configTICK_RATE_HZ))
#endif


#define tick_after(a,b)		\
	(typecheck(uint32_t, a) && \
	 typecheck(uint32_t, b) && \
	 ((int32_t)((b) - (a)) < 0))
#define tick_before(a,b)	tick_after(b,a)

#define tick_after_eq(a,b)	\
	(typecheck(uint32_t, a) && \
	 typecheck(uint32_t, b) && \
	 ((int32_t)((a) - (b)) >= 0))
#define tick_before_eq(a,b)	tick_after_eq(b,a)

#ifndef MIN
#define MIN(x,y) ((x)<(y)?(x):(y))
#endif

#ifndef MAX
#define MAX(x,y) ((x)>(y)?(x):(y))
#endif

#define TIME_LOCAL_TIMER_MS						(60 * 1000)
#define TIME_LOCAL_TIMER_NAME					"Local Timer"

#define POE_USER_PORT_MAP				(module_product_poe_port_map_type())
#define POE_USER_PORT_BEGIN				(module_product_poe_port_user_start_idx())
#define POE_USER_PORT_END				(module_product_poe_port_user_end_idx())
#define POE_FLAT_PORT_BEGIN				(module_product_poe_port_flat_start_idx())
#define POE_FLAT_PORT_END				(module_product_poe_port_flat_end_idx())
#define POE_PORT_TOTAL					(module_product_poe_port_total())
#define POE_CHIP						(module_product_poe_chip())
#define POE_CHIP_TOTAL					(module_product_poe_chip_total())
#define POE_MAX_BOARD_PWR_MW			(module_product_poe_max_board_pwr_mw())
#define POE_USABLE_BOARD_PWR_MW			(module_product_poe_usable_board_pwr_mw())
#define POE_PWR_CALIBRATION_MW			(module_product_poe_pwr_calibration_mw())

#define SWITCH_PORT_TOTAL				(module_product_switch_port_total())
#define CPU_PORT_TOTAL					(module_product_cpu_port_total())
#define ASIC_PORT_TOTAL					(module_product_asic_port_total())
#define UPLINK_PORT_TOTAL				(module_product_uplink_port_total())
#define CPU_PORT_START					(module_product_cpu_port_start())
#define CPU_PORT_END					(module_product_cpu_port_end())
#define ASIC_PORT_START					(module_product_asic_port_start())
#define ASIC_PORT_END					(module_product_asic_port_end())
#define UPLINK_PORT_START				(module_product_uplink_port_start())
#define UPLINK_PORT_END					(module_product_uplink_port_end())

#define MW_PRODUCT_ID_EN8851C_MAX   (16)
#define MW_PRODUCT_ID_EN8851C_START (0)
#define MW_PRODUCT_ID_EN8851C_END   (MW_PRODUCT_ID_EN8851C_START + MW_PRODUCT_ID_EN8851C_MAX - 1)

#define MW_PRODUCT_ID_EN8853C_MAX   (32)
#define MW_PRODUCT_ID_EN8853C_START (MW_PRODUCT_ID_EN8851C_END + 1)
#define MW_PRODUCT_ID_EN8853C_END   (MW_PRODUCT_ID_EN8853C_START + MW_PRODUCT_ID_EN8853C_MAX - 1)

#define MW_PRODUCT_ID_AN8855M_MAX   (16)
#define MW_PRODUCT_ID_AN8855M_START (MW_PRODUCT_ID_EN8853C_END + 1)
#define MW_PRODUCT_ID_AN8855M_END   (MW_PRODUCT_ID_AN8855M_START + MW_PRODUCT_ID_AN8855M_MAX - 1)

typedef enum
{
    /* EN8851 */
    MW_PRODUCT_ID_EN8851C_8P = MW_PRODUCT_ID_EN8851C_START,
    MW_PRODUCT_ID_EN8851C_RFB,
    MW_PRODUCT_ID_EN8851C_8P_2SFP,
    MW_PRODUCT_ID_EN8851C_8P_2SFP_L,
    MW_PRODUCT_ID_EN8851C_LAST,
    /* Add a new product ID for EN8851 above this line. */

    /* EN8853 */
    MW_PRODUCT_ID_EN8853C_24P = MW_PRODUCT_ID_EN8853C_START,
    MW_PRODUCT_ID_EN8853C_RFB,
    MW_PRODUCT_ID_EN8853C_24P_EN8804_4P_4SFP,
#if 1		/* wy,added for EN8853C_16P_EN8804_2P_2SFP_NON_COMBO */
	MW_PRODUCT_ID_EN8853C_16P_EN8804_2P_2SFP_NON_COMBO,
#endif
#if 1		/* wy,added for EN8853C_24P_EN8804_2P_2SFP_NON_COMBO */
	MW_PRODUCT_ID_EN8853C_24P_EN8804_2P_2SFP_NON_COMBO,
#endif
#if 1		/* wy,added for EN8853C_24P_EN8804_2P_2SFP */
    MW_PRODUCT_ID_EN8853C_24P_EN8804_2P_2SFP,
#endif
    MW_PRODUCT_ID_EN8853C_24P_1SFP,
    MW_PRODUCT_ID_EN8853C_8P_EN8804_4SFP,
    MW_PRODUCT_ID_EN8853C_8P_EN8804_8SFP,
    MW_PRODUCT_ID_EN8853C_8P_EN8804_12SFP,
    MW_PRODUCT_ID_EN8853C_8P_EN8804_16SFP,
    MW_PRODUCT_ID_EN8853C_16P_2SFP,
    /* Add a new product ID for EN8853 above this line. */
	MW_PRODUCT_ID_EN8853C_LAST,

    /* AN8855 */
    MW_PRODUCT_ID_AN8855M_5P_1P = MW_PRODUCT_ID_AN8855M_START,
    MW_PRODUCT_ID_AN8855M_5P_1SFP,
    MW_PRODUCT_ID_AN8855M_5P_5P,
	MW_PRODUCT_ID_AN8855M_LAST,

    MW_PRODUCT_ID_LAST
}MW_PRODUCT_ID_T;

typedef struct
{
	uint8_t type;
	int count;
}critical_t;

typedef struct
{
	uint64_t time_val;		/* seconds */
	int16_t time_zone;
}time_network_t;

typedef enum
{
	CELL_SECONDS = 0,
	CELL_MINUTE,
	CELL_HOUR,
	CELL_DAY,
	CELL_WEEK,
	CELL_MONTH,
	CELL_YEAR,
	CELL_MAX,
}datetime_cell_t;

typedef enum
{
	POE_PORT_MAP_DIRECTLY = 0,
	POE_PORT_MAP_PCB2142,
	POE_PORT_MAP_MAX,
}poe_port_map_e;

typedef struct datetime
{
	int year;
	int month;
	int week;
	int day;
	int hour;
	int minute;
	int seconds;
}datetime_t;

typedef struct
{
	uint8_t inited;
	time_network_t tn;
	uint32_t ticks_init;
	uint32_t ticks;
	uint32_t ticks_carry;
}time_local_tick_t;

typedef struct
{
	uint8_t port_total;		/* All switch port,include cpu port and Asic port */
	uint8_t port_total_cpu;
	uint8_t port_total_asic;
	uint8_t port_total_uplink;

	uint8_t port_cpu_start_idx;
	uint8_t port_cpu_end_idx;

	uint8_t port_asic_start_idx;
	uint8_t port_asic_end_idx;

	uint8_t port_uplink_start_idx;
	uint8_t port_uplink_end_idx;
}board_sw_port_cfg_t;

typedef struct
{
	uint8_t port_total;		/* All POE port */	
	uint8_t port_user_start_idx;
	uint8_t port_user_end_idx;
	uint8_t port_flat_start_idx;
	uint8_t port_flat_end_idx;
	uint8_t port_map_type;	/* Accroding to enum poe_port_map_e */
}board_poe_port_cfg_t;

typedef struct
{
	uint8_t mp_id;
	char *mp_str;
	board_sw_port_cfg_t sw;
	board_poe_port_cfg_t poe;
	poe_id_t *poe_chip;
	uint8_t poe_chip_total;
	uint32_t poe_max_board_pwr_mw;		/* power limitation of overload */
	uint32_t poe_usable_board_pwr_mw;	/* power limitation of usable */
	uint32_t poe_pwr_cal_mw;			/* power calibration(mw) per 10w */
}board_port_cfg_t;

extern poe_id_t poe_id_all_16p_2p_2sfp_non_combo[];
extern uint8_t poe_id_all_16p_2p_2sfp_non_combo_total;
extern poe_id_t poe_id_all_24p_2p_2sfp_non_combo[];
extern uint8_t poe_id_all_24p_2p_2sfp_non_combo_total;
extern poe_id_t poe_id_all_24p_2p_2sfp[];
extern uint8_t poe_id_all_24p_2p_2sfp_total;

extern char *__mw_product_str;
extern uint8_t __mw_product_id;

void os_sleep(uint32_t ms);
int os_enter_critical(void);
int os_exit_critical(void);
void os_print_critical(void);
int os_status_critical_disabled(void);
uint64_t time_current_ms(void);
uint32_t time_current_tick(void);
int time_local_update(time_network_t *ptn);
void time_local_init(time_network_t *ptn);
int time_local_get_time(time_network_t *out);
int time_local_get_datetime(datetime_t *pdt);
int time_datetime_cmp(datetime_t *pdta, datetime_t *pdtb, uint8_t *perrbit);
char *module_product_id_get(uint8_t *pdct/* Accroding to MW_PRODUCT_ID_T */, board_port_cfg_t **ppbpc);
void module_product_print(board_port_cfg_t *pbpc);
void module_product_init(void);
board_port_cfg_t* module_product_cfg_get(void);
uint8_t module_product_poe_port_map_type(void);
uint8_t module_product_poe_port_user_start_idx(void);
uint8_t module_product_poe_port_user_end_idx(void);
uint8_t module_product_poe_port_flat_start_idx(void);
uint8_t module_product_poe_port_flat_end_idx(void);
uint8_t module_product_poe_port_total(void);
poe_id_t *module_product_poe_chip(void);
uint8_t module_product_poe_chip_total(void);
uint32_t module_product_poe_max_board_pwr_mw(void);
uint32_t module_product_poe_usable_board_pwr_mw(void);
uint32_t module_product_poe_pwr_calibration_mw(void);
uint8_t module_product_switch_port_total(void);
uint8_t module_product_cpu_port_total(void);
uint8_t module_product_asic_port_total(void);
uint8_t module_product_uplink_port_total(void);
uint8_t module_product_cpu_port_start(void);
uint8_t module_product_cpu_port_end(void);
uint8_t module_product_asic_port_start(void);
uint8_t module_product_asic_port_end(void);
uint8_t module_product_uplink_port_start(void);
uint8_t module_product_uplink_port_end(void);

#endif

