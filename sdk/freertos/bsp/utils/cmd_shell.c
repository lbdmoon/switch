/* Standard includes. */
#include <stdio.h>

/* Kernel includes. */
#include <FreeRTOS.h>
#include "FreeRTOSConfig.h"
#include "task.h"
#include "timers.h"
#include "queue.h"

/* Utils includes. */
#include "cmd_interpreter.h"
#include "dsh_parser.h"
#ifdef AIR_MW_SUPPORT
#include "mw_cmd_parser.h"
#endif
#include "spinorwrite.h"
#include "lwip/opt.h"
#include "lwip/tcpip.h"
#include <lwip/ip.h>
#include "lwip/dhcp.h"
#ifdef AIR_SUPPORT_IPV6
#include "lwip/priv/nd6_priv.h"
#endif

#if 1		/* wy,debug */
#include <test.h>
#endif

#define CMD_FIX_BACKSPACE_FOR_UI

extern int outbyte(int c);
extern int tftp_get_file(char *ip, char *name, unsigned int flash_addr);

#define CMD_SH_MAX_INPUT        (256)
#define CMD_SH_MAX_OUTPUT       (256)

#define IPV4_STR_SIZE        (16)
#ifdef AIR_SUPPORT_IPV6
#define IPV6_STATE_SIZE      (8)
#endif
#define IPV4_TO_STR(__buf__,__ipv4__)    \
                        sprintf(__buf__, "%d.%d.%d.%d",   \
                        ((__ipv4__)&0xFF000000)>>24,((__ipv4__)&0x00FF0000)>>16,    \
                        ((__ipv4__)&0x0000FF00)>>8, ((__ipv4__)&0x000000FF))

GDMPSRAM_DATA TaskHandle_t cmd_taskHandle = NULL;
GDMPSRAM_DATA QueueHandle_t g_cmd_queue = NULL;
GDMPSRAM_DATA CMD_PRIVILEGE_MODE_TYPE_T g_privilege_mode = CMD_PRIVILEGE_MODE_EXEC;

static portBASE_TYPE task_stats_cmd(signed char *buf, size_t len, const signed char * input);
static portBASE_TYPE run_time_stats_cmd(signed char *buf, size_t len, const signed char * input);
static portBASE_TYPE proc_cmd(signed char *buf, size_t len, const signed char * input);
static portBASE_TYPE port_stats_cmd(signed char *buf, size_t len, const signed char * input);
#ifdef IS_FPGA_STAGE
static portBASE_TYPE fpga_init_cmd(signed char *buf, size_t len, const signed char * input);
#endif
static portBASE_TYPE memory_read_cmd(signed char *buf, size_t len, const signed char * input);
static portBASE_TYPE memory_write_cmd(signed char *buf, size_t len, const signed char * input);
static portBASE_TYPE pbus_read_cmd(signed char *buf, size_t len, const signed char * input);
static portBASE_TYPE pbus_write_cmd(signed char *buf, size_t len, const signed char * input);
static portBASE_TYPE tkr_read_cmd(signed char *buf, size_t len, const signed char * input);
static portBASE_TYPE tkr_write_cmd(signed char *buf, size_t len, const signed char * input);
static portBASE_TYPE memory_dump_cmd(signed char *buf, size_t len, const signed char * input);
static portBASE_TYPE cpu_reg_dump_cmd(signed char *buf, size_t len, const signed char * input);
static portBASE_TYPE kill_wdog_cmd(signed char *buf, size_t len, const signed char * input);
static portBASE_TYPE show_interrupts_cmd(signed char *buf, size_t len, const signed char * input);
static portBASE_TYPE xmodem_rcv_cmd(signed char *buf, size_t len, const signed char * input);
static portBASE_TYPE tftp_get_cmd(signed char *buf, size_t len, const signed char * input);
static portBASE_TYPE upgrade_cmd(signed char *buf, size_t len, const signed char * input);

static portBASE_TYPE pdma_init_cmd(signed char *buf, size_t len, const signed char * input);
static portBASE_TYPE pdma_debug_cmd(signed char *buf, size_t len, const signed char * input);
static portBASE_TYPE lwip_stats_cmd(signed char *buf, size_t len, const signed char * input);
//static portBASE_TYPE httpclient_start_cmd(signed char *buf, size_t len, const signed char * input);
//static portBASE_TYPE sshd_cmd(signed char *buf, size_t len, const signed char * input);
#ifdef AIR_WEBSOCKET_DEMO
static portBASE_TYPE httpclient_ws_cmd(signed char *buf, size_t len, const signed char * input);
#endif

/* for en8851 sdk build in freeRTOS */
/*SDK session start*/
static portBASE_TYPE diag_shell_cmd(signed char *buf, size_t len, const signed char * input);
/*SDK session end*/
static portBASE_TYPE peripheral_shell_cmd(signed char *buf, size_t len, const signed char * input);
#ifdef AIR_MW_SUPPORT
static portBASE_TYPE mw_shell_cmd(signed char *buf, size_t len, const signed char * input);
static portBASE_TYPE sysmac_set_cmd(signed char *buf, size_t len, const signed char * input);
#endif

static portBASE_TYPE debug_shell_cmd(signed char *buf, size_t len, const signed char * input);
static portBASE_TYPE tftp_fw_upgrade_cmd(signed char *buf, size_t len, const signed char * input);
static portBASE_TYPE reset_cmd(signed char *buf, size_t len, const signed char * input);

static portBASE_TYPE ip_get_cmd(signed char *buf, size_t len, const signed char * input);

extern unsigned int an8801sb_phy_readBuckPbus(const unsigned short port_phy_addr, const unsigned int pbus_addr);
extern void an8801sb_phy_writeBuckPbus(const unsigned short port_phy_addr, const unsigned int pbus_addr, const unsigned int pbus_data);

static portBASE_TYPE
_ip_set_cmd(
    signed char *ptr_buf,
    size_t len,
    const signed char *ptr_input);


static const CMD_LINE_INPUT_T sysCmds[] = {
    {"task-stats",      "task-stats: Displays a table showing the state of each FreeRTOS task\r\n",                             task_stats_cmd,       0},
    {"run-time-stats",  "run-time-stats: Displays a table showing how much processing time each FreeRTOS task has used\r\n",    run_time_stats_cmd,   0},
    {"sdk",             "sdk: enter sdk diag shell mode\r\n",                                                                   diag_shell_cmd,       0},
    {"peripheral",      "peripheral: enter peripheral shell mode\r\n",                                                          peripheral_shell_cmd, 0},
#ifdef AIR_MW_SUPPORT
    {"mw",              "mw: enter magic wand debug shell mode\r\n",                                                            mw_shell_cmd,         0},
#endif
    {"debug",           "debug: enter debug shell mode\r\n",                                                                    debug_shell_cmd,      0},
    {"tftp-fw-upgrade", "tftp-fw-upgrade <server-ip> <firmware-name>: upgrade image using TFTP\r\n",                            tftp_fw_upgrade_cmd,  0},
    {"reset",           "reset: reset system\r\n",                                                                              reset_cmd,            0},
    {NULL,              NULL,                                                                                                   NULL,                 0}
};

static const CMD_LINE_INPUT_T debugCmds[] = {
    {"echo", "echo: compatiable sif read/write command\r\n", proc_cmd, 0},
    {"port-stats", "port-stats <port>: display port number\r\n", port_stats_cmd, 0},
#ifdef IS_FPGA_STAGE
    {"fpga-init", "fpga-init <type>: FPGA bit file scripts\r\n", fpga_init_cmd, 0},
#endif
    {"memrl", "memrl address: read 4 bytes from register or memory at the address\r\n", memory_read_cmd, 1},
    {"memwl", "memwl address value: write 4-byte value to register or memory at the address\r\n", memory_write_cmd, 2},
    {"memory", "memory address len: dump registers or memory for range (address)~(address+len)\r\n", memory_dump_cmd, 2},
    {"cpu_reg_dump", "cpu_reg_dump: dump CPU registers for debug\r\n", cpu_reg_dump_cmd, 0},
    {"kill_wdog", "kill_wdog: kill watchdog\r\n", kill_wdog_cmd, 0},
    {"show_interrupts", "show_interrupts: show interrupts' counters\r\n", show_interrupts_cmd, 0},
#ifndef SRAM_SHRINK___XMODEM_CMD_REMOVE
    {"xmodem_rcv", "xmodem_rcv [dst_addr]: recv xmodem data to specify addr\r\n", xmodem_rcv_cmd, 0},
#endif
    {"tftp_get", "tftp_get <ip> <name> [dst_addr]: tftp get file\r\n", tftp_get_cmd, 0},
    {"upgrade", "upgrade <type> <mode> [ip] [name]: upgrade image\r\n", upgrade_cmd, 0},

    /* PDMA test cmds */
    {"pdma_init", "pdma_init: init pdma driver\r\n", pdma_init_cmd, 0},
    {"pdma_debug", "pdma_debug: enable/disable pdma debug\r\n", pdma_debug_cmd, 0},
    {"lwip_stats", "lwip_stats: display lwip statistics\r\n", lwip_stats_cmd, 0},
    //{"httpclient_start", "httpclient_start: start httpclient\r\n", httpclient_start_cmd, 0},
    //{"sshd", "sshd: start sshd\r\n", sshd_cmd, 0},
#ifdef AIR_WEBSOCKET_DEMO
    {"wsclient", "wsclient <url>: start websocket client connect to url\r\n", httpclient_ws_cmd, 0},
#endif

    /* IP cmds */
    {"ip_get" ,  "ip_get: Show IP address\r\n", ip_get_cmd, 0},
    {"ip_set" ,  "ip_set: Set IP config. Write ip, netmask, gw and dns server in dot-deciaml notation\r\n", _ip_set_cmd, 0},
#ifdef AIR_MW_SUPPORT
    /* Set system mac */
    {"sysmac_set" ,  "sysmac_set <mac_addr>\r\n", sysmac_set_cmd, 0},
#endif
    {"pbusrl", "pbusrl address: read 4 bytes from register or memory at the address\r\n", pbus_read_cmd, 1},
    {"pbuswl", "pbuswl address value: write 4-byte value to register or memory at the address\r\n", pbus_write_cmd, 2},
    {"tkr", "tkr <phyaddr> <reg10>\r\n", tkr_read_cmd, 2},
    {"tkw", "tkw <phyaddr> <reg11> <reg12> <reg10>\r\n", tkr_write_cmd, 4},

#if 1		/* wy,debug */
	{"test" ,  "test: Do some testing\r\n", testing, 0},
#endif

    {NULL, NULL, NULL, 0}
};

static void cmd_prompt(void)
{
    switch(g_privilege_mode)
    {
        case CMD_PRIVILEGE_MODE_EXEC:
            printf("\r\n#");
            break;
        case CMD_PRIVILEGE_MODE_SDK:
            printf("\r\nsdk#");
            break;
        case CMD_PRIVILEGE_MODE_PERIPHERAL:
            printf("\r\nperipheral#");
            break;
#ifdef AIR_MW_SUPPORT
        case CMD_PRIVILEGE_MODE_MW:
            printf("\r\nmw#");
            break;
#endif
        case CMD_PRIVILEGE_MODE_DEBUG:
            printf("\r\ndebug#");
            break;
        default:
            printf("\r\n#");
            break;
    }
}

static portBASE_TYPE task_stats_cmd(signed char *buf, size_t len, const signed char * input)
{
    const char *const pcHeader = "Task            State\tPriority\tStack(cur/max/size)\tallocMem\t#\r\n*********************************************************************************\r\n";

#ifndef SRAM_SHRINK___CMD
    char * p_buf = pvPortMalloc(1024, "cmd");
    if (!p_buf)
    {
        return pdFALSE;
    }
#else
    char p_buf[1024];
#endif

    strcpy( p_buf, pcHeader );
    vTaskList(p_buf + strlen( pcHeader ) );
    printf(p_buf);

#ifndef SRAM_SHRINK___CMD
    vPortFree(p_buf);
#endif
    xPortModuleMallocDumpInfo();
    printf("\nTotal Heap Size    : %7d Bytes\n\n", configTOTAL_HEAP_SIZE);
    printf(" Heap used size : %7d Bytes\n", xPortGetHeapUsableBytes() - xPortGetFreeHeapSize());
    printf(" Free Heap Size : %7d Bytes (%d %%)\n", xPortGetFreeHeapSize(), (xPortGetFreeHeapSize()*100)/xPortGetHeapUsableBytes());
    printf(" Free Heap size : %7d Bytes (%d %%,Minimum Ever)\n", xPortGetMinimumEverFreeHeapSize(), (xPortGetMinimumEverFreeHeapSize()*100)/xPortGetHeapUsableBytes());

    return pdFALSE;
}

static portBASE_TYPE run_time_stats_cmd(signed char *buf, size_t len, const signed char * input)
{
    uint32_t currentTimeStats[32];
    memset((void*)currentTimeStats, 0, 32);

    printf("Task\t\tTick\tRatio\t5 sec\t5 sec\t10_sec\t10 sec\n*********************************************************************\n");
    airTaskGetRunTimeStats(&currentTimeStats[0], 1);

    return pdFALSE;
}

static portBASE_TYPE port_stats_cmd(signed char *buf, size_t len, const signed char * input)
{
    char cmd[32];
    int port = -1;

    sscanf(input, "%s %d", cmd, &port);
    if ((port < 0) || (port > 28))
    {
        printf("Error: Invalid port number!\r\n");
        return pdFALSE;
    }

    printf("Port%d:\r\n", port);
    printf("Tx Drop Packet      : %d\r\n", io_read32((0x10214000 + 0x200 * port)));
    printf("Tx CRC Error        : %d\r\n", io_read32((0x10214004 + 0x200 * port)));
    printf("Tx Unicast Packet   : %d\r\n", io_read32((0x10214008 + 0x200 * port)));
    printf("Tx Multicast Packet : %d\r\n", io_read32((0x1021400c + 0x200 * port)));
    printf("Tx Broadcast Packet : %d\r\n", io_read32((0x10214010 + 0x200 * port)));
    printf("Tx Collision Event  : %d\r\n", io_read32((0x10214014 + 0x200 * port)));
    printf("Tx Pause Packet     : %d\r\n", io_read32((0x1021402c + 0x200 * port)));
    printf("Tx 64    Packet     : %d\r\n", io_read32((0x10214030 + 0x200 * port)));
    printf("Tx 65-   Packet     : %d\r\n", io_read32((0x10214034 + 0x200 * port)));
    printf("Tx 128-  Packet     : %d\r\n", io_read32((0x10214038 + 0x200 * port)));
    printf("Tx 256-  Packet     : %d\r\n", io_read32((0x1021403c + 0x200 * port)));
    printf("Tx 512-  Packet     : %d\r\n", io_read32((0x10214040 + 0x200 * port)));
    printf("Tx 1024- Packet     : %d\r\n", io_read32((0x10214044 + 0x200 * port)));
    printf("Tx 1519- Packet     : %d\r\n", io_read32((0x10214048 + 0x200 * port)));
    printf("Rx Drop Packet      : %d\r\n", io_read32((0x10214080 + 0x200 * port)));
    printf("Rx Filtering Packet : %d\r\n", io_read32((0x10214084 + 0x200 * port)));
    printf("Rx Unicast Packet   : %d\r\n", io_read32((0x10214088 + 0x200 * port)));
    printf("Rx Multicast Packet : %d\r\n", io_read32((0x1021408c + 0x200 * port)));
    printf("Rx Broadcast Packet : %d\r\n", io_read32((0x10214090 + 0x200 * port)));
    printf("Rx Alignment Error  : %d\r\n", io_read32((0x10214094 + 0x200 * port)));
    printf("Rx CRC Error        : %d\r\n", io_read32((0x10214098 + 0x200 * port)));
    printf("Rx Undersize Error  : %d\r\n", io_read32((0x1021409c + 0x200 * port)));
    printf("Rx Fragment Error   : %d\r\n", io_read32((0x102140a0 + 0x200 * port)));
    printf("Rx Oversize Error   : %d\r\n", io_read32((0x102140a4 + 0x200 * port)));
    printf("Rx Jabber Error     : %d\r\n", io_read32((0x102140a8 + 0x200 * port)));
    printf("Rx Pause Packet     : %d\r\n", io_read32((0x102140ac + 0x200 * port)));
    printf("Rx 64    Packet     : %d\r\n", io_read32((0x102140B0 + 0x200 * port)));
    printf("Rx 65-   Packet     : %d\r\n", io_read32((0x102140B4 + 0x200 * port)));
    printf("Rx 128-  Packet     : %d\r\n", io_read32((0x102140B8 + 0x200 * port)));
    printf("Rx 256-  Packet     : %d\r\n", io_read32((0x102140Bc + 0x200 * port)));
    printf("Rx 512-  Packet     : %d\r\n", io_read32((0x102140C0 + 0x200 * port)));
    printf("Rx 1024- Packet     : %d\r\n", io_read32((0x102140C4 + 0x200 * port)));
    printf("Rx 1519- Packet     : %d\r\n", io_read32((0x102140C8 + 0x200 * port)));
    return pdFALSE;
}

#ifdef IS_FPGA_STAGE

#define FPGA_INIT_DELAY_TICK    (10)

#define _IO_WRITE32_DELAY(__reg__, __val__) do          \
{                                                       \
    io_write32((__reg__), (__val__));                   \
    vTaskDelay(pdMS_TO_TICKS(FPGA_INIT_DELAY_TICK));    \
}while(0)

void fpga_init_8218b_ch0_1()
{
    _IO_WRITE32_DELAY(0x10218020, 0xBE150000);
    _IO_WRITE32_DELAY(0x10218020, 0xBE190000);
    _IO_WRITE32_DELAY(0x10218020, 0xBC150008);
    _IO_WRITE32_DELAY(0x10218020, 0xBC190000);
    _IO_WRITE32_DELAY(0x10218020, 0xBE150405);
    _IO_WRITE32_DELAY(0x10218020, 0xBE190000);
    _IO_WRITE32_DELAY(0x10218020, 0xA81508EC);
    _IO_WRITE32_DELAY(0x10218020, 0xA8190000);
    _IO_WRITE32_DELAY(0x10218020, 0xBE150404);
    _IO_WRITE32_DELAY(0x10218020, 0xBE190000);
    _IO_WRITE32_DELAY(0x10218020, 0xAE155359);
    _IO_WRITE32_DELAY(0x10218020, 0xAE190000);
    _IO_WRITE32_DELAY(0x10218020, 0xBE150424);
    _IO_WRITE32_DELAY(0x10218020, 0xBE190000);
    _IO_WRITE32_DELAY(0x10218020, 0xAE155359);
    _IO_WRITE32_DELAY(0x10218020, 0xAE190000);
    _IO_WRITE32_DELAY(0x10218020, 0xBE15042C);
    _IO_WRITE32_DELAY(0x10218020, 0xBE190000);
    _IO_WRITE32_DELAY(0x10218020, 0xA2154000);
    _IO_WRITE32_DELAY(0x10218020, 0xA2190000);
    _IO_WRITE32_DELAY(0x10218020, 0xA4152020);
    _IO_WRITE32_DELAY(0x10218020, 0xA4190000);
    _IO_WRITE32_DELAY(0x10218020, 0xBE15042D);
    _IO_WRITE32_DELAY(0x10218020, 0xBE190000);
    _IO_WRITE32_DELAY(0x10218020, 0xA215C014);
    _IO_WRITE32_DELAY(0x10218020, 0xA2190000);
    _IO_WRITE32_DELAY(0x10218020, 0xA4156078);
    _IO_WRITE32_DELAY(0x10218020, 0xA4190000);
    _IO_WRITE32_DELAY(0x10218020, 0xBE150423);
    _IO_WRITE32_DELAY(0x10218020, 0xBE190000);
    _IO_WRITE32_DELAY(0x10218020, 0xA2152189);
    _IO_WRITE32_DELAY(0x10218020, 0xA2190000);
    _IO_WRITE32_DELAY(0x10218020, 0xBE150460);
    _IO_WRITE32_DELAY(0x10218020, 0xBE190000);
    _IO_WRITE32_DELAY(0x10218020, 0xA0154800);
    _IO_WRITE32_DELAY(0x10218020, 0xA0190000);
    _IO_WRITE32_DELAY(0x10218020, 0xBE150464);
    _IO_WRITE32_DELAY(0x10218020, 0xBE190000);
    _IO_WRITE32_DELAY(0x10218020, 0xA4151FB0);
    _IO_WRITE32_DELAY(0x10218020, 0xA4190000);
    _IO_WRITE32_DELAY(0x10218020, 0xA6153E30);
    _IO_WRITE32_DELAY(0x10218020, 0xA6190000);
    _IO_WRITE32_DELAY(0x10218020, 0xAA15202A);
    _IO_WRITE32_DELAY(0x10218020, 0xAA190000);
    _IO_WRITE32_DELAY(0x10218020, 0xAC15F072);
    _IO_WRITE32_DELAY(0x10218020, 0xAC190000);
    _IO_WRITE32_DELAY(0x10218020, 0xBE150465);
    _IO_WRITE32_DELAY(0x10218020, 0xBE190000);
    _IO_WRITE32_DELAY(0x10218020, 0xA0154208);
    _IO_WRITE32_DELAY(0x10218020, 0xA0190000);
    _IO_WRITE32_DELAY(0x10218020, 0xA2153A08);
    _IO_WRITE32_DELAY(0x10218020, 0xA2190000);
    _IO_WRITE32_DELAY(0x10218020, 0xA4154007);
    _IO_WRITE32_DELAY(0x10218020, 0xA4190000);
    _IO_WRITE32_DELAY(0x10218020, 0xA815619F);
    _IO_WRITE32_DELAY(0x10218020, 0xA8190000);
    _IO_WRITE32_DELAY(0x10218020, 0xBE150462);
    _IO_WRITE32_DELAY(0x10218020, 0xBE190000);
    _IO_WRITE32_DELAY(0x10218020, 0xA6155318);
    _IO_WRITE32_DELAY(0x10218020, 0xA6190000);
    _IO_WRITE32_DELAY(0x10218020, 0xAA152A58);
    _IO_WRITE32_DELAY(0x10218020, 0xAA190000);
    _IO_WRITE32_DELAY(0x10218020, 0xA41597B3);
    _IO_WRITE32_DELAY(0x10218020, 0xA4190000);
    _IO_WRITE32_DELAY(0x10218020, 0xBE150261);
    _IO_WRITE32_DELAY(0x10218020, 0xBE190000);
    _IO_WRITE32_DELAY(0x10218020, 0xA0156000);
    _IO_WRITE32_DELAY(0x10218020, 0xA0190000);
    _IO_WRITE32_DELAY(0x10218020, 0xA0150000);
    _IO_WRITE32_DELAY(0x10218020, 0xA0190000);
    _IO_WRITE32_DELAY(0x10218020, 0xBE150000);
    _IO_WRITE32_DELAY(0x10218020, 0xBE190000);
    _IO_WRITE32_DELAY(0x10218020, 0xBC150000);
    _IO_WRITE32_DELAY(0x10218020, 0xBC190000);
    printf("fpga_init_8218b_ch0_1 done!\r\n");
}

void fpga_init_8218b_ch2()
{
    _IO_WRITE32_DELAY(0x10218020, 0xBE950000);
    _IO_WRITE32_DELAY(0x10218020, 0xBE990000);
    _IO_WRITE32_DELAY(0x10218020, 0xBC950008);
    _IO_WRITE32_DELAY(0x10218020, 0xBC990000);
    _IO_WRITE32_DELAY(0x10218020, 0xBE950405);
    _IO_WRITE32_DELAY(0x10218020, 0xBE990000);
    _IO_WRITE32_DELAY(0x10218020, 0xA89508EC);
    _IO_WRITE32_DELAY(0x10218020, 0xA8990000);
    _IO_WRITE32_DELAY(0x10218020, 0xBE950404);
    _IO_WRITE32_DELAY(0x10218020, 0xBE990000);
    _IO_WRITE32_DELAY(0x10218020, 0xAE955359);
    _IO_WRITE32_DELAY(0x10218020, 0xAE990000);
    _IO_WRITE32_DELAY(0x10218020, 0xBE950424);
    _IO_WRITE32_DELAY(0x10218020, 0xBE990000);
    _IO_WRITE32_DELAY(0x10218020, 0xAE955359);
    _IO_WRITE32_DELAY(0x10218020, 0xAE990000);
    _IO_WRITE32_DELAY(0x10218020, 0xBE95042C);
    _IO_WRITE32_DELAY(0x10218020, 0xBE990000);
    _IO_WRITE32_DELAY(0x10218020, 0xA2954000);
    _IO_WRITE32_DELAY(0x10218020, 0xA2990000);
    _IO_WRITE32_DELAY(0x10218020, 0xA4952020);
    _IO_WRITE32_DELAY(0x10218020, 0xA4990000);
    _IO_WRITE32_DELAY(0x10218020, 0xBE95042D);
    _IO_WRITE32_DELAY(0x10218020, 0xBE990000);
    _IO_WRITE32_DELAY(0x10218020, 0xA295C014);
    _IO_WRITE32_DELAY(0x10218020, 0xA2990000);
    _IO_WRITE32_DELAY(0x10218020, 0xA4956078);
    _IO_WRITE32_DELAY(0x10218020, 0xA4990000);
    _IO_WRITE32_DELAY(0x10218020, 0xBE950423);
    _IO_WRITE32_DELAY(0x10218020, 0xBE990000);
    _IO_WRITE32_DELAY(0x10218020, 0xA2952189);
    _IO_WRITE32_DELAY(0x10218020, 0xA2990000);
    _IO_WRITE32_DELAY(0x10218020, 0xBE950460);
    _IO_WRITE32_DELAY(0x10218020, 0xBE990000);
    _IO_WRITE32_DELAY(0x10218020, 0xA0954800);
    _IO_WRITE32_DELAY(0x10218020, 0xA0990000);
    _IO_WRITE32_DELAY(0x10218020, 0xBE950464);
    _IO_WRITE32_DELAY(0x10218020, 0xBE990000);
    _IO_WRITE32_DELAY(0x10218020, 0xA4951FB0);
    _IO_WRITE32_DELAY(0x10218020, 0xA4990000);
    _IO_WRITE32_DELAY(0x10218020, 0xA6953E30);
    _IO_WRITE32_DELAY(0x10218020, 0xA6990000);
    _IO_WRITE32_DELAY(0x10218020, 0xAA95202A);
    _IO_WRITE32_DELAY(0x10218020, 0xAA990000);
    _IO_WRITE32_DELAY(0x10218020, 0xAC95F072);
    _IO_WRITE32_DELAY(0x10218020, 0xAC990000);
    _IO_WRITE32_DELAY(0x10218020, 0xBE950465);
    _IO_WRITE32_DELAY(0x10218020, 0xBE990000);
    _IO_WRITE32_DELAY(0x10218020, 0xA0954208);
    _IO_WRITE32_DELAY(0x10218020, 0xA0990000);
    _IO_WRITE32_DELAY(0x10218020, 0xA2953A08);
    _IO_WRITE32_DELAY(0x10218020, 0xA2990000);
    _IO_WRITE32_DELAY(0x10218020, 0xA4954007);
    _IO_WRITE32_DELAY(0x10218020, 0xA4990000);
    _IO_WRITE32_DELAY(0x10218020, 0xA895619F);
    _IO_WRITE32_DELAY(0x10218020, 0xA8990000);
    _IO_WRITE32_DELAY(0x10218020, 0xBE950462);
    _IO_WRITE32_DELAY(0x10218020, 0xBE990000);
    _IO_WRITE32_DELAY(0x10218020, 0xA6955318);
    _IO_WRITE32_DELAY(0x10218020, 0xA6990000);
    _IO_WRITE32_DELAY(0x10218020, 0xAA952A58);
    _IO_WRITE32_DELAY(0x10218020, 0xAA990000);
    _IO_WRITE32_DELAY(0x10218020, 0xA49597B3);
    _IO_WRITE32_DELAY(0x10218020, 0xA4990000);
    _IO_WRITE32_DELAY(0x10218020, 0xBE950261);
    _IO_WRITE32_DELAY(0x10218020, 0xBE990000);
    _IO_WRITE32_DELAY(0x10218020, 0xA0956000);
    _IO_WRITE32_DELAY(0x10218020, 0xA0990000);
    _IO_WRITE32_DELAY(0x10218020, 0xA0950000);
    _IO_WRITE32_DELAY(0x10218020, 0xA0990000);
    _IO_WRITE32_DELAY(0x10218020, 0xBE950000);
    _IO_WRITE32_DELAY(0x10218020, 0xBE990000);
    _IO_WRITE32_DELAY(0x10218020, 0xBC950000);
    _IO_WRITE32_DELAY(0x10218020, 0xBC990000);
    printf("fpga_init_8218b_ch2 done!\r\n");
}

void fpga_init_8218b_ch3()
{
    _IO_WRITE32_DELAY(0x10218020, 0xBE950000);
    _IO_WRITE32_DELAY(0x10218020, 0xBE990000);
    _IO_WRITE32_DELAY(0x10218020, 0xBC950008);
    _IO_WRITE32_DELAY(0x10218020, 0xBC990000);
    _IO_WRITE32_DELAY(0x10218020, 0xBE950405);
    _IO_WRITE32_DELAY(0x10218020, 0xBE990000);
    _IO_WRITE32_DELAY(0x10218020, 0xA89508EC);
    _IO_WRITE32_DELAY(0x10218020, 0xA8990000);
    _IO_WRITE32_DELAY(0x10218020, 0xBE950404);
    _IO_WRITE32_DELAY(0x10218020, 0xBE990000);
    _IO_WRITE32_DELAY(0x10218020, 0xAE955359);
    _IO_WRITE32_DELAY(0x10218020, 0xAE990000);
    _IO_WRITE32_DELAY(0x10218020, 0xBE950424);
    _IO_WRITE32_DELAY(0x10218020, 0xBE990000);
    _IO_WRITE32_DELAY(0x10218020, 0xAE955359);
    _IO_WRITE32_DELAY(0x10218020, 0xAE990000);
    _IO_WRITE32_DELAY(0x10218020, 0xBE95042C);
    _IO_WRITE32_DELAY(0x10218020, 0xBE990000);
    _IO_WRITE32_DELAY(0x10218020, 0xA2954000);
    _IO_WRITE32_DELAY(0x10218020, 0xA2990000);
    _IO_WRITE32_DELAY(0x10218020, 0xA4952020);
    _IO_WRITE32_DELAY(0x10218020, 0xA4990000);
    _IO_WRITE32_DELAY(0x10218020, 0xBE95042D);
    _IO_WRITE32_DELAY(0x10218020, 0xBE990000);
    _IO_WRITE32_DELAY(0x10218020, 0xA295C014);
    _IO_WRITE32_DELAY(0x10218020, 0xA2990000);
    _IO_WRITE32_DELAY(0x10218020, 0xA4956078);
    _IO_WRITE32_DELAY(0x10218020, 0xA4990000);
    _IO_WRITE32_DELAY(0x10218020, 0xBE950423);
    _IO_WRITE32_DELAY(0x10218020, 0xBE990000);
    _IO_WRITE32_DELAY(0x10218020, 0xA2952189);
    _IO_WRITE32_DELAY(0x10218020, 0xA2990000);
    _IO_WRITE32_DELAY(0x10218020, 0xBE950460);
    _IO_WRITE32_DELAY(0x10218020, 0xBE990000);
    _IO_WRITE32_DELAY(0x10218020, 0xA0954800);
    _IO_WRITE32_DELAY(0x10218020, 0xA0990000);
    _IO_WRITE32_DELAY(0x10218020, 0xBE950464);
    _IO_WRITE32_DELAY(0x10218020, 0xBE990000);
    _IO_WRITE32_DELAY(0x10218020, 0xA4951FB0);
    _IO_WRITE32_DELAY(0x10218020, 0xA4990000);
    _IO_WRITE32_DELAY(0x10218020, 0xA6953E30);
    _IO_WRITE32_DELAY(0x10218020, 0xA6990000);
    _IO_WRITE32_DELAY(0x10218020, 0xAA95202A);
    _IO_WRITE32_DELAY(0x10218020, 0xAA990000);
    _IO_WRITE32_DELAY(0x10218020, 0xAC95F072);
    _IO_WRITE32_DELAY(0x10218020, 0xAC990000);
    _IO_WRITE32_DELAY(0x10218020, 0xBE950465);
    _IO_WRITE32_DELAY(0x10218020, 0xBE990000);
    _IO_WRITE32_DELAY(0x10218020, 0xA0954208);
    _IO_WRITE32_DELAY(0x10218020, 0xA0990000);
    _IO_WRITE32_DELAY(0x10218020, 0xA2953A08);
    _IO_WRITE32_DELAY(0x10218020, 0xA2990000);
    _IO_WRITE32_DELAY(0x10218020, 0xA4954007);
    _IO_WRITE32_DELAY(0x10218020, 0xA4990000);
    _IO_WRITE32_DELAY(0x10218020, 0xA895619F);
    _IO_WRITE32_DELAY(0x10218020, 0xA8990000);
    _IO_WRITE32_DELAY(0x10218020, 0xBE950462);
    _IO_WRITE32_DELAY(0x10218020, 0xBE990000);
    _IO_WRITE32_DELAY(0x10218020, 0xA6955318);
    _IO_WRITE32_DELAY(0x10218020, 0xA6990000);
    _IO_WRITE32_DELAY(0x10218020, 0xAA952A58);
    _IO_WRITE32_DELAY(0x10218020, 0xAA990000);
    _IO_WRITE32_DELAY(0x10218020, 0xA49597B3);
    _IO_WRITE32_DELAY(0x10218020, 0xA4990000);
    _IO_WRITE32_DELAY(0x10218020, 0xBE950261);
    _IO_WRITE32_DELAY(0x10218020, 0xBE990000);
    _IO_WRITE32_DELAY(0x10218020, 0xA0956000);
    _IO_WRITE32_DELAY(0x10218020, 0xA0990000);
    _IO_WRITE32_DELAY(0x10218020, 0xA0950000);
    _IO_WRITE32_DELAY(0x10218020, 0xA0990000);
    _IO_WRITE32_DELAY(0x10218020, 0xBE950000);
    _IO_WRITE32_DELAY(0x10218020, 0xBE990000);
    _IO_WRITE32_DELAY(0x10218020, 0xBC950000);
    _IO_WRITE32_DELAY(0x10218020, 0xBC990000);
    printf("fpga_init_8218b_ch3 done!\r\n");
}

void fpga_init_8218b_ch4()
{
    _IO_WRITE32_DELAY(0x10218020, 0xBF950000);
    _IO_WRITE32_DELAY(0x10218020, 0xBF990000);
    _IO_WRITE32_DELAY(0x10218020, 0xBD950008);
    _IO_WRITE32_DELAY(0x10218020, 0xBD990000);
    _IO_WRITE32_DELAY(0x10218020, 0xBF950405);
    _IO_WRITE32_DELAY(0x10218020, 0xBF990000);
    _IO_WRITE32_DELAY(0x10218020, 0xA99508EC);
    _IO_WRITE32_DELAY(0x10218020, 0xA9990000);
    _IO_WRITE32_DELAY(0x10218020, 0xBF950404);
    _IO_WRITE32_DELAY(0x10218020, 0xBF990000);
    _IO_WRITE32_DELAY(0x10218020, 0xAF955359);
    _IO_WRITE32_DELAY(0x10218020, 0xAF990000);
    _IO_WRITE32_DELAY(0x10218020, 0xBF950424);
    _IO_WRITE32_DELAY(0x10218020, 0xBF990000);
    _IO_WRITE32_DELAY(0x10218020, 0xAF955359);
    _IO_WRITE32_DELAY(0x10218020, 0xAF990000);
    _IO_WRITE32_DELAY(0x10218020, 0xBF95042C);
    _IO_WRITE32_DELAY(0x10218020, 0xBF990000);
    _IO_WRITE32_DELAY(0x10218020, 0xA3954000);
    _IO_WRITE32_DELAY(0x10218020, 0xA3990000);
    _IO_WRITE32_DELAY(0x10218020, 0xA5952020);
    _IO_WRITE32_DELAY(0x10218020, 0xA5990000);
    _IO_WRITE32_DELAY(0x10218020, 0xBF95042D);
    _IO_WRITE32_DELAY(0x10218020, 0xBF990000);
    _IO_WRITE32_DELAY(0x10218020, 0xA395C014);
    _IO_WRITE32_DELAY(0x10218020, 0xA3990000);
    _IO_WRITE32_DELAY(0x10218020, 0xA5956078);
    _IO_WRITE32_DELAY(0x10218020, 0xA5990000);
    _IO_WRITE32_DELAY(0x10218020, 0xBF950423);
    _IO_WRITE32_DELAY(0x10218020, 0xBF990000);
    _IO_WRITE32_DELAY(0x10218020, 0xA3952189);
    _IO_WRITE32_DELAY(0x10218020, 0xA3990000);
    _IO_WRITE32_DELAY(0x10218020, 0xBF950460);
    _IO_WRITE32_DELAY(0x10218020, 0xBF990000);
    _IO_WRITE32_DELAY(0x10218020, 0xA1954800);
    _IO_WRITE32_DELAY(0x10218020, 0xA1990000);
    _IO_WRITE32_DELAY(0x10218020, 0xBF950464);
    _IO_WRITE32_DELAY(0x10218020, 0xBF990000);
    _IO_WRITE32_DELAY(0x10218020, 0xA5951FB0);
    _IO_WRITE32_DELAY(0x10218020, 0xA5990000);
    _IO_WRITE32_DELAY(0x10218020, 0xA7953E30);
    _IO_WRITE32_DELAY(0x10218020, 0xA7990000);
    _IO_WRITE32_DELAY(0x10218020, 0xAB95202A);
    _IO_WRITE32_DELAY(0x10218020, 0xAB990000);
    _IO_WRITE32_DELAY(0x10218020, 0xAD95F072);
    _IO_WRITE32_DELAY(0x10218020, 0xAD990000);
    _IO_WRITE32_DELAY(0x10218020, 0xBF950465);
    _IO_WRITE32_DELAY(0x10218020, 0xBF990000);
    _IO_WRITE32_DELAY(0x10218020, 0xA1954208);
    _IO_WRITE32_DELAY(0x10218020, 0xA1990000);
    _IO_WRITE32_DELAY(0x10218020, 0xA3953A08);
    _IO_WRITE32_DELAY(0x10218020, 0xA3990000);
    _IO_WRITE32_DELAY(0x10218020, 0xA5954007);
    _IO_WRITE32_DELAY(0x10218020, 0xA5990000);
    _IO_WRITE32_DELAY(0x10218020, 0xA995619F);
    _IO_WRITE32_DELAY(0x10218020, 0xA9990000);
    _IO_WRITE32_DELAY(0x10218020, 0xBF950462);
    _IO_WRITE32_DELAY(0x10218020, 0xBF990000);
    _IO_WRITE32_DELAY(0x10218020, 0xA7955318);
    _IO_WRITE32_DELAY(0x10218020, 0xA7990000);
    _IO_WRITE32_DELAY(0x10218020, 0xAB952A58);
    _IO_WRITE32_DELAY(0x10218020, 0xAB990000);
    _IO_WRITE32_DELAY(0x10218020, 0xA59597B3);
    _IO_WRITE32_DELAY(0x10218020, 0xA5990000);
    _IO_WRITE32_DELAY(0x10218020, 0xBF950261);
    _IO_WRITE32_DELAY(0x10218020, 0xBF990000);
    _IO_WRITE32_DELAY(0x10218020, 0xA1956000);
    _IO_WRITE32_DELAY(0x10218020, 0xA1990000);
    _IO_WRITE32_DELAY(0x10218020, 0xA1950000);
    _IO_WRITE32_DELAY(0x10218020, 0xA1990000);
    _IO_WRITE32_DELAY(0x10218020, 0xBF950000);
    _IO_WRITE32_DELAY(0x10218020, 0xBF990000);
    _IO_WRITE32_DELAY(0x10218020, 0xBD950000);
    _IO_WRITE32_DELAY(0x10218020, 0xBD990000);
    printf("fpga_init_8218b_ch4 done!\r\n");
}

void fpga_init_phy_force()
{
    // ---8851_PHY_INIT--------------------
    //Add FPGA_RESET 210329
    _IO_WRITE32_DELAY(0x1026a400, 0x00000ff0);
    _IO_WRITE32_DELAY(0x1026a400, 0x00000000);

    _IO_WRITE32_DELAY(0x10266100, 0x10000000);
    _IO_WRITE32_DELAY(0x1026609C, 0x0100000a);
    _IO_WRITE32_DELAY(0x10264100, 0x00010011);
    _IO_WRITE32_DELAY(0x10266100, 0x10000000);
    _IO_WRITE32_DELAY(0x10266108, 0x00000000);
    _IO_WRITE32_DELAY(0x10266110, 0x00000000);
    _IO_WRITE32_DELAY(0x10266118, 0x00000000);
    _IO_WRITE32_DELAY(0x10264004, 0x00000700);
    _IO_WRITE32_DELAY(0x1026450c, 0x00000700);
    _IO_WRITE32_DELAY(0x1026a04c, 0x60000007);
    _IO_WRITE32_DELAY(0x1026a02c, 0x0001481b);
    _IO_WRITE32_DELAY(0x1026a0f8, 0x01a01500);
    _IO_WRITE32_DELAY(0x10266000, 0x0c000c00);
    _IO_WRITE32_DELAY(0x10260000, 0x00001140);
    _IO_WRITE32_DELAY(0x10261000, 0x00001140);
    _IO_WRITE32_DELAY(0x10262000, 0x00001140);
    _IO_WRITE32_DELAY(0x10263000, 0x00001140);
    _IO_WRITE32_DELAY(0x10264018, 0x14141424);
    _IO_WRITE32_DELAY(0x10264000, 0x00010020);
    _IO_WRITE32_DELAY(0x1026002c, 0x00000050);
    _IO_WRITE32_DELAY(0x1026102c, 0x00000050);
    _IO_WRITE32_DELAY(0x1026202c, 0x00000050);
    _IO_WRITE32_DELAY(0x1026302c, 0x00000050);

    printf("fpga_init_phy_force done!\r\n");
}

void fpga_init_phy_qsgmii_ra_10M()
{
    //10M
    _IO_WRITE32_DELAY(0x10266100, 0x1000000F);
    _IO_WRITE32_DELAY(0x10266108, 0x1000000F);
    _IO_WRITE32_DELAY(0x10266110, 0x1000000F);
    _IO_WRITE32_DELAY(0x10266118, 0x1000000F);
    _IO_WRITE32_DELAY(0x10264004, 0x000007aa);
    _IO_WRITE32_DELAY(0x1026450c, 0x000007aa);

    //CLK
    _IO_WRITE32_DELAY(0x1026a400, 0xC0000000);

    //PHY2MAC
    _IO_WRITE32_DELAY(0x10264018, 0x0C0C0C14);
    printf("fpga_init_phy_qsgmii_ra_10M done!\r\n");
}

void fpga_init_phy_qsgmii_ra_100M()
{
    _IO_WRITE32_DELAY(0x10266100, 0x1000000C);
    _IO_WRITE32_DELAY(0x10266108, 0x1000000C);
    _IO_WRITE32_DELAY(0x10266110, 0x1000000C);
    _IO_WRITE32_DELAY(0x10266118, 0x1000000C);
    _IO_WRITE32_DELAY(0x10264004, 0x00000755);
    _IO_WRITE32_DELAY(0x1026450c, 0x00000755);

    //CLK
    _IO_WRITE32_DELAY(0x1026a400, 0x80000000);

    //PHY2MAC
    _IO_WRITE32_DELAY(0x10264018, 0x0C0C0C14);

    printf("fpga_init_phy_qsgmii_ra_100M done!\r\n");
}

void fpga_init_phy_qsgmii_ra_1000M()
{
    //1G
    _IO_WRITE32_DELAY(0x10266100, 0x10000000);
    _IO_WRITE32_DELAY(0x10266108, 0x00000000);
    _IO_WRITE32_DELAY(0x10266110, 0x00000000);
    _IO_WRITE32_DELAY(0x10266118, 0x00000000);
    _IO_WRITE32_DELAY(0x10264004, 0x00000700);
    _IO_WRITE32_DELAY(0x1026450c, 0x00000700);

    //CLK
    _IO_WRITE32_DELAY(0x1026a400, 0x00000000);

    //PHY2MAC
    _IO_WRITE32_DELAY(0x10264018, 0x14141424);

    printf("fpga_init_phy_qsgmii_ra_1000M done!\r\n");
}

void fpga_init_mac_sgmii_10M()
{
    //Set all port force 10M FC Disable
    _IO_WRITE32_DELAY(0x10213600, 0x1791C3);
    _IO_WRITE32_DELAY(0x10212800, 0x1791C3);
    _IO_WRITE32_DELAY(0x10212A00, 0x1791C3);
    _IO_WRITE32_DELAY(0x10212C00, 0x1791C3);
    _IO_WRITE32_DELAY(0x10212E00, 0x1791C3);
    _IO_WRITE32_DELAY(0x10213000, 0x1791C3);

    printf("fpga_init_mac_sgmii_10M done!\r\n");
}

void fpga_init_mac_sgmii_100M()
{
    //Set all port force 100M FC Disable
    _IO_WRITE32_DELAY(0x10213600, 0x1791C7);
    _IO_WRITE32_DELAY(0x10212800, 0x1791C7);
    _IO_WRITE32_DELAY(0x10212A00, 0x1791C7);
    _IO_WRITE32_DELAY(0x10212C00, 0x1791C7);
    _IO_WRITE32_DELAY(0x10212E00, 0x1791C7);
    _IO_WRITE32_DELAY(0x10213000, 0x1791C7);

    printf("fpga_init_mac_sgmii_100M done!\r\n");
}

void fpga_init_mac_sgmii_1000M()
{
    //Set all port force 1000M FC Disable
    _IO_WRITE32_DELAY(0x10213600, 0x1791CB);
    _IO_WRITE32_DELAY(0x10212800, 0x1791CB);
    _IO_WRITE32_DELAY(0x10212A00, 0x1791CB);
    _IO_WRITE32_DELAY(0x10212C00, 0x1791CB);
    _IO_WRITE32_DELAY(0x10212E00, 0x1791CB);
    _IO_WRITE32_DELAY(0x10213000, 0x1791CB);

    printf("fpga_init_mac_sgmii_1000M done!\r\n");
}

void fpga_init_mac_hsgmii_10M()
{
    //Set all port force 10M FC Disable
    _IO_WRITE32_DELAY(0x10213600, 0x1791C3);
    _IO_WRITE32_DELAY(0x10212800, 0x1791C3);
    _IO_WRITE32_DELAY(0x10212A00, 0x1791C3);
    _IO_WRITE32_DELAY(0x10212C00, 0x1791C3);
    _IO_WRITE32_DELAY(0x10212E00, 0x1791CF);
    _IO_WRITE32_DELAY(0x10213000, 0x1791CF);

    printf("fpga_init_mac_hsgmii_10M done!\r\n");
}

void fpga_init_mac_hsgmii_100M()
{
    //Set all port force 100M FC Disable
    _IO_WRITE32_DELAY(0x10213600, 0x1791C7);
    _IO_WRITE32_DELAY(0x10212800, 0x1791C7);
    _IO_WRITE32_DELAY(0x10212A00, 0x1791C7);
    _IO_WRITE32_DELAY(0x10212C00, 0x1791C7);
    _IO_WRITE32_DELAY(0x10212E00, 0x1791CF);
    _IO_WRITE32_DELAY(0x10213000, 0x1791CF);

    printf("fpga_init_mac_hsgmii_100M done!\r\n");
}

void fpga_init_mac_hsgmii_1000M()
{
    //Set all port force 1000M FC Disable
    _IO_WRITE32_DELAY(0x10213600, 0x1791CB);
    _IO_WRITE32_DELAY(0x10212800, 0x1791CB);
    _IO_WRITE32_DELAY(0x10212A00, 0x1791CB);
    _IO_WRITE32_DELAY(0x10212C00, 0x1791CB);
    _IO_WRITE32_DELAY(0x10212E00, 0x1791CF);
    _IO_WRITE32_DELAY(0x10213000, 0x1791CF);

    printf("fpga_init_mac_hsgmii_1000M done!\r\n");
}

void fpga_init_phy_verify()
{
    printf("fpga_init_phy_verify\r\n");
    printf("Reg[0x10260b04]=%08X\n", io_read32(0x10260b04));
    printf("Reg[0x10261b04]=%08X\n", io_read32(0x10261b04));
    printf("Reg[0x10262b04]=%08X\n", io_read32(0x10262b04));
    printf("Reg[0x10263b04]=%08X\n", io_read32(0x10263b04));
    printf("fpga_init_phy_verify done\r\n");
}

void fpga_init_phy_force_100M()
{
    _IO_WRITE32_DELAY(0x10218020, 0x92150000);
    _IO_WRITE32_DELAY(0x10218020, 0x88150101);
    _IO_WRITE32_DELAY(0x10218020, 0x80151200);
    _IO_WRITE32_DELAY(0x10218020, 0x92250000);
    _IO_WRITE32_DELAY(0x10218020, 0x88250101);
    _IO_WRITE32_DELAY(0x10218020, 0x80251200);
    _IO_WRITE32_DELAY(0x10218020, 0x92350000);
    _IO_WRITE32_DELAY(0x10218020, 0x88350101);
    _IO_WRITE32_DELAY(0x10218020, 0x80351200);
    _IO_WRITE32_DELAY(0x10218020, 0x92450000);
    _IO_WRITE32_DELAY(0x10218020, 0x88450101);
    _IO_WRITE32_DELAY(0x10218020, 0x80451200);
    _IO_WRITE32_DELAY(0x10218020, 0x92550000);
    _IO_WRITE32_DELAY(0x10218020, 0x88550101);
    _IO_WRITE32_DELAY(0x10218020, 0x80551200);
    _IO_WRITE32_DELAY(0x10218020, 0x92650000);
    _IO_WRITE32_DELAY(0x10218020, 0x88650101);
    _IO_WRITE32_DELAY(0x10218020, 0x80651200);
    _IO_WRITE32_DELAY(0x10218020, 0x92750000);
    _IO_WRITE32_DELAY(0x10218020, 0x88750101);
    _IO_WRITE32_DELAY(0x10218020, 0x80751200);
    _IO_WRITE32_DELAY(0x10218020, 0x92850000);
    _IO_WRITE32_DELAY(0x10218020, 0x88850101);
    _IO_WRITE32_DELAY(0x10218020, 0x80851200);
    _IO_WRITE32_DELAY(0x10218020, 0x92950000);
    _IO_WRITE32_DELAY(0x10218020, 0x88950101);
    _IO_WRITE32_DELAY(0x10218020, 0x80951200);
    _IO_WRITE32_DELAY(0x10218020, 0x92a50000);
    _IO_WRITE32_DELAY(0x10218020, 0x88a50101);
    _IO_WRITE32_DELAY(0x10218020, 0x80a51200);
    _IO_WRITE32_DELAY(0x10218020, 0x92b50000);
    _IO_WRITE32_DELAY(0x10218020, 0x88b50101);
    _IO_WRITE32_DELAY(0x10218020, 0x80b51200);
    _IO_WRITE32_DELAY(0x10218020, 0x92c50000);
    _IO_WRITE32_DELAY(0x10218020, 0x88c50101);
    _IO_WRITE32_DELAY(0x10218020, 0x80c51200);
    _IO_WRITE32_DELAY(0x10218020, 0x93150000);
    _IO_WRITE32_DELAY(0x10218020, 0x89150101);
    _IO_WRITE32_DELAY(0x10218020, 0x81151200);
    _IO_WRITE32_DELAY(0x10218020, 0x93250000);
    _IO_WRITE32_DELAY(0x10218020, 0x89250101);
    _IO_WRITE32_DELAY(0x10218020, 0x81251200);
    _IO_WRITE32_DELAY(0x10218020, 0x93350000);
    _IO_WRITE32_DELAY(0x10218020, 0x89350101);
    _IO_WRITE32_DELAY(0x10218020, 0x81351200);
    _IO_WRITE32_DELAY(0x10218020, 0x93450000);
    _IO_WRITE32_DELAY(0x10218020, 0x89450101);
    _IO_WRITE32_DELAY(0x10218020, 0x81451200);
    _IO_WRITE32_DELAY(0x10218020, 0x93950000);
    _IO_WRITE32_DELAY(0x10218020, 0x89950101);
    _IO_WRITE32_DELAY(0x10218020, 0x81951200);
    _IO_WRITE32_DELAY(0x10218020, 0x93a50000);
    _IO_WRITE32_DELAY(0x10218020, 0x89a50101);
    _IO_WRITE32_DELAY(0x10218020, 0x81a51200);
    _IO_WRITE32_DELAY(0x10218020, 0x93b50000);
    _IO_WRITE32_DELAY(0x10218020, 0x89b50101);
    _IO_WRITE32_DELAY(0x10218020, 0x81b51200);
    _IO_WRITE32_DELAY(0x10218020, 0x93c50000);
    _IO_WRITE32_DELAY(0x10218020, 0x89c50101);
    _IO_WRITE32_DELAY(0x10218020, 0x81c51200);
    printf("fpga_init_phy_force_100M done!\r\n");
}

void fpga_init_phy_force_10M()
{
    _IO_WRITE32_DELAY(0x10218020, 0x92150000);
    _IO_WRITE32_DELAY(0x10218020, 0x88150041);
    _IO_WRITE32_DELAY(0x10218020, 0x80151200);
    _IO_WRITE32_DELAY(0x10218020, 0x92250000);
    _IO_WRITE32_DELAY(0x10218020, 0x88250041);
    _IO_WRITE32_DELAY(0x10218020, 0x80251200);
    _IO_WRITE32_DELAY(0x10218020, 0x92350000);
    _IO_WRITE32_DELAY(0x10218020, 0x88350041);
    _IO_WRITE32_DELAY(0x10218020, 0x80351200);
    _IO_WRITE32_DELAY(0x10218020, 0x92450000);
    _IO_WRITE32_DELAY(0x10218020, 0x88450041);
    _IO_WRITE32_DELAY(0x10218020, 0x80451200);
    _IO_WRITE32_DELAY(0x10218020, 0x92550000);
    _IO_WRITE32_DELAY(0x10218020, 0x88550041);
    _IO_WRITE32_DELAY(0x10218020, 0x80551200);
    _IO_WRITE32_DELAY(0x10218020, 0x92650000);
    _IO_WRITE32_DELAY(0x10218020, 0x88650041);
    _IO_WRITE32_DELAY(0x10218020, 0x80651200);
    _IO_WRITE32_DELAY(0x10218020, 0x92750000);
    _IO_WRITE32_DELAY(0x10218020, 0x88750041);
    _IO_WRITE32_DELAY(0x10218020, 0x80751200);
    _IO_WRITE32_DELAY(0x10218020, 0x92850000);
    _IO_WRITE32_DELAY(0x10218020, 0x88850041);
    _IO_WRITE32_DELAY(0x10218020, 0x80851200);
    _IO_WRITE32_DELAY(0x10218020, 0x92950000);
    _IO_WRITE32_DELAY(0x10218020, 0x88950041);
    _IO_WRITE32_DELAY(0x10218020, 0x80951200);
    _IO_WRITE32_DELAY(0x10218020, 0x92a50000);
    _IO_WRITE32_DELAY(0x10218020, 0x88a50041);
    _IO_WRITE32_DELAY(0x10218020, 0x80a51200);
    _IO_WRITE32_DELAY(0x10218020, 0x92b50000);
    _IO_WRITE32_DELAY(0x10218020, 0x88b50041);
    _IO_WRITE32_DELAY(0x10218020, 0x80b51200);
    _IO_WRITE32_DELAY(0x10218020, 0x92c50000);
    _IO_WRITE32_DELAY(0x10218020, 0x88c50041);
    _IO_WRITE32_DELAY(0x10218020, 0x80c51200);
    _IO_WRITE32_DELAY(0x10218020, 0x93150000);
    _IO_WRITE32_DELAY(0x10218020, 0x89150041);
    _IO_WRITE32_DELAY(0x10218020, 0x81151200);
    _IO_WRITE32_DELAY(0x10218020, 0x93250000);
    _IO_WRITE32_DELAY(0x10218020, 0x89250041);
    _IO_WRITE32_DELAY(0x10218020, 0x81251200);
    _IO_WRITE32_DELAY(0x10218020, 0x93350000);
    _IO_WRITE32_DELAY(0x10218020, 0x89350041);
    _IO_WRITE32_DELAY(0x10218020, 0x81351200);
    _IO_WRITE32_DELAY(0x10218020, 0x93450000);
    _IO_WRITE32_DELAY(0x10218020, 0x89450041);
    _IO_WRITE32_DELAY(0x10218020, 0x81451200);
    _IO_WRITE32_DELAY(0x10218020, 0x93950000);
    _IO_WRITE32_DELAY(0x10218020, 0x89950041);
    _IO_WRITE32_DELAY(0x10218020, 0x81951200);
    _IO_WRITE32_DELAY(0x10218020, 0x93a50000);
    _IO_WRITE32_DELAY(0x10218020, 0x89a50041);
    _IO_WRITE32_DELAY(0x10218020, 0x81a51200);
    _IO_WRITE32_DELAY(0x10218020, 0x93b50000);
    _IO_WRITE32_DELAY(0x10218020, 0x89b50041);
    _IO_WRITE32_DELAY(0x10218020, 0x81b51200);
    _IO_WRITE32_DELAY(0x10218020, 0x93c50000);
    _IO_WRITE32_DELAY(0x10218020, 0x89c50041);
    _IO_WRITE32_DELAY(0x10218020, 0x81c51200);
    printf("fpga_init_phy_force_10M done!\r\n");
}

void fpga_init_ch_init_100M()
{
    _IO_WRITE32_DELAY(0x10000080, 0x800001ff);
    _IO_WRITE32_DELAY(0x10000090, 0x800001ff);
    _IO_WRITE32_DELAY(0x100000d0, 0x800001ff);
    _IO_WRITE32_DELAY(0x100000d4, 0x800001ff);
    _IO_WRITE32_DELAY(0x100000d8, 0x800001ff);
    _IO_WRITE32_DELAY(0x100000dc, 0x800001ff);
    _IO_WRITE32_DELAY(0x100000e0, 0x800001ff);
    printf("fpga_init_ch_init_100M done!\r\n");
}

void fpga_init_ch_init_10M()
{
    _IO_WRITE32_DELAY(0x10000080, 0x80000155);
    _IO_WRITE32_DELAY(0x10000090, 0x80000155);
    _IO_WRITE32_DELAY(0x100000d0, 0x80000155);
    _IO_WRITE32_DELAY(0x100000d4, 0x80000155);
    _IO_WRITE32_DELAY(0x100000d8, 0x80000155);
    _IO_WRITE32_DELAY(0x100000dc, 0x80000155);
    _IO_WRITE32_DELAY(0x100000e0, 0x80000155);
    printf("fpga_init_ch_init_10M done!\r\n");
}

void fpga_init_hsgmii_phy_init()
{
    //MAC---------------------------------
    _IO_WRITE32_DELAY(0x10213e8c, 0x0000000f);
    //P0---------------------------------
    _IO_WRITE32_DELAY(0x1022a02c, 0x00014817);
    _IO_WRITE32_DELAY(0x10220000, 0x00000140);
    //P1---------------------------------
    _IO_WRITE32_DELAY(0x1023a02c, 0x00014817);
    _IO_WRITE32_DELAY(0x10230000, 0x00000140);
    //P2---------------------------------
    _IO_WRITE32_DELAY(0x1024a02c, 0x00014817);
    _IO_WRITE32_DELAY(0x10240000, 0x00000140);
    //P3---------------------------------
    _IO_WRITE32_DELAY(0x1025a02c, 0x00014817);
    _IO_WRITE32_DELAY(0x10250000, 0x00000140);
    //P4---------------------------------
    _IO_WRITE32_DELAY(0x1026a02c, 0x00014817);
    _IO_WRITE32_DELAY(0x10260000, 0x00000140);
    //
    printf("fpga_init_hsgmii_phy_init done!\r\n");
}

void fpga_init_hsgmii_phy_init_C0()
{
    //P0---------------------------------
    _IO_WRITE32_DELAY(0x1022a02c, 0x00014817);
    _IO_WRITE32_DELAY(0x10220000, 0x00000140);
    //P1---------------------------------
    _IO_WRITE32_DELAY(0x1023a02c, 0x00014817);
    _IO_WRITE32_DELAY(0x10230000, 0x00000140);

    printf("fpga_init_hsgmii_phy_init_C0 done!\r\n");
}

void fpga_init_phy_sgmii_ra_1000M()
{
    _IO_WRITE32_DELAY(0x1022a400, 0xff0);
    _IO_WRITE32_DELAY(0x1022a400, 0x0);
    _IO_WRITE32_DELAY(0x1023a400, 0xff0);
    _IO_WRITE32_DELAY(0x1023a400, 0x0);
    //P0---------------------------------
    _IO_WRITE32_DELAY(0x1022a02c, 0x00014813);
    _IO_WRITE32_DELAY(0x10220000, 0x00001140);
    //P1---------------------------------
    _IO_WRITE32_DELAY(0x1023a02c, 0x00014813);
    _IO_WRITE32_DELAY(0x10230000, 0x00001140);
    //P2---------------------------------
    _IO_WRITE32_DELAY(0x1024a400, 0xff0);
    _IO_WRITE32_DELAY(0x1024a400, 0x0);
    //P3---------------------------------
    _IO_WRITE32_DELAY(0x1025a400, 0xff0);
    _IO_WRITE32_DELAY(0x1025a400, 0x0);
    //P4---------------------------------
    _IO_WRITE32_DELAY(0x1026a400, 0xff0);
    _IO_WRITE32_DELAY(0x1026a400, 0x0);

    printf("fpga_init_phy_sgmii_ra_1000M done!\r\n");
}

void fpga_init_phy_sgmii_ra_1000M_C()
{
    _IO_WRITE32_DELAY(0x1022a400, 0xff0);
    _IO_WRITE32_DELAY(0x1022a400, 0x0);
    _IO_WRITE32_DELAY(0x1023a400, 0xff0);
    _IO_WRITE32_DELAY(0x1023a400, 0x0);
    //P0---------------------------------
    _IO_WRITE32_DELAY(0x1022a02c, 0x00014813);
    _IO_WRITE32_DELAY(0x10220000, 0x00001140);
    //P1---------------------------------
    _IO_WRITE32_DELAY(0x1023a02c, 0x00014813);
    _IO_WRITE32_DELAY(0x10230000, 0x00001140);

    printf("fpga_init_phy_sgmii_ra_1000M_C done!\r\n");
}

void fpga_init_phy_sgmii_ra_100M()
{
    //SPEED
    _IO_WRITE32_DELAY(0x10224018, 0x14);
    _IO_WRITE32_DELAY(0x10234018, 0x14);
    //====
    _IO_WRITE32_DELAY(0x1022a400, 0x80000000);
    _IO_WRITE32_DELAY(0x10226100, 0x1000000C);
    _IO_WRITE32_DELAY(0x1022a140, 0x10);
    _IO_WRITE32_DELAY(0x10224004, 0x1);
    _IO_WRITE32_DELAY(0x1022450c, 0x1);
    _IO_WRITE32_DELAY(0x1022a400, 0x80000ff0);
    _IO_WRITE32_DELAY(0x1022a400, 0x80000000);
    //P1
    _IO_WRITE32_DELAY(0x1023a400, 0x80000000);
    _IO_WRITE32_DELAY(0x10236100, 0x1000000C);
    _IO_WRITE32_DELAY(0x1023a140, 0x10);
    _IO_WRITE32_DELAY(0x10234004, 0x1);
    _IO_WRITE32_DELAY(0x1023450c, 0x1);
    _IO_WRITE32_DELAY(0x1023a400, 0x80000ff0);
    _IO_WRITE32_DELAY(0x1023a400, 0x80000000);
    //MAC---------------------------------
    //P0---------------------------------
    _IO_WRITE32_DELAY(0x1022a02c, 0x00014813);
    _IO_WRITE32_DELAY(0x10220000, 0x00001140);
    //P1---------------------------------
    _IO_WRITE32_DELAY(0x1023a02c, 0x00014813);
    _IO_WRITE32_DELAY(0x10230000, 0x00001140);

    printf("fpga_init_phy_sgmii_ra_100M done!\r\n");
}

void fpga_init_phy_sgmii_ra_10M()
{
    //SPEED
    _IO_WRITE32_DELAY(0x10224018, 0x4);
    _IO_WRITE32_DELAY(0x10234018, 0x4);
    //====
    _IO_WRITE32_DELAY(0x1022a400, 0xC0000000);
    _IO_WRITE32_DELAY(0x10226100, 0x1000000F);
    _IO_WRITE32_DELAY(0x1022a140, 0x20);
    _IO_WRITE32_DELAY(0x10224004, 0x1);
    _IO_WRITE32_DELAY(0x1022450c, 0x1);
    _IO_WRITE32_DELAY(0x1022a400, 0xC0000ff0);
    _IO_WRITE32_DELAY(0x1022a400, 0xC0000000);
    //P1
    _IO_WRITE32_DELAY(0x1023a400, 0xC0000000);
    _IO_WRITE32_DELAY(0x10236100, 0x1000000F);
    _IO_WRITE32_DELAY(0x1023a140, 0x20);
    _IO_WRITE32_DELAY(0x10234004, 0x1);
    _IO_WRITE32_DELAY(0x1023450c, 0x1);
    _IO_WRITE32_DELAY(0x1023a400, 0xC0000ff0);
    _IO_WRITE32_DELAY(0x1023a400, 0xC0000000);
    //MAC---------------------------------
    //P0---------------------------------
    _IO_WRITE32_DELAY(0x1022a02c, 0x00014813);
    _IO_WRITE32_DELAY(0x10220000, 0x00001140);

    //P1---------------------------------
    _IO_WRITE32_DELAY(0x1023a02c, 0x00014813);
    _IO_WRITE32_DELAY(0x10230000, 0x00001140);

    printf("fpga_init_phy_sgmii_ra_10M done!\r\n");
}

void fpga_init_mac()
{
    _IO_WRITE32_DELAY(0x10210000, 0x0017980b);
    _IO_WRITE32_DELAY(0x10210200, 0x0017980b);
    _IO_WRITE32_DELAY(0x10210400, 0x0017980b);
    _IO_WRITE32_DELAY(0x10210600, 0x0017980b);
    _IO_WRITE32_DELAY(0x10210800, 0x0017980b);
    _IO_WRITE32_DELAY(0x10210a00, 0x0017980b);
    _IO_WRITE32_DELAY(0x10210c00, 0x0017980b);
    _IO_WRITE32_DELAY(0x10210e00, 0x0017980b);
    _IO_WRITE32_DELAY(0x10211000, 0x0017980b);
    _IO_WRITE32_DELAY(0x10211200, 0x0017980b);
    _IO_WRITE32_DELAY(0x10211400, 0x0017980b);
    _IO_WRITE32_DELAY(0x10211600, 0x0017980b);
    _IO_WRITE32_DELAY(0x10211800, 0x0017980b);
    _IO_WRITE32_DELAY(0x10211a00, 0x0017980b);
    _IO_WRITE32_DELAY(0x10211c00, 0x0017980b);
    _IO_WRITE32_DELAY(0x10211e00, 0x0017980b);
    _IO_WRITE32_DELAY(0x10212000, 0x0017980b);
    _IO_WRITE32_DELAY(0x10212200, 0x0017980b);
    _IO_WRITE32_DELAY(0x10212400, 0x0017980b);
    _IO_WRITE32_DELAY(0x10212600, 0x0017980b);
    _IO_WRITE32_DELAY(0x10212800, 0x0017980b);
    _IO_WRITE32_DELAY(0x10212a00, 0x0017980b);
    _IO_WRITE32_DELAY(0x10212c00, 0x0017980b);
    _IO_WRITE32_DELAY(0x10212e00, 0x0017980b);
    _IO_WRITE32_DELAY(0x10213000, 0x0017980b);
    _IO_WRITE32_DELAY(0x10213200, 0x0017980b);
    _IO_WRITE32_DELAY(0x10213400, 0x0017980b);
    _IO_WRITE32_DELAY(0x10213600, 0x0017980b);
    _IO_WRITE32_DELAY(0x10213800, 0x0017980A);
    printf("fpga_init_mac done!\r\n");
}

void fpga_init_p28()
{
    /* Init MAC */
    _IO_WRITE32_DELAY(0x10213800, 0x0017980b);

    /* Set Unknown ucst don't flooding to P28 */
    _IO_WRITE32_DELAY(0x102000B4, 0x0FFFFFFF);
    /* Set Unknown mcst don't flooding to P28 */
    _IO_WRITE32_DELAY(0x102000B8, 0x0FFFFFFF);
    /* Set Unknown bcst don't flooding to P28 */
    _IO_WRITE32_DELAY(0x102000BC, 0x0FFFFFFF);

    /* Specific frame to CPU */
    /* IGMP report & query */
    _IO_WRITE32_DELAY(0x1020001C, 0x40854085);
    /* ARP & PPPoE */
    _IO_WRITE32_DELAY(0x10200020, 0x40854085);
    /* PAE */
    _IO_WRITE32_DELAY(0x10200024, 0x40854085);
    /* DHCP v4 & v6 */
    _IO_WRITE32_DELAY(0x102000A4, 0x00850085);
    /* BPDU */
    _IO_WRITE32_DELAY(0x102000D0, 0x00805085);
    /* MLD report & query */
    _IO_WRITE32_DELAY(0x102000D4, 0x40854085);

    /* Set Egress Rate limit of P28 to 512kbps */
    _IO_WRITE32_DELAY(0x1020F840, 0x80580010);

    printf("fpga_init_p28 done!\r\n");
}

void fpga_init_mac_b()
{
    _IO_WRITE32_DELAY(0x10210000, 0x0017980b);
    _IO_WRITE32_DELAY(0x10210200, 0x0017980b);
    _IO_WRITE32_DELAY(0x10210400, 0x0017980b);
    _IO_WRITE32_DELAY(0x10210600, 0x0017980b);
    _IO_WRITE32_DELAY(0x10210800, 0x0017980b);
    _IO_WRITE32_DELAY(0x10210a00, 0x0017980b);
    _IO_WRITE32_DELAY(0x10210c00, 0x0017980b);
    _IO_WRITE32_DELAY(0x10210e00, 0x0017980b);

    _IO_WRITE32_DELAY(0x1022a400, 0x00000ff0);
    _IO_WRITE32_DELAY(0x1022a400, 0x0000000);

    _IO_WRITE32_DELAY(0x1023a400, 0x00000ff0);
    _IO_WRITE32_DELAY(0x1023a400, 0x0000000);

    _IO_WRITE32_DELAY(0x1024a400, 0x00000ff0);
    _IO_WRITE32_DELAY(0x1024a400, 0x0000000);

    _IO_WRITE32_DELAY(0x1025a400, 0x00000ff0);
    _IO_WRITE32_DELAY(0x1025a400, 0x0000000);

    _IO_WRITE32_DELAY(0x1026a400, 0x00000ff0);
    _IO_WRITE32_DELAY(0x1026a400, 0x0000000);
    printf("fpga_init_mac_b done!\r\n");
}
static portBASE_TYPE fpga_init_cmd(signed char *buf, size_t len, const signed char * input)
{
    char cmd[32];
    char str[32];
    char para[32] = {0};
    unsigned char is_10M = 0;
    unsigned char is_100M = 0;
    unsigned char is_1000M = 0;

    sscanf(input, "%s %s %s", cmd, str, para);
    if (!strncmp(para, "10M", strlen("10M")))
    {
        is_10M = 1;
    }
    else if (!strncmp(para, "100M", strlen("100M")))
    {
        is_100M = 1;
    }
    else
    {
        is_1000M = 1;
    }

    if (!strncmp(str, "A", 1))
    {
        fpga_init_8218b_ch0_1();
        fpga_init_8218b_ch2();
        fpga_init_8218b_ch3();
        fpga_init_8218b_ch4();
        if (is_10M)
        {
            fpga_init_phy_force_10M();
        }
        else
        {
            fpga_init_phy_force_100M();
        }

        /* switch to con7 */
        io_write32(0x10000080, 0x80010000);

        fpga_init_8218b_ch0_1();
        fpga_init_8218b_ch2();
        fpga_init_8218b_ch3();
        fpga_init_8218b_ch4();
        if (is_10M)
        {
            fpga_init_phy_force_10M();
        }
        else
        {
            fpga_init_phy_force_100M();
        }

        if (is_10M)
        {
            fpga_init_ch_init_10M();
        }
        else
        {
            fpga_init_ch_init_100M();
        }

        fpga_init_mac();
        fpga_init_p28();
        fpga_init_phy_verify();
    }
    else if (!strncmp(str, "B", 1))
    {
        fpga_init_8218b_ch0_1();
        if (is_10M)
        {
            fpga_init_phy_force_10M();
        }
        else
        {
            fpga_init_phy_force_100M();
        }

        if (is_10M)
        {
            fpga_init_ch_init_10M();
        }
        else
        {
            fpga_init_ch_init_100M();
        }

        fpga_init_mac_b();
        fpga_init_hsgmii_phy_init();
        fpga_init_phy_sgmii_ra_1000M();
        fpga_init_p28();
        fpga_init_phy_verify();
    }
    else if (!strncmp(str, "C0", 2))
    {
        fpga_init_8218b_ch2();
        fpga_init_phy_force();
        fpga_init_hsgmii_phy_init_C0();
        if (is_10M)
        {
            fpga_init_phy_qsgmii_ra_10M();
            fpga_init_mac_hsgmii_10M();
        }
        else if (is_100M)
        {
            fpga_init_phy_qsgmii_ra_100M();
            fpga_init_mac_hsgmii_100M();
        }
        else
        {
            fpga_init_phy_qsgmii_ra_1000M();
            fpga_init_mac_hsgmii_1000M();
        }
        fpga_init_p28();
        fpga_init_phy_verify();
    }
    else if (!strncmp(str, "C1", 2))
    {
        fpga_init_8218b_ch2();
        fpga_init_phy_force();
        if (is_10M)
        {
            fpga_init_phy_qsgmii_ra_10M();
            fpga_init_phy_sgmii_ra_10M();
            fpga_init_mac_sgmii_10M();
        }
        else if (is_100M)
        {
            fpga_init_phy_qsgmii_ra_100M();
            fpga_init_phy_sgmii_ra_100M();
            fpga_init_mac_sgmii_100M();
        }
        else
        {
            fpga_init_phy_qsgmii_ra_1000M();
            fpga_init_phy_sgmii_ra_1000M_C();
            fpga_init_mac_sgmii_1000M();
        }
        fpga_init_p28();
        fpga_init_phy_verify();
    }

    return pdFALSE;
}
#endif // IS_FPGA_STAGE

static portBASE_TYPE proc_cmd(signed char *buf, size_t len, const signed char * input)
{
    char cmd[32];
    char str[32];
    unsigned int addr = 0;
    unsigned int val = 0;
    int ret;

    ret = sscanf(input, "%s %x %x %s", cmd, &addr, &val, str) ;
    if (ret == 2)
    {
        ret = sscanf(input, "%s %x %s", cmd, &addr, str);
        if (ret == 3)
        {
            //printf("cmd %s, proc %s, addr 0x%x\n", cmd, str, addr);
        }
        if(isDmemAddr(addr) || isRegAddr(addr))
        {
            val = io_read32(addr);
        }
        else
        {
            errAddrInfo(addr);
            return pdFALSE;
        }
        printf("reg %x: 0x%x\r\n", addr, val);
    }
    else if (ret == 4)
    {
        //printf("cmd %s, proc %s, addr 0x%x, val 0x%x\n", cmd, str, addr, val);
        if(isDmemAddr(addr) || isRegAddr(addr))
        {
            io_write32(addr, val);
        }
        else
        {
            errAddrInfo(addr);
            return pdFALSE;
        }
        printf("reg %x: 0x%x\r\n", addr, val);
    }

    return pdFALSE;
}

static portBASE_TYPE memory_read_cmd(signed char *buf, size_t len, const signed char * input)
{
    char str[32];
    unsigned int addr;

    sscanf(input, "%s %x", str, &addr) ;

    doSysMemrl(addr);

    return pdFALSE;
}

static portBASE_TYPE memory_write_cmd(signed char *buf, size_t len, const signed char * input)
{
    char str[32];
    unsigned int addr, val;

    sscanf(input, "%s %x %x", str, &addr, &val) ;

    doSysMemwl(addr, val);

    return pdFALSE;
}

int doPbusMemrl(unsigned int addr)
{
    unsigned int value;

    value = an8801sb_phy_readBuckPbus(0x19, addr);

    printf("\r\n<Address>\t<Value>\r\n");
    printf("0x%08lx\t0x%08lx\r\n", addr, value);

    return 0;
}

int doPbusMemwl(unsigned int addr, unsigned int value)
{
    an8801sb_phy_writeBuckPbus(0x19, addr, value);
    
    return 0;
}

static portBASE_TYPE pbus_read_cmd(signed char *buf, size_t len, const signed char * input)
{
    char str[32];
    unsigned int addr;

    sscanf(input, "%s %x", str, &addr) ;

    doPbusMemrl(addr);

    return pdFALSE;
}

static portBASE_TYPE pbus_write_cmd(signed char *buf, size_t len, const signed char * input)
{
    char str[32];
    unsigned int addr, val;

    sscanf(input, "%s %x %x", str, &addr, &val) ;

    doPbusMemwl(addr, val);

    return pdFALSE;
}

static portBASE_TYPE tkr_read_cmd(signed char *buf, size_t len, const signed char * input)
{
    char str[32];
    unsigned int phyaddr = 0, reg11 = 0, reg12 = 0, reg10 = 0;

    sscanf(input, "%s %x %x", str, &phyaddr, &reg10) ;

    cl22_write(phyaddr, 2, 0x11, 0);
    cl22_write(phyaddr, 2, 0x12, 0);
    cl22_write(phyaddr, 2, 0x10, reg10);
    delay1ms(5);
    reg11 = ((uint32_t) cl22_read(phyaddr, 2, 0x11));
    reg12 = ((uint32_t) cl22_read(phyaddr, 2, 0x12));

    printf("0x%x 0x%x\n", reg11, reg12);

    return pdFALSE;
}

static portBASE_TYPE tkr_write_cmd(signed char *buf, size_t len, const signed char * input)
{
    char str[32];
    unsigned int phyaddr = 0, reg11 = 0, reg12 = 0, reg10 = 0;

    sscanf(input, "%s %x %x %x %x", str, &phyaddr, &reg11, &reg12, &reg10) ;

    cl22_write(phyaddr, 2, 0x11, reg11);
    cl22_write(phyaddr, 2, 0x12, reg12);
    cl22_write(phyaddr, 2, 0x10, reg10);

    return pdFALSE;
}

static portBASE_TYPE memory_dump_cmd(signed char *buf, size_t len, const signed char * input)
{
    char str[32];
    unsigned int addr, length;

    sscanf(input, "%s %x %x", str, &addr, &length) ;

    doSysMemory(addr, length);

    return pdFALSE;
}

static portBASE_TYPE cpu_reg_dump_cmd(signed char *buf, size_t len, const signed char * input)
{
    cpu_reg_dump();

    return pdFALSE;
}

static portBASE_TYPE kill_wdog_cmd(signed char *buf, size_t len, const signed char * input)
{
    wdog_kill();

    return pdFALSE;
}

static portBASE_TYPE show_interrupts_cmd(signed char *buf, size_t len, const signed char * input)
{
    show_interrupts();

    return pdFALSE;
}

static portBASE_TYPE pdma_init_cmd(signed char *buf, size_t len, const signed char * input)
{
    pdma_init();

    return pdFALSE;
}

static portBASE_TYPE diag_shell_cmd(signed char *buf, size_t len, const signed char * input)
{
    g_privilege_mode = CMD_PRIVILEGE_MODE_SDK;
    return pdFALSE;
}

static portBASE_TYPE peripheral_shell_cmd(signed char *buf, size_t len, const signed char * input)
{
    g_privilege_mode = CMD_PRIVILEGE_MODE_PERIPHERAL;
    return pdFALSE;
}
#ifdef AIR_MW_SUPPORT
static portBASE_TYPE mw_shell_cmd(signed char *buf, size_t len, const signed char * input)
{
    g_privilege_mode = CMD_PRIVILEGE_MODE_MW;
    return pdFALSE;
}
#endif

static portBASE_TYPE debug_shell_cmd(signed char *buf, size_t len, const signed char * input)
{
    g_privilege_mode = CMD_PRIVILEGE_MODE_DEBUG;
    return pdFALSE;
}

static portBASE_TYPE reset_cmd(signed char *buf, size_t len, const signed char * input)
{
    air_chipscu_resetSystem(0);
    //io_write32(0x10005010, 0x0);
    //io_write32(0x100050C0, 0x80000000);
    return pdFALSE;
}

/*SDK session end*/

static portBASE_TYPE pdma_debug_cmd(signed char *buf, size_t len, const signed char * input)
{
    pdma_debug(input);

    return pdFALSE;
}

#ifndef SRAM_SHRINK___XMODEM_CMD_REMOVE
static portBASE_TYPE xmodem_rcv_cmd(signed char *buf, size_t len, const signed char * input)
{
    char str[32];
    unsigned int addr = ~0UL;
    int recvSize = 0;

    sscanf(input, "%s %x", str, &addr);

    //disable UART isr
    io_write32(0x1000a004, 0);

    recvSize = XModemReceive(addr);

    //enable UART isr
    io_write32(0x1000a004, 1);

    printf("\nReceived %d bytes!\n", recvSize);

    return pdFALSE;
}
#endif

static portBASE_TYPE tftp_get_cmd(signed char *buf, size_t len, const signed char * input)
{
    char *str = NULL;
    char *ip = NULL;
    char *name = NULL;
    char *tmp = NULL;
    unsigned int addr = ~0UL;
    int recvSize = 0;

    str = strtok(input, " ");
    ip = strtok(NULL, " ");
    name = strtok(NULL, " ");
    tmp = strtok(NULL, " ");

    if ((ip != NULL) && (name != NULL))
    {
        if (tmp != NULL)
            sscanf(tmp, "%x", &addr);

        recvSize = tftp_get_file(ip, name, addr);

        printf("\nReceived %d bytes!\n", recvSize);
    }
    else
    {
        printf("Illegal argument!\r\n");
    }

    return pdFALSE;
}

static portBASE_TYPE upgrade_cmd(signed char *buf, size_t len, const signed char * input)
{
    char *str = NULL;
    char *type = NULL;
    char *mode = NULL;
    char *ip = NULL;
    char *name = NULL;
    unsigned int addr = ~0UL;
    int recvSize = 0;

    str = strtok(input, " ");
    type = strtok(NULL, " ");
    mode = strtok(NULL, " ");

    if ((type != NULL) && (mode != NULL))
    {
        if (strncmp(type, "firmware", 8) == 0)
        {
#ifndef SRAM_SHRINK___XMODEM_CMD_REMOVE
            if (strncmp(mode, "xmdm", 4) == 0)
            {
                //disable UART isr
                io_write32(0x1000a004, 0);

                recvSize = XModemReceive(addr);

                //enable UART isr
                io_write32(0x1000a004, 1);
            }
            else
#endif
            if (strncmp(mode, "tftp", 4) == 0)
            {
                ip = strtok(NULL, " ");
                name = strtok(NULL, " ");

                if ((ip != NULL) && (name != NULL))
                {
                    recvSize = tftp_get_file(ip, name, addr);
                }
                else
                {
                    printf("Illegal argument!\r\n");
                    return pdFALSE;
                }
            }
            else
            {
                printf("Illegal mode!\r\n");
                return pdFALSE;
            }

            if (recvSize <= 0)
            {
                printf("\nReceiving Failed(%d)!\n", recvSize);
                return pdFALSE;
            }
            
            printf("\nReceived %d bytes!\n", recvSize);

            /* Chechk image CRC and prepare fw upgrade */
            if (crc_check((unsigned char *) TempSystemBase) == 0)
            {
                update_upgrade_flag(1);
            }
            else
            {
                printf("FW CRC32 is mismatch\r\n");
            }
        }
        else
        {
            printf("Illegal type!\r\n");
        }
    }
    else
    {
        printf("Illegal argument!\r\n");
    }

    return pdFALSE;
}
#if 0
extern void HttpTLS_TestTask( void *pvParameters );
static portBASE_TYPE httpclient_start_cmd(signed char *buf, size_t len, const signed char * input)
{
    xTaskCreate(HttpTLS_TestTask,            /* The function that implements the task. */
                "WEBS",                          /* The text name assigned to the task - for debug only as it is not used by the kernel. */
                2048,       /* The size of the stack to allocate to the task. */
                NULL,                           /* The parameter passed to the task - not used in this simple case. */
                (tskIDLE_PRIORITY + 3),         /* The priority assigned to the task. */
                NULL );                         /* The task handle is not required, so NULL is passed. */
    //http_tls_main(0, NULL);
    return pdFALSE;
}
#endif
#ifdef AIR_WEBSOCKET_DEMO
extern void HttpWS_TestTask( void *pvParameters );
static portBASE_TYPE httpclient_ws_cmd(signed char *buf, size_t len, const signed char * input)
{
    char str[32] = { 0 }, addr[50] = { 0 };

    sscanf((char*)input, "%s %s", str, addr);

    xTaskCreate(HttpWS_TestTask,
            "WS",
            2048,
            addr,
            (tskIDLE_PRIORITY + 5),
            NULL );

    return pdFALSE;
}
#endif

static portBASE_TYPE tftp_fw_upgrade_cmd(signed char *buf, size_t len, const signed char * input)
{
    char *str = NULL;
    char *ip = NULL;
    char *name = NULL;
    unsigned int addr = ~0UL;
    int recvSize = 0;

    str = strtok(input, " ");
    ip = strtok(NULL, " ");
    name = strtok(NULL, " ");

    if ((ip != NULL) && (name != NULL))
    {
        recvSize = tftp_get_file(ip, name, addr);
        if (recvSize <= 0)
        {
            printf("\nReceiving Failed(%d)!\n", recvSize);
            return pdFALSE;
        }
        
        printf("\nReceived %d bytes!\n", recvSize);

        /* Chechk image CRC and prepare fw upgrade */
        if (crc_check((unsigned char *) TempSystemBase) == 0)
        {
            update_upgrade_flag(1);
        }
        else
        {
            printf("FW CRC32 is mismatch\r\n");
        }
    }
    else
    {
        printf("Illegal argument!\r\n");
    }

    return pdFALSE;
}

static portBASE_TYPE lwip_stats_cmd(signed char *buf, size_t len, const signed char * input)
{
    stats_display();

    printf("[MemTrace]: xMinimumEverFreeBytesRemaining %d\n", xPortGetMinimumEverFreeHeapSize());
    printf("[MemTrace]: xFreeBytesRemaining    %d\n", xPortGetFreeHeapSize());

    return pdFALSE;
}
#if 0
extern void DropBear_MainTask( void *pvParameters );
static int sshdIsRun = 0;
static portBASE_TYPE sshd_cmd(signed char *buf, size_t len, const signed char * input)
{
    if(!sshdIsRun)
    {
        sshdIsRun = 1;
        xTaskCreate( DropBear_MainTask,
                "sshd",
                configMINIMAL_STACK_SIZE,
                NULL,
                configMAX_PRIORITIES - 2,
                NULL );
        strcpy( buf, "sshd running..." );
    }
    else
    {
        strcpy( buf, "sshd is running..." );
    }

    return pdFALSE;
}
#endif
static portBASE_TYPE ip_get_cmd(signed char *buf, size_t len, const signed char * input)
{
    char   ip_str[IPV4_STR_SIZE];
#ifdef AIR_SUPPORT_IPV6
    char ipv6_state[IPV6_STATE_SIZE];
    u8_t i = 0, addr_state;
    u8_t glb_ipv6_exist = 0;
#endif
    u8_t netif_num = netif_num_get();
    struct netif *xNetIf = netif_get_by_index(netif_num);
#if LWIP_DNS
    ip_addr_t *ptr_dns = dns_getserver(0);
#endif /* LWIP_DNS */

    if (xNetIf != NULL)
    {
        memset(ip_str, 0, IPV4_STR_SIZE);
        printf("  Interface Name: %c%c\n", xNetIf->name[0], xNetIf->name[1]);
        IPV4_TO_STR(ip_str, PP_HTONL(ip_addr_get_ip4_u32(&xNetIf->ip_addr)));
        printf("  IPv4 Address  : %s\n", ip_str);
#ifdef AIR_SUPPORT_IPV6
        printf("  Link-local IPv6 Address  : %s/64 (Auto)\n", (UI8_T *)ipaddr_ntoa((const ip_addr_t *)netif_ip6_addr(xNetIf, 0)));
        printf("                             (preferred lifetime: forever; valid lifetime: forever)\n");
        for (i = 1; i < LWIP_IPV6_NUM_ADDRESSES; ++i)
        {
            if (0 == ip_addr_isany(&(xNetIf->ip6_addr[i])))
            {
                addr_state = netif_ip6_addr_state(xNetIf, i);
                if (0 != ip6_addr_isvalid(addr_state))
                {
                    memcpy(ipv6_state, IP6_STATE_AUTO, sizeof(IP6_STATE_AUTO));
                }
                else
                {
                    memcpy(ipv6_state, IP6_STATE_INVAILD, sizeof(IP6_STATE_INVAILD));
                }
                if (0 == glb_ipv6_exist)
                {
                    printf("  Global IPv6 Address(es)  : %s/64 (%s)\n", (UI8_T *)ipaddr_ntoa((const ip_addr_t *)netif_ip6_addr(xNetIf, i)), ipv6_state);
#if LWIP_IPV6_ADDRESS_LIFETIMES
                    printf("                             (preferred lifetime: %dsec; valid lifetime: %dsec)\n",
                           netif_ip6_addr_pref_life(xNetIf, i), netif_ip6_addr_valid_life(xNetIf, i));
#endif
                    glb_ipv6_exist = 1;
                }
                else
                {
                    printf("                             %s/64 (%s)\n", (UI8_T *)ipaddr_ntoa((const ip_addr_t *)netif_ip6_addr(xNetIf, i)), ipv6_state);
#if LWIP_IPV6_ADDRESS_LIFETIMES
                    printf("                             (preferred lifetime: %dsec; valid lifetime: %dsec)\n",
                           netif_ip6_addr_pref_life(xNetIf, i), netif_ip6_addr_valid_life(xNetIf, i));
#endif
                }
            }
        }
        if (0 == glb_ipv6_exist)
        {
            printf("  Global IPv6 Address(es)  : None\n");
        }
#endif
        IPV4_TO_STR(ip_str, PP_HTONL(ip_addr_get_ip4_u32(&xNetIf->netmask)));
        printf("  Net Mask      : %s\n", ip_str);
        IPV4_TO_STR(ip_str, PP_HTONL(ip_addr_get_ip4_u32(&xNetIf->gw)));
        printf("  Gateway       : ");
#ifdef AIR_SUPPORT_IPV6
        i = nd6_select_router(NULL, xNetIf);
        if ((i >= 0) && (i < LWIP_ND6_NUM_ROUTERS) && (NULL != default_router_list[i].neighbor_entry))
        {
            printf("%s\n", (UI8_T *)ip6addr_ntoa(ip_2_ip6((const ip_addr_t *)&(default_router_list[i].neighbor_entry->next_hop_address))));
            printf("                  %s\n", ip_str);
        }
        else
#endif
        {
            printf("%s\n", ip_str);
        }
#if LWIP_DNS
        IPV4_TO_STR(ip_str, PP_HTONL(ip_addr_get_ip4_u32(ptr_dns)));
        printf("  Dns           : %s\n", ip_str);
#endif /* LWIP_DNS */
    }

    return pdFALSE;
}


static BOOL_T
_cmd_ip4_addr_isbroadcast(
    ip4_addr_t *ptr_ip,
    ip4_addr_t *ptr_netmask)
{
    u32_t addr;

    if ((NULL == ptr_ip) || (NULL == ptr_netmask))
    {
        return FALSE;
    }

    addr = ip4_addr_get_u32(ptr_ip);

    /* all ones (broadcast) or all zeroes (old skool broadcast) */
    if ((~addr == IPADDR_ANY) || (addr == IPADDR_ANY))
    {
        return TRUE;
    }
    else if ((addr & (~ip4_addr_get_u32(ptr_netmask))) ==
             (IPADDR_BROADCAST & (~ip4_addr_get_u32(ptr_netmask))))
    {
        /* => network broadcast address */
        return TRUE;
    }

    return FALSE;
}


static portBASE_TYPE
_ip_set_cmd(
    signed char *ptr_buf,
    size_t len,
    const signed char *ptr_input)
{
    char *ptr_str = NULL, *ptr_ip = NULL, *ptr_netmask = NULL, *ptr_gw = NULL;
    ip4_addr_t ip, netmask, gw;
#if LWIP_DNS
    char *ptr_dns = NULL;
    ip_addr_t dns;
#endif /*LWIP_DNS*/
    u8_t netif_num = 0;
    struct netif *ptr_netif = NULL;

    netif_num = netif_num_get();
    ptr_netif = netif_get_by_index(netif_num);

    if ((NULL == ptr_input) || (NULL == ptr_netif))
    {
        return pdFAIL;
    }

    ptr_str = strtok(ptr_input, " ");
    ptr_ip = strtok(NULL, " ");
    ptr_netmask = strtok(NULL, " ");
    ptr_gw = strtok(NULL, " ");
#if LWIP_DNS
    ptr_dns = strtok(NULL, " ");
#endif

    if (((NULL == ptr_ip) || (0 == ip4addr_aton(ptr_ip, &ip))) ||
        ((NULL == ptr_netmask) || (0 == ip4addr_aton(ptr_netmask, &netmask))) ||
        ((NULL == ptr_gw) || (0 == ip4addr_aton(ptr_gw, &gw)))
#if LWIP_DNS
         || ((NULL == ptr_dns) || (0 == ipaddr_aton(ptr_dns, &dns)))
#endif
       )
    {
        if (NULL != ptr_ip)
        {
            printf("Addresses are not all in dot-deciaml notation.\n");
        }
        return pdFAIL;
    }

    if ((0 == ip_addr_netmask_valid(&netmask)) || (TRUE == ip4_addr_isany_val(netmask)))
    {
        printf("Invalid netmask.\n");
        return pdFAIL;
    }

    if ((TRUE == _cmd_ip4_addr_isbroadcast(&ip, &netmask)) ||
        (TRUE == ip4_addr_isloopback(&ip)) ||
        (TRUE == ip4_addr_ismulticast(&ip)) ||
        (0 == ip4_addr1_val(ip)) ||
        (0 == (ip4_addr_get_u32(&ip) & (~ip4_addr_get_u32(&netmask)))))
    {
        printf("Invalid IP address.\n");
        return pdFAIL;
    }

    if ((TRUE == _cmd_ip4_addr_isbroadcast(&gw, &netmask)) ||
        (TRUE == ip4_addr_isloopback(&gw)) ||
        (TRUE == ip4_addr_ismulticast(&gw)) ||
        (0 == ip4_addr1_val(gw)) ||
        (0 == (ip4_addr_get_u32(&gw) & (~ip4_addr_get_u32(&netmask)))))
    {
        printf("Invalid gateway address.\n");
        return pdFAIL;
    }

#if LWIP_DNS
    if ((0 == ip4_addr1_val(*ip_2_ip4(&dns))) || (TRUE == ip4_addr_isloopback(ip_2_ip4(&dns))))
    {
        printf("Invalid DNS server address.\n");
        return pdFAIL;
    }
#endif /*LWIP_DNS*/

#if LWIP_DNS
    dns_setserver(0, &dns);
#endif /*LWIP_DNS*/
    netif_set_addr(ptr_netif, &ip, &netmask, &gw);

    printf("ip_set OK\n");
    return pdTRUE;
}

#ifdef AIR_MW_SUPPORT
static portBASE_TYPE sysmac_set_cmd(signed char *buf, size_t len, const signed char * input)
{
    char str[32], str_mac[32], str_tmp[3];
    char *ptr_str = str_mac;
    unsigned char mac[6];
    unsigned char i = 0;

    sscanf((char*)input, "%s %s", str, str_mac);

    if (strlen(str_mac) != 12)
    {
        printf("Invalid length of MAC.\n");
        return pdFALSE;
    }
    printf("\nSet new MAC =");
    for (i = 0; i < 6; i++ )
    {
        strncpy(str_tmp, ptr_str, 2);
        ptr_str+=2;
        mac[i] = strtoul(str_tmp, NULL, 16);
        printf(" %02X", mac[i]);
    }
    printf("\n");
    update_mac_addr(mac);

    return pdFALSE;
}
#endif

void cmd_output(char * buf, uint32_t len)
{
    int i;

    for (i = 0; i < len; i++)
    {
        outbyte((int)buf[i]);
    }
}

#ifdef SRAM_SHRINK___TOP
void vTimerCallback_Top ( TimerHandle_t xTimer )
{
    memcpy(timeStats_10, timeStats_5, 32);
    memset((void*)timeStats_5, 0, 32);
    airTaskGetRunTimeStats(timeStats_5, 0);
}

void setup_timeStats_update_interval(uint32_t interval_ms)
{
    TimerHandle_t xTimers = xTimerCreate( "top",
                                          ( interval_ms* portTICK_PERIOD_MS ),
                                          pdTRUE,
                                          NULL,
                                          vTimerCallback_Top);
    xTimerStart( xTimers, portMAX_DELAY);
}
#else
static void top_task(void)
{
    static portTickType xLastWakeTime;
    const portTickType Xfrequency = pdMS_TO_TICKS(5000);
    xLastWakeTime = xTaskGetTickCount();
    for( ;; )
    {
        vTaskDelayUntil(&xLastWakeTime, Xfrequency);
        memcpy(timeStats_10, timeStats_5, 32);
        memset((void*)timeStats_5, 0, 32);
        airTaskGetRunTimeStats(timeStats_5, 0);
    }
}
void create_top_task(void)
{
    xTaskCreate(top_task,            /* The function that implements the task. */
                "top",                          /* The text name assigned to the task - for debug only as it is not used by the kernel. */
                (configMINIMAL_STACK_SIZE * 2), /* The size of the stack to allocate to the task. */
                NULL,                           /* The parameter passed to the task - not used in this simple case. */
                (tskIDLE_PRIORITY + 2),         /* The priority assigned to the task. */
                NULL );                         /* The task handle is not required, so NULL is passed. */

    return;
}
#endif

static void cmd_queue_recv_task( void *pvParameters )
{
    uint32_t value;
#if 1//ndef SRAM_SHRINK___CMD
    GDMPSRAM_BSS static signed char input[CMD_SH_MAX_INPUT], output[CMD_SH_MAX_OUTPUT];
#else
    // raven.todo.
    // is cmd task stack enough ?
    // how about use heap ???
    signed char input[CMD_SH_MAX_INPUT], output[CMD_SH_MAX_OUTPUT];
#endif

    unsigned char input_char;
    signed int input_index;
    portBASE_TYPE ret;
    int i;

#ifndef SRAM_SHRINK___CMD
    g_cmd_queue = xQueueCreate(128, sizeof(uint32_t), "cmd");
#else
    g_cmd_queue = xQueueCreate(128, sizeof(unsigned char), "cmd");
#endif

    if(g_cmd_queue == NULL)
    {
        printf("\nError: xQueueCreate failed !\n");
        return;
    }

#ifndef SRAM_SHRINK___CMD_REDESIGN
    /* register_commands */
    for(i=0; sysCmds[i].cmd !=NULL; i++)
    {
        cmd_register(&sysCmds[i], CMD_PRIVILEGE_MODE_EXEC);
    }

    /* register_debug_commands */
    for(i=0; debugCmds[i].cmd !=NULL; i++)
    {
        cmd_register(&debugCmds[i], CMD_PRIVILEGE_MODE_DEBUG);
    }
#else
    cmd_register(&sysCmds, CMD_PRIVILEGE_MODE_EXEC);
    cmd_register(&debugCmds, CMD_PRIVILEGE_MODE_DEBUG);
#endif

    memset(input, 0x00, CMD_SH_MAX_INPUT);
    memset(output, 0x00, CMD_SH_MAX_OUTPUT);
    cmd_prompt();

#ifdef SRAM_SHRINK___TOP
    setup_timeStats_update_interval(5000);
#endif


    input_index = 0;
    for( ;; )
    {
        /* Wait until something arrives in the queue - this task will block
        indefinitely provided INCLUDE_vTaskSuspend is set to 1 in
        FreeRTOSConfig.h.  It will not use any CPU time while it is in the
        Blocked state. */
#ifndef SRAM_SHRINK___CMD
        xQueueReceive(g_cmd_queue, &value, portMAX_DELAY);
        input_char = (signed char)value;
#else
        xQueueReceive(g_cmd_queue, &input_char, portMAX_DELAY);
#endif

#ifdef CMD_FIX_BACKSPACE_FOR_UI
        if( input_char == '\b' )
        {
            if( input_index > 0 )
            {
                serial_outc(input_char);

                char temp[]=" \b";
                cmd_output(temp, strlen(temp));
            }
        }
        else
#endif
        {
            serial_outc(input_char);
        }

        if ((input_char == '\n') || (input_char == '\r'))
        {
            cmd_output("\r\n", strlen("\r\n"));
            if (    !strncmp(input, "exit", strlen("exit")) &&
                    (strlen("exit") == input_index) &&
                    (   (CMD_PRIVILEGE_MODE_SDK == g_privilege_mode) ||
                        (CMD_PRIVILEGE_MODE_PERIPHERAL == g_privilege_mode) ||
#ifdef AIR_MW_SUPPORT
                        (CMD_PRIVILEGE_MODE_MW == g_privilege_mode) ||
#endif
                        (CMD_PRIVILEGE_MODE_DEBUG == g_privilege_mode)
                    )
                )
            {
                g_privilege_mode = CMD_PRIVILEGE_MODE_EXEC;
            }
            else
            {
                if (CMD_PRIVILEGE_MODE_SDK == g_privilege_mode)
                {
                    dsh_parseString(input);
                }
#ifdef AIR_8851_SUPPORT
                else if (CMD_PRIVILEGE_MODE_PERIPHERAL == g_privilege_mode)
                {
                    dsh_peripheral_parseString(input);
                }
#endif
#ifdef AIR_MW_SUPPORT
                else if (CMD_PRIVILEGE_MODE_MW == g_privilege_mode)
                {
                    mw_cmd_parseString(input);
                }
#endif
                else
                {
                    do
                    {
                        ret = cmd_process(input, output, CMD_SH_MAX_OUTPUT, g_privilege_mode);
                        cmd_output(output, strlen(output));
                    } while( ret != pdFALSE );
                }
            }

            input_index = 0;
            memset(input, 0x00, CMD_SH_MAX_INPUT);
            memset(output, 0x00, CMD_SH_MAX_OUTPUT);
            cmd_prompt();
        }
        else
        {
            if( input_char == '\b' )
            {
                /* Backspace was pressed.  Erase the last  character in the string - if any. */
                if( input_index > 0 )
                {
                    input_index--;
                    input[input_index] = '\0';
                }
            }
            else
            {
                /* A character was entered.  Add it to the string entered so far.  When a \n is entered the complete string will be passed to the command interpreter. */
                if( input_index < sizeof(input) )
                {
                    input[input_index] = input_char;
                    input_index++;
                }
            }
        }
    }
}

void create_queue_recv_task(void)
{
    if (!cmd_taskHandle)
    {
        xTaskCreate(cmd_queue_recv_task,            /* The function that implements the task. */
                    "CMD",                          /* The text name assigned to the task - for debug only as it is not used by the kernel. */
                    (288 * 3),                      /* The size of the stack to allocate to the task. */
                    NULL,                           /* The parameter passed to the task - not used in this simple case. */
                    (tskIDLE_PRIORITY + 3),         /* The priority assigned to the task. */
                    &cmd_taskHandle );              /* The task handle is not required, so NULL is passed. */
    }

    return;
}
