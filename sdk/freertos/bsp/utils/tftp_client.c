/*******************************************************************************
*  The software may not be copied and the information
*  contained herein may not be used or disclosed except with the written
*  permission of Airoha Technology Corp. (C) 2021
*
*  BY OPENING THIS FILE, BUYER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
*  THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("AIROHA SOFTWARE")
*  RECEIVED FROM AIROHA AND/OR ITS REPRESENTATIVES ARE PROVIDED TO BUYER ON
*  AN "AS-IS" BASIS ONLY. AIROHA EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
*  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
*  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
*  NEITHER DOES AIROHA PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
*  SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
*  SUPPLIED WITH THE AIROHA SOFTWARE, AND BUYER AGREES TO LOOK ONLY TO SUCH
*  THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. AIROHA SHALL ALSO
*  NOT BE RESPONSIBLE FOR ANY AIROHA SOFTWARE RELEASES MADE TO BUYER'S
*  SPECIFICATION OR TO CONFORM TO A PARTICULAR STANDARD OR OPEN FORUM.
*
*  BUYER'S SOLE AND EXCLUSIVE REMEDY AND AIROHA'S ENTIRE AND CUMULATIVE
*  LIABILITY WITH RESPECT TO THE AIROHA SOFTWARE RELEASED HEREUNDER WILL BE,
*  AT AIROHA'S OPTION, TO REVISE OR REPLACE THE AIROHA SOFTWARE AT ISSUE,
*  OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY BUYER TO
*  AIROHA FOR SUCH AIROHA SOFTWARE AT ISSUE.
*
*  THE TRANSACTION CONTEMPLATED HEREUNDER SHALL BE CONSTRUED IN ACCORDANCE
*  WITH THE LAWS OF THE STATE OF CALIFORNIA, USA, EXCLUDING ITS CONFLICT OF
*  LAWS PRINCIPLES.  ANY DISPUTES, CONTROVERSIES OR CLAIMS ARISING THEREOF AND
*  RELATED THERETO SHALL BE SETTLED BY ARBITRATION IN SAN FRANCISCO, CA, UNDER
*  THE RULES OF THE INTERNATIONAL CHAMBER OF COMMERCE (ICC).
*
*******************************************************************************/
/* FILE NAME:  tftp_client.c
 * PURPOSE:
 *    tftp_client to get file by lwip and write to flash.
 *
 * NOTES:
 *
 */
/* INCLUDE FILE DECLARATIONS
 */

#include <stdio.h>
#include <FreeRTOS.h>
#include <task.h>
#include "FreeRTOSConfig.h"
#include "spinorwrite.h"

#include "lwip/apps/tftp_client.h"

#include <string.h>

/* NAMING CONSTANT DECLARATIONS
 */

/* MACRO FUNCTION DECLARATIONS
 */

/* DATA TYPE DECLARATIONS
 */

/* GLOBAL VARIABLE DECLARATIONS
 */
extern void air_wdog_kick(void);

/* LOCAL SUBPROGRAM DECLARATIONS
 */

#if LWIP_UDP
/* STATIC VARIABLE DECLARATIONS
 */

static TaskHandle_t xHandle;
static int recvSize;
#ifdef AIR_8855_SUPPORT
static unsigned char *tftp_flash_addr;
static unsigned char *tftp_buf_base;
static unsigned char *tftp_flash_buf;
#endif

/* LOCAL SUBPROGRAM BODIES
 */
#ifdef AIR_8855_SUPPORT
int tftp_write_buffer(unsigned char *buffer, int size)
{
    int i = 0;
    int result = 0;

    if (!tftp_flash_buf)
    {
        return 0;
    }
    
    for (i = 0; i < size; i++)
    {
        *tftp_buf_base++ = *buffer++;
        if (tftp_buf_base == (tftp_flash_buf + SPI_NOR_SECTOR_SIZE))
        {
            spinor_write(tftp_flash_buf, tftp_flash_addr, SPI_NOR_SECTOR_SIZE);
            if (result != 0)
            {
                return result;
            }
            printf(".");

            tftp_flash_addr += SPI_NOR_SECTOR_SIZE;
            tftp_buf_base = tftp_flash_buf;
        }
    }

    return 0;
}

int tftp_write_lastbuffer(void)
{
    if (!tftp_flash_buf)
    {
        return 0;
    }

    if (tftp_buf_base > tftp_flash_buf)
    {
        spinor_write(tftp_flash_buf, tftp_flash_addr, ((unsigned int)tftp_buf_base - (unsigned int)tftp_flash_buf));
    }

    return 0;
}

void tftp_write_buffer_init(unsigned char *flash_addr)
{
    tftp_flash_addr = flash_addr;
    tftp_buf_base = tftp_flash_buf;
}

#endif

/* FUNCTION NAME:   tftp_open
 * PURPOSE:
 *      No used.
 * INPUT:
 *      fname               -- Filename.
 *      mode                -- Mode string from TFTP RFC 1350.
 *      is_write            -- Flag indicating read (0) or write (!= 0) access.
 * OUTPUT:
 *      None
 * RETURN:
 *      handle              -- File handle supplied to other functions.
 * NOTES:
 *
 */
static void* tftp_open(const char* fname, const char* mode, u8_t is_write)
{
    return NULL;
}

/* FUNCTION NAME:   tftp_close
 * PURPOSE:
 *      Write last data to flash and chechk image CRC and prepare fw upgrade.
 * INPUT:
 *      handle              -- File handle supplied to other functions.
 * OUTPUT:
 *      None
 * RETURN:
 *      None
 * NOTES:
 *
 */
static void tftp_close(void* handle)
{    
    #ifdef AIR_8855_SUPPORT
    tftp_write_lastbuffer();
    #else
    WriteLastBuffer();
    #endif

    xTaskNotify((TaskHandle_t) handle, recvSize, eSetValueWithOverwrite);
    recvSize = 0;
}

/* FUNCTION NAME:   tftp_read
 * PURPOSE:
 *      No used.
 * INPUT:
 *      handle              -- File handle returned by open()/tftp_put()/tftp_get().
 *      buf                 -- Target buffer to copy read data to.
 *      bytes               -- Number of bytes to copy to buf.
 * OUTPUT:
 *      None
 * RETURN:
 *      return              -- &gt;= 0: Success; &lt; 0: Error.
 * NOTES:
 *
 */
static int tftp_read(void* handle, void* buf, int bytes)
{
    return -1;
}

/* FUNCTION NAME:   tftp_write
 * PURPOSE:
 *      Write data to flash.
 * INPUT:
 *      handle              -- File handle returned by open()/tftp_put()/tftp_get().
 *      pbuf                -- PBUF adjusted such that payload pointer points
 *                             to the beginning of write data. In other words,
 *                             TFTP headers are stripped off.
 * OUTPUT:
 *      None
 * RETURN:
 *      return              -- &gt;= 0: Success; &lt; 0: Error.
 * NOTES:
 */
static int tftp_write(void* handle, struct pbuf* p)
{
    int result = 0;

    while ((p != NULL) && (result == 0))
    {
        air_wdog_kick();
        #ifdef AIR_8855_SUPPORT
        result = tftp_write_buffer(p->payload, p->len);
        #else
        result = WriteBuffer(p->payload, p->len);
        #endif
        recvSize += p->len;
        p = p->next;
    }
    return result;
}

/* FUNCTION NAME:   tftp_error
 * PURPOSE:
 *      Error indication from client or response from server.
 * INPUT:
 *      handle              -- File handle returned by open()/tftp_put()/tftp_get().
 *      err                 -- error code from client or server.
 *      msg                 -- error message from client or server.
 *      size                -- size size of msg.
 * OUTPUT:
 *      None
 * RETURN:
 *      None
 * NOTES:
 */
/* For TFTP client only */
static void tftp_error(void* handle, int err, const char* msg, int size)
{
    char message[100];

    memset(message, 0, sizeof(message));
    MEMCPY(message, msg, LWIP_MIN(sizeof(message)-1, (size_t)size));

    printf("\nTFTP error: %d (%s)\n", err, message);

    xTaskNotify((TaskHandle_t) handle, 0, eSetValueWithOverwrite);
}

static const struct tftp_context tftp = {
    tftp_open,
    tftp_close,
    tftp_read,
    tftp_write,
    tftp_error
};

/* EXPORTED SUBPROGRAM BODIES
 */

/* Initial to tftp client mode */
void tftp_init(void)
{
    err_t err;

    err = tftp_init_client(&tftp);
    LWIP_ASSERT("tftp_init_client failed", err == ERR_OK);
}

/* Create tftp get task */
int tftp_get_file(char *ip, char *name, unsigned int flash_addr)
{
    err_t err = ERR_OK;
    TickType_t xMaxBlockTime = pdMS_TO_TICKS(60000);
    ip_addr_t srv;
    uint32_t ulReturn;
    unsigned int i = 0;
    unsigned int temp_src = 0;
    unsigned int temp_dst =0;
    unsigned int temp_size = 0;

    ipaddr_aton(ip, &srv);

    #ifdef AIR_8855_SUPPORT
    tftp_flash_buf = pvPortMalloc(SPI_NOR_SECTOR_SIZE, "cmd");
    if (!tftp_flash_buf)
    {
        printf("Error: Not enough memory!\n");
        return ERR_MEM;
    }
    tftp_write_buffer_init((unsigned char *) TempSystemBase);
    #else
    WriteBufferInit((unsigned char *) TempSystemBase);
    #endif

    xHandle = xTaskGetCurrentTaskHandle();
    recvSize = 0;
    err = tftp_get(xHandle, &srv, TFTP_PORT, name, TFTP_MODE_OCTET);
    LWIP_ASSERT("tftp_get failed", err == ERR_OK);
    if (err == ERR_OK)
    {
        ulReturn = ulTaskNotifyTake(pdTRUE, xMaxBlockTime);
        #ifdef AIR_8855_SUPPORT
        vPortFree(tftp_flash_buf);
        tftp_flash_buf = NULL;
        #endif
    }
    else
    {
        #ifdef AIR_8855_SUPPORT
        vPortFree(tftp_flash_buf);
        tftp_flash_buf = NULL;
        #endif
        return err;
    }
    xHandle = NULL;

    if (ulReturn > 0)
    {
        /* Update raw image to special flash address */
        if (flash_addr != ~0UL)
        {
            temp_src = TempSystemBase;
            temp_dst = flash_addr;
            temp_size = ulReturn;

            while (temp_size)
            {
                if (temp_size > SPI_NOR_SECTOR_SIZE)
                    i = SPI_NOR_SECTOR_SIZE;
                else
                    i = temp_size;

                spinor_write(temp_src, temp_dst, i);
                temp_src += i;
                temp_dst += i;
                temp_size -= i;
            }
        }
    }

    return ulReturn;
}

#endif /* LWIP_UDP */
