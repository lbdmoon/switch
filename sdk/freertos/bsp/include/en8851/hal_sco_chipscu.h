/*******************************************************************************
*  Copyright Statement:
*  --------------------
*  This software is protected by Copyright and the information contained
*  herein is confidential. The software may not be copied and the information
*  contained herein may not be used or disclosed except with the written
*  permission of Airoha Technology Corp. (C) 2022
*
*  BY OPENING THIS FILE, BUYER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
*  THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("AIROHA SOFTWARE")
*  RECEIVED FROM AIROHA AND/OR ITS REPRESENTATIVES ARE PROVIDED TO BUYER ON
*  AN "AS-IS" BASIS ONLY. AIROHA EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
*  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
*  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
*  NEITHER DOES AIROHA PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
*  SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
*  SUPPLIED WITH THE AIROHA SOFTWARE, AND BUYER AGREES TO LOOK ONLY TO SUCH
*  THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. AIROHA SHALL ALSO
*  NOT BE RESPONSIBLE FOR ANY AIROHA SOFTWARE RELEASES MADE TO BUYER'S
*  SPECIFICATION OR TO CONFORM TO A PARTICULAR STANDARD OR OPEN FORUM.
*
*  BUYER'S SOLE AND EXCLUSIVE REMEDY AND AIROHA'S ENTIRE AND CUMULATIVE
*  LIABILITY WITH RESPECT TO THE AIROHA SOFTWARE RELEASED HEREUNDER WILL BE,
*  AT AIROHA'S OPTION, TO REVISE OR REPLACE THE AIROHA SOFTWARE AT ISSUE,
*  OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY BUYER TO
*  AIROHA FOR SUCH AIROHA SOFTWARE AT ISSUE.
*
*  THE TRANSACTION CONTEMPLATED HEREUNDER SHALL BE CONSTRUED IN ACCORDANCE
*  WITH THE LAWS OF THE STATE OF CALIFORNIA, USA, EXCLUDING ITS CONFLICT OF
*  LAWS PRINCIPLES.  ANY DISPUTES, CONTROVERSIES OR CLAIMS ARISING THEREOF AND
*  RELATED THERETO SHALL BE SETTLED BY ARBITRATION IN SAN FRANCISCO, CA, UNDER
*  THE RULES OF THE INTERNATIONAL CHAMBER OF COMMERCE (ICC).
*
*******************************************************************************/

/* FILE NAME:  hal_sco_chipscu.h
 * PURPOSE:
 *  Define CHIP SCU module HAL function.
 *
 * NOTES:
 *
 */

#ifndef HAL_SCO_CHIPSCU_H
#define HAL_SCO_CHIPSCU_H

/* INCLUDE FILE DECLARTIONS
*/
#include <air_chipscu.h>

/* NAMING CONSTANT DECLARATIONS
*/

/* MACRO FUNCTION DECLARATIONS
*/

/* DATA TYPE DECLARATIONS
*/

/* EXPORTED SUBPROGRAM SPECIFICATIONS
*/
/* FUNCTION NAME: hal_sco_chipscu_getInterruptTriggerMode
 * PURPOSE:
 *      Get HW interrupt trigger type for CPU
 *
 * INPUT:
 *      unit            --  Device ID
 *      hw_intr_src     --  CPU HW interrupt source number
 *
 * OUTPUT:
 *      ptr_trig_type   --  CPU HW interrupt trigger type
 *                          0: high-level-triggered
 *                          1: positive-edge-triggered
 *
 * RETURN:
 *      E_OK
 *
 * NOTES:
 *      Specific for N10 CPU only
 */
int
hal_sco_chipscu_getInterruptTriggerMode(
    const unsigned int unit,
    const AIR_CHIPSCU_INTR_SOURCE_T hw_intr_src,
    AIR_CHIPSCU_INTR_TRIGGER_TYPE_T *ptr_trig_types);

/* FUNCTION NAME: hal_sco_chipscu_setInterruptTriggerMode
 * PURPOSE:
 *      Configure HW interrupt trigger type for CPU
 *
 * INPUT:
 *      unit            --  Device ID
 *      hw_intr_src     --  CPU HW interrupt source number
 *      trigger_type    --  CPU HW interrupt trigger type
 *                          0: high-level-triggered
 *                          1: positive-edge-triggered
 *
 * OUTPUT:
 *      None
 *
 * RETURN:
 *      E_OK
 *
 * NOTES:
 *      Specific for N10 CPU only
 */
int
hal_sco_chipscu_setInterruptTriggerMode(
    const unsigned int unit,
    const AIR_CHIPSCU_INTR_SOURCE_T hw_intr_src,
    AIR_CHIPSCU_INTR_TRIGGER_TYPE_T chip_trig_types);

/* FUNCTION NAME: hal_sco_chipscu_getIomuxFuncState
 * PURPOSE:
 *      Get specific function state of IOMUX.
 *
 * INPUT:
 *      unit            --  Device ID
 *      func_idx        --  Funciton index of IOMUX
 *
 * OUTPUT:
 *      ptr_state       --  specific function state of IOMUX
 *                          0: disable
 *                          1: enable
 *
 * RETURN:
 *      E_OK
 *
 * NOTES:
 *      If needs to use IOMUX get/set API to change pin function for specific purpose,
 *      be aware of precedence relation between function of IOMUX and check the precedence in related document.
 *
 *      For EN8851/53/60, few IOMUX pins are effected as group control by below functions:
 *      C_MDIO/ P_MDIO/ SGPIO/ IRQ/ SPI_QUAD/ ... etc.
 *      Ex. GPIO9~GPIO12 pin will translate function from GPIO to IRQ if enable IRQ mode via IOMUX.
 *      Ex. GPIO10/GPIO11 pin will tranlate function from GPIO to SPI_HOLD/SPI_WP if enable SPI_QUAD mode via IOMUX.
 *      The IOMUX pin will translate function by precedence in case such conflict happened.
 *      For example, the precedence of GPIO pin 10 & GPIO 11 is Force GPIO > GPIO LAN LED > IRQ > SPI Quad
 *      Both enabling SPI_QUAD and IRQ mode via IOMUX, GPIO pin 9 ~ GPIO pin 12 will tranlate to IRQ mode,
 *      GPIO pin 10 & GPIO pin 11 will keep stay IRQ mode because IRQ mode has higher precedence than SPI Quad mode.
 *
 */
int
hal_sco_chipscu_getIomuxFuncState(
    const unsigned int unit,
    const AIR_CHIPSCU_IOMUX_FUNC_T func_idx,
    int *ptr_state);

/* FUNCTION NAME: hal_sco_chipscu_setIomuxFuncState
 * PURPOSE:
 *      Set specific function state of IOMUX.
 *
 * INPUT:
 *      unit            --  Device ID
 *      func_idx        --  Funciton index of IOMUX
 *      state           --  specific function state of IOMUX
 *                          0: disable
 *                          1: enable
 *
 * OUTPUT:
 *      None
 *
 * RETURN:
 *      E_OK
 *
 * NOTES:
 *      If needs to use IOMUX get/set API to change pin function for specific purpose,
 *      be aware of precedence relation between function of IOMUX and check the precedence in related document.
 *
 *      For EN8851/53/60, few IOMUX pins are effected as group control by below functions:
 *      C_MDIO/ P_MDIO/ SGPIO/ IRQ/ SPI_QUAD/ ... etc.
 *      Ex. GPIO9~GPIO12 pin will translate function from GPIO to IRQ if enable IRQ mode via IOMUX.
 *      Ex. GPIO10/GPIO11 pin will tranlate function from GPIO to SPI_HOLD/SPI_WP if enable SPI_QUAD mode via IOMUX.
 *      The IOMUX pin will translate function by precedence in case such conflict happened.
 *      For example, the precedence of GPIO pin 10 & GPIO 11 is Force GPIO > GPIO LAN LED > IRQ > SPI Quad
 *      Both enabling SPI_QUAD and IRQ mode via IOMUX, GPIO pin 9 ~ GPIO pin 12 will tranlate to IRQ mode,
 *      GPIO pin 10 & GPIO pin 11 will keep stay IRQ mode because IRQ mode has higher precedence than SPI Quad mode.
 *
 */
int
hal_sco_chipscu_setIomuxFuncState(
    const unsigned int unit,
    const AIR_CHIPSCU_IOMUX_FUNC_T func_idx,
    int state);

/* FUNCTION NAME: hal_sco_chipscu_resetSystem
 * PURPOSE:
 *      Reset system.
 *
 * INPUT:
 *      unit            --  Device ID
 *
 * OUTPUT:
 *      None
 *
 * RETURN:
 *      E_OK
 *
 * NOTES:
 *      None
 */
int
hal_sco_chipscu_resetSystem(
    const unsigned int unit);

#endif /* end of HAL_SCO_CHIPSCU_H */
