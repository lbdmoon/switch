/*
 * FreeRTOS Kernel V10.1.1
 * Copyright (C) 2017 Amazon.com, Inc. or its affiliates.  All Rights Reserved.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software. If you wish to use our Amazon
 * FreeRTOS name, please do so in a fair use way that does not cause confusion.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * http://www.FreeRTOS.org
 * http://aws.amazon.com/freertos
 *
 * 1 tab == 4 spaces!
 */

/******************************************************************************
 * NOTE 1:  This project provides two demo applications.  A simple blinky
 * style project, and a more comprehensive test and demo application.  The
 * mainSELECTED_APPLICATION setting in main.c is used to select between the two.
 * See the notes on using mainSELECTED_APPLICATION where it is defined below.
 *
 * NOTE 2:  This file only contains the source code that is not specific to
 * either the simply blinky or full demos - this includes initialisation code
 * and callback functions.
 */

/* Standard includes. */
#include <stdio.h>

/* Kernel includes. */
#include "FreeRTOS.h"
#include "task.h"

/* lwIP core includes */
#include "lwip/opt.h"
#include "lwip/tcpip.h"
#include "lwip/ip.h"

#include "spinorwrite.h"
#include "sdk_ref.h"
#include "customer_system.h"
#include "timer.h"
#ifdef AIR_MW_SUPPORT
#include "mw_error.h"
#endif
#ifdef AIR_SUPPORT_SFP
#include "sfp_task.h"
#endif

#if 1	/* wy,added */
int system_inspection(void);
#endif

/* Prototypes for the standard FreeRTOS callback/hook functions implemented
within this file. */
void vApplicationMallocFailedHook( void );
void vApplicationIdleHook( void );
void vApplicationStackOverflowHook( TaskHandle_t pxTask, char *pcTaskName );
void vApplicationTickHook( void );

/*-----------------------------------------------------------*/
static void airSetupHardware( void )
{
    airInitHardware();
    return;
}

extern void pdma_init (void);
extern err_t ethernetif_init( struct netif *xNetIf );
extern err_t mac_rcv_init(void);
extern void sys_mem_init(void);
#ifdef AIR_EN_APP_SFP
extern void air_app_sfp_createSfpAppTask(void);
#endif
#ifdef SRAM_SHRINK___FLASH
void sram_code_init(void);
#endif

void lwip_app_init()
{
    /* This call creates the TCP/IP thread. */
    tcpip_init(NULL, NULL);

#ifdef AIR_MW_SUPPORT
    /* Create the httpd server from the standard lwIP code.  This demonstrates
    use of the lwIP raw API. */
    httpd_init();
#endif
#if LWIP_SNMP
    snmp_init();
#endif
}

void net_interface_init()
{
    ip4_addr_t xIPAddr, xNetMask, xGateway;
    ip_addr_t xDns;
    static struct netif xNetIf;

    /* Set up the network interface. */
    ip4_addr_set_zero(&xGateway);
    ip4_addr_set_zero(&xIPAddr);
    ip4_addr_set_zero(&xNetMask);
    ip_addr_set_zero(&xDns);

    LWIP_PORT_INIT_GW(&xGateway);
    LWIP_PORT_INIT_IPADDR(&xIPAddr);
    LWIP_PORT_INIT_NETMASK(&xNetMask);
    LWIP_PORT_INIT_DNS(&xDns);

    netif_set_default(netif_add(&xNetIf, &xIPAddr, &xNetMask, &xGateway, NULL, ethernetif_init, tcpip_input));

#if LWIP_IPV6
    printf("MAC address: %x%x%x%x%x%x\n", xNetIf.hwaddr[0], xNetIf.hwaddr[1], xNetIf.hwaddr[2], xNetIf.hwaddr[3], xNetIf.hwaddr[4], xNetIf.hwaddr[5]);
    netif_create_ip6_linklocal_address(&xNetIf, 1);
#if LWIP_IPV6_AUTOCONFIG
    xNetIf.ip6_autoconfig_enabled = 1;
#endif /* LWIP_IPV6_AUTOCONFIG */
#endif /* LWIP_IPV6 */

#if LWIP_DNS
    dns_setserver(0,&xDns);
#endif /* LWIP_DNS */
    netif_set_up(&xNetIf);

#if LWIP_IPV6
    printf("IPv6 linklocal address: %s; addr_state:%d\n", ip6addr_ntoa(netif_ip6_addr(&xNetIf, 0)), netif_ip6_addr_state(&xNetIf, 0));
#endif /* LWIP_IPV6 */
}

#ifdef SRAM_SHRINK___FLASH
void sram_code_init(void)
{
    extern unsigned char __dmemimg_lmastart, __dmemimg_vmastart, __dmemimg_vmaend;

    memcpy((unsigned char*)&(__dmemimg_vmastart),
        (unsigned char*)&(__dmemimg_lmastart),
        (unsigned int)(&(__dmemimg_vmaend)-&(__dmemimg_vmastart)));
    nds32_dcache_flush();
    nds32_icache_flush();
}
#endif

int main( void )
{
    int rc;

    /* Configure the hardware ready to run the demo. */
    prvSetupHardware();
    airSetupHardware();
    printf("Setting up hardware...Done!\n");

#ifndef AIR_8855_SUPPORT
    /* create tasks */
    printf("Creating shell task for uart...");
    create_queue_recv_task();
    printf("Done!\n");
#endif

#ifdef SRAM_SHRINK___FLASH
    //sram_code_init();
#endif

    //spinor_write_init();
    //create_flash_conf_task();
    //printf("Done!\n");

#if 1	/* wy,added */
	system_inspection();
#endif

#if defined(AIR_8851_SUPPORT) || defined(AIR_8855_SUPPORT)
    /* Customer system initialization */
    printf("Initializing customer system...");
    rc = customer_system_init();
    if (E_OK == rc)
    {
        printf("Done!\n");

        /* sdk references initialization */
        printf("Initializing sdk reference...\n");
        rc = sdk_ref_init();
        if (AIR_E_OK == rc)
        {
            /* Customer system post-initialization */
            printf("Post-initializing customer system...\n");
            rc = customer_system_post_init();
            if (E_OK == rc)
            {
                printf("Done!\n");
            }
            else
            {
                printf("Failed!\n");
            }
            //printf("Done!\n");
            //printf("Initializing packet related...\n");
            //pdma_init();
            //mac_rcv_init();
            //sys_mem_init();
            //printf("Done!\n");

            //printf("Initializing lwip ...\n");
            //lwip_app_init();
            //tftp_init();
            //printf("Done!\n");
#ifdef AIR_MW_SUPPORT
            /* Middleware module initialization */
            printf("Middleware module initialization ...\n");
            rc = mw_init_initModule(0);
            if (MW_E_OK == rc)
            {
                printf("Done!\n");
            }
            else
            {
                printf("Failed!\n");
            }
#endif
            //net_interface_init();

#if defined(AIR_MW_SUPPORT) && defined(AIR_SUPPORT_IPV6)
            mw_init_set_acl_for_multicast_mac_addresses();
#endif
        }
        else
        {
            printf("Failed!\n");
        }
    }
    else
    {
        printf("Failed!\n");
    }
#endif

#ifndef SRAM_SHRINK___TOP
    create_top_task();
#endif

#ifdef AIR_EN_APP_SFP
    air_app_sfp_createSfpAppTask();
#endif

#ifdef AIR_SUPPORT_SFP
    sfp_task_create();
#endif

    printf("setup watchdog\n");
    wdog_setup(20,300);

    /* start tasks scheduling */
    vTaskStartScheduler();
    while(1); /* Don't expect to reach here. */

    /* Don't expect to reach here. */
    return 0;
}
/*-----------------------------------------------------------*/

void prvSetupHardware( void )
{
    /* Ensure no interrupts execute while the scheduler is in an inconsistent
    state.  Interrupts are automatically enabled when the scheduler is
    started. */
    portDISABLE_INTERRUPTS();
}
/*-----------------------------------------------------------*/

void vAssertCalled( const char * pcFile, unsigned long ulLine )
{
volatile unsigned long ul = 0;

    ( void ) pcFile;
    ( void ) ulLine;

    printf( "ASSERT! Line %d, file %s\r\n", ( int )ulLine, pcFile );

    taskENTER_CRITICAL();
    {
        /* Set ul to a non-zero value using the debugger to step out of this
        function. */
        while( ul == 0 )
        {
            portNOP();
        }
    }
    taskEXIT_CRITICAL();
}
/*-----------------------------------------------------------*/

void vApplicationMallocFailedHook( void )
{
    /* vApplicationMallocFailedHook() will only be called if
    configUSE_MALLOC_FAILED_HOOK is set to 1 in FreeRTOSConfig.h.  It is a hook
    function that will get called if a call to pvPortMalloc() fails.
    pvPortMalloc() is called internally by the kernel whenever a task, queue,
    timer or semaphore is created.  It is also called by various parts of the
    demo application.  If heap_1.c, heap_2.c or heap_4.c is being used, then the
    size of the     heap available to pvPortMalloc() is defined by
    configTOTAL_HEAP_SIZE in FreeRTOSConfig.h, and the xPortGetFreeHeapSize()
    API function can be used to query the size of free heap space that remains
    (although it does not provide information on how the remaining heap might be
    fragmented).  See http://www.freertos.org/a00111.html for more
    information. */
    #ifdef AIR_8855_SUPPORT
    DUMP_STACK();
    #endif
    vAssertCalled( __FILE__, __LINE__ );
}
/*-----------------------------------------------------------*/

void vApplicationStackOverflowHook( TaskHandle_t pxTask, char *pcTaskName )
{
    ( void ) pcTaskName;
    ( void ) pxTask;

    printf( "StackOverflow task %s\r\n", pcTaskName );

    /* Run time stack overflow checking is performed if
    configCHECK_FOR_STACK_OVERFLOW is defined to 1 or 2.  This hook
    function is called if a stack overflow is detected. */
    #ifdef AIR_8855_SUPPORT
    DUMP_STACK();
    #endif
    vAssertCalled( __FILE__, __LINE__ );
}
/*-----------------------------------------------------------*/

void vApplicationIdleHook( void )
{
volatile size_t xFreeHeapSpace;

    /* This is just a trivial example of an idle hook.  It is called on each
    cycle of the idle task.  It must *NOT* attempt to block.  In this case the
    idle task just queries the amount of FreeRTOS heap that remains.  See the
    memory management section on the http://www.FreeRTOS.org web site for memory
    management options.  If there is a lot of heap memory free then the
    configTOTAL_HEAP_SIZE value in FreeRTOSConfig.h can be reduced to free up
    RAM. */
    xFreeHeapSpace = xPortGetFreeHeapSize();

    /* Remove compiler warning about xFreeHeapSpace being set but never used. */
    ( void ) xFreeHeapSpace;

    wdog_idle_handler();
}

/*-----------------------------------------------------------*/

void vApplicationTickHook( void )
{

}
/*-----------------------------------------------------------*/

/* configUSE_STATIC_ALLOCATION is set to 1, so the application must provide an
implementation of vApplicationGetIdleTaskMemory() to provide the memory that is
used by the Idle task. */
void vApplicationGetIdleTaskMemory( StaticTask_t **ppxIdleTaskTCBBuffer, StackType_t **ppxIdleTaskStackBuffer, uint32_t *pulIdleTaskStackSize )
{
/* If the buffers to be provided to the Idle task are declared inside this
function then they must be declared static - otherwise they will be allocated on
the stack and so not exists after this function exits. */
GDMPSRAM_BSS static StaticTask_t xIdleTaskTCB;
GDMPSRAM_BSS static StackType_t uxIdleTaskStack[ configMINIMAL_STACK_SIZE ];

    /* Pass out a pointer to the StaticTask_t structure in which the Idle task's
    state will be stored. */
    *ppxIdleTaskTCBBuffer = &xIdleTaskTCB;

    /* Pass out the array that will be used as the Idle task's stack. */
    *ppxIdleTaskStackBuffer = uxIdleTaskStack;

    /* Pass out the size of the array pointed to by *ppxIdleTaskStackBuffer.
    Note that, as the array is necessarily of type StackType_t,
    configMINIMAL_STACK_SIZE is specified in words, not bytes. */
    *pulIdleTaskStackSize = configMINIMAL_STACK_SIZE;
}
/*-----------------------------------------------------------*/

/* configUSE_STATIC_ALLOCATION and configUSE_TIMERS are both set to 1, so the
application must provide an implementation of vApplicationGetTimerTaskMemory()
to provide the memory that is used by the Timer service task. */
void vApplicationGetTimerTaskMemory( StaticTask_t **ppxTimerTaskTCBBuffer, StackType_t **ppxTimerTaskStackBuffer, uint32_t *pulTimerTaskStackSize )
{
/* If the buffers to be provided to the Timer task are declared inside this
function then they must be declared static - otherwise they will be allocated on
the stack and so not exists after this function exits. */
GDMPSRAM_BSS static StaticTask_t xTimerTaskTCB;
GDMPSRAM_BSS static StackType_t uxTimerTaskStack[ configTIMER_TASK_STACK_DEPTH ];

    /* Pass out a pointer to the StaticTask_t structure in which the Timer
    task's state will be stored. */
    *ppxTimerTaskTCBBuffer = &xTimerTaskTCB;

    /* Pass out the array that will be used as the Timer task's stack. */
    *ppxTimerTaskStackBuffer = uxTimerTaskStack;

    /* Pass out the size of the array pointed to by *ppxTimerTaskStackBuffer.
    Note that, as the array is necessarily of type StackType_t,
    configMINIMAL_STACK_SIZE is specified in words, not bytes. */
    *pulTimerTaskStackSize = configTIMER_TASK_STACK_DEPTH;
}
/*-----------------------------------------------------------*/

void vPreSleepProcessing( unsigned long uxModifiableIdleTime )
{
    /* Called by the kernel before it places the MCU into a sleep mode because
    configPRE_SLEEP_PROCESSING() is #defined to vPreSleepProcessing().

    NOTE:  Additional actions can be taken here to get the power consumption
    even lower.  For example, peripherals can be turned off here, and then back
    on again in the post sleep processing function.  For maximum power saving
    ensure all unused pins are in their lowest power state. */

    /* Avoid compiler warnings about the unused parameter. */
    ( void ) uxModifiableIdleTime;
}
/*-----------------------------------------------------------*/

void vPostSleepProcessing( unsigned long uxModifiableIdleTime )
{
    /* Called by the kernel when the MCU exits a sleep mode because
    configPOST_SLEEP_PROCESSING is #defined to vPostSleepProcessing(). */

    /* Avoid compiler warnings about the unused parameter. */
    ( void ) uxModifiableIdleTime;
}
