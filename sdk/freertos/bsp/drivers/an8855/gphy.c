#include <platform.h>
#include <util.h>
#include <stdint.h>

#define WIDTH1_MSK      (0x1)
#define WIDTH2_MSK      (0x3)
#define WIDTH3_MSK      (0x7)
#define WIDTH4_MSK      (0xF)
#define WIDTH5_MSK      (0x1F)
#define WIDTH6_MSK      (0x3F)
#define WIDTH7_MSK      (0x7F)
#define WIDTH8_MSK      (0xFF)
#define WIDTH16_MSK     (0xFFFF)

#define PB_PHY_REG_BASE             (0xA0000000)
#define PB_PHY_ACCESS_BIT           (0x00800000)
#define PB_PHY_ADDR_OFS             (24)
#define PB_PHY_ADDR_MSK             (WIDTH5_MSK)
#define PB_PHY_ADDR_SEL(phyAddr)    ((phyAddr & PB_PHY_ADDR_MSK) << PB_PHY_ADDR_OFS)
#define PB_PHY_PAGE_OFS             (12)
#define PB_PHY_PAGE_MSK             (WIDTH4_MSK)
#define PB_PHY_PAGE_SEL(page)       ((page & PB_PHY_PAGE_MSK) << PB_PHY_PAGE_OFS)
#define PB_CL22_MAIN_PAGE           (0x00000000)
#define PB_PHY_CL22_REG_OFS         (4)
#define PB_PHY_CL22_REG_MSK         (WIDTH5_MSK)
#define PB_PHY_REG_SEL(reg)         ((reg & WIDTH5_MSK) << PB_PHY_CL22_REG_OFS)

/* CL45 */
#define PB_PHY_CL45_DEV_OFS         (18)
#define PB_PHY_CL45_DEV_MSK         (WIDTH5_MSK)
#define PB_PHY_CL45_DEV_SEL(dev)    ((dev & PB_PHY_CL45_DEV_MSK) << PB_PHY_CL45_DEV_OFS)
#define PB_PHY_CL45_REG_OFS         (2)
#define PB_PHY_CL45_REG_MSK         (WIDTH16_MSK)
#define PB_PHY_CL45_REG_SEL(reg)    ((reg & PB_PHY_CL45_REG_MSK) << PB_PHY_CL45_REG_OFS)

#define PB_PHY_PAGE_0000            (0)
#define PB_PHY_PAGE_52B5            (2)

#define dbg_flag_sel_08b__module_ad_adc_a       (0x08b)
#define RND_MASK                                (0xf)
#define PHY_ID_0                                (0)
#define DEVID_03                                (0x03)
#define DEVID_07                                (0x07)
#define DEVID_1E                                (0x1E)
#define DEVID_1F                                (0x1F)

typedef union
{
    struct
    {
        uint16_t rsv_0                                      :1;     /* 0 */
        uint16_t data_addr                                  :6;     /* 1:6 */
        uint16_t node_addr                                  :4;     /* 7:10 */
        uint16_t channel_addr                               :2;     /* 11:12 */
        uint16_t wr_rd_ctrl                                 :1;     /* 13 */
        uint16_t rsv_14                                     :1;     /* 14 */
        uint16_t pkt_xmt_sta                                :1;     /* 15 */
    } DataBitField;
    uint16_t DATA;
} gephy_all_REG_Trreg10h;

typedef union
{
    struct
    {
        uint16_t rg_dbg_flag_0_sel                          :9;     /* 0:8 */
        uint16_t rsv_9                                      :3;     /* 9:11 */
        uint16_t rg_dbg_flag_0_en                           :1;     /* 12 */
        uint16_t rg_dbg_port_sel                            :3;     /* 13:15 */
    } DataBitField;
    uint16_t DATA;
} gephy_all_REG_Dev1Fh_reg015h;

typedef union
{
    struct
    {
        uint16_t power_down_adcck_d                         :1;     /* 0 */
        uint16_t power_down_adcck_c                         :1;     /* 1 */
        uint16_t power_down_adcck_b                         :1;     /* 2 */
        uint16_t power_down_adcck_a                         :1;     /* 3 */
        uint16_t power_down_adc_d                           :1;     /* 4 */
        uint16_t power_down_adc_c                           :1;     /* 5 */
        uint16_t power_down_adc_b                           :1;     /* 6 */
        uint16_t power_down_adc_a                           :1;     /* 7 */
        uint16_t bypass_pd_adcck_d                          :1;     /* 8 */
        uint16_t bypass_pd_adcck_c                          :1;     /* 9 */
        uint16_t bypass_pd_adcck_b                          :1;     /* 10 */
        uint16_t bypass_pd_adcck_a                          :1;     /* 11 */
        uint16_t bypass_pd_adc_d                            :1;     /* 12 */
        uint16_t bypass_pd_adc_c                            :1;     /* 13 */
        uint16_t bypass_pd_adc_b                            :1;     /* 14 */
        uint16_t bypass_pd_adc_a                            :1;     /* 15 */
    } DataBitField;
    uint16_t DATA;
} gephy_all_REG_Dev1Eh_reg03Ch;

typedef union
{
    struct
    {
        uint32_t gain_swap_disable_d_force_val              :1;     /* 0 */
        uint32_t gain_swap_disable_c_force_val              :1;     /* 1 */
        uint32_t gain_swap_disable_b_force_val              :1;     /* 2 */
        uint32_t gain_swap_disable_a_force_val              :1;     /* 3 */
        uint32_t gain_swap_disable_d_force                  :1;     /* 4 */
        uint32_t gain_swap_disable_c_force                  :1;     /* 5 */
        uint32_t gain_swap_disable_b_force                  :1;     /* 6 */
        uint32_t gain_swap_disable_a_force                  :1;     /* 7 */
        uint32_t vga_start_up_gain                          :5;     /* 8:12 */
        uint32_t vga_a_freeze_force                         :1;     /* 13 */
        uint32_t vga_a_freeze_force_val                     :1;     /* 14 */
        uint32_t vga_state_a_force                          :1;     /* 15 */
        uint32_t vga_state_a_force_val                      :5;     /* 16:20 */
        uint32_t rsv_21                                     :11;    /* 21:31 */
    } DataBitField;
    uint32_t DATA;
} gephy_all_REG_PMA_10h;

typedef union
{
    struct
    {
        uint16_t rsv_0                                  :6;     /* 0:5 */
        uint16_t mrForceSpeedSelInt_1                   :1;     /* 6 */
        uint16_t mrCollisionTestEn                      :1;     /* 7 */
        uint16_t mrDuplexModeInt                        :1;     /* 8 */
        uint16_t mrRestartAutoneg                       :1;     /* 9 */
        uint16_t isolateInt                             :1;     /* 10 */
        uint16_t pwdn                                   :1;     /* 11 */
        uint16_t mrAutonegEnableSMIInt                  :1;     /* 12 */
        uint16_t mrForceSpeedSelInt_0                   :1;     /* 13 */
        uint16_t loopback                               :1;     /* 14 */
        uint16_t mainReset                              :1;     /* 15 */
    } DataBitField;
    uint16_t DATA;
} gephy_all_REG_reg00h;

void cl22_write(const uint8_t phyAddr, const uint8_t pageSel, const uint8_t regSel, const uint16_t wdata)
{
    uint32_t pbusAddr = (PB_PHY_REG_BASE | PB_PHY_ACCESS_BIT
                    | PB_PHY_ADDR_SEL(phyAddr) | PB_CL22_MAIN_PAGE
                    | PB_PHY_PAGE_SEL(pageSel) | PB_PHY_REG_SEL(regSel));

    io_write16(pbusAddr, wdata);
}

uint16_t cl22_read(const uint8_t phyAddr, const uint8_t pageSel, const uint8_t regSel)
{
    uint32_t pbusAddr = (PB_PHY_REG_BASE | PB_PHY_ACCESS_BIT
                    | PB_PHY_ADDR_SEL(phyAddr) | PB_CL22_MAIN_PAGE
                    | PB_PHY_PAGE_SEL(pageSel) | PB_PHY_REG_SEL(regSel));

    return io_read16(pbusAddr);
}

void cl45_write(
    const uint8_t phyAddr,
    const uint8_t devID,
    const uint16_t regSel,
    const uint16_t wdata)
{
    uint32_t pbusAddr = (PB_PHY_REG_BASE | PB_PHY_ACCESS_BIT
                    |    PB_PHY_ADDR_SEL(phyAddr) | PB_PHY_CL45_DEV_SEL(devID)
                    |    PB_PHY_CL45_REG_SEL(regSel));

    io_write16(pbusAddr, wdata);
}

uint16_t cl45_read(const uint8_t phyAddr, const uint8_t devID, const uint16_t regSel)
{
    uint32_t pbusAddr = (PB_PHY_REG_BASE | PB_PHY_ACCESS_BIT
                    | PB_PHY_ADDR_SEL(phyAddr) | PB_PHY_CL45_DEV_SEL(devID)
                    | PB_PHY_CL45_REG_SEL(regSel));

    return io_read16(pbusAddr);
}

void tkrg_wait_ready(const uint8_t phyAddr)
{
    gephy_all_REG_Trreg10h trrg;
    trrg.DATA = 0;
    uint8_t retry = 0x7F;

    do
    {
        trrg.DATA = cl22_read(phyAddr, PB_PHY_PAGE_52B5, 0x10);
        retry--;
    } while ((trrg.DataBitField.pkt_xmt_sta == 0) && (retry > 0));
}

void tkrg_write(const uint8_t phyAddr, const uint8_t channel_addr, const uint8_t node_addr, const uint8_t data_addr, const uint32_t wdata)
{
    gephy_all_REG_Trreg10h trrg;
    trrg.DATA = 0;
    uint16_t l_data = (uint16_t) (wdata);
    uint16_t h_data = (uint16_t) (wdata >> 16);

    trrg.DataBitField.data_addr = data_addr;
    trrg.DataBitField.node_addr = node_addr;
    trrg.DataBitField.channel_addr = channel_addr;
    trrg.DataBitField.wr_rd_ctrl = 0;
    trrg.DataBitField.pkt_xmt_sta = 1;

    cl22_write(phyAddr, PB_PHY_PAGE_52B5, 0x11, l_data);
    cl22_write(phyAddr, PB_PHY_PAGE_52B5, 0x12, h_data);
    cl22_write(phyAddr, PB_PHY_PAGE_52B5, 0x10, trrg.DATA);
}

uint32_t tkrg_read(const uint8_t phyAddr, const uint8_t channel_addr, const uint8_t node_addr, const uint8_t data_addr)
{
    gephy_all_REG_Trreg10h trrg;
    trrg.DATA = 0;
    uint32_t data = 0;

    trrg.DataBitField.data_addr = data_addr;
    trrg.DataBitField.node_addr = node_addr;
    trrg.DataBitField.channel_addr = channel_addr;
    trrg.DataBitField.wr_rd_ctrl = 1;
    trrg.DataBitField.pkt_xmt_sta = 1;

    cl22_write(phyAddr, PB_PHY_PAGE_52B5, 0x10, trrg.DATA);
    tkrg_wait_ready(phyAddr);

    data = ((uint32_t) cl22_read(phyAddr, PB_PHY_PAGE_52B5, 0x11));
    data |= ((uint32_t) cl22_read(phyAddr, PB_PHY_PAGE_52B5, 0x12)) << 16;

    return data;
}

uint32_t get_random(uint8_t phy_addr)
{
    gephy_all_REG_Dev1Fh_reg015h dbg_flag_0_ctrl;
    dbg_flag_0_ctrl.DATA = 0;
    gephy_all_REG_Dev1Eh_reg03Ch ana_bypass_pd_adc_ctrl;
    ana_bypass_pd_adc_ctrl.DATA = 0;
    gephy_all_REG_PMA_10h pma_10;
    pma_10.DATA = 0;
    uint32_t dbg_flag_0_ctrl_backup = 0;
    uint32_t ana_bypass_pd_adc_ctrl_backup = 0;
    uint32_t pma_10_backup = 0;
    int count = 0;
    uint32_t random = 0;
    uint32_t tmp = 0;
    gephy_all_REG_reg00h reg;
    reg.DATA = 0;

    reg.DATA = cl22_read(phy_addr, PB_PHY_PAGE_0000, 0);
    reg.DataBitField.pwdn = 0;
    cl22_write(phy_addr, PB_PHY_PAGE_0000, 0, reg.DATA);

    ana_bypass_pd_adc_ctrl_backup = cl45_read(phy_addr, DEVID_1E, 0x3C);
    dbg_flag_0_ctrl_backup = cl45_read(phy_addr, DEVID_1F, 0x15);
    pma_10_backup = tkrg_read(phy_addr, 0x1, 0xF, 0x10);

    ana_bypass_pd_adc_ctrl.DataBitField.bypass_pd_adc_a = 0x1;
    ana_bypass_pd_adc_ctrl.DataBitField.bypass_pd_adcck_a = 0x1;
    cl45_write(phy_addr, DEVID_1E, 0x3C, ana_bypass_pd_adc_ctrl.DATA);

    dbg_flag_0_ctrl.DataBitField.rg_dbg_port_sel = phy_addr;
    dbg_flag_0_ctrl.DataBitField.rg_dbg_flag_0_en = 1;
    dbg_flag_0_ctrl.DataBitField.rg_dbg_flag_0_sel = dbg_flag_sel_08b__module_ad_adc_a;
    cl45_write(phy_addr, DEVID_1F, 0x15, dbg_flag_0_ctrl.DATA);

    pma_10.DATA = pma_10_backup;
    pma_10.DataBitField.vga_state_a_force_val = 0xF;
    pma_10.DataBitField.vga_state_a_force = 0x1;

    tkrg_write(phy_addr, 0x1, 0xF, 0x10, pma_10.DATA);

    random = 0;
    for (count = 0; count < 32; ++count)
    {
        tmp = cl45_read(phy_addr, DEVID_1F, 0x1A);
        random |= ((tmp & 0x1) << count);
    }

    tkrg_write(phy_addr, 0x1, 0xF, 0x10, pma_10_backup);
    cl45_write(phy_addr, DEVID_1F, 0x15, dbg_flag_0_ctrl_backup);
    cl45_write(phy_addr, DEVID_1E, 0x3C, ana_bypass_pd_adc_ctrl_backup);

    reg.DataBitField.pwdn = 1;
    cl22_write(phy_addr, PB_PHY_PAGE_0000, 0, reg.DATA);

    return random;
}


