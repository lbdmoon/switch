/*******************************************************************************
*  Copyright Statement:
*  --------------------
*  This software is protected by Copyright and the information contained
*  herein is confidential. The software may not be copied and the information
*  contained herein may not be used or disclosed except with the written
*  permission of Airoha Technology Corp. (C) 2021
*
*  BY OPENING THIS FILE, BUYER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
*  THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("AIROHA SOFTWARE")
*  RECEIVED FROM AIROHA AND/OR ITS REPRESENTATIVES ARE PROVIDED TO BUYER ON
*  AN "AS-IS" BASIS ONLY. AIROHA EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
*  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
*  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
*  NEITHER DOES AIROHA PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
*  SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
*  SUPPLIED WITH THE AIROHA SOFTWARE, AND BUYER AGREES TO LOOK ONLY TO SUCH
*  THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. AIROHA SHALL ALSO
*  NOT BE RESPONSIBLE FOR ANY AIROHA SOFTWARE RELEASES MADE TO BUYER'S
*  SPECIFICATION OR TO CONFORM TO A PARTICULAR STANDARD OR OPEN FORUM.
*
*  BUYER'S SOLE AND EXCLUSIVE REMEDY AND AIROHA'S ENTIRE AND CUMULATIVE
*  LIABILITY WITH RESPECT TO THE AIROHA SOFTWARE RELEASED HEREUNDER WILL BE,
*  AT AIROHA'S OPTION, TO REVISE OR REPLACE THE AIROHA SOFTWARE AT ISSUE,
*  OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY BUYER TO
*  AIROHA FOR SUCH AIROHA SOFTWARE AT ISSUE.
*
*  THE TRANSACTION CONTEMPLATED HEREUNDER SHALL BE CONSTRUED IN ACCORDANCE
*  WITH THE LAWS OF THE STATE OF CALIFORNIA, USA, EXCLUDING ITS CONFLICT OF
*  LAWS PRINCIPLES.  ANY DISPUTES, CONTROVERSIES OR CLAIMS ARISING THEREOF AND
*  RELATED THERETO SHALL BE SETTLED BY ARBITRATION IN SAN FRANCISCO, CA, UNDER
*  THE RULES OF THE INTERNATIONAL CHAMBER OF COMMERCE (ICC).
*
*******************************************************************************/

/* FILE NAME:  air_gpio.c
 * PURPOSE:
 *    It provide GPIO module API.
 *
 * NOTES:
 *
 */

/* INCLUDE FILE DECLARATIONS
 */

#include <air_gpio.h>
#ifdef AIR_8851_SUPPORT
#include <hal_sco_gpio.h>
#endif
#ifdef AIR_8855_SUPPORT
#include <hal_pearl_gpio.h>
#endif

/* DIAG_SET_MODULE_INFO(AIR_MODULE_GPIO, "air_gpio.c"); */
/* EXPORTED SUBPROGRAM BODIES
*/

/* FUNCTION NAME: air_gpio_getDirection
 * PURPOSE:
 *      Get direction of specific GPIO pin.
 *
 * INPUT:
 *      pin             --  Select GPIO pin number
 *
 * OUTPUT:
 *      ptr_dir         --  GPIO direction
 *                          0: input mode
 *                          1: output mode
 *
 * RETURN:
 *      E_OK
 *      E_ENTRY_NOT_FOUND
 *
 * NOTES:
 *      None
 */
int
air_gpio_getDirection(
    const unsigned char pin,
    unsigned char *ptr_dir)
{
    int rc;

#ifdef AIR_8851_SUPPORT
    rc = hal_sco_gpio_getDirection(pin, ptr_dir);
#endif
#ifdef AIR_8855_SUPPORT
    rc = hal_pearl_gpio_getDirection(pin, ptr_dir);
#endif
    return rc;
}

/* FUNCTION NAME: air_gpio_setDirection
 * PURPOSE:
 *      Set direction of specific GPIO pin.
 *
 * INPUT:
 *      pin             --  Select GPIO pin number
 *      dir             --  GPIO direction
 *                          0: input mode
 *                          1: output mode
 *
 * OUTPUT:
 *      None
 *
 * RETURN:
 *      E_OK
 *      E_ENTRY_NOT_FOUND
 *
 * NOTES:
 *      None
 */
int
air_gpio_setDirection(
    const unsigned char pin,
    const unsigned char dir)
{
    int rc;

#ifdef AIR_8851_SUPPORT
    rc = hal_sco_gpio_setDirection(pin, dir);
#endif
#ifdef AIR_8855_SUPPORT
    rc = hal_pearl_gpio_setDirection(pin, dir);
#endif
    return rc;
}

/* FUNCTION NAME: air_gpio_getOutputEnable
 * PURPOSE:
 *      Get output enable state of specific GPIO pin.
 *
 * INPUT:
 *      gpio_pin        --  GPIO pin index
 *
 * OUTPUT:
 *      ptr_mode        --  GPIO output enable mode.
 *                          TRUE : enable
 *                          FALSE: disable in Hi-Z state;
 *
 * RETURN:
 *      E_OK
 *
 * NOTES:
 *      None
 */
int
air_gpio_getOutputEnable(
    const unsigned char pin,
    int *ptr_mode)
{
    int rc;

#ifdef AIR_8851_SUPPORT
    rc = hal_sco_gpio_getOutputEnable(pin, ptr_mode);
#endif
#ifdef AIR_8855_SUPPORT
    rc = hal_pearl_gpio_getOutputEnable(pin, ptr_mode);
#endif
    return rc;
}

/* FUNCTION NAME: air_gpio_setOutputEnable
 * PURPOSE:
 *      Set output enable mode of specific GPIO pin when it is set to output mode.
 *
 * INPUT:
 *      gpio_pin        --  GPIO pin index
 *      mode            --  GPIO output enable mode.
 *                          TRUE : enable
 *                          FALSE: disable in Hi-Z state;
 *
 * OUTPUT:
 *      None
 *
 * RETURN:
 *      E_OK
 *
 * NOTES:
 *      None
 */
int
air_gpio_setOutputEnable(
    const unsigned char pin,
    const int mode)
{
    int rc;

#ifdef AIR_8851_SUPPORT
    rc = hal_sco_gpio_setOutputEnable(pin, mode);
#endif
#ifdef AIR_8855_SUPPORT
    rc = hal_pearl_gpio_setOutputEnable(pin, mode);
#endif
    return rc;
}

/* FUNCTION NAME: air_gpio_getValue
 * PURPOSE:
 *      Get input/output current value of specific GPIO pin.
 *
 * INPUT:
 *      gpio_pin        --  GPIO pin
 *
 * OUTPUT:
 *      ptr_value       --  current input/output value
 *                          input mode:
 *                               FALSE: The current state is low
 *                                TRUE: The current state is high
 *                          output mode:
 *                               FALSE: Drive low
 *                                TRUE: Drive high
 *
 * RETURN:
 *      E_OK
 *
 * NOTES:
 *      None
 */
int
air_gpio_getValue(
    const unsigned char pin,
    int *ptr_value)
{
    int rc;

#ifdef AIR_8851_SUPPORT
    rc = hal_sco_gpio_getValue(pin, ptr_value);
#endif
#ifdef AIR_8855_SUPPORT
    rc = hal_pearl_gpio_getValue(pin, ptr_value);
#endif
    return rc;
}

/* FUNCTION NAME: air_gpio_setValue
 * PURPOSE:
 *      Set output value of specific GPIO pin.
 *
 * INPUT:
 *      gpio_pin        --  GPIO pin
 *      value           --  output value
 *                               FALSE: Drive low
 *                                TRUE: Drive high
 *
 * OUTPUT:
 *      None
 *
 * RETURN:
 *      E_OK
 *
 * NOTES:
 *      None
 */
int
air_gpio_setValue(
    const unsigned char pin,
    const int value)
{
    int rc;

#ifdef AIR_8851_SUPPORT
    rc = hal_sco_gpio_setValue(pin, value);
#endif
#ifdef AIR_8855_SUPPORT
    rc = hal_pearl_gpio_setValue(pin, value);
#endif
    return rc;
}

/* FUNCTION NAME: air_gpio_getInterruptEdgeDetectMode
 * PURPOSE:
 *      Get interrupt edge detect mode settings of specific GPIO pin.
 *
 * INPUT:
 *      gpio_pin        --  GPIO pin index
 *
 * OUTPUT:
 *      ptr_mode   --  Edge triggger mode.
 *                          0: Disable edge trigger;
 *                          1: Rising edge, interrupt triggered when GPIO pin toggles from low to high
 *                          2: Falling edge, interrupt triggered when GPIO pin toggles from high to low
 *                          3: Both, interrupt triggered when GPIO pin toggles
 *
 * RETURN:
 *      E_OK
 *      E_ENTRY_NOT_FOUND
 *
 * NOTES:
 *      None
 */
int
air_gpio_getInterruptEdgeDetectMode(
    const unsigned char pin,
    unsigned char *ptr_mode)
{
    int rc;

#ifdef AIR_8851_SUPPORT
    rc = hal_sco_gpio_getInterruptEdgeDetectMode(pin, ptr_mode);
#endif
#ifdef AIR_8855_SUPPORT
    rc = hal_pearl_gpio_getInterruptEdgeDetectMode(pin, ptr_mode);
#endif
    return rc;
}

/* FUNCTION NAME: air_gpio_setInterruptEdgeDetectMode
 * PURPOSE:
 *      Set interrupt edge detect mode settings of specific GPIO pin.
 *
 * INPUT:
 *      gpio_pin        --  GPIO pin index
 *      trig_mode       --  Edge triggger mode.
 *                          0: Disable edge trigger;
 *                          1: Rising edge, interrupt triggered when GPIO pin toggles from low to high
 *                          2: Falling edge, interrupt triggered when GPIO pin toggles from high to low
 *                          3: Both, interrupt triggered when GPIO pin toggles
 *
 * OUTPUT:
 *      None
 *
 * RETURN:
 *      E_OK
 *      E_ENTRY_NOT_FOUND
 *
 * NOTES:
 *      None
 */
int
air_gpio_setInterruptEdgeDetectMode(
    const unsigned char pin,
    const unsigned char mode)
{
    int rc;

#ifdef AIR_8851_SUPPORT
    rc = hal_sco_gpio_setInterruptEdgeDetectMode(pin, mode);
#endif
#ifdef AIR_8855_SUPPORT
    rc = hal_pearl_gpio_setInterruptEdgeDetectMode(pin, mode);
#endif
    return rc;
}

/* FUNCTION NAME: air_gpio_getInterruptLevelDetectMode
 * PURPOSE:
 *      Get interrupt level detect mode settings of specific GPIO pin.
 *
 * INPUT:
 *      gpio_pin        --  GPIO pin index
 *
 * OUTPUT:
 *      ptr_mode        --  Level triggger mode.
 *                          0: Disable level trigger;
 *                          1: High level, interrupt triggered when GPIO pin is high
 *                          2: Low level, interrupt triggered when GPIO pin is low
 *
 * RETURN:
 *      E_OK
 *      E_ENTRY_NOT_FOUND
 *
 * NOTES:
 *      None
 */
int
air_gpio_getInterruptLevelDetectMode(
    const unsigned char pin,
    unsigned char *ptr_mode)
{
    int rc;

#ifdef AIR_8851_SUPPORT
    rc = hal_sco_gpio_getInterruptLevelDetectMode(pin, ptr_mode);
#endif
#ifdef AIR_8855_SUPPORT
    rc = hal_pearl_gpio_getInterruptLevelDetectMode(pin, ptr_mode);
#endif
    return rc;
}

/* FUNCTION NAME: air_gpio_setInterruptLevelDetectMode
 * PURPOSE:
 *      Set interrupt level detect mode settings of specific GPIO pin.
 *
 * INPUT:
 *      gpio_pin        --  GPIO pin index
 *      trig_mode       --  Level triggger mode.
 *                          0: Disable edge trigger;
 *                          1: High level, interrupt triggered when GPIO pin is high
 *                          2: Low level, interrupt triggered when GPIO pin is low
 *
 * OUTPUT:
 *      None
 *
 * RETURN:
 *      E_OK
 *      E_ENTRY_NOT_FOUND
 *
 * NOTES:
 *      None
 */
int
air_gpio_setInterruptLevelDetectMode(
    const unsigned char pin,
    const unsigned char mode)
{
    int rc;

#ifdef AIR_8851_SUPPORT
    rc = hal_sco_gpio_setInterruptLevelDetectMode(pin, mode);
#endif
#ifdef AIR_8855_SUPPORT
    rc = hal_pearl_gpio_setInterruptLevelDetectMode(pin, mode);
#endif
    return rc;
}

/* FUNCTION NAME: air_gpio_getInterruptStatus
 * PURPOSE:
 *      Get interrupt status of specific GPIO pin.
 *
 * INPUT:
 *      gpio_pin        --  GPIO pin
 *
 * OUTPUT:
 *      ptr_irs         --  current interrupt status
 *                          FALSE: Interrupt is not asserted
 *                          TRUE : Interrupt is asserted
 *
 * RETURN:
 *      E_OK
 *
 * NOTES:
 *      None
 */
int
air_gpio_getInterruptStatus(
    const unsigned char pin,
    int *ptr_status)
{
    int rc;

#ifdef AIR_8851_SUPPORT
    rc = hal_sco_gpio_getInterruptStatus(pin, ptr_status);
#endif
#ifdef AIR_8855_SUPPORT
    rc = hal_pearl_gpio_getInterruptStatus(pin, ptr_status);
#endif
    return rc;
}

/* FUNCTION NAME: air_gpio_clearInterrupt
 * PURPOSE:
 *      Clear interrupt status of specific GPIO pin.
 *
 * INPUT:
 *      gpio_pin        --  GPIO pin
 *
 * OUTPUT:
 *      None
 *
 * RETURN:
 *      E_OK
 *
 * NOTES:
 *      None
 */
int
air_gpio_clearInterrupt(
    const unsigned char pin)
{
    int rc;

#ifdef AIR_8851_SUPPORT
    rc = hal_sco_gpio_clearInterrupt(pin);
#endif
#ifdef AIR_8855_SUPPORT
    rc = hal_pearl_gpio_clearInterrupt(pin);
#endif
    return rc;
}
