#include <platform.h>
#include <util.h>
#include <cache.h>
#include "FreeRTOS.h"
#include <nds32_intrinsic.h>

#define DBG_ERR     (1<<0)
#define DBG_INFO    (1<<1)

int dbg_print_level=(DBG_ERR|DBG_INFO);

#define dbg_print(level, F, B...) { \
    if(dbg_print_level & level) { \
        printf(F, ##B) ; \
    } \
}

#define ILLACC_WR_READ          (0)
#define ILLACC_WR_WRITE         (1)
#define ILLACC_ID_CPU           (6)

#define NUM_TEST_CASE           (2)
#define NUM_TEST_SEC            (7)

enum access_type {
    acc_type_legal=0,
    acc_type_overram,    
    acc_type_illseg,
    acc_type_max
};
