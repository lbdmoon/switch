
#include <platform.h>
#include <util.h>
#include <cache.h>
#include "FreeRTOS.h"
#include <nds32_intrinsic.h>
#include "task.h"


#define FORCE_TIMEOUT		(CR_RBUS_BASE + 0xbc)

#define DMEM_TEST_ADDR		(DMEM_BASE+S_124K)
#define DMEM_TEST_UNC_ADDR  (DMEM_TEST_ADDR+UNC_ADDR_OFFSET)
#define DMEM_TEST_ADDR2		(DMEM_BASE+S_112K)
#define DMEM_TEST_UNC_ADDR2 (DMEM_TEST_ADDR2+UNC_ADDR_OFFSET)
#define GDMP_SRAM_TEST_UNC_ADDR (GDMPSRAM_UNC_BASE+S_29K)

#define CR_SCREG_WF0        (CR_NP_SCU_BASE+0x10) /* keep value after wdog-reset */
#define CR_SCREG_WF1        (CR_NP_SCU_BASE+0x14) /* keep value after wdog-reset */
#define CR_SCREG_WR0        (CR_NP_SCU_BASE+0x18) /* deafult value after wdog-reset */
#define CR_SCREG_WR1        (CR_NP_SCU_BASE+0x1C) /* deafult value after wdog-reset */

#define CR_SYS_SW_RESET     (CR_NP_SCU_BASE+0xC0)
#define DO_SYS_SW_RESET     (1<<31)

#define PFM_EVENT_CYCLE     (0)
#define PFM_EVENT_INST      (1)

#define LATENCY_TEST_CNT    (10)

#define NUM_RDBYPASS_TYPE   (4)
#define NUM_RDBYPASS_STRESS (3)

#define AXI2RBUS_INFRA_CFG  (CR_CPU_CTRL_INFRA_BASE+0x00)
#define RBUS_PENDING_CNT    (CR_CPU_CTRL_INFRA_BASE+0x10)

#define TIMER1_LVR          (CR_TIMER_BASE+0x0C)
#define TIMER2_LVR          (CR_TIMER_BASE+0x14)

#define CPU_CLKSRC_SEL      (CR_CHIP_SCU_BASE+0xC8)
#define BUS_CLKSRC_SEL      (CR_CHIP_SCU_BASE+0xCC)

/* PFT_CTL */
#define T_LEVEL(N)              ((N) << 4)
#define T_LEVEL_MSK             (0xF << 4)
#define FAST_INT                (1UL << 8)
#define LOWEST_PERF_LEVEL       (15)
#define HIGHEST_PERF_LEVEL      (0)
#define TOTAL_T_LEVELS          (16)
#define T_LEVEL_ITEMS           (TOTAL_T_LEVELS+2)

#define NUM_RBUS_TOUT_TEST      (7)

unsigned char testBytePat[] = {0x5a, 0xa5, 0xff, 0x01};
unsigned int testWordPat[] = {0x5a5a5a5a, 0xa5a5a5a5, 0xff00ff00, 0x00ff00ff, 0x0000ffff};

unsigned int registers_addr[] = {
            DRAM_ILLADDR_START1, DRAM_ILLADDR_END1, RBUS_PENDING_CNT, CR_SCREG_WR0, 
            CR_SCREG_WR1, TIMER1_LVR, TIMER2_LVR
};

volatile unsigned int perfThrottlFlag[3] = {0,0,0};

extern void _btb_test_start(void);

typedef void (*rbus_timeout_test_func)(void);

void rbus_timeout_disable_test(void);
void rbus_timeout_read_trigger_test(void);
void rbus_timeout_write_trigger_test(void);
void rbus_read_timeout_cnt_test(void);
void rbus_write_timeout_cnt_test(void);
void rbus_timeout_interrupt_test(void);
void rbus_timeout_wdog_reboot_test(void);

rbus_timeout_test_func rbus_timeout_tests[NUM_RBUS_TOUT_TEST+1] = 
{
    NULL,
    rbus_timeout_disable_test,
    rbus_timeout_read_trigger_test,
    rbus_timeout_write_trigger_test,
    rbus_read_timeout_cnt_test,
    rbus_write_timeout_cnt_test,
    rbus_timeout_interrupt_test,
    rbus_timeout_wdog_reboot_test
};


enum cacheTestCase
{
    ctc_dc_inv=0,
    ctc_dc_wback_inv,
    ctc_ic_inv,
    ctc_max_case
};

void rbus_tout_isr(void)
{
    printf ("\n%s\n", __func__);
    rbus_timeout_check();
    return;
}

void rbus_timeout_disable_test(void)
{
    unsigned int read_data =0 ;

    printf("\nRbus timeout disable Test\r\n");
    printf("TIMEOUT_STS0 = 0x%x\r\n", io_read32(TIMEOUT_STS0));
    printf("Force bus timeout when rbus timeout is disabled!\n");
    io_write32(FORCE_TIMEOUT, 0);
    printf("Trying to read data from DMEM ... CPU will hang ...\n");
    /* System will hang here */
    read_data = io_read32(DMEM_TEST_ADDR);
    /* this should not be excuted */
    printf("Error! Shouldn't be here!\n");
    return;
}

void rbus_timeout_read_trigger_test(void)
{
    unsigned int read_data =0 ;
    unsigned int testVal = 0x12345678;

    printf("\nRbus timeout trigger by read command Test\r\n");
    io_write32(DMEM_TEST_ADDR, testVal); /* write a value before test */
	io_write32(TIMEOUT_STS0, 0x80000000); /* enable timeout*/
	printf("TIMEOUT_STS0 = 0x%x\n", io_read32(TIMEOUT_STS0));
	printf("Force bus timeout\n");
	io_write32(FORCE_TIMEOUT, 0);
	printf("Read data from readAddr:0x%x (DMEM)\n", DMEM_TEST_ADDR);
	read_data = io_read32(DMEM_TEST_ADDR);

	printf("CPU BUS Test: action: Read\n");
	printf("read_data = 0x%x; rbus timeout status = 0x%x ; errAddr = 0x%x \n",
            read_data, io_read32(TIMEOUT_STS0), io_read32(TIMEOUT_STS1));
    return;
}

void rbus_timeout_write_trigger_test(void)
{
    unsigned int testVal = 0x12345678;

    printf("\nRbus timeout trigger by write command Test\r\n");
	io_write32(TIMEOUT_STS0, 0x80000000); /* enable timeout*/
	printf("TIMEOUT_STS0 = 0x%x\n", io_read32(TIMEOUT_STS0));
	printf("Force bus timeout\n");
	io_write32(FORCE_TIMEOUT, 0);
	printf("Write data to writeAddr:0x%x (DMEM)\n", DMEM_TEST_ADDR);
	io_write32(DMEM_TEST_ADDR, testVal); //write data to DMEM

	printf("CPU BUS Test: action: Write\n");
	printf("rbus timeout status = 0x%x ; errAddr = 0x%x ; TIMEOUT_CFG0 = 0x%x \n",
            io_read32(TIMEOUT_STS0), io_read32(TIMEOUT_STS1),io_read32(TIMEOUT_CFG0));
    return;
}

void rbus_read_timeout_cnt_test(void)
{
	unsigned int addr = 0;
	unsigned int time_config = 0;
	unsigned int time_start = 0;
	unsigned int time_end = 0;

    printf("\nRbus timeout cmd cnt Read Test\r\n");
	io_write32(AXI2RBUS_INFRA_CFG, 0); //set to non bufferable

	for (time_config = 1 ; time_config < 6; time_config ++)
	{
		io_write32(TIMEOUT_CFG0, DMEM_CTRL_CLK * time_config); /* set cmd_timeout_cnt as "time_config" secs */
        io_write32(TIMEOUT_CFG2, DMEM_CTRL_CLK * time_config); /* set rdata_timeout_cnt as "time_config" secs */
		io_write32(TIMEOUT_STS0, 0x80000000); /* enable timeout*/
        addr = DMEM_TEST_ADDR + (0x100 * time_config);
		
		printf("TIMEOUT_CFG0 = 0x%x , readAddr = 0x%x, timer config = %d sec(s)\n", 
                io_read32(TIMEOUT_CFG0), addr, time_config);
		io_write32(FORCE_TIMEOUT, 0);
		printf("force timeout\n");
		time_start = get_cpu_timer_cnt(0);
		addr = io_read32(addr);
		time_end = get_cpu_timer_cnt(0);
		printf("CPU BUS Test: read timeout time\n");
		printf("rbus timeout status = 0x%x ; errAddr = 0x%x ; wait time = %dms\n", 
                io_read32(TIMEOUT_STS0), io_read32(TIMEOUT_STS1), get_msTime_by_cpuTmrTick(time_end - time_start));
	}
    return;
}

void rbus_write_timeout_cnt_test(void)
{
    unsigned int addr = 0;
    unsigned int time_config = 0;
    int i = 0;
    unsigned int time_start = 0;
    unsigned int time_end = 0;
    unsigned int testVal = 0x12345678;

    printf("\nRbus timeout cmd cnt Write Test\r\n");

	io_write32(AXI2RBUS_INFRA_CFG, 0); //set to non bufferable
	for (time_config = 1 ; time_config < 6; time_config ++)
	{
		io_write32(TIMEOUT_CFG0, DMEM_CTRL_CLK * time_config); /* set cmd_timeout_cnt as "time_config" secs */
        io_write32(TIMEOUT_CFG1, DMEM_CTRL_CLK * time_config); /* set wdata_timeout_cnt as "time_config" secs */
		io_write32(TIMEOUT_STS0, 0x80000000);
		addr = DMEM_TEST_ADDR + (0x100 * time_config);			
	
		printf("TIMEOUT_CFG0 = 0x%x , writeAddr = 0x%x, timer config = %d sec(s)\n", io_read32(TIMEOUT_CFG0), addr, time_config);
		io_write32(FORCE_TIMEOUT, 0);
		printf("force timeout\n");
		time_start = get_cpu_timer_cnt(0);
		io_write32(addr, testVal);
		i = 0;
		/* wait write cmd into bus*/
		while(io_read32(TIMEOUT_STS1)!= addr)
		{
			i++;
		}
		time_end = get_cpu_timer_cnt(0);
		printf("CPU BUS Test: write timeout time\n");
		printf("loop %d times; wait time = %dms\nrbus timeout status = 0x%x ; errAddr = 0x%x\n", 
            i, get_msTime_by_cpuTmrTick(time_end - time_start), io_read32(TIMEOUT_STS0), io_read32(TIMEOUT_STS1));
	}
    return;
}

void rbus_timeout_interrupt_test(void)
{
    unsigned int read_data =0 ;
    unsigned int testVal = 0x12345678;

    printf("\nRbus timeout interrupt Test\r\n");
    io_write32(DMEM_TEST_ADDR, testVal); /* write a value before test */
    /* unregister isr before registering it, because it's been registered as "pbus" isr during booting */
    unregister_isr(IRQ_BUS_TOUT);
    register_isr(IRQ_BUS_TOUT, rbus_tout_isr); /* enable interrupt and register ISR for RBUS_Timeout */
	io_write32(TIMEOUT_STS0, 0x80000000); /* enable timeout*/
	printf("TIMEOUT_STS0 = 0x%x\n", io_read32(TIMEOUT_STS0));
	printf("Force bus timeout\n");
	io_write32(FORCE_TIMEOUT, 0);
	printf("Read data from readAddr:0x%x (DMEM)\n", DMEM_TEST_ADDR);
	read_data = io_read32(DMEM_TEST_ADDR);
    delay1ms(100); /* ISR should be issued during delay */
    return;
}

void rbus_timeout_wdog_reboot_test(void)
{
    unsigned int read_data =0 ;

    printf("\nTIMEOUT_STS0:0x%x, TIMEOUT_CFG0:0x%x, TIMEOUT_CFG1:0x%x, TIMEOUT_CFG2:0x%x\r\n", 
            io_read32(TIMEOUT_STS0), io_read32(TIMEOUT_CFG0), io_read32(TIMEOUT_CFG1), io_read32(TIMEOUT_CFG2));
	printf("wdog_init for reset in 3 secs\n");
    wdog_init(1, 3000, 3000);
    printf("Force bus timeout\n");
	io_write32(FORCE_TIMEOUT, 0);
	printf("read data from readAddr:0x%x (DMEM)\n", DMEM_TEST_ADDR);
	read_data = io_read32(DMEM_TEST_ADDR);
	printf("while(1) until wdog reset\n");
    while(1);
}

void rbus_timeout_test(int test_index)
{
	unsigned int read_data =0 ;
	unsigned int reg = 0;
	unsigned int addr = 0;
	unsigned int time_config = 0;
	int i = 0;
	int test =0 ;
	unsigned int time_start = 0;
	unsigned int time_end = 0;
    unsigned int testVal = 0x12345678;


    if (!((1<=test_index)&&(test_index<=NUM_RBUS_TOUT_TEST))) {
        printf("\nError: test_index:%d should be 1~7\n", test_index);
        return;
    }

	/* reset to default before tests (except for test 7) */
    if (test_index != NUM_RBUS_TOUT_TEST) {
    	io_write32(TIMEOUT_STS0, 0);
    	io_write32(TIMEOUT_STS1, 0);
    }

    rbus_timeout_tests[test_index]();
    
    printf("\ntest is done. Reset system now!\n");
	/* do system reset after test finished*/
    io_write32(CR_SYS_SW_RESET, DO_SYS_SW_RESET);
	while(1);
}

void task_switching(unsigned int *accessP, unsigned int *max_accessP, unsigned mask)
{
    #if 0
    TaskStatus_t taskInfo;
    #endif

    (*accessP)++;

    if ((*accessP)==(*max_accessP)) {
        (*accessP)=0;
        (*max_accessP)=(get_timer0_tick()&mask);
        #if 0
        vTaskGetInfo(NULL, &taskInfo, pdTRUE, eInvalid);
        printf("tas%d out\n", taskInfo.xTaskNumber);
        #endif
        taskYIELD();
        #if 0
        vTaskGetInfo(NULL, &taskInfo, pdTRUE, eInvalid);
        printf("task%d in\n", taskInfo.xTaskNumber);
        #endif
    }
    return;
}

int air_rw_ram_test (unsigned long startAddr, unsigned int size, unsigned long patAdd)
{
    int i, j;
    unsigned long tmpAddr, tmpWord;
    unsigned char tmpByte;
    unsigned char patAddB = (unsigned char)(patAdd&0xff);
    unsigned int wordLen = sizeof(unsigned long);
    unsigned int testWordNum = size/wordLen;
    unsigned int wordPatNum = sizeof(testWordPat)/sizeof(testWordPat[0]);
    unsigned int byteLen = 1;
    unsigned int testByteNum = size;
    unsigned int bytePatNum = sizeof(testBytePat)/sizeof(testBytePat[0]);
    unsigned int mask=(S_16K-1);
    unsigned int access=0, max_access=(get_timer0_tick()&mask);

        
    for (j=0; j<wordPatNum; j++) {

        tmpAddr = startAddr;
        tmpWord=testWordPat[j]+patAdd;

        /* CPU writes words to RAM */
        for (i=0; i<testWordNum; i++) {
            ramWrite32(tmpAddr,tmpWord);
            tmpAddr += wordLen;
        }


        tmpAddr = startAddr;
        tmpWord=testWordPat[j]+patAdd;

        /* CPU reads words from RAM and compare */
        for (i=0; i<testWordNum; i++) {
            if (ramRead32(tmpAddr) != tmpWord) {
                printf("\nERROR1 ramRead32(0x%lx):0x%lx != tmpWord:0x%lx at (i,j)==(%d,%d)\n", 
                                    tmpAddr, ramRead32(tmpAddr), tmpWord, i, j);
                return -1;
            }
            tmpAddr += wordLen;
        }

        tmpAddr = startAddr;
        tmpWord=testWordPat[j]+patAdd+1;

        /* CPU writes a word then reads it back and compare */
        for (i=0; i<testWordNum; i++) {
            ramWrite32(tmpAddr,tmpWord);
            if (ramRead32(tmpAddr) != tmpWord) {
                printf("\nERROR2 ramRead32(0x%lx):0x%lx != tmpWord:0x%lx at (i,j)==(%d,%d)\n", 
                                    tmpAddr, ramRead32(tmpAddr), tmpWord, i, j);
                return -1;
            }
            tmpAddr += wordLen;
        }

        task_switching(&access, &max_access, mask);
    }


    for (j=0; j<bytePatNum; j++) {
    
        tmpAddr = startAddr;
        tmpByte=testBytePat[j]+patAddB;

        /* CPU writes bytes to RAM */
        for (i=0; i<testByteNum; i++) {
            ramWrite8(tmpAddr,tmpByte);
            tmpAddr += byteLen;
        }
        
        tmpAddr = startAddr;
        tmpByte=testBytePat[j]+patAddB;

        /* CPU reads bytes from RAM and compare */
        for (i=0; i<testByteNum; i++) {
            if (ramRead8(tmpAddr) != tmpByte) {
                printf("\nERROR3 ramRead8(0x%lx):0x%x != tmpByte:0x%x at (i,j)==(%d,%d)\n", 
                                    tmpAddr, ramRead8(tmpAddr), tmpByte, i, j);
                return -1;
            }
            tmpAddr += byteLen;
        }

        tmpAddr = startAddr;
        tmpByte=testBytePat[j]+patAddB+1;

        /* CPU writes a byte then reads it back and compare */
        for (i=0; i<testByteNum; i++) {
            ramWrite8(tmpAddr,tmpByte);
            if (ramRead8(tmpAddr) != tmpByte) {
                printf("\nERROR4 ramRead8(0x%lx):0x%x != tmpByte:0x%x at (i,j)==(%d,%d)\n", 
                                    tmpAddr, ramRead8(tmpAddr), tmpByte, i, j);
                return -1;
            }
            tmpAddr += byteLen;
        }

        if ((j&0x3)==0)
            task_switching(&access, &max_access, mask);
    }

    return 0;
}

/* isFirstTime==1, just for putting this func in icache.
 * isFirstTime==0, this func will always be in while(1). */
void icache_inv_jump_func(volatile int isFirstTime)
{
    volatile unsigned int i=0,j=0;

   while(1) {
    
        i++;
        if (i==0xffffffff) {
            i=0;
            j++;
        }
        if (j==0xffffffff)
            j=0;
        if (isFirstTime)
            break;
    }
}

int cache_test (int idx)
{
    unsigned int cac_dmem_addr, unc_dmem_addr;
    unsigned int cac_gdmpSram_addr, unc_gdmpSram_addr;
    unsigned int data0, data1;
    unsigned int tmpVal=0x12345678;
    int res = 0;
    unsigned int func_addr;

    cac_dmem_addr = (unsigned int)pvPortMalloc(S_256, air_module_getModuleName(AIR_MODULE_SWC));
    unc_dmem_addr = cac_dmem_addr + UNC_ADDR_OFFSET;
    cac_gdmpSram_addr = GDMPSRAM_BASE;
    unc_gdmpSram_addr = cac_gdmpSram_addr + UNC_ADDR_OFFSET;


    if (idx == ctc_dc_inv) {
        
        printf("dcache_inv test: ");
        /* read data from dmem to cache */
        data0 = ramRead32(cac_dmem_addr);
        /* unc-write data1 to dmem */
        data1 = data0+1;
        ramWrite32(unc_dmem_addr, data1);
        /* read data from cache */
        data0 = ramRead32(cac_dmem_addr);
        if (data0==data1) {
            printf("failed due to data0:0x%x == data1:0x%x at addr:0x%x\n", data0, data1, cac_dmem_addr);
            res=-1;
            goto cache_test_out;
        }
        /* invalidate dcache */
        air_dcache_inv(cac_dmem_addr, sizeof(data0));
        /* read data from dmem to cache */
        data0 = ramRead32(cac_dmem_addr);
        if (data0!=data1) {
            printf("failed due to data0:0x%x != data1:0x%x at addr:0x%x\n", data0, data1, cac_dmem_addr);
            res=-1;
            goto cache_test_out;
        }
    }
    else if (idx == ctc_dc_wback_inv) {
        
        printf("dcache_wback_inv test: ");
        /* write to cache before test, in case of cache dirty */
        ramWrite32(cac_dmem_addr, 0);
        /* unc-write dmem before test */
        ramWrite32(unc_dmem_addr, 0);
        /* write data to cache */
        data0 = tmpVal;
        ramWrite32(cac_dmem_addr, data0);
        /* unc-read data from dmem */
        data1 = ramRead32(unc_dmem_addr);
        if (data0==data1) {
            printf("failed due to data0:0x%x == data1:0x%x at addr:0x%x\n", data0, data1, cac_dmem_addr);
            res=-1;
            goto cache_test_out;
        }
        /* writeback & invalidate dcache */
        air_dcache_wback_inv(cac_dmem_addr, sizeof(data0));
        /* unc-read data from dmem (for wback test) */
        data1 = ramRead32(unc_dmem_addr);
        if (data0!=data1) {
            printf("(wback)failed due to data0:0x%x != data1:0x%x at addr:0x%x\n", data0, data1, cac_dmem_addr);
            res=-1;
            goto cache_test_out;
        }
        /* unc-write data to dmem */
        data1 = tmpVal+1;
        ramWrite32(unc_dmem_addr, data1);
        /* read data from dmem to cache (for inv test) */
        data0 = ramRead32(cac_dmem_addr);
        if (data0!=data1) {
            printf("(inv)failed due to data0:0x%x != data1:0x%x at addr:0x%x\n", data0, data1, cac_dmem_addr);
            res=-1;
            goto cache_test_out;
        }
    }
    else if (idx == ctc_ic_inv) {
        printf("icache invalidate test: ");
        func_addr = icache_inv_jump_func;
        printf("func_addr:0x%x\n", func_addr);
        /* AICE will intervene the test, starting here */
        while (io_read32(CR_SCREG_WR0)==0) ;
        icache_inv_jump_func(1);
        air_icache_inv(func_addr, 64);
        icache_inv_jump_func(0);
    }
    else {
        printf("Error: Wrong cache case:%d\n", idx);
    }

    printf(" pass\n");

cache_test_out:
    vPortFree((void*)cac_dmem_addr);

    return res;
}

unsigned int cpu_cycles_calculate
    (unsigned int* cnt1_p, unsigned int* cnt2_p, unsigned int testRound)
{
    unsigned int totalCpuCounts=0, diff;
    int i;

    for (i=0; i<testRound; i++) {

        if (i==0) /* the 1st result may be too large due to icache miss, so skip it */
            continue;
        
        if (cnt2_p[i] > cnt1_p[i])
            diff = (cnt2_p[i] - cnt1_p[i]);
        else
            diff = (cnt1_p[i] - cnt2_p[i]);
        
        totalCpuCounts+=diff;

        //printf("cnt1:0x%lx  cnt2:0x%lx  diff:0x%lx\n", cnt1_p[i], cnt2_p[i], diff);
    }

    testRound--; /* due to 1st result being skipped */

    return (totalCpuCounts/testRound);
}


int cpu_cycles_test (unsigned int addr, int action)
{
    volatile unsigned int cycle1[LATENCY_TEST_CNT], cycle2[LATENCY_TEST_CNT];
    unsigned int val;
    int i;

    startPFM(0, PFM_EVENT_CYCLE);

    __nds32__gie_dis(); /* disable global interrupt */

    if (action==0) /* base value */
    {
        printf("\nlantency base test\n");
        for (i=0; i<LATENCY_TEST_CNT; i++) {
            cycle1[i] = get_pfm_cnt0;
            cycle2[i] = get_pfm_cnt0;
        }
    }
    else if (action==1) /* read lantency */
    {
        printf("\nCPU read lantency test for addr:0x%x\n", addr);
        for (i=0; i<LATENCY_TEST_CNT; i++) {
            cycle1[i] = get_pfm_cnt0;
            val = VPint(addr);
            cycle2[i] = get_pfm_cnt0;
        }
    }
    else /* write lantency */
    {
        printf("\nCPU write lantency test for addr:0x%x\n", addr);
        for (i=0; i<LATENCY_TEST_CNT; i++) {
            cycle1[i] = get_pfm_cnt0;
            VPint(addr) = 0x12345678;
            cycle2[i] = get_pfm_cnt0;
        }
    }

    __nds32__gie_en(); /* enable global interrupt */

    stopPFM(0);

    printf("Average CPU Cycles: 0x%x\n", cpu_cycles_calculate(cycle1, cycle2, LATENCY_TEST_CNT));
    return 0;
}

void set_rbus_pending_high8bits_addr(unsigned int addr)
{
    unsigned int val;

    val = io_read32(AXI2RBUS_INFRA_CFG);
    val &= (~(0xff<<2));
    val |= (((addr>>24)&0xff)<<2);
    io_write32(AXI2RBUS_INFRA_CFG, val);
    return;
}

void enable_rbus_pending(int enable)
{
    unsigned int val;

    val = io_read32(AXI2RBUS_INFRA_CFG);
    val &= (~(0x1<<1));
    if (enable)
        val |= (0x1<<1);
    io_write32(AXI2RBUS_INFRA_CFG, val);
    return;
}

void set_rbus_pending_count(unsigned int pending_cnt)
{
    io_write32(RBUS_PENDING_CNT, pending_cnt);
    return;
}

void enable_rdbypasswt(int enable)
{
    unsigned int val = io_read32(RDBYPASSWT_CFG);

    if (enable) {
        if ((val&0x1)==0) {
            val |= (0x1); 
            io_write32(RDBYPASSWT_CFG, val);
        }
    }
    else {
        if (val&0x1) {
            val &= (~0x1); 
            io_write32(RDBYPASSWT_CFG, val);
        }
    }
    return;
}

void config_rdbypasswt(unsigned int cmd_fifo_depth, unsigned int rdbypass_fifo_depth, unsigned int rdbypass_mask)
{
    unsigned int val;

    if ((cmd_fifo_depth>8) || (rdbypass_fifo_depth>4)) {
        printf("\nError: cmd_fifo_depth:%d>8  or rdbypass_fifo_depth:%d>4\n", cmd_fifo_depth, rdbypass_fifo_depth);
        return;
    }

    val = io_read32(RDBYPASSWT_CFG);
    val &= (~0xfe);
    val |= ((cmd_fifo_depth&0xf)<<1);
    val |= ((rdbypass_fifo_depth&0x7)<<5);
    io_write32(RDBYPASSWT_CFG, val);
    io_write32(RDBYPASSWT_MASK, rdbypass_mask);
    return;
}

int rdBypassWt_count(unsigned int writes, unsigned int reads, unsigned int testCase)
{
	unsigned long *dmem_unc_addr;
	unsigned long cnt0 = 0, cnt1 = 0;
    unsigned long cpuTotalCnts[NUM_RDBYPASS_TYPE], cpuCnt_diff;
    unsigned int i = 0, j = 0, k = 0,  tmpVal = 0;
	unsigned int bypassCnt0 = 0, bypassCnt1 = 0;
    unsigned int bypassTotalCnts[NUM_RDBYPASS_TYPE], bypassCnt_diff;
    unsigned long *tmp_wt_addr, *tmp_rd_addr;
    unsigned int testSize, testWord, tMask;
    unsigned int val;
    unsigned int pendingCnt=1000;
    unsigned int cpuPendCnt=pendingCnt*(CPUFREQ/BUSCLKFREQ);
    unsigned int minPbusCnt=55;
    unsigned int tolerance1=100, tolerance2=minPbusCnt;
    unsigned int min_cpuCnt_t1_t2[NUM_RDBYPASS_TYPE]={cpuPendCnt*(2+reads), cpuPendCnt*(2+reads), minPbusCnt*reads, minPbusCnt*reads};
    unsigned int max_cpuCnt_t1_t2[NUM_RDBYPASS_TYPE]={
                        min_cpuCnt_t1_t2[0]+tolerance1, min_cpuCnt_t1_t2[1]+tolerance1, 
                        min_cpuCnt_t1_t2[2]+tolerance2, min_cpuCnt_t1_t2[3]+tolerance2};
    unsigned int min_cpuCnt_t3_t4_t5[NUM_RDBYPASS_TYPE]={cpuPendCnt*(9+reads), cpuPendCnt*(reads*4), cpuPendCnt*reads, minPbusCnt*reads};
    unsigned int max_cpuCnt_t3_t4_t5[NUM_RDBYPASS_TYPE]={
                        min_cpuCnt_t3_t4_t5[0]+tolerance1, min_cpuCnt_t3_t4_t5[1]+tolerance1,
                        min_cpuCnt_t3_t4_t5[2]+tolerance1, min_cpuCnt_t3_t4_t5[3]+tolerance2};
    unsigned int rdbypassCnt_t3_t4_t5[NUM_RDBYPASS_TYPE]={0, reads, reads, 0};


    enable_rdbypasswt(0); /* disable before config */
    
    if (testCase==1) {
       config_rdbypasswt(1, 1, 0xffffffe0);
    }
    else if (testCase==2) {
        config_rdbypasswt(1, 4, 0xffffffe0);
    }
    else if (testCase==3) {
        config_rdbypasswt(8, 1, 0xffffffe0);
    }
    else if (testCase==4) {
        config_rdbypasswt(8, 4, 0xffffffe0);
    }
    else if (testCase==5) {
        config_rdbypasswt(8, 4, 0xffffffc0);
    }
    else {
        printf("\nError: wrong testCase:%d\n", testCase);
        return 0;
    }
    
    testSize = ALL_FF-(io_read32(RDBYPASSWT_MASK))+1;
    testWord = (testSize / sizeof(unsigned long)); 
    tMask = testWord-1;
    
    dmem_unc_addr = DMEM_TEST_UNC_ADDR;
    tmp_wt_addr = dmem_unc_addr; /* for write Rbus */

    /* config rbus_pending by tmp_wt_addr */
    set_rbus_pending_high8bits_addr((((unsigned int)tmp_wt_addr)&(~UNC_ADDR_OFFSET)));
    set_rbus_pending_count(pendingCnt);

    startPFM(0, PFM_EVENT_CYCLE);

	for (k = 0; k < NUM_RDBYPASS_TYPE; k++)
	{
		if (k==0) { /* enable rdbypasswt for read Rbus near */
            enable_rdbypasswt(1);
			tmp_rd_addr = (dmem_unc_addr); 
        }
		else if (k==1) { /* enable rdbypasswt for read Rbus far */
            enable_rdbypasswt(1);
		    tmp_rd_addr = (dmem_unc_addr + testWord); 
        }
        else if (k==2) { /* enable rdbypasswt for read Pbus far */
            enable_rdbypasswt(1);
            tmp_rd_addr = GDMP_SRAM_TEST_UNC_ADDR; 
        }
        else { /* disable rdbypasswt for read Pbus far */
            enable_rdbypasswt(0);
            tmp_rd_addr = GDMP_SRAM_TEST_UNC_ADDR; 
        }

        __nds32__gie_dis(); /* disable global interrupt */

        enable_rbus_pending(1);
        __nds32__dsb();

        /* uncached write DMEM on Rbus */
        for (j = 0; j < writes; j++)
    			ramWrite32(&tmp_wt_addr[(j&tMask)], j);

        bypassCnt0 = io_read32(RDBYPASSWT_CNT);
        cnt0 = get_pfm_cnt0;

        /* uncached read DMEM on Rbus or GDMPSRAM on Pbus */
        for (j = 0; j < reads; j++)
    			tmpVal = ramRead32(&tmp_rd_addr[(j&tMask)]);

		cnt1 = get_pfm_cnt0;
		bypassCnt1 = io_read32(RDBYPASSWT_CNT);

        enable_rbus_pending(0);
        
         __nds32__gie_en(); /* enable global interrupt */

        if (cnt1 >= cnt0)
			cpuCnt_diff = (cnt1 - cnt0);
		else
			cpuCnt_diff = ((~(0UL) - cnt1) + cnt0);

		if (bypassCnt1 >= bypassCnt0)
			bypassCnt_diff = (bypassCnt1 - bypassCnt0);
		else
			bypassCnt_diff = ((~(0U) - bypassCnt1) + bypassCnt0);


        cpuTotalCnts[k] = cpuCnt_diff;
        bypassTotalCnts[k] = bypassCnt_diff;
        
        if (testCase==1 || testCase==2) {
            if (!( (min_cpuCnt_t1_t2[k]<cpuTotalCnts[k]) && (cpuTotalCnts[k]<max_cpuCnt_t1_t2[k]) )) {
                printf("\nERROR: cpuTotalCnts[%d]:%d is not in range %d~%d for testCase%d\n",
                        k, cpuTotalCnts[k], min_cpuCnt_t1_t2[k], max_cpuCnt_t1_t2[k], testCase);
                break;
            }
            if (bypassTotalCnts[k]!=0) {
                printf("\nERROR: bypassTotalCnts[%d]:%d!=0 for testCase%d\n", k, bypassTotalCnts[k], testCase);
                break;
            }
        }
        else { /* testCase==3,4,5 */
            if (!( (min_cpuCnt_t3_t4_t5[k]<cpuTotalCnts[k]) && (cpuTotalCnts[k]<max_cpuCnt_t3_t4_t5[k]) )) {
                printf("\nERROR: cpuTotalCnts[%d]:%d is not in range %d~%d for testCase%d\n",
                        k, cpuTotalCnts[k], min_cpuCnt_t3_t4_t5[k], max_cpuCnt_t3_t4_t5[k], testCase);
                break;
            }
            if (!( (rdbypassCnt_t3_t4_t5[k]<=bypassTotalCnts[k]) && (bypassTotalCnts[k]<=(rdbypassCnt_t3_t4_t5[k]+1)) )) {
                printf("\nERROR: bypassTotalCnts[%d]:%d is not in range %d~%d for testCase%d\n", 
                        k, bypassTotalCnts[k], rdbypassCnt_t3_t4_t5[k], rdbypassCnt_t3_t4_t5[k]+1, testCase);
                break;
            }
        }
	}
    
    stopPFM(0);

	printf("\nRDBYPASSWT_CFG:0x%x, RDBYPASSWT_MASK:0x%x\n", io_read32(RDBYPASSWT_CFG), io_read32(RDBYPASSWT_MASK));
    printf("AXI2RBUS_INFRA_CFG:0x%x, RBUS_PENDING_CNT:0x%x\n", io_read32(AXI2RBUS_INFRA_CFG), io_read32(RBUS_PENDING_CNT));
	printf("tmp_wt_addr:%p tmp_rd_rbus_near:%p tmp_rd_rbus_far:%p tmp_rd_pbus_far:%p\n",
			tmp_wt_addr, dmem_unc_addr, (dmem_unc_addr + testWord), GDMP_SRAM_TEST_UNC_ADDR);
    printf("%d writes, %d reads (pendingCnt:%d)\n", writes, reads, pendingCnt);
	printf("non-bypass rbus read: cpuCycles:%d, rdBypassWtCnt:%d\n", cpuTotalCnts[0], bypassTotalCnts[0]);
	printf("hit-bypass rbus read: cpuCycles:%d, rdBypassWtCnt:%d\n", cpuTotalCnts[1], bypassTotalCnts[1]);
    printf("hit-bypass pbus read: cpuCycles:%d, rdBypassWtCnt:%d\n", cpuTotalCnts[2], bypassTotalCnts[2]);
    printf("dis-bypass pbus read: cpuCycles:%d, rdBypassWtCnt:%d\n", cpuTotalCnts[3], bypassTotalCnts[3]);

    if (k==NUM_RDBYPASS_TYPE)
        printf("\n%s PASS\n", __func__);
    else
        printf("\n%s FAIL\n", __func__);

    return 0;
}


int read_and_incCompare( unsigned int *addr, unsigned int words, unsigned int cpmVal)
{
    int j;
    
    for (j = 0; j < words; j++)
    {
        if (ramRead32(&addr[j]) != (cpmVal + j))
        {
            printf("\nError! ramRead32(0x%x):0x%08x != 0x%08x at j==%d\n",  &addr[j], ramRead32(&addr[j]), (cpmVal + j), j);
            return -1;
        }
    }

    return 0;
}

unsigned int diff_count (unsigned int cnt0, unsigned int cnt1)
{
    if (cnt1 >= cnt0)
		return (cnt1 - cnt0);
	else
		return ((~(0U) - cnt1) + cnt0);
}

int rdBypassWt_stress(void)
{
	unsigned int *cached_region0, *uncached_region0;
	unsigned int *uncached_region1, *uncached_region2;
    unsigned int *read_addr_p, *write_addr_p;
	unsigned int j = 0, k;
	unsigned int value1 = 0x12345678, value2 = 0x87654321, read_val;
	unsigned long rounds = 1;
    unsigned int testSize = CACHE_LINE_SIZE(DCACHE);
    unsigned int testWord = (testSize>>2);
    unsigned int random_word;
    unsigned int tmpVal;
    unsigned int bypassCnt0 = 0, bypassCnt1 = 0, bypassTotalCnts[NUM_RDBYPASS_STRESS];


	cached_region0 = DMEM_TEST_ADDR;
    uncached_region0 = DMEM_TEST_ADDR+UNC_ADDR_OFFSET;
	uncached_region1 = uncached_region0 + testWord;
    uncached_region2 = GDMP_SRAM_TEST_UNC_ADDR;

    /* disable watchdog */
    wdog_kill();

    /* make sure that rdBypassWt is enabled */
    if ((io_read32(RDBYPASSWT_CFG)&(0x1))==0) {
        io_write32(RDBYPASSWT_MASK, ALL_FF-(testSize-1));
        io_write32(RDBYPASSWT_CFG, 0x91);
    }
    printf("\n%s (RDBYPASSWT_CFG:0x%x, RDBYPASSWT_MASK:0x%x)\n", 
            __func__, io_read32(RDBYPASSWT_CFG), io_read32(RDBYPASSWT_MASK));
    
    printf("enable rbus pending, in order to trigger rdbypasswt cnt during test\n");
    /* config rbus_pending and enable */
    set_rbus_pending_high8bits_addr(DMEM_TEST_ADDR);
    set_rbus_pending_count(1000);

    /* init region1 & region2 */
	for (j = 0; j < testWord; j++) {
		ramWrite32(&uncached_region1[j], value1+j);
        ramWrite32(&uncached_region2[j], value2+j);
    }

	while(rounds++)
	{
        for (k=0; k<NUM_RDBYPASS_STRESS; k++)
        {
            if (k==0) {
                write_addr_p = cached_region0;
                read_addr_p = uncached_region1;
                read_val = value1;
            }
            else if (k==1) {
                write_addr_p = uncached_region0;
                read_addr_p = uncached_region1;
                read_val = value1;
            }
            else /*k==2*/ {
                write_addr_p = uncached_region0;
                read_addr_p = uncached_region2;
                read_val = value2;
            }
        
    		random_word = get_cpu_timer_cnt(0);

            enable_rbus_pending(1);
            __nds32__dsb();

            bypassCnt0 = io_read32(RDBYPASSWT_CNT);
            
            /* writes to regionX */
    		for (j = 0; j < testWord; j++) {
    			ramWrite32(&write_addr_p[j], random_word+j);
                if (k==0) {
                    /* one dcache_wback just contributes one write cmd */
                    __nds32__cctlva_wbinval_one_lvl(NDS32_CCTL_L1D_VA_WB, (void *)&write_addr_p[j]);
                }
            }
            
            /* reads from regionY to trigger bypassCnt if there is any */
            if (read_and_incCompare(read_addr_p, testWord, read_val)) {
                printf("\nError! read_and_incCompare failed at round:%d, k:%d\n", rounds, k);
                return -1;
            }

            bypassCnt1 = io_read32(RDBYPASSWT_CNT);

            enable_rbus_pending(0);

            /* read from regionX to make sure previous write is OK */
            if (read_and_incCompare(write_addr_p, testWord, random_word)) {
                printf("\nError2! read_and_incCompare failed at round:%d, k:%d\n", rounds, k);
                return -1;
            }

    		bypassTotalCnts[k] += diff_count(bypassCnt0, bypassCnt1);
        }
        
		if ((rounds&0xfff)==0xfff) {
            for (k=0; k<NUM_RDBYPASS_STRESS; k++)
                printf("bypassTotalCnt%d:0x%x ", k, bypassTotalCnts[k]);
            printf("for rounds:0x%x\n", rounds);
        }
	}

	return 0;
}

static void registers_stress_task( void *pvParameters )
{
    int i, j;
    unsigned int regs_num = sizeof(registers_addr)/sizeof(registers_addr[0]);
    unsigned int pat_num = sizeof(testWordPat)/sizeof(testWordPat[0]);
    unsigned int mask=0xffff;
    unsigned int rounds = 0;
    unsigned int access=0, max_access=(get_timer0_tick()&mask);
    unsigned pat;

    printf("%s start\n", __func__);

	while(1)
	{
	    /* write a register then read and compare */
        for (i=0; i<regs_num; i++) 
        {
            for (j=0; j<pat_num; j++) {
                pat = testWordPat[j]+rounds;
                io_write32(registers_addr[i], pat);
                if (io_read32(registers_addr[i]) != pat) {
                    printf("\nError1: io_read32(0x%x):0x%x != pat:0x%x at (i,j,r)==(%d,%d,%d)\n", 
                            registers_addr[i], io_read32(registers_addr[i]), pat, i, j, rounds);
                    vTaskDelete(NULL);
                    return;
                }
            }
            task_switching(&access, &max_access, mask);
        }

        /* write all registers then read and compare */
        for (j=0; j<pat_num; j++) 
        {
            for (i=0; i<regs_num; i++) {
                pat = testWordPat[j]+rounds;
                io_write32(registers_addr[i], pat);
            }
            for (i=0; i<regs_num; i++) {
                pat = testWordPat[j]+rounds;
                if (io_read32(registers_addr[i]) != pat) {
                    printf("\nError2: io_read32(0x%x):0x%x != pat:0x%x at (i,j,r)==(%d,%d,%d)\n", 
                            registers_addr[i], io_read32(registers_addr[i]), pat, i, j, rounds);
                    vTaskDelete(NULL);
                    return;
                }
            }
            task_switching(&access, &max_access, mask);
        }

        rounds++;
        if ((rounds&mask)==mask)
            printf("%s rounds:0x%x\n", __func__, rounds);
    }
}

static void sram_stress_task( void *pvParameters )
{
    unsigned int rounds = 0;
    unsigned int mask=0x3;

    printf("%s start\n", __func__);

    while(1)
    {
        /* GDMP_SRAM cached access */
        if (air_rw_ram_test(GDMPSRAM_BASE, GDMPSRAM_SIZE, rounds)) {
            printf("Error: GDMP_SRAM cached access failed at round%d\n", rounds);
            vTaskDelete(NULL);
            return;
        }
        air_dcache_wback_inv(GDMPSRAM_BASE,GDMPSRAM_SIZE);
        
        /* GDMP_SRAM un-cached access */
        if (air_rw_ram_test(GDMPSRAM_UNC_BASE, GDMPSRAM_SIZE, rounds)) {
            printf("Error2: GDMP_SRAM un-cached access failed at round%d\n", rounds);
            vTaskDelete(NULL);
            return;
        }

        rounds++;
        if ((rounds&mask)==mask)
            printf("%s rounds:0x%x\n", __func__, rounds);
    }
}

static void dmem_stress_task( void *pvParameters )
{
    unsigned int rounds = 0;
    unsigned int mask=0x3;

    printf("%s start\n", __func__);

    while(1)
    {
        /* DMEM cached access */
        if (air_rw_ram_test(DMEM_TEST_ADDR2, S_16K, rounds)) {
            printf("Error: DMEM cached access failed at round%d\n", rounds);
            vTaskDelete(NULL);
            return;
        }
        air_dcache_wback_inv(DMEM_TEST_ADDR2, S_16K);

        /* DMEM un-cached access */
        if (air_rw_ram_test(DMEM_TEST_UNC_ADDR2, S_16K, rounds)) {
            printf("Error2: DMEM un-cached access failed at round%d\n", rounds);
            vTaskDelete(NULL);
            return;
        }

        rounds++;
        if ((rounds&mask)==mask)
            printf("%s rounds:0x%x\n", __func__, rounds);
    }
}


void create_registers_stress_task(void)
{
    xTaskCreate(registers_stress_task,          /* The function that implements the task. */
                "registers_stress_task",        /* The text name assigned to the task - for debug only as it is not used by the kernel. */
                configMINIMAL_STACK_SIZE,       /* The size of the stack to allocate to the task. */
                NULL,                           /* The parameter passed to the task - not used in this simple case. */
                (tskIDLE_PRIORITY),             /* The priority assigned to the task. */
                NULL );               
    return;
}

void create_sram_stress_task(void)
{
    xTaskCreate(sram_stress_task,          /* The function that implements the task. */
                "sram_stress_task",        /* The text name assigned to the task - for debug only as it is not used by the kernel. */
                configMINIMAL_STACK_SIZE,       /* The size of the stack to allocate to the task. */
                NULL,                           /* The parameter passed to the task - not used in this simple case. */
                (tskIDLE_PRIORITY),             /* The priority assigned to the task. */
                NULL );               
    return;
}

void create_dmem_stress_task(void)
{
    xTaskCreate(dmem_stress_task,          /* The function that implements the task. */
                "dmem_stress_task",        /* The text name assigned to the task - for debug only as it is not used by the kernel. */
                configMINIMAL_STACK_SIZE,       /* The size of the stack to allocate to the task. */
                NULL,                           /* The parameter passed to the task - not used in this simple case. */
                (tskIDLE_PRIORITY),             /* The priority assigned to the task. */
                NULL );               
    return;
}


int bus_stress_test(void)
{    
    printf("\n%s\n", __func__);

    /* disable rbus pending because pending_cnt register (in registers_addr[]) is going to be R/W randomly */
    enable_rbus_pending(0);

    /* make sure that timer 1 & 2 are disabled because their registes are going to be R/W randomly */
    if (isTimerEnabled(1) || isTimerEnabled(2)) {
        printf("\nError: timer1 or timer2 is enabled!\n");
        return -1;
    }

    /* disable illegal access because its registers will be R/W randomly */
    io_write32(DRAM_END_ADDR, DMEM_BASE+DMEM_SIZE+0x100);
    io_write32(DRAM_ILLACC_CHKEN, 0);

    /* diable watchdog */
    wdog_kill();

    /* create stress tasks */
    create_registers_stress_task();
    create_sram_stress_task();
    create_dmem_stress_task();

    return 0;
}

int btb_test (void)
{
    io_write32(CR_SCREG_WR0, 0);

    _btb_test_start();

    if (io_read32(CR_SCREG_WR0) != 0x21522152) {
        printf("\n%s failed (io_read32(0x%x):0x%x != 0x21522152)\n", 
                __func__, CR_SCREG_WR0, io_read32(CR_SCREG_WR0));
        return -1;
    }

    printf("\n%s pass\n", __func__);
    return 0;
}

/*
 * 1. no_wait_grant standby doesn't need to wait for wakeup_ok signal from Power Management Uint (PMU).
 *  1.1. wait_grant or wait_done standby need to wait for wakeup_ok signal from PMU which 8851 doesn't support.
 * 2. External Interrupt should be able to wake up CPU from no_wait_grant standby.
 *  2.1. disabling interrupt mask can't stop External Interrupt from waking up CPU, so periodic interrupt
 *       sources (timer,cpuTmr) have to be disabled during test.
 */
int cpu_noWaitGrant_standby_test (void)
{
    unsigned int mask = __nds32__mfsr( NDS32_SR_INT_MASK2 );
    unsigned int i;

	printf("\nCPU no-wait-grant StandBy and Wakeup Test\n");

    
    /* disable periodic interrupts, such as timer and cpu timer */
    timer_init(0, 0, 1000); /* disable timer0 */
    cpu_timer_init(0, DISABLE, configCPUTMR_CNT_PER_TICK); /* disable CPU timer0 */

	/* Disable global interrupt for core to execute next
	 * instruction of __nds32__standby_no_wake_grant() when wake up */
	__nds32__gie_dis();

	/* Enter standby mode */
	printf("Entering StandBy mode ... Press any key on console to wake CPU up ...\n");
	__nds32__standby_no_wake_grant();
	printf("\nCPU is Waked up from standby mode ...\n");

    /* enable global interrupt */
    __nds32__gie_en();

    /* enable periodic interrupts, such as timer and cpu timer */
    cpu_timer_init(0, ENABLE, configCPUTMR_CNT_PER_TICK); /* enable CPU timer0 */
    timer_init(0, 1, 1000); /* enable timer0 */
    
	return 0;
}

int isPSW_PFT_EN (void)
{
    if (__nds32__mfsr(NDS32_SR_PSW)&(1<<21))
        return 1;
    else
        return 0;
}

/*
 * level: 0~15 (level 0 has highest performance, level 15 has lowest performance)
 * isFastInt: when set, PSW.PFT_EN will be cleared automatically when CPU takes an interrupt
 */
void config_perf_throttling(unsigned int level, int isFastIntEn)
{
    unsigned int val;

    if (level > LOWEST_PERF_LEVEL) {
        printf("\nERROR: Wrong throttling level:%d\n", level);
        return;
    }

    /* Disable performance throttling */
    val = __nds32__mfsr(NDS32_SR_PSW);
    val &= (~(1<<21));
    __nds32__mtsr(val, NDS32_SR_PSW);

    /* Set Throttling Level and FAST_INT */
    val = __nds32__mfsr(__NDS32_REG_PFT_CTL__);
    val &= (~(T_LEVEL_MSK|FAST_INT));
    val |= T_LEVEL(level);
    if (isFastIntEn)
       val |= FAST_INT; 
	__nds32__mtsr(val, __NDS32_REG_PFT_CTL__);

    /* Enable performance throttling */
	val = __nds32__mfsr(NDS32_SR_PSW);
    val |= (1<<21);
    __nds32__mtsr(val, NDS32_SR_PSW);

    return;
}

unsigned int count_cycles_for_N_nop (unsigned int N /*nop*/)
{
    register unsigned int i = 0;
    unsigned int cycle0, cycle1;

    __nds32__gie_dis();
    cycle0 = get_pfm_cnt0;
    
    for(i = 0; i < N; i++) {
		asm volatile ("nop");
	}
    
    cycle1 = get_pfm_cnt0;
    __nds32__gie_en();

    return diff_count(cycle0, cycle1);
}

/* with FAST_INT==1, perf_throttling will be cleared in ISR automatically. That is, performance
 * comes back to normal (un-throttled).
 * with FAST_INT==0, perf_throttling keeps enabled in ISR. That is, performance is still
 * be throttled. */
void pft_fast_int_test_in_isr(void)
{
    if (perfThrottlFlag[0]==1)
    {
        perfThrottlFlag[0]=0;
        if (perfThrottlFlag[1]==1) { /* FAST_INT test 1 (with FAST_INT==1) */
            if (isPSW_PFT_EN()!=0) {
                printf("(%s)ERROR: isPSW_PFT_EN()!=0\n", __func__);
                perfThrottlFlag[2]=0;
            }
            else {
                printf("(%s) PSW.PFT_EN is cleared automatically\n", __func__);
                perfThrottlFlag[2]=count_cycles_for_N_nop(10000);
            }
        }
        else { /* FAST_INT test 2 (with FAST_INT==0) */
            perfThrottlFlag[2]=count_cycles_for_N_nop(10000);
        }
    }

    return;
}

int cpu_perf_throttling_test (void)
{
    unsigned int totalCycles[T_LEVEL_ITEMS];
    unsigned int level;
    int res=0;

    if (!(__nds32__mfsr(NDS32_SR_MSC_CFG) & (1<<26))) {
		printf("\nCPU does NOT support Performance Throttling.\n");
		return -1;
	}

    for (level=0; level<T_LEVEL_ITEMS; level++){
        totalCycles[level]=0;
    }

    startPFM(0, PFM_EVENT_CYCLE);  

    printf("\nStart Performance Throttling Level Test\n");

    for (level=0; level<TOTAL_T_LEVELS; level++) 
    {
        /* configure perf_throttling level with FAST_INT disabled, then enable perf_throttling */
        config_perf_throttling(level, 0);
        /* count cycles for 10000 nops */
        totalCycles[level] = count_cycles_for_N_nop(10000);
    }

    /* compare results: 
     * lower level has higher performance which means fewer cycles per nop instruction,
     * So, results should be totalCycles[0] < totalCycles[1] < ... < totalCycles[15] */
    for (level=0; level<(TOTAL_T_LEVELS-1); level++)
    {
        /* lower level has higher performance. higher performance means fewer cycles per nop instruction */
        if (totalCycles[level]>=totalCycles[level+1]) {
            printf("\nERROR: totalCycles[%d]:%d >= totalCycles[%d]:%d\n", 
                        level, totalCycles[level], level+1, totalCycles[level+1]);
            res = -1;
            goto perf_throttling_test_end;
        }
    }

    printf("\t--> pass\n");

    printf("Start Performance Throttling FAST_INT Test\n");

    /* before test, disable periodic interrupts, such as timer and cpu timer */
    timer_init(0, 0, 1000); /* disable timer0 */
    cpu_timer_init(0, DISABLE, configCPUTMR_CNT_PER_TICK); /* disable CPU timer0 */

    /* FAST_INT Test 1 (with FAST_INT==1) */
    /* configure perf_throttling to Lowest Performance level with FAST_INT enabled, then enable perf_throttling */
    config_perf_throttling(LOWEST_PERF_LEVEL, 1);

    if (isPSW_PFT_EN()!=1) {
        printf("\nERROR: PSW.PFT_EN is not set to 1 correctly\n");
        res = -1;
        goto perf_throttling_test_end;
    }

    perfThrottlFlag[1]=1; /* FAST_INT test 1 */
    perfThrottlFlag[0]=1; /* wait flag */
    printf("Press a key in console ...\n");
    
    /* when Pressing a key, uart_interrupt will be raised and PSW.PFT_EN will be cleared automatically.
     * Then do tests in air_uart_isr. 
     * Note: After ISR, PSW.PFT_EN will be set automatically due to restoring from IPSW */
    while (perfThrottlFlag[0]==1); /* wait until uart isr is done */
    if (isPSW_PFT_EN()!=1) {
        printf("\nERROR: PSW.PFT_EN is not restored to 1 correctly\n");
        res = -1;
        goto perf_throttling_test_end;
    } 

    /* get count cycles from air_uart_isr */
    if (perfThrottlFlag[2]==0) {
        printf("\nERROR: perfThrottlFlag[2]==0\n");
        res = -1;
        goto perf_throttling_test_end;
    }
    totalCycles[TOTAL_T_LEVELS] = perfThrottlFlag[2];


    /* FAST_INT Test 2 (with FAST_INT==0) */
    /* configure perf_throttling to Lowest Performance level with FAST_INT disabled, then enable perf_throttling */
    config_perf_throttling(LOWEST_PERF_LEVEL, 0);

    perfThrottlFlag[1]=2; /* FAST_INT test 2 */
    perfThrottlFlag[0]=1; /* wait flag */
    printf("Press a key in console ...\n");
    while (perfThrottlFlag[0]==1); /* wait until uart isr is done */

    /* get count cycles from air_uart_isr */
    totalCycles[TOTAL_T_LEVELS+1] = perfThrottlFlag[2];

    /* after test, enable periodic interrupts, such as timer and cpu timer */
    cpu_timer_init(0, ENABLE, configCPUTMR_CNT_PER_TICK); /* enable CPU timer0 */
    timer_init(0, 1, 1000); /* enable timer0 */

    /* compare results: 
     * with FAST_INT==1, perf_throttling will be disabled in ISR, which will have best performance.
     * with FAST_INT==0, perf_throttling keeps enabled in ISR, which makes performance downgrade.
     * So, results Should be (totalCycles[16] < totalCycles[0]) && (totalCycles[17] == totalCycles[15]) */
    if ((totalCycles[TOTAL_T_LEVELS]>=totalCycles[0]) ||
        (totalCycles[TOTAL_T_LEVELS+1]!=totalCycles[LOWEST_PERF_LEVEL])) {
        printf("\nERROR: (totalCycles[%d]:%d >= totalCycles[0]:%d) or (totalCycles[%d]:%d != totalCycles[0]:%d)\n", 
                        TOTAL_T_LEVELS, totalCycles[TOTAL_T_LEVELS], totalCycles[0],
                        TOTAL_T_LEVELS+1, totalCycles[TOTAL_T_LEVELS+1], TOTAL_T_LEVELS, totalCycles[TOTAL_T_LEVELS]);
        res = -1;
    }

perf_throttling_test_end:
    stopPFM(0);

    printf("\n");
    for (level=0; level<T_LEVEL_ITEMS; level++){
        printf("totalCycles[%d]:%d\t", level, totalCycles[level]);
        if ((level&0x3)==0x3)
            printf("\n");
    }
    
    if (res==0) {
        printf("\n\nResults: totalCycles[16] < totalCycles[0] < ... < totalCycles[15] == totalCycles[17]\n");
        printf("CPU Performance Throttling Test --> Pass!!\n");
    }
    else
        printf("\n\nCPU Performance Throttling Test --> Fail!!\n");
    return res;
}

int wdog_test(void)
{
    /* init wdog: 
     * if cpu doesn't feed wdog for 10 secs, system will reboot. In last 3 secs, wdog interrupt will be issued */
    wdog_init(1, 10000, 3000);

    /* air_wdog_isr already was registered during booting */
    //register_isr(IRQ_TMR3, air_wdog_isr);
    
    printf("\nwdog_isr will be issued in 7 secs. After isr, system will reset in 3 secs\n");
    while(1);
    return 0;
}

int cpu_bus_clock_adjust_test (void)
{
    int i;

    printf("\nIn FPGA Stage, just do R/W test for CPU_CLKSRC_SEL and BUS_CLKSRC_SEL registers\n");

    for (i=0; i<4; i++) { /* test bit0,1 */
        io_write32(CPU_CLKSRC_SEL, i);
        io_write32(BUS_CLKSRC_SEL, i);
        if ((io_read32(CPU_CLKSRC_SEL)!=i) || (io_read32(BUS_CLKSRC_SEL)!=i)) {
            printf("\nERROR: (reg(CPU_CLKSRC_SEL):0x%x != 0x%x) || (reg(BUS_CLKSRC_SEL):0x%x != 0x%x)\n",
                    io_read32(CPU_CLKSRC_SEL), i, io_read32(BUS_CLKSRC_SEL), i);
            return -1;
        }
    }

    printf("test done!\n");
    return 0;
}

