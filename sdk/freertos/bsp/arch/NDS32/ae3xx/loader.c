/*
 * Copyright (c) 2012-2018 Andes Technology Corporation
 * All rights reserved.
 *
 */

//james,#include "config.h"

#ifdef CFG_BURN

/* No GCOV available */
#pragma GCC optimize "no-test-coverage"
#pragma GCC optimize "no-profile-arcs"

extern unsigned int __flash_start, __text_lmastart, __text_start, _edata;
extern unsigned int __loader_start, __loader_lmaend, __loader_lmastart;

asm (".no_ex9_begin");
asm (".no_ifc_begin");

/*
 * The function copies program code bank from rom/flash to memory.
 * It executes on memory instead of flash to get better coping performance.
 */
void __attribute__((no_prologue, optimize("O0"), section(".loader"))) loader(void) {
	register unsigned int *src_ptr, *dst_ptr;
	register int i, size;
	register void (*entry)(void) = (void *)&__text_start;

	/* Copy code bank to VMA area */
	size = ((unsigned int)&_edata - (unsigned int)&__text_start) / sizeof(int);

	src_ptr = (unsigned int *)((unsigned int)&__flash_start + (unsigned int)&__text_lmastart);
	dst_ptr = (unsigned int *)&__text_start;

	for (i = 0; i < size; i++)
		*dst_ptr++ = *src_ptr++;

	entry();
}

/* The entry function when boot, executing on ROM/FLASH. */
void __attribute__((no_prologue, optimize("O0"), section(".bootloader"))) boot_loader(void) {
	register unsigned int *src_ptr, *dst_ptr;
	register int i, size;
	register void (*ldr_entry)(void) = (void *)&__loader_start;

	/* Initial $GP incase used by compiler */
	asm ("la $gp, _SDA_BASE_");

	/* Copy loader() code to VMA area (On memory) */
	size = ((unsigned int)&__loader_lmaend - (unsigned int)&__loader_lmastart + (sizeof(int)-1)) / sizeof(int);

	src_ptr = (unsigned int *)((unsigned int)&__flash_start + (unsigned int)&__loader_lmastart);
	dst_ptr = (unsigned int *)&__loader_start;

	for (i = 0; i < size; i++)
		*dst_ptr++ = *src_ptr++;

	/*
	 * Call the real program loader which executes on memory instead
	 * of flash for batter coping performance.
	 */
	ldr_entry();
}

asm (".no_ifc_end");
asm (".no_ex9_end");

#endif	// CFG_BURN
