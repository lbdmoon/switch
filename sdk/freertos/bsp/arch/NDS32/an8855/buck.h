#ifndef BUCK_H
#define BUCK_H

#define RG_VBG_SEL_OFS              (6)
#define RG_VBG_SEL_MSK              (0x7)

#define RG_V2I_VREF_OFS             (0)
#define RG_V2I_VREF_MSK             (0x7)

#define RG_V2I_IBIAS_OFS            (3)
#define RG_V2I_IBIAS_MSK            (0xf)

#define RG_BUCK_VOSEL_OFS           (19)
#define RG_BUCK_VOSEL_MSK           (0x1f)

#define RG_BUCK_UGSR_OFS            (3)
#define RG_BUCK_UGSR_MSK            (0x7)

#define RG_BUCK_IB_TRIM_OFS         (16)
#define RG_BUCK_IB_TRIM_MSK         (0x1f)

#define RG_BUCK_PFMOS_TRIM_OFS      (21)
#define RG_BUCK_PFMOS_TRIM_MSK      (0x3f)

#define RG_BUCK_PFMOS_SEL_TRIM_OFS  (27)
#define RG_BUCK_PFMOS_SEL_TRIM_MSK  (0x1)

#define RG_BUCK_PWMOS_TRIM_OFS      (0)
#define RG_BUCK_PWMOS_TRIM_MSK      (0x3f)

#define RG_BUCK_PWMOS_SEL_TRIM_OFS  (6)
#define RG_BUCK_PWMOS_SEL_TRIM_MSK  (0x1)

#define RG_BUCK_CSOS_TRIM_OFS       (7)
#define RG_BUCK_CSOS_TRIM_MSK       (0x3f)

#define RG_BUCK_CSOS_SEL_TRIM_OFS   (13)
#define RG_BUCK_CSOS_SEL_TRIM_MSK   (0x1)

#define RG_BUCK_CSAUTO_TRIM_OFS     (14)
#define RG_BUCK_CSAUTO_TRIM_MSK     (0x7)

#define RG_BUCK_CSAUTO_SEL_TRIM_OFS (17)
#define RG_BUCK_CSAUTO_SEL_TRIM_MSK (0x1)

#define RG_BUCK_CSM_TRIM_OFS        (18)
#define RG_BUCK_CSM_TRIM_MSK        (0x7)

#define RG_BUCK_ZXOS_TRIM_OFS       (21)
#define RG_BUCK_ZXOS_TRIM_MSK       (0xf)

#endif
