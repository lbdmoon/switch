#ifndef AN8855_H
#define AN8855_H

/*************************************************/

// Below definitions are for sdk shrink sram
#if 1

#define SRAM_SHRINK___XMODEM_BUF
#define SRAM_SHRINK___XMODEM_CMD_REMOVE

#define SRAM_SHRINK___CMD

#define SRAM_SHRINK___CMD_REMOVE_UNUSED

#define SRAM_SHRINK___CMD_REDESIGN

//#define DBG_LOG printf("%s %d:",__FUNCTION__, __LINE__ ); printf
#define DBG_LOG(...)

#define SRAM_SHRINK___HEAP_INFO

#ifdef CONFIG_SRAM_SHRINK___FLASH
#define SRAM_SHRINK___FLASH
#endif

//#define SRAM_SHRINK___DBG

#define SRAM_SHRINK___TOP

#else

#define DBG_LOG(...)

#endif

/*************************************************/

#endif
