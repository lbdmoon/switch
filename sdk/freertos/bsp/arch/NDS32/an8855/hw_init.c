
#include <nds32_intrinsic.h>
#include "nds32_defs.h"
#include <platform.h>
#include <cache.h>

#include "efuse.h"
#include "buck.h"

#if 1		/* wy,debug */
#include "product_config.h"
#endif

#define RG_GPIO_UART1_MODE      (CR_CHIP_SCU_BASE + 0x74)

#define GPIO0_UART_OFS          (0)
#define GPIO0_UART_MSK          (0x1)

#define AN_COLLISION_MAX_DELAY_US   (10000)
#define AN_COLLISION_MAX_DELAY_MS   (500)

extern char __data_start, _edata, __sbss_end;

extern uint16_t cl22_read(const uint8_t phyAddr, const uint8_t pageSel, const uint8_t regSel);
extern void cl22_write(const unsigned char phyAddr, const unsigned char pageSel, const unsigned char regSel, const unsigned short wdata);
extern uint16_t cl45_read(const uint8_t phyAddr, const uint8_t devID, const uint16_t regSel);
extern void cl45_write(const uint8_t phyAddr, const uint8_t devID, const uint16_t regSel, const uint16_t wdata);
extern uint32_t get_random(uint8_t phy_addr);

extern int
hal_pearl_mdio_writeC22(
    const unsigned int    unit,
    const unsigned short    bus_id,
    const unsigned short    phy_addr,
    const unsigned short    reg_addr,
    const unsigned short    reg_data);

extern int
hal_pearl_mdio_readC22(
    const unsigned int    unit,
    const unsigned short    bus_id,
    const unsigned short    phy_addr,
    const unsigned short    reg_addr,
    unsigned short          *ptr_reg_data);

extern int
hal_pearl_mdio_writeC45(
    const unsigned int    unit,
    const unsigned short    bus_id,
    const unsigned short    phy_addr,
    const unsigned short    dev_type,
    const unsigned short    reg_addr,
    const unsigned short    reg_data);

extern int
hal_pearl_mdio_readC45(
    const unsigned int    unit,
    const unsigned short    bus_id,
    const unsigned short    phy_addr,
    const unsigned short    dev_type,
    const unsigned short    reg_addr,
    unsigned short          *ptr_reg_data);

unsigned int an8801sb_phy_readBuckPbus(const unsigned short port_phy_addr, const unsigned int pbus_addr)
{
    unsigned int          pbus_data = 0;
    unsigned int          pbus_data_low = 0, pbus_data_high = 0;

    hal_pearl_mdio_writeC22(0, 0, port_phy_addr, 0x1F, 0x4);
    hal_pearl_mdio_writeC22(0, 0, port_phy_addr, 0x10, 0x0);
    hal_pearl_mdio_writeC22(0, 0, port_phy_addr, 0x15, (unsigned int)((pbus_addr >> 16) & 0xffff));
    hal_pearl_mdio_writeC22(0, 0, port_phy_addr, 0x16, (unsigned int)(pbus_addr & 0xffff));
    hal_pearl_mdio_readC22(0, 0, port_phy_addr, 0x17, &pbus_data_high);
    hal_pearl_mdio_readC22(0, 0, port_phy_addr, 0x18, &pbus_data_low);

    pbus_data = (pbus_data_high << 16) + pbus_data_low;
    return pbus_data;
}

void an8801sb_phy_writeBuckPbus(const unsigned short port_phy_addr, const unsigned int pbus_addr, const unsigned int pbus_data)
{
    hal_pearl_mdio_writeC22(0, 0, port_phy_addr, 0x1F, 0x4);
    hal_pearl_mdio_writeC22(0, 0, port_phy_addr, 0x10, 0x0);
    hal_pearl_mdio_writeC22(0, 0, port_phy_addr, 0x11, (unsigned int)((pbus_addr >> 16) & 0xffff));
    hal_pearl_mdio_writeC22(0, 0, port_phy_addr, 0x12, (unsigned int)(pbus_addr & 0xffff));
    hal_pearl_mdio_writeC22(0, 0, port_phy_addr, 0x13, (unsigned int)((pbus_data >> 16) & 0xffff));
    hal_pearl_mdio_writeC22(0, 0, port_phy_addr, 0x14, (unsigned int)(pbus_data & 0xffff));
}

void rbus_timeout_check(void)
{
    if ((io_read32(TIMEOUT_STS0) & 0x1) == 1)
    {
        printf("\r\nWARNING !!! Rbus timeout happended !!!\r\n");
        printf("Rbus timeout status:0x%x, errAddr:0x%x\r\n", io_read32(TIMEOUT_STS0), io_read32(TIMEOUT_STS1));
        /* clear the status*/
        io_write32(TIMEOUT_STS0, 0);
        io_write32(TIMEOUT_STS1, 0);
    }
}

void rbus_timeout_init(void)
{
    rbus_timeout_check();

    /* set cmd/wdata/rdata timeout_cnt as 100 ms */
    io_write32(TIMEOUT_CFG0, DMEM_CTRL_CLK/10);
    io_write32(TIMEOUT_CFG1, DMEM_CTRL_CLK/10);
    io_write32(TIMEOUT_CFG2, DMEM_CTRL_CLK/10);

    /* enable timeout.
     * Note: ISR won't work due to data/bss section in DMEM */
    io_write32(TIMEOUT_STS0, 0x80000000);

    return;
}

void air_pbus_tout_isr(void)
{
    int isBusTout=0;
    unsigned int errAddr;


    isBusTout = (io_read32(PB_TIMEOUT_INT)&0x1);
    io_write32(PB_TIMEOUT_INT, 1); /* clear intr */

    errAddr = io_read32(PB_TIMEOUT_INFO);

    if (isBusTout)
        printf("\n\npbus timeout interrupt errAddr=%08lx\n\n", errAddr);
    else
        printf("\n\nunknown bus timeout interrupt errAddr=%08lx\n\n", errAddr);

    return;
}

void pbus_timeout_init(void)
{
    unsigned int val;

    register_isr(IRQ_BUS_TOUT, air_pbus_tout_isr);

    val = BUSCLKFREQ/10; /* timeout time: 100 ms */
    io_write32(PB_TIMEOUT_CFG, val);

    val |= (1<<31); /* enable pbus timeout */
    io_write32(PB_TIMEOUT_CFG, val);

    return;
}

void rbus_rdbypasswt_init(void)
{
    unsigned int line_size = CACHE_LINE_SIZE(DCACHE);
    unsigned int mask = ALL_FF-(line_size-1);
    unsigned int val;


    /* disable rdbypasswt before setting mask */
    val = io_read32(RDBYPASSWT_CFG);
    val &= (~0x1);
    io_write32(RDBYPASSWT_CFG, val);

    /* set rdbypasswt mask according to dcache line size */
    io_write32(RDBYPASSWT_MASK, mask);

    /* enable rdbypasswt.
     * Note: don't change cmd_fifo and rdcmd_fifo, otherwise, HW will go wrong */
    val = io_read32(RDBYPASSWT_CFG);
    val |= (0x1);
    io_write32(RDBYPASSWT_CFG, val);

    //printf("RDBYPASSWT_CFG:0x%x, RDBYPASSWT_MASK:0x%x\n", io_read32(RDBYPASSWT_CFG), io_read32(RDBYPASSWT_MASK));

    return;
}

void airInitHardware( void )
{
    int i;
    unsigned short phy_data = 0;
    unsigned int rnd = 0, rnd1 = 0, rnd2 = 0; 
    unsigned int delay_time = 0, powerup_delay[6], force_seed_delay[6];
    /* Enable IOMUX for UART pins.
     * Note: Only support in AN8801F GPIO8/GPIO9, this setting should be checked or revised at AN8855 IC back.
     */
#ifndef IS_FPGA_STAGE
    /* read 0x10000074 */
    unsigned int reg = io_read32(RG_GPIO_UART1_MODE);
    unsigned int data;

#ifdef SKU_2149
    //disable gpio7/8 gpio function
    data = io_read32(0x1000007c);
    data &= ~((1 << 7) | (1 << 8));
    io_write32(0x1000007c, data);
    
    //enable smi master on gpio7/8
    data = io_read32(0x10000070);
    data |= (0x1 << 1);
    io_write32(0x10000070, data);

#ifndef SMI_CLOCK_1_P_5625_MHZ
#error "SMI_CLOCK_1_P_5625_MHZ not defined"
#endif

#if defined(SMI_CLOCK_1_P_5625_MHZ) && SMI_CLOCK_1_P_5625_MHZ
	//change smi master clk
	data = io_read32(0x10005048);
	data &= ~0x7f;
	data |= 31;
	io_write32(0x10005048, data);
#endif

    //reset 8801
    //enable gpio6 gpio function
    data = io_read32(0x1000007c);
    data |= (1 << 6);
    io_write32(0x1000007c, data);
    
    //set gpio6 output mode
    data = io_read32(0x1000a300);
    data &= ~(0x3 << (6*2));
    data |= (0x1 << (6*2));
    io_write32(0x1000a300, data);
    
    //set gpio6 output enable
    data = io_read32(0x1000a314);
    data |= (0x1 << 6);
    io_write32(0x1000a314, data);
    
    //set gpio6 low
    data = io_read32(0x1000a304);
    data &= ~(0x1 << 6);
    io_write32(0x1000a304, data);
    
    delay1ms(5);
    
    //set gpio6 high
    data = io_read32(0x1000a304);
    data |= (0x1 << 6);
    io_write32(0x1000a304, data);

    delay1ms(50);

    //light on gpio1, low active
    data = an8801sb_phy_readBuckPbus(0x19, 0x10000070);
    data |= (0x1 << 1);
    an8801sb_phy_writeBuckPbus(0x19, 0x10000070, data);

    data = an8801sb_phy_readBuckPbus(0x19, 0x1000a300);
    data &= ~(0x3 << (1*2));
    data |= (0x1 << (1*2));
    an8801sb_phy_writeBuckPbus(0x19, 0x1000a300, data);

    data = an8801sb_phy_readBuckPbus(0x19, 0x1000a314);
    data |= (0x1 << 1);
    an8801sb_phy_writeBuckPbus(0x19, 0x1000a314, data);

    data = an8801sb_phy_readBuckPbus(0x19, 0x1000a304);
    data &= ~(0x1 << 1);
    an8801sb_phy_writeBuckPbus(0x19, 0x1000a304, data);

    hal_pearl_mdio_writeC22(0, 0, 0x19, 9, 0x0200);
    hal_pearl_mdio_writeC22(0, 0, 0x19, 0, 0x1200);
    
    //reset SLED
    //enable gpio5 gpio function
    data = io_read32(0x1000007c);
    data |= (1 << 5);
    io_write32(0x1000007c, data);

    //set gpio5 output mode
    data = io_read32(0x1000a300);
    data &= ~(0x3 << (5*2));
    data |= (0x1 << (5*2));
    io_write32(0x1000a300, data);

    //set gpio5 output enable
    data = io_read32(0x1000a314);
    data |= (0x1 << 5);
    io_write32(0x1000a314, data);

    //set gpio5 low
    data = io_read32(0x1000a304);
    data &= ~(0x1 << 5);
    io_write32(0x1000a304, data);
#endif

    /* GPIO0 default pull down, need pull up */
    data = io_read32(0x10000020);
    data &= ~0x3;
    io_write32(0x10000020, data);

    data = io_read32(0x1000001c);
    data |= 0x3;
    io_write32(0x1000001c, data);

    /* reset UART module  */
    data = io_read32(0x100050c4);
    data |= 0x1;
    io_write32(0x100050c4, data);

    delay1ms(5);

    data = io_read32(0x100050c4);
    data &= ~0x1;
    io_write32(0x100050c4, data);

    /* setup uart on GPIO 0,1 */
    reg |= ((1 & GPIO0_UART_MSK) << GPIO0_UART_OFS);
    io_write32(RG_GPIO_UART1_MODE, reg);
#endif

    uart_init();
    printf("\n[%s] __data_start:0x%x, _edata:0x%x, __sbss_end:0x%x\n",
            __func__, (unsigned int)&__data_start, (unsigned int)&_edata, (unsigned int)&__sbss_end);

    /* wait efuse ready.. */
    efuse_init();
    rbus_timeout_init();
    rbus_rdbypasswt_init();
    pbus_timeout_init();

    /* enable timers */
    timer_init(0, 1, 1000); /* enable timer0 for delay1ms */

#ifndef IS_FPGA_STAGE
    //gphy_calibration();
#endif

    init_interrupts_count();

    /* switch init */
    //enable all port backpressure
    for(i = 0; i < 6; i++)
    {
        reg = 0x10210000+ 0x200 * i;
        data = io_read32(reg);
        data |= (1 << 11);
        io_write32(reg, data);            
    }
    io_write32(0x1022451c, 0xf);

    /* init phy */
    io_write32(0x1028c840, 0); //afe power up

    //an collision solution
    //set random seed
    rnd = get_random(0);
    printf("Random seed 0x%x\n", rnd);
    srand(rnd);

    //all port power down
    for(i = 0; i < 5; i++)
    {
        cl22_write(i, 0, 0, 0x1840);
    }
    #ifdef SKU_2149
    hal_pearl_mdio_writeC22(0, 0, 0x19, 0, 0x1840);
    #endif

    //get random power up and force random seed time
    for(i = 0; i < 6; i++)
    {
        rnd1 = rand();
        powerup_delay[i] = rnd1 % AN_COLLISION_MAX_DELAY_MS;
        
        rnd2 = rand();
        force_seed_delay[i] = rnd2 % AN_COLLISION_MAX_DELAY_US;
        
        //printf("port %d, rnd1 0x%x, rnd2 0x%x, powerup_delay 0x%x, force_seed_delay 0x%x\n",
        //    i, rnd1, rnd2, powerup_delay[i], force_seed_delay[i]);
    }

    for(delay_time = 0; delay_time < AN_COLLISION_MAX_DELAY_MS; delay_time++)
    {
        for(i = 0; i < 5; i++)
        {
            if (powerup_delay[i] == delay_time)
            {
                cl22_write(i, 0, 0, 0x1040);
                //printf("power up port %d, time %d\n", i, delay_time);
            }
        }
        #ifdef SKU_2149
        if (powerup_delay[5] == delay_time)
        {
            hal_pearl_mdio_writeC22(0, 0, 0x19, 0, 0x1040);
        }
        #endif
        delay1ms(1);
    }

    //power up left gphy
    for(i = 0; i < 5; i++)
    {
        data = cl22_read(i, 0, 0);
        if (data & (0x1 << 11))
        {
            cl22_write(i, 0, 0, 0x1040);
            //printf("Power up left gphy %d\n", i);
        }
    }
    #ifdef SKU_2149
    hal_pearl_mdio_readC22(0, 0, 0x19, 0, &phy_data);
    if (phy_data & (0x1 << 11))
    {
        hal_pearl_mdio_writeC22(0, 0, 0x19, 0, 0x1040);
    }
    #endif

    delay1ms(10);

    //force random seed
    for(i = 0; i < 5; i++)
    {
        data = cl45_read(i, 0x1E, 0x327);
        cl45_write(i, 0x1E, 0x327, data | 0x1F);
    }
    #ifdef SKU_2149
    hal_pearl_mdio_readC45(0, 0, 0x19, 0x1E, 0x327, &phy_data);
    hal_pearl_mdio_writeC45(0, 0, 0x19, 0x1E, 0x327, phy_data | 0x1F);
    #endif
    
    for(delay_time = 0; delay_time < AN_COLLISION_MAX_DELAY_US; delay_time++)
    {
        for(i = 0; i < 5; i++)
        {
            if (force_seed_delay[i] == delay_time)
            {
                data = cl45_read(i, 0x1E, 0x327);
                cl45_write(i, 0x1E, 0x327, data & 0xFFE0);
                //printf("unforce random seed port %d, time %d\n", i, delay_time);
            }
        }
        #ifdef SKU_2149
        if (force_seed_delay[5] == delay_time)
        {
            hal_pearl_mdio_readC45(0, 0, 0x19, 0x1E, 0x327, &phy_data);
            hal_pearl_mdio_writeC45(0, 0, 0x19, 0x1E, 0x327, phy_data & 0xFFE0);
        }
        #endif
        delay1us(50);
    }

    //all port re-AN
    for(i = 0; i < 5; i++)
    {
        cl22_write(i, 0, 0, 0x1200);
    }
    #ifdef SKU_2149
    hal_pearl_mdio_writeC22(0, 0, 0x19, 0, 0x1200);
    #endif
}


