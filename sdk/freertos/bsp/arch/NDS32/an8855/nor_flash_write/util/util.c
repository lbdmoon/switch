#include "util.h"
#define VPint   *(volatile unsigned int *)

#define HIGH_BIT_UNC        (0x1<<30)

#define CR_UART_BASE            (0x1000A000)
#define CR_UART_THR         (0x00+CR_UART_BASE+0)
#define CR_UART_LSR         (0x14+CR_UART_BASE+0)
#define LSR_THRE                            0x20

#define S_128K  (0x20000)
#define S_196K  (0x30000)
#define S_256K  (0x40000)
#define DMEM_BASE           (0x80000000)
#define DMEM_SIZE           (S_256K)

#define S_16M   (0x1000000)
#define REGS_BASE           (0x10000000)
#define REGS_SIZE           (S_16M)

static int isDmemAddr(unsigned int addr)
{
    if ((addr>=DMEM_BASE) && (addr<(DMEM_BASE+DMEM_SIZE)))
        return 1;
    else
        return 0;
}

static int isRegAddr(unsigned int addr)
{
    if ((addr>=REGS_BASE) && (addr<(REGS_BASE+REGS_SIZE)))
        return 1;
    else
        return 0;
}

unsigned int io_read32(unsigned int addr)
{
    addr |= HIGH_BIT_UNC;
    return VPint(addr);
}

void io_write32(unsigned int addr, unsigned int vlaue)
{
    addr |= HIGH_BIT_UNC;
    VPint(addr) = vlaue;
}

void ramWrite32(unsigned int addr, unsigned int vlaue)
{
    VPint(addr) = vlaue;
}

unsigned int ramRead32(unsigned int addr)
{
    return VPint(addr);
}

int memRead32(unsigned int addr, unsigned int *value_p)
{
    if(isDmemAddr(addr) || isRegAddr(addr)) {
        *value_p = io_read32(addr);
    }
    else {
        *value_p = ramRead32(addr);
        return 0;
    }

    return 0;
}

void memWrite32(unsigned int addr, unsigned int value)
{
    if(isDmemAddr(addr) || isRegAddr(addr))
    {
        io_write32(addr, value);
    }
    else
    {
        ramWrite32(addr, value);
    }
}

static void serial_outc(char c)
{
    while (!((io_read32(CR_UART_LSR)) & LSR_THRE)) ;

    io_write32(CR_UART_THR, c);
}

int outbyte(char c)
{
    serial_outc(c);
    if (c =='\n')
        serial_outc('\r');
    return c;
}

/* for simple_printf*/
static char* uitoa(unsigned int num, int base, char *str)
{
    int idx = 11;
    int remain = 0;
    unsigned int tmp;
    str[11] = 0;

    while(num)
    {
        remain = num % base;

        if(remain > 9)
        {
            str[--idx] = 'a' + (remain - 10);
        }
        else
        {
            str[--idx] = '0' + remain;
        }

        num = num / base;
    }

    if(11 == idx)
        str[--idx] = '0';

    return &str[idx];
}

int simple_printf(const char *fmt, ...)
{
    va_list args;
    const char *p;
    char sbuf[12];
    char *sval;
    unsigned int ival;
    int len;
    int idx;
    int tmp;

    va_start(args, fmt);

    for(p = fmt; *p; p++)
    {
        if(*p != '%')
        {
            outbyte(*p);
            continue;
        }

        switch (*++p)
        {
            case 'u':
                ival = va_arg(args, unsigned int);
                sval = uitoa(ival, 10, &sbuf);

                len = strlen(sval);
                for(idx = 0; idx <len; ++idx)
                    outbyte(sval[idx]);

                break;

            case 's':
                sval = va_arg(args, char *);

                len = strlen(sval);
                for(idx = 0; idx <len; ++idx)
                    outbyte(sval[idx]);

                break;

            case 'x':
                ival = va_arg(args, unsigned int);
                sval = uitoa(ival, 16, &sbuf);

                len = strlen(sval);
                for(idx = 0; idx <len; ++idx)
                    outbyte(sval[idx]);

                break;

            default:
                outbyte(*p);
                break;
        }
    }

    va_end(args);

    return 0;
}