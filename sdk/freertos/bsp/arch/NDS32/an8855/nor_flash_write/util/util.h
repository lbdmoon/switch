#include <stdarg.h>
#define SPI_DRIVER_DEBUG_MODE 0
#define VPint   *(volatile unsigned int *)

#if SPI_DRIVER_DEBUG_MODE
#define SPI_DEBUG(...) simple_printf(__VA_ARGS__)
#else
#define SPI_DEBUG(...)
#endif

unsigned int io_read32(unsigned int addr);
void io_write32(unsigned int addr, unsigned int vlaue);
int outbyte(char c);
int memRead32(unsigned int addr, unsigned int *value_p);
void memWrite32(unsigned int addr, unsigned int value);
static int isDmemAddr(unsigned int addr);
static int isRegAddr(unsigned int addr);
unsigned int ramRead32(unsigned int addr);
static void serial_outc(char c);
int simple_printf(const char *fmt, ...);