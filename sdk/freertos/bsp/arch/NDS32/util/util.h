#ifndef __NDS32_UTIL_H__
#define __NDS32_UTIL_H__

#include <nds32_intrinsic.h>

#define get_pfm_cnt0 __nds32__mfsr(NDS32_SR_PFMC0)
#define get_pfm_cnt1 __nds32__mfsr(NDS32_SR_PFMC1)
#define get_pfm_cnt2 __nds32__mfsr(NDS32_SR_PFMC2)

extern void startPFM(unsigned int counter, unsigned int event);
extern void stopPFM(unsigned int counter);

#endif

