
#include <nds32_intrinsic.h>
#include "nds32_defs.h"
#include <platform.h>
#include <cache.h>

#define PHI_SMI_PMDC_MASK       (~(0x3 << 6))
#define PHI_SMI_PMDC_11_2M      (0xc0)          /* 11.2 MHZ */
#define PHI_SMI_PMDC_5_6M       (0x80)          /* 5.6 MHZ */
#define PHI_SMI_PMDC_2_8M       (0x40)          /* 2.8 MHZ */

extern char __data_start, _edata, __sbss_end;

void rbus_timeout_check(void)
{
    if ((io_read32(TIMEOUT_STS0) & 0x1) == 1)
    {
        printf("\r\nWARNING !!! Rbus timeout happended !!!\r\n");
        printf("Rbus timeout status:0x%x, errAddr:0x%x\r\n", io_read32(TIMEOUT_STS0), io_read32(TIMEOUT_STS1));
        /* clear the status*/
        io_write32(TIMEOUT_STS0, 0);
        io_write32(TIMEOUT_STS1, 0);
    }
}

void rbus_timeout_init(void)
{
    rbus_timeout_check();

    /* set cmd/wdata/rdata timeout_cnt as 100 ms */
    io_write32(TIMEOUT_CFG0, DMEM_CTRL_CLK/10);
    io_write32(TIMEOUT_CFG1, DMEM_CTRL_CLK/10);
    io_write32(TIMEOUT_CFG2, DMEM_CTRL_CLK/10);

    /* enable timeout.
     * Note: ISR won't work due to data/bss section in DMEM */
    io_write32(TIMEOUT_STS0, 0x80000000);

    return;
}

void air_pbus_tout_isr(void)
{
    int isBusTout=0;
    unsigned int errAddr;


    isBusTout = (io_read32(PB_TIMEOUT_INT)&0x1);
    io_write32(PB_TIMEOUT_INT, 1); /* clear intr */

    errAddr = io_read32(PB_TIMEOUT_INFO);

    if (isBusTout)
        printf("\n\npbus timeout interrupt errAddr=%08lx\n\n", errAddr);
    else
        printf("\n\nunknown bus timeout interrupt errAddr=%08lx\n\n", errAddr);

    return;
}

void pbus_timeout_init(void)
{
    unsigned int val;

    register_isr(IRQ_BUS_TOUT, air_pbus_tout_isr);

    val = BUSCLKFREQ/10; /* timeout time: 100 ms */
    io_write32(PB_TIMEOUT_CFG, val);

    val |= (1<<31); /* enable pbus timeout */
    io_write32(PB_TIMEOUT_CFG, val);

    return;
}

void rbus_rdbypasswt_init(void)
{
    unsigned int line_size = CACHE_LINE_SIZE(DCACHE);
    unsigned int mask = ALL_FF-(line_size-1);
    unsigned int val;


    /* disable rdbypasswt before setting mask */
    val = io_read32(RDBYPASSWT_CFG);
    val &= (~0x1);
    io_write32(RDBYPASSWT_CFG, val);

    /* set rdbypasswt mask according to dcache line size */
    io_write32(RDBYPASSWT_MASK, mask);

    /* enable rdbypasswt.
     * Note: don't change cmd_fifo and rdcmd_fifo, otherwise, HW will go wrong */
    val = io_read32(RDBYPASSWT_CFG);
    val |= (0x1);
    io_write32(RDBYPASSWT_CFG, val);

    //printf("RDBYPASSWT_CFG:0x%x, RDBYPASSWT_MASK:0x%x\n", io_read32(RDBYPASSWT_CFG), io_read32(RDBYPASSWT_MASK));

    return;
}

void airInitHardware( void )
{
    unsigned int val;
    uart_init();
    printf("\n[%s] __data_start:0x%x, _edata:0x%x, __sbss_end:0x%x\n",
            __func__, (unsigned int)&__data_start, (unsigned int)&_edata, (unsigned int)&__sbss_end);
    rbus_timeout_init();
    rbus_rdbypasswt_init();
    pbus_timeout_init();

    /* enable timers */
    timer_init(0, 1, 1000); /* enable timer0 for delay1ms */

    /* set MDC clock */
    val = io_read32(CR_PHY_SMI);
    val = (val & PHI_SMI_PMDC_MASK) | (PHI_SMI_PMDC_2_8M);
    io_write32(CR_PHY_SMI, val);

    init_interrupts_count();
}


