#include <stdio.h>
void main(int argc, char *argv[])
{
    int size;
    char str[4];
    int ret = 0;
    FILE *rfp;
    FILE *wfp;
    char c;
    printf("Raw Image %s\n",argv[1]);
    printf("Output Image %s\n",argv[2]);
    rfp = fopen(argv[1],"rb");
    wfp = fopen(argv[2],"wb");

    fread(str,3, 1, rfp);
    ret= strncmp(str, "air",3);
    if (ret == 0)
    {
        fclose(rfp);
        fclose(wfp);
    }

    fseek(rfp, 0, SEEK_END);
    size = (int) ftell(rfp);
    fseek(rfp, 0, SEEK_SET);
    printf("Size = %0d\n", size);

    fprintf(wfp,"%s","air");
    fwrite(&size, 1, sizeof(size), wfp);

    while(!feof(rfp))
    {
        fread(&c, 1, 1, rfp);
        fwrite(&c, 1, 1, wfp);
    }

    fclose(rfp);
    fclose(wfp);
}
