################################################################################
ARCHIVE_NAME            := air_sdk

################################################################################
TGT_OBJS_TOTAL          := $(addprefix $(AIR_SYSTEM_BUILD)/,$(subst $(AIR_SYSTEM),,$(OBJS_TOTAL)))
TGT_DEPS_TOTAL          := $(addprefix $(AIR_SYSTEM_BUILD)/,$(subst $(AIR_SYSTEM),,$(DEPS_TOTAL)))
HW_UTIL_LIB             := $(AIR_SYSTEM)/air_sdk/lib/cmlib_hw_util.a
################################################################################
# Standard things
sinclude $(TGT_DEPS_TOTAL)

ifeq ($(findstring SHARED_LIB,$(BUILD_TYPE)),SHARED_LIB)
LIB_OPTION              := -fPIC
else
LIB_OPTION              :=
endif
CFLAGS += -I$(OS_PATH)/kernel/include \
          -I$(OS_PATH)/bsp \
          -I$(OS_PATH)/bsp/include \
          -I$(OS_PATH)/bsp/arch/NDS32/GCC \
          -I$(OS_PATH)/bsp/arch/NDS32/en8851
################################################################################
compile:: $(TGT_OBJS_TOTAL)

ifeq ($(findstring SHARED_LIB,$(BUILD_TYPE)),SHARED_LIB)
	$(CC) -shared $(CFLAGS) -o  $(AIR_SYSTEM_BUILD)/image/lib$(ARCHIVE_NAME).so $(TGT_OBJS_TOTAL)
else
	$(AR) -cr $(AIR_SYSTEM_BUILD)/image/$(ARCHIVE_NAME).a $(TGT_OBJS_TOTAL) $(HW_UTIL_LIB)
endif

$(AIR_SYSTEM_BUILD)/%.o: ../%.c
	$(TEST_PATH) $(dir $@) || $(MKDIR) $(dir $@)
	-$(CC) $(EXTRA_CFLAGS) $(CFLAGS) $(AIR_CFLAGS) $(LIB_OPTION)  -c -o $@ $< >> $(AIR_LOG) 2>&1
	-$(CC) -MM -MT $@ $(EXTRA_CFLAGS) $(CFLAGS) $(AIR_CFLAGS) $(LIB_OPTION)  $< > $(patsubst %.o,%.d, $@)

clean::
	$(RM) $(TGT_OBJS_TOTAL)
	$(RM) $(TGT_DEPS_TOTAL)
	$(RM) $(AIR_LOG)
ifeq ($(findstring SHARED_LIB,$(BUILD_TYPE)),SHARED_LIB)
	$(RM) $(AIR_SYSTEM_BUILD)/image/lib$(ARCHIVE_NAME).so
else
	$(RM) $(AIR_SYSTEM_BUILD)/image/$(ARCHIVE_NAME).a
endif

install::
