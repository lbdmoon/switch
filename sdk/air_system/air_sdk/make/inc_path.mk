################################################################################
INCS    += -I$(AIR_SDK)/include
INCS    += -I$(AIR_SDK)/src/inc
INCS    += -I$(AIR_SYSTEM)/app/diag_shell/inc
INCS    += -I$(AIR_SYSTEM)/app/sdk_ref
ifeq ($(findstring ENABLED,$(AIR_EN_SFP_LED)),ENABLED)
INCS    += -I$(AIR_SYSTEM)/app/sfp_led
endif
################################################################################
