################################################################################
# Default global configuration
################################################################################
EXTRA_CFLAGS            += $(INCS)

################################################################################
CHIP_MK_LIST            := $(addprefix $(AIR_SDK)/make/,$(addsuffix .mk,$(ENABLED_SWITCH_CHIPS)))

include $(CHIP_MK_LIST)

FEATURE_LIST            := $(sort $(FEATURE_LIST))

EN_FEATURE_LIST         := $(filter-out $(CUSTOM_DISABLE_LIST),$(FEATURE_LIST))

FEATURE_EN_FLAGS        := $(addprefix -DAIR_EN_,$(addsuffix =1,$(EN_FEATURE_LIST)))

EXTRA_CFLAGS            += $(FEATURE_EN_FLAGS)

################################################################################
# Internal features
################################################################################

################################################################################
# External features
################################################################################
# linux: ks or us
ifeq ($(findstring linux,$(OS_TYPE)),linux)
ifeq ($(findstring USER_SPACE,$(WORK_SPACE)),USER_SPACE)
EXTRA_CFLAGS            += -DAIR_LINUX_USER_MODE
endif
ifeq ($(findstring KERNEL_SPACE,$(WORK_SPACE)),KERNEL_SPACE)
EXTRA_CFLAGS            += -DAIR_LINUX_KERNEL_MODE
endif
endif

# freertos:
ifeq ($(findstring freertos,$(OS_TYPE)),freertos)
EXTRA_CFLAGS            += -DAIR_FREERTOS
endif

ifeq ($(findstring ENABLED,$(AIR_EN_DEBUG)),ENABLED)
EXTRA_CFLAGS            += -DAIR_EN_DEBUG
endif

ifeq ($(findstring ENABLED,$(AIR_EN_I2C_PHY)),ENABLED)
EXTRA_CFLAGS            += -DAIR_EN_I2C_PHY
endif

ifeq ($(findstring ENABLED,$(AIR_EN_I2C_TO_I2C_ACCESS)),ENABLED)
EXTRA_CFLAGS            += -DAIR_EN_I2C_TO_I2C_ACCESS
endif

ifeq ($(findstring ENABLED,$(AIR_EN_COMPILER_SUPPORT_FUNCTION)),ENABLED)
EXTRA_CFLAGS            += -DAIR_EN_COMPILER_SUPPORT_FUNCTION
endif

ifeq ($(findstring ENABLED,$(AIR_EN_COMPILER_SUPPORT_LONG_LONG)),ENABLED)
EXTRA_CFLAGS            += -DAIR_EN_COMPILER_SUPPORT_LONG_LONG
endif

ifeq ($(findstring ENABLED,$(AIR_EN_DMA_RESERVED)),ENABLED)
EXTRA_CFLAGS            += -DAIR_EN_DMA_RESERVED
EXTRA_CFLAGS            += -DAIR_DMA_RESERVED_SZ=$(AIR_DMA_RESERVED_SZ)
EXTRA_CFLAGS            += -DAIR_OS_MEMORY_SZ=$(AIR_OS_MEMORY_SZ)
endif

################################################################################
# Endian
################################################################################
ifeq ($(findstring AIR_EN_HOST_32_BIT_BIG_ENDIAN,$(AIR_HOST_ENDIAN)),AIR_EN_HOST_32_BIT_BIG_ENDIAN)
EXTRA_CFLAGS            += -DAIR_EN_HOST_32_BIT_BIG_ENDIAN
EXTRA_CFLAGS            += -DAIR_EN_BIG_ENDIAN
endif

ifeq ($(findstring AIR_EN_HOST_32_BIT_LITTLE_ENDIAN,$(AIR_HOST_ENDIAN)),AIR_EN_HOST_32_BIT_LITTLE_ENDIAN)
EXTRA_CFLAGS            += -DAIR_EN_HOST_32_BIT_LITTLE_ENDIAN
EXTRA_CFLAGS            += -DAIR_EN_LITTLE_ENDIAN
endif

ifeq ($(findstring AIR_EN_HOST_64_BIT_BIG_ENDIAN,$(AIR_HOST_ENDIAN)),AIR_EN_HOST_64_BIT_BIG_ENDIAN)
EXTRA_CFLAGS            += -DAIR_EN_HOST_64_BIT_BIG_ENDIAN
EXTRA_CFLAGS            += -DAIR_EN_BIG_ENDIAN
endif

ifeq ($(findstring AIR_EN_HOST_64_BIT_LITTLE_ENDIAN,$(AIR_HOST_ENDIAN)),AIR_EN_HOST_64_BIT_LITTLE_ENDIAN)
EXTRA_CFLAGS            += -DAIR_EN_HOST_64_BIT_LITTLE_ENDIAN
EXTRA_CFLAGS            += -DAIR_EN_LITTLE_ENDIAN
endif

################################################################################
ifeq ($(findstring ENABLED,$(AIR_EN_64BIT_ADDR)),ENABLED)
EXTRA_CFLAGS            += -DAIR_EN_64BIT_ADDR
endif

################################################################################
ifeq ($(findstring ENABLED,$(AIR_EN_LED_CLK_SYNC)),ENABLED)
EXTRA_CFLAGS            += -DAIR_EN_LED_CLK_SYNC
endif

################################################################################
ifeq ($(findstring ENABLED,$(AIR_EN_SFP_LED)),ENABLED)
EXTRA_CFLAGS            += -DAIR_EN_SFP_LED
endif

################################################################################
ifeq ($(findstring scorpio,$(ENABLED_SWITCH_CHIPS)),scorpio)
EXTRA_CFLAGS            += -DAIR_EN_SCORPIO
endif
ifeq ($(findstring en8801s,$(ENABLED_PHY_CHIPS)),en8801s)
EXTRA_CFLAGS            += -DAIR_EN_EN8801S_PHY
endif
ifeq ($(findstring en8808,$(ENABLED_PHY_CHIPS)),en8808)
EXTRA_CFLAGS            += -DAIR_EN_EN8808_PHY
endif
ifeq ($(findstring en8811h,$(ENABLED_PHY_CHIPS)),en8811h)
EXTRA_CFLAGS            += -DAIR_EN_EN8811H_PHY
endif
ifeq ($(findstring an8801sb,$(ENABLED_PHY_CHIPS)),an8801sb)
EXTRA_CFLAGS            += -DAIR_EN_AN8801SB_PHY
endif
ifeq ($(findstring pearl,$(ENABLED_SWITCH_CHIPS)),pearl)
EXTRA_CFLAGS            += -DAIR_EN_PEARL
EXTRA_CFLAGS            += -DAIR_EN_USERDEFINED_CHIPNUM=1
EXTRA_CFLAGS            += -DAIR_EN_USERDEFINED_PORTNUM=7
endif
ifeq ($(findstring an8804,$(ENABLED_PHY_CHIPS)),an8804)
EXTRA_CFLAGS            += -DAIR_EN_AN8804_PHY
endif
################################################################################
export EXTRA_CFLAGS
