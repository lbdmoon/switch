#ifndef CMLIB_HW_UTIL_H
#define CMLIB_HW_UTIL_H
/* INCLUDE FILE DECLARATIONS
 */
#include <air_types.h>
#include <air_error.h>
#include <hal/common/hal_phy.h>

/* NAMING CONSTANT DECLARATIONS
 */

/* MACRO FUNCTION DECLARATIONS
 */

/* DATA TYPE DECLARATIONS
 */

/* Macros for phy related information */

/* FUNCTION NAME: cmlib_hw_util_triggerCableDiag
 * PURPOSE:
 *      Get cable diagnostic result.
 *
 * INPUT:
 *      unit            --  Device id
 *      port            --  Select port number
 *      pair            --  Select test pair
 *      ptr_result      --  Cable length result
 *
 * OUTPUT:
 *
 * RETURN:
 *      AIR_E_OK
 *
 * NOTES:
 */
AIR_ERROR_NO_T
cmlib_hw_util_triggerCableDiag(
    const UI32_T unit,
    const UI32_T port,
    UI32_T       pair,
    HAL_PHY_CABLE_TEST_RSLT_T *ptr_result);

/* FUNCTION NAME: cmlib_hw_util_getCableDiagRawData
 * PURPOSE:
 *      Get cable diagnostic ec train raw data.
 *
 * INPUT:
 *      unit            --  Device id
 *      port            --  Select port number
 *      pair            --  Select test pair
 *      ptr_data        --  Raw data result
 *
 * OUTPUT:
 *
 * RETURN:
 *      AIR_E_OK
 *
 * NOTES:
 */
AIR_ERROR_NO_T
cmlib_hw_util_getCableDiagRawData(
    const UI32_T unit,
    const UI32_T port,
    UI32_T       pair,
    UI32_T      *ptr_data);

#endif
