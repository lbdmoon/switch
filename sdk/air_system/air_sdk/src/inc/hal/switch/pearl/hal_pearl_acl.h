/*******************************************************************************
*  Copyright Statement:
*  --------------------
*  This software is protected by Copyright and the information contained
*  herein is confidential. The software may not be copied and the information
*  contained herein may not be used or disclosed except with the written
*  permission of Airoha Technology Corp. (C) 2021
*
*  BY OPENING THIS FILE, BUYER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
*  THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("AIROHA SOFTWARE")
*  RECEIVED FROM AIROHA AND/OR ITS REPRESENTATIVES ARE PROVIDED TO BUYER ON
*  AN "AS-IS" BASIS ONLY. AIROHA EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
*  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
*  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
*  NEITHER DOES AIROHA PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
*  SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
*  SUPPLIED WITH THE AIROHA SOFTWARE, AND BUYER AGREES TO LOOK ONLY TO SUCH
*  THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. AIROHA SHALL ALSO
*  NOT BE RESPONSIBLE FOR ANY AIROHA SOFTWARE RELEASES MADE TO BUYER'S
*  SPECIFICATION OR TO CONFORM TO A PARTICULAR STANDARD OR OPEN FORUM.
*
*  BUYER'S SOLE AND EXCLUSIVE REMEDY AND AIROHA'S ENTIRE AND CUMULATIVE
*  LIABILITY WITH RESPECT TO THE AIROHA SOFTWARE RELEASED HEREUNDER WILL BE,
*  AT AIROHA'S OPTION, TO REVISE OR REPLACE THE AIROHA SOFTWARE AT ISSUE,
*  OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY BUYER TO
*  AIROHA FOR SUCH AIROHA SOFTWARE AT ISSUE.
*
*  THE TRANSACTION CONTEMPLATED HEREUNDER SHALL BE CONSTRUED IN ACCORDANCE
*  WITH THE LAWS OF THE STATE OF CALIFORNIA, USA, EXCLUDING ITS CONFLICT OF
*  LAWS PRINCIPLES.  ANY DISPUTES, CONTROVERSIES OR CLAIMS ARISING THEREOF AND
*  RELATED THERETO SHALL BE SETTLED BY ARBITRATION IN SAN FRANCISCO, CA, UNDER
*  THE RULES OF THE INTERNATIONAL CHAMBER OF COMMERCE (ICC).
*
*******************************************************************************/

/* FILE NAME:  hal_pearl_acl.h
 * PURPOSE:
 *  Define ACL module HAL function.
 *
 * NOTES:
 *
 */

#ifndef HAL_PEARL_ACL_H
#define HAL_PEARL_ACL_H

/* INCLUDE FILE DECLARTIONS
*/
#include <air_types.h>
#include <air_error.h>
#include <air_acl.h>
#include <air_port.h>
#include <air_qos.h>
#include <osal/osal.h>

/* NAMING CONSTANT DECLARATIONS
*/
#define HAL_PEARL_MAX_NUM_OF_ACL_ENTRY                (128)
#define HAL_PEARL_MAX_NUM_OF_MIB_ID                   (64)
#define HAL_PEARL_MAX_NUM_OF_UDF_ENTRY                (16)
#define HAL_PEARL_MAX_NUM_OF_METER_ID                 (32)
#define HAL_PEARL_MAX_NUM_OF_TOKEN                    (0xffff)
#define HAL_PEARL_MAX_NUM_OF_VLAN_ID                  (4096)
#define HAL_PEARL_MAX_NUM_OF_TRTCM_ENTRY              (32)
#define HAL_PEARL_MAX_NUM_OF_DROP_PCD                 (8)
#define HAL_PEARL_MAX_NUM_OF_CLASS_SLR                (8)
#define HAL_PEARL_MAX_NUM_OF_USER_PRI                 (8)
#define HAL_PEARL_MAX_NUM_OF_MIRROR_PORT              (0xf)
#define HAL_PEARL_MAX_NUM_OF_ATTACK_RATE_ID           (96)
#define HAL_PEARL_MAX_NUM_OF_OFST_TP                  (8)
#define HAL_PEARL_MAX_NUM_OF_WORD_OFST                (128)
#define HAL_PEARL_MAX_NUM_OF_CMP_SEL                  (2)
#define HAL_PEARL_MAX_NUM_OF_CMP_PAT                  (0xffff)
#define HAL_PEARL_MAX_NUM_OF_CMP_BIT                  (0xffff)
#define HAL_PEARL_MAX_NUM_OF_DMAC_MASK                (0x3f)
#define HAL_PEARL_MAX_NUM_OF_SMAC_MASK                (0x3f)
#define HAL_PEARL_MAX_NUM_OF_ETYPE_MASK               (0x3)
#define HAL_PEARL_MAX_NUM_OF_STAG_MASK                (0x3)
#define HAL_PEARL_MAX_NUM_OF_CTAG_MASK                (0x3)
#define HAL_PEARL_MAX_NUM_OF_DIP_MASK_IPV4            (0xf)
#define HAL_PEARL_MAX_NUM_OF_SIP_MASK_IPV4            (0xf)
#define HAL_PEARL_MAX_NUM_OF_DIP_MASK_IPV6            (0xffff)
#define HAL_PEARL_MAX_NUM_OF_SIP_MASK_IPV6            (0xffff)
#define HAL_PEARL_MAX_NUM_OF_FLOW_LABEL_MASK          (0x7)
#define HAL_PEARL_MAX_NUM_OF_DPORT_MASK               (0x3)
#define HAL_PEARL_MAX_NUM_OF_SPORT_MASK               (0x3)
#define HAL_PEARL_ACL_MAX_BUSY_TIME                   (10)
#define HAL_PEARL_MAX_NUM_OF_CBS                      (0xffff)
#define HAL_PEARL_MAX_NUM_OF_CIR                      (0xffff)
#define HAL_PEARL_MAX_NUM_OF_PBS                      (0xffff)
#define HAL_PEARL_MAX_NUM_OF_PIR                      (0xffff)

/*ACL rule field offset and width*/
#define RULE_TYPE0_OFFSET            (0)
#define DMAC_OFFSET                  (1)
#define SMAC_OFFSET                  (49)
#define STAG_OFFSET                  (97)
#define CTAG_OFFSET                  (113)
#define ETYPE_OFFSET                 (129)
#define DIP_OFFSET                   (145)
#define SIP_OFFSET                   (177)
#define DSCP_OFFSET                  (209)
#define PROTOCOL_OFFSET              (217)
#define DPORT_OFFSET                 (225)
#define SPORT_OFFSET                 (241)
#define UDF_OFFSET                   (257)
#define FIELDMAP_OFFSET              (273)
#define IS_IPV6_OFFSET               (286)
#define PORTMAP_OFFSET               (287)

#define RULE_TYPE1_OFFSET            (0)
#define DIP_IPV6_OFFSET              (1)
#define SIP_IPV6_OFFSET              (97)
#define FLOW_LABEL_OFFSET            (193)

#define RULE_TYPE0_WIDTH            (1)
#define DMAC_WIDTH                  (8)
#define SMAC_WIDTH                  (8)
#define STAG_WIDTH                  (16)
#define CTAG_WIDTH                  (16)
#define ETYPE_WIDTH                 (16)
#define DIP_WIDTH                   (32)
#define SIP_WIDTH                   (32)
#define DSCP_WIDTH                  (8)
#define PROTOCOL_WIDTH              (8)
#define DPORT_WIDTH                 (16)
#define SPORT_WIDTH                 (16)
#define UDF_WIDTH                   (16)
#define FIELDMAP_WIDTH              (13)
#define IS_IPV6_WIDTH               (1)
#define PORTMAP_WIDTH               (7)

#define RULE_TYPE1_WIDTH            (1)
#define DIP_IPV6_WIDTH              (32)
#define SIP_IPV6_WIDTH              (32)
#define FLOW_LABEL_WIDTH            (20)

/*ACL action offset and width*/
#define ACL_VLAN_VID_OFFSET         (0)
#define ACL_VLAN_HIT_OFFSET         (12)
#define ACL_CLASS_IDX_OFFSET        (13)
#define ACL_TCM_OFFSET              (18)
#define ACL_TCM_SEL_OFFSET          (20)
#define ACL_DROP_PCD_G_OFFSET       (21)
#define ACL_DROP_PCD_Y_OFFSET       (24)
#define ACL_DROP_PCD_R_OFFSET       (27)
#define CLASS_SLR_OFFSET            (30)
#define CLASS_SLR_SEL_OFFSET        (33)
#define DROP_PCD_SEL_OFFSET         (34)
#define TRTCM_EN_OFFSET             (35)
#define ACL_MANG_OFFSET             (36)
#define LKY_VLAN_OFFSET             (37)
#define LKY_VLAN_EN_OFFSET          (38)
#define EG_TAG_OFFSET               (39)
#define EG_TAG_EN_OFFSET            (42)
#define PRI_USER_OFFSET             (43)
#define PRI_USER_EN_OFFSET          (46)
#define MIRROR_OFFSET               (47)
#define FW_PORT_OFFSET              (49)
#define PORT_FW_EN_OFFSET           (52)
#define RATE_INDEX_OFFSET           (53)
#define RATE_EN_OFFSET              (58)
#define ATTACK_RATE_ID_OFFSET       (59)
#define ATTACK_RATE_EN_OFFSET       (66)
#define ACL_MIB_ID_OFFSET           (67)
#define ACL_MIB_EN_OFFSET           (73)
#define VLAN_PORT_SWAP_OFFSET       (74)
#define DST_PORT_SWAP_OFFSET        (75)
#define BPDU_OFFSET                 (76)
#define PORT_OFFSET                 (77)
#define PORT_FORCE_OFFSET           (84)

#define ACL_VLAN_VID_WIDTH          (12)
#define ACL_VLAN_HIT_WIDTH          (1)
#define ACL_CLASS_IDX_WIDTH         (5)
#define ACL_TCM_WIDTH               (2)
#define ACL_TCM_SEL_WIDTH           (1)
#define ACL_DROP_PCD_G_WIDTH        (3)
#define ACL_DROP_PCD_Y_WIDTH        (3)
#define ACL_DROP_PCD_R_WIDTH        (3)
#define CLASS_SLR_WIDTH             (3)
#define CLASS_SLR_SEL_WIDTH         (1)
#define DROP_PCD_SEL_WIDTH          (1)
#define TRTCM_EN_WIDTH              (1)
#define ACL_MANG_WIDTH              (1)
#define LKY_VLAN_WIDTH              (1)
#define LKY_VLAN_EN_WIDTH           (1)
#define EG_TAG_WIDTH                (3)
#define EG_TAG_EN_WIDTH             (1)
#define PRI_USER_WIDTH              (3)
#define PRI_USER_EN_WIDTH           (1)
#define MIRROR_WIDTH                (2)
#define FW_PORT_WIDTH               (3)
#define PORT_FW_EN_WIDTH            (1)
#define RATE_INDEX_WIDTH            (5)
#define RATE_EN_WIDTH               (1)
#define ATTACK_RATE_ID_WIDTH        (7)
#define ATTACK_RATE_EN_WIDTH        (1)
#define ACL_MIB_ID_WIDTH            (6)
#define ACL_MIB_EN_WIDTH            (1)
#define VLAN_PORT_SWAP_WIDTH        (1)
#define DST_PORT_SWAP_WIDTH         (1)
#define BPDU_WIDTH                  (1)
#define PORT_WIDTH                  (7)
#define PORT_FORCE_WIDTH            (1)

/*ACL UDF table offset and width*/
#define UDF_RULE_EN_OFFSET          (0)
#define UDF_PKT_TYPE_OFFSET         (1)
#define WORD_OFST_OFFSET            (4)
#define CMP_SEL_OFFSET              (11)
#define CMP_PAT_OFFSET              (32)
#define CMP_MASK_OFFSET             (48)
#define PORT_BITMAP_OFFSET          (64)

#define UDF_RULE_EN_WIDTH           (1)
#define UDF_PKT_TYPE_WIDTH          (3)
#define WORD_OFST_WIDTH             (7)
#define CMP_SEL_WIDTH               (1)
#define CMP_PAT_WIDTH               (16)
#define CMP_MASK_WIDTH              (16)
#define PORT_BITMAP_WIDTH           (29)

/* MACRO FUNCTION DECLARATIONS
*/


/* DATA TYPE DECLARATIONS
*/
typedef enum
{
    HAL_PEARL_ACL_RULE_TYPE_0,
    HAL_PEARL_ACL_RULE_TYPE_1,
    HAL_PEARL_ACL_RULE_TYPE_LAST
}HAL_PEARL_ACL_RULE_TYPE_T;

typedef enum
{
    HAL_PEARL_ACL_RULE_T_CELL,
    HAL_PEARL_ACL_RULE_C_CELL,
    HAL_PEARL_ACL_RULE_TCAM_LAST
}HAL_PEARL_ACL_RULE_TCAM_T;

typedef enum
{
    HAL_PEARL_ACL_MEM_SEL_RULE,
    HAL_PEARL_ACL_MEM_SEL_ACTION,
    HAL_PEARL_ACL_MEM_SEL_LAST
}HAL_PEARL_ACL_MEM_SEL_T;

typedef enum
{
    HAL_PEARL_ACL_MEM_FUNC_READ = 0,
    HAL_PEARL_ACL_MEM_FUNC_WRITE,
    HAL_PEARL_ACL_MEM_FUNC_CLEAR,
    HAL_PEARL_ACL_MEM_FUNC_CONFIG_READ = 4,
    HAL_PEARL_ACL_MEM_FUNC_CONFIG_WRITE,
    HAL_PEARL_ACL_MEM_FUNC_LAST
}HAL_PEARL_ACL_MEM_FUNC_T;

typedef enum
{
    HAL_PEARL_ACL_RULE_CONFIG_ENABLE,
    HAL_PEARL_ACL_RULE_CONFIG_END,
    HAL_PEARL_ACL_RULE_CONFIG_REVERSE,
    HAL_PEARL_ACL_RULE_CONFIG_LAST
}HAL_PEARL_ACL_RULE_CONFIG_T;

typedef enum
{
    HAL_PEARL_ACL_CHECK_ACL,
    HAL_PEARL_ACL_CHECK_UDF,
    HAL_PEARL_ACL_CHECK_TRTCM,
    HAL_PEARL_ACL_CHECK_METER,
    HAL_PEARL_ACL_CHECK_TYPE_LAST
}HAL_PEARL_ACL_CHECK_TYPE_T;

typedef enum
{
    HAL_PEARL_ACL_DMAC,
    HAL_PEARL_ACL_SMAC,  
    HAL_PEARL_ACL_STAG,
    HAL_PEARL_ACL_CTAG,
    HAL_PEARL_ACL_ETYPE,
    HAL_PEARL_ACL_DIP,
    HAL_PEARL_ACL_SIP,
    HAL_PEARL_ACL_DSCP,
    HAL_PEARL_ACL_PROTOCOL,    
    HAL_PEARL_ACL_DPORT,
    HAL_PEARL_ACL_SPORT,
    HAL_PEARL_ACL_UDF,
    HAL_PEARL_ACL_FLOW_LABEL,
    HAL_PEARL_ACL_FIELD_TYPE_LAST
}HAL_PEARL_ACL_FIELD_TYPE_T;

typedef enum
{
    HAL_PEARL_ACL_ACT_FWD_DIS,             /* Don't change forwarding behaviour by ACL */
    HAL_PEARL_ACL_ACT_FWD_CPU_EX = 4,      /* Forward by system default & CPU port is excluded */
    HAL_PEARL_ACL_ACT_FWD_CPU_IN,          /* Forward by system default & CPU port is included */
    HAL_PEARL_ACL_ACT_FWD_CPU,             /* Forward to CPU port only */
    HAL_PEARL_ACL_ACT_FWD_DROP,            /* Frame dropped */
    HAL_PEARL_ACL_ACT_FWD_LAST
}HAL_PEARL_ACL_ACT_FWD_T;

typedef enum
{
    HAL_PEARL_ACL_ACT_EGTAG_DIS,
    HAL_PEARL_ACL_ACT_EGTAG_CONSISTENT,
    HAL_PEARL_ACL_ACT_EGTAG_UNTAG = 4,
    HAL_PEARL_ACL_ACT_EGTAG_SWAP,
    HAL_PEARL_ACL_ACT_EGTAG_TAG,
    HAL_PEARL_ACL_ACT_EGTAG_STACK,
    HAL_PEARL_ACL_ACT_EGTAG_LAST
}HAL_PEARL_ACL_ACT_EGTAG_T;

typedef enum
{
    HAL_PEARL_ACL_ACT_USR_TCM_DEFAULT,         /* Normal packets, don't work based on color */
    HAL_PEARL_ACL_ACT_USR_TCM_GREEN,           /* Green */
    HAL_PEARL_ACL_ACT_USR_TCM_YELLOW,          /* Yellow */
    HAL_PEARL_ACL_ACT_USR_TCM_RED,             /* Red */
    HAL_PEARL_ACL_ACT_USR_TCM_LAST
}HAL_PEARL_ACL_ACT_USR_TCM_T;

typedef struct HAL_PEARL_ACL_FIELD_S
{
    UI8_T dmac[6];
    UI8_T smac[6];
    UI16_T stag;
    UI16_T ctag;
    UI16_T etype;
    UI32_T dip[4];
    UI32_T sip[4];
    UI8_T dscp;
    UI8_T protocol;
    UI32_T flow_label;
    UI16_T dport;
    UI16_T sport;
    UI16_T udf;
    BOOL_T isipv6;
    UI16_T fieldmap;
    AIR_PORT_BITMAP_T portmap;
} HAL_PEARL_ACL_FIELD_T;

typedef struct HAL_PEARL_ACL_CTRL_S
{
    BOOL_T rule_en;
    BOOL_T reverse;
    BOOL_T end;
}HAL_PEARL_ACL_CTRL_T;

typedef struct HAL_PEARL_ACL_RULE_S
{
    HAL_PEARL_ACL_FIELD_T key;
    HAL_PEARL_ACL_FIELD_T mask;
    HAL_PEARL_ACL_CTRL_T ctrl;
} HAL_PEARL_ACL_RULE_T;

typedef struct HAL_PEARL_ACL_ACT_TRTCM_S
{
    BOOL_T cls_slr_sel;                     /* FALSE: Select original class selector value
                                               TRUE:  Select ACL control table defined class selector value */
    UI8_T cls_slr;                          /* User defined class selector */

    BOOL_T drop_pcd_sel;                    /* FALSE: Select original drop precedence value
                                               TRUE:  Select ACL control table defined drop precedence value */
    UI8_T drop_pcd_r;                       /* User defined drop precedence for red packets */
    UI8_T drop_pcd_y;                       /* User defined drop precedence for yellow packets */
    UI8_T drop_pcd_g;                       /* User defined drop precedence for green packets */

    BOOL_T tcm_sel;                         /* FALSE: Select user defined color value
                                               TRUE:  Select color remark by trtcm table */
    HAL_PEARL_ACL_ACT_USR_TCM_T usr_tcm;          /* User defined color remark */
    UI8_T tcm_idx;                          /* Index for the 32-entries trtcm table */
}HAL_PEARL_ACL_ACT_TRTCM_T;

typedef struct HAL_PEARL_ACL_ACTION_S
{
    BOOL_T port_en;
    BOOL_T dest_port_sel;               /* Swap destination port member by portmap when port_en=1 */
    BOOL_T vlan_port_sel;               /* Swap VLAN port member by portmap when port_en=1 */
    AIR_PORT_BITMAP_T portmap;

    BOOL_T cnt_en;
    UI32_T cnt_idx;                     /* Counter index */

    BOOL_T attack_en;
    UI32_T attack_idx;                  /* Attack rate index */

    BOOL_T rate_en;
    UI32_T rate_idx;                    /* Index of meter table */

    BOOL_T vlan_en;
    UI32_T vlan_idx;                    /* Vid from ACL */

    UI8_T mirrormap;                    /* mirror session bitmap */

    BOOL_T pri_user_en;
    UI8_T pri_user;                     /* User Priority from ACL */

    BOOL_T lyvlan_en;
    BOOL_T lyvlan;                      /* Leaky VLAN */

    BOOL_T mang;                        /* Management frame attribute */

    BOOL_T bpdu;                        /* BPDU frame attribute */

    BOOL_T fwd_en;
    HAL_PEARL_ACL_ACT_FWD_T fwd;              /* Frame TO_CPU Forwarding */

    BOOL_T egtag_en;
    HAL_PEARL_ACL_ACT_EGTAG_T egtag;          /* Egress tag control */

    BOOL_T trtcm_en;
    HAL_PEARL_ACL_ACT_TRTCM_T trtcm;          /* TRTCM control */

}HAL_PEARL_ACL_ACTION_T;

/* EXPORTED SUBPROGRAM SPECIFICATIONS
*/
/* FUNCTION NAME:
 *      hal_pearl_acl_getGlobalState
 * PURPOSE:
 *      Get the ACL global enable state.
 * INPUT:
 *      unit             -- unit id
 * OUTPUT:
 *      ptr_enable       -- enable state
 * RETURN:
 *      AIR_E_OK
 *      AIR_E_BAD_PARAMETER
 * NOTES:
 *      None
 */
AIR_ERROR_NO_T
hal_pearl_acl_getGlobalState(
    const UI32_T         unit,
    BOOL_T               *ptr_enable);

/* FUNCTION NAME:
 *      hal_pearl_acl_setGlobalState
 * PURPOSE:
 *      Set the ACL global enable state.
 * INPUT:
 *      unit        -- unit id
 *      enable      -- enable state of ACL
 * OUTPUT:
 *      None
 * RETURN:
 *      AIR_E_OK
 *      AIR_E_BAD_PARAMETER
 * NOTES:
 *      None
 */
AIR_ERROR_NO_T
hal_pearl_acl_setGlobalState(
    const UI32_T        unit,
    const BOOL_T        enable);

/* FUNCTION NAME:
 *      hal_pearl_acl_getPortState
 * PURPOSE:
 *      Get enable status of ACL on specified port.
 * INPUT:
 *      unit            -- unit id
 *      port            -- port id
 * OUTPUT:
 *      ptr_enable      -- enable state
 * RETURN:
 *      AIR_E_OK
 *      AIR_E_BAD_PARAMETER
 * NOTES:
 *      None
 */
AIR_ERROR_NO_T
hal_pearl_acl_getPortState(
    const UI32_T               unit,
    const UI32_T               port,
    BOOL_T                     *ptr_enable);

/* FUNCTION NAME:
 *      hal_pearl_acl_setPortState
 * PURPOSE:
 *      Set enable state of ACL on specified port.
 * INPUT:
 *      unit            -- unit id
 *      port            -- port id
 *      enable          -- enable state of ACL
 * OUTPUT:
 *      None
 * RETURN:
 *      AIR_E_OK
 *      AIR_E_BAD_PARAMETER
 * NOTES:
 *      None
 */
AIR_ERROR_NO_T
hal_pearl_acl_setPortState(
    const UI32_T               unit,
    const UI32_T               port,
    const BOOL_T               enable);

/* FUNCTION NAME:
 *      hal_pearl_acl_clearAll
 * PURPOSE:
 *      Clear ACL all rule and action HW memory.
 * INPUT:
 *      unit        -- unit id
 * OUTPUT:
 *      None
 * RETURN:
 *      AIR_E_OK
 * NOTES:
 *      None
 */
AIR_ERROR_NO_T
hal_pearl_acl_clearAll(
    const UI32_T   unit);

/* FUNCTION NAME:
 *      hal_pearl_acl_getRule
 * PURPOSE:
 *      Get ACL rule of specified entry index.
 * INPUT:
 *      unit             -- unit id
 *      entry_idx        -- ACL rule entry index
 * OUTPUT:
 *      ptr_rule         -- pointer buffer of rule
 * RETURN:
 *      AIR_E_OK
 *      AIR_E_BAD_PARAMETER
 * NOTES:
 *      None
 */
AIR_ERROR_NO_T
hal_pearl_acl_getRule(
    const UI32_T             unit,
    const UI32_T             entry_idx,
    AIR_ACL_RULE_T           *ptr_rule);

/* FUNCTION NAME:
 *      hal_pearl_acl_setRule
 * PURPOSE:
 *      Add/Update ACL rule of specified entry index.
 * INPUT:
 *      unit             -- unit id
 *      entry_idx        -- ACL rule entry index
 *      ptr_rule         -- pointer buffer of rule
 * OUTPUT:
 *      None
 * RETURN:
 *      AIR_E_OK
 *      AIR_E_BAD_PARAMETER
 * NOTES:
 *      1.If isipv6=1, dip&sip&dscp&next header will hit only when packet is IPv6;
 *      2.Every 4 rule can do aggregation. For rule aggregation, final hit id rule-end must be 1;
 *        When rule-end=0, means that it can be aggregated with next rule;
 *        Rule3 cannot aggregate with rule4, if rule3 end=0, rule3 will be an invalid rule;
 *      3.For reverse function use, do reverse before rule aggregation.
 *      4.When byte mask of each field is set 0, key of each field must be set 0;
 *      5.Udf-list is bit mask of udf-rule entry, range 1-16;
 */
AIR_ERROR_NO_T
hal_pearl_acl_setRule(
    const UI32_T            unit,
    const UI32_T            entry_idx,
    AIR_ACL_RULE_T          *ptr_rule);

/* FUNCTION NAME:
 *      hal_pearl_acl_delRule
 * PURPOSE:
 *      Delete ACL rule of specified entry index.
 * INPUT:
 *      unit             -- unit id
 *      entry_idx        -- ACL rule entry index
 * OUTPUT:
 *      None
 * RETURN:
 *      AIR_E_OK
 *      AIR_E_BAD_PARAMETER
 * NOTES:
 *      None
 */
AIR_ERROR_NO_T
hal_pearl_acl_delRule(
    const UI32_T            unit,
    const UI32_T            entry_idx);

/* FUNCTION NAME:
 *      hal_pearl_acl_getAction
 * PURPOSE:
 *      Get ACL action of specified entry index.
 * INPUT:
 *      unit              -- unit id
 *      entry_idx         -- ACL action entry index
 * OUTPUT:
 *      ptr_action        -- pointer buffer of action
 * RETURN:
 *      AIR_E_OK
 *      AIR_E_BAD_PARAMETER
 * NOTES:
 *      None
 */
AIR_ERROR_NO_T
hal_pearl_acl_getAction(
    const UI32_T                unit,
    const UI32_T                entry_idx,
    AIR_ACL_ACTION_T            *ptr_action);

/* FUNCTION NAME:
 *      hal_pearl_acl_setAction
 * PURPOSE:
 *      Add/Update ACL action of specified entry index.
 * INPUT:
 *      unit             -- unit id
 *      entry_idx        -- ACL action entry index
 *      ptr_action       -- pointer buffer of action
 * OUTPUT:
 *      None
 * RETURN:
 *      AIR_E_OK
 *      AIR_E_BAD_PARAMETER
 * NOTES:
 *      If acl-tcm=defined, the corresponding action is defined-color; If acl-tcm=meter, the corresponding action is meter-id;
 */
AIR_ERROR_NO_T
hal_pearl_acl_setAction(
    const UI32_T                unit,
    const UI32_T                entry_idx,
    AIR_ACL_ACTION_T            *ptr_action);

/* FUNCTION NAME:
 *      hal_pearl_acl_delAction
 * PURPOSE:
 *      Delete ACL action of specified entry index.
 * INPUT:
 *      unit             -- unit id
 *      entry_idx        -- ACL action entry index
 * OUTPUT:
 *      None
 * RETURN:
 *      AIR_E_OK
 *      AIR_E_BAD_PARAMETER
 * NOTES:
 *      None
 */
AIR_ERROR_NO_T
hal_pearl_acl_delAction(
    const UI32_T         unit,
    const UI32_T         entry_idx);

/* FUNCTION NAME:
 *      hal_pearl_acl_getMibCnt
 * PURPOSE:
 *      Get ACL mib counter.
 * INPUT:
 *      unit             -- unit id
 *      cnt_index        -- mib counter index
 * OUTPUT:
 *      ptr_cnt          -- pointer to receive count
 * RETURN:
 *      AIR_E_OK
 *      AIR_E_BAD_PARAMETER
 * NOTES:
 *      ACL has total 64 mib counters, the counter index can be set by hal_pearl_acl_setAction.
 */
AIR_ERROR_NO_T
hal_pearl_acl_getMibCnt(
    const UI32_T        unit,
    const UI32_T        cnt_index,
    UI32_T              *ptr_cnt);

/* FUNCTION NAME:
 *      hal_pearl_acl_clearMibCnt
 * PURPOSE:
 *      Clear ACL mib counter.
 * INPUT:
 *      unit             -- unit id
 * OUTPUT:
 *      None
 * RETURN:
 *      AIR_E_OK
 *      AIR_E_BAD_PARAMETER
 * NOTES:
 *      For clear all mib counters, ACL has total 64 mib counters, the counter index can be set by hal_pearl_acl_setAction.
 */
AIR_ERROR_NO_T
hal_pearl_acl_clearMibCnt(
    const UI32_T        unit);

/* FUNCTION NAME:
 *      hal_pearl_acl_getUdfRule
 * PURPOSE:
 *      Get ACL UDF rule of specified entry index.
 * INPUT:
 *      unit             -- unit id
 *      entry_idx        -- ACLUDF table entry index
 * OUTPUT:
 *      ptr_udf_rule     -- pointer buffer of rule
 * RETURN:
 *      AIR_E_OK
 *      AIR_E_BAD_PARAMETER
 * NOTES:
 *      None
 */
AIR_ERROR_NO_T
hal_pearl_acl_getUdfRule(
    const UI32_T                unit,
    const UI8_T                 entry_idx,
    AIR_ACL_UDF_RULE_T          *ptr_udf_rule);

/* FUNCTION NAME:
 *      hal_pearl_acl_setUdfRule
 * PURPOSE:
 *      Set ACL UDF rule of specified entry index.
 * INPUT:
 *      unit             -- unit id
 *      entry_idx        -- ACLUDF table entry index
 *      ptr_udf_rule     -- pointer buffer of rule
 * OUTPUT:
 *      None
 * RETURN:
 *      AIR_E_OK
 *      AIR_E_BAD_PARAMETER
 * NOTES:
 *      If cmp=pattern, the corresponding setting is cmp-key&cmp-mask; If cmp=threshold, the corresponding setting is cmp-low&cmp-high;
 */
AIR_ERROR_NO_T
hal_pearl_acl_setUdfRule(
    const UI32_T                unit,
    const UI8_T                 entry_idx,
    AIR_ACL_UDF_RULE_T          *ptr_udf_rule);

/* FUNCTION NAME:
 *      hal_pearl_acl_delUdfRule
 * PURPOSE:
 *      Delete ACL UDF rule of specified entry index.
 * INPUT:
 *      unit             -- unit id
 *      entry_idx        -- ACLUDF table entry index
 * OUTPUT:
 *      None
 * RETURN:
 *      AIR_E_OK
 *      AIR_E_BAD_PARAMETER
 * NOTES:
 *      None
 */
AIR_ERROR_NO_T
hal_pearl_acl_delUdfRule(
    const UI32_T      unit,
    const UI8_T       entry_idx);

/* FUNCTION NAME:
 *      hal_pearl_acl_clearUdfRule
 * PURPOSE:
 *      Clear acl all udf rule.
 * INPUT:
 *      unit             -- unit id
 * OUTPUT:
 *      None
 * RETURN:
 *      AIR_E_OK
 *      AIR_E_BAD_PARAMETER
 * NOTES:
 *      None
 */
AIR_ERROR_NO_T
hal_pearl_acl_clearUdfRule(
    const UI32_T    unit);

/* FUNCTION NAME:
 *      hal_pearl_acl_getMeterTable
 * PURPOSE:
 *      get meter table configuration.
 * INPUT:
 *      unit                -- unit id
 *      meter_id            -- meter id
 * OUTPUT:
 *      ptr_enable          -- meter enable state
 *      ptr_rate            -- ratelimit(unit:64kbps)
 * RETURN:
 *      AIR_E_OK
 *      AIR_E_BAD_PARAMETER
 * NOTES:
 *      None
 */
AIR_ERROR_NO_T
hal_pearl_acl_getMeterTable(
    const UI32_T            unit,
    const UI32_T            meter_id,
    BOOL_T                  *ptr_enable,
    UI32_T                  *ptr_rate);

/* FUNCTION NAME:
 *      hal_pearl_acl_setMeterTable
 * PURPOSE:
 *      Set flow ingress rate limit by meter table.
 * INPUT:
 *      unit                -- unit id
 *      meter_id            -- meter id
 *      enable              -- meter enable state
 *      rate                -- ratelimit(unit:64kbps)
 * OUTPUT:
 *      None
 * RETURN:
 *      AIR_E_OK
 *      AIR_E_BAD_PARAMETER
 * NOTES:
 *      None
 */
AIR_ERROR_NO_T
hal_pearl_acl_setMeterTable(
    const UI32_T            unit,
    const UI32_T            meter_id,
    const BOOL_T            enable,
    const UI32_T            rate);

/* FUNCTION NAME:
 *      hal_pearl_acl_getDropEn
 * PURPOSE:
 *      Get enable state of drop precedence on specified port.
 * INPUT:
 *      unit            -- unit id
 *      port            -- port id
 * OUTPUT:
 *      ptr_enable      -- enable state
 * RETURN:
 *      AIR_E_OK
 *      AIR_E_BAD_PARAMETER
 * NOTES:
 *      None
 */
AIR_ERROR_NO_T
hal_pearl_acl_getDropEn(
    const UI32_T       unit,
    const UI32_T       port,
    BOOL_T             *ptr_enable);

/* FUNCTION NAME:
 *      hal_pearl_acl_setDropEn
 * PURPOSE:
 *      Set enable state of drop precedence on specified port.
 * INPUT:
 *      unit        -- unit id
 *      port        -- port id
 *      enable      -- enable state of drop precedence
 * OUTPUT:
 *      None
 * RETURN:
 *      AIR_E_OK
 *      AIR_E_BAD_PARAMETER
 * NOTES:
 *      None
 */
AIR_ERROR_NO_T
hal_pearl_acl_setDropEn(
    const UI32_T        unit,
    const UI32_T        port,
    const BOOL_T        enable);

/* FUNCTION NAME:
 *      hal_pearl_acl_getDropThrsh
 * PURPOSE:
 *      Get ACL drop threshold.
 * INPUT:
 *      unit            --  unit id
 *      port            --  port id
 *      color           --  AIR_ACL_DP_COLOR_GREEN : Green
 *                          AIR_ACL_DP_COLOR_YELLOW: Yellow
 *                          AIR_ACL_DP_COLOR_RED   : Red
 *      queue           --  Output queue number
 * OUTPUT:
 *      ptr_high        --  High threshold
 *      ptr_low         --  Low threshold
 * RETURN:
 *      AIR_E_OK
 *      AIR_E_BAD_PARAMETER
 * NOTES:
 *      Key parameter include port, color, queue.
 */
AIR_ERROR_NO_T
hal_pearl_acl_getDropThrsh(
    const UI32_T             unit,
    const UI32_T             port,
    const AIR_ACL_DP_COLOR_T color,
    const UI8_T              queue,
    UI32_T                   *ptr_high,
    UI32_T                   *ptr_low);

/* FUNCTION NAME:
 *      hal_pearl_acl_setDropThrsh
 * PURPOSE:
 *      Set ACL drop threshold.
 * INPUT:
 *      unit            --  unit id
 *      port            --  port id
 *      color           --  AIR_ACL_DP_COLOR_GREEN : Green
 *                          AIR_ACL_DP_COLOR_YELLOW: Yellow
 *                          AIR_ACL_DP_COLOR_RED   : Red
 *      queue           --  Output queue number
 *      high            --  High threshold
 *      low             --  Low threshold
 * OUTPUT:
 *      None
 * RETURN:
 *      AIR_E_OK
 *      AIR_E_BAD_PARAMETER
 * NOTES:
 *      Key parameter include port, color, queue.
 */
AIR_ERROR_NO_T
hal_pearl_acl_setDropThrsh(
    const UI32_T             unit,
    const UI32_T             port,
    const AIR_ACL_DP_COLOR_T color,
    const UI8_T              queue,
    const UI32_T             high,
    const UI32_T             low);

/* FUNCTION NAME:
 *      hal_pearl_acl_getDropPbb
 * PURPOSE:
 *      Get ACL drop probability.
 * INPUT:
 *      unit            --  unit id
 *      port            --  port id
 *      color           --  AIR_ACL_DP_COLOR_GREEN : Green
 *                          AIR_ACL_DP_COLOR_YELLOW: Yellow
 *                          AIR_ACL_DP_COLOR_RED   : Red
 *      queue           --  Output queue number
 * OUTPUT:
 *      ptr_pbb         --  Drop probability(unit:1/1023)
 * RETURN:
 *      AIR_E_OK
 *      AIR_E_BAD_PARAMETER
 * NOTES:
 *      Key parameter include port, color, queue.
 */
AIR_ERROR_NO_T
hal_pearl_acl_getDropPbb(
    const UI32_T             unit,
    const UI32_T             port,
    const AIR_ACL_DP_COLOR_T color,
    const UI8_T              queue,
    UI32_T                   *ptr_pbb);

/* FUNCTION NAME:
 *      hal_pearl_acl_setDropPbb
 * PURPOSE:
 *      Set ACL drop probability.
 * INPUT:
 *      unit            --  unit id
 *      port            --  port id
 *      color           --  AIR_ACL_DP_COLOR_GREEN : Green
 *                          AIR_ACL_DP_COLOR_YELLOW: Yellow
 *                          AIR_ACL_DP_COLOR_RED   : Red
 *      queue           --  Output queue number
 *      pbb             --  Drop probability(unit:1/1023)
 * OUTPUT:
 *      None
 * RETURN:
 *      AIR_E_OK
 *      AIR_E_BAD_PARAMETER
 * NOTES:
 *      Key parameter include port, color, queue.
 */
AIR_ERROR_NO_T
hal_pearl_acl_setDropPbb(
    const UI32_T             unit,
    const UI32_T             port,
    const AIR_ACL_DP_COLOR_T color,
    const UI8_T              queue,
    const UI32_T             pbb);

/* FUNCTION NAME:
 *      hal_pearl_acl_getDropExMfrm
 * PURPOSE:
 *      Exclude/Include management frames to drop precedence control.
 * INPUT:
 *      unit            -- unit id
 * OUTPUT:
 *      ptr_type        -- TRUE: Exclude management frame
 *                         FALSE:Include management frame
 * RETURN:
 *      AIR_E_OK
 *      AIR_E_BAD_PARAMETER
 * NOTES:
 *      None
 */
AIR_ERROR_NO_T
hal_pearl_acl_getDropExMfrm(
    const UI32_T         unit,
    BOOL_T               *ptr_type);

/* FUNCTION NAME:
 *      hal_pearl_acl_setDropExMfrm
 * PURPOSE:
 *      Exclude/Include management frames to drop precedence control.
 * INPUT:
 *      unit            --  unit id
 *      type            --  TRUE: Exclude management frame
 *                          FALSE:Include management frame
 * OUTPUT:
 *      None
 * RETURN:
 *      AIR_E_OK
 *      AIR_E_BAD_PARAMETER
 * NOTES:
 *      None
 */
AIR_ERROR_NO_T
hal_pearl_acl_setDropExMfrm(
    const UI32_T         unit,
    const BOOL_T         type);

/* FUNCTION NAME:
 *      hal_pearl_acl_getTrtcmEn
 * PURPOSE:
 *      Get TRTCM enable status.
 *
 * INPUT:
 *      unit            --  Device ID
 *
 * OUTPUT:
 *      ptr_enable      --  Enable/Disable trTCM
 *
 * RETURN:
 *      AIR_E_OK
 *      AIR_E_BAD_PARAMETER
 *      AIR_E_TIMEOUT
 *
 * NOTES:
 *      None
 */
AIR_ERROR_NO_T
hal_pearl_acl_getTrtcmEn(
    const UI32_T unit,
    BOOL_T *ptr_enable);

/* FUNCTION NAME:
 *      hal_pearl_acl_setTrtcmEn
 * PURPOSE:
 *      Set TRTCM enable status.
 *
 * INPUT:
 *      unit            --  Device ID
 *      enable          --  Enable/Disable TRTCM
 *
 * OUTPUT:
 *      None
 *
 * RETURN:
 *      AIR_E_OK
 *      AIR_E_BAD_PARAMETER
 *      AIR_E_TIMEOUT
 *
 * NOTES:
 *      None
 */
AIR_ERROR_NO_T
hal_pearl_acl_setTrtcmEn(
    const UI32_T unit,
    BOOL_T       enable);

/* FUNCTION NAME:
 *      hal_pearl_acl_getTrtcmTable
 * PURPOSE:
 *      Get a trTCM entry with the specific index.
 *
 * INPUT:
 *      unit            --  Device ID
 *      tcm_idx         --  Index of trTCM entry
 *
 * OUTPUT:
 *      ptr_tcm         --  Structure of trTCM entry
 *
 * RETURN:
 *      AIR_E_OK
 *      AIR_E_BAD_PARAMETER
 *      AIR_E_TIMEOUT
 *
 * NOTES:
 *      None
 */
AIR_ERROR_NO_T
hal_pearl_acl_getTrtcmTable(
    const UI32_T unit,
    const UI32_T tcm_idx,
    AIR_ACL_TRTCM_T *ptr_tcm);

/* FUNCTION NAME:
 *      hal_pearl_acl_setTrtcmTable
 * PURPOSE:
 *      Set a trTCM entry with the specific index.
 *
 * INPUT:
 *      unit            --  Device ID
 *      tcm_idx         --  Index of trTCM entry
 *      ptr_tcm         --  Structure of trTCM entry
 *
 * OUTPUT:
 *      None
 *
 * RETURN:
 *      AIR_E_OK
 *      AIR_E_BAD_PARAMETER
 *      AIR_E_TIMEOUT
 *
 * NOTES:
 *      None
 */
AIR_ERROR_NO_T
hal_pearl_acl_setTrtcmTable(
    const UI32_T unit,
    const UI32_T tcm_idx,
    AIR_ACL_TRTCM_T *ptr_tcm);

/* FUNCTION NAME:
 *      hal_pearl_acl_init
 * PURPOSE:
 *      This API is used to init acl.
 * INPUT:
 *      unit             -- unit id
 * OUTPUT:
 *      None
 * RETURN:
 *      AIR_E_OK
 *      AIR_E_BAD_PARAMETER
 * NOTES:
 *      None
 */
AIR_ERROR_NO_T
hal_pearl_acl_init(
    const UI32_T      unit);

#endif /* end of HAL_PEARL_ACL_H */
