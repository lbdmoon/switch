/*******************************************************************************
*  Copyright Statement:
*  --------------------
*  This software is protected by Copyright and the information contained
*  herein is confidential. The software may not be copied and the information
*  contained herein may not be used or disclosed except with the written
*  permission of Airoha Technology Corp. (C) 2021
*
*  BY OPENING THIS FILE, BUYER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
*  THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("AIROHA SOFTWARE")
*  RECEIVED FROM AIROHA AND/OR ITS REPRESENTATIVES ARE PROVIDED TO BUYER ON
*  AN "AS-IS" BASIS ONLY. AIROHA EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
*  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
*  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
*  NEITHER DOES AIROHA PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
*  SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
*  SUPPLIED WITH THE AIROHA SOFTWARE, AND BUYER AGREES TO LOOK ONLY TO SUCH
*  THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. AIROHA SHALL ALSO
*  NOT BE RESPONSIBLE FOR ANY AIROHA SOFTWARE RELEASES MADE TO BUYER'S
*  SPECIFICATION OR TO CONFORM TO A PARTICULAR STANDARD OR OPEN FORUM.
*
*  BUYER'S SOLE AND EXCLUSIVE REMEDY AND AIROHA'S ENTIRE AND CUMULATIVE
*  LIABILITY WITH RESPECT TO THE AIROHA SOFTWARE RELEASED HEREUNDER WILL BE,
*  AT AIROHA'S OPTION, TO REVISE OR REPLACE THE AIROHA SOFTWARE AT ISSUE,
*  OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY BUYER TO
*  AIROHA FOR SUCH AIROHA SOFTWARE AT ISSUE.
*
*  THE TRANSACTION CONTEMPLATED HEREUNDER SHALL BE CONSTRUED IN ACCORDANCE
*  WITH THE LAWS OF THE STATE OF CALIFORNIA, USA, EXCLUDING ITS CONFLICT OF
*  LAWS PRINCIPLES.  ANY DISPUTES, CONTROVERSIES OR CLAIMS ARISING THEREOF AND
*  RELATED THERETO SHALL BE SETTLED BY ARBITRATION IN SAN FRANCISCO, CA, UNDER
*  THE RULES OF THE INTERNATIONAL CHAMBER OF COMMERCE (ICC).
*
*******************************************************************************/

/* FILE NAME:  air_sflow.c
 * PURPOSE:
 *    It provide sFlow module API.
 * NOTES:
 *
 */

/* INCLUDE FILE DECLARATIONS
 */
#include <osal/osal.h>
#include <api/diag.h>
#include <hal/common/hal.h>
#include <air_sflow.h>

DIAG_SET_MODULE_INFO(AIR_MODULE_SVLAN, "air_sflow.c");

/* NAMING CONSTANT DECLARATIONS
*/

/* MACRO FUNCTION DECLARATIONS
 */

/* DATA TYPE DECLARATIONS
*/

/* GLOBAL VARIABLE DECLARATIONS
*/
/* DIAG_SET_MODULE_INFO(AIR_MODULE_SFLOW, "air_sflow.c"); */

/* LOCAL SUBPROGRAM DECLARATIONS
*/

/* STATIC VARIABLE DECLARATIONS
*/

/* EXPORTED SUBPROGRAM BODIES
*/
/* FUNCTION NAME:   air_sflow_setSampling
 * PURPOSE:
 *      This API is used to set sFlow sampling rate, number
 * INPUT:
 *      unit                     -- Device ID
 *      port                     -- Index of port number
 *      rate                     -- sFlow sampling rate
 *      number                   -- sFlow sampling number
 * OUTPUT:
 *      None
 * RETURN:
 *      AIR_E_OK                 -- Operation success.
 *      AIR_E_BAD_PARAMETER      -- Parameter is wrong.
 * NOTES:
 *      1.  Don't set sflow sampling on CPU port.
 */
AIR_ERROR_NO_T
air_sflow_setSampling(
    const UI32_T    unit,
    const UI32_T    port,
    const UI32_T    rate,
    const UI32_T    number)
{
    AIR_ERROR_NO_T  rv;

    HAL_CHECK_UNIT(unit);
    HAL_CHECK_ETH_PORT(unit, port);
    rv = HAL_FUNC_CALL(unit, sflow, setSampling, (unit, port, rate, number));
    return rv;
}

/* FUNCTION NAME:   air_sflow_getSampling
 * PURPOSE:
 *      This API is used to get sFlow sampling rate, number
 * INPUT:
 *      unit                     -- Device ID
 *      port                     -- Index of port number
 * OUTPUT:
 *      ptr_rate                 -- sFlow sampling rate
 *      ptr_number               -- sFlow sampling number
 * RETURN:
 *      AIR_E_OK                 -- Operation success.
 *      AIR_E_BAD_PARAMETER      -- Parameter is wrong.
 * NOTES:
 *      None
 */
AIR_ERROR_NO_T
air_sflow_getSampling(
    const UI32_T    unit,
    const UI32_T    port,
    UI32_T  *ptr_rate,
    UI32_T  *ptr_number)
{
    AIR_ERROR_NO_T rv;

    HAL_CHECK_UNIT(unit);
    HAL_CHECK_ETH_PORT(unit, port);
    HAL_CHECK_PTR(ptr_rate);
    HAL_CHECK_PTR(ptr_number);
    rv = HAL_FUNC_CALL(unit, sflow, getSampling, (unit, port, ptr_rate, ptr_number));
    return rv;
}

