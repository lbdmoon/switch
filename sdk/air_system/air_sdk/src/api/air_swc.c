/*******************************************************************************
*  Copyright Statement:
*  --------------------
*  This software is protected by Copyright and the information contained
*  herein is confidential. The software may not be copied and the information
*  contained herein may not be used or disclosed except with the written
*  permission of Airoha Technology Corp. (C) 2021
*
*  BY OPENING THIS FILE, BUYER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
*  THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("AIROHA SOFTWARE")
*  RECEIVED FROM AIROHA AND/OR ITS REPRESENTATIVES ARE PROVIDED TO BUYER ON
*  AN "AS-IS" BASIS ONLY. AIROHA EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
*  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
*  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
*  NEITHER DOES AIROHA PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
*  SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
*  SUPPLIED WITH THE AIROHA SOFTWARE, AND BUYER AGREES TO LOOK ONLY TO SUCH
*  THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. AIROHA SHALL ALSO
*  NOT BE RESPONSIBLE FOR ANY AIROHA SOFTWARE RELEASES MADE TO BUYER'S
*  SPECIFICATION OR TO CONFORM TO A PARTICULAR STANDARD OR OPEN FORUM.
*
*  BUYER'S SOLE AND EXCLUSIVE REMEDY AND AIROHA'S ENTIRE AND CUMULATIVE
*  LIABILITY WITH RESPECT TO THE AIROHA SOFTWARE RELEASED HEREUNDER WILL BE,
*  AT AIROHA'S OPTION, TO REVISE OR REPLACE THE AIROHA SOFTWARE AT ISSUE,
*  OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY BUYER TO
*  AIROHA FOR SUCH AIROHA SOFTWARE AT ISSUE.
*
*  THE TRANSACTION CONTEMPLATED HEREUNDER SHALL BE CONSTRUED IN ACCORDANCE
*  WITH THE LAWS OF THE STATE OF CALIFORNIA, USA, EXCLUDING ITS CONFLICT OF
*  LAWS PRINCIPLES.  ANY DISPUTES, CONTROVERSIES OR CLAIMS ARISING THEREOF AND
*  RELATED THERETO SHALL BE SETTLED BY ARBITRATION IN SAN FRANCISCO, CA, UNDER
*  THE RULES OF THE INTERNATIONAL CHAMBER OF COMMERCE (ICC).
*
*******************************************************************************/

/* FILE NAME:  air_swc.c
 * PURPOSE:
 *    It provide switch module API.
 *
 * NOTES:
 *
 */

/* INCLUDE FILE DECLARATIONS
 */

#include <air_types.h>
#include <air_error.h>
#include <air_init.h>
#include <air_swc.h>
#include <air_port.h>
#include <hal/common/hal.h>

/* GLOBAL VARIABLE DECLARATIONS
 */
DIAG_SET_MODULE_INFO(AIR_MODULE_SWC, "air_swc.c");
/* EXPORTED SUBPROGRAM BODIES
*/

/* FUNCTION NAME:   air_swc_setMgmtFrameCfg
 * PURPOSE:
 *      Set management frame config.
 * INPUT:
 *      unit                     -- Device ID
 *      ptr_cfg                  -- Management frame configuration
 *                                  AIR_SWC_MGMT_FRAME_CFG_T
 * OUTPUT:
 *      None
 * RETURN:
 *      AIR_E_OK                 -- Operation Success.
 *      AIR_E_BAD_PARAMETER      -- Parameter is wrong.
 * NOTES:
 *      REV_xx = 01-80-C2-00-00-xx of destination mac
 *      REV_UN = others
 */
AIR_ERROR_NO_T
air_swc_setMgmtFrameCfg(
    const UI32_T                unit,
    AIR_SWC_MGMT_FRAME_CFG_T    *ptr_cfg)
{
    HAL_CHECK_UNIT(unit);
    HAL_CHECK_PTR(ptr_cfg);

    return HAL_FUNC_CALL(unit, swc, setMgmtFrameCfg, (unit, ptr_cfg));
}

/* FUNCTION NAME:   air_swc_getMgmtFrameCfg
 * PURPOSE:
 *      Get management frame config.
 * INPUT:
 *      unit                     -- Device ID
 * OUTPUT:
 *      ptr_cfg                  -- Management frame configuration
 *                                  AIR_SWC_MGMT_FRAME_CFG_T
 * RETURN:
 *      AIR_E_OK                 -- Operation Success.
 *      AIR_E_BAD_PARAMETER      -- Parameter is wrong.
 * NOTES:
 *      REV_xx = 01-80-C2-00-00-xx of destination mac
 *      REV_UN = others
 */
AIR_ERROR_NO_T
air_swc_getMgmtFrameCfg(
    const UI32_T                unit,
    AIR_SWC_MGMT_FRAME_CFG_T    *ptr_cfg)
{
    HAL_CHECK_UNIT(unit);
    HAL_CHECK_PTR(ptr_cfg);

    return HAL_FUNC_CALL(unit, swc, getMgmtFrameCfg, (unit, ptr_cfg));
}

/* FUNCTION NAME:   air_swc_triggerCableTest
 * PURPOSE:
 *      Get cable status.
 * INPUT:
 *      unit                     -- Device ID
 *      port                     -- Port ID
 *      test_pair                -- Tested cable pair
 *                                  AIR_SWC_CABLE_TEST_PAIR_T
 * OUTPUT:
 *      ptr_test_rslt            -- Cable diagnostic information
 *                                  AIR_SWC_CABLE_TEST_RSLT_T
 * RETURN:
 *      AIR_E_OK                 -- Operation Success.
 *      AIR_E_NOT_SUPPORT        -- Feature is not supported.
 *      AIR_E_BAD_PARAMETER      -- Parameter is wrong.
 * NOTES:
 *      Support cable diagnostic in speed 1G only.
 *
 *      This API will be obsoleted in later release, please use
 *      new APIs designed on air_port.h instead.
 */
AIR_ERROR_NO_T
air_swc_triggerCableTest(
    const UI32_T                unit,
    const UI32_T                port,
    AIR_SWC_CABLE_TEST_PAIR_T   test_pair,
    AIR_SWC_CABLE_TEST_RSLT_T   *ptr_test_rslt)
{
    AIR_ERROR_NO_T ret = AIR_E_OK;
    UI32_T i = 0;
    AIR_PORT_CABLE_TEST_RSLT_T port_test_rslt;

    HAL_CHECK_UNIT(unit);
    HAL_CHECK_ETH_PORT(unit, port);
    HAL_CHECK_PTR(ptr_test_rslt);

    ret =  HAL_FUNC_CALL(unit, port, triggerCableTest, (unit, port, test_pair, &port_test_rslt));
    for (i = 0; i< AIR_SWC_CABLE_MAX_PAIR; i++)
    {
        ptr_test_rslt->status[i] = port_test_rslt.status[i];
        ptr_test_rslt->length[i] = port_test_rslt.length[i];
    }
    return ret;
}

/* FUNCTION NAME:   air_swc_setSystemMac
 * PURPOSE:
 *      Set the system MAC address.
 * INPUT:
 *      unit                     -- Device ID
 *      mac                      -- System MAC address
 * OUTPUT:
 *      None
 * RETURN:
 *      AIR_E_OK                 -- Operation Success.
 *      AIR_E_BAD_PARAMETER      -- Parameter is wrong.
 * NOTES:
 *      It's unique and specified for pause frame.
 */
AIR_ERROR_NO_T
air_swc_setSystemMac(
    const UI32_T    unit,
    const AIR_MAC_T mac)
{
    HAL_CHECK_UNIT(unit);
    HAL_CHECK_PTR(mac);

    return HAL_FUNC_CALL(unit, swc, setSystemMac, (unit, mac));
}

/* FUNCTION NAME:   air_swc_getSystemMac
 * PURPOSE:
 *      Get the system MAC address.
 * INPUT:
 *      unit                     -- Device ID
 * OUTPUT:
 *      mac                      -- System MAC address
 * RETURN:
 *      AIR_E_OK                 -- Operation Success.
 *      AIR_E_BAD_PARAMETER      -- Parameter is wrong.
 * NOTES:
 *      It's unique and specified for pause frame.
 */
AIR_ERROR_NO_T
air_swc_getSystemMac(
    const UI32_T unit,
    AIR_MAC_T    mac)
{
    HAL_CHECK_UNIT(unit);
    HAL_CHECK_PTR(mac);

    return HAL_FUNC_CALL(unit, swc, getSystemMac, (unit, mac));
}

/* FUNCTION NAME:   air_swc_setJumboSize
 * PURPOSE:
 *      Set accepting jumbo frmes with specificied size.
 * INPUT:
 *      unit                     -- Device ID
 *      jumbo_size               -- Maximun jumbo packet size
 *                                  AIR_SWC_JUMBO_SIZE_T
 * OUTPUT:
 *      None
 * RETURN:
 *      AIR_E_OK                 -- Operation Success.
 *      AIR_E_BAD_PARAMETER      -- Parameter is wrong.
 * NOTES:
 */
AIR_ERROR_NO_T
air_swc_setJumboSize(
    const UI32_T                unit,
    const AIR_SWC_JUMBO_SIZE_T  jumbo_size)
{
    HAL_CHECK_UNIT(unit);
    HAL_CHECK_ENUM_RANGE(jumbo_size, (AIR_SWC_JUMBO_SIZE_LAST));

    return HAL_FUNC_CALL(unit, swc, setJumboSize, (unit, jumbo_size));
}

/* FUNCTION NAME:   air_swc_getJumboSize
 * PURPOSE:
 *      Get accepting jumbo frmes with specificied size.
 * INPUT:
 *      unit                     -- Device ID
 * OUTPUT:
 *      ptr_jumbo_size           -- Maximun jumbo packet size
 *                                  AIR_SWC_JUMBO_SIZE_T
 * RETURN:
 *      AIR_E_OK                 -- Operation Success.
 *      AIR_E_BAD_PARAMETER      -- Parameter is wrong.
 * NOTES:
 *      None
 */
AIR_ERROR_NO_T
air_swc_getJumboSize(
    const UI32_T            unit,
    AIR_SWC_JUMBO_SIZE_T    *ptr_jumbo_size)
{
    HAL_CHECK_UNIT(unit);
    HAL_CHECK_PTR(ptr_jumbo_size);

    return HAL_FUNC_CALL(unit, swc, getJumboSize, (unit, ptr_jumbo_size));
}

/* FUNCTION NAME:   air_swc_setProperty
 * PURPOSE:
 *      Set switch property.
 * INPUT:
 *      unit                     -- Device ID
 *      property                 -- Select switch property
 *                                  AIR_SWC_PROPERTY_T
 *      param0                   -- 1: Enable
 *                                  0: Disable
 *      param1                   -- Reserved
 * OUTPUT:
 *      None
 * RETURN:
 *      AIR_E_OK                 -- Operation Success.
 *      AIR_E_BAD_PARAMETER      -- Parameter is wrong.
 * NOTES:
 *      None
 */
AIR_ERROR_NO_T
air_swc_setProperty(
    const UI32_T                unit,
    const AIR_SWC_PROPERTY_T    property,
    const UI32_T                param0,
    const UI32_T                param1)
{
    HAL_CHECK_UNIT(unit);
    HAL_CHECK_ENUM_RANGE(property, AIR_SWC_PROPERTY_LAST);

    return HAL_FUNC_CALL(unit, swc, setProperty, (unit, property, param0, param1));
}

/* FUNCTION NAME:   air_swc_getProperty
 * PURPOSE:
 *      Get switch property.
 * INPUT:
 *      unit                     -- Device ID
 *      property                 -- Select switch property
 *                                  AIR_SWC_PROPERTY_T
 * OUTPUT:
 *      ptr_param0               -- 1: Enable
 *                                  0: Disable
 *      ptr_param1               -- Reserved
 * RETURN:
 *      AIR_E_OK                 -- Operation Success.
 *      AIR_E_BAD_PARAMETER      -- Parameter is wrong.
 * NOTES:
 *      None
 */
AIR_ERROR_NO_T
air_swc_getProperty(
    const UI32_T                unit,
    const AIR_SWC_PROPERTY_T    property,
    UI32_T                      *ptr_param0,
    UI32_T                      *ptr_param1)
{
    HAL_CHECK_UNIT(unit);
    HAL_CHECK_ENUM_RANGE(property, AIR_SWC_PROPERTY_LAST);

    return HAL_FUNC_CALL(unit, swc, getProperty, (unit, property, ptr_param0, ptr_param1));
}

/* FUNCTION NAME:   air_swc_setLoopDetect
 * PURPOSE:
 *      Set loop detection mode.
 * INPUT:
 *      unit                     -- Device ID
 *      port                     -- Port ID
 *      mode                     -- Loop detection mode
 *                                  AIR_SWC_LPDET_MODE_T
 * OUTPUT:
 *      None
 * RETURN:
 *      AIR_E_OK                 -- Operation Success.
 *      AIR_E_BAD_PARAMETER      -- Parameter is wrong.
 * NOTES:
 *      None
 */
AIR_ERROR_NO_T
air_swc_setLoopDetect(
    const UI32_T                unit,
    const UI32_T                port,
    const AIR_SWC_LPDET_MODE_T  mode)
{
    HAL_CHECK_UNIT(unit);
    HAL_CHECK_PORT(unit, port);
    HAL_CHECK_BOOL(mode);

    return HAL_FUNC_CALL(unit, swc, setLoopDetect, (unit, port, mode));
}

/* FUNCTION NAME:   air_swc_getLoopDetect
 * PURPOSE:
 *      Get loop detection mode.
 * INPUT:
 *      unit                     -- Device ID
 *      port                     -- Port ID
 * OUTPUT:
 *      ptr_mode                 -- Loop detection mode
 *                                  AIR_SWC_LPDET_MODE_T
 * RETURN:
 *      AIR_E_OK                 -- Operation Success.
 *      AIR_E_BAD_PARAMETER      -- Parameter is wrong.
 * NOTES:
 *      None
 */
AIR_ERROR_NO_T
air_swc_getLoopDetect(
    const UI32_T            unit,
    const UI32_T            port,
    AIR_SWC_LPDET_MODE_T    *ptr_mode)
{
    HAL_CHECK_UNIT(unit);
    HAL_CHECK_PORT(unit, port);
    HAL_CHECK_PTR(ptr_mode);

    return HAL_FUNC_CALL(unit, swc, getLoopDetect, (unit, port, ptr_mode));
}

/* FUNCTION NAME:   air_swc_setLoopDetectFrame
 * PURPOSE:
 *      Set source mac or ether type.
 * INPUT:
 *      unit                     -- Device ID
 *      ether_type               -- Ether type of loop detect frame
 *      smac                     -- Source MAC address of loop detect
 *                                  frame
 *                                  AIR_MAC_T
 * OUTPUT:
 *      None
 * RETURN:
 *      AIR_E_OK                 -- Operation Success.
 *      AIR_E_BAD_PARAMETER      -- Parameter is wrong.
 * NOTES:
 *      Use current setting if type = 0
 *      Use current setting if smac = 0
 */
AIR_ERROR_NO_T
air_swc_setLoopDetectFrame(
    const UI32_T                    unit,
    const UI32_T                    ether_type,
    AIR_MAC_T                       smac)
{
    HAL_CHECK_UNIT(unit);
    HAL_CHECK_MIN_MAX_RANGE(ether_type, 0, AIR_MAX_ETHER_TYPE_VAL);
    HAL_CHECK_PTR(smac);

    return HAL_FUNC_CALL(unit, swc, setLoopDetectFrame, (unit, ether_type, smac));
}

/* FUNCTION NAME:   air_swc_getLoopDetectFrame
 * PURPOSE:
 *      Get source mac, ether type.
 * INPUT:
 *      unit                     -- Device ID
 * OUTPUT:
 *      ptr_ether_type           -- Ether type of loop detect frame
 *      smac                     -- Source MAC address of loop detect
 *                                  frame
 *                                  AIR_MAC_T
 * RETURN:
 *      AIR_E_OK                 -- Operation Success.
 *      AIR_E_BAD_PARAMETER      -- Parameter is wrong.
 * NOTES:
 *      None
 */
AIR_ERROR_NO_T
air_swc_getLoopDetectFrame(
    const UI32_T            unit,
    UI32_T                  *ptr_ether_type,
    AIR_MAC_T               smac)
{
    HAL_CHECK_UNIT(unit);
    HAL_CHECK_PTR(ptr_ether_type);
    HAL_CHECK_PTR(smac);

    return HAL_FUNC_CALL(unit, swc, getLoopDetectFrame, (unit, ptr_ether_type, smac));
}

/* FUNCTION NAME:   air_swc_getLoopDetectStatus
 * PURPOSE:
 *      Get the current loop detection status.
 * INPUT:
 *      unit                     -- Device ID
 *      port_bitmap              -- Port bitmap
 * OUTPUT:
 *      ld_status_bitmap         -- Loop status bitmap
 *                                  TRUE  : loop occur
 *                                  FALSE : loop not occur
 * RETURN:
 *      AIR_E_OK                 -- Operation Success.
 *      AIR_E_BAD_PARAMETER      -- Parameter is wrong.
 *      AIR_E_OTHERS             -- Other errors.
 * NOTES:
 *      None
 */
AIR_ERROR_NO_T
air_swc_getLoopDetectStatus(
    const UI32_T                unit,
    const AIR_PORT_BITMAP_T     port_bitmap,
    AIR_PORT_BITMAP_T           ld_status_bitmap)
{
    HAL_CHECK_UNIT(unit);
    HAL_CHECK_PORT_BITMAP(unit, port_bitmap);
    HAL_CHECK_PTR(ld_status_bitmap);

    return HAL_FUNC_CALL(unit, swc, getLoopDetectStatus, (unit, port_bitmap, ld_status_bitmap));
}

/* FUNCTION NAME:   air_swc_clearLoopDetectStatus
 * PURPOSE:
 *      Clear the current loop detection status of specific port
 *      bitmap.
 * INPUT:
 *      unit                     -- Device ID
 *      port_bitmap              -- Port bitmap
 * OUTPUT:
 *      None
 * RETURN:
 *      AIR_E_OK                 -- Operation Success.
 *      AIR_E_BAD_PARAMETER      -- Parameter is wrong.
 *      AIR_E_OTHERS             -- Other errors.
 * NOTES:
 *      None
 */
AIR_ERROR_NO_T
air_swc_clearLoopDetectStatus(
    const UI32_T                unit,
    const AIR_PORT_BITMAP_T     port_bitmap)
{
    HAL_CHECK_UNIT(unit);
    HAL_CHECK_PORT_BITMAP(unit, port_bitmap);

    return HAL_FUNC_CALL(unit, swc, clearLoopDetectStatus, (unit, port_bitmap));
}

