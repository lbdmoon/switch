include          $(AIR_SDK)/make/make_prologue.mk

# Subdirectories, in random order
ifeq ($(findstring scorpio,$(ENABLED_SWITCH_CHIPS)),scorpio)
dir             := $(d)/sco
include         $(dir)/make.mk
endif

ifeq ($(findstring pearl,$(ENABLED_SWITCH_CHIPS)),pearl)
dir             := $(d)/pearl
include         $(dir)/make.mk
endif

# End subdirectories
# Local rules


include          $(AIR_SDK)/make/make_epilogue.mk

