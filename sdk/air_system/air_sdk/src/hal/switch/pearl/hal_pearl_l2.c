/*******************************************************************************
*  Copyright Statement:
*  --------------------
*  This software is protected by Copyright and the information contained
*  herein is confidential. The software may not be copied and the information
*  contained herein may not be used or disclosed except with the written
*  permission of Airoha Technology Corp. (C) 2021
*
*  BY OPENING THIS FILE, BUYER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
*  THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("AIROHA SOFTWARE")
*  RECEIVED FROM AIROHA AND/OR ITS REPRESENTATIVES ARE PROVIDED TO BUYER ON
*  AN "AS-IS" BASIS ONLY. AIROHA EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
*  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
*  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
*  NEITHER DOES AIROHA PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
*  SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
*  SUPPLIED WITH THE AIROHA SOFTWARE, AND BUYER AGREES TO LOOK ONLY TO SUCH
*  THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. AIROHA SHALL ALSO
*  NOT BE RESPONSIBLE FOR ANY AIROHA SOFTWARE RELEASES MADE TO BUYER'S
*  SPECIFICATION OR TO CONFORM TO A PARTICULAR STANDARD OR OPEN FORUM.
*
*  BUYER'S SOLE AND EXCLUSIVE REMEDY AND AIROHA'S ENTIRE AND CUMULATIVE
*  LIABILITY WITH RESPECT TO THE AIROHA SOFTWARE RELEASED HEREUNDER WILL BE,
*  AT AIROHA'S OPTION, TO REVISE OR REPLACE THE AIROHA SOFTWARE AT ISSUE,
*  OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY BUYER TO
*  AIROHA FOR SUCH AIROHA SOFTWARE AT ISSUE.
*
*  THE TRANSACTION CONTEMPLATED HEREUNDER SHALL BE CONSTRUED IN ACCORDANCE
*  WITH THE LAWS OF THE STATE OF CALIFORNIA, USA, EXCLUDING ITS CONFLICT OF
*  LAWS PRINCIPLES.  ANY DISPUTES, CONTROVERSIES OR CLAIMS ARISING THEREOF AND
*  RELATED THERETO SHALL BE SETTLED BY ARBITRATION IN SAN FRANCISCO, CA, UNDER
*  THE RULES OF THE INTERNATIONAL CHAMBER OF COMMERCE (ICC).
*
*******************************************************************************/

/* FILE NAME:  hal_pearl_l2.c
 * PURPOSE:
 *  Implement L2 module HAL function.
 *
 * NOTES:
 *
 */


/* INCLUDE FILE DECLARTIONS
*/
#include <osal/osal.h>
#include <hal/switch/pearl/hal_pearl_l2.h>
#include <hal/switch/pearl/hal_pearl_stp.h>
#include <hal/switch/pearl/hal_pearl_reg.h>
#include <hal/switch/pearl/hal_pearl_vlan.h>
#include <aml/aml.h>
#include <api/diag.h>
#include <hal/common/hal_dbg.h>
#include <cmlib/cmlib_bit.h>

/* NAMING CONSTANT DECLARATIONS
*/
#define HAL_PEARL_L2_DELAY_US             (1000)
#define HAL_PEARL_L2_WDOG_KICK_NUM        (1000)
#define HAL_PEARL_L2_FORWARD_VALUE        (0xFFFFFFFF)
#define HAL_PEARL_L2_AGING_MS_CONSTANT    (1024)
#define HAL_PEARL_L2_AGING_1000MS         (1000)

typedef enum
{
    _HAL_PEARL_L2_MAC_TB_TY_MAC,
    _HAL_PEARL_L2_MAC_TB_TY_DIP,
    _HAL_PEARL_L2_MAC_TB_TY_DIP_SIP,
    _HAL_PEARL_L2_MAC_TB_TY_LAST
}_HAL_PEARL_L2_MAC_TB_TY_T;

typedef enum
{
    _HAL_PEARL_L2_MAC_MAT_MAC,
    _HAL_PEARL_L2_MAC_MAT_DYNAMIC_MAC,
    _HAL_PEARL_L2_MAC_MAT_STATIC_MAC,
    _HAL_PEARL_L2_MAC_MAT_MAC_BY_VID,
    _HAL_PEARL_L2_MAC_MAT_MAC_BY_FID,
    _HAL_PEARL_L2_MAC_MAT_MAC_BY_PORT,
    _HAL_PEARL_L2_MAC_MAT_MAC_BY_LAST
}_HAL_PEARL_L2_MAC_MAT_T;

/* L2 MAC table multi-searching */
typedef enum
{
    _HAL_PEARL_L2_MAC_MS_START,   /* Start search */
    _HAL_PEARL_L2_MAC_MS_NEXT,    /* Next search */
    _HAL_PEARL_L2_MAC_MS_LAST
}_HAL_PEARL_L2_MAC_MS_T;

typedef enum
{
    _HAL_PEARL_L2_FWD_CTRL_DEFAULT =      0x0,
    _HAL_PEARL_L2_FWD_CTRL_CPU_EXCLUDE =  0x4,
    _HAL_PEARL_L2_FWD_CTRL_CPU_INCLUDE =  0x5,
    _HAL_PEARL_L2_FWD_CTRL_CPU_ONLY =     0x6,
    _HAL_PEARL_L2_FWD_CTRL_DROP =         0x7,
    _HAL_PEARL_L2_FWD_CTRL_LAST
}_HAL_PEARL_L2_FWD_CTRL_T;

/* MACRO FUNCTION DECLARATIONS
 */
#define _HAL_PEARL_L2_AGING_TIME(__cnt__, __unit__)   \
    (((__cnt__) + 1) * ((__unit__) + 1) * HAL_PEARL_L2_AGING_MS_CONSTANT / HAL_PEARL_L2_AGING_1000MS)

#define _HAL_PEARL_L2_AGING_TIME_2_CNT(__time__, __cnt__, __unit__)   do\
{\
    UI32_T _value_= (__time__) * HAL_PEARL_L2_AGING_1000MS / HAL_PEARL_L2_AGING_MS_CONSTANT;\
    (__unit__)  = (_value_ / BIT(AAC_AGE_CNT_LENGTH) + 1);\
    (__cnt__)   = (_value_ / (__unit__) + 1);\
}while(0)

#define PTR_HAL_PEARL_L2_HW_TBL_MUTEX(__unit__)               \
    (&(_l2_fdb_cb[__unit__].hw_tbl_mutex))

/* DATA TYPE DECLARATIONS
*/

/* GLOBAL VARIABLE DECLARATIONS
*/
DIAG_SET_MODULE_INFO(AIR_MODULE_L2, "hal_pearl_l2.c");

/* LOCAL SUBPROGRAM SPECIFICATIONS
*/
static BOOL_T
_cmpMac(
    const AIR_MAC_T mac1,
    const AIR_MAC_T mac2);

static void
_fill_MAC_ATA(
    const UI32_T unit,
    const AIR_MAC_ENTRY_T *ptr_mac_entry);

static void
_fill_MAC_ATWD(
    const UI32_T            unit,
    const AIR_MAC_ENTRY_T   *ptr_mac_entry,
    const BOOL_T            valid);

static void
_fill_MAC_ATRDS(
    const UI32_T unit,
    UI8_T bank);

static AIR_ERROR_NO_T
_read_MAC_ATRD(
    const UI32_T unit,
    AIR_MAC_ENTRY_T *ptr_mac_entry);

static AIR_ERROR_NO_T
_checkL2Busy(
    const UI32_T unit);

static AIR_ERROR_NO_T
_searchMacEntry(
    const UI32_T unit,
    const _HAL_PEARL_L2_MAC_MS_T ms,
    const _HAL_PEARL_L2_MAC_MAT_T multi_target,
    UI32_T *ptr_addr,
    UI32_T *ptr_bank);

static AIR_ERROR_NO_T
_hal_pearl_l2_initRsrc(
    const   UI32_T  unit);

static AIR_ERROR_NO_T
_hal_pearl_l2_deinitRsrc(
    const   UI32_T  unit);

static AIR_ERROR_NO_T
_hal_pearl_l2_initModule(
    const   UI32_T  unit);

static AIR_ERROR_NO_T
_hal_pearl_l2_deinitModule(
    const   UI32_T  unit);

/* STATIC VARIABLE DECLARATIONS
 */
static HAL_PEARL_L2_FDB_CB_T  _l2_fdb_cb[AIR_CFG_MAXIMUM_CHIPS_PER_SYSTEM];
static BOOL_T _search_end = FALSE;

/* LOCAL SUBPROGRAM BODIES
 */

/* FUNCTION NAME: _cmpMac
 * PURPOSE:
 *      Compare MAC address to check whether those MAC is same.
 *
 * INPUT:
 *      mac1            --  1st MAC address
 *      mac2            --  2nd MAC address
 *
 * OUTPUT:
 *      None
 *
 * RETURN:
 *      TRUE
 *      FALSE
 *
 * NOTES:
 *      None
 */
static BOOL_T
_cmpMac(
    const AIR_MAC_T mac1,
    const AIR_MAC_T mac2)
{
    UI32_T i;
    for (i = 0; i < 6; i++)
    {
        if (mac1[i] != mac2[i])
        {
            return FALSE;
        }
    }
    return TRUE;
}

/* FUNCTION NAME: _isMacEntryValid
 * PURPOSE:
 *      Verify whether the input MAC entry is valid.
 * INPUT:
 *      ptr_mac_entry   --  MAC Address entry
 * OUTPUT:
 *      None
 * RETURN:
 *      TRUE
 *      FALSE
 * NOTES:
 *      None
 */
static BOOL_T
_isMacEntryValid(
    AIR_MAC_ENTRY_T *ptr_mac_entry)
{
    UI8_T   i;
    for (i = 0; i < 6; i++)
    {
        if (0 != ptr_mac_entry->mac[i])
        {
            return TRUE;
        }
    }
    return FALSE;
}

/* FUNCTION NAME: _fill_MAC_ATA
 * PURPOSE:
 *      Fill register ATA for MAC Address table.
 *
 * INPUT:
 *      unit            --  Device ID
 *      ptr_mac_entry   --  Structure of MAC Address table
 *
 * OUTPUT:
 *      None
 *
 * RETURN:
 *      None
 *
 * NOTES:
 *      None
 */
static void
_fill_MAC_ATA(
    const UI32_T unit,
    const AIR_MAC_ENTRY_T *ptr_mac_entry)
{
    UI32_T u32dat;
    UI32_T i;

    /* Fill ATA1 */
    u32dat=0;
    for (i = 0; i < 4; i++)
    {
        u32dat |= ((UI32_T)(ptr_mac_entry->mac[i] & BITS(0,7))) << ( (3-i) * 8);
    }
    aml_writeReg(unit, ATA1, &u32dat, sizeof(u32dat));
    osal_delayUs(HAL_PEARL_L2_DELAY_US);

    /* Fill ATA2 */
    u32dat=0;
    for (i = 4; i < 6; i++)
    {
        u32dat |= ((UI32_T)(ptr_mac_entry->mac[i] & BITS(0,7))) << ( (7-i) * 8);
    }
    if (!(ptr_mac_entry->flags & AIR_L2_MAC_ENTRY_FLAGS_STATIC))
    {
        /* type is dynamic */
        u32dat |= BITS_OFF_L(1UL, ATA2_MAC_LIFETIME_OFFSET, ATA2_MAC_LIFETIME_LENGTH);
        /* set aging counter as system aging conuter */
        u32dat |= BITS_OFF_L(   ptr_mac_entry->timer,
                                ATA2_MAC_AGETIME_OFFSET,
                                ATA2_MAC_AGETIME_LENGTH);
    }
    if (ptr_mac_entry->flags & AIR_L2_MAC_ENTRY_FLAGS_UNAUTH)
    {
        u32dat |= BITS_OFF_L(1UL, ATA2_MAC_UNAUTH_OFFSET, ATA2_MAC_UNAUTH_LENGTH);
    }

    aml_writeReg(unit, ATA2, &u32dat, sizeof(u32dat));
    osal_delayUs(HAL_PEARL_L2_DELAY_US);
}

/* FUNCTION NAME: _fill_MAC_ATWD
 * PURPOSE:
 *      Fill register ATWD for MAC Address table.
 *
 * INPUT:
 *      unit            --  Device ID
 *      ptr_mac_entry   --  Structure of MAC Address table
 *      valid           --  TRUE
 *                          FALSE
 * OUTPUT:
 *      None
 *
 * RETURN:
 *      None
 *
 * NOTES:
 *      None
 */
static void
_fill_MAC_ATWD(
    const UI32_T            unit,
    const AIR_MAC_ENTRY_T   *ptr_mac_entry,
    const BOOL_T            valid)
{
    UI32_T u32dat = 0;
    UI32_T fwd_val = 0;

    u32dat = 0;
    /* Fill ATWD */
    /* set valid bit */
    if (TRUE == valid)
    {
        u32dat |= BITS_OFF_L(1UL, ATWD_MAC_LIVE_OFFSET, ATWD_MAC_LIVE_LENGTH);
    }

    /* set IVL */
    if (ptr_mac_entry->flags & AIR_L2_MAC_ENTRY_FLAGS_IVL)
    {
        u32dat |= BITS_OFF_L(1UL, ATWD_MAC_IVL_OFFSET, ATWD_MAC_IVL_LENGTH);
    }
    /* set VID */
    u32dat |= BITS_OFF_L(ptr_mac_entry->cvid, ATWD_MAC_VID_OFFSET, ATWD_MAC_VID_LENGTH);
    /* set FID */
    u32dat |= BITS_OFF_L(ptr_mac_entry->fid, ATWD_MAC_FID_OFFSET, ATWD_MAC_FID_LENGTH);
    /* set leaky VLAN */
    if (ptr_mac_entry->flags & AIR_L2_MAC_ENTRY_FLAGS_DISABLE_EGRESS_VLAN_FILTER)
    {
        u32dat |= BITS_OFF_L(1UL, ATWD_MAC_LEAK_OFFSET, ATWD_MAC_LEAK_LENGTH);
    }

    /* Set forwarding control */
    switch (ptr_mac_entry->sa_fwd)
    {
        case AIR_L2_FWD_CTRL_DEFAULT:
            fwd_val = _HAL_PEARL_L2_FWD_CTRL_DEFAULT;
            break;
        case AIR_L2_FWD_CTRL_CPU_INCLUDE:
            fwd_val = _HAL_PEARL_L2_FWD_CTRL_CPU_INCLUDE;
            break;
        case AIR_L2_FWD_CTRL_CPU_EXCLUDE:
            fwd_val = _HAL_PEARL_L2_FWD_CTRL_CPU_EXCLUDE;
            break;
        case AIR_L2_FWD_CTRL_CPU_ONLY:
            fwd_val = _HAL_PEARL_L2_FWD_CTRL_CPU_ONLY;
            break;
        case AIR_L2_FWD_CTRL_DROP:
            fwd_val = _HAL_PEARL_L2_FWD_CTRL_DROP;
            break;
        default:
            break;
    }
    u32dat |= BITS_OFF_L(fwd_val, ATWD_MAC_FWD_OFFSET, ATWD_MAC_FWD_LENGTH);
    aml_writeReg(unit, ATWD, &u32dat, sizeof(u32dat));
    osal_delayUs(HAL_PEARL_L2_DELAY_US);

    /* Fill ATWD2 */
    u32dat = BITS_OFF_L(ptr_mac_entry->port_bitmap[0],
                        ATWD2_MAC_PORT_OFFSET,
                        ATWD2_MAC_PORT_LENGTH);
    aml_writeReg(unit, ATWD2, &u32dat, sizeof(u32dat));
    osal_delayUs(HAL_PEARL_L2_DELAY_US);
}

/* FUNCTION NAME: _fill_MAC_ATRDS
 * PURPOSE:
 *      Fill register ATRDS for select bank after ATC search L2 table.
 *
 * INPUT:
 *      unit            --  Device ID
 *      bank            --  Selected index of bank
 *
 * OUTPUT:
 *      None
 *
 * RETURN:
 *      None
 *
 * NOTES:
 *      None
 */
static void
_fill_MAC_ATRDS(
    const UI32_T unit,
    UI8_T bank)
{
    UI32_T u32dat;

    /* Fill ATRDS */
    u32dat = BITS_OFF_L(bank, ATRD0_MAC_SEL_OFFSET, ATRD0_MAC_SEL_LENGTH);
    aml_writeReg(unit, ATRDS, &u32dat, sizeof(u32dat));
    osal_delayUs(HAL_PEARL_L2_DELAY_US);
}

/* FUNCTION NAME: _read_MAC_ATRD
 * PURPOSE:
 *      Read register ATRD for MAC Address table.
 *
 * INPUT:
 *      unit            --  Device ID
 *
 * OUTPUT:
 *      ptr_mac_entry   --  Structure of MAC Address table
 *
 * RETURN:
 *      AIR_E_OK
 *      AIR_E_ENTRY_NOT_FOUND
 *
 * NOTES:
 *      None
 */
static AIR_ERROR_NO_T
_read_MAC_ATRD(
    const UI32_T unit,
    AIR_MAC_ENTRY_T *ptr_mac_entry)
{
    UI32_T u32dat;
    UI32_T i;
    BOOL_T live = FALSE;
    UI32_T type;
    UI32_T age_unit;
    UI32_T age_cnt;
    UI32_T sa_fwd;

    /* Read ATRD0 */
    aml_readReg(unit, ATRD0, &u32dat, sizeof(u32dat));
    live = BITS_OFF_R(u32dat, ATRD0_MAC_LIVE_OFFSET, ATRD0_MAC_LIVE_LENGTH);
    type = BITS_OFF_R(u32dat, ATRD0_MAC_TYPE_OFFSET, ATRD0_MAC_TYPE_LENGTH);
    if (FALSE == live)
    {
        return AIR_E_ENTRY_NOT_FOUND;
    }
    if (_HAL_PEARL_L2_MAC_TB_TY_MAC != type)
    {
        return AIR_E_ENTRY_NOT_FOUND;
    }
    /* Clear table */
    osal_memset(ptr_mac_entry, 0, sizeof(AIR_MAC_ENTRY_T));

    ptr_mac_entry->cvid = (UI16_T)BITS_OFF_R(u32dat, ATRD0_MAC_VID_OFFSET, ATRD0_MAC_VID_LENGTH);
    ptr_mac_entry->fid = (UI16_T)BITS_OFF_R(u32dat, ATRD0_MAC_FID_OFFSET, ATRD0_MAC_FID_LENGTH);
    if (!!BITS_OFF_R(u32dat, ATRD0_MAC_LIFETIME_OFFSET, ATRD0_MAC_LIFETIME_LENGTH))
    {
        ptr_mac_entry->flags |= AIR_L2_MAC_ENTRY_FLAGS_STATIC;
    }
    if (!!BITS_OFF_R(u32dat, ATRD0_MAC_IVL_OFFSET, ATRD0_MAC_IVL_LENGTH))
    {
        ptr_mac_entry->flags |= AIR_L2_MAC_ENTRY_FLAGS_IVL;
    }
    if (!!BITS_OFF_R(u32dat, ATRD1_MAC_UNAUTH_OFFSET, ATRD1_MAC_UNAUTH_LENGTH))
    {
        ptr_mac_entry->flags |= AIR_L2_MAC_ENTRY_FLAGS_UNAUTH;
    }
    if (!!BITS_OFF_R(u32dat, ATRD0_MAC_LEAK_OFFSET, ATRD0_MAC_LEAK_LENGTH))
    {
        ptr_mac_entry->flags |= AIR_L2_MAC_ENTRY_FLAGS_DISABLE_EGRESS_VLAN_FILTER;
    }

    /* Get the L2 MAC aging unit */
    aml_readReg(unit, AAC, &u32dat, sizeof(u32dat));
    age_unit = BITS_OFF_R(u32dat, AAC_AGE_UNIT_OFFSET, AAC_AGE_UNIT_LENGTH);

    /* Read ATRD1 */
    aml_readReg(unit, ATRD1, &u32dat, sizeof(u32dat));
    for (i = 4; i < 6; i++)
    {
        ptr_mac_entry->mac[i] = BITS_OFF_R(u32dat, (7 - i)*8, 8);
    }
    /* Aging time */
    age_cnt = BITS_OFF_R(u32dat, ATRD1_MAC_AGETIME_OFFSET, ATRD1_MAC_AGETIME_LENGTH);
    ptr_mac_entry->timer = _HAL_PEARL_L2_AGING_TIME(age_cnt, age_unit);
    /* SA forwarding */
    sa_fwd = BITS_OFF_R(u32dat, ATRD1_MAC_FWD_OFFSET, ATRD1_MAC_FWD_LENGTH);
    switch (sa_fwd)
    {
        case _HAL_PEARL_L2_FWD_CTRL_DEFAULT:
            ptr_mac_entry->sa_fwd = AIR_L2_FWD_CTRL_DEFAULT;
            break;
        case _HAL_PEARL_L2_FWD_CTRL_CPU_INCLUDE:
            ptr_mac_entry->sa_fwd = AIR_L2_FWD_CTRL_CPU_INCLUDE;
            break;
        case _HAL_PEARL_L2_FWD_CTRL_CPU_EXCLUDE:
            ptr_mac_entry->sa_fwd = AIR_L2_FWD_CTRL_CPU_EXCLUDE;
            break;
        case _HAL_PEARL_L2_FWD_CTRL_CPU_ONLY:
            ptr_mac_entry->sa_fwd = AIR_L2_FWD_CTRL_CPU_ONLY;
            break;
        case _HAL_PEARL_L2_FWD_CTRL_DROP:
            ptr_mac_entry->sa_fwd = AIR_L2_FWD_CTRL_DROP;
            break;
        default:
            ptr_mac_entry->sa_fwd = AIR_L2_FWD_CTRL_DEFAULT;
            break;
    }

    /* Read ATRD2 */
    aml_readReg(unit, ATRD2, &u32dat, sizeof(u32dat));
    for (i = 0; i < 4; i++)
    {
        ptr_mac_entry->mac[i] = BITS_OFF_R(u32dat, (3 - i)*8, 8);
    }

    /* Read ATRD3 */
    aml_readReg(unit, ATRD3, &u32dat, sizeof(u32dat));
    ptr_mac_entry->port_bitmap[0] = BITS_OFF_R( u32dat,
                                                ATRD3_MAC_PORT_OFFSET,
                                                ATRD3_MAC_PORT_LENGTH);

    return AIR_E_OK;
}

/* FUNCTION NAME: _checkL2Busy
 * PURPOSE:
 *      Check BUSY bit of ATC
 *
 * INPUT:
 *      unit            --  Device ID
 *
 * OUTPUT:
 *      None
 *
 * RETURN:
 *      AIR_E_OK
 *      AIR_E_TIMEOUT
 *
 * NOTES:
 *      None
 */
static AIR_ERROR_NO_T
_checkL2Busy(
    const UI32_T unit)
{
    UI32_T i;
    UI32_T reg_atc;

    /* Check BUSY bit is 0 */
    for (i = 0; i < HAL_PEARL_L2_MAX_BUSY_TIME; i++)
    {
        aml_readReg(unit, ATC, &reg_atc, sizeof(reg_atc));
        if (!BITS_OFF_R(reg_atc, ATC_BUSY_OFFSET, ATC_BUSY_LENGTH))
        {
            break;
        }
        osal_delayUs(HAL_PEARL_L2_DELAY_US);
    }
    if (i >= HAL_PEARL_L2_MAX_BUSY_TIME)
    {
        return AIR_E_TIMEOUT;
    }
    return AIR_E_OK;
}

/* FUNCTION NAME: _checkL2EntryHit
 * PURPOSE:
 *      Check entry hit of ATC
 *
 * INPUT:
 *      unit            --  Device ID
 *
 * OUTPUT:
 *      None
 *
 * RETURN:
 *      The entry hit bitmap
 *
 * NOTES:
 *      None
 */
static UI32_T
_checkL2EntryHit(
    const UI32_T unit)
{
    UI32_T reg_atc;
    aml_readReg(unit, ATC, &reg_atc, sizeof(reg_atc));
    return BITS_OFF_R(reg_atc, ATC_ENTRY_HIT_OFFSET, ATC_ENTRY_HIT_LENGTH);
}

/* FUNCTION NAME: _checkL2ReadSingleHit
 * PURPOSE:
 *      Check reading single entry hit of ATC
 *
 * INPUT:
 *      unit            --  Device ID
 *
 * OUTPUT:
 *      None
 *
 * RETURN:
 *      The entry hit result
 *
 * NOTES:
 *      None
 */
static UI32_T
_checkL2ReadSingleHit(
    const UI32_T unit)
{
    UI32_T reg_atc;
    aml_readReg(unit, ATC, &reg_atc, sizeof(reg_atc));
    return BITS_OFF_R(reg_atc, ATC_SINGLE_HIT_OFFSET, ATC_SINGLE_HIT_LENGTH);
}

/* FUNCTION NAME: _searchMacEntry
 * PURPOSE:
 *      Search MAC Address table.
 *
 * INPUT:
 *      unit            --  Device ID
 *      ms              --  _HAL_PEARL_L2_MAC_MS_START:           Start search command
 *                          _HAL_PEARL_L2_MAC_MS_NEXT:            Next search command
 *      multi_target    --  _HAL_PEARL_L2_MAC_MAT_MAC:            MAC address entries
 *                          _HAL_PEARL_L2_MAC_MAT_DYNAMIC_MAC:    Dynamic MAC address entries
 *                          _HAL_PEARL_L2_MAC_MAT_STATIC_MAC:     Static MAC address entries
 *                          _HAL_PEARL_L2_MAC_MAT_MAC_BY_VID:     MAC address entries with specific CVID
 *                          _HAL_PEARL_L2_MAC_MAT_MAC_BY_FID:     MAC address entries with specific FID
 *                          _HAL_PEARL_L2_MAC_MAT_MAC_BY_PORT:    MAC address entries with specific port
 * OUTPUT:
 *      ptr_addr        --  MAC Table Access Index
 *      ptr_bank        --  Searching result in which bank
 *
 * RETURN:
 *      AIR_E_OK
 *      AIR_E_BAD_PARAMETER
 *      AIR_E_TIMEOUT
 *
 * NOTES:
 *      None
 */
static AIR_ERROR_NO_T
_searchMacEntry(
    const UI32_T unit,
    const _HAL_PEARL_L2_MAC_MS_T ms,
    const _HAL_PEARL_L2_MAC_MAT_T multi_target,
    UI32_T *ptr_addr,
    UI32_T *ptr_bank)
{
    UI32_T u32dat;

    u32dat = ATC_START_BUSY;
    if (_HAL_PEARL_L2_MAC_MS_START == ms)
    {
        /* Start search 1st valid entry */
        u32dat |= ATC_CMD_SEARCH;
    }
    else if (_HAL_PEARL_L2_MAC_MS_NEXT == ms)
    {
        /* Search next valid entry */
        u32dat |= ATC_CMD_SEARCH_NEXT;
    }
    else
    {
        /* Unknown commnad */
        return AIR_E_BAD_PARAMETER;
    }

    switch(multi_target)
    {
        case _HAL_PEARL_L2_MAC_MAT_MAC:
            u32dat |= ATC_MAT_MAC;
            break;
        case _HAL_PEARL_L2_MAC_MAT_DYNAMIC_MAC:
            u32dat |= ATC_MAT_DYNAMIC_MAC;
            break;
        case _HAL_PEARL_L2_MAC_MAT_STATIC_MAC:
            u32dat |= ATC_MAT_STATIC_MAC;
            break;
        case _HAL_PEARL_L2_MAC_MAT_MAC_BY_VID:
            u32dat |= ATC_MAT_MAC_BY_VID;
            break;
        case _HAL_PEARL_L2_MAC_MAT_MAC_BY_FID:
            u32dat |= ATC_MAT_MAC_BY_FID;
            break;
        case _HAL_PEARL_L2_MAC_MAT_MAC_BY_PORT:
            u32dat |= ATC_MAT_MAC_BY_PORT;
            break;
        default:
            /* Unknown searching mode */
            return AIR_E_BAD_PARAMETER;
    }
    aml_writeReg(unit, ATC, &u32dat, sizeof(u32dat));
    if (AIR_E_TIMEOUT == _checkL2Busy(unit))
    {
        return AIR_E_TIMEOUT;
    }

    aml_readReg(unit, ATC, &u32dat, sizeof(u32dat));
    /* Get address */
    (*ptr_addr) = BITS_OFF_R(u32dat, ATC_ADDR_OFFSET, ATC_ADDR_LENGTH);
    /* Get banks */
    (*ptr_bank) = BITS_OFF_R(u32dat, ATC_ENTRY_HIT_OFFSET, ATC_ENTRY_HIT_LENGTH);
    if ((HAL_PEARL_L2_MAX_ADDR_NUM - 1) == (*ptr_addr))
    {
        _search_end = TRUE;
    }
    else
    {
        _search_end = FALSE;
    }

    return AIR_E_OK;
}

/* FUNCTION NAME: _hal_pearl_l2_initRsrc
 * PURPOSE:
 *      To initialize module resource of L2 MAC table.
 * INPUT:
 *      unit                --  Device unit number
 * OUTPUT:
 *      None
 * RETURN:
 *      AIR_E_OK            --  Operation is successfull.
 *      AIR_E_BAD_PARAMETER --  Bad parameter.
 * NOTES:
 *      None
 */
static AIR_ERROR_NO_T
_hal_pearl_l2_initRsrc(
    const   UI32_T  unit)
{
    AIR_ERROR_NO_T      rc = AIR_E_OK;

    HAL_PEARL_L2_FDB_CB_T *ptr_cb = NULL;

    HAL_CHECK_UNIT(unit);

    ptr_cb = &(_l2_fdb_cb[unit]);
    osal_memset(ptr_cb, 0, sizeof(HAL_PEARL_L2_FDB_CB_T));

    /* Create mutex lock */
    rc = osal_createSemaphore(
            "L2_HW_TABLE",
            AIR_SEMAPHORE_BINARY,
            PTR_HAL_PEARL_L2_HW_TBL_MUTEX(unit),
            air_module_getModuleName(AIR_MODULE_L2));
    if (AIR_E_OK != rc)
    {
        DIAG_PRINT(HAL_DBG_WARN, "Create semaphore failed, rc=(%d)\n", rc);
    }

    return rc;
}

/* FUNCTION NAME: _hal_pearl_l2_deinitRsrc
 * PURPOSE:
 *      To deinitialize module resource of L2 MAC table.
 * INPUT:
 *      unit                --  Device unit number
 * OUTPUT:
 *      None
 * RETURN:
 *      AIR_E_OK            --  Operation is successfull.
 *      AIR_E_BAD_PARAMETER --  Bad parameter.
 * NOTES:
 *      None
 */
static AIR_ERROR_NO_T
_hal_pearl_l2_deinitRsrc(
    const   UI32_T  unit)
{
    AIR_ERROR_NO_T      rc = AIR_E_OK;
    HAL_PEARL_L2_FDB_CB_T *ptr_cb = NULL;

    HAL_CHECK_UNIT(unit);

    ptr_cb = &(_l2_fdb_cb[unit]);

    /* Destory mutex lock */
    rc = osal_destroySemaphore(PTR_HAL_PEARL_L2_HW_TBL_MUTEX(unit));

    if (AIR_E_OK != rc)
    {
        DIAG_PRINT(HAL_DBG_WARN, "Destroy semaphore failed, rc=(%d)\n", rc);
    }

    /* Reset control block */
    osal_memset(ptr_cb, 0, sizeof(HAL_PEARL_L2_FDB_CB_T));
    return rc;
}

/* FUNCTION NAME: _hal_pearl_l2_initModule
 * PURPOSE:
 *      To initialize module configuration of L2 MAC table.
 * INPUT:
 *      unit                --  Device unit number
 * OUTPUT:
 *      None
 * RETURN:
 *      AIR_E_OK            --  Operation is successfull.
 *      AIR_E_BAD_PARAMETER --  Bad parameter.
 * NOTES:
 *      None
 */
static AIR_ERROR_NO_T
_hal_pearl_l2_initModule(
    const   UI32_T  unit)
{
    AIR_ERROR_NO_T      rc = AIR_E_OK;
    AIR_FORWARD_TYPE_T  type;
    AIR_PORT_BITMAP_T   pbm = {0};

    /* Translate port bitmap type */
    AIR_PORT_BITMAP_COPY(pbm, HAL_PORT_BMP_ETH(unit));
    /* Exclude CPU port */
    AIR_PORT_DEL(pbm, HAL_CPU_PORT(unit));
    /* Initialize L2 forwarding mode as port-bitmap to all ether port for mc, bc, uuc, uipmc */
    for(type = AIR_FORWARD_TYPE_BCST; type < AIR_FORWARD_TYPE_LAST; type++)
    {
        /* Flood to each port without CPU port */
        hal_pearl_l2_setForwardMode(unit, type, AIR_FORWARD_ACTION_TO_PBM, pbm);
    }

    return rc;
}

/* FUNCTION NAME: _hal_pearl_l2_deinitModule
 * PURPOSE:
 *      To deinitialize module configuration of L2 MAC table.
 * INPUT:
 *      unit                --  Device unit number
 * OUTPUT:
 *      None
 * RETURN:
 *      AIR_E_OK            --  Operation is successfull.
 *      AIR_E_BAD_PARAMETER --  Bad parameter.
 * NOTES:
 *      None
 */
static AIR_ERROR_NO_T
_hal_pearl_l2_deinitModule(
    const   UI32_T  unit)
{
    hal_pearl_l2_clearMacAddr(unit);

    return AIR_E_OK;
}

/* EXPORTED SUBPROGRAM BODIES
*/
/* FUNCTION NAME: hal_pearl_l2_init
 * PURPOSE:
 *      Initialization of L2 MAC table.
 *
 * INPUT:
 *      unit            --  Device ID
 *
 * OUTPUT:
 *      None
 *
 * RETURN:
 *      AIR_E_OK
 *
 * NOTES:
 *      None
 */
AIR_ERROR_NO_T
hal_pearl_l2_init(
    const UI32_T unit)
{
    AIR_ERROR_NO_T              rc = AIR_E_OK;

    if (HAL_MODULE_INITED(unit, AIR_MODULE_L2) & HAL_INIT_STAGE(unit))
    {
        rc = AIR_E_ALREADY_INITED;
    }

    if (AIR_E_OK == rc)
    {
        if (HAL_INIT_STAGE(unit) & HAL_INIT_STAGE_TASK_RSRC)
        {
            rc = _hal_pearl_l2_initRsrc(unit);
        }
        if (HAL_INIT_STAGE(unit) & HAL_INIT_STAGE_MODULE)
        {
            rc = _hal_pearl_l2_initModule(unit);
        }
    }

    if (AIR_E_OK == rc)
    {
        HAL_MODULE_INITED(unit, AIR_MODULE_L2) |= HAL_INIT_STAGE(unit);
    }

    return rc;
}

/* FUNCTION NAME: hal_pearl_l2_deinit
 * PURPOSE:
 *      Deinitialization of L2 MAC table.
 *
 * INPUT:
 *      unit            --  Device ID
 *
 * OUTPUT:
 *      None
 *
 * RETURN:
 *      AIR_E_OK
 *
 * NOTES:
 *      None
 */
AIR_ERROR_NO_T
hal_pearl_l2_deinit(
    const UI32_T unit)
{
    AIR_ERROR_NO_T              rc = AIR_E_OK;

    if (!(HAL_MODULE_INITED(unit, AIR_MODULE_L2) & HAL_INIT_STAGE(unit)))
    {
        rc = AIR_E_NOT_INITED;
    }

    if (AIR_E_OK == rc)
    {
        if (HAL_INIT_STAGE(unit) & HAL_INIT_STAGE_TASK_RSRC)
        {
            rc = _hal_pearl_l2_deinitRsrc(unit);
        }
        if (HAL_INIT_STAGE(unit) & HAL_INIT_STAGE_MODULE)
        {
            rc = _hal_pearl_l2_deinitModule(unit);
        }
    }

    if (AIR_E_OK == rc)
    {
        HAL_MODULE_INITED(unit, AIR_MODULE_L2) &= ~HAL_INIT_STAGE(unit);
    }

    return rc;
}

/* FUNCTION NAME: hal_pearl_l2_addMacAddr
 * PURPOSE:
 *      Add or set a L2 unicast MAC address entry.
 *      If the address entry doesn't exist, it will add the entry.
 *      If the address entry already exists, it will set the entry
 *      with user input value.
 *
 * INPUT:
 *      unit            --  Device ID
 *      ptr_mac_entry   --  The structure of MAC Address table
 *
 * OUTPUT:
 *      None
 *
 * RETURN:
 *      AIR_E_OK
 *      AIR_E_BAD_PARAMETER
 *      AIR_E_TABLE_FULL
 *      AIR_E_ENTRY_NOT_FOUND
 *      AIR_E_TIMEOUT
 *
 * NOTES:
 *      None
 */
AIR_ERROR_NO_T
hal_pearl_l2_addMacAddr(
    const UI32_T            unit,
    const AIR_MAC_ENTRY_T   *ptr_mac_entry)
{
    UI32_T u32dat;
    UI32_T reg_aac;
    UI32_T age_cnt;
    UI32_T age_unit, sys_unit;
    AIR_MAC_ENTRY_T set_mac_entry;

    /* Check elements in the structure of MAC table */
    HAL_CHECK_PORT_BITMAP(unit, ptr_mac_entry->port_bitmap);
    if (ptr_mac_entry->flags & AIR_L2_MAC_ENTRY_FLAGS_IVL)
    {
        HAL_CHECK_VLAN(ptr_mac_entry->cvid);
    }
    else
    {
        HAL_CHECK_MIN_MAX_RANGE(ptr_mac_entry->fid, 0, (AIR_STP_FID_NUMBER - 1));
    }

    /* Set the target MAC entry as setting entry no mater the hash addrees is existed or not */
    osal_memcpy(&set_mac_entry, ptr_mac_entry, sizeof(AIR_MAC_ENTRY_T));
    /* Translate port bitmap type */
    HAL_AIR_PBMP_TO_MAC_PBMP(   unit,
            ptr_mac_entry->port_bitmap,
            set_mac_entry.port_bitmap);
    HAL_PEARL_L2_FDB_LOCK(unit);
    /* set aging counter as system aging conuter */
    aml_readReg(unit, AAC, &reg_aac, sizeof(reg_aac));
    if (ptr_mac_entry->timer != AIR_L2_MAC_DEF_AGE_OUT_TIME)
    {
        _HAL_PEARL_L2_AGING_TIME_2_CNT(ptr_mac_entry->timer, age_cnt, age_unit);
        sys_unit = BITS_OFF_R(reg_aac, AAC_AGE_UNIT_OFFSET, AAC_AGE_UNIT_LENGTH) + 1;
        if (age_unit == sys_unit)
        {
            set_mac_entry.timer = age_cnt;
        }
        else if (age_unit > sys_unit)
        {
            return AIR_E_BAD_PARAMETER;
        }
        else
        {
            set_mac_entry.timer = ptr_mac_entry->timer/sys_unit;
        }
    }
    else
    {
        set_mac_entry.timer = BITS_OFF_R(reg_aac, AAC_AGE_CNT_OFFSET, AAC_AGE_CNT_LENGTH);
    }

    /* Fill MAC address entry */
    _fill_MAC_ATA(unit, &set_mac_entry);
    _fill_MAC_ATWD(unit, &set_mac_entry, TRUE);

    /* Write data by ATC */
    u32dat = (ATC_SAT_MAC | ATC_CMD_WRITE | ATC_START_BUSY);
    aml_writeReg(unit, ATC, &u32dat, sizeof(u32dat));
    if (AIR_E_TIMEOUT == _checkL2Busy(unit))
    {
        HAL_PEARL_L2_FDB_UNLOCK(unit);
        return AIR_E_TIMEOUT;
    }
    if ( !_checkL2EntryHit(unit))
    {
        HAL_PEARL_L2_FDB_UNLOCK(unit);
        return AIR_E_TABLE_FULL;
    }
    HAL_PEARL_L2_FDB_UNLOCK(unit);
    return AIR_E_OK;
}

/* FUNCTION NAME: hal_pearl_l2_delMacAddr
 * PURPOSE:
 *      Delete a L2 unicast MAC address entry.
 *
 * INPUT:
 *      unit            --  Device ID
 *      ptr_mac_entry   --  The structure of MAC Address table
 *
 * OUTPUT:
 *      None
 *
 * RETURN:
 *      AIR_E_OK
 *      AIR_E_BAD_PARAMETER
 *
 * NOTES:
 *      None
 */
AIR_ERROR_NO_T
hal_pearl_l2_delMacAddr(
    const UI32_T            unit,
    const AIR_MAC_ENTRY_T   *ptr_mac_entry)
{
    UI32_T u32dat;
    AIR_MAC_ENTRY_T del_mac_entry;

    /* Check elements in the structure of MAC table */
    if (ptr_mac_entry->flags & AIR_L2_MAC_ENTRY_FLAGS_IVL)
    {
        HAL_CHECK_VLAN(ptr_mac_entry->cvid);
    }
    else
    {
        HAL_CHECK_MIN_MAX_RANGE(ptr_mac_entry->fid, 0, (AIR_STP_FID_NUMBER - 1));
    }

    osal_memcpy(&del_mac_entry, ptr_mac_entry, sizeof(AIR_MAC_ENTRY_T));

    HAL_PEARL_L2_FDB_LOCK(unit);

    /* Fill MAC address entry */
    _fill_MAC_ATA(unit, &del_mac_entry);
    _fill_MAC_ATWD(unit, &del_mac_entry, FALSE);

    /* Write data by ATC to delete entry */
    u32dat = (ATC_SAT_MAC | ATC_CMD_WRITE | ATC_START_BUSY);
    aml_writeReg(unit, ATC, &u32dat, sizeof(u32dat));
    if (AIR_E_TIMEOUT == _checkL2Busy(unit))
    {
        HAL_PEARL_L2_FDB_UNLOCK(unit);
        return AIR_E_TIMEOUT;
    }

    HAL_PEARL_L2_FDB_UNLOCK(unit);
    return AIR_E_OK;
}

/* FUNCTION NAME:  hal_pearl_l2_getMacAddr
 * PURPOSE:
 *      Get a L2 unicast MAC address entry.
 *
 * INPUT:
 *      unit            --  Device ID
 *      ptr_mac_entry   --  The structure of MAC Address table
 *
 * OUTPUT:
 *      ptr_count       --  The number of returned MAC entries
 *      ptr_mac_entry   --  Structure of MAC Address table for searching result.
 *                          The size of ptr_mac_entry depends on the max. number of bank.
 *                          The memory size should greater than
 *                          ((# of Bank) * (Size of entry structure))
 * RETURN:
 *      AIR_E_OK
 *      AIR_E_BAD_PARAMETER
 *      AIR_E_TIMEOUT
 *      AIR_E_ENTRY_NOT_FOUND
 *
 * NOTES:
 *      If the parameter:mac in input argument ptr_mac_entry[0] is empty.
 *      It means to search the first valid MAC address entry in MAC address table.
 *      Otherwise, to search the specific MAC address entry in input argument ptr_mac_entry[0].
 *      Input argument ptr_mac_entry[0] needs include mac, ivl and (fid or cvid) depends on ivl.
 *      If argument ivl is TRUE, cvid is necessary, or fid is.
 */
AIR_ERROR_NO_T
hal_pearl_l2_getMacAddr(
    const UI32_T    unit,
    UI8_T           *ptr_count,
    AIR_MAC_ENTRY_T *ptr_mac_entry)
{
    AIR_ERROR_NO_T rc = AIR_E_OK;
    UI32_T i = 0;
    AIR_MAC_ENTRY_T hw_mac_entry;
    UI32_T addr = 0;
    UI32_T banks = 0;
    UI32_T u32dat = 0;

    /* Check elements in the structure of MAC table */
    if (ptr_mac_entry->flags & AIR_L2_MAC_ENTRY_FLAGS_IVL)
    {
        HAL_CHECK_VLAN(ptr_mac_entry->cvid);
    }
    else
    {
        HAL_CHECK_MIN_MAX_RANGE(ptr_mac_entry->fid, 0, (AIR_STP_FID_NUMBER - 1));
    }

    (*ptr_count) = 0;

    /* Check MAC Address field of input data */
    if (_isMacEntryValid(ptr_mac_entry))
    {
        /* MAC address isn't empty, means to search a specific MAC entry */
        HAL_PEARL_L2_FDB_LOCK(unit);
        _fill_MAC_ATA(unit, ptr_mac_entry);
        _fill_MAC_ATWD(unit, ptr_mac_entry, TRUE);

        /* Read data by ATC */
        u32dat = (ATC_SAT_MAC | ATC_CMD_READ | ATC_START_BUSY);
        aml_writeReg(unit, ATC, &u32dat, sizeof(u32dat));
        if (AIR_E_TIMEOUT == _checkL2Busy(unit))
        {
            HAL_PEARL_L2_FDB_UNLOCK(unit);
            return AIR_E_TIMEOUT;
        }
        if ( !_checkL2ReadSingleHit(unit))
        {
            HAL_PEARL_L2_FDB_UNLOCK(unit);
            return AIR_E_ENTRY_NOT_FOUND;
        }

        /* Read MAC entry */
        osal_memset(&hw_mac_entry, 0, sizeof(AIR_MAC_ENTRY_T));
        rc = _read_MAC_ATRD(unit, &hw_mac_entry);
        HAL_PEARL_L2_FDB_UNLOCK(unit);
        if (AIR_E_OK != rc)
        {
            DIAG_PRINT(HAL_DBG_INFO, "rc=(%d)\n", rc);
            return rc;
        }
        /* The found MAC is the target, restore data and leave */
        osal_memcpy(ptr_mac_entry, &hw_mac_entry, sizeof(AIR_MAC_ENTRY_T));
        /* Translate port bitmap type */
        HAL_MAC_PBMP_TO_AIR_PBMP(unit,  hw_mac_entry.port_bitmap,
                                        ptr_mac_entry->port_bitmap);
        (*ptr_count) = 1;

        return AIR_E_OK;
    }
    else
    {
        /* MAC address is empty, means to search the 1st MAC entry */
        HAL_PEARL_L2_FDB_LOCK(unit);
        rc = _searchMacEntry(unit,
                             _HAL_PEARL_L2_MAC_MS_START,
                             _HAL_PEARL_L2_MAC_MAT_MAC,
                             &addr,
                             &banks);

        switch(rc)
        {
            case AIR_E_OK:
                /* Searching bank and read data */
                DIAG_PRINT(HAL_DBG_INFO, "banks=(%d)\n", banks);
                if (0 == banks)
                {
                    HAL_PEARL_L2_FDB_UNLOCK(unit);
                    return AIR_E_ENTRY_NOT_FOUND;
                }
                for (i = 0; i < HAL_PEARL_L2_MAC_SET_NUM; i++)
                {
                    if (!!BITS_OFF_R(banks, i, 1))
                    {
                        /* Found a valid MAC entry */
                        /* Select bank */
                        _fill_MAC_ATRDS(unit, i);

                        /* Read MAC entry */
                        osal_memset(&hw_mac_entry, 0, sizeof(AIR_MAC_ENTRY_T));
                        rc = _read_MAC_ATRD(unit, &hw_mac_entry);
                        if (AIR_E_OK != rc)
                        {
                            DIAG_PRINT(HAL_DBG_INFO, "rc=(%d)\n", rc);
                            continue;
                        }
                        osal_memcpy(&ptr_mac_entry[(*ptr_count)],
                                    &hw_mac_entry,
                                    sizeof(AIR_MAC_ENTRY_T));
                        /* Translate port bitmap type */
                        HAL_MAC_PBMP_TO_AIR_PBMP(   unit,
                                                    hw_mac_entry.port_bitmap,
                                                    ptr_mac_entry[(*ptr_count)].port_bitmap);
                        (*ptr_count)++;
                    }
                }
                HAL_PEARL_L2_FDB_UNLOCK(unit);
                return AIR_E_OK;
            case AIR_E_TIMEOUT:
                /* Searching over time */
                HAL_PEARL_L2_FDB_UNLOCK(unit);
                return AIR_E_TIMEOUT;
            default:
                DIAG_PRINT(HAL_DBG_INFO, "rc=(%d)\n", rc);
                HAL_PEARL_L2_FDB_UNLOCK(unit);
                return AIR_E_ENTRY_NOT_FOUND;
        }
    }
}

/* FUNCTION NAME: hal_pearl_l2_getNextMacAddr
 * PURPOSE:
 *      Get the next L2 unicast MAC address entries.
 *
 * INPUT:
 *      unit            --  Device ID
 *      ptr_mac_entry   --  The structure of MAC Address table
 *
 * OUTPUT:
 *      ptr_count       --  The number of returned MAC entries
 *      ptr_mac_entry   --  Structure of MAC Address table for searching result.
 *                          The size of ptr_mac_entry depends on the max. number of bank.
 *                          The memory size should greater than ((# of Bank) * (Table size))
 * RETURN:
 *      AIR_E_OK
 *      AIR_E_BAD_PARAMETER
 *      AIR_E_TIMEOUT
 *      AIR_E_ENTRY_NOT_FOUND
 * NOTES:
 *      If the parameter:mac in input argument ptr_mac_entry[0] is empty.
 *      It means to search the next valid MAC address entries of last searching result.
 *      Otherwise, to search the next valid MAC address entry of the specific MAC address
 *      entry in input argument ptr_mac_entry[0].
 *      Input argument ptr_mac_entry[0] needs include mac, ivl and (fid or cvid) depends on ivl.
 *      If argument ivl is TRUE, cvid is necessary, or fid is.
 */
AIR_ERROR_NO_T
hal_pearl_l2_getNextMacAddr(
    const UI32_T    unit,
    UI8_T           *ptr_count,
    AIR_MAC_ENTRY_T *ptr_mac_entry)
{
    AIR_ERROR_NO_T rc = AIR_E_OK;
    UI32_T i = 0;
    BOOL_T found_target = FALSE;
    AIR_MAC_ENTRY_T hw_mac_entry;
    UI32_T addr;
    UI32_T banks;

    /* Check elements in the structure of MAC table */
    if (ptr_mac_entry->flags & AIR_L2_MAC_ENTRY_FLAGS_IVL)
    {
        HAL_CHECK_VLAN(ptr_mac_entry->cvid);
    }
    else
    {
        HAL_CHECK_MIN_MAX_RANGE(ptr_mac_entry->fid, 0, (AIR_STP_FID_NUMBER - 1));
    }

    /* If found the lastest entry last time, we couldn't keep to search the next entry */
    if (TRUE == _search_end)
    {
        return AIR_E_ENTRY_NOT_FOUND;
    }

    (*ptr_count)=0;

    HAL_PEARL_L2_FDB_LOCK(unit);
    /* Check MAC Address field of input data */
    if (_isMacEntryValid(ptr_mac_entry))
    {
        /* MAC address isn't empty, means to search the next entries of input MAC Address */
        /* Search the target MAC entry */
        _fill_MAC_ATA(unit, ptr_mac_entry);
        rc = _searchMacEntry(unit,
                             _HAL_PEARL_L2_MAC_MS_START,
                             _HAL_PEARL_L2_MAC_MAT_MAC,
                             &addr,
                             &banks);
        while(AIR_E_OK == rc)
        {
            DIAG_PRINT(HAL_DBG_INFO, "banks=(%d)\n", banks);
            if (0 == banks)
            {
                HAL_PEARL_L2_FDB_UNLOCK(unit);
                return AIR_E_ENTRY_NOT_FOUND;
            }
            for (i = 0; i < HAL_PEARL_L2_MAC_SET_NUM; i++)
            {
                if (!!BITS_OFF_R(banks, i, 1))
                {
                    /* Found a valid MAC entry */
                    /* Select bank */
                    _fill_MAC_ATRDS(unit, i);

                    /* Read MAC entry */
                    osal_memset(&hw_mac_entry, 0, sizeof(AIR_MAC_ENTRY_T));
                    rc = _read_MAC_ATRD(unit, &hw_mac_entry);
                    if (AIR_E_OK != rc)
                    {
                        DIAG_PRINT(HAL_DBG_INFO, "rc=(%d)\n", rc);
                        continue;
                    }
                    if (TRUE == _cmpMac(ptr_mac_entry->mac, hw_mac_entry.mac))
                    {
                        /* The found MAC is the target, restore data and leave */
                        found_target = TRUE;
                        break;
                    }
                }
            }

            if ( TRUE == found_target)
            {
                break;
            }

            /* The found MAC isn't the target, keep searching or leave
             * when found the last entry */
            if (TRUE == _search_end)
            {
                HAL_PEARL_L2_FDB_UNLOCK(unit);
                return AIR_E_ENTRY_NOT_FOUND;
            }
            else
            {
                rc = _searchMacEntry(unit,
                                     _HAL_PEARL_L2_MAC_MS_NEXT,
                                     _HAL_PEARL_L2_MAC_MAT_MAC,
                                     &addr,
                                     &banks);
            }
        }

        if ( FALSE == found_target )
        {
            /* Entry not bank */
            HAL_PEARL_L2_FDB_UNLOCK(unit);
            return AIR_E_ENTRY_NOT_FOUND;
        }
        else
        {
            /* Found the target MAC entry, and try to search the next address */
            rc = _searchMacEntry(unit,
                                 _HAL_PEARL_L2_MAC_MS_NEXT,
                                 _HAL_PEARL_L2_MAC_MAT_MAC,
                                 &addr,
                                 &banks);
            if (AIR_E_OK == rc)
            {
                DIAG_PRINT(HAL_DBG_INFO, "banks=(%d)\n", banks);
                if (0 == banks)
                {
                    HAL_PEARL_L2_FDB_UNLOCK(unit);
                    return AIR_E_ENTRY_NOT_FOUND;
                }
                for (i = 0; i < HAL_PEARL_L2_MAC_SET_NUM; i++)
                {
                    if (!!BITS_OFF_R(banks, i, 1))
                    {
                        /* Found a valid MAC entry */
                        /* Select bank */
                        _fill_MAC_ATRDS(unit, i);

                        /* Read MAC entry */
                        osal_memset(&hw_mac_entry, 0, sizeof(AIR_MAC_ENTRY_T));
                        rc = _read_MAC_ATRD(unit, &hw_mac_entry);
                        if (AIR_E_OK != rc)
                        {
                            DIAG_PRINT(HAL_DBG_INFO, "rc=(%d)\n", rc);
                            continue;
                        }
                        osal_memcpy(&ptr_mac_entry[(*ptr_count)],
                                    &hw_mac_entry,
                                    sizeof(AIR_MAC_ENTRY_T));
                        /* Translate port bitmap type */
                        HAL_MAC_PBMP_TO_AIR_PBMP(   unit,
                                                    hw_mac_entry.port_bitmap,
                                                    ptr_mac_entry[(*ptr_count)].port_bitmap);
                        (*ptr_count)++;
                    }
                }
                HAL_PEARL_L2_FDB_UNLOCK(unit);
                return AIR_E_OK;
            }
            else
            {
                DIAG_PRINT(HAL_DBG_INFO, "rc=(%d)\n", rc);
                HAL_PEARL_L2_FDB_UNLOCK(unit);
                return AIR_E_ENTRY_NOT_FOUND;
            }
        }
    }
    else
    {
        /* MAC address is empty, means to search next entry */
        rc = _searchMacEntry(unit, _HAL_PEARL_L2_MAC_MS_NEXT, _HAL_PEARL_L2_MAC_MAT_MAC, &addr, &banks);
        if (AIR_E_OK == rc)
        {
            DIAG_PRINT(HAL_DBG_INFO, "banks=(%d)\n", banks);
            if (0 == banks)
            {
                HAL_PEARL_L2_FDB_UNLOCK(unit);
                return AIR_E_ENTRY_NOT_FOUND;
            }
            for (i = 0; i < HAL_PEARL_L2_MAC_SET_NUM; i++)
            {
                if (!!BITS_OFF_R(banks, i, 1))
                {
                    /* Found a valid MAC entry */
                    /* Select bank */
                    _fill_MAC_ATRDS(unit, i);

                    /* Read MAC entry */
                    osal_memset(&hw_mac_entry, 0, sizeof(AIR_MAC_ENTRY_T));
                    rc = _read_MAC_ATRD(unit, &hw_mac_entry);
                    if (AIR_E_OK != rc)
                    {
                        DIAG_PRINT(HAL_DBG_INFO, "rc=(%d)\n", rc);
                        continue;
                    }
                    osal_memcpy(&ptr_mac_entry[(*ptr_count)],
                                &hw_mac_entry,
                                sizeof(AIR_MAC_ENTRY_T));
                    /* Translate port bitmap type */
                    HAL_MAC_PBMP_TO_AIR_PBMP(   unit,
                                                hw_mac_entry.port_bitmap,
                                                ptr_mac_entry[(*ptr_count)].port_bitmap);
                    (*ptr_count)++;
                }
            }
            HAL_PEARL_L2_FDB_UNLOCK(unit);
            return AIR_E_OK;
        }
        else
        {
            DIAG_PRINT(HAL_DBG_INFO, "rc=(%d)\n", rc);
            HAL_PEARL_L2_FDB_UNLOCK(unit);
            return AIR_E_ENTRY_NOT_FOUND;
        }
    }
}

/* FUNCTION NAME: hal_pearl_l2_clearMacAddr
 * PURPOSE:
 *      Clear all L2 unicast MAC address entries.
 *
 * INPUT:
 *      unit            --  Device ID
 *
 * OUTPUT:
 *      None
 *
 * RETURN:
 *      AIR_E_OK
 *      AIR_E_BAD_PARAMETER
 *
 * NOTES:
 *      None
 */
AIR_ERROR_NO_T
hal_pearl_l2_clearMacAddr(
    const UI32_T unit)
{
    UI32_T u32dat;

    /* Write data by ATC to clear all MAC address entries */
    u32dat = (ATC_SAT_MAC | ATC_CMD_CLEAN | ATC_START_BUSY);
    HAL_PEARL_L2_FDB_LOCK(unit);
    aml_writeReg(unit, ATC, &u32dat, sizeof(u32dat));
    if (AIR_E_TIMEOUT == _checkL2Busy(unit))
    {
        HAL_PEARL_L2_FDB_UNLOCK(unit);
        return AIR_E_TIMEOUT;
    }

    HAL_PEARL_L2_FDB_UNLOCK(unit);
    return AIR_E_OK;
}

/* FUNCTION NAME: hal_pearl_l2_flushMacAddr
 * PURPOSE:
 *      Flush all L2 unicast MAC address entries by vid, by fid or by port.
 *
 * INPUT:
 *      unit            --  Device ID
 *      type            --  Flush l2 mac address by vid/fid/port
 *      value           --  value of vid/fid/port
 *
 * OUTPUT:
 *      None
 *
 * RETURN:
 *      AIR_E_OK
 *      AIR_E_BAD_PARAMETER
 *
 * NOTES:
 *      None
 */
AIR_ERROR_NO_T
hal_pearl_l2_flushMacAddr(
    const UI32_T                    unit,
    const AIR_L2_MAC_FLUSH_TYPE_T   type,
    const UI32_T                    value)
{
    AIR_ERROR_NO_T      rc = AIR_E_OK;
    UI8_T               count = 0;
    UI32_T              i = 0, mac_num = 0, set_num = 0, wdog_count = 0;
    AIR_MAC_ENTRY_T     *ptr_mac_entry = NULL;

    /* Set size of Mac Table with the defined set_num in order to reduce memory usage*/
    rc = hal_pearl_l2_getMacBucketSize(unit, &set_num);
    if (AIR_E_OK != rc)
    {
        osal_printf("***Error***, get max. set number fail\n");
        return (rc);
    }
    ptr_mac_entry = osal_alloc(sizeof(AIR_MAC_ENTRY_T) * set_num,
                               air_module_getModuleName(AIR_MODULE_L2));
    if (NULL == ptr_mac_entry)
    {
        osal_printf("***Error***, allocate memory fail\n");
        return AIR_E_NO_MEMORY;
    }

    /* Using mac_num to represent the number of Mac Addr found in this loop, initial mac_num = 0 */
    while (1)
    {
        mac_num++;
        osal_memset(ptr_mac_entry, 0, sizeof(AIR_MAC_ENTRY_T) * set_num);
        /* In the first loop, mac_num = 1, using hal_pearl_l2_getMacAddr to find the 1st Mac Addr */
        if (1 == mac_num)
        {
            rc = hal_pearl_l2_getMacAddr(unit, &count, ptr_mac_entry);
            /* If not found Mac Addr in first loop, means Mac table is empty, then break */
            if (AIR_E_ENTRY_NOT_FOUND == rc)
            {
                osal_printf("MAC table is empty\n");
                break;
            }
        }
        /* In other loops, mac_num >= 2, using hal_pearl_l2_getNextMacAddr to get the other Mac Addr */
        else
        {
            rc = hal_pearl_l2_getNextMacAddr(unit, &count, ptr_mac_entry);
            if (AIR_E_ENTRY_NOT_FOUND == rc)
            {
                break;
            }
        }
        if (AIR_E_OK != rc)
        {
            osal_printf("***Error***, flush mac table fail\n");
            break;
        }
        /* Function get(Next)MacAddr one entry from each bank,
         * if there are two banks, then get two mac addr */
        for (i = 0; i < count; i++)
        {
            /* Continue if a mac address is static */
            if (ptr_mac_entry[i].flags & AIR_L2_MAC_ENTRY_FLAGS_STATIC)
            {
                continue;
            }
            if (AIR_L2_MAC_FLUSH_TYPE_PORT == type)
            {
                /* If flush Mac Addr by port,
                 * (1 << value) represents port num on which the Mac Addr would be flushed */
                if ((1 << value) & ptr_mac_entry[i].port_bitmap[0])
                {
                    rc = hal_pearl_l2_delMacAddr(unit, &(ptr_mac_entry[i]));
                }
            }
            else if (AIR_L2_MAC_FLUSH_TYPE_FID == type)
            {
                if (    (!(ptr_mac_entry[i].flags & AIR_L2_MAC_ENTRY_FLAGS_IVL))
                    &&  (value == ptr_mac_entry[i].fid))
                {
                    rc = hal_pearl_l2_delMacAddr(unit, &(ptr_mac_entry[i]));
                }
            }
            else if (AIR_L2_MAC_FLUSH_TYPE_VID == type)
            {
                if (    (ptr_mac_entry[i].flags & AIR_L2_MAC_ENTRY_FLAGS_IVL)
                    &&  (value == ptr_mac_entry[i].cvid))
                {
                    rc = hal_pearl_l2_delMacAddr(unit, &(ptr_mac_entry[i]));
                }
            }
            else
            {
                continue;
            }
            if (AIR_E_OK != rc)
            {
                osal_printf("***Error***, delete mac address fail\n");
            }
        }

        wdog_count += count;
        if (wdog_count >= HAL_PEARL_L2_WDOG_KICK_NUM)
        {
            osal_wdog_kick();
            wdog_count -= HAL_PEARL_L2_WDOG_KICK_NUM;
        }
    }

    osal_free(ptr_mac_entry);
    return AIR_E_OK;
}

/* FUNCTION NAME: hal_pearl_l2_setMacAddrAgeOut
 * PURPOSE:
 *      Set the age out time of L2 MAC address entries.
 *
 * INPUT:
 *      unit            --  Device ID
 *      age_time        --  age out time (second)
 *                          (1..AIR_L2_MAC_MAX_AGE_OUT_TIME)
 * OUTPUT:
 *      None
 *
 * RETURN:
 *      AIR_E_OK
 *      AIR_E_BAD_PARAMETER
 *
 * NOTES:
 *      None
 */
AIR_ERROR_NO_T
hal_pearl_l2_setMacAddrAgeOut(
    const UI32_T    unit,
    const UI32_T    age_time)
{
    UI32_T u32dat;
    UI32_T age_cnt, age_unit;

    /* Read the old register value */
    aml_readReg(unit, AAC, &u32dat, sizeof(u32dat));

    u32dat &= ~BITS_RANGE(AAC_AGE_UNIT_OFFSET, AAC_AGE_UNIT_LENGTH);
    u32dat &= ~BITS_RANGE(AAC_AGE_CNT_OFFSET, AAC_AGE_CNT_LENGTH);

    /* Calcuate the aging count/unit */
    _HAL_PEARL_L2_AGING_TIME_2_CNT(age_time, age_cnt, age_unit);

    /* Write the new register value */
    u32dat |= BITS_OFF_L((age_unit - 1), AAC_AGE_UNIT_OFFSET, AAC_AGE_UNIT_LENGTH);
    u32dat |= BITS_OFF_L((age_cnt - 1), AAC_AGE_CNT_OFFSET, AAC_AGE_CNT_LENGTH);

    aml_writeReg(unit, AAC, &u32dat, sizeof(u32dat));

    return AIR_E_OK;
}

/* FUNCTION NAME: hal_pearl_l2_getMacAddrAgeOut
 * PURPOSE:
 *      Get the age out time of unicast MAC address.
 *
 * INPUT:
 *      unit            --  Device ID
 *
 * OUTPUT:
 *      ptr_age_time    --  age out time
 *
 * RETURN:
 *      AIR_E_OK
 *      AIR_E_BAD_PARAMETER
 *
 * NOTES:
 *      None
 */
AIR_ERROR_NO_T
hal_pearl_l2_getMacAddrAgeOut(
    const UI32_T    unit,
    UI32_T          *ptr_age_time)
{
    UI32_T u32dat;
    UI32_T age_cnt, age_unit;

    /* Read data from register */
    aml_readReg(unit, AAC, &u32dat, sizeof(u32dat));

    age_cnt = BITS_OFF_R(u32dat, AAC_AGE_CNT_OFFSET, AAC_AGE_CNT_LENGTH);
    age_unit = BITS_OFF_R(u32dat, AAC_AGE_UNIT_OFFSET, AAC_AGE_UNIT_LENGTH);
    (*ptr_age_time) = _HAL_PEARL_L2_AGING_TIME(age_cnt, age_unit);

    return AIR_E_OK;
}

/* FUNCTION NAME: hal_pearl_l2_setMacAddrAgeOutMode
 * PURPOSE:
 *      Set the age out mode for specific port.
 *
 * INPUT:
 *      unit            --  Device ID
 *      port            --  Index of port number
 *      enable          --  TRUE:   Enable L2 MAC table aging out.
 *                          FALSE:  Disable L2 MAC table aging out.
 * OUTPUT:
 *      None
 *
 * RETURN:
 *      AIR_E_OK
 *      AIR_E_BAD_PARAMETER
 *
 * NOTES:
 *      None
 */
AIR_ERROR_NO_T
hal_pearl_l2_setMacAddrAgeOutMode(
    const UI32_T    unit,
    const UI32_T    port,
    const BOOL_T    enable)
{
    UI32_T u32dat;
    UI32_T mac_port;

    /* Translate port type */
    HAL_AIR_PORT_TO_MAC_PORT(unit, port, mac_port);

    /* Access regiser */
    aml_readReg(unit, AGDIS, &u32dat, sizeof(u32dat));
    if (TRUE == enable)
    {
        u32dat &= ~BIT(mac_port);
    }
    else
    {
        u32dat |= BIT(mac_port);
    }
    aml_writeReg(unit, AGDIS, &u32dat, sizeof(u32dat));

    return AIR_E_OK;
}

/* FUNCTION NAME: hal_pearl_l2_getMacAddrAgeOutMode
 * PURPOSE:
 *      Get the age out mode for specific port.
 *
 * INPUT:
 *      unit            --  Device ID
 *      port            --  Index of port number
 *
 * OUTPUT:
 *      ptr_enable      --  TRUE:   Enable L2 MAC table aging out.
 *                          FALSE:  Disable L2 MAC table aging out.
 * RETURN:
 *      AIR_E_OK
 *      AIR_E_BAD_PARAMETER
 *
 * NOTES:
 *      None
 */
AIR_ERROR_NO_T
hal_pearl_l2_getMacAddrAgeOutMode(
    const UI32_T    unit,
    const UI32_T    port,
    BOOL_T          *ptr_enable)
{
    UI32_T u32dat;
    UI32_T mac_port;

    /* Translate port type */
    HAL_AIR_PORT_TO_MAC_PORT(unit, port, mac_port);

    /* Access regiser */
    aml_readReg(unit, AGDIS, &u32dat, sizeof(u32dat));
    (*ptr_enable) = (!!BITS_OFF_R(u32dat, mac_port, 1))?FALSE:TRUE;

    return AIR_E_OK;
}

/* FUNCTION NAME: hal_pearl_l2_getMacBucketSize
 * PURPOSE:
 *      Get the bucket size of one MAC address set when searching L2 table.
 *
 * INPUT:
 *      unit            --  Device ID
 *
 * OUTPUT:
 *      ptr_size        --  The bucket size
 *
 * RETURN:
 *      AIR_E_OK
 *      AIR_E_BAD_PARAMETER
 *
 * NOTES:
 *      None
 */
AIR_ERROR_NO_T
hal_pearl_l2_getMacBucketSize(
    const UI32_T    unit,
    UI32_T          *ptr_size)
{
    /* Access regiser */
    (*ptr_size) = HAL_PEARL_L2_MAC_SET_NUM;

    return AIR_E_OK;
}

/* FUNCTION NAME: hal_pearl_l2_setForwardMode
 * PURPOSE:
 *      Set per port forwarding status for unknown type frame.
 *
 * INPUT:
 *      unit            --  Select device ID
 *      type            --  AIR_FORWARD_TYPE_BCST
 *                          AIR_FORWARD_TYPE_MCST
 *                          AIR_FORWARD_TYPE_UCST
 *                          AIR_FORWARD_TYPE_UIPMCST
 *      action          --  AIR_FORWARD_DROP
 *                          AIR_FORWARD_FLOODING
 *                          AIR_FORWARD_TO_PBM
 *      port_bitmap     --  Forwarding port bitmap
 * OUTPUT:
 *      None
 *
 * RETURN:
 *      AIR_E_OK
 *      AIR_E_BAD_PARAMETER
 * NOTES:
 *      None
 */
AIR_ERROR_NO_T
hal_pearl_l2_setForwardMode(
    const UI32_T unit,
    const AIR_FORWARD_TYPE_T    type,
    const AIR_FORWARD_ACTION_T  action,
    const AIR_PORT_BITMAP_T     port_bitmap)
{
    AIR_ERROR_NO_T  rc = AIR_E_OK;
    UI32_T offset = 0;
    AIR_PORT_BITMAP_T pbm = {0}, mac_pbm = {0};

    switch(type)
    {
        case AIR_FORWARD_TYPE_BCST:
            offset = BCF;
            break;
        case AIR_FORWARD_TYPE_MCST:
            offset = UNMF;
            break;
        case AIR_FORWARD_TYPE_UCST:
            offset = UNUF;
            break;
        case AIR_FORWARD_TYPE_UIPMCST:
            offset = UNIPMF;
            break;
        default:
            rc = AIR_E_BAD_PARAMETER;
            break;
    }

    switch(action)
    {
        case AIR_FORWARD_ACTION_FLOODING:
            /* Translate port bitmap type */
            AIR_PORT_BITMAP_COPY(pbm, HAL_PORT_BMP_ETH(unit));
            /* Translate port bitmap type */
            HAL_AIR_PBMP_TO_MAC_PBMP(   unit,
                                        pbm,
                                        mac_pbm);
            break;
        case AIR_FORWARD_ACTION_DROP:
            AIR_PORT_BITMAP_CLEAR(mac_pbm);
            break;
        case AIR_FORWARD_ACTION_TO_PBM:
            /* Translate port bitmap type */
            HAL_AIR_PBMP_TO_MAC_PBMP(   unit,
                                        port_bitmap,
                                        mac_pbm);
            break;
        default:
            rc = AIR_E_BAD_PARAMETER;
            break;
    }

    DIAG_PRINT(HAL_DBG_INFO, "Type=(%d), Action=(%d), pbm[0]=(0x%x)\n", type, action, mac_pbm[0]);
    if (AIR_E_OK == rc)
    {
        rc = aml_writeReg(unit, offset, &(mac_pbm[0]), sizeof(UI32_T));
    }

    return rc;
}

/* FUNCTION NAME: hal_pearl_l2_getForwardMode
 * PURPOSE:
 *      Get per port forwarding status for unknown type frame.
 *
 * INPUT:
 *      unit            --  Select device ID
 *      type            --  AIR_FORWARD_TYPE_BCST
 *                          AIR_FORWARD_TYPE_MCST
 *                          AIR_FORWARD_TYPE_UCST
 *                          AIR_FORWARD_TYPE_UIPMCST
 * OUTPUT:
 *      ptr_action      --  AIR_FORWARD_DROP
 *                          AIR_FORWARD_FLOODING
 *                          AIR_FORWARD_TO_PBM
 *      port_bitmap     --  Forwarding port bitmap
 * RETURN:
 *      AIR_E_OK
 *      AIR_E_BAD_PARAMETER
 *
 * NOTES:
 *      None
 */
AIR_ERROR_NO_T
hal_pearl_l2_getForwardMode(
    const UI32_T                unit,
    const AIR_FORWARD_TYPE_T    type,
    AIR_FORWARD_ACTION_T        *ptr_action,
    AIR_PORT_BITMAP_T           port_bitmap)
{
    AIR_ERROR_NO_T  rc = AIR_E_OK;
    UI32_T offset = 0, getAction;
    AIR_PORT_BITMAP_T getPbm, air_Pbm;

    switch(type)
    {
        case AIR_FORWARD_TYPE_BCST:
            offset = BCF;
            break;
        case AIR_FORWARD_TYPE_MCST:
            offset = UNMF;
            break;
        case AIR_FORWARD_TYPE_UCST:
            offset = UNUF;
            break;
        case AIR_FORWARD_TYPE_UIPMCST:
            offset = UNIPMF;
            break;
        default:
            rc = AIR_E_BAD_PARAMETER;
            break;
    }

    if (AIR_E_OK == rc)
    {
        /* Read data */
        aml_readReg(unit, offset, &(getPbm[0]), sizeof(UI32_T));
        /* Translate port bitmap type */
        HAL_MAC_PBMP_TO_AIR_PBMP(   unit,
                                    getPbm,
                                    air_Pbm);

        if ( AIR_PORT_BITMAP_EQUAL(HAL_PORT_BMP_ETH(unit), air_Pbm) )
        {
            getAction = AIR_FORWARD_ACTION_FLOODING;
            AIR_PORT_BITMAP_CLEAR(port_bitmap);
        }
        else if (AIR_PORT_BITMAP_EMPTY(air_Pbm))
        {
            getAction = AIR_FORWARD_ACTION_DROP;
            AIR_PORT_BITMAP_CLEAR(port_bitmap);
        }
        else
        {
            getAction = AIR_FORWARD_ACTION_TO_PBM;
            AIR_PORT_BITMAP_COPY(port_bitmap, air_Pbm);
        }

        *ptr_action = getAction;
        DIAG_PRINT(HAL_DBG_INFO, "Type=(%d), Action=(%d), pbm[0]=(0x%x)\n",
                                type, getAction, port_bitmap);
    }

    return rc;
}

/* FUNCTION NAME: hal_pearl_l2_lockL2FdbResource
 *
 * PURPOSE:
 *      Lock the resource of L2 table.
 * INPUT:
 *      unit                --  Device unit number
 * OUTPUT:
 *      None
 * RETURN:
 *      AIR_E_OK            --  Operation is successfull.
 *      AIR_E_BAD_PARAMETER --  Bad parameter.
 * NOTES:
 *
 */
AIR_ERROR_NO_T
hal_pearl_l2_lockL2FdbResource(
    const UI32_T                    unit)
{
    HAL_CHECK_UNIT(unit);
    HAL_CHECK_INIT(unit, AIR_MODULE_L2);

    return HAL_COMMON_LOCK_RESOURCE(PTR_HAL_PEARL_L2_HW_TBL_MUTEX(unit),
                                    AIR_SEMAPHORE_WAIT_FOREVER);
}

/* FUNCTION NAME: hal_pearl_l2_unlockL2FdbResource
 *
 * PURPOSE:
 *      Unlock the resource of L2 table.
 * INPUT:
 *      unit                --  Device unit number
 * OUTPUT:
 *      None
 * RETURN:
 *      AIR_E_OK            --  Operation is successfull.
 *      AIR_E_BAD_PARAMETER --  Bad parameter.
 * NOTES:
 *
 */
AIR_ERROR_NO_T
hal_pearl_l2_unlockL2FdbResource(
    const UI32_T                    unit)
{
    HAL_CHECK_UNIT(unit);
    HAL_CHECK_INIT(unit, AIR_MODULE_L2);

    return HAL_COMMON_FREE_RESOURCE(PTR_HAL_PEARL_L2_HW_TBL_MUTEX(unit));
}

