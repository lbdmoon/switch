/*******************************************************************************
*  Copyright Statement:
*  --------------------
*  This software is protected by Copyright and the information contained
*  herein is confidential. The software may not be copied and the information
*  contained herein may not be used or disclosed except with the written
*  permission of Airoha Technology Corp. (C) 2021
*
*  BY OPENING THIS FILE, BUYER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
*  THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("AIROHA SOFTWARE")
*  RECEIVED FROM AIROHA AND/OR ITS REPRESENTATIVES ARE PROVIDED TO BUYER ON
*  AN "AS-IS" BASIS ONLY. AIROHA EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
*  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
*  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
*  NEITHER DOES AIROHA PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
*  SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
*  SUPPLIED WITH THE AIROHA SOFTWARE, AND BUYER AGREES TO LOOK ONLY TO SUCH
*  THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. AIROHA SHALL ALSO
*  NOT BE RESPONSIBLE FOR ANY AIROHA SOFTWARE RELEASES MADE TO BUYER'S
*  SPECIFICATION OR TO CONFORM TO A PARTICULAR STANDARD OR OPEN FORUM.
*
*  BUYER'S SOLE AND EXCLUSIVE REMEDY AND AIROHA'S ENTIRE AND CUMULATIVE
*  LIABILITY WITH RESPECT TO THE AIROHA SOFTWARE RELEASED HEREUNDER WILL BE,
*  AT AIROHA'S OPTION, TO REVISE OR REPLACE THE AIROHA SOFTWARE AT ISSUE,
*  OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY BUYER TO
*  AIROHA FOR SUCH AIROHA SOFTWARE AT ISSUE.
*
*  THE TRANSACTION CONTEMPLATED HEREUNDER SHALL BE CONSTRUED IN ACCORDANCE
*  WITH THE LAWS OF THE STATE OF CALIFORNIA, USA, EXCLUDING ITS CONFLICT OF
*  LAWS PRINCIPLES.  ANY DISPUTES, CONTROVERSIES OR CLAIMS ARISING THEREOF AND
*  RELATED THERETO SHALL BE SETTLED BY ARBITRATION IN SAN FRANCISCO, CA, UNDER
*  THE RULES OF THE INTERNATIONAL CHAMBER OF COMMERCE (ICC).
*
*******************************************************************************/

/* FILE NAME:  hal_pearl_chip.c
 * PURPOSE:
 *  Implement Chip module HAL function.
 *
 * NOTES:
 *
 */

/* INCLUDE FILE DECLARTIONS
*/
#include <aml/aml.h>
#include <api/diag.h>
#include <cmlib/cmlib_bit.h>
#include <hal/switch/pearl/hal_pearl_chip.h>
#include <hal/switch/pearl/hal_pearl_reg.h>
#include <hal/switch/pearl/hal_pearl_mdio.h>
#include <hal/common/hal_cfg.h>
#include <air_cfg.h>

/* NAMING CONSTANT DECLARATIONS
*/

/* MACRO FUNCTION DECLARATIONS
 */

/* DATA TYPE DECLARATIONS
*/
typedef struct EFUSE_DATA_S {
    UI32_T data[4];
} EFUSE_DATA_T;

#define COL_TO_BYTE(col)            (col  << 4)
#define WORD_TO_COL(word)           (word >> 2)
#define EFUSE_MAX_DELAY             (0x10000000)  /* (1073741823 * 6) / 900M ~= 1.78s */
#define EFUSE_KICK_SHIFT            (30)

#define EFUSE_ADDR_MASK             (0x1FF)
#define EFUSE_ADDR_SHIFT            (16)
#define EFUSE_MODE_MASK             (0x3)
#define EFUSE_MODE_SHIFT            (6)
#define EFUSE_MODE_READ             (0x1)

#define EFUSE_REMARK_DEVICE_ID_OFFT (16)
#define EFUSE_REMARK_DEVICE_ID_LENG (8)
#define EFUSE_DEVICE_ID_OFFT        (8)
#define EFUSE_DEVICE_ID_LENG        (8)
#define EFUSE_REVISION_ID_OFFT      (0)
#define EFUSE_REVISION_ID_LENG      (3)

#define RESET_CONTROL_REG_2         (0x100050C4)
#define HSGMII_HSI0_RESET           (0x2000)
#define HSGMII_HSI1_RESET           (0x4000)
#define HSGMII_HSI2_RESET           (0x8000)
#define HSGMII_HSI3_RESET           (0x10000)
#define HSGMII_HSI4_RESET           (0x20000)

#define GLOBAL_FC_PORT_BLK_HI       (0x84)

/* GLOBAL VARIABLE DECLARATIONS
*/

/* STATIC VARIABLE DECLARATIONS
 */
//DIAG_SET_MODULE_INFO(AIR_MODULE_CHIP, "hal_pearl_chip.c");

/* LOCAL SUBPROGRAM SPECIFICATIONS
*/

/* EXPORTED SUBPROGRAM BODIES
*/
/* FUNCTION NAME:   hal_pearl_chip_readDeviceInfo
 * PURPOSE:
 *      To read the device/revision ID of the EFUSE.
 * INPUT:
 *      unit            -- the device unit
 * OUTPUT:
 *      ptr_device_id   -- pointer for the device ID
 *      ptr_revision_id -- pointer for the revision ID
 * RETURN:
 *      AIR_E_OK            -- Successfully get the IDs.
 *      AIR_E_BAD_PARAMETER -- Invalid parameter.
 * NOTES:
 *      none
 */
AIR_ERROR_NO_T
hal_pearl_chip_readDeviceInfo(
    const UI32_T unit,
    UI32_T       *ptr_device_id,
    UI32_T       *ptr_revision_id)
{
    AIR_ERROR_NO_T rc = AIR_E_OK;

    HAL_CHECK_PTR(ptr_device_id);
    HAL_CHECK_PTR(ptr_revision_id);

    (*ptr_device_id) = 0;
    (*ptr_revision_id) = 0;
    return rc;
}

/* FUNCTION NAME: hal_pearl_chip_init
 * PURPOSE:
 *      Chip initialization
 *
 * INPUT:
 *      unit            --  Device ID
 *
 * OUTPUT:
 *      ptr_chip_init_param     --  chip init parameter
 *
 * RETURN:
 *        AIR_E_OK
 *
 * NOTES:
 *      None
 */
AIR_ERROR_NO_T
hal_pearl_chip_init(
    const UI32_T unit,
    HAL_CHIP_INIT_PARAM_T *ptr_chip_init_param)
{
    AIR_CFG_VALUE_T     sled;
    AIR_CFG_VALUE_T     led_type;
    UI32_T              u32dat = 0;
    HAL_CHECK_PTR(ptr_chip_init_param);

    osal_memset(&sled, 0, sizeof(AIR_CFG_VALUE_T));
    osal_memset(&led_type, 0, sizeof(AIR_CFG_VALUE_T));

    /*
     * Disable MAC power saving
     * Note: Acl udf access clock will stop when all port link down.
     */
    aml_readReg(unit, CKGCR, &u32dat, sizeof(u32dat));
    u32dat &= ~CKG_LNKDN_GLB_STOP;
    aml_writeReg(unit, CKGCR, &u32dat, sizeof(u32dat));

    /* setup LED for serial or parallel type */
    led_type.value = 0;
    hal_cfg_getValue(unit, AIR_CFG_TYPE_PHY_LED_TYPE, &led_type);
    if (0 == led_type.value)
    {
        /* setup serial LED */
        u32dat = 0x00760076;
        aml_writeReg(unit, SLED_FLASH_SET_CTRL0, &u32dat, sizeof(u32dat));
        aml_writeReg(unit, SLED_FLASH_SET_CTRL1, &u32dat, sizeof(u32dat));
        aml_writeReg(unit, SLED_FLASH_SET_CTRL2, &u32dat, sizeof(u32dat));
        aml_writeReg(unit, SLED_FLASH_SET_CTRL3, &u32dat, sizeof(u32dat));
        
        sled.value = 2;
        hal_cfg_getValue(unit, AIR_CFG_TYPE_PHY_LED_COUNT, &sled);
        //u32dat = ((sled.value << 1) + SLED_RISING_EDGE + SLED_OUTPUT_TRANSITION_MODE) | (0xFF << 8);
        u32dat = ((sled.value << 1) + SLED_RISING_EDGE + SLED_OUTPUT_CONTINUOUS_MODE) | (0xFF << 8);
        aml_writeReg(unit, SLED_CTRL0, &u32dat, sizeof(u32dat));

        aml_readReg(unit, RG_LAN_LED_IOMUX, &u32dat, sizeof(u32dat));
        u32dat |= RG_GPIO_SLED_MODE0;
        aml_writeReg(unit, RG_LAN_LED_IOMUX, &u32dat, sizeof(u32dat));
    }
    return AIR_E_OK;
}

/* FUNCTION NAME: hal_pearl_chip_deinit
 * PURPOSE:
 *      Chip initialization
 *
 * INPUT:
 *      unit            --  Device ID
 *
 * OUTPUT:
 *        None
 *
 * RETURN:
 *        AIR_E_OK
 *
 * NOTES:
 *      None
 */
AIR_ERROR_NO_T
hal_pearl_chip_deinit(
    const UI32_T unit)
{
    return AIR_E_OK;
}

