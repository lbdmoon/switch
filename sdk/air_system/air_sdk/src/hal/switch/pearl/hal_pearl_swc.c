/*******************************************************************************
*  Copyright Statement:
*  --------------------
*  This software is protected by Copyright and the information contained
*  herein is confidential. The software may not be copied and the information
*  contained herein may not be used or disclosed except with the written
*  permission of Airoha Technology Corp. (C) 2021
*
*  BY OPENING THIS FILE, BUYER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
*  THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("AIROHA SOFTWARE")
*  RECEIVED FROM AIROHA AND/OR ITS REPRESENTATIVES ARE PROVIDED TO BUYER ON
*  AN "AS-IS" BASIS ONLY. AIROHA EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
*  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
*  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
*  NEITHER DOES AIROHA PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
*  SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
*  SUPPLIED WITH THE AIROHA SOFTWARE, AND BUYER AGREES TO LOOK ONLY TO SUCH
*  THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. AIROHA SHALL ALSO
*  NOT BE RESPONSIBLE FOR ANY AIROHA SOFTWARE RELEASES MADE TO BUYER'S
*  SPECIFICATION OR TO CONFORM TO A PARTICULAR STANDARD OR OPEN FORUM.
*
*  BUYER'S SOLE AND EXCLUSIVE REMEDY AND AIROHA'S ENTIRE AND CUMULATIVE
*  LIABILITY WITH RESPECT TO THE AIROHA SOFTWARE RELEASED HEREUNDER WILL BE,
*  AT AIROHA'S OPTION, TO REVISE OR REPLACE THE AIROHA SOFTWARE AT ISSUE,
*  OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY BUYER TO
*  AIROHA FOR SUCH AIROHA SOFTWARE AT ISSUE.
*
*  THE TRANSACTION CONTEMPLATED HEREUNDER SHALL BE CONSTRUED IN ACCORDANCE
*  WITH THE LAWS OF THE STATE OF CALIFORNIA, USA, EXCLUDING ITS CONFLICT OF
*  LAWS PRINCIPLES.  ANY DISPUTES, CONTROVERSIES OR CLAIMS ARISING THEREOF AND
*  RELATED THERETO SHALL BE SETTLED BY ARBITRATION IN SAN FRANCISCO, CA, UNDER
*  THE RULES OF THE INTERNATIONAL CHAMBER OF COMMERCE (ICC).
*
*******************************************************************************/

/* FILE NAME:  hal_pearl_swc.c
 * PURPOSE:
 *  Implement switch module HAL function.
 *
 * NOTES:
 *
 */

/* INCLUDE FILE DECLARTIONS
*/
#include <osal/osal_lib.h>
#include <hal/switch/pearl/hal_pearl_swc.h>
#include <hal/switch/pearl/hal_pearl_reg.h>
#include <aml/aml.h>
#include <hal/common/hal.h>
#include <hal/common/hal_dbg.h>
#include <api/diag.h>
#include <cmlib/cmlib_bit.h>
#include <hal/common/hal_mdio.h>

/* NAMING CONSTANT DECLARATIONS
*/

/* MACRO FUNCTION DECLARATIONS
 */
#define _HAL_PEARL_SWC_SET_VALUE(__out__, __val__, __offset__, __length__) do                 \
{                                                                                           \
    (__out__) &= ~BITS_RANGE((__offset__), (__length__));                                   \
    (__out__) |= BITS_OFF_L((__val__), (__offset__), (__length__));                         \
}while(0)

/* DATA TYPE DECLARATIONS
*/

static const I16_T twiddle[65] =
{
    0x0,    0x0324, 0x0648, 0x096A, 0x0C8C, 0x0FAB, 0x12C8, 0x15E2,
    0x18F9, 0x1C0B, 0x1F1A, 0x2223, 0x2528, 0x2826, 0x2B1F, 0x2E11,
    0x30FB, 0x33DF, 0x36BA, 0x398C, 0x3C56, 0x3F17, 0x41CE, 0x447A,
    0x471C, 0x49B4, 0x4C3F, 0x4EBF, 0x5133, 0x539B, 0x55F5, 0x5842,
    0x5A82, 0x5CB3, 0x5ED7, 0x60EB, 0x62F1, 0x64E8, 0x66CF, 0x68A6,
    0x6A6D, 0x6C23, 0x6DC9, 0x6F5E, 0x70E2, 0x7254, 0x73B5, 0x7504,
    0x7641, 0x776B, 0x7884, 0x7989, 0x7A7C, 0x7B5C, 0x7C29, 0x7CE3,
    0x7D89, 0x7E1D, 0x7E9C, 0x7F09, 0x7F61, 0x7FA6, 0x7FD8, 0x7FF5,
    0x7FFF,
};


#define MAX_RX_JUMBO_LEN  (0x3)

/* GLOBAL VARIABLE DECLARATIONS
*/
DIAG_SET_MODULE_INFO(AIR_MODULE_SWC, "hal_pearl_swc.c");

/* LOCAL SUBPROGRAM SPECIFICATIONS
*/

/* STATIC VARIABLE DECLARATIONS
 */

/* EXPORTED SUBPROGRAM BODIES
*/
/* FUNCTION NAME: hal_pearl_swc_setMgmtFrameCfg
 * PURPOSE:
 *      Set management frame config.
 *
 * INPUT:
 *      unit            --  Select device ID
 *      ptr_cfg         --  config for specific management frame
 * OUTPUT:
 *      None
 *
 * RETURN:
 *      AIR_E_OK
 *      AIR_E_BAD_PARAMETER
 *
 * NOTES:
 *      REV_xx = 01-80-C2-00-00-xx of destination mac
 *      REV_UN = others
 */
AIR_ERROR_NO_T
hal_pearl_swc_setMgmtFrameCfg(
    const UI32_T                unit,
    AIR_SWC_MGMT_FRAME_CFG_T    *ptr_cfg)
{
    AIR_ERROR_NO_T ret = AIR_E_OK;
    UI32_T u32dat = 0;
    UI32_T enable = 0, pri_high = 0, fwd = 0;

    HAL_CHECK_ENUM_RANGE(ptr_cfg->frame_type, AIR_SWC_MGMT_FRAME_TYPE_LAST);
    HAL_CHECK_ENUM_RANGE(ptr_cfg->forward_mode, AIR_SWC_MGMT_FWD_MODE_LAST);

    if (ptr_cfg->flags & AIR_SWC_MGMT_FRAME_CFG_FLAGS_ENABLE)
    {
        enable = 1;
    }
    else
    {
        enable = 0;
    }

    if (ptr_cfg->flags & AIR_SWC_MGMT_FRAME_CFG_FLAGS_PRI_HIGH)
    {
        pri_high = 1;
    }
    else
    {
        pri_high = 0;
    }

    switch(ptr_cfg->forward_mode)
    {
        case AIR_SWC_MGMT_FWD_MODE_SYS_SETTING:
            fwd = HAL_PEARL_FWD_MODE_SYS_SETTING;
            break;
        case AIR_SWC_MGMT_FWD_MODE_SYS_SETTING_INCLUDE_CPU:
            fwd = HAL_PEARL_FWD_MODE_SYS_SETTING_INCLUDE_CPU;
            break;
        case AIR_SWC_MGMT_FWD_MODE_SYS_SETTING_EXCLUDE_CPU:
            fwd = HAL_PEARL_FWD_MODE_SYS_SETTING_EXCLUDE_CPU;
            break;
        case AIR_SWC_MGMT_FWD_MODE_CPU_ONLY:
            fwd = HAL_PEARL_FWD_MODE_CPU_ONLY;
            break;
        case AIR_SWC_MGMT_FWD_MODE_DROP:
            fwd = HAL_PEARL_FWD_MODE_DROP;
            break;
        default:
            break;
    }

    switch(ptr_cfg->frame_type)
    {
        case AIR_SWC_MGMT_FRAME_TYPE_IGMP:
            aml_readReg(unit, IMMC, &u32dat, sizeof(u32dat));
            _HAL_PEARL_SWC_SET_VALUE(u32dat, enable, REG_IGMP_MANG_OFFT, REG_IGMP_MANG_LENGTH);
            _HAL_PEARL_SWC_SET_VALUE(u32dat, pri_high, REG_IGMP_PRI_HIGH_OFFT, REG_IGMP_PRI_HIGH_LENGTH);
            _HAL_PEARL_SWC_SET_VALUE(u32dat, fwd, REG_IGMP_PORT_FWD_OFFT, REG_IGMP_PORT_FWD_LENGTH);
            aml_writeReg(unit, IMMC, &u32dat, sizeof(u32dat));
            break;
        case AIR_SWC_MGMT_FRAME_TYPE_PPPOE:
            aml_readReg(unit, APC, &u32dat, sizeof(u32dat));
            _HAL_PEARL_SWC_SET_VALUE(u32dat, enable, REG_PPPOE_MANG_OFFT, REG_PPPOE_MANG_LENGTH);
            _HAL_PEARL_SWC_SET_VALUE(u32dat, pri_high, REG_PPPOE_PRI_HIGH_OFFT, REG_PPPOE_PRI_HIGH_LENGTH);
            _HAL_PEARL_SWC_SET_VALUE(u32dat, fwd, REG_PPPOE_PORT_FWD_OFFT, REG_PPPOE_PORT_FWD_LENGTH);
            aml_writeReg(unit, APC, &u32dat, sizeof(u32dat));
            break;
        case AIR_SWC_MGMT_FRAME_TYPE_ARP:
            aml_readReg(unit, APC, &u32dat, sizeof(u32dat));
            _HAL_PEARL_SWC_SET_VALUE(u32dat, enable, REG_ARP_MANG_OFFT, REG_ARP_MANG_LENGTH);
            _HAL_PEARL_SWC_SET_VALUE(u32dat, pri_high, REG_ARP_PRI_HIGH_OFFT, REG_ARP_PRI_HIGH_LENGTH);
            _HAL_PEARL_SWC_SET_VALUE(u32dat, fwd, REG_ARP_PORT_FWD_OFFT, REG_ARP_PORT_FWD_LENGTH);
            aml_writeReg(unit, APC, &u32dat, sizeof(u32dat));
            break;
        case AIR_SWC_MGMT_FRAME_TYPE_PAE:
            aml_readReg(unit, PAC, &u32dat, sizeof(u32dat));
            _HAL_PEARL_SWC_SET_VALUE(u32dat, enable, REG_TAG_PAE_MANG_OFFT, REG_TAG_PAE_MANG_LENGTH);
            _HAL_PEARL_SWC_SET_VALUE(u32dat, pri_high, REG_TAG_PAE_PRI_HIGH_OFFT, REG_TAG_PAE_PRI_HIGH_LENGTH);
            _HAL_PEARL_SWC_SET_VALUE(u32dat, fwd, REG_TAG_PAE_PORT_FWD_OFFT, REG_TAG_PAE_PORT_FWD_LENGTH);
            _HAL_PEARL_SWC_SET_VALUE(u32dat, enable, REG_UTAG_PAE_MANG_OFFT, REG_UTAG_PAE_MANG_LENGTH);
            _HAL_PEARL_SWC_SET_VALUE(u32dat, pri_high, REG_UTAG_PAE_PRI_HIGH_OFFT, REG_UTAG_PAE_PRI_HIGH_LENGTH);
            _HAL_PEARL_SWC_SET_VALUE(u32dat, fwd, REG_UTAG_PAE_PORT_FWD_OFFT, REG_UTAG_PAE_PORT_FWD_LENGTH);
            aml_writeReg(unit, PAC, &u32dat, sizeof(u32dat));
            break;
        case AIR_SWC_MGMT_FRAME_TYPE_DHCP:
            aml_readReg(unit, DHCP, &u32dat, sizeof(u32dat));
            _HAL_PEARL_SWC_SET_VALUE(u32dat, enable, REG_DHCP_6_MANG_OFFT, REG_DHCP_6_MANG_LENGTH);
            _HAL_PEARL_SWC_SET_VALUE(u32dat, pri_high, REG_DHCP_6_PRI_HIGH_OFFT, REG_DHCP_6_PRI_HIGH_LENGTH);
            _HAL_PEARL_SWC_SET_VALUE(u32dat, fwd, REG_DHCP_6_PORT_FWD_OFFT, REG_DHCP_6_PORT_FWD_LENGTH);
            _HAL_PEARL_SWC_SET_VALUE(u32dat, enable, REG_DHCP_4_MANG_OFFT, REG_DHCP_4_MANG_LENGTH);
            _HAL_PEARL_SWC_SET_VALUE(u32dat, pri_high, REG_DHCP_4_PRI_HIGH_OFFT, REG_DHCP_4_PRI_HIGH_LENGTH);
            _HAL_PEARL_SWC_SET_VALUE(u32dat, fwd, REG_DHCP_4_PORT_FWD_OFFT, REG_DHCP_4_PORT_FWD_LENGTH);
            aml_writeReg(unit, DHCP, &u32dat, sizeof(u32dat));
            break;
        case AIR_SWC_MGMT_FRAME_TYPE_TTL_0:
            aml_readReg(unit, BPC, &u32dat, sizeof(u32dat));
            _HAL_PEARL_SWC_SET_VALUE(u32dat, enable, REG_TTL_0_MANG_OFFT, REG_TTL_0_MANG_LENGTH);
            _HAL_PEARL_SWC_SET_VALUE(u32dat, pri_high, REG_TTL_0_PRI_HIGH_OFFT, REG_TTL_0_PRI_HIGH_LENGTH);
            _HAL_PEARL_SWC_SET_VALUE(u32dat, fwd, REG_TTL_0_PORT_FWD_OFFT, REG_TTL_0_PORT_FWD_LENGTH);
            aml_writeReg(unit, BPC, &u32dat, sizeof(u32dat));
            break;
        case AIR_SWC_MGMT_FRAME_TYPE_BPDU:
            aml_readReg(unit, BPC, &u32dat, sizeof(u32dat));
            _HAL_PEARL_SWC_SET_VALUE(u32dat, enable, REG_BPDU_MANG_OFFT, REG_BPDU_MANG_LENGTH);
            _HAL_PEARL_SWC_SET_VALUE(u32dat, pri_high, REG_BPDU_PRI_HIGH_OFFT, REG_BPDU_PRI_HIGH_LENGTH);
            _HAL_PEARL_SWC_SET_VALUE(u32dat, fwd, REG_BPDU_PORT_FWD_OFFT, REG_BPDU_PORT_FWD_LENGTH);
            aml_writeReg(unit, BPC, &u32dat, sizeof(u32dat));
            break;
        case AIR_SWC_MGMT_FRAME_TYPE_MLD:
            aml_readReg(unit, IMMC, &u32dat, sizeof(u32dat));
            _HAL_PEARL_SWC_SET_VALUE(u32dat, enable, REG_MLD_MANG_OFFT, REG_MLD_MANG_LENGTH);
            _HAL_PEARL_SWC_SET_VALUE(u32dat, pri_high, REG_MLD_PRI_HIGH_OFFT, REG_MLD_PRI_HIGH_LENGTH);
            _HAL_PEARL_SWC_SET_VALUE(u32dat, fwd, REG_MLD_PORT_FWD_OFFT, REG_MLD_PORT_FWD_LENGTH);
            aml_writeReg(unit, IMMC, &u32dat, sizeof(u32dat));
            break;
        case AIR_SWC_MGMT_FRAME_TYPE_REV_01:
            aml_readReg(unit, RGAC1, &u32dat, sizeof(u32dat));
            _HAL_PEARL_SWC_SET_VALUE(u32dat, enable, REG_REV_01_MANG_OFFT, REG_REV_01_MANG_LENGTH);
            _HAL_PEARL_SWC_SET_VALUE(u32dat, pri_high, REG_REV_01_PRI_HIGH_OFFT, REG_REV_01_PRI_HIGH_LENGTH);
            _HAL_PEARL_SWC_SET_VALUE(u32dat, fwd, REG_REV_01_PORT_FWD_OFFT, REG_REV_01_PORT_FWD_LENGTH);
            aml_writeReg(unit, RGAC1, &u32dat, sizeof(u32dat));
            break;
        case AIR_SWC_MGMT_FRAME_TYPE_REV_02:
            aml_readReg(unit, RGAC1, &u32dat, sizeof(u32dat));
            _HAL_PEARL_SWC_SET_VALUE(u32dat, enable, REG_REV_02_MANG_OFFT, REG_REV_02_MANG_LENGTH);
            _HAL_PEARL_SWC_SET_VALUE(u32dat, pri_high, REG_REV_02_PRI_HIGH_OFFT, REG_REV_02_PRI_HIGH_LENGTH);
            _HAL_PEARL_SWC_SET_VALUE(u32dat, fwd, REG_REV_02_PORT_FWD_OFFT, REG_REV_02_PORT_FWD_LENGTH);
            aml_writeReg(unit, RGAC1, &u32dat, sizeof(u32dat));
            break;
        case AIR_SWC_MGMT_FRAME_TYPE_REV_03:
            aml_readReg(unit, RGAC2, &u32dat, sizeof(u32dat));
            _HAL_PEARL_SWC_SET_VALUE(u32dat, enable, REG_REV_03_MANG_OFFT, REG_REV_03_MANG_LENGTH);
            _HAL_PEARL_SWC_SET_VALUE(u32dat, pri_high, REG_REV_03_PRI_HIGH_OFFT, REG_REV_03_PRI_HIGH_LENGTH);
            _HAL_PEARL_SWC_SET_VALUE(u32dat, fwd, REG_REV_03_PORT_FWD_OFFT, REG_REV_03_PORT_FWD_LENGTH);
            aml_writeReg(unit, RGAC2, &u32dat, sizeof(u32dat));
            break;
        case AIR_SWC_MGMT_FRAME_TYPE_REV_0E:
            aml_readReg(unit, RGAC2, &u32dat, sizeof(u32dat));
            _HAL_PEARL_SWC_SET_VALUE(u32dat, enable, REG_REV_0E_MANG_OFFT, REG_REV_0E_MANG_LENGTH);
            _HAL_PEARL_SWC_SET_VALUE(u32dat, pri_high, REG_REV_0E_PRI_HIGH_OFFT, REG_REV_0E_PRI_HIGH_LENGTH);
            _HAL_PEARL_SWC_SET_VALUE(u32dat, fwd, REG_REV_0E_PORT_FWD_OFFT, REG_REV_0E_PORT_FWD_LENGTH);
            aml_writeReg(unit, RGAC2, &u32dat, sizeof(u32dat));
            break;
        case AIR_SWC_MGMT_FRAME_TYPE_REV_10:
            aml_readReg(unit, RGAC3, &u32dat, sizeof(u32dat));
            _HAL_PEARL_SWC_SET_VALUE(u32dat, enable, REG_REV_10_MANG_OFFT, REG_REV_10_MANG_LENGTH);
            _HAL_PEARL_SWC_SET_VALUE(u32dat, pri_high, REG_REV_10_PRI_HIGH_OFFT, REG_REV_10_PRI_HIGH_LENGTH);
            _HAL_PEARL_SWC_SET_VALUE(u32dat, fwd, REG_REV_10_PORT_FWD_OFFT, REG_REV_10_PORT_FWD_LENGTH);
            aml_writeReg(unit, RGAC3, &u32dat, sizeof(u32dat));
            break;
        case AIR_SWC_MGMT_FRAME_TYPE_REV_20:
            aml_readReg(unit, RGAC3, &u32dat, sizeof(u32dat));
            _HAL_PEARL_SWC_SET_VALUE(u32dat, enable, REG_REV_20_MANG_OFFT, REG_REV_20_MANG_LENGTH);
            _HAL_PEARL_SWC_SET_VALUE(u32dat, pri_high, REG_REV_20_PRI_HIGH_OFFT, REG_REV_20_PRI_HIGH_LENGTH);
            _HAL_PEARL_SWC_SET_VALUE(u32dat, fwd, REG_REV_20_PORT_FWD_OFFT, REG_REV_20_PORT_FWD_LENGTH);
            aml_writeReg(unit, RGAC3, &u32dat, sizeof(u32dat));
            break;
        case AIR_SWC_MGMT_FRAME_TYPE_REV_21:
            aml_readReg(unit, RGAC4, &u32dat, sizeof(u32dat));
            _HAL_PEARL_SWC_SET_VALUE(u32dat, enable, REG_REV_21_MANG_OFFT, REG_REV_21_MANG_LENGTH);
            _HAL_PEARL_SWC_SET_VALUE(u32dat, pri_high, REG_REV_21_PRI_HIGH_OFFT, REG_REV_21_PRI_HIGH_LENGTH);
            _HAL_PEARL_SWC_SET_VALUE(u32dat, fwd, REG_REV_21_PORT_FWD_OFFT, REG_REV_21_PORT_FWD_LENGTH);
            aml_writeReg(unit, RGAC4, &u32dat, sizeof(u32dat));
            break;
        case AIR_SWC_MGMT_FRAME_TYPE_REV_UN:
            aml_readReg(unit, RGAC4, &u32dat, sizeof(u32dat));
            _HAL_PEARL_SWC_SET_VALUE(u32dat, enable, REG_REV_UN_MANG_OFFT, REG_REV_UN_MANG_LENGTH);
            _HAL_PEARL_SWC_SET_VALUE(u32dat, pri_high, REG_REV_UN_PRI_HIGH_OFFT, REG_REV_UN_PRI_HIGH_LENGTH);
            _HAL_PEARL_SWC_SET_VALUE(u32dat, fwd, REG_REV_UN_PORT_FWD_OFFT, REG_REV_UN_PORT_FWD_LENGTH);
            aml_writeReg(unit, RGAC4, &u32dat, sizeof(u32dat));
            break;
        default:
            break;
    }
    return ret;
}

/* FUNCTION NAME: hal_pearl_swc_getMgmtFrameCfg
 * PURPOSE:
 *     Get management frame config.
 *
 * INPUT:
 *      unit            --  Select device ID
 * OUTPUT:
 *      ptr_cfg         --  config for specific management frame
 *
 * RETURN:
 *      AIR_E_OK
 *      AIR_E_BAD_PARAMETER
 *
 * NOTES:
 *      REV_xx = 01-80-C2-00-00-xx of destination mac
 *      REV_UN = others
 */
AIR_ERROR_NO_T
hal_pearl_swc_getMgmtFrameCfg(
    const UI32_T                unit,
    AIR_SWC_MGMT_FRAME_CFG_T    *ptr_cfg)
{
    AIR_ERROR_NO_T ret = AIR_E_OK;
    UI32_T u32dat = 0;
    UI32_T enable = 0, pri_high = 0, fwd = 0;
    HAL_CHECK_ENUM_RANGE(ptr_cfg->frame_type, AIR_SWC_MGMT_FRAME_TYPE_LAST);

    switch(ptr_cfg->frame_type)
    {
        case AIR_SWC_MGMT_FRAME_TYPE_IGMP:
            aml_readReg(unit, IMMC, &u32dat, sizeof(u32dat));
            enable = BITS_OFF_R(u32dat, REG_IGMP_MANG_OFFT, REG_IGMP_MANG_LENGTH);
            pri_high = BITS_OFF_R(u32dat, REG_IGMP_PRI_HIGH_OFFT, REG_IGMP_PRI_HIGH_LENGTH);
            fwd = BITS_OFF_R(u32dat, REG_IGMP_PORT_FWD_OFFT, REG_IGMP_PORT_FWD_LENGTH);
            break;
        case AIR_SWC_MGMT_FRAME_TYPE_PPPOE:
            aml_readReg(unit, APC, &u32dat, sizeof(u32dat));
            enable = BITS_OFF_R(u32dat, REG_PPPOE_MANG_OFFT, REG_PPPOE_MANG_LENGTH);
            pri_high = BITS_OFF_R(u32dat, REG_PPPOE_PRI_HIGH_OFFT, REG_PPPOE_PRI_HIGH_LENGTH);
            fwd = BITS_OFF_R(u32dat, REG_PPPOE_PORT_FWD_OFFT, REG_PPPOE_PORT_FWD_LENGTH);
            break;
        case AIR_SWC_MGMT_FRAME_TYPE_ARP:
            aml_readReg(unit, APC, &u32dat, sizeof(u32dat));
            enable = BITS_OFF_R(u32dat, REG_ARP_MANG_OFFT, REG_ARP_MANG_LENGTH);
            pri_high = BITS_OFF_R(u32dat, REG_ARP_PRI_HIGH_OFFT, REG_ARP_PRI_HIGH_LENGTH);
            fwd = BITS_OFF_R(u32dat, REG_ARP_PORT_FWD_OFFT, REG_ARP_PORT_FWD_LENGTH);
            break;
        case AIR_SWC_MGMT_FRAME_TYPE_PAE:
            aml_readReg(unit, PAC, &u32dat, sizeof(u32dat));
            enable = BITS_OFF_R(u32dat, REG_TAG_PAE_MANG_OFFT, REG_TAG_PAE_MANG_LENGTH);
            pri_high = BITS_OFF_R(u32dat, REG_TAG_PAE_PRI_HIGH_OFFT, REG_TAG_PAE_PRI_HIGH_LENGTH);
            fwd = BITS_OFF_R(u32dat, REG_TAG_PAE_PORT_FWD_OFFT, REG_TAG_PAE_PORT_FWD_LENGTH);
            enable = BITS_OFF_R(u32dat, REG_UTAG_PAE_MANG_OFFT, REG_UTAG_PAE_MANG_LENGTH);
            pri_high = BITS_OFF_R(u32dat, REG_UTAG_PAE_PRI_HIGH_OFFT, REG_UTAG_PAE_PRI_HIGH_LENGTH);
            fwd = BITS_OFF_R(u32dat, REG_UTAG_PAE_PORT_FWD_OFFT, REG_UTAG_PAE_PORT_FWD_LENGTH);
            break;
        case AIR_SWC_MGMT_FRAME_TYPE_DHCP:
            aml_readReg(unit, DHCP, &u32dat, sizeof(u32dat));
            enable = BITS_OFF_R(u32dat, REG_DHCP_6_MANG_OFFT, REG_DHCP_6_MANG_LENGTH);
            pri_high = BITS_OFF_R(u32dat, REG_DHCP_6_PRI_HIGH_OFFT, REG_DHCP_6_PRI_HIGH_LENGTH);
            fwd = BITS_OFF_R(u32dat, REG_DHCP_6_PORT_FWD_OFFT, REG_DHCP_6_PORT_FWD_LENGTH);
            enable = BITS_OFF_R(u32dat, REG_DHCP_4_MANG_OFFT, REG_DHCP_4_MANG_LENGTH);
            pri_high = BITS_OFF_R(u32dat, REG_DHCP_4_PRI_HIGH_OFFT, REG_DHCP_4_PRI_HIGH_LENGTH);
            fwd = BITS_OFF_R(u32dat, REG_DHCP_4_PORT_FWD_OFFT, REG_DHCP_4_PORT_FWD_LENGTH);
            break;
        case AIR_SWC_MGMT_FRAME_TYPE_TTL_0:
            aml_readReg(unit, BPC, &u32dat, sizeof(u32dat));
            enable = BITS_OFF_R(u32dat, REG_TTL_0_MANG_OFFT, REG_TTL_0_MANG_LENGTH);
            pri_high = BITS_OFF_R(u32dat, REG_TTL_0_PRI_HIGH_OFFT, REG_TTL_0_PRI_HIGH_LENGTH);
            fwd = BITS_OFF_R(u32dat, REG_TTL_0_PORT_FWD_OFFT, REG_TTL_0_PORT_FWD_LENGTH);
            break;
        case AIR_SWC_MGMT_FRAME_TYPE_BPDU:
            aml_readReg(unit, BPC, &u32dat, sizeof(u32dat));
            enable = BITS_OFF_R(u32dat, REG_BPDU_MANG_OFFT, REG_BPDU_MANG_LENGTH);
            pri_high = BITS_OFF_R(u32dat, REG_BPDU_PRI_HIGH_OFFT, REG_BPDU_PRI_HIGH_LENGTH);
            fwd = BITS_OFF_R(u32dat, REG_BPDU_PORT_FWD_OFFT, REG_BPDU_PORT_FWD_LENGTH);
            break;
        case AIR_SWC_MGMT_FRAME_TYPE_MLD:
            aml_readReg(unit, IMMC, &u32dat, sizeof(u32dat));
            enable = BITS_OFF_R(u32dat, REG_MLD_MANG_OFFT, REG_MLD_MANG_LENGTH);
            pri_high = BITS_OFF_R(u32dat, REG_MLD_PRI_HIGH_OFFT, REG_MLD_PRI_HIGH_LENGTH);
            fwd = BITS_OFF_R(u32dat, REG_MLD_PORT_FWD_OFFT, REG_MLD_PORT_FWD_LENGTH);
            break;
        case AIR_SWC_MGMT_FRAME_TYPE_REV_01:
            aml_readReg(unit, RGAC1, &u32dat, sizeof(u32dat));
            enable = BITS_OFF_R(u32dat, REG_REV_01_MANG_OFFT, REG_REV_01_MANG_LENGTH);
            pri_high = BITS_OFF_R(u32dat, REG_REV_01_PRI_HIGH_OFFT, REG_REV_01_PRI_HIGH_LENGTH);
            fwd = BITS_OFF_R(u32dat, REG_REV_01_PORT_FWD_OFFT, REG_REV_01_PORT_FWD_LENGTH);
            break;
        case AIR_SWC_MGMT_FRAME_TYPE_REV_02:
            aml_readReg(unit, RGAC1, &u32dat, sizeof(u32dat));
            enable = BITS_OFF_R(u32dat, REG_REV_02_MANG_OFFT, REG_REV_02_MANG_LENGTH);
            pri_high = BITS_OFF_R(u32dat, REG_REV_02_PRI_HIGH_OFFT, REG_REV_02_PRI_HIGH_LENGTH);
            fwd = BITS_OFF_R(u32dat, REG_REV_02_PORT_FWD_OFFT, REG_REV_02_PORT_FWD_LENGTH);
            break;
        case AIR_SWC_MGMT_FRAME_TYPE_REV_03:
            aml_readReg(unit, RGAC2, &u32dat, sizeof(u32dat));
            enable = BITS_OFF_R(u32dat, REG_REV_03_MANG_OFFT, REG_REV_03_MANG_LENGTH);
            pri_high = BITS_OFF_R(u32dat, REG_REV_03_PRI_HIGH_OFFT, REG_REV_03_PRI_HIGH_LENGTH);
            fwd = BITS_OFF_R(u32dat, REG_REV_03_PORT_FWD_OFFT, REG_REV_03_PORT_FWD_LENGTH);
            break;
        case AIR_SWC_MGMT_FRAME_TYPE_REV_0E:
            aml_readReg(unit, RGAC2, &u32dat, sizeof(u32dat));
            enable = BITS_OFF_R(u32dat, REG_REV_0E_MANG_OFFT, REG_REV_0E_MANG_LENGTH);
            pri_high = BITS_OFF_R(u32dat, REG_REV_0E_PRI_HIGH_OFFT, REG_REV_0E_PRI_HIGH_LENGTH);
            fwd = BITS_OFF_R(u32dat, REG_REV_0E_PORT_FWD_OFFT, REG_REV_0E_PORT_FWD_LENGTH);
            break;
        case AIR_SWC_MGMT_FRAME_TYPE_REV_10:
            aml_readReg(unit, RGAC3, &u32dat, sizeof(u32dat));
            enable = BITS_OFF_R(u32dat, REG_REV_10_MANG_OFFT, REG_REV_10_MANG_LENGTH);
            pri_high = BITS_OFF_R(u32dat, REG_REV_10_PRI_HIGH_OFFT, REG_REV_10_PRI_HIGH_LENGTH);
            fwd = BITS_OFF_R(u32dat, REG_REV_10_PORT_FWD_OFFT, REG_REV_10_PORT_FWD_LENGTH);
            break;
        case AIR_SWC_MGMT_FRAME_TYPE_REV_20:
            aml_readReg(unit, RGAC3, &u32dat, sizeof(u32dat));
            enable = BITS_OFF_R(u32dat, REG_REV_20_MANG_OFFT, REG_REV_20_MANG_LENGTH);
            pri_high = BITS_OFF_R(u32dat, REG_REV_20_PRI_HIGH_OFFT, REG_REV_20_PRI_HIGH_LENGTH);
            fwd = BITS_OFF_R(u32dat, REG_REV_20_PORT_FWD_OFFT, REG_REV_20_PORT_FWD_LENGTH);
            break;
        case AIR_SWC_MGMT_FRAME_TYPE_REV_21:
            aml_readReg(unit, RGAC4, &u32dat, sizeof(u32dat));
            enable = BITS_OFF_R(u32dat, REG_REV_21_MANG_OFFT, REG_REV_21_MANG_LENGTH);
            pri_high = BITS_OFF_R(u32dat, REG_REV_21_PRI_HIGH_OFFT, REG_REV_21_PRI_HIGH_LENGTH);
            fwd = BITS_OFF_R(u32dat, REG_REV_21_PORT_FWD_OFFT, REG_REV_21_PORT_FWD_LENGTH);
            break;
        case AIR_SWC_MGMT_FRAME_TYPE_REV_UN:
            aml_readReg(unit, RGAC4, &u32dat, sizeof(u32dat));
            enable = BITS_OFF_R(u32dat, REG_REV_UN_MANG_OFFT, REG_REV_UN_MANG_LENGTH);
            pri_high = BITS_OFF_R(u32dat, REG_REV_UN_PRI_HIGH_OFFT, REG_REV_UN_PRI_HIGH_LENGTH);
            fwd = BITS_OFF_R(u32dat, REG_REV_UN_PORT_FWD_OFFT, REG_REV_UN_PORT_FWD_LENGTH);
            break;
        default:
            break;
    }
    if (1 == enable)
    {
        ptr_cfg->flags |= AIR_SWC_MGMT_FRAME_CFG_FLAGS_ENABLE;
    }
    else
    {
        ptr_cfg->flags &= ~AIR_SWC_MGMT_FRAME_CFG_FLAGS_ENABLE;
    }

    if (1 == pri_high)
    {
        ptr_cfg->flags |= AIR_SWC_MGMT_FRAME_CFG_FLAGS_PRI_HIGH;
    }
    else
    {
        ptr_cfg->flags &= ~AIR_SWC_MGMT_FRAME_CFG_FLAGS_PRI_HIGH;
    }

    switch(fwd)
    {
        case HAL_PEARL_FWD_MODE_SYS_SETTING:
            ptr_cfg->forward_mode = AIR_SWC_MGMT_FWD_MODE_SYS_SETTING;
            break;
        case HAL_PEARL_FWD_MODE_SYS_SETTING_INCLUDE_CPU:
            ptr_cfg->forward_mode = AIR_SWC_MGMT_FWD_MODE_SYS_SETTING_INCLUDE_CPU;
            break;
        case HAL_PEARL_FWD_MODE_SYS_SETTING_EXCLUDE_CPU:
            ptr_cfg->forward_mode = AIR_SWC_MGMT_FWD_MODE_SYS_SETTING_EXCLUDE_CPU;
            break;
        case HAL_PEARL_FWD_MODE_CPU_ONLY:
            ptr_cfg->forward_mode = AIR_SWC_MGMT_FWD_MODE_CPU_ONLY;
            break;
        case HAL_PEARL_FWD_MODE_DROP:
            ptr_cfg->forward_mode = AIR_SWC_MGMT_FWD_MODE_DROP;
            break;
        default:
            break;
    }

    return ret;
}


/* FUNCTION NAME: hal_pearl_swc_setSystemMac
 * PURPOSE:
 *      Set the system MAC address.
 *
 * INPUT:
 *      unit            --  Select device ID
 *      mac             --  System MAC address
 *
 * OUTPUT:
 *      None
 *
 * RETURN:
 *      AIR_E_OK
 *      AIR_E_BAD_PARAMETER
 *
 * NOTES:
 *      It's unique and specified for pause frame.
 */
AIR_ERROR_NO_T
hal_pearl_swc_setSystemMac(
    const UI32_T unit,
    const AIR_MAC_T mac)
{
    UI32_T u32dat;
    UI8_T i;

    /* SMACCR 1 */
    u32dat=0;
    for (i = 0; i < 2; i++)
    {
        u32dat |= BITS_OFF_L(mac[i], ((1 - i) * 8), 8);
    }
    aml_writeReg(unit, SMACCR1, &u32dat, sizeof(u32dat));

    /* SMACCR 0 */
    u32dat=0;
    for (i = 2; i < 6; i++)
    {
        u32dat |= BITS_OFF_L(mac[i], ((5 - i) * 8), 8);
    }
    aml_writeReg(unit, SMACCR0, &u32dat, sizeof(u32dat));

    return AIR_E_OK;
}

/* FUNCTION NAME: hal_pearl_swc_getSysMac
 * PURPOSE:
 *      Get the system MAC address.
 *
 * INPUT:
 *      unit            --  Select device ID
 *
 * OUTPUT:
 *      mac             --  System MAC address
 *
 * RETURN:
 *      AIR_E_OK
 *      AIR_E_BAD_PARAMETER
 *
 * NOTES:
 *      It's unique and specified for pause frame.
 */
AIR_ERROR_NO_T
hal_pearl_swc_getSystemMac(
    const UI32_T unit,
    AIR_MAC_T mac)
{
    UI32_T u32dat;
    UI8_T i;

    /* SMACCR 1 */
    aml_readReg(unit, SMACCR1, &u32dat, sizeof(u32dat));
    for (i = 0; i < 2; i++)
    {
        mac[i] = BITS_OFF_R(u32dat, ((1 - i) * 8), 8);
    }

    /* SMACCR 0 */
    aml_readReg(unit, SMACCR0, &u32dat, sizeof(u32dat));
    for (i = 2; i < 6; i++)
    {
        mac[i] = BITS_OFF_R(u32dat, ((5 - i) * 8), 8);
    }

    return AIR_E_OK;
}

/* FUNCTION NAME:
 *      hal_pearl_swc_init
 * PURPOSE:
 *      This API is used to initialize switch related items:
 *      1. Set REV02/03/0E/20/21 as BPDU frames.
 *      2. Dynamic entry auto deleted when port link down.
 * INPUT:
 *      unit             -- unit id
 * OUTPUT:
 *      None
 * RETURN:
 *      AIR_E_OK
 *      AIR_E_BAD_PARAMETER
 * NOTES:
 *      None
 */
AIR_ERROR_NO_T
hal_pearl_swc_init(
    const UI32_T      unit)
{
    UI32_T u32dat = 0;
    AIR_PORT_BITMAP_T   pbm = {0}, mac_pbm = {0};

    aml_readReg(unit, RGAC1, &u32dat, sizeof(u32dat));
    u32dat |= BIT(REG_REV_02_BPDU_OFFT);
    aml_writeReg(unit, RGAC1, &u32dat, sizeof(u32dat));

    aml_readReg(unit, RGAC2, &u32dat, sizeof(u32dat));
    u32dat |= BIT(REG_REV_0E_BPDU_OFFT);
    u32dat |= BIT(REG_REV_03_BPDU_OFFT);
    aml_writeReg(unit, RGAC2, &u32dat, sizeof(u32dat));

    aml_readReg(unit, RGAC3, &u32dat, sizeof(u32dat));
    u32dat |= BIT(REG_REV_20_BPDU_OFFT);
    aml_writeReg(unit, RGAC3, &u32dat, sizeof(u32dat));

    aml_readReg(unit, RGAC4, &u32dat, sizeof(u32dat));
    u32dat |= BIT(REG_REV_21_BPDU_OFFT);
    aml_writeReg(unit, RGAC4, &u32dat, sizeof(u32dat));

    aml_readReg(unit, AAC, &u32dat, sizeof(u32dat));
    u32dat |= BIT(AAC_AUTO_FLUSH_OFFSET);
    aml_writeReg(unit, AAC, &u32dat, sizeof(u32dat));

    /* Set ARP forward including CPU  */
    aml_readReg(unit, APC, &u32dat, sizeof(u32dat));
    _HAL_PEARL_SWC_SET_VALUE(u32dat, HAL_PEARL_FWD_MODE_SYS_SETTING_INCLUDE_CPU, REG_ARP_PORT_FWD_OFFT, REG_ARP_PORT_FWD_LENGTH);
    aml_writeReg(unit, APC, &u32dat, sizeof(u32dat));

    /* Set mtcc = 0 */
    aml_readReg(unit, GMACCR, &u32dat, sizeof(u32dat));
    u32dat &= ~BITS_RANGE(GMACCR_MTCC_OFFT, GMACCR_MTCC_LENG);
    aml_writeReg(unit, GMACCR, &u32dat, sizeof(u32dat));

    AIR_PORT_BITMAP_COPY(pbm, HAL_PORT_BMP_ETH(unit));
    AIR_PORT_DEL(pbm, HAL_CPU_PORT(unit));
    HAL_AIR_PBMP_TO_MAC_PBMP(unit, pbm, mac_pbm);

    /* Set IGMP query message forwarding port bitmap */
    aml_writeReg(unit, DRP, &mac_pbm[0], sizeof(UI32_T));

    /* Set IGMP router port bitmap */
    aml_writeReg(unit, QRYP, &mac_pbm[0], sizeof(UI32_T));

    /* SLED clock rate fine tune; refresh 60Hz, clock 2.08Mhz */
    u32dat = (35000 << 16) + 12;
    aml_writeReg(unit, SLED_CTRL1, &u32dat, sizeof(u32dat));

    /* Set default rate limit include preamble/IPG/CRC */
    aml_readReg(unit, GERLCR, &u32dat, sizeof(u32dat));
    _HAL_PEARL_SWC_SET_VALUE(u32dat, L1_RATE_IPG_BYTE_CNT, REG_IPG_BYTE_OFFT, REG_IPG_BYTE_LENG);
    aml_writeReg(unit, GERLCR, &u32dat, sizeof(u32dat));

    aml_readReg(unit, GIRLCR, &u32dat, sizeof(u32dat));
    _HAL_PEARL_SWC_SET_VALUE(u32dat, L1_RATE_IPG_BYTE_CNT, REG_IPG_BYTE_OFFT, REG_IPG_BYTE_LENG);
    aml_writeReg(unit, GIRLCR, &u32dat, sizeof(u32dat));

    aml_readReg(unit, AGC, &u32dat, sizeof(u32dat));
    _HAL_PEARL_SWC_SET_VALUE(u32dat, L1_RATE_IPG_BYTE_CNT, AGC_COMP_BNUM_OFFT, AGC_COMP_BNUM_LENG);
    aml_writeReg(unit, AGC, &u32dat, sizeof(u32dat));
    return AIR_E_OK;
}

/* FUNCTION NAME: hal_pearl_swc_setJumboSize
 * PURPOSE:
 *      Set accepting jumbo frmes with specificied size.
 *
 * INPUT:
 *      unit            --  Select device ID
 *      frame_len       --  AIR_SWC_JUMBO_SIZE_1518,
 *                          AIR_SWC_JUMBO_SIZE_1536,
 *                          AIR_SWC_JUMBO_SIZE_1552,
 *                          AIR_SWC_JUMBO_SIZE_2048,
 *                          AIR_SWC_JUMBO_SIZE_3072,
 *                          AIR_SWC_JUMBO_SIZE_4096,
 *                          AIR_SWC_JUMBO_SIZE_5120,
 *                          AIR_SWC_JUMBO_SIZE_6144,
 *                          AIR_SWC_JUMBO_SIZE_7168,
 *                          AIR_SWC_JUMBO_SIZE_8192,
 *                          AIR_SWC_JUMBO_SIZE_9216,
 *                          AIR_SWC_JUMBO_SIZE_12288,
 *                          AIR_SWC_JUMBO_SIZE_15360,
 * OUTPUT:
 *      None
 *
 * RETURN:
 *      AIR_E_OK
 *      AIR_E_BAD_PARAMETER
 *
 * NOTES:
 *      None
 */

AIR_ERROR_NO_T
hal_pearl_swc_setJumboSize(
    const UI32_T            unit,
    const AIR_SWC_JUMBO_SIZE_T   frame_len)
{

    UI32_T u32dat = 0;
    DIAG_PRINT(HAL_DBG_INFO,"[Dbg]: frame_len = %d\n", frame_len);
    /* Read and clear jumbo frame info */
    aml_readReg(unit, GMACCR, &u32dat, sizeof(u32dat));

    u32dat &= ~BITS_RANGE(MAX_RX_PKT_LEN_OFFT, MAX_RX_PKT_LEN_LENG);
    u32dat &= ~BITS_RANGE(MAX_RX_JUMBO_OFFT, MAX_RX_JUMBO_LENG);

    if ( frame_len < MAX_RX_JUMBO_LEN)
    {
        u32dat |= frame_len;
    }
    else /* AIR_SWC_JUMBO_SIZE_2048 ~ AIR_SWC_JUMBO_SIZE_15360*/
    {
        u32dat |= MAX_RX_JUMBO_LEN;
        u32dat |= (((frame_len - AIR_SWC_JUMBO_SIZE_2048) + 2) << MAX_RX_JUMBO_OFFT);

    }
    DIAG_PRINT(HAL_DBG_INFO,"[Dbg]: GMACCR value = %x\n", u32dat);
    aml_writeReg(unit, GMACCR, &u32dat, sizeof(u32dat));

    return AIR_E_OK;
}

/* FUNCTION NAME: hal_pearl_swc_getJumboSize
 * PURPOSE:
 *      Get accepting jumbo frmes with specificied size.
 *
 * INPUT:
 *      unit            --  Select device ID
 *
 * OUTPUT:
 *      ptr_frame_len   --  AIR_SWC_JUMBO_SIZE_1518,
 *                          AIR_SWC_JUMBO_SIZE_1536,
 *                          AIR_SWC_JUMBO_SIZE_1552,
 *                          AIR_SWC_JUMBO_SIZE_2048,
 *                          AIR_SWC_JUMBO_SIZE_3072,
 *                          AIR_SWC_JUMBO_SIZE_4096,
 *                          AIR_SWC_JUMBO_SIZE_5120,
 *                          AIR_SWC_JUMBO_SIZE_6144,
 *                          AIR_SWC_JUMBO_SIZE_7168,
 *                          AIR_SWC_JUMBO_SIZE_8192,
 *                          AIR_SWC_JUMBO_SIZE_9216,
 *                          AIR_SWC_JUMBO_SIZE_12288,
 *                          AIR_SWC_JUMBO_SIZE_15360,
 * RETURN:
 *      AIR_E_OK
 *      AIR_E_BAD_PARAMETER
 *
 * NOTES:
 *      None
 */
AIR_ERROR_NO_T
hal_pearl_swc_getJumboSize(
    const UI32_T    unit,
    AIR_SWC_JUMBO_SIZE_T *ptr_frame_len)
{
    UI32_T u32dat = 0, pkt_len;

    /* Read and clear jumbo frame info */
    aml_readReg(unit, GMACCR, &u32dat, sizeof(u32dat));

    pkt_len = BITS_OFF_R(u32dat, MAX_RX_PKT_LEN_OFFT, MAX_RX_PKT_LEN_LENG);

    if(pkt_len < MAX_RX_JUMBO_LEN)
    {
        *ptr_frame_len = pkt_len;
    }
    else
    {
        pkt_len = BITS_OFF_R(u32dat, MAX_RX_JUMBO_OFFT, MAX_RX_JUMBO_LENG);
        *ptr_frame_len = pkt_len + 1;
    }
    return AIR_E_OK;
}

/* FUNCTION NAME: hal_pearl_swc_setProperty
 * PURPOSE:
 *      Set switch property.
 *
 * INPUT:
 *      unit            --  Select device ID
 *      property        --  Select switch property
 *                          AIR_SWC_PROPERTY_ENABLE_MAC_AUTO_FLUSH
 *                          AIR_SWC_PROPERTY_ENABLE_L1_RATE_CTRL
 *                          AIR_SWC_PROPERTY_ACL_RATE_CTRL_MGMT_FRAME_INCLUDE
 *                          AIR_SWC_PROPERTY_STORM_CTRL_MGMT_FRAME_INCLUDE
 *      param0          --  1: Enable 0: Disable
 *      param1          --  Reserved
 *
 * OUTPUT:
 *      None
 *
 * RETURN:
 *      AIR_E_OK
 *      AIR_E_BAD_PARAMETER
 *
 * NOTES:
 *      None
 */
AIR_ERROR_NO_T
hal_pearl_swc_setProperty(
    const UI32_T                unit,
    const AIR_SWC_PROPERTY_T    property,
    const UI32_T                param0,
    const UI32_T                param1)
{
    UI32_T u32dat = 0, type = 0 ,enable = 0;

    switch(property)
    {
        case AIR_SWC_PROPERTY_ENABLE_MAC_AUTO_FLUSH:
            aml_readReg(unit, AAC, &u32dat, sizeof(u32dat));
            DIAG_PRINT(HAL_DBG_INFO,"[Dbg]: read AAC 0x%x = 0x%x\n", AAC, u32dat);
            _HAL_PEARL_SWC_SET_VALUE(u32dat, param0, AAC_AUTO_FLUSH_OFFSET, AAC_AUTO_FLUSH_LENGTH);
            DIAG_PRINT(HAL_DBG_INFO,"[Dbg]: write AAC 0x%x = 0x%x\n", AAC, u32dat);
            aml_writeReg(unit, AAC, &u32dat, sizeof(u32dat));
            break;
        case AIR_SWC_PROPERTY_ENABLE_L1_RATE_CTRL:
            type = (1 == param0) ? L1_RATE_IPG_BYTE_CNT : L2_RATE_IPG_BYTE_CNT;
            /* set egress rate control */
            aml_readReg(unit, GERLCR, &u32dat, sizeof(u32dat));
            DIAG_PRINT(HAL_DBG_INFO,"[Dbg]: read GERLCR 0x%x = 0x%x\n", GERLCR, u32dat);
            _HAL_PEARL_SWC_SET_VALUE(u32dat, type, REG_IPG_BYTE_OFFT, REG_IPG_BYTE_LENG);
            DIAG_PRINT(HAL_DBG_INFO,"[Dbg]: write GERLCR 0x%x = 0x%x\n", GERLCR, u32dat);
            aml_writeReg(unit, GERLCR, &u32dat, sizeof(u32dat));

            /* set ingress rate control */
            aml_readReg(unit, GIRLCR, &u32dat, sizeof(u32dat));
            DIAG_PRINT(HAL_DBG_INFO,"[Dbg]: read GIRLCR 0x%x = 0x%x\n", GIRLCR, u32dat);
            _HAL_PEARL_SWC_SET_VALUE(u32dat, type, REG_IPG_BYTE_OFFT, REG_IPG_BYTE_LENG);
            DIAG_PRINT(HAL_DBG_INFO,"[Dbg]: write GIRLCR 0x%x = 0x%x\n", GIRLCR, u32dat);
            aml_writeReg(unit, GIRLCR, &u32dat, sizeof(u32dat));

            /* set compensation byte number include/exclude L1 size */
            aml_readReg(unit, AGC, &u32dat, sizeof(u32dat));
            DIAG_PRINT(HAL_DBG_INFO,"[Dbg]: read AGC 0x%x = 0x%x\n", AGC, u32dat);
            _HAL_PEARL_SWC_SET_VALUE(u32dat, type, AGC_COMP_BNUM_OFFT, AGC_COMP_BNUM_LENG);
            DIAG_PRINT(HAL_DBG_INFO,"[Dbg]: write AGC 0x%x = 0x%x\n", AGC, u32dat);
            aml_writeReg(unit, AGC, &u32dat, sizeof(u32dat));

            break;
        case AIR_SWC_PROPERTY_ACL_RATE_CTRL_MGMT_FRAME_INCLUDE:
            enable = (1 == param0) ? 0 : 1;
            aml_readReg(unit, AGC, &u32dat, sizeof(u32dat));
            DIAG_PRINT(HAL_DBG_INFO,"[Dbg]: read AGC 0x%x = 0x%x\n", AGC, u32dat);
            _HAL_PEARL_SWC_SET_VALUE(u32dat, enable, AGC_ACLRATE_EXC_MG_OFFT, AGC_ACLRATE_EXC_MG_LENG);
            DIAG_PRINT(HAL_DBG_INFO,"[Dbg]: write AGC 0x%x = 0x%x\n", AGC, u32dat);
            aml_writeReg(unit, AGC, &u32dat, sizeof(u32dat));
            break;
        case AIR_SWC_PROPERTY_STORM_CTRL_MGMT_FRAME_INCLUDE:
            enable = (1 == param0) ? 0 : 1;
            aml_readReg(unit, AGC, &u32dat, sizeof(u32dat));
            DIAG_PRINT(HAL_DBG_INFO,"[Dbg]: read AGC 0x%x = 0x%x\n", AGC, u32dat);
            _HAL_PEARL_SWC_SET_VALUE(u32dat, enable, AGC_BCSTRM_EXC_MG_OFFT, AGC_BCSTRM_EXC_MG_LENG);
            DIAG_PRINT(HAL_DBG_INFO,"[Dbg]: write AGC 0x%x = 0x%x\n", AGC, u32dat);
            aml_writeReg(unit, AGC, &u32dat, sizeof(u32dat));
            break;
        default:
            break;
    }

    return AIR_E_OK;
}

/* FUNCTION NAME: hal_pearl_swc_getProperty
 * PURPOSE:
 *      Get switch property.
 *
 * INPUT:
 *      unit            --  Select device ID
 *      property        --  Select switch property
 *                          AIR_SWC_PROPERTY_ENABLE_MAC_AUTO_FLUSH
 *                          AIR_SWC_PROPERTY_ENABLE_L1_RATE_CTRL
 *                          AIR_SWC_PROPERTY_ACL_RATE_CTRL_MGMT_FRAME_INCLUDE
 *                          AIR_SWC_PROPERTY_STORM_CTRL_MGMT_FRAME_INCLUDE
 *
 * OUTPUT:
 *      ptr_param0      --  1: Enable 0: Disable
 *      ptr_param1      --  Reserved
 *
 * RETURN:
 *      AIR_E_OK
 *      AIR_E_BAD_PARAMETER
 *
 * NOTES:
 *      None
 */
AIR_ERROR_NO_T
hal_pearl_swc_getProperty(
    const UI32_T                unit,
    const AIR_SWC_PROPERTY_T    property,
    UI32_T                      *ptr_param0,
    UI32_T                      *ptr_param1)
{
    UI32_T u32dat = 0, type = 0, enable = 0;

    switch(property)
    {
        case AIR_SWC_PROPERTY_ENABLE_MAC_AUTO_FLUSH:
            aml_readReg(unit, AAC, &u32dat, sizeof(u32dat));
            DIAG_PRINT(HAL_DBG_INFO,"[Dbg]: read AAC 0x%x = 0x%x\n", AAC, u32dat);
            *ptr_param0 = BITS_OFF_R(u32dat, AAC_AUTO_FLUSH_OFFSET, AAC_AUTO_FLUSH_LENGTH);
            break;
        case AIR_SWC_PROPERTY_ENABLE_L1_RATE_CTRL:
            /* get egress rate control */
            aml_readReg(unit, GERLCR, &u32dat, sizeof(u32dat));
            DIAG_PRINT(HAL_DBG_INFO,"[Dbg]: read GERLCR 0x%x = 0x%x\n", GERLCR, u32dat);
            type = BITS_OFF_R(u32dat, REG_IPG_BYTE_OFFT, REG_IPG_BYTE_LENG);
            *ptr_param0 = (L1_RATE_IPG_BYTE_CNT == type) ? 1 : 0;
            break;
        case AIR_SWC_PROPERTY_ACL_RATE_CTRL_MGMT_FRAME_INCLUDE:
            aml_readReg(unit, AGC, &u32dat, sizeof(u32dat));
            DIAG_PRINT(HAL_DBG_INFO,"[Dbg]: read AGC 0x%x = 0x%x\n", AGC, u32dat);
            enable = BITS_OFF_R(u32dat, AGC_ACLRATE_EXC_MG_OFFT, AGC_ACLRATE_EXC_MG_LENG);
            *ptr_param0 = (0 == enable) ? 1 : 0;
            break;
        case AIR_SWC_PROPERTY_STORM_CTRL_MGMT_FRAME_INCLUDE:
            aml_readReg(unit, AGC, &u32dat, sizeof(u32dat));
            DIAG_PRINT(HAL_DBG_INFO,"[Dbg]: read AGC 0x%x = 0x%x\n", AGC, u32dat);
            enable = BITS_OFF_R(u32dat, AGC_BCSTRM_EXC_MG_OFFT, AGC_BCSTRM_EXC_MG_LENG);
            *ptr_param0 = (0 == enable) ? 1 : 0;
            break;
        default:
            break;
    }

    return AIR_E_OK;
}

/* FUNCTION NAME: hal_pearl_swc_getGlobalFreePages
 * PURPOSE:
 *      Get the free page link counter
 *
 * INPUT:
 *      unit            --  Select device ID
 *
 * OUTPUT:
 *      ptr_fp_cnt      --  Free page counter
 *      ptr_min_fp_cnt  --  Minimal Free page counter
 *
 * RETURN:
 *      AIR_E_OK
 *      AIR_E_BAD_PARAMETER
 *      AIR_E_OTHERS
 *
 * NOTES:
 *      None
 */
AIR_ERROR_NO_T
hal_pearl_swc_getGlobalFreePages(
    const UI32_T    unit,
    UI32_T          *ptr_fp_cnt,
    UI32_T          *ptr_min_fp_cnt)
{
    AIR_ERROR_NO_T rc = AIR_E_OK;
    UI32_T u32dat;

    /* Parameters checking */
    HAL_CHECK_UNIT(unit);
    HAL_CHECK_PTR(ptr_fp_cnt);
    HAL_CHECK_PTR(ptr_min_fp_cnt);

    /* Get Packet/Page counter probe status */
    rc = aml_readReg(unit, FPLC, &u32dat, sizeof(UI32_T));
    if (AIR_E_OK != rc)
    {
        DIAG_PRINT(HAL_DBG_ERR, "[Dbg]: read FPLC failed(%d)\n", rc);
        return rc;
    }

    *ptr_fp_cnt = BITS_OFF_R(u32dat, FPLC_FREE_PL_CNT_OFFT, FPLC_FREE_PL_CNT_LENG);
    *ptr_min_fp_cnt = BITS_OFF_R(u32dat, FPLC_MIN_FREE_PL_CNT_OFFT, FPLC_MIN_FREE_PL_CNT_LENG);
    return rc;
}

/* FUNCTION NAME: hal_pearl_swc_getPortAllocatedPages
 * PURPOSE:
 *      Get the allocated free page counter in RxCtrl of specific port
 *
 * INPUT:
 *      unit            --  Select device ID
 *      port            --  Select port number
 *
 * OUTPUT:
 *      ptr_fp_cnt      --  Free page counter
 *
 * RETURN:
 *      AIR_E_OK
 *      AIR_E_BAD_PARAMETER
 *      AIR_E_OTHERS
 *
 * NOTES:
 *      None
 */
AIR_ERROR_NO_T
hal_pearl_swc_getPortAllocatedPages(
    const UI32_T    unit,
    const UI32_T    port,
    UI32_T          *ptr_fp_cnt)
{
    AIR_ERROR_NO_T rc = AIR_E_OK;
    UI32_T mac_port;
    UI32_T u32dat;

    /* Parameters checking */
    HAL_CHECK_UNIT(unit);
    HAL_CHECK_PORT(unit, port);
    HAL_CHECK_PTR(ptr_fp_cnt);

    /* Change to MAC port */
    HAL_AIR_PORT_TO_MAC_PORT(unit, port, mac_port);

    /* Get Packet/Page counter probe status */
    rc = aml_readReg(unit, FPC_RXCTRL(mac_port), &u32dat, sizeof(UI32_T));
    if (AIR_E_OK != rc)
    {
        DIAG_PRINT(HAL_DBG_ERR, "[Dbg]: read FPC_RXCTRL(0x%08X) failed(%d)\n", FPC_RXCTRL(mac_port), rc);
        return rc;
    }

    *ptr_fp_cnt = BITS_OFF_R(u32dat, FPC_RXCTRL_FP_CNT_OFFT, FPC_RXCTRL_FP_CNT_LENG);
    return rc;
}

/* FUNCTION NAME: hal_pearl_swc_getPortUsedPages
 * PURPOSE:
 *      Get the used resource counter of specific port/queue
 *
 * INPUT:
 *      unit            --  Select device ID
 *      port            --  Select port number
 *      queue           --  Select queue index
 *      mode            --  0: Page counter
 *                          1: Packet counter
 * OUTPUT:
 *      ptr_cnt         --  Used resource counter
 *
 * RETURN:
 *      AIR_E_OK
 *      AIR_E_BAD_PARAMETER
 *      AIR_E_OTHERS
 *
 * NOTES:
 *      None
 */

AIR_ERROR_NO_T
hal_pearl_swc_getPortUsedPages(
    const UI32_T    unit,
    const UI32_T    port,
    const UI32_T    queue,
    const UI32_T    mode,
    UI32_T          *ptr_cnt)
{
    AIR_ERROR_NO_T rc = AIR_E_OK;
    UI32_T mac_port;
    UI32_T u32dat;

    /* Parameters checking */
    HAL_CHECK_UNIT(unit);
    HAL_CHECK_PORT(unit, port);
    HAL_CHECK_ENUM_RANGE(queue, AIR_QOS_QUEUE_MAX_NUM);
    HAL_CHECK_ENUM_RANGE(mode, 2);
    HAL_CHECK_PTR(ptr_cnt);

    /* Change to MAC port */
    HAL_AIR_PORT_TO_MAC_PORT(unit, port, mac_port);

    /* Set Packet/Page counter selector */
    u32dat = (1 == mode)?BIT(PCPCR_PACKET_SEL_OFFT):0;
    u32dat |= BITS_OFF_L(mac_port, PCPCR_PORT_SEL_OFFT, PCPCR_PORT_SEL_LENG);
    u32dat |= BITS_OFF_L(queue, PCPCR_QUEUE_SEL_OFFT, PCPCR_QUEUE_SEL_LENG);

    rc = aml_writeReg(unit, PCPCR, &u32dat, sizeof(UI32_T));
    if (AIR_E_OK != rc)
    {
        DIAG_PRINT(HAL_DBG_ERR, "[Dbg]: write PCPCR=0x%x failed(%d)\n", u32dat, rc);
        return rc;
    }

    /* Get Packet/Page counter probe status */
    rc = aml_readReg(unit, PCPSR, &u32dat, sizeof(UI32_T));
    if (AIR_E_OK != rc)
    {
        DIAG_PRINT(HAL_DBG_ERR, "[Dbg]: read PCPSR failed(%d)\n", rc);
        return rc;
    }

    *ptr_cnt = BITS_OFF_R(u32dat, PCPSR_COUNTER_OFFT, PCPSR_COUNTER_LENG);
    return rc;
}

