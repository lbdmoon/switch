/*******************************************************************************
*  Copyright Statement:
*  --------------------
*  This software is protected by Copyright and the information contained
*  herein is confidential. The software may not be copied and the information
*  contained herein may not be used or disclosed except with the written
*  permission of Airoha Technology Corp. (C) 2021
*
*  BY OPENING THIS FILE, BUYER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
*  THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("AIROHA SOFTWARE")
*  RECEIVED FROM AIROHA AND/OR ITS REPRESENTATIVES ARE PROVIDED TO BUYER ON
*  AN "AS-IS" BASIS ONLY. AIROHA EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
*  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
*  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
*  NEITHER DOES AIROHA PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
*  SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
*  SUPPLIED WITH THE AIROHA SOFTWARE, AND BUYER AGREES TO LOOK ONLY TO SUCH
*  THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. AIROHA SHALL ALSO
*  NOT BE RESPONSIBLE FOR ANY AIROHA SOFTWARE RELEASES MADE TO BUYER'S
*  SPECIFICATION OR TO CONFORM TO A PARTICULAR STANDARD OR OPEN FORUM.
*
*  BUYER'S SOLE AND EXCLUSIVE REMEDY AND AIROHA'S ENTIRE AND CUMULATIVE
*  LIABILITY WITH RESPECT TO THE AIROHA SOFTWARE RELEASED HEREUNDER WILL BE,
*  AT AIROHA'S OPTION, TO REVISE OR REPLACE THE AIROHA SOFTWARE AT ISSUE,
*  OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY BUYER TO
*  AIROHA FOR SUCH AIROHA SOFTWARE AT ISSUE.
*
*  THE TRANSACTION CONTEMPLATED HEREUNDER SHALL BE CONSTRUED IN ACCORDANCE
*  WITH THE LAWS OF THE STATE OF CALIFORNIA, USA, EXCLUDING ITS CONFLICT OF
*  LAWS PRINCIPLES.  ANY DISPUTES, CONTROVERSIES OR CLAIMS ARISING THEREOF AND
*  RELATED THERETO SHALL BE SETTLED BY ARBITRATION IN SAN FRANCISCO, CA, UNDER
*  THE RULES OF THE INTERNATIONAL CHAMBER OF COMMERCE (ICC).
*
*******************************************************************************/

/* FILE NAME:  hal_pearl_sif.c
 * PURPOSE:
 *  Implement SIF module HAL function.
 *
 * NOTES:
 *
 */
#include <aml/aml.h>
#include <hal/common/hal.h>
#include <api/diag.h>
#include <hal/switch/pearl/hal_pearl_sif.h>
#include <hal/common/hal_cfg.h>
#include <air_cfg.h>
#include <air_sif.h>
#include <air_error.h>

#if 1		/* wy,debug */
#include <stdio.h>
#include <stdint.h>
uint32_t time_current_tick(void);
#endif

/* NAMING CONSTANT DECLARATIONS
*/
#define HAL_PEARL_SIF_PIN_MUX                   (0x1000006cUL)

#define HAL_PEARL_SIF_CHANNEL_COUNT             (1)

#define HAL_PEARL_SIFM_BASE                     (0x10008000UL)
#define HAL_PEARL_SIFM_CTRL(base)               ((base) + 0x40U)
#define HAL_PEARL_SIFM_MODE(base)               ((base) + 0x44U)
#define HAL_PEARL_SIFM_DATA0(base)              ((base) + 0x50U)
#define HAL_PEARL_SIFM_DATA1(base)              ((base) + 0x54U)

#define HAL_PEARL_SIFM_OD_BIT                   (31)
#define HAL_PEARL_SIFM_CLK_BIT                  (16)
#define HAL_PEARL_SIFM_SCL_BIT                  (2)
#define HAL_PEARL_SIFM_SDA_BIT                  (3)
#define HAL_PEARL_SIF_EN_BIT                    (1)

#define HAL_PEARL_SIFM_ACK_BIT                  (16)
#define HAL_PEARL_SIFM_PAGE_BIT                 (8)
#define HAL_PEARL_SIFM_MODE_BIT                 (4)
#define HAL_PEARL_SIFM_EN_BIT                   (0)

#define HAL_PEARL_SIFM_START                    (1)
#define HAL_PEARL_SIFM_WRITE                    (2)
#define HAL_PEARL_SIFM_STOP                     (3)
#define HAL_PEARL_SIFM_READ_F                   (4)
#define HAL_PEARL_SIFM_READ                     (5)

#define HAL_PEARL_SIFM_DATA_CNT                 (8)

/* sif message flags */
#define SIF_MSG_WRITE                           (0x00)
#define SIF_MSG_READ                            (0x01)
#define SIF_MSG_NSTART                          (0x02)
#define SIF_MSG_REVERSE                         (0x04)

#define mask(n)                                 (~(~0UL << (n)))

/* MACRO FUNCTION DECLARATIONS
*/
/* This semaphore cannot be removed, like mdio module */
#define HAL_PEARL_SIF_SEMA(unit)                (*_ptr_hal_pearl_sif_sema[unit])

#if defined(I2C_SIF_NON_PREEMPTIBLE) && I2C_SIF_NON_PREEMPTIBLE
#define HAL_PEARL_SIF_TAKE_SEMA(unit)                                                         \
({                                                                                            \
    AIR_ERROR_NO_T __rc = AIR_E_OK;                                                           \
    if(os_status_critical_disabled())                                                         \
    {                                                                                         \
    __rc = HAL_COMMON_LOCK_RESOURCE(&(HAL_PEARL_SIF_SEMA(unit)), AIR_SEMAPHORE_WAIT_FOREVER); \
    if (AIR_E_OK != __rc)                                                                     \
    {                                                                                         \
        DIAG_PRINT(HAL_DBG_ERR, "u = %u, take semaphore failed, rc = %d\n", unit, __rc);      \
        return __rc;                                                                          \
    }                                                                                         \
    }                                                                                         \
})

#define HAL_PEARL_SIF_GIVE_SEMA(unit)                                                         \
({                                                                                            \
    AIR_ERROR_NO_T __rc = AIR_E_OK;                                                           \
    if(os_status_critical_disabled())                                                         \
    {                                                                                         \
    __rc = HAL_COMMON_FREE_RESOURCE(&(HAL_PEARL_SIF_SEMA(unit)));                             \
    if (AIR_E_OK != __rc)                                                                     \
    {                                                                                         \
        DIAG_PRINT(HAL_DBG_ERR, "u = %u, take semaphore failed, rc = %d\n", unit, __rc);      \
        return __rc;                                                                          \
    }                                                                                         \
    }                                                                                         \
})
#else
#define HAL_PEARL_SIF_TAKE_SEMA(unit)                                                         \
({                                                                                            \
    AIR_ERROR_NO_T __rc = AIR_E_OK;                                                           \
    __rc = HAL_COMMON_LOCK_RESOURCE(&(HAL_PEARL_SIF_SEMA(unit)), AIR_SEMAPHORE_WAIT_FOREVER); \
    if (AIR_E_OK != __rc)                                                                     \
    {                                                                                         \
        DIAG_PRINT(HAL_DBG_ERR, "u = %u, take semaphore failed, rc = %d\n", unit, __rc);      \
        return __rc;                                                                          \
    }                                                                                         \
})

#define HAL_PEARL_SIF_GIVE_SEMA(unit)                                                         \
({                                                                                            \
    AIR_ERROR_NO_T __rc = AIR_E_OK;                                                           \
    __rc = HAL_COMMON_FREE_RESOURCE(&(HAL_PEARL_SIF_SEMA(unit)));                             \
    if (AIR_E_OK != __rc)                                                                     \
    {                                                                                         \
        DIAG_PRINT(HAL_DBG_ERR, "u = %u, take semaphore failed, rc = %d\n", unit, __rc);      \
        return __rc;                                                                          \
    }                                                                                         \
})
#endif

#define HAL_PEARL_SIF_CHECK_CHANNEL(__channel__)                                              \
do{                                                                                           \
    if((__channel__) > HAL_PEARL_SIF_CHANNEL_COUNT - 1)                                       \
    {                                                                                         \
        return AIR_E_BAD_PARAMETER;                                                           \
    }                                                                                         \
} while (0)

#define _HAL_PEARL_SIF_SET_REG(__VAL__, __DATA__, __MASK__)                                   \
    (((__VAL__) & (~(__MASK__))) | ((__DATA__) & (__MASK__)))

/* DATA TYPE DECLARATIONS
*/
typedef struct HAL_PEARL_SIF_MSG_S
{
    UI8_T    slave_id;
    UI8_T    flag;
    UI16_T   len;
    UI8_T   *ptr_buf;
} HAL_PEARL_SIF_MSG_T;

/* GLOBAL VARIABLE DECLARATIONS
*/

/* STATIC VARIABLE DECLARATIONS
 */
DIAG_SET_MODULE_INFO(AIR_MODULE_SIF, "hal_pearl_sif.c");

/* Used for semaphore */
static AIR_SEMAPHORE_ID_T  *_ptr_hal_pearl_sif_sema[AIR_CFG_MAXIMUM_CHIPS_PER_SYSTEM];

/* LOCAL SUBPROGRAM DECLARATIONS
 */
static UI32_T
_hal_pearl_sif_getAddr(
    const UI32_T    unit,
    const UI32_T    channel
)
{
    return HAL_PEARL_SIFM_BASE;
}

static AIR_ERROR_NO_T
_hal_pearl_sif_writeMask(
    const UI32_T    unit,
    const UI32_T    addr,
    const UI32_T    data,
    const UI32_T    mask
)
{
    AIR_ERROR_NO_T rc = AIR_E_OK;
    UI32_T         val = 0;
    rc = aml_readReg(unit, addr, &val, sizeof(UI32_T));
    if(AIR_E_OK == rc)
    {
        val = _HAL_PEARL_SIF_SET_REG(val, data, mask);
        rc = aml_writeReg(unit, addr, &val, sizeof(UI32_T));
    }
    return rc;
}

static AIR_ERROR_NO_T _hal_pearl_sif_configSpeed(
    const UI32_T    unit,
    const UI32_T    channel,
    const UI32_T    clk_spd
)
{
    AIR_ERROR_NO_T rc = AIR_E_OK;

    UI32_T data = (clk_spd << HAL_PEARL_SIFM_CLK_BIT) |
                  (mask(1) << HAL_PEARL_SIF_EN_BIT);
    UI32_T mask = (mask(12) << HAL_PEARL_SIFM_CLK_BIT) |
                  (mask(1) << HAL_PEARL_SIF_EN_BIT);
    UI32_T base = _hal_pearl_sif_getAddr(unit, channel);
    UI32_T addr = HAL_PEARL_SIFM_CTRL(base);

    rc = _hal_pearl_sif_writeMask(unit, addr, data, mask);

    return rc;
}

static void
_hal_pearl_sif_reverseMsg(
    const UI32_T           unit,
    HAL_PEARL_SIF_MSG_T   *ptr_msg
)
{
    I32_T start = 0, end = ptr_msg->len - 1;
    while(end > start)
    {
        ptr_msg->ptr_buf[start] ^= ptr_msg->ptr_buf[end];
        ptr_msg->ptr_buf[end] ^= ptr_msg->ptr_buf[start];
        ptr_msg->ptr_buf[start] ^= ptr_msg->ptr_buf[end];

        start++, end--;
    }
}

static BOOL_T
_hal_pearl_sif_checkBus(
    const UI32_T    unit,
    const UI32_T    channel
)
{
    AIR_ERROR_NO_T rc = AIR_E_OK;

    UI32_T base = _hal_pearl_sif_getAddr(unit, channel);
    UI32_T addr = HAL_PEARL_SIFM_CTRL(base);
    UI32_T val  = 0;

    rc = aml_readReg(unit, addr, &val, sizeof(UI32_T));
    if(rc != AIR_E_OK)
    {
        return FALSE;
    }

    return !((val & (1 << HAL_PEARL_SIFM_SCL_BIT)) && (val & (1 << HAL_PEARL_SIFM_SDA_BIT)));
}

static AIR_ERROR_NO_T
_hal_pearl_sif_putMode(
    const UI32_T    unit,
    const UI32_T    channel,
    const UI32_T    op,
    const UI32_T    len
)
{
    AIR_ERROR_NO_T rc = AIR_E_OK;
    UI32_T data = len << HAL_PEARL_SIFM_PAGE_BIT |
                  op << HAL_PEARL_SIFM_MODE_BIT |
                  mask(1) << HAL_PEARL_SIFM_EN_BIT;

    UI32_T mask = mask(3) << HAL_PEARL_SIFM_PAGE_BIT |
                  mask(3) << HAL_PEARL_SIFM_MODE_BIT |
                  mask(1) << HAL_PEARL_SIFM_EN_BIT;

    UI32_T base = _hal_pearl_sif_getAddr(unit, channel);
    UI32_T addr = HAL_PEARL_SIFM_MODE(base);

#if 1		/* wy,debug */
	UI32_T ct = 0;
	uint32_t tick_start = time_current_tick();
#endif

    rc = _hal_pearl_sif_writeMask(unit, addr, data, mask);
    if (AIR_E_OK != rc)
    {
        return rc;
    }

    do {
        rc = aml_readReg(unit, addr, &data, sizeof(UI32_T));

#if 1		/* wy,debug */
		ct++;
		if(!(ct % 1000))
		{
			printf("%s:%d:%s count %d,tick start %lu,tick end %lu,rc %d\n", 
				__FILE__, __LINE__, __FUNCTION__, ct, tick_start, time_current_tick(), rc);
		}

		if(ct >= 1000 * 1000)
		{
			printf("%s:%d:%s count %d,tick start %lu,tick end %lu,rc %d,quit\n", 
				__FILE__, __LINE__, __FUNCTION__, ct, tick_start, time_current_tick(), rc);
			break;
		}
#endif
    } while(data & mask(1));

#if 1		/* wy,debug */
	if(ct > 1000)
	{
		printf("%s:%d:%s count %d,tick start %lu,tick end %lu,rc %d,out loop\n", 
			__FILE__, __LINE__, __FUNCTION__, ct, tick_start, time_current_tick(), rc);
	}
#endif

    return AIR_E_OK;
}

#define _hal_pearl_sif_putStart(unit, channel)  \
        _hal_pearl_sif_putMode(unit, channel, HAL_PEARL_SIFM_START, 0)
#define _hal_pearl_sif_putWrite(unit, channel, len) \
        _hal_pearl_sif_putMode(unit, channel, HAL_PEARL_SIFM_WRITE, len)
#define _hal_pearl_sif_putStop(unit, channel) \
        _hal_pearl_sif_putMode(unit, channel, HAL_PEARL_SIFM_STOP, 0)
#define _hal_pearl_sif_putReadFinal(unit, channel, len) \
        _hal_pearl_sif_putMode(unit, channel, HAL_PEARL_SIFM_READ_F, len)
#define _hal_pearl_sif_putRead(unit, channel, len) \
        _hal_pearl_sif_putMode(unit, channel, HAL_PEARL_SIFM_READ, len)

static UI32_T
_hal_pearl_sif_checkAck(
    const UI32_T    unit,
    const UI32_T    channel,
    const UI32_T    expect
)
{
    AIR_ERROR_NO_T rc = AIR_E_OK;
    UI32_T         data = 0, ack = 0, result;
    UI32_T         base = _hal_pearl_sif_getAddr(unit, channel);

    rc = aml_readReg(unit, HAL_PEARL_SIFM_MODE(base), &data, sizeof(UI32_T));
    if(rc)
    {
        return 1;
    }

    DIAG_PRINT(HAL_DBG_INFO, "data = 0x%x\n", data);
    data &= (mask(8) << HAL_PEARL_SIFM_ACK_BIT);

    while(data) {
        data &= (data - 1);
        ack++;
    }

    result = !(ack == expect);

    if(result)
    {
        DIAG_PRINT(HAL_DBG_ERR, "Ack error, get ack %d, expect %d\n", ack, expect);
    }

    return result;
}

static AIR_ERROR_NO_T
_hal_pearl_sif_sendBytes(
    const UI32_T    unit,
    const UI32_T    channel,
    const UI32_T    len,
    UI8_T          *ptr_buf
)
{
    AIR_ERROR_NO_T rc = AIR_E_OK;

    UI32_T loop_count = (len / HAL_PEARL_SIFM_DATA_CNT), i = 0;
    UI32_T remain = len % HAL_PEARL_SIFM_DATA_CNT, j = 0;
    UI32_T data[2];
    UI32_T base = _hal_pearl_sif_getAddr(unit, channel);

    for(; i < loop_count; i++, j += HAL_PEARL_SIFM_DATA_CNT) {
        osal_memcpy(data, ptr_buf + j, HAL_PEARL_SIFM_DATA_CNT);
        aml_writeReg(unit, HAL_PEARL_SIFM_DATA0(base), &data[0], sizeof(UI32_T));
        aml_writeReg(unit, HAL_PEARL_SIFM_DATA1(base), &data[1], sizeof(UI32_T));

        rc = _hal_pearl_sif_putWrite(unit, channel, HAL_PEARL_SIFM_DATA_CNT - 1);
        data[0] = 0, data[1] = 0;

        if(_hal_pearl_sif_checkAck(unit, channel, HAL_PEARL_SIFM_DATA_CNT))
        {
            return AIR_E_OTHERS;
        }
    }

    if(remain)
    {
        osal_memcpy(data, ptr_buf + j, remain);
        aml_writeReg(unit, HAL_PEARL_SIFM_DATA0(base), &data[0], sizeof(UI32_T));
        aml_writeReg(unit, HAL_PEARL_SIFM_DATA1(base), &data[1], sizeof(UI32_T));

        rc = _hal_pearl_sif_putWrite(unit, channel, remain - 1);

        if(_hal_pearl_sif_checkAck(unit, channel, remain))
        {
            return AIR_E_OTHERS;
        }
    }

    return rc;
}

#define _hal_pearl_sif_sendByte(unit, channel, ptr_buf) \
        _hal_pearl_sif_sendBytes(unit, channel, 1, ptr_buf)

#define _hal_pearl_sif_sendAddr(unit, channel, ptr_buf) \
        _hal_pearl_sif_sendByte(unit, channel, ptr_buf)

static AIR_ERROR_NO_T
_hal_pearl_sif_getBytes(
    const UI32_T    unit,
    const UI32_T    channel,
    const UI32_T    len,
    UI8_T          *ptr_buf
)
{
    AIR_ERROR_NO_T  rc = AIR_E_OK;

    UI32_T loop_count = (len / HAL_PEARL_SIFM_DATA_CNT), i = 0;
    UI32_T remain = len % HAL_PEARL_SIFM_DATA_CNT, j = 0;
    UI32_T data[2];

    for(; i < loop_count; i++, j += HAL_PEARL_SIFM_DATA_CNT) {
        rc = _hal_pearl_sif_putRead(unit, channel, HAL_PEARL_SIFM_DATA_CNT - 1);
        aml_readReg(unit, HAL_PEARL_SIFM_DATA0(HAL_PEARL_SIFM_BASE), &data[0], sizeof(UI32_T));
        aml_readReg(unit, HAL_PEARL_SIFM_DATA1(HAL_PEARL_SIFM_BASE), &data[1], sizeof(UI32_T));
        osal_memcpy(ptr_buf + j, data, HAL_PEARL_SIFM_DATA_CNT);
    }

    if(remain)
    {
        rc = _hal_pearl_sif_putReadFinal(unit, channel, remain - 1);
        aml_readReg(unit, HAL_PEARL_SIFM_DATA0(HAL_PEARL_SIFM_BASE), &data[0], sizeof(UI32_T));
        aml_readReg(unit, HAL_PEARL_SIFM_DATA1(HAL_PEARL_SIFM_BASE), &data[1], sizeof(UI32_T));
        osal_memcpy(ptr_buf + j, data, remain);
    }

    return rc;
}

static AIR_ERROR_NO_T
_hal_pearl_sif_xfer(
    const UI32_T            unit,
    const UI32_T            channel,
    HAL_PEARL_SIF_MSG_T    *ptr_sif_msg,
    const UI32_T            count
)
{
    AIR_ERROR_NO_T        rc = AIR_E_OK;
    HAL_PEARL_SIF_MSG_T  *ptr_msg;
    UI32_T                i = 0;
    UI8_T                 slave_id = 0;

    if(_hal_pearl_sif_checkBus(unit, channel))
    {
        rc = _hal_pearl_sif_putStop(unit, channel);
        if(rc != AIR_E_OK)
        {
            return rc;
        }

        if(_hal_pearl_sif_checkBus(unit, channel))
        {
            DIAG_PRINT(HAL_DBG_ERR, "Bus busy\n");
            return AIR_E_OP_INCOMPLETE;
        }
    }

    for(; i < count && rc == AIR_E_OK; i++)
    {
        ptr_msg = &ptr_sif_msg[i];

        if(!(ptr_msg->flag & SIF_MSG_NSTART))
        {
            rc = _hal_pearl_sif_putStart(unit, channel);
            if(rc != AIR_E_OK)
            {
                break;
            }

            slave_id = (ptr_msg->slave_id << 1) | (ptr_msg->flag & SIF_MSG_READ);
            rc = _hal_pearl_sif_sendAddr(unit, channel, &slave_id);
            if(rc != AIR_E_OK)
            {
                break;
            }
        }

        if(ptr_msg->flag & SIF_MSG_READ)
        {
            rc = _hal_pearl_sif_getBytes(unit, channel, ptr_msg->len, ptr_msg->ptr_buf);
            if(rc != AIR_E_OK)
            {
                DIAG_PRINT(HAL_DBG_ERR, "Get message error\n");
                break;
            }
        }
        else
        {
            if(ptr_msg->flag & SIF_MSG_REVERSE)
            {
                _hal_pearl_sif_reverseMsg(unit, ptr_msg);
            }

            rc = _hal_pearl_sif_sendBytes(unit, channel, ptr_msg->len, ptr_msg->ptr_buf);
            if(rc != AIR_E_OK)
            {
                DIAG_PRINT(HAL_DBG_ERR, "Send message error\n");
                break;
            }
        }
    }

    rc |= _hal_pearl_sif_putStop(unit, channel);

    return rc;
}

static AIR_ERROR_NO_T
_hal_pearl_sif_initRscr(
    const UI32_T            unit
)
{
    AIR_ERROR_NO_T      rc = AIR_E_OK;
    AIR_SEMAPHORE_ID_T *ptr_sif_sema_id = NULL;
    UI32_T              size = 0;

    size = sizeof(AIR_SEMAPHORE_ID_T);
    ptr_sif_sema_id = (AIR_SEMAPHORE_ID_T *) osal_alloc(size, air_module_getModuleName(AIR_MODULE_SIF));
    HAL_CHECK_PTR(ptr_sif_sema_id);
    osal_memset(ptr_sif_sema_id, 0, size);
    _ptr_hal_pearl_sif_sema[unit] = ptr_sif_sema_id;
    rc = osal_createSemaphore("SIF_SEMA", AIR_SEMAPHORE_BINARY,
                              ptr_sif_sema_id, air_module_getModuleName(AIR_MODULE_SIF));

    return rc;
}
static AIR_ERROR_NO_T
_hal_pearl_sif_deinitRscr(
    const UI32_T unit
)
{
    AIR_ERROR_NO_T      rc = AIR_E_OK;
    AIR_SEMAPHORE_ID_T  *ptr_sif_sema_id = NULL;

    ptr_sif_sema_id = _ptr_hal_pearl_sif_sema[unit];

    rc = osal_destroySemaphore(ptr_sif_sema_id);
    if (AIR_E_OK == rc)
    {
        osal_free(ptr_sif_sema_id);
        _ptr_hal_pearl_sif_sema[unit] = NULL;
    }
    return rc;
}

/* FUNCTION NAME:   hal_pearl_sif_init
 * PURPOSE:
 *      Initialize sif module.
 * INPUT:
 *      unit                 -- Device unit number
 * OUTPUT:
 *      None
 * RETURN:
 *      AIR_E_OK             -- Operation success.
 * NOTES:
 *      None
 */
AIR_ERROR_NO_T
hal_pearl_sif_init(
    const UI32_T unit
)
{
    AIR_ERROR_NO_T  rc;
    //AIR_CFG_VALUE_T sif_cfg;

    /* 0xc7 denotes for standard mode, 100kHz
     * 0X31 denotes for fast mode, 400kHz
     * 0x13 denotes for fast mode plus, 1MHz
     */
    const UI32_T sif_clk_spd[] = {0xc7, 0x31, 0x13};

    /* temporary used */
    //UI32_T addr = HAL_PEARL_SIF_PIN_MUX;
    //UI32_T data = 0x5;

    //rc = aml_writeReg(unit, addr, &data, 4);

    /* initialize the lock */
    rc = _hal_pearl_sif_initRscr(unit);
    if (AIR_E_OK != rc)
    {
        return rc;
    }

    /* initialize the customer channel */
    //osal_memset(&sif_cfg, 0, sizeof(AIR_CFG_VALUE_T));

    //sif_cfg.param0 = 0;
    //sif_cfg.param1 = 0;
    //sif_cfg.value = 0;
    //rc = hal_cfg_getValue(unit, AIR_CFG_TYPE_SIF_LOCAL_CLOCK, &sif_cfg);
    //if (AIR_E_OK != rc)
    //{
    //    return rc;
    //}
    rc = _hal_pearl_sif_configSpeed(unit, 0, sif_clk_spd[0]);

    return rc;
}

/* FUNCTION NAME:   hal_pearl_sif_deinit
 * PURPOSE:
 *      Deinitialize sif module
 * INPUT:
 *      unit                 -- Device unit number
 * OUTPUT:
 *      None
 * RETURN:
 *      AIR_E_OK             -- Operation success.
 * NOTES:
 *      None
 */
AIR_ERROR_NO_T
hal_pearl_sif_deinit(
    const UI32_T unit
)
{
    AIR_ERROR_NO_T rc;
    UI32_T reg_value;

    reg_value = 0;
    aml_writeReg(unit, HAL_PEARL_SIFM_CTRL(HAL_PEARL_SIFM_BASE), &reg_value, sizeof(reg_value));
    aml_writeReg(unit, HAL_PEARL_SIFM_MODE(HAL_PEARL_SIFM_BASE), &reg_value, sizeof(reg_value));
    aml_writeReg(unit, HAL_PEARL_SIFM_DATA0(HAL_PEARL_SIFM_BASE), &reg_value, sizeof(reg_value));
    aml_writeReg(unit, HAL_PEARL_SIFM_DATA1(HAL_PEARL_SIFM_BASE), &reg_value, sizeof(reg_value));

    /* initialize the lock */
    rc = _hal_pearl_sif_deinitRscr(unit);

    return rc;
}

/* FUNCTION NAME:   hal_pearl_sif_write
 * PURPOSE:
 *      This API is used to do the I2C write operation
 * INPUT:
 *      unit                 -- Device unit number
 *      ptr_sif_info         -- Pointer of sif information
 *                              AIR_SIF_INFO_T
 *      ptr_sif_param        -- Pointer of sif parameter
 *                              AIR_SIF_PARAM_T
 * OUTPUT:
 *      None
 * RETURN:
 *      AIR_E_OK             -- Operation success.
 *      AIR_E_OTHERS         -- Other errors.
 *      AIR_E_BAD_PARAMETER  -- Parameter is wrong.
 *      AIR_E_OP_INVALID     -- Operation is invalid.
 * NOTES:
 *      None
 */
AIR_ERROR_NO_T
hal_pearl_sif_write(
    const UI32_T            unit,
    const AIR_SIF_INFO_T   *ptr_sif_info,
    AIR_SIF_PARAM_T        *ptr_sif_param
)
{
    AIR_ERROR_NO_T        rc = AIR_E_OK;
    HAL_PEARL_SIF_MSG_T   sif_msgs[2];

    HAL_PEARL_SIF_CHECK_CHANNEL(ptr_sif_info->channel);

    sif_msgs[0].slave_id = ptr_sif_info->slave_id;
    sif_msgs[0].flag = SIF_MSG_WRITE | SIF_MSG_REVERSE;
    sif_msgs[0].len = ptr_sif_param->addr_len;
    sif_msgs[0].ptr_buf = (UI8_T *) &ptr_sif_param->addr;

    sif_msgs[1].slave_id = ptr_sif_info->slave_id;
    sif_msgs[1].flag = SIF_MSG_WRITE | SIF_MSG_NSTART;
    sif_msgs[1].len = ptr_sif_param->data_len;

    if(4 < ptr_sif_param->data_len)
    {
        sif_msgs[1].ptr_buf = (UI8_T *) ptr_sif_param->info.ptr_data;
    }
    else
    {
        sif_msgs[1].ptr_buf = (UI8_T *) &ptr_sif_param->info.data;
    }

    HAL_PEARL_SIF_TAKE_SEMA(unit);
    rc = _hal_pearl_sif_xfer(unit, ptr_sif_info->channel, sif_msgs, 2);
    HAL_PEARL_SIF_GIVE_SEMA(unit);

    return rc;
}


/* FUNCTION NAME:   hal_pearl_sif_read
 * PURPOSE:
 *      This API is used to do the I2C read operation
 * INPUT:
 *      unit                 -- Device unit number
 *      ptr_sif_info         -- Pointer of sif information
 *                              AIR_SIF_INFO_T
 *      ptr_sif_param        -- Pointer of sif parameter
 *                              AIR_SIF_PARAM_T
 * OUTPUT:
 *      ptr_sif_param        -- Pointer of sif parameter
 *                              AIR_SIF_PARAM_T
 * RETURN:
 *      AIR_E_OK             -- Operation success.
 *      AIR_E_OTHERS         -- Other errors.
 *      AIR_E_BAD_PARAMETER  -- Parameter is wrong.
 *      AIR_E_OP_INVALID     -- Operation is invalid.
 * NOTES:
 *      None
 */
AIR_ERROR_NO_T
hal_pearl_sif_read(
    const UI32_T            unit,
    const AIR_SIF_INFO_T   *ptr_sif_info,
    AIR_SIF_PARAM_T        *ptr_sif_param
)
{
    AIR_ERROR_NO_T        rc = AIR_E_OK;
    HAL_PEARL_SIF_MSG_T   sif_msgs[2];

    HAL_PEARL_SIF_CHECK_CHANNEL(ptr_sif_info->channel);

    sif_msgs[0].slave_id = ptr_sif_info->slave_id;
    sif_msgs[0].flag = SIF_MSG_WRITE | SIF_MSG_REVERSE;
    sif_msgs[0].len = ptr_sif_param->addr_len;
    sif_msgs[0].ptr_buf = (UI8_T *) &ptr_sif_param->addr;

    sif_msgs[1].slave_id = ptr_sif_info->slave_id;
    sif_msgs[1].flag = SIF_MSG_READ;
    sif_msgs[1].len = ptr_sif_param->data_len;

    if(4 < ptr_sif_param->data_len)
    {
        sif_msgs[1].ptr_buf = (UI8_T *) ptr_sif_param->info.ptr_data;
    }
    else
    {
        sif_msgs[1].ptr_buf = (UI8_T *) &ptr_sif_param->info.data;
    }

    HAL_PEARL_SIF_TAKE_SEMA(unit);
    if(0 == ptr_sif_param->addr_len)
    {
        rc = _hal_pearl_sif_xfer(unit, ptr_sif_info->channel, &sif_msgs[1], 1);
    }
    else
    {
        rc = _hal_pearl_sif_xfer(unit, ptr_sif_info->channel, sif_msgs, 2);
    }
    HAL_PEARL_SIF_GIVE_SEMA(unit);

    return rc;
}

/* FUNCTION NAME:   hal_pearl_sif_writeByRemote
 * PURPOSE:
 *      This API is used to do the I2C write operation
 *      by remote device
 * INPUT:
 *      unit                 -- Device unit number
 *      ptr_sif_info         -- Pointer of sif information
 *                              AIR_SIF_INFO_T
 *      ptr_sif_remote_info  -- Pointer of remote sif information
 *                              AIR_SIF_INFO_T
 *      ptr_sif_param        -- Pointer of sif parameter
 *                              AIR_SIF_PARAM_T
 * OUTPUT:
 *      None
 * RETURN:
 *      AIR_E_OK             -- Operation success.
 *      AIR_E_OTHERS         -- Other errors.
 *      AIR_E_BAD_PARAMETER  -- Parameter is wrong.
 *      AIR_E_OP_INVALID     -- Operation is invalid.
 * NOTES:
 *      None
 */
AIR_ERROR_NO_T
hal_pearl_sif_writeByRemote(
    const UI32_T            unit,
    const AIR_SIF_INFO_T    *ptr_sif_info,
    const AIR_SIF_INFO_T    *ptr_sif_remote_info,
    AIR_SIF_PARAM_T         *ptr_sif_param
)
{
    return AIR_E_NOT_SUPPORT;
}

/* FUNCTION NAME:   hal_pearl_sif_readByRemote
 * PURPOSE:
 *      This API is used to do the I2C read operation
 *      by remote device
 * INPUT:
 *      unit                 -- Device unit number
 *      ptr_sif_info         -- Pointer of sif information
 *                              AIR_SIF_INFO_T
 *      ptr_sif_remote_info  -- Pointer of remote sif information
 *                              AIR_SIF_INFO_T
 *      ptr_sif_param        -- Pointer of sif parameter
 *                              AIR_SIF_PARAM_T
 * OUTPUT:
 *      ptr_sif_param        -- Pointer of sif parameter
 *                              AIR_SIF_PARAM_T
 * RETURN:
 *      AIR_E_OK             -- Operation success.
 *      AIR_E_OTHERS         -- Other errors.
 *      AIR_E_BAD_PARAMETER  -- Parameter is wrong.
 *      AIR_E_OP_INVALID     -- Operation failed.
 * NOTES:
 *      None
 */
AIR_ERROR_NO_T
hal_pearl_sif_readByRemote(
    const UI32_T            unit,
    const AIR_SIF_INFO_T    *ptr_sif_info,
    const AIR_SIF_INFO_T    *ptr_sif_remote_info,
    AIR_SIF_PARAM_T         *ptr_sif_param
)
{
    return AIR_E_NOT_SUPPORT;
}
