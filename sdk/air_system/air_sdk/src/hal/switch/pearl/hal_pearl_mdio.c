/*******************************************************************************
*  Copyright Statement:
*  --------------------
*  This software is protected by Copyright and the information contained
*  herein is confidential. The software may not be copied and the information
*  contained herein may not be used or disclosed except with the written
*  permission of Airoha Technology Corp. (C) 2021
*
*  BY OPENING THIS FILE, BUYER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
*  THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("AIROHA SOFTWARE")
*  RECEIVED FROM AIROHA AND/OR ITS REPRESENTATIVES ARE PROVIDED TO BUYER ON
*  AN "AS-IS" BASIS ONLY. AIROHA EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
*  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
*  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
*  NEITHER DOES AIROHA PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
*  SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
*  SUPPLIED WITH THE AIROHA SOFTWARE, AND BUYER AGREES TO LOOK ONLY TO SUCH
*  THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. AIROHA SHALL ALSO
*  NOT BE RESPONSIBLE FOR ANY AIROHA SOFTWARE RELEASES MADE TO BUYER'S
*  SPECIFICATION OR TO CONFORM TO A PARTICULAR STANDARD OR OPEN FORUM.
*
*  BUYER'S SOLE AND EXCLUSIVE REMEDY AND AIROHA'S ENTIRE AND CUMULATIVE
*  LIABILITY WITH RESPECT TO THE AIROHA SOFTWARE RELEASED HEREUNDER WILL BE,
*  AT AIROHA'S OPTION, TO REVISE OR REPLACE THE AIROHA SOFTWARE AT ISSUE,
*  OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY BUYER TO
*  AIROHA FOR SUCH AIROHA SOFTWARE AT ISSUE.
*
*  THE TRANSACTION CONTEMPLATED HEREUNDER SHALL BE CONSTRUED IN ACCORDANCE
*  WITH THE LAWS OF THE STATE OF CALIFORNIA, USA, EXCLUDING ITS CONFLICT OF
*  LAWS PRINCIPLES.  ANY DISPUTES, CONTROVERSIES OR CLAIMS ARISING THEREOF AND
*  RELATED THERETO SHALL BE SETTLED BY ARBITRATION IN SAN FRANCISCO, CA, UNDER
*  THE RULES OF THE INTERNATIONAL CHAMBER OF COMMERCE (ICC).
*
*******************************************************************************/

/* FILE NAME:  hal_pearl_mdio.c
 * PURPOSE:
 *  Implement MDIO module HAL function.
 *
 * NOTES:
 *
 */

/* INCLUDE FILE DECLARTIONS
*/
#include <aml/aml.h>
#include <api/diag.h>
#include <hal/switch/pearl/hal_pearl_mdio.h>
#include <hal/common/hal_dbg.h>
#include <hal/common/hal_cfg.h>

/* NAMING CONSTANT DECLARATIONS
*/
#define WIDTH1_MSK      (0x1)
#define WIDTH2_MSK      (0x3)
#define WIDTH3_MSK      (0x7)
#define WIDTH4_MSK      (0xF)
#define WIDTH5_MSK      (0x1F)
#define WIDTH6_MSK      (0x3F)
#define WIDTH7_MSK      (0x7F)
#define WIDTH8_MSK      (0xFF)
#define WIDTH16_MSK     (0xFFFF)
    
#define PB_PHY_REG_BASE             (0xA0000000)
#define PB_PHY_ACCESS_BIT           (0x00800000)
#define PB_PHY_ADDR_OFS             (24)
#define PB_PHY_ADDR_MSK             (WIDTH5_MSK)
#define PB_PHY_ADDR_SEL(phyAddr)    ((phyAddr & PB_PHY_ADDR_MSK) << PB_PHY_ADDR_OFS)
#define PB_PHY_PAGE_OFS             (12)
#define PB_PHY_PAGE_MSK             (WIDTH4_MSK)
#define PB_PHY_PAGE_SEL(page)       ((page & PB_PHY_PAGE_MSK) << PB_PHY_PAGE_OFS)
#define PB_CL22_MAIN_PAGE           (0x00000000)
#define PB_PHY_CL22_REG_OFS         (4)
#define PB_PHY_CL22_REG_MSK         (WIDTH5_MSK)
#define PB_PHY_REG_SEL(reg)         ((reg & WIDTH5_MSK) << PB_PHY_CL22_REG_OFS)
    
#define PB_PHY_CL45_DEV_OFS         (18)
#define PB_PHY_CL45_DEV_MSK         (WIDTH5_MSK)
#define PB_PHY_CL45_DEV_SEL(dev)    ((dev & PB_PHY_CL45_DEV_MSK) << PB_PHY_CL45_DEV_OFS)
#define PB_PHY_CL45_REG_OFS         (2)
#define PB_PHY_CL45_REG_MSK         (WIDTH16_MSK)
#define PB_PHY_CL45_REG_SEL(reg)    ((reg & PB_PHY_CL45_REG_MSK) << PB_PHY_CL45_REG_OFS)

#define IAC_MAX_BUSY_TIME (1000)

/* MACRO FUNCTION DECLARATIONS
 */

/* DATA TYPE DECLARATIONS
*/

/* GLOBAL VARIABLE DECLARATIONS
*/
DIAG_SET_MODULE_INFO(AIR_MODULE_MDIO, "hal_pearl_mdio.c");

/* LOCAL SUBPROGRAM SPECIFICATIONS
*/
//TODO: move to aml later
extern unsigned short io_read16(unsigned int addr);
extern unsigned int io_read32(unsigned int addr);
extern void io_write16(unsigned int reg, unsigned short vlaue);
extern void io_write32(unsigned int addr, unsigned int vlaue);

static void
_cl22_write(
    const UI8_T phyAddr,
    const UI8_T pageSel,
    const UI8_T regSel,
    const UI16_T wdata)
{
    UI32_T pbusAddr = (PB_PHY_REG_BASE | PB_PHY_ACCESS_BIT
                    | PB_PHY_ADDR_SEL(phyAddr) | PB_CL22_MAIN_PAGE
                    | PB_PHY_PAGE_SEL(pageSel) | PB_PHY_REG_SEL(regSel));

    DIAG_PRINT(HAL_DBG_INFO, "phyAddr: 0x%x, regSel: 0x%x\n", phyAddr, regSel);
    DIAG_PRINT(HAL_DBG_INFO, "data: 0x%x\n", wdata);
    DIAG_PRINT(HAL_DBG_INFO, "pbusAddr: 0x%x\n", pbusAddr);

    io_write16(pbusAddr, wdata);
}

static UI16_T
_cl22_read(
    const UI8_T phyAddr,
    const UI8_T pageSel,
    const UI8_T regSel)
{
    UI32_T pbusAddr = (PB_PHY_REG_BASE | PB_PHY_ACCESS_BIT
                    | PB_PHY_ADDR_SEL(phyAddr) | PB_CL22_MAIN_PAGE
                    | PB_PHY_PAGE_SEL(pageSel) | PB_PHY_REG_SEL(regSel));

    DIAG_PRINT(HAL_DBG_INFO, "phyAddr: 0x%x, regSel: 0x%x\n", phyAddr, regSel);
    DIAG_PRINT(HAL_DBG_INFO, "pbusAddr: 0x%x\n", pbusAddr);

    return io_read16(pbusAddr);
}

static void
_cl45_write(
    const UI8_T phyAddr,
    const UI8_T devID,
    const UI16_T regSel,
    const UI16_T wdata)
{
    UI32_T pbusAddr = (PB_PHY_REG_BASE | PB_PHY_ACCESS_BIT
                    |    PB_PHY_ADDR_SEL(phyAddr) | PB_PHY_CL45_DEV_SEL(devID)
                    |    PB_PHY_CL45_REG_SEL(regSel));

    DIAG_PRINT(HAL_DBG_INFO, "phyAddr: 0x%x, devID:0x%x, regSel: 0x%x\n", phyAddr, devID, regSel);
    DIAG_PRINT(HAL_DBG_INFO, "data: 0x%x\n", wdata);
    DIAG_PRINT(HAL_DBG_INFO, "pbusAddr: 0x%x\n", pbusAddr);

    io_write16(pbusAddr, wdata);
}

static UI16_T 
_cl45_read(
    const UI8_T phyAddr,
    const UI8_T devID,
    const UI16_T regSel)
{
    UI32_T pbusAddr = (PB_PHY_REG_BASE | PB_PHY_ACCESS_BIT
                    | PB_PHY_ADDR_SEL(phyAddr) | PB_PHY_CL45_DEV_SEL(devID)
                    | PB_PHY_CL45_REG_SEL(regSel));

    DIAG_PRINT(HAL_DBG_INFO, "phyAddr: 0x%x, devID:0x%x, regSel: 0x%x\n", phyAddr, devID, regSel);
    DIAG_PRINT(HAL_DBG_INFO, "pbusAddr: 0x%x\n", pbusAddr);

    return io_read16(pbusAddr);
}

AIR_ERROR_NO_T
_hal_pearl_mdio_writeC22(
    const UI32_T    unit,
    const UI16_T    bus_id,
    const UI16_T    phy_addr,
    const UI16_T    reg_addr,
    const UI16_T    reg_data)
{
    AIR_ERROR_NO_T rv = AIR_E_OK;
    UI32_T wdata = 0;
    UI32_T checkBit = 0, busytime = 0;

    DIAG_PRINT(HAL_DBG_INFO, " write_c22 unit(%u) bus(0x%X) phy(0x%X) reg(0x%X) val(0x%X)\n",
            unit, bus_id, phy_addr, reg_addr, reg_data);
    wdata= SET_MDIO_PHY(phy_addr) | SET_MDIO_REG(reg_addr) | SET_MDIO_DATA(reg_data)\
                       | (SET_MDIO_CMD_CL22W) | (SET_MDIO_ST_CL22);
    io_write32(PHY_IAC, wdata);
    wdata |= SET_MDIO_ACS_START;
    io_write32(PHY_IAC, wdata);

    while(1)
    {
        checkBit = io_read32(PHY_IAC);
        if (!(checkBit & SET_MDIO_ACS_START))
        {
            break;
        }
        else if (busytime >= IAC_MAX_BUSY_TIME)
        {
            rv = AIR_E_TIMEOUT;
            break;
        }
        busytime++;
    }

    DIAG_PRINT(HAL_DBG_INFO, " (%u) Write BusId[%u] PhyAddr[0x%x] Reg[0x%X] : 0x%08X\n",
        unit, bus_id, phy_addr, reg_addr, reg_data);
    return rv;
}

AIR_ERROR_NO_T
_hal_pearl_mdio_readC22(
    const UI32_T    unit,
    const UI16_T    bus_id,
    const UI16_T    phy_addr,
    const UI16_T    reg_addr,
    UI16_T          *ptr_reg_data)
{
    AIR_ERROR_NO_T rv = AIR_E_OK;
    UI32_T wdata = 0;
    UI32_T checkBit = 0, busytime = 0;
    UI32_T data = 0;

    wdata = SET_MDIO_PHY(phy_addr) | SET_MDIO_REG(reg_addr) \
                       | SET_MDIO_CMD_CL22R | SET_MDIO_ST_CL22;
    io_write32(PHY_IAC, wdata);
    wdata |= SET_MDIO_ACS_START;
    io_write32(PHY_IAC, wdata);
    while(1)
    {
        checkBit = io_read32(PHY_IAC);
        if (!(checkBit & SET_MDIO_ACS_START))
        {
            break;
        }
        else if (busytime >= IAC_MAX_BUSY_TIME)
        {
            rv = AIR_E_TIMEOUT;
            break;
        }
        busytime++;
    }

    data = io_read32(PHY_IAD);
    *ptr_reg_data = GET_MDIO_DATA(data);
    DIAG_PRINT(HAL_DBG_INFO, " read_c22 unit(%u) bus(0x%X) phy(0x%X) reg(0x%X) val(0x%X)\n",
            unit, bus_id, phy_addr, reg_addr, *ptr_reg_data);

    return rv;
}

AIR_ERROR_NO_T
_hal_pearl_mdio_writeC45(
    const UI32_T    unit,
    const UI16_T    bus_id,
    const UI16_T    phy_addr,
    const UI16_T    dev_type,
    const UI16_T    reg_addr,
    const UI16_T    reg_data)
{
    AIR_ERROR_NO_T rv = AIR_E_OK;
    UI32_T wdata = 0;
    UI32_T checkBit = 0, busytime = 0;

    DIAG_PRINT(HAL_DBG_INFO,
            " write_c45 unit(%u) bus(0x%X) phy(0x%X) dev(0x%X) reg(0x%X) val(0x%X)\n",
            unit, bus_id, phy_addr, dev_type, reg_addr, reg_data);
    /* phase1 set ctrl mode */
    wdata = SET_MDIO_PHY(phy_addr) | SET_MDIO_REG(dev_type) | SET_MDIO_DATA(reg_addr)\
            | (SET_MDIO_CMD_CL45ADDR) | (SET_MDIO_ST_CL45);
    io_write32(PHY_IAC, wdata);
    wdata |= SET_MDIO_ACS_START;
    io_write32(PHY_IAC, wdata);
    while(1)
    {
        checkBit = io_read32(PHY_IAC);
        if (!(checkBit & SET_MDIO_ACS_START))
        {
            break;
        }
        else if (busytime >= IAC_MAX_BUSY_TIME)
        {
            rv = AIR_E_TIMEOUT;
            break;
        }
        busytime++;
    }

    if (AIR_E_OK == rv)
    {
        /* phase2 set write mode & write data */
        wdata = SET_MDIO_PHY(phy_addr) | SET_MDIO_REG(dev_type) | SET_MDIO_DATA(reg_data)\
                | (SET_MDIO_CMD_CL45W) | (SET_MDIO_ST_CL45);
        io_write32(PHY_IAC, wdata);
        wdata |= SET_MDIO_ACS_START;
        io_write32(PHY_IAC, wdata);
        busytime = 0;
        while(1)
        {
            checkBit = io_read32(PHY_IAC);
            if (!(checkBit & SET_MDIO_ACS_START))
            {
                break;
            }
            else if (busytime >= IAC_MAX_BUSY_TIME)
            {
                rv = AIR_E_TIMEOUT;
                break;
            }
            busytime++;
        }
    }

    if (AIR_E_OK == rv)
    {
        DIAG_PRINT(HAL_DBG_INFO, " (%u) Write BusId[%u] PhyAddr[0x%x] DevId[0x%X] Reg[0x%X] : 0x%08X\n",
            unit, bus_id, phy_addr, dev_type, reg_addr, reg_data);
    }
    else
    {
        DIAG_PRINT(HAL_DBG_ERR, " (%u) rv=(%d)\n", unit, rv);
    }
    return rv;
}

AIR_ERROR_NO_T
_hal_pearl_mdio_readC45(
    const UI32_T    unit,
    const UI16_T    bus_id,
    const UI16_T    phy_addr,
    const UI16_T    dev_type,
    const UI16_T    reg_addr,
    UI16_T          *ptr_reg_data)
{
    AIR_ERROR_NO_T rv = AIR_E_OK;
    UI32_T wdata = 0;
    UI32_T checkBit = 0, busytime = 0;
    UI32_T data = 0;

    /* phase1 set ctrl mode */
    wdata = SET_MDIO_PHY(phy_addr) | SET_MDIO_REG(dev_type) | SET_MDIO_DATA(reg_addr)\
            | (SET_MDIO_CMD_CL45ADDR) | (SET_MDIO_ST_CL45);
    io_write32(PHY_IAC, wdata);
    wdata |= SET_MDIO_ACS_START;
    io_write32(PHY_IAC, wdata);
    while(1)
    {
        checkBit = io_read32(PHY_IAC);
        if (!(checkBit & SET_MDIO_ACS_START))
        {
            break;
        }
        else if (busytime >= IAC_MAX_BUSY_TIME)
        {
            rv = AIR_E_TIMEOUT;
            break;
        }
        busytime++;
    }

    if (AIR_E_OK == rv)
    {
        /* phase2 set read mode */
        wdata = SET_MDIO_PHY(phy_addr) | SET_MDIO_REG(dev_type) \
                | (SET_MDIO_CMD_CL45R) | (SET_MDIO_ST_CL45);
        io_write32(PHY_IAC, wdata);
        wdata |= SET_MDIO_ACS_START;
        io_write32(PHY_IAC, wdata);
        busytime = 0;
        while(1)
        {
            checkBit = io_read32(PHY_IAC);
            if(!(checkBit & SET_MDIO_ACS_START))
            {
                break;
            }
            else if (busytime >= IAC_MAX_BUSY_TIME)
            {
                rv = AIR_E_TIMEOUT;
                break;
            }
            busytime++;
        }
        if (AIR_E_OK == rv)
        {
            /* phase3 read data */
            data = io_read32(PHY_IAD);
            *ptr_reg_data = GET_MDIO_DATA(data);
            DIAG_PRINT(HAL_DBG_INFO,
                    " read_c45 unit(%u) bus(0x%X) phy(0x%X) dev(0x%X) reg(0x%X) val(0x%X)\n",
                    unit, bus_id, phy_addr, dev_type, reg_addr, *ptr_reg_data);
        }
    }

    if (AIR_E_OK == rv)
    {
        DIAG_PRINT(HAL_DBG_INFO, " (%u) Read BusId[%u] PhyAddr[0x%x] DevId[0x%X] Reg[0x%X] : 0x%08X\n",
            unit, bus_id, phy_addr, dev_type, reg_addr, data);
    }
    else
    {
        DIAG_PRINT(HAL_DBG_ERR, " (%u) rv=(%d)\n", unit, rv);
    }

    return rv;
}

/* STATIC VARIABLE DECLARATIONS */

/* table/register control blocks */


/* EXPORTED SUBPROGRAM BODIES*/
/* FUNCTION NAME: hal_pearl_mdio_writeC22
 * PURPOSE:
 *      Write the mdio data for IEEE clause 22
 *
 * INPUT:
 *      unit            --  Device ID
 *      bus_id          --  Bus ID
 *      phy_addr        --  PHY address
 *      reg_addr        --  Register address
 *      reg_data        --  Register data
 *
 * OUTPUT:
 *        None
 *
 * RETURN:
 *        AIR_E_OK
 *        AIR_E_BAD_PARAMETER
 *
 * NOTES:
 *      None
 */
AIR_ERROR_NO_T
hal_pearl_mdio_writeC22(
    const UI32_T    unit,
    const UI16_T    bus_id,
    const UI16_T    phy_addr,
    const UI16_T    reg_addr,
    const UI16_T    reg_data)
{
    AIR_ERROR_NO_T rv = AIR_E_OK;

    DIAG_PRINT(HAL_DBG_INFO, " write_c22 unit(%u) bus(0x%X) phy(0x%X) reg(0x%X) val(0x%X)\n",
            unit, bus_id, phy_addr, reg_addr, reg_data);
    //diag_print(__AIR_MODULE__,__func__,__LINE__, " write_c22 unit(%u) bus(0x%X) phy(0x%X) reg(0x%X) val(0x%X)\n",
    //        unit, bus_id, phy_addr, reg_addr, reg_data);

    if (phy_addr <= 4)
    {   
        _cl22_write(phy_addr, 0x0, reg_addr, reg_data);
    }
    else
    {
        _hal_pearl_mdio_writeC22(unit, bus_id, phy_addr, reg_addr, reg_data);
    }
    return rv;
}

/* FUNCTION NAME: hal_pearl_mdio_readC22
 * PURPOSE:
 *      Read the mdio data for IEEE clause 22
 *
 * INPUT:
 *      unit            --  Device ID
 *      bus_id          --  Bus ID
 *      phy_addr        --  PHY address
 *      reg_addr        --  Register address
 *
 * OUTPUT:
 *      ptr_reg_data    -- Data of mdio slave
 *
 * RETURN:
 *        AIR_E_OK
 *        AIR_E_BAD_PARAMETER
 *
 * NOTES:
 *      None
 */
AIR_ERROR_NO_T
hal_pearl_mdio_readC22(
    const UI32_T    unit,
    const UI16_T    bus_id,
    const UI16_T    phy_addr,
    const UI16_T    reg_addr,
    UI16_T          *ptr_reg_data)
{
    AIR_ERROR_NO_T rv = AIR_E_OK;

    if (phy_addr <= 4)
    {
        *ptr_reg_data = _cl22_read(phy_addr, 0x0, reg_addr);
    }
    else
    {
        _hal_pearl_mdio_readC22(unit, bus_id, phy_addr, reg_addr, ptr_reg_data);
    }
    //diag_print(__AIR_MODULE__,__func__,__LINE__, " read_c22 unit(%u) bus(0x%X) phy(0x%X) reg(0x%X) val(0x%X)\n",
    //        unit, bus_id, phy_addr, reg_addr, *ptr_reg_data);
    return rv;
}

/* FUNCTION NAME: hal_pearl_mdio_writeC45
 * PURPOSE:
 *      Write the mdio data for IEEE clause 45
 *
 * INPUT:
 *      unit            --  Device ID
 *      port            --  Port Number
 *      dev_type        --  Device type
 *      reg_addr        --  Register address
 *      reg_data        --  Register data
 *
 * OUTPUT:
 *        None
 *
 * RETURN:
 *        AIR_E_OK
 *        AIR_E_BAD_PARAMETER
 *
 * NOTES:
 *      None
 */
AIR_ERROR_NO_T
hal_pearl_mdio_writeC45(
    const UI32_T    unit,
    const UI16_T    bus_id,
    const UI16_T    phy_addr,
    const UI16_T    dev_type,
    const UI16_T    reg_addr,
    const UI16_T    reg_data)
{
    AIR_ERROR_NO_T rv = AIR_E_OK;

    DIAG_PRINT(HAL_DBG_INFO,
            " write_c45 unit(%u) bus(0x%X) phy(0x%X) dev(0x%X) reg(0x%X) val(0x%X)\n",
            unit, bus_id, phy_addr, dev_type, reg_addr, reg_data);
    //diag_print(__AIR_MODULE__,__func__,__LINE__, " write_c45 unit(%u) bus(0x%X) phy(0x%X) dev(0x%X) reg(0x%X) val(0x%X)\n",
    //        unit, bus_id, phy_addr, dev_type, reg_addr, reg_data);
    if (phy_addr <= 4)
    {
        _cl45_write(phy_addr, dev_type, reg_addr, reg_data);
    }
    else
    {
        _hal_pearl_mdio_writeC45(unit, bus_id, phy_addr, dev_type, reg_addr, reg_data);
    }

    return rv;
}

/* FUNCTION NAME: hal_pearl_mdio_readC45
 * PURPOSE:
 *      Read the mdio data for IEEE clause 45
 *
 * INPUT:
 *      unit            --  Device ID
 *      port            --  Port number
 *      dev_type        --  Device type
 *      reg_addr        --  Register address
 *
 * OUTPUT:
 *      ptr_reg_data    -- Data of mdio slave
 *
 * RETURN:
 *        AIR_E_OK
 *        AIR_E_BAD_PARAMETER
 *
 * NOTES:
 *      None
 */
AIR_ERROR_NO_T
hal_pearl_mdio_readC45(
    const UI32_T    unit,
    const UI16_T    bus_id,
    const UI16_T    phy_addr,
    const UI16_T    dev_type,
    const UI16_T    reg_addr,
    UI16_T          *ptr_reg_data)
{
    AIR_ERROR_NO_T rv = AIR_E_OK;

    if (phy_addr <= 4)
    {
        *ptr_reg_data = _cl45_read(phy_addr, dev_type, reg_addr);
    }
    else
    {
        _hal_pearl_mdio_readC45(unit, bus_id, phy_addr, dev_type, reg_addr, ptr_reg_data);
    }
    //diag_print(__AIR_MODULE__,__func__,__LINE__, " read_c45 unit(%u) bus(0x%X) phy(0x%X) dev(0x%X) reg(0x%X) val(0x%X)\n",
    //        unit, bus_id, phy_addr, dev_type, reg_addr, *ptr_reg_data);
    return rv;
}

