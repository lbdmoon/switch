/*******************************************************************************
*  Copyright Statement:
*  --------------------
*  This software is protected by Copyright and the information contained
*  herein is confidential. The software may not be copied and the information
*  contained herein may not be used or disclosed except with the written
*  permission of Airoha Technology Corp. (C) 2021
*
*  BY OPENING THIS FILE, BUYER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
*  THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("AIROHA SOFTWARE")
*  RECEIVED FROM AIROHA AND/OR ITS REPRESENTATIVES ARE PROVIDED TO BUYER ON
*  AN "AS-IS" BASIS ONLY. AIROHA EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
*  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
*  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
*  NEITHER DOES AIROHA PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
*  SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
*  SUPPLIED WITH THE AIROHA SOFTWARE, AND BUYER AGREES TO LOOK ONLY TO SUCH
*  THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. AIROHA SHALL ALSO
*  NOT BE RESPONSIBLE FOR ANY AIROHA SOFTWARE RELEASES MADE TO BUYER'S
*  SPECIFICATION OR TO CONFORM TO A PARTICULAR STANDARD OR OPEN FORUM.
*
*  BUYER'S SOLE AND EXCLUSIVE REMEDY AND AIROHA'S ENTIRE AND CUMULATIVE
*  LIABILITY WITH RESPECT TO THE AIROHA SOFTWARE RELEASED HEREUNDER WILL BE,
*  AT AIROHA'S OPTION, TO REVISE OR REPLACE THE AIROHA SOFTWARE AT ISSUE,
*  OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY BUYER TO
*  AIROHA FOR SUCH AIROHA SOFTWARE AT ISSUE.
*
*  THE TRANSACTION CONTEMPLATED HEREUNDER SHALL BE CONSTRUED IN ACCORDANCE
*  WITH THE LAWS OF THE STATE OF CALIFORNIA, USA, EXCLUDING ITS CONFLICT OF
*  LAWS PRINCIPLES.  ANY DISPUTES, CONTROVERSIES OR CLAIMS ARISING THEREOF AND
*  RELATED THERETO SHALL BE SETTLED BY ARBITRATION IN SAN FRANCISCO, CA, UNDER
*  THE RULES OF THE INTERNATIONAL CHAMBER OF COMMERCE (ICC).
*
*******************************************************************************/

/* FILE NAME:  hal_sco_chip.c
 * PURPOSE:
 *  Implement Chip module HAL function.
 *
 * NOTES:
 *
 */

/* INCLUDE FILE DECLARTIONS
*/
#include <aml/aml.h>
#include <api/diag.h>
#include <cmlib/cmlib_bit.h>
#include <hal/switch/sco/hal_sco_chip.h>
#include <hal/switch/sco/hal_sco_reg.h>
#include <hal/switch/sco/hal_sco_mdio.h>
#include <hal/common/hal_cfg.h>
#include <air_cfg.h>

/* NAMING CONSTANT DECLARATIONS
*/

/* MACRO FUNCTION DECLARATIONS
 */

/* DATA TYPE DECLARATIONS
*/
typedef struct EFUSE_DATA_S {
    UI32_T data[4];
} EFUSE_DATA_T;

#define COL_TO_BYTE(col)            (col  << 4)
#define WORD_TO_COL(word)           (word >> 2)
#define EFUSE_MAX_DELAY             (0x10000000)  /* (1073741823 * 6) / 900M ~= 1.78s */
#define EFUSE_KICK_SHIFT            (30)

#define EFUSE_ADDR_MASK             (0x1FF)
#define EFUSE_ADDR_SHIFT            (16)
#define EFUSE_MODE_MASK             (0x3)
#define EFUSE_MODE_SHIFT            (6)
#define EFUSE_MODE_READ             (0x1)

#define EFUSE_REMARK_DEVICE_ID_OFFT (16)
#define EFUSE_REMARK_DEVICE_ID_LENG (8)
#define EFUSE_DEVICE_ID_OFFT        (8)
#define EFUSE_DEVICE_ID_LENG        (8)
#define EFUSE_REVISION_ID_OFFT      (0)
#define EFUSE_REVISION_ID_LENG      (3)

#define RESET_CONTROL_REG_2         (0x100050C4)
#define HSGMII_HSI0_RESET           (0x2000)
#define HSGMII_HSI1_RESET           (0x4000)
#define HSGMII_HSI2_RESET           (0x8000)
#define HSGMII_HSI3_RESET           (0x10000)
#define HSGMII_HSI4_RESET           (0x20000)

#define GLOBAL_FC_PORT_BLK_HI       (0x84)

/* GLOBAL VARIABLE DECLARATIONS
*/

/* STATIC VARIABLE DECLARATIONS
 */
DIAG_SET_MODULE_INFO(AIR_MODULE_CHIP, "hal_sco_chip.c");

/* LOCAL SUBPROGRAM SPECIFICATIONS
*/
static AIR_ERROR_NO_T
_hal_chip_efuse_check_busy(
    const UI32_T unit)
{
    UI32_T delay = EFUSE_MAX_DELAY;
    UI32_T efuse_ctrl;

    do
    {
        delay--;
        aml_readReg(unit, EFUSE_CTRL, &efuse_ctrl, sizeof(efuse_ctrl));
    } while((efuse_ctrl & (1 << EFUSE_KICK_SHIFT)) && delay);

    if (!delay)
    {
        DIAG_PRINT(HAL_DBG_ERR, "efuse_read_data timeout \n");
        return AIR_E_TIMEOUT;
    }
    return AIR_E_OK;
}

static AIR_ERROR_NO_T
_hal_chip_efuse_read_data(
    const UI32_T unit,
    const UI32_T byte_offset,
    EFUSE_DATA_T *ptr_data)
{
    AIR_ERROR_NO_T rc = AIR_E_OK;
    UI32_T efuse_ctrl = 0;

    rc = aml_readReg(unit, EFUSE_CTRL, &efuse_ctrl, sizeof(UI32_T));
    if (AIR_E_OK != rc)
    {
        return rc;
    }

    if (efuse_ctrl & (1 << EFUSE_KICK_SHIFT))
    {
        return AIR_E_OP_INVALID;
    }

    efuse_ctrl &= ~((EFUSE_ADDR_MASK << EFUSE_ADDR_SHIFT) | (EFUSE_MODE_MASK << EFUSE_MODE_SHIFT));
    efuse_ctrl |= ((EFUSE_MODE_READ << EFUSE_MODE_SHIFT) | ((byte_offset & EFUSE_ADDR_MASK) << EFUSE_ADDR_SHIFT) | (1 << EFUSE_KICK_SHIFT));

    rc = aml_writeReg(unit, EFUSE_CTRL, &efuse_ctrl, sizeof(UI32_T));
    if (AIR_E_OK != rc)
    {
        return rc;
    }

    rc = _hal_chip_efuse_check_busy(unit);
    if (AIR_E_OK != rc)
    {
        return rc;
    }

    aml_readReg(unit, EFUSE_RDATA0, &ptr_data->data[0], sizeof(UI32_T));
    aml_readReg(unit, EFUSE_RDATA1, &ptr_data->data[1], sizeof(UI32_T));
    aml_readReg(unit, EFUSE_RDATA2, &ptr_data->data[2], sizeof(UI32_T));
    aml_readReg(unit, EFUSE_RDATA3, &ptr_data->data[3], sizeof(UI32_T));

    return AIR_E_OK;
}

static AIR_ERROR_NO_T
_hal_chip_efuse_read_word(
    const UI32_T unit,
    const UI32_T word_offset,
    UI32_T *ptr_data)
{
    AIR_ERROR_NO_T rc = AIR_E_OK;
    EFUSE_DATA_T efuse_data;
    osal_memset(&efuse_data, 0, sizeof(EFUSE_DATA_T));

    rc = _hal_chip_efuse_read_data(unit, COL_TO_BYTE(WORD_TO_COL(word_offset)), &efuse_data);
    DIAG_PRINT(HAL_DBG_INFO, "word %d: %x \n", word_offset, efuse_data.data[word_offset % 4]);

    (*ptr_data) = efuse_data.data[word_offset % 4];

    return rc;
}


/* EXPORTED SUBPROGRAM BODIES
*/
/* FUNCTION NAME:   hal_sco_chip_readDeviceInfo
 * PURPOSE:
 *      To read the device/revision ID of the EFUSE.
 * INPUT:
 *      unit            -- the device unit
 * OUTPUT:
 *      ptr_device_id   -- pointer for the device ID
 *      ptr_revision_id -- pointer for the revision ID
 * RETURN:
 *      AIR_E_OK            -- Successfully get the IDs.
 *      AIR_E_BAD_PARAMETER -- Invalid parameter.
 * NOTES:
 *      none
 */
AIR_ERROR_NO_T
hal_sco_chip_readDeviceInfo(
    const UI32_T unit,
    UI32_T       *ptr_device_id,
    UI32_T       *ptr_revision_id)
{
    AIR_ERROR_NO_T rc = AIR_E_OK;
    UI32_T efuse_data = 0;
    UI32_T remark_device_id = 0;

    HAL_CHECK_PTR(ptr_device_id);
    HAL_CHECK_PTR(ptr_revision_id);

    rc = _hal_chip_efuse_read_word(unit, 0, &efuse_data);
    if (AIR_E_OK != rc)
    {
        DIAG_PRINT(HAL_DBG_ERR, "unit=%u, get efuse failed, rc=%d\n", unit, rc);
        return rc;
    }

    remark_device_id = BITS_OFF_R(efuse_data, EFUSE_REMARK_DEVICE_ID_OFFT, EFUSE_REMARK_DEVICE_ID_LENG);
    if (0 == remark_device_id)
    {
        (*ptr_device_id) = BITS_OFF_R(efuse_data, EFUSE_DEVICE_ID_OFFT, EFUSE_DEVICE_ID_LENG);
        (*ptr_revision_id) = BITS_OFF_R(efuse_data, EFUSE_REVISION_ID_OFFT, EFUSE_REVISION_ID_LENG);
    }
    else
    {
        (*ptr_device_id) = remark_device_id;
        (*ptr_revision_id) = BITS_OFF_R(efuse_data, EFUSE_REVISION_ID_OFFT, EFUSE_REVISION_ID_LENG);
    }
    DIAG_PRINT(HAL_DBG_INFO, "unit=%u, device_id=%d revision_id=%d\n", unit, (*ptr_device_id), (*ptr_revision_id));

    return rc;
}

/* FUNCTION NAME: hal_sco_chip_init
 * PURPOSE:
 *      Chip initialization
 *
 * INPUT:
 *      unit            --  Device ID
 *
 * OUTPUT:
 *      ptr_chip_init_param     --  chip init parameter
 *
 * RETURN:
 *        AIR_E_OK
 *
 * NOTES:
 *      None
 */
AIR_ERROR_NO_T
hal_sco_chip_init(
    const UI32_T unit,
    HAL_CHIP_INIT_PARAM_T *ptr_chip_init_param)
{
    AIR_CFG_VALUE_T     mdio_clock;
    AIR_CFG_VALUE_T     sled;
    AIR_CFG_VALUE_T     led_type;
    UI32_T              u32dat = 0;
    HAL_CHECK_PTR(ptr_chip_init_param);

    osal_memset(&mdio_clock, 0, sizeof(AIR_CFG_VALUE_T));
    osal_memset(&sled, 0, sizeof(AIR_CFG_VALUE_T));
    osal_memset(&led_type, 0, sizeof(AIR_CFG_VALUE_T));
    /*
     * Disable MAC power saving
     * Note: Acl udf access clock will stop when all port link down.
     */
    aml_readReg(unit, CKGCR, &u32dat, sizeof(u32dat));
    u32dat &= ~CKG_LNKDN_GLB_STOP;
    aml_writeReg(unit, CKGCR, &u32dat, sizeof(u32dat));
    /* setup mdio clock */
    mdio_clock.value = 0;
    hal_cfg_getValue(unit, AIR_CFG_TYPE_MDIO_CLOCK, &mdio_clock);
    aml_readReg(unit, PHY_SMI, &u32dat, sizeof(u32dat));
    u32dat = (u32dat & ~(CSR_SMI_PMDC_MASK)) | ((mdio_clock.value & 0x03) << CSR_SMI_PMDC_OFFT);
    aml_writeReg(unit, PHY_SMI, &u32dat, sizeof(u32dat));

    /* setup serial LED */
    sled.value = 2;
    hal_cfg_getValue(unit, AIR_CFG_TYPE_PHY_LED_COUNT, &sled);
    u32dat = (sled.value << 1) + SLED_RISING_EDGE + SLED_OUTPUT_TRANSITION_MODE;
    aml_writeReg(unit, SLED_CTRL0, &u32dat, sizeof(u32dat));

    /* setup LED for serial or parallel type */
    led_type.value = 0;
    hal_cfg_getValue(unit, AIR_CFG_TYPE_PHY_LED_TYPE, &led_type);
    if (0 == led_type.value)
    {
        /* enable serial LED */
        u32dat = 0x0;
        aml_writeReg(unit, RG_GPIO_INVERSE, &u32dat, sizeof(u32dat));
        u32dat = 0x0;
        aml_writeReg(unit, RG_LAN_LED_IOMUX, &u32dat, sizeof(u32dat));
    }
    else
    {
        u32dat = HAL_DEVICE_CHIP_ID(unit);
        if ((HAL_SCO_DEVICE_ID_EN8851C != u32dat) && (HAL_SCO_DEVICE_ID_EN8851E != u32dat))
        {
            DIAG_PRINT(HAL_DBG_ERR, "unit=%u, parallel LED is not supported by the device.\n", unit);
            return AIR_E_NOT_SUPPORT;
        }
        /* enable parallel LED */
        aml_readReg(unit, RG_GPIO_EN_REG, &u32dat, sizeof(u32dat));
        u32dat &= ~(PARALLEL_LED_PORT);
        aml_writeReg(unit, RG_GPIO_EN_REG, &u32dat, sizeof(u32dat));
        u32dat = PARALLEL_LED_PORT;
        aml_writeReg(unit, RG_GPIO_INVERSE, &u32dat, sizeof(u32dat));
        u32dat = PARALLEL_LED_MODE;
        aml_writeReg(unit, RG_LAN_LED_IOMUX, &u32dat, sizeof(u32dat));
    }

    /* Disabled unused CKO */
    switch (HAL_DEVICE_CHIP_ID(unit))
    {
        case HAL_SCO_DEVICE_ID_EN8851C:
        case HAL_SCO_DEVICE_ID_EN8851E:
            aml_readReg(unit, REG_BGPOR_CTRL1, &u32dat, sizeof(u32dat));
            u32dat |= BIT(16);
            aml_writeReg(unit, REG_BGPOR_CTRL1, &u32dat, sizeof(u32dat));
            break;
        default:
            break;
    }

    /* Power down all serdes */
    aml_readReg(unit, RESET_CONTROL_REG_2, &u32dat, sizeof(u32dat));
    u32dat |= (HSGMII_HSI0_RESET | HSGMII_HSI1_RESET | HSGMII_HSI2_RESET | HSGMII_HSI3_RESET | HSGMII_HSI4_RESET);
    aml_writeReg(unit, RESET_CONTROL_REG_2, &u32dat, sizeof(u32dat));

    /* Config Global Flow Control Threshold */
    aml_readReg(unit, GFCCR1, &u32dat, sizeof(u32dat));
    u32dat &= ~(BITS_RANGE(GFCCR1_PORT_BLK_HI_OFFT, GFCCR1_PORT_BLK_HI_LENG));
    u32dat |= BITS_OFF_L(GLOBAL_FC_PORT_BLK_HI, GFCCR1_PORT_BLK_HI_OFFT, GFCCR1_PORT_BLK_HI_LENG);
    aml_writeReg(unit, GFCCR1, &u32dat, sizeof(u32dat));

    return AIR_E_OK;
}

/* FUNCTION NAME: hal_sco_chip_deinit
 * PURPOSE:
 *      Chip initialization
 *
 * INPUT:
 *      unit            --  Device ID
 *
 * OUTPUT:
 *        None
 *
 * RETURN:
 *        AIR_E_OK
 *
 * NOTES:
 *      None
 */
AIR_ERROR_NO_T
hal_sco_chip_deinit(
    const UI32_T unit)
{
    return AIR_E_OK;
}

