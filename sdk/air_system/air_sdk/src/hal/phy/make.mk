include          $(AIR_SDK)/make/make_prologue.mk

# Subdirectories, in random order
ifeq ($(findstring en8801s,$(ENABLED_PHY_CHIPS)),en8801s)
dir             := $(d)/en8801s
include         $(dir)/make.mk
endif

ifeq ($(findstring en8808,$(ENABLED_PHY_CHIPS)),en8808)
dir             := $(d)/en8808
include         $(dir)/make.mk
endif

ifeq ($(findstring en8811h,$(ENABLED_PHY_CHIPS)),en8811h)
dir             := $(d)/en8811h
include         $(dir)/make.mk
endif

ifeq ($(findstring an8801sb,$(ENABLED_PHY_CHIPS)),an8801sb)
dir             := $(d)/an8801sb
include         $(dir)/make.mk
endif

ifeq ($(findstring an8804,$(ENABLED_PHY_CHIPS)),an8804)
dir             := $(d)/an8804
include         $(dir)/make.mk
endif

# End subdirectories
# Local rules


include          $(AIR_SDK)/make/make_epilogue.mk

