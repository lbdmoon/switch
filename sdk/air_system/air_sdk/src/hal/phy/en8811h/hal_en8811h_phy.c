/*******************************************************************************
*  Copyright Statement:
*  --------------------
*  This software is protected by Copyright and the information contained
*  herein is confidential. The software may not be copied and the information
*  contained herein may not be used or disclosed except with the written
*  permission of Airoha Technology Corp. (C) 2021
*
*  BY OPENING THIS FILE, BUYER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
*  THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("AIROHA SOFTWARE")
*  RECEIVED FROM AIROHA AND/OR ITS REPRESENTATIVES ARE PROVIDED TO BUYER ON
*  AN "AS-IS" BASIS ONLY. AIROHA EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
*  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
*  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
*  NEITHER DOES AIROHA PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
*  SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
*  SUPPLIED WITH THE AIROHA SOFTWARE, AND BUYER AGREES TO LOOK ONLY TO SUCH
*  THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. AIROHA SHALL ALSO
*  NOT BE RESPONSIBLE FOR ANY AIROHA SOFTWARE RELEASES MADE TO BUYER'S
*  SPECIFICATION OR TO CONFORM TO A PARTICULAR STANDARD OR OPEN FORUM.
*
*  BUYER'S SOLE AND EXCLUSIVE REMEDY AND AIROHA'S ENTIRE AND CUMULATIVE
*  LIABILITY WITH RESPECT TO THE AIROHA SOFTWARE RELEASED HEREUNDER WILL BE,
*  AT AIROHA'S OPTION, TO REVISE OR REPLACE THE AIROHA SOFTWARE AT ISSUE,
*  OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY BUYER TO
*  AIROHA FOR SUCH AIROHA SOFTWARE AT ISSUE.
*
*  THE TRANSACTION CONTEMPLATED HEREUNDER SHALL BE CONSTRUED IN ACCORDANCE
*  WITH THE LAWS OF THE STATE OF CALIFORNIA, USA, EXCLUDING ITS CONFLICT OF
*  LAWS PRINCIPLES.  ANY DISPUTES, CONTROVERSIES OR CLAIMS ARISING THEREOF AND
*  RELATED THERETO SHALL BE SETTLED BY ARBITRATION IN SAN FRANCISCO, CA, UNDER
*  THE RULES OF THE INTERNATIONAL CHAMBER OF COMMERCE (ICC).
*
*******************************************************************************/

/* FILE NAME:  hal_en8811h_phy.c
 * PURPOSE:
 *  Implement en8811 phy module HAL function.
 *
 * NOTES:
 *
 */

/* INCLUDE FILE DECLARTIONS
*/
#include <hal/common/hal.h>
#include <hal/common/hal_cfg.h>
#include <hal/common/hal_phy.h>
#include <hal/common/hal_cmn_phy.h>
#include <hal/common/hal_mdio.h>
#include <hal/phy/en8811h/hal_en8811h_phy.h>
#include <cmlib/cmlib_bit.h>

/* NAMING CONSTANT DECLARATIONS
*/
#define HAL_EN8811H_PBUS_PHY_ADDRESS    (0x17)
#define HAL_EN8811H_PHY_READY           (0x02)
#define HAL_EN8811H_MAX_RETRY           (50)
#define HAL_EN8811H_LINK_SPEED_2500M    (0x10)

#define PHY_MAX_NUMBER                  (32)

#define PHY_LED_TYPE_ON                 (0)
#define PHY_LED_TYPE_BLK                (1)

#define HAL_EN8811H_MAX_LED_COUNT       (2)
#define PHY_LED_CFG_REG                 (EN8811H_LED_3_BLK_MASK)
/* MACRO FUNCTION DECLARATIONS
 */
#define LED_ID_TO_LED_REG_ADDRESS(__led_id__, __led_type__, __led_reg_addr__) do        \
    {                                                                                   \
        ((__led_type__) == PHY_LED_TYPE_ON ?                                            \
            ((__led_reg_addr__) = EN8811H_LED_0_ON_MASK) :                              \
            ((__led_reg_addr__) = EN8811H_LED_0_BLK_MASK));                             \
        __led_reg_addr__ += (EN8811H_LED_RG_OFFSET * __led_id__);                       \
    }while(0)
/* DATA TYPE DECLARATIONS
*/

/* GLOBAL VARIABLE DECLARATIONS
*/
DIAG_SET_MODULE_INFO(AIR_MODULE_PHY, "hal_en8811_phy.c");

extern const UI32_T en8811h_fw_dm_size;
extern const UI8_T  en8811h_fw_dm[];
extern const UI32_T en8811h_fw_pm_size;
extern const UI8_T  en8811h_fw_pm[];

/* LOCAL SUBPROGRAM SPECIFICATIONS
*/
/* EN8811H PBUS write function */
static AIR_ERROR_NO_T _hal_en8811h_pbus_regwr(
    const UI32_T pbus_addr,
    const UI32_T pbus_data)
{
    AIR_ERROR_NO_T  rv = AIR_E_OK;

    rv |= hal_mdio_writeC22(0, 0, HAL_EN8811H_PBUS_PHY_ADDRESS, 0x1F, (UI16_T)(pbus_addr >> 6));
    rv |= hal_mdio_writeC22(0, 0, HAL_EN8811H_PBUS_PHY_ADDRESS, (UI16_T)((pbus_addr >> 2) & 0xf), (UI16_T)(pbus_data & 0xFFFF));
    rv |= hal_mdio_writeC22(0, 0, HAL_EN8811H_PBUS_PHY_ADDRESS, 0x10, (UI16_T)(pbus_data >> 16));
    return rv;
}

/* EN8811H BUCK write function */
static AIR_ERROR_NO_T _hal_en8811h_buck_pbus_regwr(
    const UI32_T port,
    const UI32_T pbus_addr,
    const UI32_T pbus_data)
{
    AIR_ERROR_NO_T  rv = AIR_E_OK;

    rv |= hal_mdio_writeC22ByPort(0, port, 0x1F, 0x4);
    rv |= hal_mdio_writeC22ByPort(0, port, 0x10, 0x0);
    rv |= hal_mdio_writeC22ByPort(0, port, 0x11, (UI32_T)((pbus_addr >> 16) & 0xffff));
    rv |= hal_mdio_writeC22ByPort(0, port, 0x12, (UI32_T)(pbus_addr & 0xffff));
    rv |= hal_mdio_writeC22ByPort(0, port, 0x13, (UI32_T)((pbus_data >> 16) & 0xffff));
    rv |= hal_mdio_writeC22ByPort(0, port, 0x14, (UI32_T)(pbus_data & 0xffff));
    return rv;
}

/* EN8811H BUCK read function */
static AIR_ERROR_NO_T _hal_en8811h_buck_pbus_regrd(UI32_T port, UI32_T pbus_addr, UI32_T *ptr_pbus_data)
{
    UI32_T          pbus_data = 0;
    UI16_T          pbus_data_low, pbus_data_high;
    AIR_ERROR_NO_T  rv = AIR_E_OK;

    rv |= hal_mdio_writeC22ByPort(0, port, 0x1F, 0x4);
    rv |= hal_mdio_writeC22ByPort(0, port, 0x10, 0x0);
    rv |= hal_mdio_writeC22ByPort(0, port, 0x15, (UI32_T)((pbus_addr >> 16) & 0xffff));
    rv |= hal_mdio_writeC22ByPort(0, port, 0x16, (UI32_T)(pbus_addr & 0xffff));

    rv |= hal_mdio_readC22ByPort(0, port, 0x17, &pbus_data_high);
    rv |= hal_mdio_readC22ByPort(0, port, 0x18, &pbus_data_low);

    pbus_data = (pbus_data_high << 16) + pbus_data_low;
    *ptr_pbus_data = pbus_data;
    return rv;
}

static AIR_ERROR_NO_T _hal_en8811h_mdio_write_buffer(
    const UI32_T    port,
    const UI32_T    addr,
    const UI32_T    arr_size,
    const UI8_T     *ptr_buf)
{
    UI16_T          write_data = 0;
    UI32_T          offset = 0;
    AIR_ERROR_NO_T  rv = AIR_E_OK;

    rv |= hal_mdio_writeC22ByPort(0, port, 0x1F, 0x4);
    rv |= hal_mdio_writeC22ByPort(0, port, 0x10, 0x8000);
    rv |= hal_mdio_writeC22ByPort(0, port, 0x11, (UI16_T)((addr >> 16) & 0xffff));
    rv |= hal_mdio_writeC22ByPort(0, port, 0x12, (UI16_T)(addr & 0xffff));

    for (offset = 0; offset < arr_size; offset += 4)
    {
        write_data = (ptr_buf[offset + 3] << 8) | ptr_buf[offset + 2];
        rv |= hal_mdio_writeC22ByPort(0, port, 0x13, write_data);

        write_data = (ptr_buf[offset + 1] << 8) | ptr_buf[offset];
        rv |= hal_mdio_writeC22ByPort(0, port, 0x14, write_data);
    }

    rv |= hal_mdio_writeC22ByPort(0, port, 0x1F, 0x0);
    return rv;
}

static AIR_ERROR_NO_T _hal_en8811h_firmware_download(
    const UI32_T    unit,
    const UI32_T    port)
{
    UI16_T          reg_value = 0, retry = 0;
    AIR_ERROR_NO_T  rv = AIR_E_OK;

    rv |= _hal_en8811h_pbus_regwr(0xcf928, 0x0);
    rv |= _hal_en8811h_buck_pbus_regwr(port, 0x0f0018, 0x0);
    rv |= hal_en8811h_phy_setAdminState(unit, port, HAL_PHY_ADMIN_STATE_DISABLE);
    rv |= _hal_en8811h_mdio_write_buffer(port, 0x00000000, en8811h_fw_dm_size, en8811h_fw_dm);
    rv |= _hal_en8811h_mdio_write_buffer(port, 0x00100000, en8811h_fw_pm_size, en8811h_fw_pm);
    rv |= hal_en8811h_phy_setAdminState(unit, port, HAL_PHY_ADMIN_STATE_ENABLE);
    rv |= _hal_en8811h_buck_pbus_regwr(port, 0x0f0018, 0x01);


    for (retry = 0; retry < HAL_EN8811H_MAX_RETRY; retry++)
    {
        osal_delayUs(100000);
        rv |= hal_mdio_readC45ByPort(unit, port, MMD_DEV_VSPEC1, 0x8009, &reg_value);
        if (HAL_EN8811H_PHY_READY == reg_value)
        {
            break;
        }
    }

    if (HAL_EN8811H_MAX_RETRY == retry)
    {
        DIAG_PRINT(HAL_DBG_ERR, "[Dbg]: en8811 port %d FIRMWARE download fail\n", port);
        return AIR_E_NOT_INITED;
    }

    return rv;
}

static AIR_ERROR_NO_T _hal_en8811h_led_setting(
    const UI32_T    unit,
    const UI32_T    port)
{
    AIR_ERROR_NO_T  rv = AIR_E_OK;
    AIR_CFG_VALUE_T led_behavior;
    UI16_T          led_id = 0, led_config = 0, led_count = 0;
    UI16_T          link_reg_data = 0, blk_reg_data = 0, link_reg_addr = 0, blk_reg_addr = 0;

    osal_memset(&led_behavior, 0, sizeof(AIR_CFG_VALUE_T));
    led_behavior.value = EN8811H_LED_COUNT;
    rv |= hal_cfg_getValue(0, AIR_CFG_TYPE_PHY_LED_COUNT, &led_behavior);
    led_count = led_behavior.value;

    for (led_id = 0; led_id < led_count; led_id++)
    {
        if (led_id == 0)
        {
            link_reg_addr = EN8811H_LED_0_ON_MASK;
            blk_reg_addr = EN8811H_LED_0_BLK_MASK;
        }
        else if (led_id == 1)
        {
            link_reg_addr = EN8811H_LED_1_ON_MASK;
            blk_reg_addr = EN8811H_LED_1_BLK_MASK;
        }
        else if (led_id == 2)
        {
            link_reg_addr = EN8811H_LED_2_ON_MASK;
            blk_reg_addr = EN8811H_LED_2_BLK_MASK;
        }
        else
        {
            link_reg_addr = EN8811H_LED_3_ON_MASK;
            blk_reg_addr = EN8811H_LED_3_BLK_MASK;
        }

        osal_memset(&led_behavior, 0, sizeof(AIR_CFG_VALUE_T));

        led_behavior.value = 0xFFF;
        led_behavior.param0 = port;
        led_behavior.param1 = led_id;

        rv |= hal_cfg_getValue(0, AIR_CFG_TYPE_PHY_LED_BEHAVIOR, &led_behavior);

        led_config = led_behavior.value;

        link_reg_data = 0;
        blk_reg_data = 0;

        GET_PHY_ABILITY(led_config, EN8811H_LED_BHV_LINK_1000, EN8811H_LED_LINK_1000, link_reg_data);
        GET_PHY_ABILITY(led_config, EN8811H_LED_BHV_LINK_100, EN8811H_LED_LINK_100, link_reg_data);
        GET_PHY_ABILITY(led_config, EN8811H_LED_BHV_LINK_10, EN8811H_LED_LINK_10, link_reg_data);
        GET_PHY_ABILITY(led_config, EN8811H_LED_BHV_LINK_FULLDPLX, EN8811H_LED_LINK_FULLDPLX, link_reg_data);
        GET_PHY_ABILITY(led_config, EN8811H_LED_BHV_LINK_HALFDPLX, EN8811H_LED_LINK_HALFDPLX, link_reg_data);
        GET_PHY_ABILITY(led_config, EN8811H_LED_BHV_HIGH_ACTIVE, EN8811H_LED_POL_HIGH_ACT, link_reg_data);
        GET_PHY_ABILITY(led_config, EN8811H_LED_BHV_LINK_2500, EN8811H_LED_LINK_2500, link_reg_data);

        GET_PHY_ABILITY(led_config, EN8811H_LED_BHV_BLINK_TX_1000, EN8811H_LED_BLINK_TX_1000, blk_reg_data);
        GET_PHY_ABILITY(led_config, EN8811H_LED_BHV_BLINK_RX_1000, EN8811H_LED_BLINK_RX_1000, blk_reg_data);
        GET_PHY_ABILITY(led_config, EN8811H_LED_BHV_BLINK_TX_100, EN8811H_LED_BLINK_TX_100, blk_reg_data);
        GET_PHY_ABILITY(led_config, EN8811H_LED_BHV_BLINK_RX_100, EN8811H_LED_BLINK_RX_100, blk_reg_data);
        GET_PHY_ABILITY(led_config, EN8811H_LED_BHV_BLINK_TX_10, EN8811H_LED_BLINK_TX_10, blk_reg_data);
        GET_PHY_ABILITY(led_config, EN8811H_LED_BHV_BLINK_RX_10, EN8811H_LED_BLINK_RX_10, blk_reg_data);
        GET_PHY_ABILITY(led_config, EN8811H_LED_BHV_BLINK_TX_2500, EN8811H_LED_BLINK_TX_2500, blk_reg_data);
        GET_PHY_ABILITY(led_config, EN8811H_LED_BHV_BLINK_RX_2500, EN8811H_LED_BLINK_RX_2500, blk_reg_data);

        if (link_reg_data != 0 || blk_reg_data != 0)
        {
            link_reg_data |= EN8811H_LED_FUNC_ENABLE;
        }

        DIAG_PRINT(HAL_DBG_INFO, "port %u, link_reg_addr 0x%02X, link_reg_data 0x%04X\n", port, link_reg_addr, link_reg_data);
        DIAG_PRINT(HAL_DBG_INFO, "port %u, blk_reg_addr 0x%02X, blk_reg_data 0x%04X\n", port, blk_reg_addr, blk_reg_data);

        rv |= hal_mdio_writeC45ByPort(unit, port, MMD_DEV_VSPEC2, link_reg_addr, link_reg_data);
        rv |= hal_mdio_writeC45ByPort(unit, port, MMD_DEV_VSPEC2, blk_reg_addr, blk_reg_data);
    }

    return rv;
}

/* STATIC VARIABLE DECLARATIONS */

/* table/register control blocks */

/* EXPORTED SUBPROGRAM BODIES*/

/* FUNCTION NAME:   hal_en8811h_phy_init
 * PURPOSE:
 *      EN8811 PHY initialization
 * INPUT:
 *      unit            --  Device unit number
 *      port            --  Port number
 *
 * OUTPUT:
 *
 * RETURN:
 *      AIR_E_OK
 *      AIR_E_BAD_PARAMETER
 *
 * NOTES:
 *      None
 */
AIR_ERROR_NO_T
hal_en8811h_phy_init(
    const UI32_T    unit,
    const UI32_T    port)
{
    AIR_ERROR_NO_T  rv = AIR_E_OK;
    UI16_T          phy_addr = 0;
    UI32_T          reg_data = 0;

    phy_addr = HAL_PHY_PORT_DEV_PHY_ADDR(unit, port);
    if (phy_addr >= PHY_MAX_NUMBER)
    {
        DIAG_PRINT(HAL_DBG_WARN,"[Dbg]: en8811 %d initialize fail\n", port);
        return AIR_E_BAD_PARAMETER;
    }

    rv = _hal_en8811h_firmware_download(unit, port);

    if (AIR_E_OK == rv)
    {
        /* led setup */
        rv = _hal_en8811h_buck_pbus_regrd(port, 0xcf8b8, &reg_data);
        if (AIR_E_OK == rv)
        {
            reg_data |= 0x30;
            rv = _hal_en8811h_buck_pbus_regwr(port, 0xcf8b8, reg_data);

            if (AIR_E_OK == rv)
            {
                rv = _hal_en8811h_led_setting(unit, port);
            }
        }

        if (AIR_E_OK == rv)
        {
            rv = _hal_en8811h_buck_pbus_regrd(port, 0xca0f8, &reg_data);
            if (AIR_E_OK == rv)
            {
                reg_data = (reg_data & 0xfffffffc) | EN8811H_TX_POLARITY_NORMAL | EN8811H_RX_POLARITY_NORMAL;
                rv = _hal_en8811h_buck_pbus_regwr(port, 0xca0f8, reg_data);
            }
        }

        /* set 8811 to mode-2  */
        if (AIR_E_OK == rv)
        {
            rv |= hal_mdio_writeC45ByPort(unit, port, 0x1e, 0x800c, 0x1);
            rv |= hal_mdio_writeC45ByPort(unit, port, 0x1e, 0x800d, 0x0);
            rv |= hal_mdio_writeC45ByPort(unit, port, 0x1e, 0x800e, 0x1101);
            rv |= hal_mdio_writeC45ByPort(unit, port, 0x1e, 0x800f, 0x0002);
        }
    }

    return rv;
}

/* FUNCTION NAME:   hal_en8811h_phy_setAdminState
 * PURPOSE:
 *      This API is used to set port state.
 * INPUT:
 *      unit            --  Device unit number
 *      port            --  Port number
 *      state           --  Port state
 *
 * OUTPUT:
 *
 * RETURN:
 *      AIR_E_OK
 *      AIR_E_BAD_PARAMETER
 *
 * NOTES:
 *      None
 */
AIR_ERROR_NO_T
hal_en8811h_phy_setAdminState(
    const UI32_T                unit,
    const UI32_T                port,
    const HAL_PHY_ADMIN_STATE_T state)
{
    UI16_T          reg_data = 0;
    AIR_ERROR_NO_T  rv = AIR_E_OK;

    rv = hal_mdio_readC22ByPort(unit, port, MII_BMCR, &reg_data);

    if (AIR_E_OK == rv)
    {
        if (HAL_PHY_ADMIN_STATE_ENABLE == state)
        {
            reg_data &= ~(BMCR_PDOWN);
        }
        else
        {
            reg_data |= BMCR_PDOWN;
        }

        rv = hal_mdio_writeC22ByPort(unit, port, MII_BMCR, reg_data);
    }
    return rv;
}

/* FUNCTION NAME:   hal_en8811h_phy_getAdminState
 * PURPOSE:
 *      This API is used to get port state.
 * INPUT:
 *      unit            --  Device unit number
 *      port            --  Port number
 *      ptr_state       --  Port state
 *
 * OUTPUT:
 *
 * RETURN:
 *      AIR_E_OK
 *      AIR_E_BAD_PARAMETER
 *
 * NOTES:
 *      None
 */
AIR_ERROR_NO_T
hal_en8811h_phy_getAdminState(
    const UI32_T            unit,
    const UI32_T            port,
    HAL_PHY_ADMIN_STATE_T   *ptr_state)
{
    UI16_T          reg_data = 0;
    AIR_ERROR_NO_T  rv = AIR_E_OK;

    rv = hal_mdio_readC22ByPort(unit, port, MII_BMCR, &reg_data);

    if (AIR_E_OK == rv)
    {
        if (reg_data & BMCR_PDOWN)
        {
            *ptr_state = HAL_PHY_ADMIN_STATE_DISABLE;
        }
        else
        {
            *ptr_state = HAL_PHY_ADMIN_STATE_ENABLE;
        }
    }
    return rv;
}

/* FUNCTION NAME:   hal_en8811h_phy_setAutoNego
 * PURPOSE:
 *      This API is used to set port auto-negotiation.
 * INPUT:
 *      unit            --  Device unit number
 *      port            --  Port number
 *      auto_nego       --  Auto-negotiation
 *
 * OUTPUT:
 *
 * RETURN:
 *      AIR_E_OK
 *      AIR_E_BAD_PARAMETER
 *
 * NOTES:
 *      None
 */
AIR_ERROR_NO_T
hal_en8811h_phy_setAutoNego(
    const UI32_T                unit,
    const UI32_T                port,
    const HAL_PHY_AUTO_NEGO_T   auto_nego)
{
    UI16_T          reg_data = 0;
    AIR_ERROR_NO_T  rv = AIR_E_OK;

    rv = hal_mdio_readC22ByPort(unit, port, MII_BMCR, &reg_data);

    if (AIR_E_OK == rv)
    {
        if (HAL_PHY_AUTO_NEGO_ENABLE == auto_nego)
        {
            reg_data |= BMCR_ANENABLE;
        }
        else if (HAL_PHY_AUTO_NEGO_RESTART == auto_nego)
        {
            reg_data |= BMCR_ANRESTART;
        }
        else
        {
            reg_data &= ~(BMCR_ANENABLE);
        }

        rv = hal_mdio_writeC22ByPort(unit, port, MII_BMCR, reg_data);
    }
    return rv;
}

/* FUNCTION NAME:   hal_en8811h_phy_getAutoNego
 * PURPOSE:
 *      This API is used to get port auto-negotiation.
 * INPUT:
 *      unit            --  Device unit number
 *      port            --  Port number
 *      ptr_auto_nego   --  Auto-negotiation
 *
 * OUTPUT:
 *
 * RETURN:
 *      AIR_E_OK
 *      AIR_E_BAD_PARAMETER
 *
 * NOTES:
 *      None
 */
AIR_ERROR_NO_T
hal_en8811h_phy_getAutoNego(
    const UI32_T        unit,
    const UI32_T        port,
    HAL_PHY_AUTO_NEGO_T *ptr_auto_nego)
{
    UI16_T          reg_data = 0;
    AIR_ERROR_NO_T  rv = AIR_E_OK;

    rv = hal_mdio_readC22ByPort(unit, port, MII_BMCR, &reg_data);

    if (AIR_E_OK == rv)
    {
        if (reg_data & BMCR_ANENABLE)
        {
            *ptr_auto_nego = HAL_PHY_AUTO_NEGO_ENABLE;
        }
        else
        {
            *ptr_auto_nego = HAL_PHY_AUTO_NEGO_DISABLE;
        }
    }
    return rv;
}

/* FUNCTION NAME:   hal_en8811h_phy_setLocalAdvAbility
 * PURPOSE:
 *      This API is used to set port local advertisment ability.
 * INPUT:
 *      unit            --  Device unit number
 *      port            --  Port number
 *      ptr_adv         --  Advertisement ability
 *
 * OUTPUT:
 *
 * RETURN:
 *      AIR_E_OK
 *      AIR_E_BAD_PARAMETER
 *
 * NOTES:
 *      None
 */
AIR_ERROR_NO_T
hal_en8811h_phy_setLocalAdvAbility(
    const UI32_T            unit,
    const UI32_T            port,
    const HAL_PHY_AN_ADV_T  *ptr_adv)
{
    UI16_T          reg_data = 0;
    UI16_T          flags = HAL_PHY_AN_ADV_FLAGS_10FUDX | HAL_PHY_AN_ADV_FLAGS_10HFDX | HAL_PHY_AN_ADV_FLAGS_100HFDX;
    AIR_ERROR_NO_T  rv = AIR_E_OK;

    if (ptr_adv->flags & flags)
    {
        return AIR_E_BAD_PARAMETER;
    }

    rv = hal_mdio_readC22ByPort(unit, port, MII_CTRL1000, &reg_data);

    if (AIR_E_OK == rv)
    {
        SET_PHY_ABILITY(ptr_adv->flags, HAL_PHY_AN_ADV_FLAGS_1000FUDX, ADVERTISE_1000FULL, reg_data);
        rv = hal_mdio_writeC22ByPort(unit, port, MII_CTRL1000, reg_data);
    }

    if (AIR_E_OK == rv)
    {
        rv = hal_mdio_readC22ByPort(unit, port, MII_ADVERTISE, &reg_data);
        if (AIR_E_OK == rv)
        {
            SET_PHY_ABILITY(ptr_adv->flags, HAL_PHY_AN_ADV_FLAGS_ASYM_PAUSE, ADVERTISE_PAUSE_ASYM, reg_data);
            SET_PHY_ABILITY(ptr_adv->flags, HAL_PHY_AN_ADV_FLAGS_SYM_PAUSE, ADVERTISE_PAUSE_CAP, reg_data);
            SET_PHY_ABILITY(ptr_adv->flags, HAL_PHY_AN_ADV_FLAGS_100FUDX, ADVERTISE_100FULL, reg_data);
            SET_PHY_ABILITY(ptr_adv->flags, HAL_PHY_AN_ADV_FLAGS_100HFDX, ADVERTISE_100HALF, reg_data);
            SET_PHY_ABILITY(ptr_adv->flags, HAL_PHY_AN_ADV_FLAGS_10FUDX, ADVERTISE_10FULL, reg_data);
            SET_PHY_ABILITY(ptr_adv->flags, HAL_PHY_AN_ADV_FLAGS_10HFDX, ADVERTISE_10HALF, reg_data);
            rv = hal_mdio_writeC22ByPort(unit, port, MII_ADVERTISE, reg_data);
        }
    }

    if (AIR_E_OK == rv)
    {
        reg_data = (ptr_adv->flags & HAL_PHY_AN_ADV_FLAGS_EEE) ? (EEE_1000BASE_T | EEE_100BASE_TX ) : 0;
        rv = hal_mdio_writeC45ByPort(unit, port, MMD_DEV_ANEG, MMD_EEEAR, reg_data);
    }

    if (AIR_E_OK == rv)
    {
        rv = hal_mdio_readC45ByPort(unit, port, MMD_DEV_ANEG, MULTIG_ANAR, &reg_data);
        if (AIR_E_OK == rv)
        {
            SET_PHY_ABILITY(ptr_adv->flags, HAL_PHY_AN_ADV_FLAGS_2500M, MULTIG_ANAR_2500M, reg_data);
            rv = hal_mdio_writeC45ByPort(unit, port, MMD_DEV_ANEG, MULTIG_ANAR, reg_data);
        }
    }

    return rv;
}

/* FUNCTION NAME:   hal_en8811h_phy_getLocalAdvAbility
 * PURPOSE:
 *      This API is used to get port local advertisment ability.
 * INPUT:
 *      unit            --  Device unit number
 *      port            --  Port number
 *      ptr_adv         --  Advertisement ability
 *
 * OUTPUT:
 *
 * RETURN:
 *      AIR_E_OK
 *      AIR_E_BAD_PARAMETER
 *
 * NOTES:
 *      None
 */
AIR_ERROR_NO_T
hal_en8811h_phy_getLocalAdvAbility(
    const UI32_T        unit,
    const UI32_T        port,
    HAL_PHY_AN_ADV_T    *ptr_adv)
{
    UI16_T          reg_data = 0;
    AIR_ERROR_NO_T  rv = AIR_E_OK;

    ptr_adv->flags = 0;

    rv = hal_mdio_readC22ByPort(unit, port, MII_CTRL1000, &reg_data);

    if (AIR_E_OK == rv)
    {
        GET_PHY_ABILITY(reg_data, ADVERTISE_1000FULL, HAL_PHY_AN_ADV_FLAGS_1000FUDX, ptr_adv->flags);
    }

    if (AIR_E_OK == rv)
    {
        rv = hal_mdio_readC22ByPort(unit, port, MII_ADVERTISE, &reg_data);
        if (AIR_E_OK == rv)
        {
            GET_PHY_ABILITY(reg_data, ADVERTISE_10HALF, HAL_PHY_AN_ADV_FLAGS_10HFDX, ptr_adv->flags);
            GET_PHY_ABILITY(reg_data, ADVERTISE_10FULL, HAL_PHY_AN_ADV_FLAGS_10FUDX, ptr_adv->flags);
            GET_PHY_ABILITY(reg_data, ADVERTISE_100HALF, HAL_PHY_AN_ADV_FLAGS_100HFDX, ptr_adv->flags);
            GET_PHY_ABILITY(reg_data, ADVERTISE_100FULL, HAL_PHY_AN_ADV_FLAGS_100FUDX, ptr_adv->flags);
            GET_PHY_ABILITY(reg_data, ADVERTISE_PAUSE_CAP, HAL_PHY_AN_ADV_FLAGS_SYM_PAUSE, ptr_adv->flags);
            GET_PHY_ABILITY(reg_data, ADVERTISE_PAUSE_ASYM, HAL_PHY_AN_ADV_FLAGS_ASYM_PAUSE, ptr_adv->flags);
        }
    }

    if (AIR_E_OK == rv)
    {
        rv = hal_mdio_readC45ByPort(unit, port, MMD_DEV_ANEG, MMD_EEEAR, &reg_data);
        if (AIR_E_OK == rv)
        {
            GET_PHY_ABILITY(reg_data, (EEE_1000BASE_T | EEE_100BASE_TX), HAL_PHY_AN_ADV_FLAGS_EEE, ptr_adv->flags);
        }
    }

    if (AIR_E_OK == rv)
    {
        rv = hal_mdio_readC45ByPort(unit, port, MMD_DEV_ANEG, MULTIG_ANAR, &reg_data);
        if (AIR_E_OK == rv)
        {
            GET_PHY_ABILITY(reg_data, MULTIG_ANAR_2500M, HAL_PHY_AN_ADV_FLAGS_2500M, ptr_adv->flags);
        }
    }
    return rv;
}

/* FUNCTION NAME:   hal_en8811h_phy_getRemoteAdvAbility
 * PURPOSE:
 *      This API is used to get port remote advertisment ability.
 * INPUT:
 *      unit            --  Device unit number
 *      port            --  Port number
 *      ptr_adv         --  Advertisement ability
 *
 * OUTPUT:
 *
 * RETURN:
 *      AIR_E_OK
 *      AIR_E_BAD_PARAMETER
 *
 * NOTES:
 *      None
 */
AIR_ERROR_NO_T
hal_en8811h_phy_getRemoteAdvAbility(
    const UI32_T        unit,
    const UI32_T        port,
    HAL_PHY_AN_ADV_T    *ptr_adv)
{
    UI16_T          reg_data = 0;
    AIR_ERROR_NO_T  rv = AIR_E_OK;

    ptr_adv->flags = 0;

    rv = hal_mdio_readC22ByPort(unit, port, MII_STAT1000, &reg_data);
    if (AIR_E_OK == rv)
    {
        GET_PHY_ABILITY(reg_data, LPA_1000FULL, HAL_PHY_AN_ADV_FLAGS_1000FUDX, ptr_adv->flags);
    }

    if (AIR_E_OK == rv)
    {
        rv = hal_mdio_readC22ByPort(unit, port, MII_LPA, &reg_data);
        if (AIR_E_OK == rv)
        {
            GET_PHY_ABILITY(reg_data, LPA_10HALF, HAL_PHY_AN_ADV_FLAGS_10HFDX, ptr_adv->flags);
            GET_PHY_ABILITY(reg_data, LPA_10FULL, HAL_PHY_AN_ADV_FLAGS_10FUDX, ptr_adv->flags);
            GET_PHY_ABILITY(reg_data, LPA_100HALF, HAL_PHY_AN_ADV_FLAGS_100HFDX, ptr_adv->flags);
            GET_PHY_ABILITY(reg_data, LPA_100FULL, HAL_PHY_AN_ADV_FLAGS_100FUDX, ptr_adv->flags);
            GET_PHY_ABILITY(reg_data, LPA_PAUSE_CAP, HAL_PHY_AN_ADV_FLAGS_SYM_PAUSE, ptr_adv->flags);
            GET_PHY_ABILITY(reg_data, LPA_PAUSE_ASYM, HAL_PHY_AN_ADV_FLAGS_ASYM_PAUSE, ptr_adv->flags);
        }
    }

    if (AIR_E_OK == rv)
    {
        rv = hal_mdio_readC45ByPort(unit, port, MMD_DEV_ANEG, MMD_EEELPAR, &reg_data);
        if (AIR_E_OK == rv)
        {
            GET_PHY_ABILITY(reg_data, (EEE_1000BASE_T | EEE_100BASE_TX), HAL_PHY_AN_ADV_FLAGS_EEE, ptr_adv->flags);
        }
    }

    if (AIR_E_OK == rv)
    {
        rv = hal_mdio_readC45ByPort(unit, port, MMD_DEV_ANEG, MULTIG_LPAR, &reg_data);
        if (AIR_E_OK == rv)
        {
            GET_PHY_ABILITY(reg_data, MULTIG_LPAR_2500M, HAL_PHY_AN_ADV_FLAGS_2500M, ptr_adv->flags);
        }
    }
    return rv;
}

/* FUNCTION NAME:   hal_en8811h_phy_setSpeed
 * PURPOSE:
 *      This API is used to set port speed.
 * INPUT:
 *      unit            --  Device unit number
 *      port            --  Port number
 *      speed           --  Port speed
 *
 * OUTPUT:
 *
 * RETURN:
 *      AIR_E_OK
 *      AIR_E_BAD_PARAMETER
 *
 * NOTES:
 *      None
 */
AIR_ERROR_NO_T
hal_en8811h_phy_setSpeed(
    const UI32_T            unit,
    const UI32_T            port,
    const HAL_PHY_SPEED_T   speed)
{
    UI16_T          reg_data = 0;
    AIR_ERROR_NO_T  rv = AIR_E_OK;

    if(speed >= HAL_PHY_SPEED_1000M)
    {
        return AIR_E_OP_INVALID;
    }

    rv = hal_mdio_readC22ByPort(unit, port, MII_BMCR, &reg_data);

    if (AIR_E_OK == rv)
    {
        reg_data &= ~(BMCR_SPEED1000 | BMCR_SPEED100);

        if (HAL_PHY_SPEED_1000M == speed)
        {
            reg_data |= BMCR_SPEED1000;
        }
        else if (HAL_PHY_SPEED_100M == speed)
        {
            reg_data |= BMCR_SPEED100;
        }
        else if (HAL_PHY_SPEED_10M == speed)
        {
            reg_data |= 0;
        }
        else
        {
            return AIR_E_NOT_SUPPORT;
        }

        rv = hal_mdio_writeC22ByPort(unit, port, MII_BMCR, reg_data);
    }

    return rv;
}

/* FUNCTION NAME:   hal_en8811h_phy_getSpeed
 * PURPOSE:
 *      This API is used to get port speed.
 * INPUT:
 *      unit            --  Device unit number
 *      port            --  Port number
 *      ptr_speed       --  Port speed
 *
 * OUTPUT:
 *
 * RETURN:
 *      AIR_E_OK
 *      AIR_E_BAD_PARAMETER
 *
 * NOTES:
 *      None
 */
AIR_ERROR_NO_T
hal_en8811h_phy_getSpeed(
    const UI32_T    unit,
    const UI32_T    port,
    HAL_PHY_SPEED_T *ptr_speed)
{
    UI16_T  reg_data = 0;
    AIR_ERROR_NO_T rv = AIR_E_OK;

    /* 2.5G, TODO */

    if ((rv = hal_mdio_readC22ByPort(unit, port, MII_BMCR, &reg_data)) != AIR_E_OK)
    {
        return rv;
    }

    if (reg_data & BMCR_SPEED1000)
    {
        *ptr_speed = HAL_PHY_SPEED_1000M;
    }
    else if (reg_data & BMCR_SPEED100)
    {
        *ptr_speed = HAL_PHY_SPEED_100M;
    }
    else
    {
        *ptr_speed = HAL_PHY_SPEED_10M;
    }
    return rv;
}

/* FUNCTION NAME:   hal_en8811h_phy_setDuplex
 * PURPOSE:
 *      This API is used to set port duplex.
 * INPUT:
 *      unit            --  Device unit number
 *      port            --  Port number
 *      duplex         --  Port duplex
 *
 * OUTPUT:
 *
 * RETURN:
 *      AIR_E_OK
 *      AIR_E_BAD_PARAMETER
 *
 * NOTES:
 *      None
 */
AIR_ERROR_NO_T
hal_en8811h_phy_setDuplex(
    const UI32_T            unit,
    const UI32_T            port,
    const HAL_PHY_DUPLEX_T  duplex)
{
    UI16_T          reg_data = 0;
    AIR_ERROR_NO_T  rv = AIR_E_OK;

    if (HAL_PHY_DUPLEX_HALF == duplex)
    {
        return AIR_E_BAD_PARAMETER;
    }

    rv = hal_mdio_readC22ByPort(unit, port, MII_BMCR, &reg_data);

    if (AIR_E_OK == rv)
    {
        if (HAL_PHY_DUPLEX_FULL == duplex)
        {
            reg_data |= BMCR_FULLDPLX;
        }
        else if (HAL_PHY_DUPLEX_HALF == duplex)
        {
            reg_data &= ~(BMCR_FULLDPLX);
        }
        else
        {
            return AIR_E_NOT_SUPPORT;
        }

        rv = hal_mdio_writeC22ByPort(unit, port, MII_BMCR, reg_data);
    }
    return rv;
}

/* FUNCTION NAME:   hal_en8811h_phy_getDuplex
 * PURPOSE:
 *      This API is used to get port duplex.
 * INPUT:
 *      unit            --  Device unit number
 *      port            --  Port number
 *      ptr_duplex      --  Port duplex
 *
 * OUTPUT:
 *
 * RETURN:
 *      AIR_E_OK
 *      AIR_E_BAD_PARAMETER
 *
 * NOTES:
 *      None
 */
AIR_ERROR_NO_T
hal_en8811h_phy_getDuplex(
    const UI32_T        unit,
    const UI32_T        port,
    HAL_PHY_DUPLEX_T    *ptr_duplex)
{
    UI16_T          reg_data = 0;
    AIR_ERROR_NO_T  rv = AIR_E_OK;

    rv = hal_mdio_readC22ByPort(unit, port, MII_AUX_CTRL_STA, &reg_data);
    if (AIR_E_OK == rv)
    {
        if (reg_data & AUX_FDX_STATUS)
        {
            *ptr_duplex = HAL_PHY_DUPLEX_FULL;
        }
        else
        {
            rv = hal_mdio_readC22ByPort(unit, port, MII_BMCR, &reg_data);

            if (AIR_E_OK == rv)
            {
                if (reg_data & BMCR_FULLDPLX)
                {
                    *ptr_duplex = HAL_PHY_DUPLEX_FULL;
                }
                else
                {
                    *ptr_duplex = HAL_PHY_DUPLEX_HALF;
                }
            }
        }
    }
    return rv;
}

/* FUNCTION NAME:   hal_en8811h_phy_getLinkStatus
 * PURPOSE:
 *      This API is used to get port link status.
 * INPUT:
 *      unit            --  Device unit number
 *      port            --  Port number
 *      ptr_status      --  Link Status
 *
 * OUTPUT:
 *
 * RETURN:
 *      AIR_E_OK
 *      AIR_E_BAD_PARAMETER
 *
 * NOTES:
 *      None
 */
AIR_ERROR_NO_T
hal_en8811h_phy_getLinkStatus(
    const UI32_T            unit,
    const UI32_T            port,
    HAL_PHY_LINK_STATUS_T   *ptr_status)
{
    UI16_T              reg_data = 0;
    AIR_ERROR_NO_T      rv = AIR_E_OK;
    HAL_PHY_AUTO_NEGO_T auto_nego;
    UI16_T              adv = 0, lpa = 0, lpagb = 0, common_adv_gb = 0, common_adv = 0;
    UI32_T              pbus_data = 0;

    rv = hal_mdio_readC22ByPort(0, port, MII_BMSR, &reg_data);
    if (AIR_E_OK == rv)
    {
        rv = hal_mdio_readC22ByPort(0, port, MII_BMSR, &reg_data);
        if (AIR_E_OK == rv)
        {
            SET_PHY_ABILITY(reg_data, BMSR_LSTATUS, HAL_PHY_LINK_STATUS_FLAGS_LINK_UP, ptr_status->flags);
            SET_PHY_ABILITY(reg_data, BMSR_ANEGCOMPLETE, HAL_PHY_LINK_STATUS_FLAGS_AUTO_NEGO_DONE, ptr_status->flags);
            SET_PHY_ABILITY(reg_data, BMSR_RFAULT, HAL_PHY_LINK_STATUS_FLAGS_REMOTE_FAULT, ptr_status->flags);
        }
    }

    if ((AIR_E_OK == rv) && (ptr_status->flags & HAL_PHY_LINK_STATUS_FLAGS_LINK_UP))
    {
        /* remove it when AN 2.5G, cl45 dev 7 reg 0x21 bit 5 is OK. */
        rv = _hal_en8811h_buck_pbus_regrd(port, 0x109D4, &pbus_data);
        if (AIR_E_OK == rv)
        {
            DIAG_PRINT(HAL_DBG_INFO, "[Dbg] en8811 port %d, REG 0x109D4, value = 0x%08X\n", port, pbus_data);
            if (pbus_data & HAL_EN8811H_LINK_SPEED_2500M)
            {
                ptr_status->speed = HAL_PHY_SPEED_2500M;
                ptr_status->duplex = HAL_PHY_DUPLEX_FULL;
            }
            else
            {
                rv = hal_en8811h_phy_getAutoNego(unit, port, &auto_nego);
                if ((AIR_E_OK == rv) && (HAL_PHY_AUTO_NEGO_ENABLE == auto_nego))
                {
                    rv = hal_mdio_readC22ByPort(unit, port, MII_STAT1000, &lpagb);
                    if (AIR_E_OK == rv)
                    {
                        rv = hal_mdio_readC22ByPort(unit, port, MII_CTRL1000, &adv);
                        if (AIR_E_OK == rv)
                        {
                            common_adv_gb = (lpagb & (adv << 2));
                            rv = hal_mdio_readC22ByPort(unit, port, MII_LPA, &lpa);
                            if (AIR_E_OK == rv)
                            {
                                rv = hal_mdio_readC22ByPort(unit, port, MII_ADVERTISE, &adv);
                                if (AIR_E_OK == rv)
                                {
                                    common_adv = lpa & adv;

                                    ptr_status->speed = HAL_PHY_SPEED_10M;
                                    ptr_status->duplex = HAL_PHY_DUPLEX_HALF;

                                    if (common_adv_gb & (LPA_1000FULL | LPA_1000HALF))
                                    {
                                        ptr_status->speed = HAL_PHY_SPEED_1000M;
                                        if (common_adv_gb & LPA_1000FULL)
                                        {
                                            ptr_status->duplex = HAL_PHY_DUPLEX_FULL;
                                        }
                                    }
                                    else if (common_adv & (LPA_100FULL | LPA_100HALF))
                                    {
                                        ptr_status->speed = HAL_PHY_SPEED_100M;
                                        if (common_adv & LPA_100FULL)
                                        {
                                            ptr_status->duplex = HAL_PHY_DUPLEX_FULL;
                                        }
                                    }
                                    else
                                    {
                                        if (common_adv & LPA_10FULL)
                                        {
                                            ptr_status->duplex = HAL_PHY_DUPLEX_FULL;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                else
                {
                   rv = hal_mdio_readC22ByPort(unit, port, MII_BMCR, &reg_data);
                   if (AIR_E_OK == rv)
                   {
                       if (reg_data & BMCR_FULLDPLX)
                       {
                           ptr_status->duplex = HAL_PHY_DUPLEX_FULL;
                       }
                       else
                       {
                           ptr_status->duplex = HAL_PHY_DUPLEX_HALF;
                       }
                       if (reg_data & BMCR_SPEED1000)
                       {
                           ptr_status->speed = HAL_PHY_SPEED_1000M;
                       }
                       else if (reg_data & BMCR_SPEED100)
                       {
                           ptr_status->speed = HAL_PHY_SPEED_100M;
                       }
                       else
                       {
                           ptr_status->speed = HAL_PHY_SPEED_10M;
                       }
                    }
                }
            }
        }
    }
    return rv;
}

/* FUNCTION NAME:   hal_en8811h_phy_setLoopBack
 * PURPOSE:
 *      This API is used to get port link status.
 * INPUT:
 *      unit            --  Device unit number
 *      port            --  Port number
 *      lpbk_type       --  LookBack type
 *      enable          --  mode enable/disable
 *
 * OUTPUT:
 *
 * RETURN:
 *      AIR_E_OK
 *      AIR_E_BAD_PARAMETER
 *
 * NOTES:
 *      None
 */
AIR_ERROR_NO_T
hal_en8811h_phy_setLoopBack(
    const UI32_T            unit,
    const UI32_T            port,
    const HAL_PHY_LPBK_T    lpbk_type,
    const BOOL_T            enable)
{
    UI16_T          reg_data = 0, page = 0;
    AIR_ERROR_NO_T  rv = AIR_E_OK;

    if(HAL_PHY_LPBK_FAR_END == lpbk_type)
    {
        /* Backup page */
        rv = hal_mdio_readC22ByPort(unit, port, MII_PAGE_SELECT, &page);
        if (AIR_E_OK == rv)
        {
            /* Swtich to page 1 */
            reg_data = 1;
            rv = hal_mdio_writeC22ByPort(unit, port, MII_PAGE_SELECT, reg_data);
            if (AIR_E_OK == rv)
            {
                /* Read data from ExtReg1A */
                rv = hal_mdio_readC22ByPort(unit, port, MII_RESV2, &reg_data);
                if (AIR_E_OK == rv)
                {
                    if (TRUE == enable)
                    {
                        reg_data |= LPBK_FAR_END;
                    }
                    else
                    {
                        reg_data &= ~(LPBK_FAR_END);
                    }

                    rv = hal_mdio_writeC22ByPort(unit, port, MII_RESV2, reg_data);
                    if (AIR_E_OK != rv)
                    {
                        hal_mdio_writeC22ByPort(unit, port, MII_PAGE_SELECT, page);
                        return rv;
                    }
                }

                if (AIR_E_OK == rv)
                {
                    rv = hal_mdio_writeC22ByPort(unit, port, MII_PAGE_SELECT, page);
                }
            }
            else
            {
                DIAG_PRINT(HAL_DBG_WARN, "[Dbg] en8811 %d switch to page %u fail \n", port, reg_data);
            }
        }
    }
    else
    {
        rv = hal_mdio_readC22ByPort(unit, port, MII_BMCR, &reg_data);
        if (AIR_E_OK == rv)
        {
            if (TRUE == enable)
            {
                reg_data |= BMCR_LOOPBACK;
            }
            else
            {
                reg_data &= ~(BMCR_LOOPBACK);
            }

            rv = hal_mdio_writeC22ByPort(unit, port, MII_BMCR, reg_data);
        }
    }

    return rv;
}

/* FUNCTION NAME:   hal_en8811h_phy_getLoopBack
 * PURPOSE:
 *      This API is used to get port link status.
 * INPUT:
 *      unit            --  Device unit number
 *      port            --  Port number
 *      lpbk_type       --  LookBack type
 *
 * OUTPUT:
 *      ptr_enable      --  mode enable/disable
 * RETURN:
 *      AIR_E_OK
 *      AIR_E_BAD_PARAMETER
 *
 * NOTES:
 *      None
 */
AIR_ERROR_NO_T
hal_en8811h_phy_getLoopBack(
    const UI32_T            unit,
    const UI32_T            port,
    const HAL_PHY_LPBK_T    lpbk_type,
    BOOL_T                  *ptr_enable)
{
    UI16_T          reg_data = 0, page = 0;
    AIR_ERROR_NO_T  rv = AIR_E_OK;

    if(HAL_PHY_LPBK_FAR_END == lpbk_type)
    {
        /* Backup page */
        rv = hal_mdio_readC22ByPort(unit, port, MII_PAGE_SELECT, &page);
        if (AIR_E_OK == rv)
        {
            /* Swtich to page 1 */
            reg_data = 1;
            rv = hal_mdio_writeC22ByPort(unit, port, MII_PAGE_SELECT, reg_data);
            if (AIR_E_OK == rv)
            {
                /* Read data from ExtReg1A */
                rv = hal_mdio_readC22ByPort(unit, port, MII_RESV2, &reg_data);
                if (AIR_E_OK == rv)
                {
                    if (reg_data & LPBK_FAR_END)
                    {
                        *ptr_enable = TRUE;
                    }
                    else
                    {
                        *ptr_enable = FALSE;
                    }
                }

                if (AIR_E_OK == rv)
                {
                    rv = hal_mdio_writeC22ByPort(unit, port, MII_PAGE_SELECT, page);
                }
            }
            else
            {
                DIAG_PRINT(HAL_DBG_WARN, "[Dbg] en8811 %d switch to page %u fail \n", port, reg_data);
            }
        }


        /* Restore page*/
        if ((rv = hal_mdio_writeC22ByPort(unit, port, MII_PAGE_SELECT, page)) != AIR_E_OK)
        {
            return rv;
        }
    }
    else
    {
        rv = hal_mdio_readC22ByPort(unit, port, MII_BMCR, &reg_data);
        if (AIR_E_OK == rv)
        {
            if (reg_data & BMCR_LOOPBACK)
            {
                *ptr_enable = TRUE;
            }
            else
            {
                *ptr_enable = FALSE;
            }
        }
    }

    return rv;
}

/* FUNCTION NAME:   hal_en8811h_phy_setSmartSpeedDown
 * PURPOSE:
 *      This API is used to gst port smart speed down.
 * INPUT:
 *      unit            --  Device unit number
 *      port            --  Port number
 *      ssd_mode        --  smart speed down mode
 *
 * OUTPUT:
 *
 * RETURN:
 *      AIR_E_OK
 *      AIR_E_BAD_PARAMETER
 *
 * NOTES:
 *      None
 */
AIR_ERROR_NO_T
hal_en8811h_phy_setSmartSpeedDown(
    const UI32_T                unit,
    const UI32_T                port,
    const HAL_PHY_SSD_MODE_T    ssd_mode)
{
    UI16_T          reg_data = 0, page = 0, data = 0;
    AIR_ERROR_NO_T  rv = AIR_E_OK;

    rv = hal_mdio_readC22ByPort(unit, port, MII_PAGE_SELECT, &page);
    if (AIR_E_OK == rv)
    {
        reg_data = 1;
        rv = hal_mdio_writeC22ByPort(unit, port, MII_PAGE_SELECT, reg_data);
        if (AIR_E_OK == rv)
        {
            rv = hal_mdio_readC22ByPort(unit, port, 0x14, &data);
            if (AIR_E_OK == rv)
            {
                data &= ~BITS(0,3);
                if (HAL_PHY_SSD_MODE_DISABLE != ssd_mode)
                {
                    data |= BIT(4);
                    data |= ssd_mode;
                }
                else
                {
                    data &= ~BIT(4);
                }

                rv = hal_mdio_writeC22ByPort(unit, port, 0x14, data);
                if (AIR_E_OK == rv)
                {
                    rv = hal_mdio_writeC22ByPort(unit, port, MII_PAGE_SELECT, page);
                }
            }
        }
    }
    return rv;
}

/* FUNCTION NAME:   hal_en8811h_phy_getSmartSpeedDown
 * PURPOSE:
 *      This API is used to get port power save.
 * INPUT:
 *      unit            --  Device unit number
 *      port            --  Port number
 *
 * OUTPUT:
 *      ptr_ssd_mode    --  smart speed down mode
 * RETURN:
 *      AIR_E_OK
 *      AIR_E_BAD_PARAMETER
 *
 * NOTES:
 *      None
 */
AIR_ERROR_NO_T
hal_en8811h_phy_getSmartSpeedDown(
    const UI32_T        unit,
    const UI32_T        port,
    HAL_PHY_SSD_MODE_T  *ptr_ssd_mode)
{
    UI16_T          reg_data = 0, page = 0, data = 0;
    AIR_ERROR_NO_T  rv = AIR_E_OK;

    /* Backup page */
    rv = hal_mdio_readC22ByPort(unit, port, MII_PAGE_SELECT, &page);
    if (AIR_E_OK == rv)
    {
        reg_data = 1;
        rv = hal_mdio_writeC22ByPort(unit, port, MII_PAGE_SELECT, reg_data);
        if (AIR_E_OK == rv)
        {
            rv = hal_mdio_readC22ByPort(unit, port, 0x14, &data);
            if (AIR_E_OK == rv)
            {
                reg_data = BITS_OFF_R(data, 4, 1);
                if (reg_data)
                {
                    *ptr_ssd_mode = (BITS_OFF_R(data, 0, 4));
                }
                else
                {
                    *ptr_ssd_mode = HAL_PHY_SSD_MODE_DISABLE;
                }

                rv = hal_mdio_writeC22ByPort(unit, port, MII_PAGE_SELECT, page);
            }
        }
    }
    return rv;
}

/* FUNCTION NAME:   hal_en8811h_phy_setLedOnCtrl
 * PURPOSE:
 *      This API is used to set control of port LED.
 * INPUT:
 *      unit            --  Device unit number
 *      port            --  Port number
 *      led_id          --  LED ID
 *      enable          --  FALSE:Disable
 *                          TRUE: Enable
 *
 * OUTPUT:
 *      None
 * RETURN:
 *      AIR_E_OK
 * NOTES:
 *      None
 */
AIR_ERROR_NO_T
hal_en8811h_phy_setLedOnCtrl(
    const UI32_T    unit,
    const UI32_T    port,
    const UI32_T    led_id,
    const BOOL_T    enable)
{
    UI16_T          reg_data = 0, reg_addr = 0;
    AIR_ERROR_NO_T  rv = AIR_E_OK;

    HAL_CHECK_PARAM((led_id >= HAL_EN8811H_MAX_LED_COUNT), AIR_E_BAD_PARAMETER);

    if (led_id == 0)
    {
        reg_addr = EN8811H_LED_0_ON_MASK;
    }
    else if (led_id == 1)
    {
        reg_addr = EN8811H_LED_1_ON_MASK;
    }
    else if (led_id == 2)
    {
        reg_addr = EN8811H_LED_2_ON_MASK;
    }
    else
    {
        reg_addr = EN8811H_LED_3_ON_MASK;
    }

    rv = hal_mdio_readC45ByPort(unit, port, MMD_DEV_VSPEC2, reg_addr, &reg_data);
    if (AIR_E_OK == rv)
    {
        if (TRUE == enable)
        {
            reg_data |= EN8811H_LED_LINK_FORCE_ON;
        }
        else
        {
            reg_data &= ~(EN8811H_LED_LINK_FORCE_ON);
        }

        rv = hal_mdio_writeC45ByPort(unit, port, MMD_DEV_VSPEC2, reg_addr, reg_data);
    }

    return rv;
}

/* FUNCTION NAME:   hal_en8811h_phy_getLedOnCtrl
 * PURPOSE:
 *      This API is used to get port LED control setting.
 * INPUT:
 *      unit            --  Device unit number
 *      port            --  Port number
 *      led_id          --  LED ID
 * OUTPUT:
 *      ptr_enable      --  FALSE:Disable
 *                          TRUE: Enable
 * RETURN:
 *      AIR_E_OK
 * NOTES:
 *      None
 */
AIR_ERROR_NO_T
hal_en8811h_phy_getLedOnCtrl(
    const UI32_T    unit,
    const UI32_T    port,
    const UI32_T    led_id,
    BOOL_T          *ptr_enable)
{
    UI16_T          reg_data = 0, reg_addr = 0;
    AIR_ERROR_NO_T  rv = AIR_E_OK;

    HAL_CHECK_PARAM((led_id >= HAL_EN8811H_MAX_LED_COUNT), AIR_E_BAD_PARAMETER);

    if (led_id == 0)
    {
        reg_addr = EN8811H_LED_0_ON_MASK;
    }
    else if (led_id == 1)
    {
        reg_addr = EN8811H_LED_1_ON_MASK;
    }
    else if (led_id == 2)
    {
        reg_addr = EN8811H_LED_2_ON_MASK;
    }
    else
    {
        reg_addr = EN8811H_LED_3_ON_MASK;
    }

    rv = hal_mdio_readC45ByPort(unit, port, MMD_DEV_VSPEC2, reg_addr, &reg_data);
    if (AIR_E_OK == rv)
    {
        if (reg_data & EN8811H_LED_LINK_FORCE_ON)
        {
            *ptr_enable = TRUE;
        }
        else
        {
            *ptr_enable = FALSE;
        }
    }
    return rv;
}

/* FUNCTION NAME:   hal_en8811h_phy_testTxCompliance
 * PURPOSE:
 *      This API is used to set the Tx compliance mode.
 * INPUT:
 *      unit            --  Device unit number
 *      port            --  Port number
 *      mode            --  BIST mode
 * OUTPUT:
 *      None
 * RETURN:
 *      AIR_E_OK
 * NOTES:
 *      None
 */
AIR_ERROR_NO_T
hal_en8811h_phy_testTxCompliance(
    const UI32_T                        unit,
    const UI32_T                        port,
    const HAL_PHY_TX_COMPLIANCE_MODE_T  mode)
{
    AIR_ERROR_NO_T ret = AIR_E_OK;
    ret = hal_cmn_phy_testTxCompliance(unit, port, mode);
    return ret;
}

/* FUNCTION NAME:   hal_en8811h_phy_setPhyLedCtrlMode
 * PURPOSE:
 *      This API is used to set phy led control mode.
 * INPUT:
 *      unit            --  Device unit number
 *      port            --  Port number
 *      led_id          --  LED Id
 *      ctrl_mode       --  LED control mode enumeration type
 * OUTPUT:
 *      None
 * RETURN:
 *      AIR_E_OK
 *      AIR_E_BAD_PARAMETER
 * NOTES:
 *      None
 */
AIR_ERROR_NO_T
hal_en8811h_phy_setPhyLedCtrlMode(
    const UI32_T                    unit,
    const UI32_T                    port,
    const UI32_T                    led_id,
    const HAL_PHY_LED_CTRL_MODE_T   ctrl_mode)
{
    UI16_T                  data = 0, led_config = 0, offset = 0;
    UI16_T                  link_reg_data = 0, blk_reg_data = 0, link_reg_addr = 0, blk_reg_addr = 0;
    AIR_ERROR_NO_T          rv = AIR_E_OK;
    BOOL_T                  is_force = FALSE;
    HAL_PHY_LED_STATE_T     state;
    AIR_CFG_VALUE_T         led_behavior;

    HAL_CHECK_PARAM((led_id >= HAL_EN8811H_MAX_LED_COUNT), AIR_E_BAD_PARAMETER);

    rv = hal_mdio_readC45ByPort(unit, port, MMD_DEV_VSPEC2, PHY_LED_CFG_REG, &data);
    if (AIR_E_OK == rv)
    {
        offset = (EN8811H_LED_0_CFG_CTRL_MODE_BIT + (EN8811H_LED_CFG_RG_OFFSET * led_id));
        data &= ~(BITS_OFF_L(EN8811H_LED_CFG_CTRL_MODE_MASK, offset, EN8811H_LED_CFG_CTRL_MODE_WIDTH));
        data |= BITS_OFF_L(ctrl_mode, offset, EN8811H_LED_CFG_CTRL_MODE_WIDTH);
        rv = hal_mdio_writeC45ByPort(unit, port, MMD_DEV_VSPEC2, PHY_LED_CFG_REG, data);
    }

    if (AIR_E_OK == rv)
    {
        is_force = (HAL_PHY_LED_CTRL_MODE_FORCE == ctrl_mode) ? TRUE : FALSE;
        if (TRUE == is_force)
        {
            rv = hal_en8811h_phy_getPhyLedForceState(unit, port, led_id, &state);
            if (AIR_E_OK == rv)
            {
                rv = hal_en8811h_phy_setPhyLedForceState(unit, port, led_id, state);
            }
        }
        else
        {
            osal_memset(&led_behavior, 0, sizeof(AIR_CFG_VALUE_T));
            led_behavior.value = 0xFFF;
            led_behavior.param0 = port;
            led_behavior.param1 = led_id;

            rv = hal_cfg_getValue(0, AIR_CFG_TYPE_PHY_LED_BEHAVIOR, &led_behavior);

            if (AIR_E_OK == rv)
            {
                led_config = led_behavior.value;

                link_reg_data = 0;
                blk_reg_data = 0;

                LED_ID_TO_LED_REG_ADDRESS(led_id, PHY_LED_TYPE_ON, link_reg_addr);
                LED_ID_TO_LED_REG_ADDRESS(led_id, PHY_LED_TYPE_BLK, blk_reg_addr);

                GET_PHY_ABILITY(led_config, EN8811H_LED_BHV_LINK_1000, EN8811H_LED_LINK_1000, link_reg_data);
                GET_PHY_ABILITY(led_config, EN8811H_LED_BHV_LINK_100, EN8811H_LED_LINK_100, link_reg_data);
                GET_PHY_ABILITY(led_config, EN8811H_LED_BHV_LINK_10, EN8811H_LED_LINK_10, link_reg_data);
                GET_PHY_ABILITY(led_config, EN8811H_LED_BHV_LINK_FULLDPLX, EN8811H_LED_LINK_FULLDPLX, link_reg_data);
                GET_PHY_ABILITY(led_config, EN8811H_LED_BHV_LINK_HALFDPLX, EN8811H_LED_LINK_HALFDPLX, link_reg_data);
                GET_PHY_ABILITY(led_config, EN8811H_LED_BHV_HIGH_ACTIVE, EN8811H_LED_POL_HIGH_ACT, link_reg_data);
                GET_PHY_ABILITY(led_config, EN8811H_LED_BHV_LINK_2500, EN8811H_LED_LINK_2500, link_reg_data);

                GET_PHY_ABILITY(led_config, EN8811H_LED_BHV_BLINK_TX_1000, EN8811H_LED_BLINK_TX_1000, blk_reg_data);
                GET_PHY_ABILITY(led_config, EN8811H_LED_BHV_BLINK_RX_1000, EN8811H_LED_BLINK_RX_1000, blk_reg_data);
                GET_PHY_ABILITY(led_config, EN8811H_LED_BHV_BLINK_TX_100, EN8811H_LED_BLINK_TX_100, blk_reg_data);
                GET_PHY_ABILITY(led_config, EN8811H_LED_BHV_BLINK_RX_100, EN8811H_LED_BLINK_RX_100, blk_reg_data);
                GET_PHY_ABILITY(led_config, EN8811H_LED_BHV_BLINK_TX_10, EN8811H_LED_BLINK_TX_10, blk_reg_data);
                GET_PHY_ABILITY(led_config, EN8811H_LED_BHV_BLINK_RX_10, EN8811H_LED_BLINK_RX_10, blk_reg_data);
                GET_PHY_ABILITY(led_config, EN8811H_LED_BHV_BLINK_TX_2500, EN8811H_LED_BLINK_TX_2500, blk_reg_data);
                GET_PHY_ABILITY(led_config, EN8811H_LED_BHV_BLINK_RX_2500, EN8811H_LED_BLINK_RX_2500, blk_reg_data);

                if (link_reg_data != 0 || blk_reg_data != 0)
                {
                    link_reg_data |= EN8811H_LED_FUNC_ENABLE;
                }

                DIAG_PRINT(HAL_DBG_INFO, "port %u, link_reg_addr 0x%02X, link_reg_data 0x%04X\n", port, link_reg_addr, link_reg_data);
                DIAG_PRINT(HAL_DBG_INFO, "port %u, blk_reg_addr 0x%02X, blk_reg_data 0x%04X\n", port, blk_reg_addr, blk_reg_data);

                rv = hal_mdio_writeC45ByPort(unit, port, MMD_DEV_VSPEC2, link_reg_addr, link_reg_data);
                if (AIR_E_OK == rv)
                {
                    rv = hal_mdio_writeC45ByPort(unit, port, MMD_DEV_VSPEC2, blk_reg_addr, blk_reg_data);
                    if (AIR_E_OK == rv)
                    {
                        rv = hal_mdio_writeC45ByPort(unit, port, MMD_DEV_VSPEC2, EN8811H_LED_BLINK_DURATION, EN8811H_LED_BLINK_RATE_DEFAULT);
                    }
                }
            }
        }
    }
    return rv;
}

/* FUNCTION NAME:   hal_en8811h_phy_getPhyLedCtrlMode
 * PURPOSE:
 *      This API is used to get phy led control mode.
 * INPUT:
 *      unit            --  Device unit number
 *      port            --  Port number
 *      led_id          --  LED Id
 * OUTPUT:
 *      ptr_ctrl_mode   --  LED control mode
 * RETURN:
 *      AIR_E_OK
 * NOTES:
 *      None
 */
AIR_ERROR_NO_T
hal_en8811h_phy_getPhyLedCtrlMode(
    const UI32_T                unit,
    const UI32_T                port,
    const UI32_T                led_id,
    HAL_PHY_LED_CTRL_MODE_T     *ptr_ctrl_mode)
{
    UI16_T          data = 0, offset = 0;;
    AIR_ERROR_NO_T  rv = AIR_E_OK;

    HAL_CHECK_PARAM((led_id >= HAL_EN8811H_MAX_LED_COUNT), AIR_E_BAD_PARAMETER);

    rv = hal_mdio_readC45ByPort(unit, port, MMD_DEV_VSPEC2, PHY_LED_CFG_REG, &data);
    if (AIR_E_OK == rv)
    {
        offset = (EN8811H_LED_0_CFG_CTRL_MODE_BIT + (EN8811H_LED_CFG_RG_OFFSET * led_id));
        *ptr_ctrl_mode = BITS_OFF_R(data, offset, EN8811H_LED_CFG_CTRL_MODE_WIDTH);
    }
    return rv;
}

/* FUNCTION NAME:   hal_en8811h_phy_setPhyLedForceState
 * PURPOSE:
 *      This API is used to set phy led force state.
 * INPUT:
 *      unit            --  Device unit number
 *      port            --  Port number
 *      led_id          --  LED Id
 *      state           --  LED force state enumeration type
 * OUTPUT:
 *      None
 * RETURN:
 *      AIR_E_OK
 *      AIR_E_BAD_PARAMETER
 * NOTES:
 *      None
 */
AIR_ERROR_NO_T
hal_en8811h_phy_setPhyLedForceState(
    const UI32_T                unit,
    const UI32_T                port,
    const UI32_T                led_id,
    const HAL_PHY_LED_STATE_T   state)
{
    UI16_T                      reg_data = 0, reg_addr = 0, data = 0, offset = 0;
    AIR_ERROR_NO_T              rv = AIR_E_OK;
    BOOL_T                      is_force = FALSE;
    HAL_PHY_LED_CTRL_MODE_T     ctrl_mode;
    HAL_PHY_LED_PATT_T          led_patt;

    HAL_CHECK_PARAM((led_id >= HAL_EN8811H_MAX_LED_COUNT), AIR_E_BAD_PARAMETER);

    rv = hal_mdio_readC45ByPort(unit, port, MMD_DEV_VSPEC2, PHY_LED_CFG_REG, &data);
    if (AIR_E_OK == rv)
    {
        offset = (EN8811H_LED_0_CFG_STATE_BIT + (EN8811H_LED_CFG_RG_OFFSET * led_id));
        data &= ~(BITS_OFF_L(EN8811H_LED_CFG_STATE_MASK, offset, EN8811H_LED_CFG_STATE_WIDTH));
        data |= BITS_OFF_L(state, offset, EN8811H_LED_CFG_STATE_WIDTH);
        rv = hal_mdio_writeC45ByPort(unit, port, MMD_DEV_VSPEC2, PHY_LED_CFG_REG, data);
    }

    if (AIR_E_OK == rv)
    {
        rv = hal_en8811h_phy_getPhyLedCtrlMode(unit, port, led_id, &ctrl_mode);
        if (AIR_E_OK == rv)
        {
            if (HAL_PHY_LED_CTRL_MODE_FORCE == ctrl_mode)
            {
                is_force = (HAL_PHY_LED_STATE_FORCE_PATT == state) ? TRUE : FALSE;
                if (TRUE == is_force)
                {
                    rv = hal_en8811h_phy_getPhyLedForcePattCfg(unit, port, led_id, &led_patt);
                    if (AIR_E_OK == rv)
                    {
                        rv = hal_en8811h_phy_setPhyLedForcePattCfg(unit, port, led_id, led_patt);
                    }
                }
                else
                {
                    (HAL_PHY_LED_STATE_ON == state ? (reg_data = EN8811H_LED_LINK_FORCE_ON) : (reg_data = 0));
                    LED_ID_TO_LED_REG_ADDRESS(led_id, PHY_LED_TYPE_ON, reg_addr);
                    rv = hal_mdio_writeC45ByPort(unit, port, MMD_DEV_VSPEC2, reg_addr, (reg_data | EN8811H_LED_FUNC_ENABLE));
                    if (AIR_E_OK == rv)
                    {
                        LED_ID_TO_LED_REG_ADDRESS(led_id, PHY_LED_TYPE_BLK, reg_addr);
                        rv = hal_mdio_writeC45ByPort(unit, port, MMD_DEV_VSPEC2, reg_addr, 0);
                    }
                }
            }
        }
    }
    return rv;
}

/* FUNCTION NAME:   hal_en8811h_phy_getPhyLedForceState
 * PURPOSE:
 *      This API is used to get phy led force state.
 * INPUT:
 *      unit            --  Device unit number
 *      port            --  Port number
 *      led_id          --  LED Id
 * OUTPUT:
 *      ptr_state       --  LED force state enumeration type
 * RETURN:
 *      AIR_E_OK
 * NOTES:
 *      None
 */
AIR_ERROR_NO_T
hal_en8811h_phy_getPhyLedForceState(
    const UI32_T            unit,
    const UI32_T            port,
    const UI32_T            led_id,
    HAL_PHY_LED_STATE_T     *ptr_state)
{
    UI16_T          data = 0, offset = 0;;
    AIR_ERROR_NO_T  rv = AIR_E_OK;

    HAL_CHECK_PARAM((led_id >= HAL_EN8811H_MAX_LED_COUNT), AIR_E_BAD_PARAMETER);

    rv = hal_mdio_readC45ByPort(unit, port, MMD_DEV_VSPEC2, PHY_LED_CFG_REG, &data);
    if (AIR_E_OK == rv)
    {
        offset = (EN8811H_LED_0_CFG_STATE_BIT + (EN8811H_LED_CFG_RG_OFFSET * led_id));
        *ptr_state = BITS_OFF_R(data, offset, EN8811H_LED_CFG_STATE_WIDTH);
    }
    return rv;
}

/* FUNCTION NAME:   hal_en8811h_phy_setPhyLedForcePattCfg
 * PURPOSE:
 *      This API is used to set phy led force pattern.
 * INPUT:
 *      unit            --  Device unit number
 *      port            --  Port number
 *      led_id          --  LED Id
 *      pattern         --  LED force pattern enumeration type
 * OUTPUT:
 *      None
 * RETURN:
 *      AIR_E_OK
 *      AIR_E_BAD_PARAMETER
 * NOTES:
 *      None
 */
AIR_ERROR_NO_T
hal_en8811h_phy_setPhyLedForcePattCfg(
    const UI32_T                unit,
    const UI32_T                port,
    const UI32_T                led_id,
    const HAL_PHY_LED_PATT_T    pattern)
{
    UI16_T                      reg_data = 0, data = 0, reg_addr = 0;
    AIR_ERROR_NO_T              rv = AIR_E_OK;
    HAL_PHY_LED_CTRL_MODE_T     ctrl_mode;

    HAL_CHECK_PARAM((led_id >= HAL_EN8811H_MAX_LED_COUNT), AIR_E_BAD_PARAMETER);

    switch (pattern)
    {
        case HAL_PHY_LED_PATT_HZ_HALF:
            reg_data = EN8811H_LED_BLINK_RATE_HZ_HALF;
            break;
        case HAL_PHY_LED_PATT_HZ_ONE:
            reg_data = EN8811H_LED_BLINK_RATE_HZ_ONE;
            break;
        case HAL_PHY_LED_PATT_HZ_TWO:
            reg_data = EN8811H_LED_BLINK_RATE_HZ_TWO;
            break;
        default:
            rv = AIR_E_BAD_PARAMETER;
            break;
    }

    if (AIR_E_OK == rv)
    {
        rv = hal_mdio_readC45ByPort(unit, port, MMD_DEV_VSPEC2, PHY_LED_CFG_REG, &data);
        if (AIR_E_OK == rv)
        {
            data &= ~(BITS_OFF_L(EN8811H_LED_CFG_PATT_MASK, EN8811H_LED_CFG_PATT_BIT, EN8811H_LED_CFG_PATT_WIDTH));
            data |= BITS_OFF_L(pattern, EN8811H_LED_CFG_PATT_BIT, EN8811H_LED_CFG_PATT_WIDTH);
            rv = hal_mdio_writeC45ByPort(unit, port, MMD_DEV_VSPEC2, PHY_LED_CFG_REG, data);
        }
    }

    if (AIR_E_OK == rv)
    {
        rv = hal_en8811h_phy_getPhyLedCtrlMode(unit, port, led_id, &ctrl_mode);
        if (AIR_E_OK == rv)
        {
            if (HAL_PHY_LED_CTRL_MODE_FORCE == ctrl_mode)
            {
                rv = hal_mdio_writeC45ByPort(unit, port, MMD_DEV_VSPEC2, EN8811H_LED_BLINK_DURATION, reg_data);
                if (AIR_E_OK == rv)
                {
                    LED_ID_TO_LED_REG_ADDRESS(led_id, PHY_LED_TYPE_BLK, reg_addr);
                    rv = hal_mdio_writeC45ByPort(unit, port, MMD_DEV_VSPEC2, reg_addr, EN8811H_LED_BLINK_FORCE);
                    if (AIR_E_OK == rv)
                    {
                        LED_ID_TO_LED_REG_ADDRESS(led_id, PHY_LED_TYPE_ON, reg_addr);
                        rv = hal_mdio_writeC45ByPort(unit, port, MMD_DEV_VSPEC2, reg_addr, EN8811H_LED_FUNC_ENABLE);
                    }
                }
            }
        }
    }
    return rv;
}

/* FUNCTION NAME:   hal_en8811h_phy_getPhyLedForcePattCfg
 * PURPOSE:
 *      This API is used to get phy led force pattern.
 * INPUT:
 *      unit            --  Device unit number
 *      port            --  Port number
 *      led_id          --  LED Id
 * OUTPUT:
 *      ptr_pattern     --  LED force pattern enumeration type
 * RETURN:
 *      AIR_E_OK
 * NOTES:
 *      None
 */
AIR_ERROR_NO_T
hal_en8811h_phy_getPhyLedForcePattCfg(
    const UI32_T            unit,
    const UI32_T            port,
    const UI32_T            led_id,
    HAL_PHY_LED_PATT_T      *ptr_pattern)
{
    UI16_T          data = 0;
    AIR_ERROR_NO_T  rv = AIR_E_OK;

    HAL_CHECK_PARAM((led_id >= HAL_EN8811H_MAX_LED_COUNT), AIR_E_BAD_PARAMETER);

    rv = hal_mdio_readC45ByPort(unit, port, MMD_DEV_VSPEC2, PHY_LED_CFG_REG, &data);
    if (AIR_E_OK == rv)
    {
        *ptr_pattern = BITS_OFF_R(data, EN8811H_LED_CFG_PATT_BIT, EN8811H_LED_CFG_PATT_WIDTH);
    }
    return rv;
}

/* FUNCTION NAME: hal_en8811h_phy_triggerCableTest
 * PURPOSE:
 *      Get cable status.
 *
 * INPUT:
 *      unit            --  Device ID
 *      port            --  Select port number
 *      test_pair       --  Select test pair
 *                          HAL_PHY_CABLE_TEST_PAIR_A
 *                          HAL_PHY_CABLE_TEST_PAIR_B
 *                          HAL_PHY_CABLE_TEST_PAIR_C
 *                          HAL_PHY_CABLE_TEST_PAIR_D
 *                          HAL_PHY_CABLE_TEST_PAIR_ALL
 *
 * OUTPUT:
 *      ptr_test_rslt   --  Cable diagnostic information
 *                          HAL_PHY_CABLE_TEST_RSLT_T
 *
 * RETURN:
 *      AIR_E_OK
 *      AIR_E_NOT_SUPPORT
 *      AIR_E_BAD_PARAMETER
 *
 * NOTES:
 *      Support cable diagnostic in speed 1G only.
 */
AIR_ERROR_NO_T
hal_en8811h_phy_triggerCableTest(
    const UI32_T                unit,
    const UI32_T                port,
    HAL_PHY_CABLE_TEST_PAIR_T  test_pair,
    HAL_PHY_CABLE_TEST_RSLT_T  *ptr_test_rslt)
{
    AIR_ERROR_NO_T ret = AIR_E_OK;

    ret = hal_cmn_phy_triggerCableTest(unit, port, test_pair, ptr_test_rslt);
    return ret;
}

/* FUNCTION NAME: hal_en8811h_phy_getCableTestRawData
 * PURPOSE:
 *      Get cable ec training 4 pair raw date.
 *
 * INPUT:
 *      unit                --  Device ID
 *      port                --  Select port number
 *      test_pair           --  Select test pair
 *
 * OUTPUT:
 *      pptr_raw_data_all   --  Cable diagnostic raw information
 *
 *
 * RETURN:
 *      AIR_E_OK
 *      AIR_E_NOT_SUPPORT
 *      AIR_E_BAD_PARAMETER
 *
 * NOTES:
 *      Support cable diagnostic dump pair information.
 */
AIR_ERROR_NO_T
hal_en8811h_phy_getCableTestRawData(
    const UI32_T                unit,
    const UI32_T                port,
    UI32_T                      **pptr_raw_data_all)
{
    AIR_ERROR_NO_T ret = AIR_E_OK;
    ret = hal_cmn_phy_getCableTestRawData(unit, port, pptr_raw_data_all);
    return ret;
}

/* FUNCTION NAME: hal_en8811h_phy_setPhyLedGlbCfg
 * PURPOSE:
 *      Set LED global configuration.
 * INPUT:
 *      unit                     -- Device ID
 *      port                     -- Port ID
 *      ptr_glb_cfg              -- Global configuration
 *                                  HAL_PHY_LED_GLB_CFG_T
 * OUTPUT:
 *      None
 * RETURN:
 *      AIR_E_OK                 -- Operation success.
 *      AIR_E_BAD_PARAMETER      -- Parameter is wrong.
 * NOTES:
 *      None
 */
AIR_ERROR_NO_T
hal_en8811h_phy_setPhyLedGlbCfg(
    const UI32_T            unit,
    const UI32_T            port,
    HAL_PHY_LED_GLB_CFG_T   *ptr_glb_cfg)
{
    AIR_ERROR_NO_T ret = AIR_E_OK;
    ret = hal_cmn_phy_setPhyLedGlbCfg(unit, port, ptr_glb_cfg);
    return ret;
}

/* FUNCTION NAME: hal_en8811h_phy_getPhyLedGlbCfg
 * PURPOSE:
 *      Get LED global configuration.
 * INPUT:
 *      unit                     -- Device ID
 *      port                     -- Port ID
 * OUTPUT:
 *      ptr_glb_cfg              -- Global configuration
 *                                  HAL_PHY_LED_GLB_CFG_T
 * RETURN:
 *      AIR_E_OK                 -- Operation success.
 *      AIR_E_BAD_PARAMETER      -- Parameter is wrong.
 * NOTES:
 *      None
 */
AIR_ERROR_NO_T
hal_en8811h_phy_getPhyLedGlbCfg(
    const UI32_T            unit,
    const UI32_T            port,
    HAL_PHY_LED_GLB_CFG_T   *ptr_glb_cfg)
{
    AIR_ERROR_NO_T ret = AIR_E_OK;
    ret = hal_cmn_phy_getPhyLedGlbCfg(unit, port, ptr_glb_cfg);
    return ret;
}

/* FUNCTION NAME: hal_en8811h_phy_setPhyLedBlkEvent
 * PURPOSE:
 *      Set LED blinking event combination.
 * INPUT:
 *      unit                     -- Device ID
 *      port                     -- Port ID
 *      led_id                   -- LED ID
 *      evt_flags                -- Blinking event combination
 *                                  Refer to HAL_PHY_LED_BLK_EVT_FLAGS_XXX
 * OUTPUT:
 *      None
 * RETURN:
 *      AIR_E_OK                 -- Operation success.
 *      AIR_E_BAD_PARAMETER      -- Parameter is wrong.
 * NOTES:
 *      None
 */
AIR_ERROR_NO_T
hal_en8811h_phy_setPhyLedBlkEvent(
    const UI32_T    unit,
    const UI32_T    port,
    const UI32_T    led_id,
    const UI32_T    evt_flags)
{
    HAL_CHECK_PARAM((led_id >= HAL_EN8811H_MAX_LED_COUNT), AIR_E_BAD_PARAMETER);

    AIR_ERROR_NO_T ret = AIR_E_OK;
    ret = hal_cmn_phy_setPhyLedBlkEvent(unit, port, led_id, evt_flags);
    return ret;
}

/* FUNCTION NAME: hal_en8811h_phy_getPhyLedBlkEvent
 * PURPOSE:
 *      Get LED blinking event combination.
 * INPUT:
 *      unit                     -- Device ID
 *      port                     -- Port ID
 *      led_id                   -- LED ID
 * OUTPUT:
 *      ptr_evt_flags            -- Blinking event combination
 *                                  Refer to HAL_PHY_LED_BLK_EVT_FLAGS_XXX
 * RETURN:
 *      AIR_E_OK                 -- Operation success.
 *      AIR_E_BAD_PARAMETER      -- Parameter is wrong.
 * NOTES:
 *      None
 */
AIR_ERROR_NO_T
hal_en8811h_phy_getPhyLedBlkEvent(
    const UI32_T    unit,
    const UI32_T    port,
    const UI32_T    led_id,
    UI32_T          *ptr_evt_flags)
{
    HAL_CHECK_PARAM((led_id >= HAL_EN8811H_MAX_LED_COUNT), AIR_E_BAD_PARAMETER);

    AIR_ERROR_NO_T ret = AIR_E_OK;
    ret = hal_cmn_phy_getPhyLedBlkEvent(unit, port, led_id, ptr_evt_flags);
    return ret;
}

/* FUNCTION NAME: hal_en8811h_phy_setPhyLedDuration
 * PURPOSE:
 *      Set LED duration
 * INPUT:
 *      unit                     -- Device ID
 *      port                     -- Port ID
 *      mode                     -- Duration mode
 *                                  HAL_PHY_LED_BLK_CTRL_MODE_T
 *      time                     -- Duration time, unit: ms
 * OUTPUT:
 *      None
 * RETURN:
 *      AIR_E_OK                 -- Operation success.
 *      AIR_E_BAD_PARAMETER      -- Parameter is wrong.
 * NOTES:
 *      None
 */
AIR_ERROR_NO_T
hal_en8811h_phy_setPhyLedDuration(
    const UI32_T                        unit,
    const UI32_T                        port,
    const HAL_PHY_LED_BLK_CTRL_MODE_T   mode,
    const UI32_T                        time)
{
    AIR_ERROR_NO_T ret = AIR_E_OK;
    ret = hal_cmn_phy_setPhyLedDuration(unit, port, mode, time);
    return ret;
}

/* FUNCTION NAME: hal_en8811h_phy_getPhyLedDuration
 * PURPOSE:
 *      Get LED duration
 * INPUT:
 *      unit                     -- Device ID
 *      port                     -- Port ID
 *      mode                     -- Duration mode
 *                                  HAL_PHY_LED_BLK_CTRL_MODE_T
 * OUTPUT:
 *      ptr_time                 -- Duration time, unit: ms
 * RETURN:
 *      AIR_E_OK                 -- Operation success.
 *      AIR_E_BAD_PARAMETER      -- Parameter is wrong.
 * NOTES:
 *      None
 */
AIR_ERROR_NO_T
hal_en8811h_phy_getPhyLedDuration(
    const UI32_T                        unit,
    const UI32_T                        port,
    const HAL_PHY_LED_BLK_CTRL_MODE_T   mode,
    UI32_T                              *ptr_time)
{
    AIR_ERROR_NO_T ret = AIR_E_OK;
    ret = hal_cmn_phy_getPhyLedDuration(unit, port, mode, ptr_time);
    return ret;
}

/* FUNCTION NAME: hal_en8811h_phy_dumpPhyPara
 * PURPOSE:
 *      Dump Phy parameters.
 * INPUT:
 *      unit                     -- Device ID
 *      port                     -- Port ID
 *
 * OUTPUT:
 *      None
 *
 * RETURN:
 *      AIR_E_OK                 -- Operation success.
 *      AIR_E_BAD_PARAMETER      -- Parameter is wrong.
 *      AIR_E_NOT_SUPPORT        -- Feature is not supported.
 * NOTES:
 *      None
 */
AIR_ERROR_NO_T
hal_en8811h_phy_dumpPhyPara(
    const UI32_T                        unit,
    const UI32_T                        port)
{
    AIR_ERROR_NO_T          rv = AIR_E_OK;
    UI16_T                  reg_data = 0;
    UI32_T                  pbus_data = 0;
    HAL_PHY_LINK_STATUS_T   link_status;

    osal_printf("\n=== cl22 ===\n");
    rv = hal_mdio_readC22ByPort(unit, port, MII_BMSR, &reg_data);
    if (AIR_E_OK == rv)
    {
        osal_printf("unit=%2u    port=%2u    reg-addr=%2u    reg-data=0x%04x\n", unit, port, MII_BMSR, reg_data);
    }
    rv = hal_mdio_readC22ByPort(unit, port, 0x1d, &reg_data);
    if (AIR_E_OK == rv)
    {
        osal_printf("unit=%2u    port=%2u    reg-addr=%2u    reg-data=0x%04x\n", unit, port, 0x1d, reg_data);
    }
    osal_printf("\n=== buck pbus reg ===\n");
    rv = _hal_en8811h_buck_pbus_regrd(port, 0x3b3c, &pbus_data);
    if (AIR_E_OK == rv)
    {
        osal_printf("port=%2u     en8811h version           = %x\n", port, pbus_data);
    }
    rv = _hal_en8811h_buck_pbus_regrd(port, 0xe0004, &pbus_data);
    if (AIR_E_OK == rv)
    {
        osal_printf("port=%2u     RG_SYS_LINK_MODE          = 0x%08x\n", port, pbus_data);
    }
    rv = _hal_en8811h_buck_pbus_regrd(port, 0xe000c, &pbus_data);
    if (AIR_E_OK == rv)
    {
        osal_printf("port=%2u     RG_FCM_CTRL               = 0x%08x\n", port, pbus_data);
    }
    rv = _hal_en8811h_buck_pbus_regrd(port, 0xe0020, &pbus_data);
    if (AIR_E_OK == rv)
    {
        osal_printf("port=%2u     RG_SS_PAUSE_TIME          = 0x%08x\n", port, pbus_data);
    }
    rv = _hal_en8811h_buck_pbus_regrd(port, 0xe002c, &pbus_data);
    if (AIR_E_OK == rv)
    {
        osal_printf("port=%2u     RG_MIN_IPG_NUM            = 0x%08x\n", port, pbus_data);
    }
    rv = _hal_en8811h_buck_pbus_regrd(port, 0xc0000, &pbus_data);
    if (AIR_E_OK == rv)
    {
        osal_printf("port=%2u     RG_CTROL_0                = 0x%08x\n", port, pbus_data);
    }
    rv = _hal_en8811h_buck_pbus_regrd(port, 0xc0b04, &pbus_data);
    if (AIR_E_OK == rv)
    {
        osal_printf("port=%2u     RG_LINK_STATUS            = 0x%08x\n", port, pbus_data);
    }
    rv = _hal_en8811h_buck_pbus_regrd(port, 0xc0014, &pbus_data);
    if (AIR_E_OK == rv)
    {
        osal_printf("port=%2u     RG_LINK_PARTNER_AN        = 0x%08x\n", port, pbus_data);
    }
    rv = _hal_en8811h_buck_pbus_regrd(port, 0x1020c, &pbus_data);
    if (AIR_E_OK == rv)
    {
        osal_printf("port=%2u     RG_FN_PWR_CTRL_STATUS     = 0x%08x\n", port, pbus_data);
    }
    rv = _hal_en8811h_buck_pbus_regrd(port, 0x3a48, &pbus_data);
    if (AIR_E_OK == rv)
    {
        osal_printf("port=%2u     RG_WHILE_LOOP_COUNT       = 0x%08x\n", port, pbus_data);
    }
    osal_printf("\n=== fcm counter ===\n");
    rv = _hal_en8811h_buck_pbus_regrd(port, 0xe0090, &pbus_data);
    if (AIR_E_OK == rv)
    {
        osal_printf("port=%2u     rx from line side_s       = 0x%08x\n", port, pbus_data);
    }
    rv = _hal_en8811h_buck_pbus_regrd(port, 0xe0094, &pbus_data);
    if (AIR_E_OK == rv)
    {
        osal_printf("port=%2u     rx from line side_t       = 0x%08x\n", port, pbus_data);
    }
    rv = _hal_en8811h_buck_pbus_regrd(port, 0xe009c, &pbus_data);
    if (AIR_E_OK == rv)
    {
        osal_printf("port=%2u     tx to system side_s       = 0x%08x\n", port, pbus_data);
    }
    rv = _hal_en8811h_buck_pbus_regrd(port, 0xe00a0, &pbus_data);
    if (AIR_E_OK == rv)
    {
        osal_printf("port=%2u     tx to system side_t       = 0x%08x\n", port, pbus_data);
    }
    rv = _hal_en8811h_buck_pbus_regrd(port, 0xe0078, &pbus_data);
    if (AIR_E_OK == rv)
    {
        osal_printf("port=%2u     rx from system side_s     = 0x%08x\n", port, pbus_data);
    }
    rv = _hal_en8811h_buck_pbus_regrd(port, 0xe007c, &pbus_data);
    if (AIR_E_OK == rv)
    {
        osal_printf("port=%2u     rx from system side_t     = 0x%08x\n", port, pbus_data);
    }
    rv = _hal_en8811h_buck_pbus_regrd(port, 0xe0084, &pbus_data);
    if (AIR_E_OK == rv)
    {
        osal_printf("port=%2u     tx to line side_s         = 0x%08x\n", port, pbus_data);
    }
    rv = _hal_en8811h_buck_pbus_regrd(port, 0xe0088, &pbus_data);
    if (AIR_E_OK == rv)
    {
        osal_printf("port=%2u     tx to line side_t         = 0x%08x\n", port, pbus_data);
    }
    rv = _hal_en8811h_buck_pbus_regwr(port, 0xe0074, 0xf);
    if (AIR_E_OK == rv)
    {
        rv = hal_en8811h_phy_getLinkStatus(unit, port, &link_status);
        if (AIR_E_OK == rv)
        {
            if ((link_status.flags & HAL_PHY_LINK_STATUS_FLAGS_LINK_UP) && (link_status.speed == HAL_PHY_SPEED_2500M))
            {
                osal_printf("\n=== line side counter ===\n");
                rv = _hal_en8811h_buck_pbus_regwr(port, 0x30718, 0x10);
                if (AIR_E_OK == rv)
                {
                    rv = _hal_en8811h_buck_pbus_regwr(port, 0x30718, 0x0);
                }
                if (AIR_E_OK == rv)
                {
                    osal_printf("\n=== before efifo ===\n");
                    rv = _hal_en8811h_buck_pbus_regrd(port, 0x3071c, &pbus_data);
                    if (AIR_E_OK == rv)
                    {
                        osal_printf("port=%2u     tx to line side_s         = 0x%08x\n", port, pbus_data);
                    }
                    rv = _hal_en8811h_buck_pbus_regrd(port, 0x30720, &pbus_data);
                    if (AIR_E_OK == rv)
                    {
                        osal_printf("port=%2u     tx to line side_t         = 0x%08x\n", port, pbus_data);
                    }
                    rv = _hal_en8811h_buck_pbus_regrd(port, 0x3072c, &pbus_data);
                    if (AIR_E_OK == rv)
                    {
                        osal_printf("port=%2u     rx from line side_t       = 0x%08x\n", port, pbus_data);
                    }
                    rv = _hal_en8811h_buck_pbus_regrd(port, 0x30730, &pbus_data);
                    if (AIR_E_OK == rv)
                    {
                        osal_printf("port=%2u     rx from line side_t       = 0x%08x\n", port, pbus_data);
                    }
                    rv = _hal_en8811h_buck_pbus_regrd(port, 0x30724, &pbus_data);
                    if (AIR_E_OK == rv)
                    {
                        osal_printf("port=%2u     tx_enc                    = 0x%08x\n", port, pbus_data);
                    }
                    rv = _hal_en8811h_buck_pbus_regrd(port, 0x30728, &pbus_data);
                    if (AIR_E_OK == rv)
                    {
                        osal_printf("port=%2u     rx_enc                    = 0x%08x\n", port, pbus_data);
                    }
                    osal_printf("\n=== after efifo ===\n");
                    rv = _hal_en8811h_buck_pbus_regrd(port, 0x30734, &pbus_data);
                    if (AIR_E_OK == rv)
                    {
                        osal_printf("port=%2u     tx to line side_s         = 0x%08x\n", port, pbus_data);
                    }
                    rv = _hal_en8811h_buck_pbus_regrd(port, 0x30738, &pbus_data);
                    if (AIR_E_OK == rv)
                    {
                        osal_printf("port=%2u     tx to line side_t         = 0x%08x\n", port, pbus_data);
                    }
                    rv = _hal_en8811h_buck_pbus_regrd(port, 0x30764, &pbus_data);
                    if (AIR_E_OK == rv)
                    {
                        osal_printf("port=%2u     rx from line side_t       = 0x%08x\n", port, pbus_data);
                    }
                    rv = _hal_en8811h_buck_pbus_regrd(port, 0x30768, &pbus_data);
                    if (AIR_E_OK == rv)
                    {
                        osal_printf("port=%2u     rx from line side_t       = 0x%08x\n", port, pbus_data);
                    }
                    rv = _hal_en8811h_buck_pbus_regwr(port, 0x30718, 0x13);
                    if (AIR_E_OK == rv)
                    {
                        rv = _hal_en8811h_buck_pbus_regwr(port, 0x30718, 0x3);
                        if (AIR_E_OK == rv)
                        {
                            rv = _hal_en8811h_buck_pbus_regwr(port, 0x30718, 0x10);
                            if (AIR_E_OK == rv)
                            {
                                rv = _hal_en8811h_buck_pbus_regwr(port, 0x30718, 0);
                            }
                        }
                    }

                    if (AIR_E_OK == rv)
                    {
                        osal_printf("\n=== mac counter ===\n");
                        rv = _hal_en8811h_buck_pbus_regrd(port, 0x131000, &pbus_data);
                        if (AIR_E_OK == rv)
                        {
                            osal_printf("port=%2u     tx error from system side = 0x%08x\n", port, pbus_data);
                        }
                        rv = _hal_en8811h_buck_pbus_regrd(port, 0x132000, &pbus_data);
                        if (AIR_E_OK == rv)
                        {
                            osal_printf("port=%2u     rx error to system side   = 0x%08x\n", port, pbus_data);
                        }
                        rv = _hal_en8811h_buck_pbus_regrd(port, 0x131004, &pbus_data);
                        if (AIR_E_OK == rv)
                        {
                            osal_printf("port=%2u     tx from system side       = 0x%08x\n", port, pbus_data);
                        }
                        rv = _hal_en8811h_buck_pbus_regrd(port, 0x132004, &pbus_data);
                        if (AIR_E_OK == rv)
                        {
                            osal_printf("port=%2u     rx to system side         = 0x%08x\n", port, pbus_data);
                        }
                    }
                }
            }
            else if ((link_status.flags & HAL_PHY_LINK_STATUS_FLAGS_LINK_UP)
                  && (link_status.speed != HAL_PHY_SPEED_2500M))
            {
                osal_printf("\n=== line side counter ===\n");
                rv = hal_mdio_writeC22ByPort(unit, port, 0x1f, 1);
                if (AIR_E_OK == rv)
                {
                    rv = hal_mdio_readC22ByPort(unit, port, 0x12, &reg_data);
                    if (AIR_E_OK == rv)
                    {
                        osal_printf("port=%2u     rx from line side         = 0x%08x\n", port, (reg_data & 0x7fff));
                    }
                    rv = hal_mdio_readC22ByPort(unit, port, 0x17, &reg_data);
                    if (AIR_E_OK == rv)
                    {
                        osal_printf("port=%2u     rx error from line side   = 0x%08x\n", port, (reg_data & 0xff));
                    }
                    rv = hal_mdio_writeC22ByPort(unit, port, 0x1f, 0);
                    if (AIR_E_OK == rv)
                    {
                        rv = hal_mdio_writeC22ByPort(unit, port, 0x1f, 0x52b5);
                        if (AIR_E_OK == rv)
                        {
                            rv = hal_mdio_writeC22ByPort(unit, port, 0x1f, 0xbf92);
                            if (AIR_E_OK == rv)
                            {
                                rv = hal_mdio_readC22ByPort(unit, port, 0x11, &reg_data);
                                if (AIR_E_OK == rv)
                                {
                                    osal_printf("port=%2u     tx from line side         = 0x%08x\n", port,
                                                ((reg_data & 0x7ffe) >> 1));
                                }
                                rv = hal_mdio_readC22ByPort(unit, port, 0x12, &reg_data);
                                if (AIR_E_OK == rv)
                                {
                                    osal_printf("port=%2u     tx from line side         = 0x%08x\n", port,
                                                ((reg_data & 0x7f)));
                                }
                                rv = hal_mdio_writeC22ByPort(unit, port, 0x1f, 0);
                            }
                        }
                    }
                }
            }

            if ((link_status.flags & HAL_PHY_LINK_STATUS_FLAGS_LINK_UP) && (AIR_E_OK == rv))
            {
                osal_printf("\n=== system side counter ===\n");
                rv = _hal_en8811h_buck_pbus_regwr(port, 0xc602c, 0x3);
                if (AIR_E_OK == rv)
                {
                    rv = _hal_en8811h_buck_pbus_regrd(port, 0xc60b0, &pbus_data);
                    if (AIR_E_OK == rv)
                    {
                        osal_printf("port=%2u     tx start                  = 0x%08x\n", port, pbus_data);
                    }
                    rv = _hal_en8811h_buck_pbus_regrd(port, 0xc60b4, &pbus_data);
                    if (AIR_E_OK == rv)
                    {
                        osal_printf("port=%2u     tx terminal               = 0x%08x\n", port, pbus_data);
                    }
                    rv = _hal_en8811h_buck_pbus_regrd(port, 0xc60bc, &pbus_data);
                    if (AIR_E_OK == rv)
                    {
                        osal_printf("port=%2u     rx start                  = 0x%08x\n", port, pbus_data);
                    }
                    rv = _hal_en8811h_buck_pbus_regrd(port, 0xc60c0, &pbus_data);
                    if (AIR_E_OK == rv)
                    {
                        osal_printf("port=%2u     rx terminal               = 0x%08x\n", port, pbus_data);
                    }
                    rv = _hal_en8811h_buck_pbus_regwr(port, 0xc602c, 0x4);
                }
            }
        }
    }
    return rv;
}

HAL_PHY_DRIVER_T
_ext_EN8811H_phy_func_vec =
{
    /* note: if function not support, fill "NULL". */
    hal_en8811h_phy_init,
    hal_en8811h_phy_setAdminState,
    hal_en8811h_phy_getAdminState,
    hal_en8811h_phy_setAutoNego,
    hal_en8811h_phy_getAutoNego,
    hal_en8811h_phy_setLocalAdvAbility,
    hal_en8811h_phy_getLocalAdvAbility,
    hal_en8811h_phy_getRemoteAdvAbility,
    hal_en8811h_phy_setSpeed,
    hal_en8811h_phy_getSpeed,
    hal_en8811h_phy_setDuplex,
    hal_en8811h_phy_getDuplex,
    hal_en8811h_phy_getLinkStatus,
    hal_en8811h_phy_setLoopBack,
    hal_en8811h_phy_getLoopBack,
    hal_en8811h_phy_setSmartSpeedDown,
    hal_en8811h_phy_getSmartSpeedDown,
    hal_en8811h_phy_setLedOnCtrl,
    hal_en8811h_phy_getLedOnCtrl,
    hal_en8811h_phy_testTxCompliance,
    NULL,
    NULL,
    NULL,
    NULL,
    hal_en8811h_phy_setPhyLedCtrlMode,
    hal_en8811h_phy_getPhyLedCtrlMode,
    hal_en8811h_phy_setPhyLedForceState,
    hal_en8811h_phy_getPhyLedForceState,
    hal_en8811h_phy_setPhyLedForcePattCfg,
    hal_en8811h_phy_getPhyLedForcePattCfg,
    hal_en8811h_phy_triggerCableTest,
    hal_en8811h_phy_getCableTestRawData,
    hal_en8811h_phy_setPhyLedGlbCfg,
    hal_en8811h_phy_getPhyLedGlbCfg,
    hal_en8811h_phy_setPhyLedBlkEvent,
    hal_en8811h_phy_getPhyLedBlkEvent,
    hal_en8811h_phy_setPhyLedDuration,
    hal_en8811h_phy_getPhyLedDuration,
    NULL,
    NULL,
    hal_en8811h_phy_dumpPhyPara,
};

AIR_ERROR_NO_T
hal_en8811h_phy_getDriver(
    HAL_PHY_DRIVER_T **pptr_hal_driver)
{
    (*pptr_hal_driver) = (HAL_PHY_DRIVER_T *)&_ext_EN8811H_phy_func_vec;

    return (AIR_E_OK);
}
