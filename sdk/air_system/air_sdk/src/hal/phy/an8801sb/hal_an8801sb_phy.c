/*******************************************************************************
*  Copyright Statement:
*  --------------------
*  This software is protected by Copyright and the information contained
*  herein is confidential. The software may not be copied and the information
*  contained herein may not be used or disclosed except with the written
*  permission of Airoha Technology Corp. (C) 2021
*
*  BY OPENING THIS FILE, BUYER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
*  THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("AIROHA SOFTWARE")
*  RECEIVED FROM AIROHA AND/OR ITS REPRESENTATIVES ARE PROVIDED TO BUYER ON
*  AN "AS-IS" BASIS ONLY. AIROHA EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
*  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
*  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
*  NEITHER DOES AIROHA PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
*  SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
*  SUPPLIED WITH THE AIROHA SOFTWARE, AND BUYER AGREES TO LOOK ONLY TO SUCH
*  THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. AIROHA SHALL ALSO
*  NOT BE RESPONSIBLE FOR ANY AIROHA SOFTWARE RELEASES MADE TO BUYER'S
*  SPECIFICATION OR TO CONFORM TO A PARTICULAR STANDARD OR OPEN FORUM.
*
*  BUYER'S SOLE AND EXCLUSIVE REMEDY AND AIROHA'S ENTIRE AND CUMULATIVE
*  LIABILITY WITH RESPECT TO THE AIROHA SOFTWARE RELEASED HEREUNDER WILL BE,
*  AT AIROHA'S OPTION, TO REVISE OR REPLACE THE AIROHA SOFTWARE AT ISSUE,
*  OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY BUYER TO
*  AIROHA FOR SUCH AIROHA SOFTWARE AT ISSUE.
*
*  THE TRANSACTION CONTEMPLATED HEREUNDER SHALL BE CONSTRUED IN ACCORDANCE
*  WITH THE LAWS OF THE STATE OF CALIFORNIA, USA, EXCLUDING ITS CONFLICT OF
*  LAWS PRINCIPLES.  ANY DISPUTES, CONTROVERSIES OR CLAIMS ARISING THEREOF AND
*  RELATED THERETO SHALL BE SETTLED BY ARBITRATION IN SAN FRANCISCO, CA, UNDER
*  THE RULES OF THE INTERNATIONAL CHAMBER OF COMMERCE (ICC).
*
*******************************************************************************/

/* FILE NAME:  hal_an8801sb_phy.c
 * PURPOSE:
 *  Implement an8801sb phy module HAL function.
 *
 * NOTES:
 *
 */

/* INCLUDE FILE DECLARTIONS
*/
#include <hal/common/hal.h>
#include <hal/common/hal_phy.h>
#include <hal/common/hal_cmn_phy.h>
#include <hal/common/hal_mdio.h>
#include <hal/common/hal_cfg.h>
#include <hal/phy/an8801sb/hal_an8801sb_phy.h>
#include <cmlib/cmlib_bit.h>

/* NAMING CONSTANT DECLARATIONS
*/
#define PHY_LED_BLK_CFG_REG             (AN8801SB_LED_3_ON_MASK)
#define PHY_LED_CFG_REG                 (AN8801SB_LED_3_BLK_MASK)

#define PHY_LED_TYPE_ON                 (0)
#define PHY_LED_TYPE_BLK                (1)

#define DEFAULT_LED_CFG     (AN8801SB_LED_BHV_LINK_1000 | AN8801SB_LED_BHV_LINK_100 | AN8801SB_LED_BHV_LINK_10 | \
                             AN8801SB_LED_BHV_BLINK_TX_1000 | AN8801SB_LED_BLINK_RX_1000 | \
                             AN8801SB_LED_BHV_BLINK_TX_100 | AN8801SB_LED_BLINK_RX_100 | \
                             AN8801SB_LED_BHV_BLINK_TX_10 | AN8801SB_LED_BHV_BLINK_RX_10)
#define MAX_SGMII_AN_RETRY              (100)

/* MACRO FUNCTION DECLARATIONS
 */
#define SET_PHY_ABILITY(flag, cod, sb, result)    do    \
    {                                                   \
        if (flag & cod)                                 \
        {                                               \
            result |= sb;                               \
        }                                               \
        else                                            \
        {                                               \
            result &= ~(sb);                            \
        }                                               \
    }while(0)

#define GET_PHY_ABILITY(reg, cod, sb, result)    do     \
    {                                                   \
        if (reg & cod)                                  \
        {                                               \
            result |= sb;                               \
        }                                               \
    }while(0)

#define LED_ID_TO_LED_REG_ADDRESS(led_id, led_type, reg_addr) do    \
    {                                                               \
        ((led_type) == PHY_LED_TYPE_ON ?                            \
            ((reg_addr) = AN8801SB_LED_0_ON_MASK) :                 \
            ((reg_addr) = AN8801SB_LED_0_BLK_MASK));                \
        (reg_addr) += (AN8801SB_LED_RG_OFFSET * led_id);            \
    }while(0)
/* DATA TYPE DECLARATIONS
*/

/* GLOBAL VARIABLE DECLARATIONS
*/
DIAG_SET_MODULE_INFO(AIR_MODULE_PHY, "hal_an8801sb_phy.c");

/* STATIC VARIABLE DECLARATIONS */


/* LOCAL SUBPROGRAM SPECIFICATIONS
*/
static UI32_T _hal_an8801sb_phy_readBuckPbus(UI16_T port, UI32_T pbus_addr)
{
    UI32_T          pbus_data = 0;
    UI16_T          pbus_data_low, pbus_data_high;
    AIR_ERROR_NO_T  rv = AIR_E_OK;

    rv |= hal_mdio_writeC22ByPort(0, port, 0x1F, 0x4);
    rv |= hal_mdio_writeC22ByPort(0, port, 0x10, 0x0);
    rv |= hal_mdio_writeC22ByPort(0, port, 0x15, (UI32_T)((pbus_addr >> 16) & 0xffff));
    rv |= hal_mdio_writeC22ByPort(0, port, 0x16, (UI32_T)(pbus_addr & 0xffff));

    rv |= hal_mdio_readC22ByPort(0, port, 0x17, &pbus_data_high);
    rv |= hal_mdio_readC22ByPort(0, port, 0x18, &pbus_data_low);

    pbus_data = (pbus_data_high << 16) + pbus_data_low;
    return pbus_data;
}

static void _hal_an8801sb_phy_writeBuckPbus(UI16_T port, UI32_T pbus_addr, UI32_T pbus_data)
{
    AIR_ERROR_NO_T  rv = AIR_E_OK;

    rv |= hal_mdio_writeC22ByPort(0, port, 0x1F, 0x4);
    rv |= hal_mdio_writeC22ByPort(0, port, 0x10, 0x0);
    rv |= hal_mdio_writeC22ByPort(0, port, 0x11, (UI32_T)((pbus_addr >> 16) & 0xffff));
    rv |= hal_mdio_writeC22ByPort(0, port, 0x12, (UI32_T)(pbus_addr & 0xffff));
    rv |= hal_mdio_writeC22ByPort(0, port, 0x13, (UI32_T)((pbus_data >> 16) & 0xffff));
    rv |= hal_mdio_writeC22ByPort(0, port, 0x14, (UI32_T)(pbus_data & 0xffff));
}

static AIR_ERROR_NO_T
_hal_an8801sb_updatePhyLink(UI16_T port, HAL_PHY_LINK_STATUS_T *ptr_status)
{
    AIR_ERROR_NO_T  rv = AIR_E_OK;
    UI16_T          status = 0;

    /* Do a fake read */
    rv = hal_mdio_readC22ByPort(0, port, MII_BMSR, &status);
    if (AIR_E_OK != rv)
    {
        return rv;
    }
    /* Read link and autonegotiation status */
    rv = hal_mdio_readC22ByPort(0, port, MII_BMSR, &status);
    if (AIR_E_OK != rv)
    {
        return rv;
    }

    if (status & BMSR_LSTATUS)
    {
        ptr_status->flags |= HAL_PHY_LINK_STATUS_FLAGS_LINK_UP;
    }
    else
    {
        ptr_status->flags &= ~(HAL_PHY_LINK_STATUS_FLAGS_LINK_UP);
    }
    if (status & BMSR_ANEGCOMPLETE)
    {
        ptr_status->flags |= HAL_PHY_LINK_STATUS_FLAGS_AUTO_NEGO_DONE;
    }
    else
    {
        ptr_status->flags &= ~(HAL_PHY_LINK_STATUS_FLAGS_AUTO_NEGO_DONE);
    }
    if (status & BMSR_RFAULT)
    {
        ptr_status->flags |= HAL_PHY_LINK_STATUS_FLAGS_REMOTE_FAULT;
    }
    else
    {
        ptr_status->flags &= ~(HAL_PHY_LINK_STATUS_FLAGS_REMOTE_FAULT);
    }
    return rv;
}

static AIR_ERROR_NO_T
_hal_an8801sb_readGenphyStatus(
    const UI32_T            unit,
    const UI32_T            port,
    HAL_PHY_LINK_STATUS_T   *ptr_status)
{
    AIR_ERROR_NO_T          rv = AIR_E_OK;
    HAL_PHY_AUTO_NEGO_T     auto_nego;
    UI16_T                  adv = 0, lpa = 0, lpagb = 0, bmcr = 0, common_adv_gb = 0, common_adv = 0;

    rv = _hal_an8801sb_updatePhyLink(port, ptr_status);
    if (AIR_E_OK != rv)
    {
        return rv;
    }
    hal_an8801sb_phy_getAutoNego(unit, port, &auto_nego);

    if (HAL_PHY_AUTO_NEGO_ENABLE == auto_nego)
    {
        rv = hal_mdio_readC22ByPort(0, port, MII_STAT1000, &lpagb);
        if (AIR_E_OK != rv)
        {
            return rv;
        }
        rv = hal_mdio_readC22ByPort(0, port, MII_CTRL1000, &adv);
        if (AIR_E_OK != rv)
        {
            return rv;
        }
        common_adv_gb = lpagb & adv << 2;

        rv = hal_mdio_readC22ByPort(0, port, MII_LPA, &lpa);
        if (AIR_E_OK != rv)
        {
            return rv;
        }
        rv = hal_mdio_readC22ByPort(0, port, MII_ADVERTISE, &adv);
        if (AIR_E_OK != rv)
        {
            return rv;
        }
        common_adv = lpa & adv;

        ptr_status->speed = HAL_PHY_SPEED_10M;
        ptr_status->duplex = HAL_PHY_DUPLEX_HALF;
        /*phydev->pause = phydev->asym_pause = 0; */

        if (common_adv_gb & (LPA_1000FULL | LPA_1000HALF))
        {
            ptr_status->speed = HAL_PHY_SPEED_1000M;
            if (common_adv_gb & LPA_1000FULL)
            {
                ptr_status->duplex = HAL_PHY_DUPLEX_FULL;
            }
        }
        else if (common_adv & (LPA_100FULL | LPA_100HALF))
        {
            ptr_status->speed = HAL_PHY_SPEED_100M;
            if (common_adv & LPA_100FULL)
            {
                ptr_status->duplex = HAL_PHY_DUPLEX_FULL;
            }
        }
        else
        {
            if (common_adv & LPA_10FULL)
            {
                ptr_status->duplex = HAL_PHY_DUPLEX_FULL;
            }
        }
        /*
        if (ptr_status->duplex == DUPLEX_FULL)
        {
            phydev->pause = lpa & LPA_PAUSE_CAP ? 1 : 0;
            phydev->asym_pause = lpa & LPA_PAUSE_ASYM ? 1 : 0;
        }
        */
    }
    else
    {
        rv = hal_mdio_readC22ByPort(0, port, MII_BMCR, &bmcr);
        if (AIR_E_OK != rv)
        {
            return rv;
        }
        if (bmcr & BMCR_FULLDPLX)
        {
            ptr_status->duplex = HAL_PHY_DUPLEX_FULL;
        }
        else
        {
            ptr_status->duplex = HAL_PHY_DUPLEX_HALF;
        }
        if (bmcr & BMCR_SPEED1000)
        {
            ptr_status->speed = HAL_PHY_SPEED_1000M;
        }
        else if (bmcr & BMCR_SPEED100)
        {
            ptr_status->speed = HAL_PHY_SPEED_100M;
        }
        else
        {
            ptr_status->speed = HAL_PHY_SPEED_10M;
        }
        /* phydev->pause = phydev->asym_pause = 0; */
    }
    return rv;
}


/* EXPORTED SUBPROGRAM BODIES*/

/* FUNCTION NAME: hal_an8801sb_phy_init
 * PURPOSE:
 *      AN8801SB PHY initialization
 *
 * INPUT:
 *      unit            --  Device ID
 *      port            --  PHY address
 *
 * OUTPUT:
 *        None
 *
 * RETURN:
 *        AIR_E_OK
 *        AIR_E_TIMEOUT
 *
 * NOTES:
 *      None
 */
AIR_ERROR_NO_T
hal_an8801sb_phy_init(
    const UI32_T    unit,
    const UI32_T    port)
{
    AIR_ERROR_NO_T  rv = AIR_E_OK;
    UI32_T          pbus_data;
    AIR_CFG_VALUE_T led_behavior;
    UI16_T          led_id = 0, led_config = 0, led_count = 0;
    UI16_T          link_reg_data = 0, blk_reg_data = 0, link_reg_addr = 0, blk_reg_addr = 0;
    HAL_PHY_LED_CTRL_MODE_T led_ctrl_mode;
    
    /* disable LPM */
    rv |= hal_mdio_writeC45ByPort(unit, port, MMD_DEV_VSPEC2, 0x600, 0x1e);
    rv |= hal_mdio_writeC45ByPort(unit, port, MMD_DEV_VSPEC2, 0x601, 0x02);

    /* IOMUX for LAN LED */
    //pbus_data = _hal_an8801sb_phy_readBuckPbus(port, AN8801SB_RG_FORCE_GPIO0_EN);
    //pbus_data &= ~(0x3);
    //_hal_an8801sb_phy_writeBuckPbus(port, AN8801SB_RG_FORCE_GPIO0_EN, pbus_data);
    _hal_an8801sb_phy_writeBuckPbus(port, AN8801SB_RG_GPIO_LAN0_LED0_MODE, 0x3);
    //_hal_an8801sb_phy_writeBuckPbus(port, AN8801SB_RG_GPIO_LAN0_LED0_SEL, 0x08);
    _hal_an8801sb_phy_writeBuckPbus(port, AN8801SB_RG_GPIO_LAN0_LED0_SEL, 0x00);
    _hal_an8801sb_phy_writeBuckPbus(port, AN8801SB_RG_GPIO_LED_INV, 0x01);
    /* LED 0 & 1 driving for 4 MA */
    pbus_data = _hal_an8801sb_phy_readBuckPbus(port, AN8801SB_RG_GPIO_DRIVING_E2);
    pbus_data &= ~(0x3);
    _hal_an8801sb_phy_writeBuckPbus(port, AN8801SB_RG_GPIO_DRIVING_E2, pbus_data);

    osal_memset(&led_behavior, 0, sizeof(AIR_CFG_VALUE_T));
    led_behavior.value = AN8801SB_LED_COUNT;
    hal_cfg_getValue(0, AIR_CFG_TYPE_PHY_LED_COUNT, &led_behavior);
    led_count = led_behavior.value;

    /* LED blink frequence */
    rv |= hal_mdio_writeC45ByPort(unit, port, MMD_DEV_VSPEC2, 0x21, 0x8008);
    rv |= hal_mdio_writeC45ByPort(unit, port, MMD_DEV_VSPEC2, 0x22, 0x600);
    rv |= hal_mdio_writeC45ByPort(unit, port, MMD_DEV_VSPEC2, 0x23, 0xc00);

    /* LED configuration */
    for(led_id = 0; led_id < led_count; led_id++)
    {
        if (led_id == 0)
        {
            link_reg_addr = AN8801SB_LED_0_ON_MASK;
            blk_reg_addr = AN8801SB_LED_0_BLK_MASK;
        }
        else if (led_id == 1)
        {
            link_reg_addr = AN8801SB_LED_1_ON_MASK;
            blk_reg_addr = AN8801SB_LED_1_BLK_MASK;
        }
        else if (led_id == 2)
        {
            link_reg_addr = AN8801SB_LED_2_ON_MASK;
            blk_reg_addr = AN8801SB_LED_2_BLK_MASK;
        }
        else
        {
            link_reg_addr = AN8801SB_LED_3_ON_MASK;
            blk_reg_addr = AN8801SB_LED_3_BLK_MASK;
        }

        hal_an8801sb_phy_getPhyLedCtrlMode(0, port, led_id, &led_ctrl_mode);
        if (HAL_PHY_LED_CTRL_MODE_PHY == led_ctrl_mode)
        {
            osal_memset(&led_behavior, 0, sizeof(AIR_CFG_VALUE_T));

            led_behavior.value = 0xFFF;
            led_behavior.param0 = port;
            led_behavior.param1 = led_id;

            hal_cfg_getValue(0, AIR_CFG_TYPE_PHY_LED_BEHAVIOR, &led_behavior);
            led_config = led_behavior.value;

            link_reg_data = 0;
            blk_reg_data = 0;

            GET_PHY_ABILITY(led_config, AN8801SB_LED_BHV_LINK_1000, AN8801SB_LED_LINK_1000, link_reg_data);
            GET_PHY_ABILITY(led_config, AN8801SB_LED_BHV_LINK_100, AN8801SB_LED_LINK_100, link_reg_data);
            GET_PHY_ABILITY(led_config, AN8801SB_LED_BHV_LINK_10, AN8801SB_LED_LINK_10, link_reg_data);
            GET_PHY_ABILITY(led_config, AN8801SB_LED_BHV_LINK_FULLDPLX, AN8801SB_LED_LINK_FULLDPLX, link_reg_data);
            GET_PHY_ABILITY(led_config, AN8801SB_LED_BHV_LINK_HALFDPLX, AN8801SB_LED_LINK_HALFDPLX, link_reg_data);
            GET_PHY_ABILITY(led_config, AN8801SB_LED_BHV_HIGH_ACTIVE, AN8801SB_LED_POL_HIGH_ACT, link_reg_data);

            GET_PHY_ABILITY(led_config, AN8801SB_LED_BHV_BLINK_TX_1000, AN8801SB_LED_BLINK_TX_1000, blk_reg_data);
            GET_PHY_ABILITY(led_config, AN8801SB_LED_BHV_BLINK_RX_1000, AN8801SB_LED_BLINK_RX_1000, blk_reg_data);
            GET_PHY_ABILITY(led_config, AN8801SB_LED_BHV_BLINK_TX_100, AN8801SB_LED_BLINK_TX_100, blk_reg_data);
            GET_PHY_ABILITY(led_config, AN8801SB_LED_BHV_BLINK_RX_100, AN8801SB_LED_BLINK_RX_100, blk_reg_data);
            GET_PHY_ABILITY(led_config, AN8801SB_LED_BHV_BLINK_TX_10, AN8801SB_LED_BLINK_TX_10, blk_reg_data);
            GET_PHY_ABILITY(led_config, AN8801SB_LED_BHV_BLINK_RX_10, AN8801SB_LED_BLINK_RX_10, blk_reg_data);

            if (link_reg_data != 0 || blk_reg_data != 0)
            {
                link_reg_data |= AN8801SB_LED_FUNC_ENABLE;
            }

            rv |= hal_mdio_writeC45ByPort(unit, port, MMD_DEV_VSPEC2, link_reg_addr, link_reg_data);
            rv |= hal_mdio_writeC45ByPort(unit, port, MMD_DEV_VSPEC2, blk_reg_addr, blk_reg_data);
        }
    }
    //_hal_an8801sb_phy_writeBuckPbus(port, AN8801SB_EFIFO_MODE, EFIFO_MODE_1000);
    //_hal_an8801sb_phy_writeBuckPbus(port, AN8801SB_EFIFO_CTRL, EFIFO_CTRL_1000);
    //_hal_an8801sb_phy_writeBuckPbus(port, AN8801SB_EFIFO_WM, EFIFO_WM_VALUE_1G);
    return rv;
}

/* FUNCTION NAME:   hal_an8801sb_phy_setAdminState
 * PURPOSE:
 *      This API is used to set port state.
 * INPUT:
 *      unit            --  Device unit number
 *      port            --  Port number
 *      state           --  Port state
 *
 * OUTPUT:
 *
 * RETURN:
 *      AIR_E_OK
 *      AIR_E_BAD_PARAMETER
 *
 * NOTES:
 *      None
 */
AIR_ERROR_NO_T
hal_an8801sb_phy_setAdminState(
    const UI32_T                unit,
    const UI32_T                port,
    const HAL_PHY_ADMIN_STATE_T state)
{
    AIR_ERROR_NO_T              rv = AIR_E_OK;
    UI16_T                      reg_value = 0;

    hal_mdio_readC22ByPort(0, port, MII_BMCR, &reg_value);
    if (HAL_PHY_ADMIN_STATE_DISABLE == state)
    {
        reg_value |= BMCR_PDOWN;
    }
    else
    {
        reg_value &= ~(BMCR_PDOWN);
    }
    hal_mdio_writeC22ByPort(0, port, MII_BMCR, reg_value);
    return rv;
}

/* FUNCTION NAME:   hal_an8801sb_phy_getAdminState
 * PURPOSE:
 *      This API is used to get port state.
 * INPUT:
 *      unit            --  Device unit number
 *      port            --  Port number
 *      ptr_state       --  Port state
 *
 * OUTPUT:
 *
 * RETURN:
 *      AIR_E_OK
 *      AIR_E_BAD_PARAMETER
 *
 * NOTES:
 *      None
 */
AIR_ERROR_NO_T
hal_an8801sb_phy_getAdminState(
    const UI32_T                unit,
    const UI32_T                port,
    HAL_PHY_ADMIN_STATE_T       *ptr_state)
{
    AIR_ERROR_NO_T              rv = AIR_E_OK;
    UI16_T                      reg_value = 0;

    hal_mdio_readC22ByPort(0, port, MII_BMCR, &reg_value);
    if (reg_value & BMCR_PDOWN)
    {
        *ptr_state = HAL_PHY_ADMIN_STATE_DISABLE;
    }
    else
    {
        *ptr_state = HAL_PHY_ADMIN_STATE_ENABLE;
    }
    return rv;
}

/* FUNCTION NAME:   hal_an8801sb_phy_setAutoNego
 * PURPOSE:
 *      This API is used to set port auto-negotiation.
 * INPUT:
 *      unit            --  Device unit number
 *      port            --  Port number
 *      auto_nego       --  Auto-negotiation
 *
 * OUTPUT:
 *
 * RETURN:
 *      AIR_E_OK
 *      AIR_E_BAD_PARAMETER
 *
 * NOTES:
 *      None
 */
AIR_ERROR_NO_T
hal_an8801sb_phy_setAutoNego(
    const UI32_T                unit,
    const UI32_T                port,
    const HAL_PHY_AUTO_NEGO_T   auto_nego)
{
    AIR_ERROR_NO_T              rv = AIR_E_OK;
    UI16_T                      reg_value = 0;

    hal_mdio_readC22ByPort(0, port, MII_BMCR, &reg_value);
    if (HAL_PHY_AUTO_NEGO_ENABLE == auto_nego)
    {
        reg_value |= BMCR_ANENABLE;
    }
    else if (HAL_PHY_AUTO_NEGO_RESTART == auto_nego)
    {
        reg_value |= BMCR_ANRESTART;
    }
    else
    {
        reg_value &= ~(BMCR_ANENABLE);
    }
    hal_mdio_writeC22ByPort(0, port, MII_BMCR, reg_value);
    return rv;
}

/* FUNCTION NAME:   hal_an8801sb_phy_getAutoNego
 * PURPOSE:
 *      This API is used to get port auto-negotiation.
 * INPUT:
 *      unit            --  Device unit number
 *      port            --  Port number
 *      ptr_auto_nego   --  Auto-negotiation
 *
 * OUTPUT:
 *
 * RETURN:
 *      AIR_E_OK
 *      AIR_E_BAD_PARAMETER
 *
 * NOTES:
 *      None
 */
AIR_ERROR_NO_T
hal_an8801sb_phy_getAutoNego(
    const UI32_T        unit,
    const UI32_T        port,
    HAL_PHY_AUTO_NEGO_T *ptr_auto_nego)
{
    AIR_ERROR_NO_T      rv = AIR_E_OK;
    UI16_T              reg_value = 0;

    hal_mdio_readC22ByPort(0, port, MII_BMCR, &reg_value);
    if (reg_value & BMCR_ANENABLE)
    {
        *ptr_auto_nego = HAL_PHY_AUTO_NEGO_ENABLE;
    }
    else
    {
        *ptr_auto_nego = HAL_PHY_AUTO_NEGO_DISABLE;
    }
    return rv;
}

/* FUNCTION NAME:   hal_an8801sb_phy_setLocalAdvAbility
 * PURPOSE:
 *      This API is used to set port local advertisment ability.
 * INPUT:
 *      unit            --  Device unit number
 *      port            --  Port number
 *      ptr_adv         --  Advertisement ability
 *
 * OUTPUT:
 *
 * RETURN:
 *      AIR_E_OK
 *      AIR_E_BAD_PARAMETER
 *
 * NOTES:
 *      None
 */
AIR_ERROR_NO_T
hal_an8801sb_phy_setLocalAdvAbility(
    const UI32_T            unit,
    const UI32_T            port,
    const HAL_PHY_AN_ADV_T  *ptr_adv)
{
    AIR_ERROR_NO_T          rv = AIR_E_OK;
    UI16_T                  reg_value = 0;

    rv = hal_mdio_readC22ByPort(0, port, MII_CTRL1000, &reg_value);
    if (AIR_E_OK == rv)
    {
        SET_PHY_ABILITY(ptr_adv->flags, HAL_PHY_AN_ADV_FLAGS_1000FUDX, ADVERTISE_1000FULL, reg_value);
        rv = hal_mdio_writeC22ByPort(0, port, MII_CTRL1000, reg_value);
    }

    if (AIR_E_OK == rv)
    {
        rv = hal_mdio_readC22ByPort(0, port, MII_ADVERTISE, &reg_value);
        if (AIR_E_OK == rv)
        {
            SET_PHY_ABILITY(ptr_adv->flags, HAL_PHY_AN_ADV_FLAGS_10HFDX, ADVERTISE_10HALF, reg_value);
            SET_PHY_ABILITY(ptr_adv->flags, HAL_PHY_AN_ADV_FLAGS_10FUDX, ADVERTISE_10FULL, reg_value);
            SET_PHY_ABILITY(ptr_adv->flags, HAL_PHY_AN_ADV_FLAGS_100HFDX, ADVERTISE_100HALF, reg_value);
            SET_PHY_ABILITY(ptr_adv->flags, HAL_PHY_AN_ADV_FLAGS_100FUDX, ADVERTISE_100FULL, reg_value);
            SET_PHY_ABILITY(ptr_adv->flags, HAL_PHY_AN_ADV_FLAGS_SYM_PAUSE, ADVERTISE_PAUSE_CAP, reg_value);
            SET_PHY_ABILITY(ptr_adv->flags, HAL_PHY_AN_ADV_FLAGS_ASYM_PAUSE, ADVERTISE_PAUSE_ASYM, reg_value);
            rv = hal_mdio_writeC22ByPort(0, port, MII_ADVERTISE, reg_value);
        }
    }

    if (AIR_E_OK == rv)
    {
        reg_value = (ptr_adv->flags & HAL_PHY_AN_ADV_FLAGS_EEE) ? (EEE_1000BASE_T | EEE_100BASE_TX ) : 0;
        rv = hal_mdio_writeC45ByPort(unit, port, MMD_DEV_ANEG, MMD_EEEAR, reg_value);
    }

    return rv;
}

/* FUNCTION NAME:   hal_an8801sb_phy_getLocalAdvAbility
 * PURPOSE:
 *      This API is used to get port local advertisment ability.
 * INPUT:
 *      unit            --  Device unit number
 *      port            --  Port number
 *      ptr_adv         --  Advertisement ability
 *
 * OUTPUT:
 *
 * RETURN:
 *      AIR_E_OK
 *      AIR_E_BAD_PARAMETER
 *
 * NOTES:
 *      None
 */
AIR_ERROR_NO_T
hal_an8801sb_phy_getLocalAdvAbility(
    const UI32_T        unit,
    const UI32_T        port,
    HAL_PHY_AN_ADV_T    *ptr_adv)
{
    AIR_ERROR_NO_T      rv = AIR_E_OK;
    UI16_T              reg_value = 0;

    ptr_adv->flags = 0;
    rv = hal_mdio_readC22ByPort(0, port, MII_CTRL1000, &reg_value);
    if (AIR_E_OK == rv)
    {
        GET_PHY_ABILITY(reg_value, ADVERTISE_1000FULL, HAL_PHY_AN_ADV_FLAGS_1000FUDX, ptr_adv->flags);
    }

    rv = hal_mdio_readC22ByPort(0, port, MII_ADVERTISE, &reg_value);
    if (AIR_E_OK == rv)
    {
        GET_PHY_ABILITY(reg_value, ADVERTISE_10HALF, HAL_PHY_AN_ADV_FLAGS_10HFDX, ptr_adv->flags);
        GET_PHY_ABILITY(reg_value, ADVERTISE_10FULL, HAL_PHY_AN_ADV_FLAGS_10FUDX, ptr_adv->flags);
        GET_PHY_ABILITY(reg_value, ADVERTISE_100HALF, HAL_PHY_AN_ADV_FLAGS_100HFDX, ptr_adv->flags);
        GET_PHY_ABILITY(reg_value, ADVERTISE_100FULL, HAL_PHY_AN_ADV_FLAGS_100FUDX, ptr_adv->flags);
        GET_PHY_ABILITY(reg_value, ADVERTISE_PAUSE_CAP, HAL_PHY_AN_ADV_FLAGS_SYM_PAUSE, ptr_adv->flags);
        GET_PHY_ABILITY(reg_value, ADVERTISE_PAUSE_ASYM, HAL_PHY_AN_ADV_FLAGS_ASYM_PAUSE, ptr_adv->flags);
    }

    rv = hal_mdio_readC45ByPort(unit, port, MMD_DEV_ANEG, MMD_EEEAR, &reg_value);
    if (AIR_E_OK == rv)
    {
        GET_PHY_ABILITY(reg_value, (EEE_1000BASE_T | EEE_100BASE_TX), HAL_PHY_AN_ADV_FLAGS_EEE, ptr_adv->flags);
    }

    return rv;
}

/* FUNCTION NAME:   hal_an8801sb_phy_getRemoteAdvAbility
 * PURPOSE:
 *      This API is used to get port remote advertisment ability.
 * INPUT:
 *      unit            --  Device unit number
 *      port            --  Port number
 *      ptr_adv         --  Advertisement ability
 *
 * OUTPUT:
 *
 * RETURN:
 *      AIR_E_OK
 *      AIR_E_BAD_PARAMETER
 *
 * NOTES:
 *      None
 */
AIR_ERROR_NO_T
hal_an8801sb_phy_getRemoteAdvAbility(
    const UI32_T        unit,
    const UI32_T        port,
    HAL_PHY_AN_ADV_T    *ptr_adv)
{
    AIR_ERROR_NO_T      rv = AIR_E_OK;
    UI16_T              reg_value = 0;

    ptr_adv->flags = 0;
    rv = hal_mdio_readC22ByPort(0, port, MII_STAT1000, &reg_value);
    if (AIR_E_OK == rv)
    {
        GET_PHY_ABILITY(reg_value, LPA_1000FULL, HAL_PHY_AN_ADV_FLAGS_1000FUDX, ptr_adv->flags);
    }

    rv = hal_mdio_readC22ByPort(0, port, MII_LPA, &reg_value);
    if (AIR_E_OK == rv)
    {
        GET_PHY_ABILITY(reg_value, LPA_10HALF, HAL_PHY_AN_ADV_FLAGS_10HFDX, ptr_adv->flags);
        GET_PHY_ABILITY(reg_value, LPA_10FULL, HAL_PHY_AN_ADV_FLAGS_10FUDX, ptr_adv->flags);
        GET_PHY_ABILITY(reg_value, LPA_100HALF, HAL_PHY_AN_ADV_FLAGS_100HFDX, ptr_adv->flags);
        GET_PHY_ABILITY(reg_value, LPA_100FULL, HAL_PHY_AN_ADV_FLAGS_100FUDX, ptr_adv->flags);
        GET_PHY_ABILITY(reg_value, LPA_PAUSE_CAP, HAL_PHY_AN_ADV_FLAGS_SYM_PAUSE, ptr_adv->flags);
        GET_PHY_ABILITY(reg_value, LPA_PAUSE_ASYM, HAL_PHY_AN_ADV_FLAGS_ASYM_PAUSE, ptr_adv->flags);
    }

    rv = hal_mdio_readC45ByPort(unit, port, MMD_DEV_ANEG, MMD_EEELPAR, &reg_value);
    if (AIR_E_OK == rv)
    {
        GET_PHY_ABILITY(reg_value, (EEE_1000BASE_T | EEE_100BASE_TX), HAL_PHY_AN_ADV_FLAGS_EEE, ptr_adv->flags);
    }

    return rv;
}

/* FUNCTION NAME:   hal_an8801sb_phy_setSpeed
 * PURPOSE:
 *      This API is used to set port speed.
 * INPUT:
 *      unit            --  Device unit number
 *      port            --  Port number
 *      speed           --  Port speed
 *
 * OUTPUT:
 *
 * RETURN:
 *      AIR_E_OK
 *      AIR_E_BAD_PARAMETER
 *
 * NOTES:
 *      None
 */
AIR_ERROR_NO_T
hal_an8801sb_phy_setSpeed(
    const UI32_T            unit,
    const UI32_T            port,
    const HAL_PHY_SPEED_T   speed)
{
    AIR_ERROR_NO_T          rv = AIR_E_OK;
    UI16_T                  reg_value = 0;

    if(speed >= HAL_PHY_SPEED_1000M)
    {
        return AIR_E_OP_INVALID;
    }

    if ((rv = hal_mdio_readC22ByPort(unit, port, MII_BMCR, &reg_value)) != AIR_E_OK)
    {
        return rv;
    }

    reg_value &= ~(BMCR_SPEED1000 | BMCR_SPEED100);

    if (HAL_PHY_SPEED_1000M == speed)
    {
        reg_value |= BMCR_SPEED1000;
    }
    else if (HAL_PHY_SPEED_100M == speed)
    {
        reg_value |= BMCR_SPEED100;
    }
    else if (HAL_PHY_SPEED_10M == speed)
    {
        reg_value |= 0;
    }
    else
    {
        return AIR_E_NOT_SUPPORT;
    }
    hal_mdio_writeC22ByPort(0, port, MII_BMCR, reg_value);
    return rv;
}

/* FUNCTION NAME:   hal_an8801sb_phy_getSpeed
 * PURPOSE:
 *      This API is used to get port speed.
 * INPUT:
 *      unit            --  Device unit number
 *      port            --  Port number
 *      ptr_speed       --  Port speed
 *
 * OUTPUT:
 *
 * RETURN:
 *      AIR_E_OK
 *      AIR_E_BAD_PARAMETER
 *
 * NOTES:
 *      None
 */
AIR_ERROR_NO_T
hal_an8801sb_phy_getSpeed(
    const UI32_T    unit,
    const UI32_T    port,
    HAL_PHY_SPEED_T *ptr_speed)
{
    UI16_T  reg_data = 0;
    AIR_ERROR_NO_T rv = AIR_E_OK;

    if ((rv = hal_mdio_readC22ByPort(unit, port, MII_BMCR, &reg_data)) != AIR_E_OK)
    {
        return rv;
    }

    if (reg_data & BMCR_SPEED1000)
    {
        *ptr_speed = HAL_PHY_SPEED_1000M;
    }
    else if (reg_data & BMCR_SPEED100)
    {
        *ptr_speed = HAL_PHY_SPEED_100M;
    }
    else
    {
        *ptr_speed = HAL_PHY_SPEED_10M;
    }

    return rv;
}

/* FUNCTION NAME:   hal_an8801sb_phy_setDuplex
 * PURPOSE:
 *      This API is used to set port duplex.
 * INPUT:
 *      unit            --  Device unit number
 *      port            --  Port number
 *      duplex          --  Port duplex
 *
 * OUTPUT:
 *
 * RETURN:
 *      AIR_E_OK
 *      AIR_E_BAD_PARAMETER
 *
 * NOTES:
 *      None
 */
AIR_ERROR_NO_T
hal_an8801sb_phy_setDuplex(
    const UI32_T            unit,
    const UI32_T            port,
    const HAL_PHY_DUPLEX_T  duplex)
{
    AIR_ERROR_NO_T          rv = AIR_E_OK;
    UI16_T                  reg_value = 0;

    HAL_CHECK_ENUM_RANGE(duplex, HAL_PHY_DUPLEX_LAST);

    hal_mdio_readC22ByPort(0, port, MII_BMCR, &reg_value);
    if (HAL_PHY_DUPLEX_FULL == duplex)
    {
        reg_value |= BMCR_FULLDPLX;
    }
    else
    {
        reg_value &= ~(BMCR_FULLDPLX);
    }

    hal_mdio_writeC22ByPort(0, port, MII_BMCR, reg_value);
    return rv;
}

/* FUNCTION NAME:   hal_an8801sb_phy_getDuplex
 * PURPOSE:
 *      This API is used to get port duplex.
 * INPUT:
 *      unit            --  Device unit number
 *      port            --  Port number
 *      ptr_duplex      --  Port duplex
 *
 * OUTPUT:
 *
 * RETURN:
 *      AIR_E_OK
 *      AIR_E_BAD_PARAMETER
 *
 * NOTES:
 *      None
 */
AIR_ERROR_NO_T
hal_an8801sb_phy_getDuplex(
    const UI32_T        unit,
    const UI32_T        port,
    HAL_PHY_DUPLEX_T    *ptr_duplex)
{
    UI16_T  reg_data = 0;
    AIR_ERROR_NO_T rv = AIR_E_OK;

    if ((rv = hal_mdio_readC22ByPort(unit, port, MII_BMCR, &reg_data)) != AIR_E_OK)
    {
        return rv;
    }

    if (reg_data & BMCR_FULLDPLX)
    {
        *ptr_duplex = HAL_PHY_DUPLEX_FULL;
    }
    else
    {
        *ptr_duplex = HAL_PHY_DUPLEX_HALF;
    }

    return rv;
}

/* FUNCTION NAME:   hal_an8801sb_phy_getLinkStatus
 * PURPOSE:
 *      This API is used to get port link status.
 * INPUT:
 *      unit            --  Device unit number
 *      port            --  Port number
 *      ptr_status      --  Link Status
 *
 * OUTPUT:
 *
 * RETURN:
 *      AIR_E_OK
 *      AIR_E_BAD_PARAMETER
 *
 * NOTES:
 *      None
 */
AIR_ERROR_NO_T
hal_an8801sb_phy_getLinkStatus(
    const UI32_T            unit,
    const UI32_T            port,
    HAL_PHY_LINK_STATUS_T   *ptr_status)
{
    AIR_ERROR_NO_T          rv = AIR_E_OK;
    UI16_T                  preSpeed = 0, reg_bmcr = 0;
    UI32_T                  reg_value = 0, force_speed = 0;
    I32_T                   an_retry = MAX_SGMII_AN_RETRY;

    rv = _hal_an8801sb_readGenphyStatus(unit, port, ptr_status);
    if (AIR_E_OK != rv)
    {
        return rv;
    }
    DIAG_PRINT(HAL_DBG_INFO, " (%u) port[%d] flags=(%x)\n", unit, port, ptr_status->flags);

    rv = hal_mdio_readC45ByPort(unit, port, MMD_DEV_VSPEC2, PHY_PRE_SPEED_REG, &preSpeed);
    rv |= hal_mdio_readC22ByPort(unit, port, MII_BMCR, &reg_bmcr);
    if (AIR_E_OK != rv)
    {
        return rv;
    }
    if (0 == (ptr_status->flags & HAL_PHY_LINK_STATUS_FLAGS_LINK_UP))
    {
        if (NO_SPEED != preSpeed)
        {
            reg_value = _hal_an8801sb_phy_readBuckPbus(port, AN8801SB_PCS_CTRL1);
            _hal_an8801sb_phy_writeBuckPbus(port, AN8801SB_PCS_CTRL1, \
                    ((reg_value & ~SGMII_PCS_FORCE_SYNC_MASK) | SGMII_PCS_FORCE_SYNC_OFF));
            preSpeed = NO_SPEED;
            rv |= hal_mdio_writeC45ByPort(unit, port, MMD_DEV_VSPEC2, PHY_PRE_SPEED_REG, preSpeed);
        }
        return rv;
    }

    force_speed = 0;
    if (0 == (reg_bmcr & BMCR_ANENABLE))
    {
        /* PHY is force mode so force serdes speed */
        if (reg_bmcr & BMCR_SPEED1000)
        {
            force_speed = 0x5801;
            ptr_status->speed = HAL_PHY_SPEED_1000M;
        }
        else if (reg_bmcr & BMCR_SPEED100)
        {
            force_speed = 0x5401;
            ptr_status->speed = HAL_PHY_SPEED_100M;
        }
        else
        {
            force_speed = 0x5001;
            ptr_status->speed = HAL_PHY_SPEED_10M;
        }
    }

    if (preSpeed != ptr_status->speed)
    {
        preSpeed = ptr_status->speed;
        rv = hal_mdio_writeC45ByPort(unit, port, MMD_DEV_VSPEC2, PHY_PRE_SPEED_REG, preSpeed);

        reg_value = _hal_an8801sb_phy_readBuckPbus(port, AN8801SB_PCS_CTRL1);
        _hal_an8801sb_phy_writeBuckPbus(port, AN8801SB_PCS_CTRL1, (reg_value & ~SGMII_PCS_FORCE_SYNC_MASK));
        _hal_an8801sb_phy_writeBuckPbus(port, AN8801SB_EFIFO_WM, EFIFO_WM_VALUE);
        while (an_retry > 0)
        {
            osal_delayUs(1000);      /* delay 1 ms */
            reg_value = _hal_an8801sb_phy_readBuckPbus(port, AN8801SB_PCS_STS);
            if (reg_value & AN8801SB_SGMII_AN0_AN_DONE)
            {
                break;
            }
            an_retry --;
        }
        osal_delayUs(10000);        /* delay 10 ms */
        if (force_speed != 0)
        {
            _hal_an8801sb_phy_writeBuckPbus(port, AN8801SB_SGMII_AN4, force_speed);

            reg_value = _hal_an8801sb_phy_readBuckPbus(port, AN8801SB_SGMII_AN0);
            reg_value |= AN8801SB_SGMII_AN0_ANRESTART;
            _hal_an8801sb_phy_writeBuckPbus(port, AN8801SB_SGMII_AN0, reg_value);
        }
        reg_value = _hal_an8801sb_phy_readBuckPbus(port, AN8801SB_SGMII_AN0);
        reg_value |= AN8801SB_SGMII_AN0_RESET;
        _hal_an8801sb_phy_writeBuckPbus(port, AN8801SB_SGMII_AN0, reg_value);
    }
    return rv;
}

/* FUNCTION NAME:   hal_an8801sb_phy_setLoopBack
 * PURPOSE:
 *      This API is used to get port link status.
 * INPUT:
 *      unit            --  Device unit number
 *      port            --  Port number
 *      lpbk_type       --  LookBack type
 *      enable          --  mode enable/disable
 *
 * OUTPUT:
 *
 * RETURN:
 *      AIR_E_OK
 *      AIR_E_BAD_PARAMETER
 *
 * NOTES:
 *      None
 */
AIR_ERROR_NO_T
hal_an8801sb_phy_setLoopBack(
    const UI32_T            unit,
    const UI32_T            port,
    const HAL_PHY_LPBK_T    lpbk_type,
    const BOOL_T            enable)
{
    UI16_T  reg_data = 0, page = 0;
    AIR_ERROR_NO_T rv = AIR_E_OK;

    if(HAL_PHY_LPBK_FAR_END == lpbk_type)
    {
        /* Backup page */
        if ((rv = hal_mdio_readC22ByPort(unit, port, MII_PAGE_SELECT, &page)) != AIR_E_OK)
        {
            return rv;
        }

        /* Swtich to page 1 */
        reg_data = 1;
        if ((rv = hal_mdio_writeC22ByPort(unit, port, MII_PAGE_SELECT, reg_data)) != AIR_E_OK)
        {
            return rv;
        }

        /* Read data from ExtReg1A */
        if ((rv = hal_mdio_readC22ByPort(unit, port, MII_RESV2, &reg_data)) != AIR_E_OK)
        {
            return rv;
        }

        if (TRUE == enable)
        {
            reg_data |= LPBK_FAR_END;
        }
        else
        {
            reg_data &= ~(LPBK_FAR_END);
        }

        if ((rv = hal_mdio_writeC22ByPort(unit, port, MII_RESV2, reg_data)) != AIR_E_OK)
        {
            return rv;
        }

        /* Restore page*/
        if ((rv = hal_mdio_writeC22ByPort(unit, port, MII_PAGE_SELECT, page)) != AIR_E_OK)
        {
            return rv;
        }
    }
    else
    {
        if ((rv = hal_mdio_readC22ByPort(unit, port, MII_BMCR, &reg_data)) != AIR_E_OK)
        {
            return rv;
        }

        if (TRUE == enable)
        {
            reg_data |= BMCR_LOOPBACK;
        }
        else
        {
            reg_data &= ~(BMCR_LOOPBACK);
        }

        if ((rv = hal_mdio_writeC22ByPort(unit, port, MII_BMCR, reg_data)) != AIR_E_OK)
        {
            return rv;
        }
    }

    return rv;
}

/* FUNCTION NAME:   hal_an8801sb_phy_getLoopBack
 * PURPOSE:
 *      This API is used to get port link status.
 * INPUT:
 *      unit            --  Device unit number
 *      port            --  Port number
 *      lpbk_type       --  LookBack type
 *
 * OUTPUT:
 *      ptr_enable      --  mode enable/disable
 * RETURN:
 *      AIR_E_OK
 *      AIR_E_BAD_PARAMETER
 *
 * NOTES:
 *      None
 */
AIR_ERROR_NO_T
hal_an8801sb_phy_getLoopBack(
    const UI32_T            unit,
    const UI32_T            port,
    const HAL_PHY_LPBK_T    lpbk_type,
    BOOL_T                  *ptr_enable)
{
    UI16_T  reg_data = 0, page = 0;
    AIR_ERROR_NO_T rv = AIR_E_OK;

    if(HAL_PHY_LPBK_FAR_END == lpbk_type)
    {
        /* Backup page */
        if ((rv = hal_mdio_readC22ByPort(unit, port, MII_PAGE_SELECT, &page)) != AIR_E_OK)
        {
            return rv;
        }

        /* Swtich to page 1 */
        reg_data = 1;
        if ((rv = hal_mdio_writeC22ByPort(unit, port, MII_PAGE_SELECT, reg_data)) != AIR_E_OK)
        {
            return rv;
        }

        /* Read data from ExtReg1A */
        if ((rv = hal_mdio_readC22ByPort(unit, port, MII_RESV2, &reg_data)) != AIR_E_OK)
        {
            return rv;
        }

        if (reg_data & LPBK_FAR_END)
        {
            *ptr_enable = TRUE;
        }
        else
        {
            *ptr_enable = FALSE;
        }

        /* Restore page*/
        if ((rv = hal_mdio_writeC22ByPort(unit, port, MII_PAGE_SELECT, page)) != AIR_E_OK)
        {
            return rv;
        }
    }
    else
    {
        if ((rv = hal_mdio_readC22ByPort(unit, port, MII_BMCR, &reg_data)) != AIR_E_OK)
        {
            return rv;
        }

        if (reg_data & BMCR_LOOPBACK)
        {
            *ptr_enable = TRUE;
        }
        else
        {
            *ptr_enable = FALSE;
        }
    }

    return rv;
}

/* FUNCTION NAME:   hal_an8801sb_phy_setSmartSpeedDown
 * PURPOSE:
 *      This API is used to gst port smart speed down.
 * INPUT:
 *      unit            --  Device unit number
 *      port            --  Port number
 *      ssd_mode        --  smart speed down mode
 *
 * OUTPUT:
 *
 * RETURN:
 *      AIR_E_OK
 *      AIR_E_BAD_PARAMETER
 *
 * NOTES:
 *      None
 */
AIR_ERROR_NO_T
hal_an8801sb_phy_setSmartSpeedDown(
    const UI32_T                unit,
    const UI32_T                port,
    const HAL_PHY_SSD_MODE_T    ssd_mode)
{
    UI16_T  reg_data = 0, page = 0, data = 0;
    AIR_ERROR_NO_T rv = AIR_E_OK;

    if ((HAL_PHY_SSD_MODE_1T == ssd_mode) || (HAL_PHY_SSD_MODE_5T < ssd_mode))
    {
        return AIR_E_BAD_PARAMETER;
    }

    /* Backup page */
    if ((rv = hal_mdio_readC22ByPort(unit, port, MII_PAGE_SELECT, &page)) != AIR_E_OK)
    {
        return rv;
    }

    /* Switch to page 1*/
    reg_data = 1;
    if ((rv = hal_mdio_writeC22ByPort(unit, port, MII_PAGE_SELECT, reg_data)) != AIR_E_OK)
    {
        return rv;
    }
    if ((rv = hal_mdio_readC22ByPort(unit, port, 0x14, &data)) != AIR_E_OK)
    {
        return rv;
    }

    data &= ~BITS(2,3);
    if(HAL_PHY_SSD_MODE_DISABLE != ssd_mode)
    {
        data |= BIT(4);
        data |= (ssd_mode - HAL_PHY_SSD_MODE_2T) << 2;
    }
    else
    {
        data &= ~BIT(4);
    }

    /* Switch to page 1*/
    reg_data = 1;
    if ((rv = hal_mdio_writeC22ByPort(unit, port, MII_PAGE_SELECT, reg_data)) != AIR_E_OK)
    {
        return rv;
    }

    if ((rv = hal_mdio_writeC22ByPort(unit, port, 0x14, data)) != AIR_E_OK)
    {
        return rv;
    }

    /* Restore page */
    if ((rv = hal_mdio_writeC22ByPort(unit, port, MII_PAGE_SELECT, page)) != AIR_E_OK)
    {
        return rv;
    }

    return rv;
}

/* FUNCTION NAME:   hal_an8801sb_phy_getSmartSpeedDown
 * PURPOSE:
 *      This API is used to get port power save.
 * INPUT:
 *      unit            --  Device unit number
 *      port            --  Port number
 *
 * OUTPUT:
 *      ptr_ssd_mode    --  smart speed down mode
 * RETURN:
 *      AIR_E_OK
 *      AIR_E_BAD_PARAMETER
 *
 * NOTES:
 *      None
 */
AIR_ERROR_NO_T
hal_an8801sb_phy_getSmartSpeedDown(
    const UI32_T        unit,
    const UI32_T        port,
    HAL_PHY_SSD_MODE_T  *ptr_ssd_mode)
{
    UI16_T  reg_data = 0, page = 0, data = 0;
    AIR_ERROR_NO_T rv = AIR_E_OK;

    /* Backup page */
    if ((rv = hal_mdio_readC22ByPort(unit, port, MII_PAGE_SELECT, &page)) != AIR_E_OK)
    {
        return rv;
    }

    /* Switch to page 1*/
    reg_data = 1;
    if ((rv = hal_mdio_writeC22ByPort(unit, port, MII_PAGE_SELECT, reg_data)) != AIR_E_OK)
    {
        return rv;
    }
    if ((rv = hal_mdio_readC22ByPort(unit, port, 0x14, &data)) != AIR_E_OK)
    {
        return rv;
    }

    reg_data = BITS_OFF_R(data, 4, 1);
    if (reg_data)
    {
        *ptr_ssd_mode = (BITS_OFF_R(data, 2, 2) + HAL_PHY_SSD_MODE_2T);
    }
    else
    {
        *ptr_ssd_mode = HAL_PHY_SSD_MODE_DISABLE;
    }

    /* Restore page */
    if ((rv = hal_mdio_writeC22ByPort(unit, port, MII_PAGE_SELECT, page)) != AIR_E_OK)
    {
        return rv;
    }

    return rv;
}

/* FUNCTION NAME:   hal_an8801sb_phy_setLedOnCtrl
 * PURPOSE:
 *      This API is used to set control of port LED.
 * INPUT:
 *      unit            --  Device unit number
 *      port            --  Port number
 *      led_id          --  LED ID
 *      enable          --  FALSE:Disable
 *                          TRUE: Enable
 *
 * OUTPUT:
 *      None
 * RETURN:
 *      AIR_E_OK
 * NOTES:
 *      None
 */
AIR_ERROR_NO_T
hal_an8801sb_phy_setLedOnCtrl(
    const UI32_T    unit,
    const UI32_T    port,
    const UI32_T    led_id,
    const BOOL_T    enable)
{
    UI16_T          reg_data = 0, reg_addr = 0;
    UI32_T          pbus_data = 0;
    AIR_ERROR_NO_T  rv = AIR_E_OK;

    if (led_id == 0)
    {
        reg_addr = AN8801SB_LED_0_ON_MASK;
    }
    else if (led_id == 1)
    {
        reg_addr = AN8801SB_LED_1_ON_MASK;
    }
    else if (led_id == 2)
    {
        reg_addr = AN8801SB_LED_2_ON_MASK;
    }
    else
    {
        reg_addr = AN8801SB_LED_3_ON_MASK;
    }

    /* LED configuration */
    _hal_an8801sb_phy_writeBuckPbus(port, 0x186c, 0x3);
    _hal_an8801sb_phy_writeBuckPbus(port, 0X1870, 0x100);
    pbus_data = (_hal_an8801sb_phy_readBuckPbus(port, 0x1880) & ~(0x3));
    _hal_an8801sb_phy_writeBuckPbus(port, 0x1880, pbus_data);

    rv = hal_mdio_writeC45ByPort(unit, port, MMD_DEV_VSPEC2, 0x21, 0x8008);
    if (AIR_E_OK == rv)
    {
        rv = hal_mdio_writeC45ByPort(unit, port, MMD_DEV_VSPEC2, 0x22, 0x600);
        if (AIR_E_OK == rv)
        {
            rv = hal_mdio_writeC45ByPort(unit, port, MMD_DEV_VSPEC2, 0x23, 0xc00);
        }
    }

    if (AIR_E_OK == rv)
    {
        rv = hal_mdio_readC45ByPort(unit, port, MMD_DEV_VSPEC2, reg_addr, &reg_data);
        if (AIR_E_OK == rv)
        {
            if (TRUE == enable)
            {
                reg_data |= AN8801SB_LED_LINK_FORCE_ON;
            }
            else
            {
                reg_data &= ~(AN8801SB_LED_LINK_FORCE_ON);
            }

            if (reg_data != 0)
            {
                reg_data |= AN8801SB_LED_FUNC_ENABLE;
            }

            rv = hal_mdio_writeC45ByPort(unit, port, MMD_DEV_VSPEC2, reg_addr, reg_data);
        }
    }

    return rv;
}


/* FUNCTION NAME:   hal_an8801sb_phy_getLedOnCtrl
 * PURPOSE:
 *      This API is used to get port LED control setting.
 * INPUT:
 *      unit            --  Device unit number
 *      port            --  Port number
 *      led_id          --  LED ID
 * OUTPUT:
 *      ptr_enable      --  FALSE:Disable
 *                          TRUE: Enable
 * RETURN:
 *      AIR_E_OK
 * NOTES:
 *      None
 */
AIR_ERROR_NO_T
hal_an8801sb_phy_getLedOnCtrl(
    const UI32_T    unit,
    const UI32_T    port,
    const UI32_T    led_id,
    BOOL_T          *ptr_enable)
{
    UI16_T          reg_data = 0, reg_addr = 0;
    AIR_ERROR_NO_T  rv = AIR_E_OK;

    if (led_id == 0)
    {
        reg_addr = AN8801SB_LED_0_ON_MASK;
    }
    else if (led_id == 1)
    {
        reg_addr = AN8801SB_LED_1_ON_MASK;
    }
    else if (led_id == 2)
    {
        reg_addr = AN8801SB_LED_2_ON_MASK;
    }
    else
    {
        reg_addr = AN8801SB_LED_3_ON_MASK;
    }

    rv = hal_mdio_readC45ByPort(unit, port, MMD_DEV_VSPEC2, reg_addr, &reg_data);
    if (AIR_E_OK == rv)
    {
        if (reg_data & AN8801SB_LED_LINK_FORCE_ON)
        {
            *ptr_enable = TRUE;
        }
        else
        {
            *ptr_enable = FALSE;
        }
    }
    return rv;
}

/* FUNCTION NAME:   hal_an8801sb_phy_testTxCompliance
 * PURPOSE:
 *      This API is used to set the Tx compliance mode.
 * INPUT:
 *      unit            --  Device unit number
 *      port            --  Port number
 *      mode            --  BIST mode
 * OUTPUT:
 *      None
 * RETURN:
 *      AIR_E_OK
 * NOTES:
 *      None
 */
AIR_ERROR_NO_T
hal_an8801sb_phy_testTxCompliance(
    const UI32_T                        unit,
    const UI32_T                        port,
    const HAL_PHY_TX_COMPLIANCE_MODE_T  mode)
{
    AIR_ERROR_NO_T ret = AIR_E_OK;
    ret = hal_cmn_phy_testTxCompliance(unit, port, mode);
    return ret;
}

/* FUNCTION NAME:   hal_an8801sb_phy_setPhyLedCtrlMode
 * PURPOSE:
 *      This API is used to set phy led control mode.
 * INPUT:
 *      unit            --  Device unit number
 *      port            --  Port number
 *      led_id          --  LED Id
 *      ctrl_mode       --  LED control mode enumeration type
 * OUTPUT:
 *      None
 * RETURN:
 *      AIR_E_OK
 *      AIR_E_BAD_PARAMETER
 * NOTES:
 *      None
 */
AIR_ERROR_NO_T
hal_an8801sb_phy_setPhyLedCtrlMode(
    const UI32_T                    unit,
    const UI32_T                    port,
    const UI32_T                    led_id,
    const HAL_PHY_LED_CTRL_MODE_T   ctrl_mode)
{
    UI16_T                  data = 0, led_config = 0, offset = 0;
    UI16_T                  link_reg_data = 0, blk_reg_data = 0, link_reg_addr = 0, blk_reg_addr = 0;
    UI32_T                  pbus_data = 0;
    AIR_ERROR_NO_T          rv = AIR_E_OK;
    BOOL_T                  is_force = FALSE;
    HAL_PHY_LED_STATE_T     state;
    AIR_CFG_VALUE_T         led_behavior;

    rv = hal_mdio_readC45ByPort(unit, port, MMD_DEV_VSPEC2, PHY_LED_CFG_REG, &data);
    if (AIR_E_OK == rv)
    {
        offset = (AN8801SB_LED_0_CFG_CTRL_MODE_BIT + (AN8801SB_LED_CFG_RG_OFFSET * led_id));
        data &= ~(BITS_OFF_L(AN8801SB_LED_CFG_CTRL_MODE_MASK, offset, AN8801SB_LED_CFG_CTRL_MODE_WIDTH));
        data |= BITS_OFF_L(ctrl_mode, offset, AN8801SB_LED_CFG_CTRL_MODE_WIDTH);
        rv = hal_mdio_writeC45ByPort(unit, port, MMD_DEV_VSPEC2, PHY_LED_CFG_REG, data);
    }

    if (AIR_E_OK == rv)
    {
        /* LED configuration */
        _hal_an8801sb_phy_writeBuckPbus(port, 0x186c, 0x3);
        _hal_an8801sb_phy_writeBuckPbus(port, 0X1870, 0x100);
        pbus_data = (_hal_an8801sb_phy_readBuckPbus(port, 0x1880) & ~(0x3));
        _hal_an8801sb_phy_writeBuckPbus(port, 0x1880, pbus_data);
        rv = hal_mdio_writeC45ByPort(unit, port, MMD_DEV_VSPEC2, 0x21, 0x8008);
        if (AIR_E_OK == rv)
        {
            rv = hal_mdio_writeC45ByPort(unit, port, MMD_DEV_VSPEC2, 0x22, 0x600);
        }

        if (AIR_E_OK == rv)
        {
            is_force = (HAL_PHY_LED_CTRL_MODE_FORCE == ctrl_mode) ? TRUE : FALSE;
            if (TRUE == is_force)
            {
                rv = hal_an8801sb_phy_getPhyLedForceState(unit, port, led_id, &state);
                if (AIR_E_OK == rv)
                {
                    rv = hal_an8801sb_phy_setPhyLedForceState(unit, port, led_id, state);
                }
            }
            else
            {
                osal_memset(&led_behavior, 0, sizeof(AIR_CFG_VALUE_T));
                led_behavior.value = DEFAULT_LED_CFG;
                led_behavior.param0 = port;
                led_behavior.param1 = led_id;

                rv = hal_cfg_getValue(0, AIR_CFG_TYPE_PHY_LED_BEHAVIOR, &led_behavior);

                if (AIR_E_OK == rv)
                {
                    rv = hal_mdio_writeC45ByPort(unit, port, MMD_DEV_VSPEC2, 0x23, 0xc00);
                    if (AIR_E_OK == rv)
                    {
                        led_config = led_behavior.value;

                        link_reg_data = 0;
                        blk_reg_data = 0;

                        LED_ID_TO_LED_REG_ADDRESS(led_id, PHY_LED_TYPE_ON, link_reg_addr);
                        LED_ID_TO_LED_REG_ADDRESS(led_id, PHY_LED_TYPE_BLK, blk_reg_addr);

                        GET_PHY_ABILITY(led_config, AN8801SB_LED_BHV_LINK_1000, AN8801SB_LED_LINK_1000, link_reg_data);
                        GET_PHY_ABILITY(led_config, AN8801SB_LED_BHV_LINK_100, AN8801SB_LED_LINK_100, link_reg_data);
                        GET_PHY_ABILITY(led_config, AN8801SB_LED_BHV_LINK_10, AN8801SB_LED_LINK_10, link_reg_data);
                        GET_PHY_ABILITY(led_config, \
                                        AN8801SB_LED_BHV_LINK_FULLDPLX, AN8801SB_LED_LINK_FULLDPLX, link_reg_data);
                        GET_PHY_ABILITY(led_config, \
                                        AN8801SB_LED_BHV_LINK_HALFDPLX, AN8801SB_LED_LINK_HALFDPLX, link_reg_data);
                        GET_PHY_ABILITY(led_config, \
                                        AN8801SB_LED_BHV_HIGH_ACTIVE, AN8801SB_LED_POL_HIGH_ACT, link_reg_data);

                        GET_PHY_ABILITY(led_config, \
                                        AN8801SB_LED_BHV_BLINK_TX_1000, AN8801SB_LED_BLINK_TX_1000, blk_reg_data);
                        GET_PHY_ABILITY(led_config, \
                                        AN8801SB_LED_BHV_BLINK_RX_1000, AN8801SB_LED_BLINK_RX_1000, blk_reg_data);
                        GET_PHY_ABILITY(led_config, \
                                        AN8801SB_LED_BHV_BLINK_TX_100, AN8801SB_LED_BLINK_TX_100, blk_reg_data);
                        GET_PHY_ABILITY(led_config, \
                                        AN8801SB_LED_BHV_BLINK_RX_100, AN8801SB_LED_BLINK_RX_100, blk_reg_data);
                        GET_PHY_ABILITY(led_config, \
                                        AN8801SB_LED_BHV_BLINK_TX_10, AN8801SB_LED_BLINK_TX_10, blk_reg_data);
                        GET_PHY_ABILITY(led_config, \
                                        AN8801SB_LED_BHV_BLINK_RX_10, AN8801SB_LED_BLINK_RX_10, blk_reg_data);

                        if (link_reg_data != 0 || blk_reg_data != 0)
                        {
                            link_reg_data |= AN8801SB_LED_FUNC_ENABLE;
                        }

                        DIAG_PRINT(HAL_DBG_INFO, "port %u, link_reg_addr 0x%02X, link_reg_data 0x%04X\n", \
                                    port, link_reg_addr, link_reg_data);
                        DIAG_PRINT(HAL_DBG_INFO, "port %u, blk_reg_addr 0x%02X, blk_reg_data 0x%04X\n", \
                                    port, blk_reg_addr, blk_reg_data);

                        rv = hal_mdio_writeC45ByPort(unit, port, MMD_DEV_VSPEC2, link_reg_addr, link_reg_data);
                        if (AIR_E_OK == rv)
                        {
                            rv = hal_mdio_writeC45ByPort(unit, port, MMD_DEV_VSPEC2, blk_reg_addr, blk_reg_data);
                            if (AIR_E_OK == rv)
                            {
                                rv = hal_mdio_writeC45ByPort(unit, port, MMD_DEV_VSPEC2, AN8801SB_LED_BLINK_DURATION, \
                                        AN8801SB_LED_BLINK_RATE_DEFAULT);
                            }
                        }
                    }
                }
            }
        }
    }
    return rv;
}

/* FUNCTION NAME:   hal_an8801sb_phy_getPhyLedCtrlMode
 * PURPOSE:
 *      This API is used to get phy led control mode.
 * INPUT:
 *      unit            --  Device unit number
 *      port            --  Port number
 *      led_id          --  LED Id
 * OUTPUT:
 *      ptr_ctrl_mode   --  LED control mode
 * RETURN:
 *      AIR_E_OK
 * NOTES:
 *      None
 */
AIR_ERROR_NO_T
hal_an8801sb_phy_getPhyLedCtrlMode(
    const UI32_T                unit,
    const UI32_T                port,
    const UI32_T                led_id,
    HAL_PHY_LED_CTRL_MODE_T     *ptr_ctrl_mode)
{
    UI16_T          data = 0, offset = 0;;
    AIR_ERROR_NO_T  rv = AIR_E_OK;

    rv = hal_mdio_readC45ByPort(unit, port, MMD_DEV_VSPEC2, PHY_LED_CFG_REG, &data);
    if (AIR_E_OK == rv)
    {
        offset = (AN8801SB_LED_0_CFG_CTRL_MODE_BIT + (AN8801SB_LED_CFG_RG_OFFSET * led_id));
        *ptr_ctrl_mode = BITS_OFF_R(data, offset, AN8801SB_LED_CFG_CTRL_MODE_WIDTH);
    }
    return rv;
}

/* FUNCTION NAME:   hal_an8801sb_phy_setPhyLedForceState
 * PURPOSE:
 *      This API is used to set phy led force state.
 * INPUT:
 *      unit            --  Device unit number
 *      port            --  Port number
 *      led_id          --  LED Id
 *      state           --  LED force state enumeration type
 * OUTPUT:
 *      None
 * RETURN:
 *      AIR_E_OK
 *      AIR_E_BAD_PARAMETER
 * NOTES:
 *      None
 */
AIR_ERROR_NO_T
hal_an8801sb_phy_setPhyLedForceState(
    const UI32_T                unit,
    const UI32_T                port,
    const UI32_T                led_id,
    const HAL_PHY_LED_STATE_T   state)
{
    UI16_T                     reg_data = 0, reg_addr = 0, data = 0, offset = 0;
    AIR_ERROR_NO_T             rv = AIR_E_OK;
    BOOL_T                     is_force = FALSE;
    HAL_PHY_LED_CTRL_MODE_T    ctrl_mode;
    HAL_PHY_LED_PATT_T         led_patt;

    rv = hal_mdio_readC45ByPort(unit, port, MMD_DEV_VSPEC2, PHY_LED_CFG_REG, &data);
    if (AIR_E_OK == rv)
    {
        offset = (AN8801SB_LED_0_CFG_STATE_BIT + (AN8801SB_LED_CFG_RG_OFFSET * led_id));
        data &= ~(BITS_OFF_L(AN8801SB_LED_CFG_STATE_MASK, offset, AN8801SB_LED_CFG_STATE_WIDTH));
        data |= BITS_OFF_L(state, offset, AN8801SB_LED_CFG_STATE_WIDTH);
        rv = hal_mdio_writeC45ByPort(unit, port, MMD_DEV_VSPEC2, PHY_LED_CFG_REG, data);
    }

    if (AIR_E_OK == rv)
    {
        rv = hal_an8801sb_phy_getPhyLedCtrlMode(unit, port, led_id, &ctrl_mode);
        if (AIR_E_OK == rv)
        {
            if (HAL_PHY_LED_CTRL_MODE_FORCE == ctrl_mode)
            {
                is_force = (HAL_PHY_LED_STATE_FORCE_PATT == state) ? TRUE : FALSE;
                if (TRUE == is_force)
                {
                    rv = hal_an8801sb_phy_getPhyLedForcePattCfg(unit, port, led_id, &led_patt);
                    if (AIR_E_OK == rv)
                    {
                        rv = hal_an8801sb_phy_setPhyLedForcePattCfg(unit, port, led_id, led_patt);
                    }
                }
                else
                {
                    (HAL_PHY_LED_STATE_ON == state ? (reg_data = AN8801SB_LED_LINK_FORCE_ON) : (reg_data = 0));
                    LED_ID_TO_LED_REG_ADDRESS(led_id, PHY_LED_TYPE_ON, reg_addr);
                    rv = hal_mdio_writeC45ByPort(unit, port, MMD_DEV_VSPEC2, reg_addr, \
                            (reg_data | AN8801SB_LED_FUNC_ENABLE));
                    if (AIR_E_OK == rv)
                    {
                        LED_ID_TO_LED_REG_ADDRESS(led_id, PHY_LED_TYPE_BLK, reg_addr);
                        rv = hal_mdio_writeC45ByPort(unit, port, MMD_DEV_VSPEC2, reg_addr, 0);
                    }
                }
            }
        }
    }
    return rv;
}

/* FUNCTION NAME:   hal_an8801sb_phy_getPhyLedForceState
 * PURPOSE:
 *      This API is used to get phy led force state.
 * INPUT:
 *      unit            --  Device unit number
 *      port            --  Port number
 *      led_id          --  LED Id
 * OUTPUT:
 *      ptr_state       --  LED force state enumeration type
 * RETURN:
 *      AIR_E_OK
 * NOTES:
 *      None
 */
AIR_ERROR_NO_T
hal_an8801sb_phy_getPhyLedForceState(
    const UI32_T            unit,
    const UI32_T            port,
    const UI32_T            led_id,
    HAL_PHY_LED_STATE_T     *ptr_state)
{
    UI16_T          data = 0, offset = 0;;
    AIR_ERROR_NO_T  rv = AIR_E_OK;

    rv = hal_mdio_readC45ByPort(unit, port, MMD_DEV_VSPEC2, PHY_LED_CFG_REG, &data);
    if (AIR_E_OK == rv)
    {
        offset = (AN8801SB_LED_0_CFG_STATE_BIT + (AN8801SB_LED_CFG_RG_OFFSET * led_id));
        *ptr_state = BITS_OFF_R(data, offset, AN8801SB_LED_CFG_STATE_WIDTH);
    }
    return rv;
}

/* FUNCTION NAME:   hal_an8801sb_phy_setPhyLedForcePattCfg
 * PURPOSE:
 *      This API is used to set phy led force pattern.
 * INPUT:
 *      unit            --  Device unit number
 *      port            --  Port number
 *      led_id          --  LED Id
 *      pattern         --  LED force pattern enumeration type
 * OUTPUT:
 *      None
 * RETURN:
 *      AIR_E_OK
 *      AIR_E_BAD_PARAMETER
 * NOTES:
 *      None
 */
AIR_ERROR_NO_T
hal_an8801sb_phy_setPhyLedForcePattCfg(
    const UI32_T                unit,
    const UI32_T                port,
    const UI32_T                led_id,
    const HAL_PHY_LED_PATT_T    pattern)
{
    UI16_T                      reg_data = 0, reg_addr = 0, data = 0;
    AIR_ERROR_NO_T              rv = AIR_E_OK;
    HAL_PHY_LED_CTRL_MODE_T     ctrl_mode;

    switch (pattern)
    {
        case HAL_PHY_LED_PATT_HZ_HALF:
            reg_data = AN8801SB_LED_BLINK_RATE_HZ_HALF;
            break;
        case HAL_PHY_LED_PATT_HZ_ONE:
            reg_data = AN8801SB_LED_BLINK_RATE_HZ_ONE;
            break;
        case HAL_PHY_LED_PATT_HZ_TWO:
            reg_data = AN8801SB_LED_BLINK_RATE_HZ_TWO;
            break;
        default:
            rv = AIR_E_BAD_PARAMETER;
            break;
    }

    if (AIR_E_OK == rv)
    {
        data = BITS_OFF_L(pattern, AN8801SB_LED_CFG_PATT_OFFSET, AN8801SB_LED_CFG_PATT_WIDTH);
        rv = hal_mdio_writeC45ByPort(unit, port, MMD_DEV_VSPEC2, PHY_LED_BLK_CFG_REG, data);
    }

    if (AIR_E_OK == rv)
    {
        rv = hal_an8801sb_phy_getPhyLedCtrlMode(unit, port, led_id, &ctrl_mode);
        if (AIR_E_OK == rv)
        {
            if (HAL_PHY_LED_CTRL_MODE_FORCE == ctrl_mode)
            {
                rv = hal_mdio_writeC45ByPort(unit, port, MMD_DEV_VSPEC2, AN8801SB_LED_BLINK_DURATION, reg_data);
                if (AIR_E_OK == rv)
                {
                    LED_ID_TO_LED_REG_ADDRESS(led_id, PHY_LED_TYPE_BLK, reg_addr);
                    rv = hal_mdio_writeC45ByPort(unit, port, MMD_DEV_VSPEC2, reg_addr, AN8801SB_LED_BLINK_FORCE);
                    if (AIR_E_OK == rv)
                    {
                        LED_ID_TO_LED_REG_ADDRESS(led_id, PHY_LED_TYPE_ON, reg_addr);
                        rv = hal_mdio_writeC45ByPort(unit, port, MMD_DEV_VSPEC2, reg_addr, AN8801SB_LED_FUNC_ENABLE);
                    }
                }
            }
        }
    }
    return rv;
}

/* FUNCTION NAME:   hal_an8801sb_phy_getPhyLedForcePattCfg
 * PURPOSE:
 *      This API is used to get phy led force pattern.
 * INPUT:
 *      unit            --  Device unit number
 *      port            --  Port number
 *      led_id          --  LED Id
 * OUTPUT:
 *      ptr_pattern     --  LED force pattern enumeration type
 * RETURN:
 *      AIR_E_OK
 * NOTES:
 *      None
 */
AIR_ERROR_NO_T
hal_an8801sb_phy_getPhyLedForcePattCfg(
    const UI32_T            unit,
    const UI32_T            port,
    const UI32_T            led_id,
    HAL_PHY_LED_PATT_T      *ptr_pattern)
{
    UI16_T          data = 0;
    AIR_ERROR_NO_T  rv = AIR_E_OK;

    rv = hal_mdio_readC45ByPort(unit, port, MMD_DEV_VSPEC2, PHY_LED_BLK_CFG_REG, &data);
    if (AIR_E_OK == rv)
    {
        *ptr_pattern = BITS_OFF_R(data, AN8801SB_LED_CFG_PATT_OFFSET, AN8801SB_LED_CFG_PATT_WIDTH);
    }
    return rv;
}

/* FUNCTION NAME: hal_an8801sb_phy_triggerCableTest
 * PURPOSE:
 *      Trigger cable status.
 *
 * INPUT:
 *      unit            --  Device ID
 *      port            --  Select port number
 *      test_pair       --  Select test pair
 *                          HAL_PHY_CABLE_TEST_PAIR_A
 *                          HAL_PHY_CABLE_TEST_PAIR_B
 *                          HAL_PHY_CABLE_TEST_PAIR_C
 *                          HAL_PHY_CABLE_TEST_PAIR_D
 *                          HAL_PHY_CABLE_TEST_PAIR_ALL
 *
 * OUTPUT:
 *      ptr_test_rslt   --  Cable diagnostic information
 *                          HAL_PHY_CABLE_TEST_RSLT_T
 *
 * RETURN:
 *      AIR_E_OK
 *      AIR_E_NOT_SUPPORT
 *      AIR_E_BAD_PARAMETER
 *
 * NOTES:
 *      Support cable diagnostic in speed 1G only.
 */
AIR_ERROR_NO_T
hal_an8801sb_phy_triggerCableTest(
    const UI32_T                unit,
    const UI32_T                port,
    HAL_PHY_CABLE_TEST_PAIR_T  test_pair,
    HAL_PHY_CABLE_TEST_RSLT_T  *ptr_test_rslt)
{
    AIR_ERROR_NO_T ret = AIR_E_OK;

    ret = hal_cmn_phy_triggerCableTest(unit, port, test_pair, ptr_test_rslt);
    return ret;
}

/* FUNCTION NAME: hal_an8801sb_phy_getCableTestRawData
 * PURPOSE:
 *      Get cable ec training 4 pair raw date.
 *
 * INPUT:
 *      unit                --  Device ID
 *      port                --  Select port number
 *      test_pair           --  Select test pair
 *
 * OUTPUT:
 *      pptr_raw_data_all   --  Cable diagnostic raw information
 *
 *
 * RETURN:
 *      AIR_E_OK
 *      AIR_E_NOT_SUPPORT
 *      AIR_E_BAD_PARAMETER
 *
 * NOTES:
 *      Support cable diagnostic dump pair information.
 */
AIR_ERROR_NO_T
hal_an8801sb_phy_getCableTestRawData(
    const UI32_T                unit,
    const UI32_T                port,
    UI32_T                      **pptr_raw_data_all)
{
    AIR_ERROR_NO_T ret = AIR_E_OK;
    ret = hal_cmn_phy_getCableTestRawData(unit, port, pptr_raw_data_all);
    return ret;
}

/* FUNCTION NAME: hal_an8801sb_phy_setPhyLedGlbCfg
 * PURPOSE:
 *      Set LED global configuration.
 * INPUT:
 *      unit                     -- Device ID
 *      port                     -- Port ID
 *      ptr_glb_cfg              -- Global configuration
 *                                  HAL_PHY_LED_GLB_CFG_T
 * OUTPUT:
 *      None
 * RETURN:
 *      AIR_E_OK                 -- Operation success.
 *      AIR_E_BAD_PARAMETER      -- Parameter is wrong.
 * NOTES:
 *      None
 */
AIR_ERROR_NO_T
hal_an8801sb_phy_setPhyLedGlbCfg(
    const UI32_T            unit,
    const UI32_T            port,
    HAL_PHY_LED_GLB_CFG_T   *ptr_glb_cfg)
{
    AIR_ERROR_NO_T ret = AIR_E_OK;
    ret = hal_cmn_phy_setPhyLedGlbCfg(unit, port, ptr_glb_cfg);
    return ret;
}

/* FUNCTION NAME: hal_an8801sb_phy_getPhyLedGlbCfg
 * PURPOSE:
 *      Get LED global configuration.
 * INPUT:
 *      unit                     -- Device ID
 *      port                     -- Port ID
 * OUTPUT:
 *      ptr_glb_cfg              -- Global configuration
 *                                  HAL_PHY_LED_GLB_CFG_T
 * RETURN:
 *      AIR_E_OK                 -- Operation success.
 *      AIR_E_BAD_PARAMETER      -- Parameter is wrong.
 * NOTES:
 *      None
 */
AIR_ERROR_NO_T
hal_an8801sb_phy_getPhyLedGlbCfg(
    const UI32_T            unit,
    const UI32_T            port,
    HAL_PHY_LED_GLB_CFG_T   *ptr_glb_cfg)
{
    AIR_ERROR_NO_T ret = AIR_E_OK;
    ret = hal_cmn_phy_getPhyLedGlbCfg(unit, port, ptr_glb_cfg);
    return ret;
}

/* FUNCTION NAME: hal_an8801sb_phy_setPhyLedBlkEvent
 * PURPOSE:
 *      Set LED blinking event combination.
 * INPUT:
 *      unit                     -- Device ID
 *      port                     -- Port ID
 *      led_id                   -- LED ID
 *      evt_flags                -- Blinking event combination
 *                                  Refer to HAL_PHY_LED_BLK_EVT_FLAGS_XXX
 * OUTPUT:
 *      None
 * RETURN:
 *      AIR_E_OK                 -- Operation success.
 *      AIR_E_BAD_PARAMETER      -- Parameter is wrong.
 * NOTES:
 *      None
 */
AIR_ERROR_NO_T
hal_an8801sb_phy_setPhyLedBlkEvent(
    const UI32_T    unit,
    const UI32_T    port,
    const UI32_T    led_id,
    const UI32_T    evt_flags)
{
    AIR_ERROR_NO_T ret = AIR_E_OK;
    ret = hal_cmn_phy_setPhyLedBlkEvent(unit, port, led_id, evt_flags);
    return ret;
}

/* FUNCTION NAME: hal_an8801sb_phy_getPhyLedBlkEvent
 * PURPOSE:
 *      Get LED blinking event combination.
 * INPUT:
 *      unit                     -- Device ID
 *      port                     -- Port ID
 *      led_id                   -- LED ID
 * OUTPUT:
 *      ptr_evt_flags            -- Blinking event combination
 *                                  Refer to HAL_PHY_LED_BLK_EVT_FLAGS_XXX
 * RETURN:
 *      AIR_E_OK                 -- Operation success.
 *      AIR_E_BAD_PARAMETER      -- Parameter is wrong.
 * NOTES:
 *      None
 */
AIR_ERROR_NO_T
hal_an8801sb_phy_getPhyLedBlkEvent(
    const UI32_T    unit,
    const UI32_T    port,
    const UI32_T    led_id,
    UI32_T          *ptr_evt_flags)
{
    AIR_ERROR_NO_T ret = AIR_E_OK;
    ret = hal_cmn_phy_getPhyLedBlkEvent(unit, port, led_id, ptr_evt_flags);
    return ret;
}

/* FUNCTION NAME: hal_an8801sb_phy_setPhyLedDuration
 * PURPOSE:
 *      Set LED duration
 * INPUT:
 *      unit                     -- Device ID
 *      port                     -- Port ID
 *      mode                     -- Duration mode
 *                                  HAL_PHY_LED_BLK_CTRL_MODE_T
 *      time                     -- Duration time, unit: ms
 * OUTPUT:
 *      None
 * RETURN:
 *      AIR_E_OK                 -- Operation success.
 *      AIR_E_BAD_PARAMETER      -- Parameter is wrong.
 * NOTES:
 *      None
 */
AIR_ERROR_NO_T
hal_an8801sb_phy_setPhyLedDuration(
    const UI32_T                        unit,
    const UI32_T                        port,
    const HAL_PHY_LED_BLK_CTRL_MODE_T   mode,
    const UI32_T                        time)
{
    AIR_ERROR_NO_T ret = AIR_E_OK;
    ret = hal_cmn_phy_setPhyLedDuration(unit, port, mode, time);
    return ret;
}

/* FUNCTION NAME: hal_an8801sb_phy_getPhyLedDuration
 * PURPOSE:
 *      Get LED duration
 * INPUT:
 *      unit                     -- Device ID
 *      port                     -- Port ID
 *      mode                     -- Duration mode
 *                                  HAL_PHY_LED_BLK_CTRL_MODE_T
 * OUTPUT:
 *      ptr_time                 -- Duration time, unit: ms
 * RETURN:
 *      AIR_E_OK                 -- Operation success.
 *      AIR_E_BAD_PARAMETER      -- Parameter is wrong.
 * NOTES:
 *      None
 */
AIR_ERROR_NO_T
hal_an8801sb_phy_getPhyLedDuration(
    const UI32_T                        unit,
    const UI32_T                        port,
    const HAL_PHY_LED_BLK_CTRL_MODE_T   mode,
    UI32_T                              *ptr_time)
{
    AIR_ERROR_NO_T ret = AIR_E_OK;
    ret = hal_cmn_phy_getPhyLedDuration(unit, port, mode, ptr_time);
    return ret;
}

HAL_PHY_DRIVER_T
_ext_an8801sb_phy_func_vec =
{
    /* note: if function not support, fill "NULL". */
    hal_an8801sb_phy_init,
    hal_an8801sb_phy_setAdminState,
    hal_an8801sb_phy_getAdminState,
    hal_an8801sb_phy_setAutoNego,
    hal_an8801sb_phy_getAutoNego,
    hal_an8801sb_phy_setLocalAdvAbility,
    hal_an8801sb_phy_getLocalAdvAbility,
    hal_an8801sb_phy_getRemoteAdvAbility,
    hal_an8801sb_phy_setSpeed,
    hal_an8801sb_phy_getSpeed,
    hal_an8801sb_phy_setDuplex,
    hal_an8801sb_phy_getDuplex,
    hal_an8801sb_phy_getLinkStatus,
    hal_an8801sb_phy_setLoopBack,
    hal_an8801sb_phy_getLoopBack,
    hal_an8801sb_phy_setSmartSpeedDown,
    hal_an8801sb_phy_getSmartSpeedDown,
    hal_an8801sb_phy_setLedOnCtrl,
    hal_an8801sb_phy_getLedOnCtrl,
    hal_an8801sb_phy_testTxCompliance,
    NULL,
    NULL,
    NULL,
    NULL,
    hal_an8801sb_phy_setPhyLedCtrlMode,
    hal_an8801sb_phy_getPhyLedCtrlMode,
    hal_an8801sb_phy_setPhyLedForceState,
    hal_an8801sb_phy_getPhyLedForceState,
    hal_an8801sb_phy_setPhyLedForcePattCfg,
    hal_an8801sb_phy_getPhyLedForcePattCfg,
    hal_an8801sb_phy_triggerCableTest,
    hal_an8801sb_phy_getCableTestRawData,
    hal_an8801sb_phy_setPhyLedGlbCfg,
    hal_an8801sb_phy_getPhyLedGlbCfg,
    hal_an8801sb_phy_setPhyLedBlkEvent,
    hal_an8801sb_phy_getPhyLedBlkEvent,
    hal_an8801sb_phy_setPhyLedDuration,
    hal_an8801sb_phy_getPhyLedDuration,
    NULL,
    NULL,
    NULL,
};

AIR_ERROR_NO_T
hal_an8801sb_phy_getDriver(
    HAL_PHY_DRIVER_T **pptr_hal_driver)
{
    (*pptr_hal_driver) = (HAL_PHY_DRIVER_T *)&_ext_an8801sb_phy_func_vec;

    return (AIR_E_OK);
}

