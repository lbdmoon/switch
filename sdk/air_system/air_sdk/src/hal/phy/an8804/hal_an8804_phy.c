/*******************************************************************************
*  Copyright Statement:
*  --------------------
*  This software is protected by Copyright and the information contained
*  herein is confidential. The software may not be copied and the information
*  contained herein may not be used or disclosed except with the written
*  permission of Airoha Technology Corp. (C) 2021
*
*  BY OPENING THIS FILE, BUYER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
*  THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("AIROHA SOFTWARE")
*  RECEIVED FROM AIROHA AND/OR ITS REPRESENTATIVES ARE PROVIDED TO BUYER ON
*  AN "AS-IS" BASIS ONLY. AIROHA EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
*  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
*  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
*  NEITHER DOES AIROHA PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
*  SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
*  SUPPLIED WITH THE AIROHA SOFTWARE, AND BUYER AGREES TO LOOK ONLY TO SUCH
*  THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. AIROHA SHALL ALSO
*  NOT BE RESPONSIBLE FOR ANY AIROHA SOFTWARE RELEASES MADE TO BUYER'S
*  SPECIFICATION OR TO CONFORM TO A PARTICULAR STANDARD OR OPEN FORUM.
*
*  BUYER'S SOLE AND EXCLUSIVE REMEDY AND AIROHA'S ENTIRE AND CUMULATIVE
*  LIABILITY WITH RESPECT TO THE AIROHA SOFTWARE RELEASED HEREUNDER WILL BE,
*  AT AIROHA'S OPTION, TO REVISE OR REPLACE THE AIROHA SOFTWARE AT ISSUE,
*  OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY BUYER TO
*  AIROHA FOR SUCH AIROHA SOFTWARE AT ISSUE.
*
*  THE TRANSACTION CONTEMPLATED HEREUNDER SHALL BE CONSTRUED IN ACCORDANCE
*  WITH THE LAWS OF THE STATE OF CALIFORNIA, USA, EXCLUDING ITS CONFLICT OF
*  LAWS PRINCIPLES.  ANY DISPUTES, CONTROVERSIES OR CLAIMS ARISING THEREOF AND
*  RELATED THERETO SHALL BE SETTLED BY ARBITRATION IN SAN FRANCISCO, CA, UNDER
*  THE RULES OF THE INTERNATIONAL CHAMBER OF COMMERCE (ICC).
*
*******************************************************************************/

/* FILE NAME:  hal_an8804_phy.c
 * PURPOSE:
 *  Implement an8804 phy module HAL function.
 *
 * NOTES:
 *
 */

/* INCLUDE FILE DECLARTIONS
*/
#include <aml/aml.h>
#include <hal/common/hal.h>
#include <hal/common/hal_phy.h>
#include <hal/common/hal_cmn_phy.h>
#include <hal/common/hal_mdio.h>
#include <hal/common/hal_cfg.h>
#include <hal/switch/pearl/hal_pearl_perif.h>
#include <hal/phy/an8804/hal_an8804_phy.h>
#include <cmlib/cmlib_bit.h>

/* NAMING CONSTANT DECLARATIONS
*/
#define PHY_LED_BLK_CFG_REG             (AN8804_LED_3_ON_MASK)
#define PHY_LED_CFG_REG                 (AN8804_LED_3_BLK_MASK)

#define EN8804_10M_100M_LOW_POWER   (0x53aa)
#define EN8804_1G_LOW_POWER         (0x5faa)
#define EN8804_LONG_REACH           (0x5302)

#define PHY_LED_TYPE_ON                 (0)
#define PHY_LED_TYPE_BLK                (1)
/* MACRO FUNCTION DECLARATIONS
 */
#define SET_PHY_ABILITY(flag, cod, sb, result)    do    \
    {                                                   \
        if (flag & cod)                                 \
        {                                               \
            result |= sb;                               \
        }                                               \
        else                                            \
        {                                               \
            result &= ~(sb);                            \
        }                                               \
    }while(0)

#define GET_PHY_ABILITY(reg, cod, sb, result)    do     \
    {                                                   \
        if (reg & cod)                                  \
        {                                               \
            result |= sb;                               \
        }                                               \
    }while(0)

#define SERDES_NUMBER(phy, number)               do     \
    {                                                   \
        number = (phy == AN8804_PHY_ADDR_2) ? 1: 0;    \
    }while(0)

#define LED_ID_TO_LED_REG_ADDRESS(led_id, led_type, reg_addr) do    \
    {                                                               \
        ((led_type) == PHY_LED_TYPE_ON ?                            \
            ((reg_addr) = AN8804_LED_0_ON_MASK) :                  \
            ((reg_addr) = AN8804_LED_0_BLK_MASK));                 \
        (reg_addr) += (AN8804_LED_RG_OFFSET * led_id);             \
    }while(0)
/* DATA TYPE DECLARATIONS
*/

/* GLOBAL VARIABLE DECLARATIONS
*/
DIAG_SET_MODULE_INFO(AIR_MODULE_PHY, "hal_an8804_phy.c");

extern const HAL_PHY_CFG_T _hal_en8804_longreach[];
extern const UI32_T _hal_en8804_longreach_size;
extern const HAL_PHY_CFG_T _hal_en8804_normal[];
extern const UI32_T _hal_en8804_normal_size;
UI16_T en8804_1e_170[AIR_PORT_NUM] = {0};
UI16_T en8804_1e_12[AIR_PORT_NUM] = {0};
UI16_T en8804_1e_17[AIR_PORT_NUM] = {0};

/* STATIC VARIABLE DECLARATIONS */

/* LOCAL SUBPROGRAM SPECIFICATIONS
*/
extern void cl22_write(const UI8_T phyAddr, const UI8_T pageSel, const UI8_T regSel, const UI16_T wdata);
extern UI16_T cl22_read(const UI8_T phyAddr, const UI8_T pageSel, const UI8_T regSel);

static AIR_ERROR_NO_T
_genphy_update_link(UI16_T port, HAL_PHY_LINK_STATUS_T *ptr_status)
{
    AIR_ERROR_NO_T  rv = AIR_E_OK;
    UI16_T          status = 0;

    /* Do a fake read */
    rv = hal_mdio_readC22ByPort(0, port, MII_BMSR, &status);
    if (AIR_E_OK != rv)
    {
        return rv;
    }
    /* Read link and autonegotiation status */
    rv = hal_mdio_readC22ByPort(0, port, MII_BMSR, &status);
    if (AIR_E_OK != rv)
    {
        return rv;
    }

    if (status & BMSR_ANEGCOMPLETE)
    {
        ptr_status->flags |= HAL_PHY_LINK_STATUS_FLAGS_AUTO_NEGO_DONE;
    }
    else
    {
        ptr_status->flags &= ~(HAL_PHY_LINK_STATUS_FLAGS_AUTO_NEGO_DONE);
    }
    if (status & BMSR_RFAULT)
    {
        ptr_status->flags |= HAL_PHY_LINK_STATUS_FLAGS_REMOTE_FAULT;
    }
    else
    {
        ptr_status->flags &= ~(HAL_PHY_LINK_STATUS_FLAGS_REMOTE_FAULT);
    }

    /* workaround for AN8855 PBUS access GPHY link bit issue */
    rv = hal_mdio_readC45ByPort(0, port, 0x1E, 0xA2, &status);
    if (AIR_E_OK != rv)
    {
        return rv;
    }
    
    if (status & 0xE)
    {
        ptr_status->flags |= HAL_PHY_LINK_STATUS_FLAGS_LINK_UP;
    }
    else
    {
        ptr_status->flags &= ~(HAL_PHY_LINK_STATUS_FLAGS_LINK_UP);
    }
    
    return rv;
}

static AIR_ERROR_NO_T
_genphy_read_status(
    const UI32_T            unit,
    const UI32_T            port,
    HAL_PHY_LINK_STATUS_T   *ptr_status)
{
    AIR_ERROR_NO_T          rv = AIR_E_OK;
    HAL_PHY_AUTO_NEGO_T     auto_nego;
    UI16_T                  adv = 0, lpa = 0, lpagb = 0, bmcr = 0, common_adv_gb = 0, common_adv = 0;

    rv = _genphy_update_link(port, ptr_status);
    if (AIR_E_OK != rv)
    {
        return rv;
    }
    hal_an8804_phy_getAutoNego(unit, port, &auto_nego);

    if (HAL_PHY_AUTO_NEGO_ENABLE == auto_nego)
    {
        rv = hal_mdio_readC22ByPort(0, port, MII_STAT1000, &lpagb);
        if (AIR_E_OK != rv)
        {
            return rv;
        }
        rv = hal_mdio_readC22ByPort(0, port, MII_CTRL1000, &adv);
        if (AIR_E_OK != rv)
        {
            return rv;
        }
        common_adv_gb = lpagb & adv << 2;

        rv = hal_mdio_readC22ByPort(0, port, MII_LPA, &lpa);
        if (AIR_E_OK != rv)
        {
            return rv;
        }
        rv = hal_mdio_readC22ByPort(0, port, MII_ADVERTISE, &adv);
        if (AIR_E_OK != rv)
        {
            return rv;
        }
        common_adv = lpa & adv;

        ptr_status->speed = HAL_PHY_SPEED_10M;
        ptr_status->duplex = HAL_PHY_DUPLEX_HALF;
        /*phydev->pause = phydev->asym_pause = 0; */

        if (common_adv_gb & (LPA_1000FULL | LPA_1000HALF))
        {
            ptr_status->speed = HAL_PHY_SPEED_1000M;
            if (common_adv_gb & LPA_1000FULL)
            {
                ptr_status->duplex = HAL_PHY_DUPLEX_FULL;
            }
        }
        else if (common_adv & (LPA_100FULL | LPA_100HALF))
        {
            ptr_status->speed = HAL_PHY_SPEED_100M;
            if (common_adv & LPA_100FULL)
            {
                ptr_status->duplex = HAL_PHY_DUPLEX_FULL;
            }
        }
        else
        {
            if (common_adv & LPA_10FULL)
            {
                ptr_status->duplex = HAL_PHY_DUPLEX_FULL;
            }
        }
        /*
        if (ptr_status->duplex == DUPLEX_FULL)
        {
            phydev->pause = lpa & LPA_PAUSE_CAP ? 1 : 0;
            phydev->asym_pause = lpa & LPA_PAUSE_ASYM ? 1 : 0;
        }
        */
    }
    else
    {
        rv = hal_mdio_readC22ByPort(0, port, MII_BMCR, &bmcr);
        if (AIR_E_OK != rv)
        {
            return rv;
        }
        if (bmcr & BMCR_FULLDPLX)
        {
            ptr_status->duplex = HAL_PHY_DUPLEX_FULL;
        }
        else
        {
            ptr_status->duplex = HAL_PHY_DUPLEX_HALF;
        }
        if (bmcr & BMCR_SPEED1000)
        {
            ptr_status->speed = HAL_PHY_SPEED_1000M;
        }
        else if (bmcr & BMCR_SPEED100)
        {
            ptr_status->speed = HAL_PHY_SPEED_100M;
        }
        else
        {
            ptr_status->speed = HAL_PHY_SPEED_10M;
        }
        /* phydev->pause = phydev->asym_pause = 0; */
    }
    return rv;
}

/* FUNCTION NAME:   _hal_en8804_phy_applyParam
 * PURPOSE:
 *      Apply EN8804 PHY parameters
 * INPUT:
 *      unit            --  Device unit number
 *      port            --  Port number
 *
 * OUTPUT:
 *
 * RETURN:
 *      AIR_E_OK
 *      AIR_E_BAD_PARAMETER
 *
 * NOTES:
 *      None
 */
static AIR_ERROR_NO_T _hal_en8804_phy_applyParam(
    const UI32_T        unit,
    const UI32_T        port,
    const HAL_PHY_CFG_T phy_cfg)
{
    AIR_ERROR_NO_T      rv = AIR_E_OK;
    UI16_T              data = 0, check_data = 0;

    if (phy_cfg.delay_time)
    {
        osal_delayUs(phy_cfg.delay_time * 1000);
    }

    if ((HAL_PHY_MAX_DATA_MSB == phy_cfg.data_msb) && (HAL_PHY_MIN_DATA_LSB == phy_cfg.data_lsb))
    {
        if (HAL_PHY_ACCESS_METHOD_CL22 == phy_cfg.access_method)
        {
            rv = hal_mdio_writeC22ByPort(unit, port, phy_cfg.reg_addr, phy_cfg.reg_data);
        }
        else if (HAL_PHY_ACCESS_METHOD_CL22_TR == phy_cfg.access_method)
        {
            cl22_write(HAL_PHY_PORT_DEV_PHY_ADDR(unit, port), 0x2, phy_cfg.reg_addr, phy_cfg.reg_data);
        }
        else
        {
            rv = hal_mdio_writeC45ByPort(unit, port, phy_cfg.device_id, phy_cfg.reg_addr, phy_cfg.reg_data);
        }
    }
    else
    {
        if (HAL_PHY_ACCESS_METHOD_CL22 == phy_cfg.access_method)
        {
            rv = hal_mdio_readC22ByPort(unit, port, phy_cfg.reg_addr, &data);
        }
        else
        {
            rv = hal_mdio_readC45ByPort(unit, port, phy_cfg.device_id, phy_cfg.reg_addr, &data);
        }

        if (AIR_E_OK == rv)
        {
            if (HAL_PHY_ACCESS_METHOD_CL45_INC == phy_cfg.access_method)
            {
                check_data = BITS_OFF_R(data, phy_cfg.data_lsb, (phy_cfg.data_msb - phy_cfg.data_lsb));
                if (check_data > 0x3f)
                {
                    data &= ~(BITS(phy_cfg.data_lsb, phy_cfg.data_msb));
                    data |= BITS_OFF_L(0x3f, phy_cfg.data_lsb, (phy_cfg.data_msb - phy_cfg.data_lsb));
                }
                else
                {
                    data += (phy_cfg.reg_data << phy_cfg.data_lsb);
                }
            }
            else
            {
                data &= ~(BITS(phy_cfg.data_lsb, phy_cfg.data_msb));
                data |= (phy_cfg.reg_data << phy_cfg.data_lsb);
            }

            if (HAL_PHY_ACCESS_METHOD_CL22 == phy_cfg.access_method)
            {
                rv = hal_mdio_writeC22ByPort(unit, port, phy_cfg.reg_addr, data);
            }
            else
            {
                rv = hal_mdio_writeC45ByPort(unit, port, phy_cfg.device_id, phy_cfg.reg_addr, data);
            }
        }
    }
    return rv;
}

/* FUNCTION NAME:   _hal_en8804_phy_setPhyOpMode
 * PURPOSE:
 *      Set EN8804 PHY operation mode
 * INPUT:
 *      unit            --  Device unit number
 *      port            --  Port number
 *
 * OUTPUT:
 *
 * RETURN:
 *      AIR_E_OK
 *      AIR_E_BAD_PARAMETER
 *
 * NOTES:
 *      None
 */
static AIR_ERROR_NO_T _hal_en8804_phy_setPhyOpMode(
    const UI32_T            unit,
    const UI32_T            port,
    const HAL_PHY_OP_MODE_T mode)
{
    AIR_ERROR_NO_T      rv = AIR_E_OK;
    UI32_T              count = 0;
    HAL_PHY_CFG_T       phy_cfg;

    osal_memset(&phy_cfg, 0, sizeof(HAL_PHY_CFG_T));

    if (HAL_PHY_OP_MODE_NORMAL == mode)
    {
        for (count = 0; count <_hal_en8804_normal_size; count++)
        {
            if (AIR_E_OK == rv)
            {
                phy_cfg =_hal_en8804_normal[count];
                rv = _hal_en8804_phy_applyParam(unit, port, phy_cfg);
            }
        }
        
        /* Restore Efuse data */
        hal_mdio_writeC45ByPort(unit, port, 0x1e, 0x170, en8804_1e_170[port]);
        hal_mdio_writeC45ByPort(unit, port, 0x1e, 0x12, en8804_1e_12[port]);
        hal_mdio_writeC45ByPort(unit, port, 0x1e, 0x17, en8804_1e_17[port]);
    }
    else if (HAL_PHY_OP_MODE_LONG_REACH == mode)
    {
        /* Backup Efuse data */
        hal_mdio_readC45ByPort(unit, port, 0x1e, 0x170, &en8804_1e_170[port]);
        hal_mdio_readC45ByPort(unit, port, 0x1e, 0x12, &en8804_1e_12[port]);
        hal_mdio_readC45ByPort(unit, port, 0x1e, 0x17, &en8804_1e_17[port]);
        
        for (count = 0; count < _hal_en8804_longreach_size; count++)
        {
            if (AIR_E_OK == rv)
            {
                phy_cfg = _hal_en8804_longreach[count];
                rv = _hal_en8804_phy_applyParam(unit, port, phy_cfg);
            }
        }
    }
    else
    {
        rv = AIR_E_BAD_PARAMETER;
    }

    return rv;
}


/* EXPORTED SUBPROGRAM BODIES*/

/* FUNCTION NAME: hal_an8804_phy_init
 * PURPOSE:
 *      AN8804 PHY initialization
 *
 * INPUT:
 *      unit            --  Device ID
 *      port            --  PHY address
 *
 * OUTPUT:
 *        None
 *
 * RETURN:
 *        AIR_E_OK
 *        AIR_E_TIMEOUT
 *
 * NOTES:
 *      None
 */
AIR_ERROR_NO_T
hal_an8804_phy_init(
    const UI32_T    unit,
    const UI32_T    port)
{
    AIR_ERROR_NO_T  rv = AIR_E_OK;
    AIR_CFG_VALUE_T led_behavior, cfg;
    UI16_T          led_id = 0, led_config = 0, led_count = 0;
    UI16_T          link_reg_data = 0, blk_reg_data = 0, link_reg_addr = 0, blk_reg_addr = 0;
    HAL_PHY_LED_CTRL_MODE_T led_ctrl_mode;
    UI32_T          data = 0, gpio_pin = 0;

    osal_memset(&led_behavior, 0, sizeof(AIR_CFG_VALUE_T));
    led_behavior.value = AN8804_LED_COUNT;
    led_behavior.param0 = port;
    hal_cfg_getValue(0, AIR_CFG_TYPE_PHY_LED_COUNT, &led_behavior);
    led_count = led_behavior.value;
    
    /* LED configuration */
    for(led_id = 0; led_id < led_count; led_id++)
    {
        if (led_id == 0)
        {
            link_reg_addr = AN8804_LED_0_ON_MASK;
            blk_reg_addr = AN8804_LED_0_BLK_MASK;
        }
        else if (led_id == 1)
        {
            link_reg_addr = AN8804_LED_1_ON_MASK;
            blk_reg_addr = AN8804_LED_1_BLK_MASK;
        }
        else if (led_id == 2)
        {
            link_reg_addr = AN8804_LED_2_ON_MASK;
            blk_reg_addr = AN8804_LED_2_BLK_MASK;
        }
        else
        {
            link_reg_addr = AN8804_LED_3_ON_MASK;
            blk_reg_addr = AN8804_LED_3_BLK_MASK;
        }

        hal_an8804_phy_getPhyLedCtrlMode(0, port, led_id, &led_ctrl_mode);
        if (HAL_PHY_LED_CTRL_MODE_PHY == led_ctrl_mode)
        {
            osal_memset(&led_behavior, 0, sizeof(AIR_CFG_VALUE_T));

            led_behavior.value = 0xFFF;
            led_behavior.param0 = port;
            led_behavior.param1 = led_id;

            hal_cfg_getValue(0, AIR_CFG_TYPE_PHY_LED_BEHAVIOR, &led_behavior);
            led_config = led_behavior.value;

            link_reg_data = 0;
            blk_reg_data = 0;

            GET_PHY_ABILITY(led_config, AN8804_LED_BHV_LINK_1000, AN8804_LED_LINK_1000, link_reg_data);
            GET_PHY_ABILITY(led_config, AN8804_LED_BHV_LINK_100, AN8804_LED_LINK_100, link_reg_data);
            GET_PHY_ABILITY(led_config, AN8804_LED_BHV_LINK_10, AN8804_LED_LINK_10, link_reg_data);
            GET_PHY_ABILITY(led_config, AN8804_LED_BHV_LINK_FULLDPLX, AN8804_LED_LINK_FULLDPLX, link_reg_data);
            GET_PHY_ABILITY(led_config, AN8804_LED_BHV_LINK_HALFDPLX, AN8804_LED_LINK_HALFDPLX, link_reg_data);
            GET_PHY_ABILITY(led_config, AN8804_LED_BHV_HIGH_ACTIVE, AN8804_LED_POL_HIGH_ACT, link_reg_data);

            GET_PHY_ABILITY(led_config, AN8804_LED_BHV_BLINK_TX_1000, AN8804_LED_BLINK_TX_1000, blk_reg_data);
            GET_PHY_ABILITY(led_config, AN8804_LED_BHV_BLINK_RX_1000, AN8804_LED_BLINK_RX_1000, blk_reg_data);
            GET_PHY_ABILITY(led_config, AN8804_LED_BHV_BLINK_TX_100, AN8804_LED_BLINK_TX_100, blk_reg_data);
            GET_PHY_ABILITY(led_config, AN8804_LED_BHV_BLINK_RX_100, AN8804_LED_BLINK_RX_100, blk_reg_data);
            GET_PHY_ABILITY(led_config, AN8804_LED_BHV_BLINK_TX_10, AN8804_LED_BLINK_TX_10, blk_reg_data);
            GET_PHY_ABILITY(led_config, AN8804_LED_BHV_BLINK_RX_10, AN8804_LED_BLINK_RX_10, blk_reg_data);

            if (link_reg_data != 0 || blk_reg_data != 0)
            {
                link_reg_data |= AN8804_LED_FUNC_ENABLE;
            }
            
            rv |= hal_mdio_writeC45ByPort(unit, port, MMD_DEV_VSPEC2, link_reg_addr, link_reg_data);
            rv |= hal_mdio_writeC45ByPort(unit, port, MMD_DEV_VSPEC2, blk_reg_addr, blk_reg_data);
        }
    }

    /* init wave-gen and flash map */
    aml_readReg(unit, CYCLE_CFG_VALUE1_REG, &data, sizeof(data));
    data = (data & 0x000000ff) | WAVE_GEN_CYCLE_567;
    aml_writeReg(unit, CYCLE_CFG_VALUE1_REG, &data, sizeof(data));
    data = FLASH_MAP_ID_PATT_PARALLEL;
    aml_writeReg(unit, GPIO_FLASH_MAP_CFG0_REG, &data, sizeof(data));
    data = (data & 0x0000ffff) | (FLASH_MAP_ID_PATT_1 << 16) | (FLASH_MAP_ID_PATT_0 << 24);
    aml_writeReg(unit, GPIO_FLASH_MAP_CFG1_REG, &data, sizeof(data));
    data = PATTERN_HZ_HALF | (PATTERN_HZ_HALF << 16);
    aml_writeReg(unit, GPIO_FLASH_PRD_SET3_REG, &data, sizeof(data));
    if (led_behavior.value & (1 << 11))
    {
        /* high active */
        data = (PATTERN_OFF << 16) | PATTERN_ON;
    }
    else
    {
        /* low active */
        data = (PATTERN_ON << 16) | PATTERN_OFF;
    }
    aml_writeReg(unit, GPIO_FLASH_PRD_SET2_REG, &data, sizeof(data));

    osal_memset(&cfg, 0, sizeof(AIR_CFG_VALUE_T));
    hal_cfg_getValue(unit, AIR_CFG_TYPE_PHY_LED_TYPE, &cfg);
    if (LED_TYPE_PARALLEL == cfg.value)
    {
        cfg.param0 = port;
        cfg.param1 = 0;
        cfg.value = HAL_PEARL_PERIF_GPIO_PIN_COUNT;
        hal_cfg_getValue(unit, AIR_CFG_TYPE_GPIO_LED_MAP, &cfg);
        if (cfg.value >= HAL_PEARL_PERIF_GPIO_PIN_COUNT)
        {
            return AIR_E_OK;
        }
        gpio_pin = cfg.value;
    
        /* for parallel Led flash mode config */
        aml_readReg(unit, GPIO_FLASH_MODE_CFG_REG, &data, sizeof(UI32_T));
        data |= (1 << gpio_pin);
        rv |= aml_writeReg(unit, GPIO_FLASH_MODE_CFG_REG, &data, sizeof(UI32_T));
        /* direction output */
        rv |= aml_readReg(unit, GPIO_CONTROL_REG, &data, sizeof(UI32_T));
        data &= ~(0x3 << (gpio_pin * 2));
        data |= (0x1 << (gpio_pin * 2));
        rv |= aml_writeReg(unit, GPIO_CONTROL_REG, &data, sizeof(UI32_T));
        /* output enable */
        rv |= aml_readReg(unit, GPIO_OE_REG, &data, sizeof(UI32_T));
        data |= (1 << gpio_pin);
        rv |= aml_writeReg(unit, GPIO_OE_REG, &data, sizeof(UI32_T));
    }

    return AIR_E_OK;
}

/* FUNCTION NAME:   hal_an8804_phy_setAdminState
 * PURPOSE:
 *      This API is used to set port state.
 * INPUT:
 *      unit            --  Device unit number
 *      port            --  Port number
 *      state           --  Port state
 *
 * OUTPUT:
 *
 * RETURN:
 *      AIR_E_OK
 *      AIR_E_BAD_PARAMETER
 *
 * NOTES:
 *      None
 */
AIR_ERROR_NO_T
hal_an8804_phy_setAdminState(
    const UI32_T                unit,
    const UI32_T                port,
    const HAL_PHY_ADMIN_STATE_T state)
{
    AIR_ERROR_NO_T              rv = AIR_E_OK;
    UI16_T                      reg_value = 0;

    hal_mdio_readC22ByPort(0, port, MII_BMCR, &reg_value);
    if (HAL_PHY_ADMIN_STATE_DISABLE == state)
    {
        reg_value |= BMCR_PDOWN;
    }
    else
    {
        reg_value &= ~(BMCR_PDOWN);
    }
    hal_mdio_writeC22ByPort(0, port, MII_BMCR, reg_value);
    return rv;
}

/* FUNCTION NAME:   hal_an8804_phy_getAdminState
 * PURPOSE:
 *      This API is used to get port state.
 * INPUT:
 *      unit            --  Device unit number
 *      port            --  Port number
 *      ptr_state       --  Port state
 *
 * OUTPUT:
 *
 * RETURN:
 *      AIR_E_OK
 *      AIR_E_BAD_PARAMETER
 *
 * NOTES:
 *      None
 */
AIR_ERROR_NO_T
hal_an8804_phy_getAdminState(
    const UI32_T                unit,
    const UI32_T                port,
    HAL_PHY_ADMIN_STATE_T       *ptr_state)
{
    AIR_ERROR_NO_T              rv = AIR_E_OK;
    UI16_T                      reg_value = 0;

    hal_mdio_readC22ByPort(0, port, MII_BMCR, &reg_value);
    if (reg_value & BMCR_PDOWN)
    {
        *ptr_state = HAL_PHY_ADMIN_STATE_DISABLE;
    }
    else
    {
        *ptr_state = HAL_PHY_ADMIN_STATE_ENABLE;
    }
    return rv;
}

/* FUNCTION NAME:   hal_an8804_phy_setAutoNego
 * PURPOSE:
 *      This API is used to set port auto-negotiation.
 * INPUT:
 *      unit            --  Device unit number
 *      port            --  Port number
 *      auto_nego       --  Auto-negotiation
 *
 * OUTPUT:
 *
 * RETURN:
 *      AIR_E_OK
 *      AIR_E_BAD_PARAMETER
 *
 * NOTES:
 *      None
 */
AIR_ERROR_NO_T
hal_an8804_phy_setAutoNego(
    const UI32_T                unit,
    const UI32_T                port,
    const HAL_PHY_AUTO_NEGO_T   auto_nego)
{
    AIR_ERROR_NO_T              rv = AIR_E_OK;
    UI16_T                      reg_value = 0;

    hal_mdio_readC22ByPort(0, port, MII_BMCR, &reg_value);
    if (HAL_PHY_AUTO_NEGO_ENABLE == auto_nego)
    {
        reg_value |= BMCR_ANENABLE;
    }
    else if (HAL_PHY_AUTO_NEGO_RESTART == auto_nego)
    {
        reg_value |= BMCR_ANRESTART;
    }
    else
    {
        reg_value &= ~(BMCR_ANENABLE);
    }
    hal_mdio_writeC22ByPort(0, port, MII_BMCR, reg_value);
    return rv;
}

/* FUNCTION NAME:   hal_an8804_phy_getAutoNego
 * PURPOSE:
 *      This API is used to get port auto-negotiation.
 * INPUT:
 *      unit            --  Device unit number
 *      port            --  Port number
 *      ptr_auto_nego   --  Auto-negotiation
 *
 * OUTPUT:
 *
 * RETURN:
 *      AIR_E_OK
 *      AIR_E_BAD_PARAMETER
 *
 * NOTES:
 *      None
 */
AIR_ERROR_NO_T
hal_an8804_phy_getAutoNego(
    const UI32_T        unit,
    const UI32_T        port,
    HAL_PHY_AUTO_NEGO_T *ptr_auto_nego)
{
    AIR_ERROR_NO_T      rv = AIR_E_OK;
    UI16_T              reg_value = 0;

    hal_mdio_readC22ByPort(0, port, MII_BMCR, &reg_value);
    if (reg_value & BMCR_ANENABLE)
    {
        *ptr_auto_nego = HAL_PHY_AUTO_NEGO_ENABLE;
    }
    else
    {
        *ptr_auto_nego = HAL_PHY_AUTO_NEGO_DISABLE;
    }
    return rv;
}

/* FUNCTION NAME:   hal_an8804_phy_setLocalAdvAbility
 * PURPOSE:
 *      This API is used to set port local advertisment ability.
 * INPUT:
 *      unit            --  Device unit number
 *      port            --  Port number
 *      ptr_adv         --  Advertisement ability
 *
 * OUTPUT:
 *
 * RETURN:
 *      AIR_E_OK
 *      AIR_E_BAD_PARAMETER
 *
 * NOTES:
 *      None
 */
AIR_ERROR_NO_T
hal_an8804_phy_setLocalAdvAbility(
    const UI32_T            unit,
    const UI32_T            port,
    const HAL_PHY_AN_ADV_T  *ptr_adv)
{
    AIR_ERROR_NO_T          rv = AIR_E_OK;
    UI16_T                  reg_value = 0;

    rv = hal_mdio_readC22ByPort(0, port, MII_CTRL1000, &reg_value);
    if (AIR_E_OK == rv)
    {
        SET_PHY_ABILITY(ptr_adv->flags, HAL_PHY_AN_ADV_FLAGS_1000FUDX, ADVERTISE_1000FULL, reg_value);
        rv = hal_mdio_writeC22ByPort(0, port, MII_CTRL1000, reg_value);
    }

    if (AIR_E_OK == rv)
    {
        rv = hal_mdio_readC22ByPort(0, port, MII_ADVERTISE, &reg_value);
        if (AIR_E_OK == rv)
        {
            SET_PHY_ABILITY(ptr_adv->flags, HAL_PHY_AN_ADV_FLAGS_10HFDX, ADVERTISE_10HALF, reg_value);
            SET_PHY_ABILITY(ptr_adv->flags, HAL_PHY_AN_ADV_FLAGS_10FUDX, ADVERTISE_10FULL, reg_value);
            SET_PHY_ABILITY(ptr_adv->flags, HAL_PHY_AN_ADV_FLAGS_100HFDX, ADVERTISE_100HALF, reg_value);
            SET_PHY_ABILITY(ptr_adv->flags, HAL_PHY_AN_ADV_FLAGS_100FUDX, ADVERTISE_100FULL, reg_value);
            SET_PHY_ABILITY(ptr_adv->flags, HAL_PHY_AN_ADV_FLAGS_SYM_PAUSE, ADVERTISE_PAUSE_CAP, reg_value);
            SET_PHY_ABILITY(ptr_adv->flags, HAL_PHY_AN_ADV_FLAGS_ASYM_PAUSE, ADVERTISE_PAUSE_ASYM, reg_value);
            rv = hal_mdio_writeC22ByPort(0, port, MII_ADVERTISE, reg_value);
        }
    }

    if (AIR_E_OK == rv)
    {
        reg_value = (ptr_adv->flags & HAL_PHY_AN_ADV_FLAGS_EEE) ? (EEE_1000BASE_T | EEE_100BASE_TX ) : 0;
        rv = hal_mdio_writeC45ByPort(unit, port, MMD_DEV_ANEG, MMD_EEEAR, reg_value);
    }

    return rv;
}

/* FUNCTION NAME:   hal_an8804_phy_getLocalAdvAbility
 * PURPOSE:
 *      This API is used to get port local advertisment ability.
 * INPUT:
 *      unit            --  Device unit number
 *      port            --  Port number
 *      ptr_adv         --  Advertisement ability
 *
 * OUTPUT:
 *
 * RETURN:
 *      AIR_E_OK
 *      AIR_E_BAD_PARAMETER
 *
 * NOTES:
 *      None
 */
AIR_ERROR_NO_T
hal_an8804_phy_getLocalAdvAbility(
    const UI32_T        unit,
    const UI32_T        port,
    HAL_PHY_AN_ADV_T    *ptr_adv)
{
    AIR_ERROR_NO_T      rv = AIR_E_OK;
    UI16_T              reg_value = 0;

    ptr_adv->flags = 0;
    rv = hal_mdio_readC22ByPort(0, port, MII_CTRL1000, &reg_value);
    if (AIR_E_OK == rv)
    {
        GET_PHY_ABILITY(reg_value, ADVERTISE_1000FULL, HAL_PHY_AN_ADV_FLAGS_1000FUDX, ptr_adv->flags);
    }

    rv = hal_mdio_readC22ByPort(0, port, MII_ADVERTISE, &reg_value);
    if (AIR_E_OK == rv)
    {
        GET_PHY_ABILITY(reg_value, ADVERTISE_10HALF, HAL_PHY_AN_ADV_FLAGS_10HFDX, ptr_adv->flags);
        GET_PHY_ABILITY(reg_value, ADVERTISE_10FULL, HAL_PHY_AN_ADV_FLAGS_10FUDX, ptr_adv->flags);
        GET_PHY_ABILITY(reg_value, ADVERTISE_100HALF, HAL_PHY_AN_ADV_FLAGS_100HFDX, ptr_adv->flags);
        GET_PHY_ABILITY(reg_value, ADVERTISE_100FULL, HAL_PHY_AN_ADV_FLAGS_100FUDX, ptr_adv->flags);
        GET_PHY_ABILITY(reg_value, ADVERTISE_PAUSE_CAP, HAL_PHY_AN_ADV_FLAGS_SYM_PAUSE, ptr_adv->flags);
        GET_PHY_ABILITY(reg_value, ADVERTISE_PAUSE_ASYM, HAL_PHY_AN_ADV_FLAGS_ASYM_PAUSE, ptr_adv->flags);
    }

    rv = hal_mdio_readC45ByPort(unit, port, MMD_DEV_ANEG, MMD_EEEAR, &reg_value);
    if (AIR_E_OK == rv)
    {
        GET_PHY_ABILITY(reg_value, (EEE_1000BASE_T | EEE_100BASE_TX), HAL_PHY_AN_ADV_FLAGS_EEE, ptr_adv->flags);
    }

    return rv;
}

/* FUNCTION NAME:   hal_an8804_phy_getRemoteAdvAbility
 * PURPOSE:
 *      This API is used to get port remote advertisment ability.
 * INPUT:
 *      unit            --  Device unit number
 *      port            --  Port number
 *      ptr_adv         --  Advertisement ability
 *
 * OUTPUT:
 *
 * RETURN:
 *      AIR_E_OK
 *      AIR_E_BAD_PARAMETER
 *
 * NOTES:
 *      None
 */
AIR_ERROR_NO_T
hal_an8804_phy_getRemoteAdvAbility(
    const UI32_T        unit,
    const UI32_T        port,
    HAL_PHY_AN_ADV_T    *ptr_adv)
{
    AIR_ERROR_NO_T      rv = AIR_E_OK;
    UI16_T              reg_value = 0;

    ptr_adv->flags = 0;
    rv = hal_mdio_readC22ByPort(0, port, MII_STAT1000, &reg_value);
    if (AIR_E_OK == rv)
    {
        GET_PHY_ABILITY(reg_value, LPA_1000FULL, HAL_PHY_AN_ADV_FLAGS_1000FUDX, ptr_adv->flags);
    }

    rv = hal_mdio_readC22ByPort(0, port, MII_LPA, &reg_value);
    if (AIR_E_OK == rv)
    {
        GET_PHY_ABILITY(reg_value, LPA_10HALF, HAL_PHY_AN_ADV_FLAGS_10HFDX, ptr_adv->flags);
        GET_PHY_ABILITY(reg_value, LPA_10FULL, HAL_PHY_AN_ADV_FLAGS_10FUDX, ptr_adv->flags);
        GET_PHY_ABILITY(reg_value, LPA_100HALF, HAL_PHY_AN_ADV_FLAGS_100HFDX, ptr_adv->flags);
        GET_PHY_ABILITY(reg_value, LPA_100FULL, HAL_PHY_AN_ADV_FLAGS_100FUDX, ptr_adv->flags);
        GET_PHY_ABILITY(reg_value, LPA_PAUSE_CAP, HAL_PHY_AN_ADV_FLAGS_SYM_PAUSE, ptr_adv->flags);
        GET_PHY_ABILITY(reg_value, LPA_PAUSE_ASYM, HAL_PHY_AN_ADV_FLAGS_ASYM_PAUSE, ptr_adv->flags);
    }

    rv = hal_mdio_readC45ByPort(unit, port, MMD_DEV_ANEG, MMD_EEELPAR, &reg_value);
    if (AIR_E_OK == rv)
    {
        GET_PHY_ABILITY(reg_value, (EEE_1000BASE_T | EEE_100BASE_TX), HAL_PHY_AN_ADV_FLAGS_EEE, ptr_adv->flags);
    }

    return rv;
}

/* FUNCTION NAME:   hal_an8804_phy_setSpeed
 * PURPOSE:
 *      This API is used to set port speed.
 * INPUT:
 *      unit            --  Device unit number
 *      port            --  Port number
 *      speed           --  Port speed
 *
 * OUTPUT:
 *
 * RETURN:
 *      AIR_E_OK
 *      AIR_E_BAD_PARAMETER
 *
 * NOTES:
 *      None
 */
AIR_ERROR_NO_T
hal_an8804_phy_setSpeed(
    const UI32_T            unit,
    const UI32_T            port,
    const HAL_PHY_SPEED_T   speed)
{
    AIR_ERROR_NO_T          rv = AIR_E_OK;
    UI16_T                  reg_value = 0;

    if(speed >= HAL_PHY_SPEED_1000M)
    {
        return AIR_E_OP_INVALID;
    }

    if ((rv = hal_mdio_readC22ByPort(unit, port, MII_BMCR, &reg_value)) != AIR_E_OK)
    {
        return rv;
    }

    reg_value &= ~(BMCR_SPEED1000 | BMCR_SPEED100);

    if (HAL_PHY_SPEED_1000M == speed)
    {
        reg_value |= BMCR_SPEED1000;
    }
    else if (HAL_PHY_SPEED_100M == speed)
    {
        reg_value |= BMCR_SPEED100;
    }
    else if (HAL_PHY_SPEED_10M == speed)
    {
        reg_value |= 0;
    }
    else
    {
        return AIR_E_NOT_SUPPORT;
    }
    hal_mdio_writeC22ByPort(0, port, MII_BMCR, reg_value);
    return rv;
}

/* FUNCTION NAME:   hal_an8804_phy_getSpeed
 * PURPOSE:
 *      This API is used to get port speed.
 * INPUT:
 *      unit            --  Device unit number
 *      port            --  Port number
 *      ptr_speed       --  Port speed
 *
 * OUTPUT:
 *
 * RETURN:
 *      AIR_E_OK
 *      AIR_E_BAD_PARAMETER
 *
 * NOTES:
 *      None
 */
AIR_ERROR_NO_T
hal_an8804_phy_getSpeed(
    const UI32_T    unit,
    const UI32_T    port,
    HAL_PHY_SPEED_T *ptr_speed)
{
    UI16_T  reg_data = 0;
    AIR_ERROR_NO_T rv = AIR_E_OK;

    if ((rv = hal_mdio_readC22ByPort(unit, port, MII_BMCR, &reg_data)) != AIR_E_OK)
    {
        return rv;
    }

    if (reg_data & BMCR_SPEED1000)
    {
        *ptr_speed = HAL_PHY_SPEED_1000M;
    }
    else if (reg_data & BMCR_SPEED100)
    {
        *ptr_speed = HAL_PHY_SPEED_100M;
    }
    else
    {
        *ptr_speed = HAL_PHY_SPEED_10M;
    }

    return rv;
}

/* FUNCTION NAME:   hal_an8804_phy_setDuplex
 * PURPOSE:
 *      This API is used to set port duplex.
 * INPUT:
 *      unit            --  Device unit number
 *      port            --  Port number
 *      duplex          --  Port duplex
 *
 * OUTPUT:
 *
 * RETURN:
 *      AIR_E_OK
 *      AIR_E_BAD_PARAMETER
 *
 * NOTES:
 *      None
 */
AIR_ERROR_NO_T
hal_an8804_phy_setDuplex(
    const UI32_T            unit,
    const UI32_T            port,
    const HAL_PHY_DUPLEX_T  duplex)
{
    AIR_ERROR_NO_T          rv = AIR_E_OK;
    UI16_T                  reg_value = 0;

    hal_mdio_readC22ByPort(0, port, MII_BMCR, &reg_value);
    if (HAL_PHY_DUPLEX_FULL == duplex)
    {
        reg_value |= BMCR_FULLDPLX;
    }
    else if (HAL_PHY_DUPLEX_HALF == duplex)
    {
        reg_value &= ~(BMCR_FULLDPLX);
    }
    else
    {
        return AIR_E_NOT_SUPPORT;
    }
    hal_mdio_writeC22ByPort(0, port, MII_BMCR, reg_value);
    return rv;
}

/* FUNCTION NAME:   hal_an8804_phy_getDuplex
 * PURPOSE:
 *      This API is used to get port duplex.
 * INPUT:
 *      unit            --  Device unit number
 *      port            --  Port number
 *      ptr_duplex      --  Port duplex
 *
 * OUTPUT:
 *
 * RETURN:
 *      AIR_E_OK
 *      AIR_E_BAD_PARAMETER
 *
 * NOTES:
 *      None
 */
AIR_ERROR_NO_T
hal_an8804_phy_getDuplex(
    const UI32_T        unit,
    const UI32_T        port,
    HAL_PHY_DUPLEX_T    *ptr_duplex)
{
    UI16_T  reg_data = 0;
    AIR_ERROR_NO_T rv = AIR_E_OK;

    if ((rv = hal_mdio_readC22ByPort(unit, port, MII_BMCR, &reg_data)) != AIR_E_OK)
    {
        return rv;
    }

    if (reg_data & BMCR_FULLDPLX)
    {
        *ptr_duplex = HAL_PHY_DUPLEX_FULL;
    }
    else
    {
        *ptr_duplex = HAL_PHY_DUPLEX_HALF;
    }

    return rv;
}

/* FUNCTION NAME:   hal_an8804_phy_getLinkStatus
 * PURPOSE:
 *      This API is used to get port link status.
 * INPUT:
 *      unit            --  Device unit number
 *      port            --  Port number
 *      ptr_status      --  Link Status
 *
 * OUTPUT:
 *
 * RETURN:
 *      AIR_E_OK
 *      AIR_E_BAD_PARAMETER
 *
 * NOTES:
 *      None
 */
AIR_ERROR_NO_T
hal_an8804_phy_getLinkStatus(
    const UI32_T            unit,
    const UI32_T            port,
    HAL_PHY_LINK_STATUS_T   *ptr_status)
{
    AIR_ERROR_NO_T          rv = AIR_E_OK;

    rv = _genphy_read_status(unit, port, ptr_status);
    if (AIR_E_OK != rv)
    {
        return rv;
    }
    DIAG_PRINT(HAL_DBG_INFO, " (%u) port[%d] flags=(%x)\n", unit, port, ptr_status->flags);

    return rv;
}

/* FUNCTION NAME:   hal_an8804_phy_setLoopBack
 * PURPOSE:
 *      This API is used to get port link status.
 * INPUT:
 *      unit            --  Device unit number
 *      port            --  Port number
 *      lpbk_type       --  LookBack type
 *      enable          --  mode enable/disable
 *
 * OUTPUT:
 *
 * RETURN:
 *      AIR_E_OK
 *      AIR_E_BAD_PARAMETER
 *
 * NOTES:
 *      None
 */
AIR_ERROR_NO_T
hal_an8804_phy_setLoopBack(
    const UI32_T            unit,
    const UI32_T            port,
    const HAL_PHY_LPBK_T    lpbk_type,
    const BOOL_T            enable)
{
    UI16_T  reg_data = 0, page = 0;
    AIR_ERROR_NO_T rv = AIR_E_OK;

    if(HAL_PHY_LPBK_FAR_END == lpbk_type)
    {
        /* Backup page */
        if ((rv = hal_mdio_readC22ByPort(unit, port, MII_PAGE_SELECT, &page)) != AIR_E_OK)
        {
            return rv;
        }

        /* Swtich to page 1 */
        reg_data = 1;
        if ((rv = hal_mdio_writeC22ByPort(unit, port, MII_PAGE_SELECT, reg_data)) != AIR_E_OK)
        {
            return rv;
        }

        /* Read data from ExtReg1A */
        if ((rv = hal_mdio_readC22ByPort(unit, port, MII_RESV2, &reg_data)) != AIR_E_OK)
        {
            return rv;
        }

        if (TRUE == enable)
        {
            reg_data |= LPBK_FAR_END;
        }
        else
        {
            reg_data &= ~(LPBK_FAR_END);
        }

        if ((rv = hal_mdio_writeC22ByPort(unit, port, MII_RESV2, reg_data)) != AIR_E_OK)
        {
            return rv;
        }

        /* Restore page*/
        if ((rv = hal_mdio_writeC22ByPort(unit, port, MII_PAGE_SELECT, page)) != AIR_E_OK)
        {
            return rv;
        }
    }
    else
    {
        if ((rv = hal_mdio_readC22ByPort(unit, port, MII_BMCR, &reg_data)) != AIR_E_OK)
        {
            return rv;
        }

        if (TRUE == enable)
        {
            reg_data |= BMCR_LOOPBACK;
        }
        else
        {
            reg_data &= ~(BMCR_LOOPBACK);
        }

        if ((rv = hal_mdio_writeC22ByPort(unit, port, MII_BMCR, reg_data)) != AIR_E_OK)
        {
            return rv;
        }
    }

    return rv;
}

/* FUNCTION NAME:   hal_an8804_phy_getLoopBack
 * PURPOSE:
 *      This API is used to get port link status.
 * INPUT:
 *      unit            --  Device unit number
 *      port            --  Port number
 *      lpbk_type       --  LookBack type
 *
 * OUTPUT:
 *      ptr_enable      --  mode enable/disable
 * RETURN:
 *      AIR_E_OK
 *      AIR_E_BAD_PARAMETER
 *
 * NOTES:
 *      None
 */
AIR_ERROR_NO_T
hal_an8804_phy_getLoopBack(
    const UI32_T            unit,
    const UI32_T            port,
    const HAL_PHY_LPBK_T    lpbk_type,
    BOOL_T                  *ptr_enable)
{
    UI16_T  reg_data = 0, page = 0;
    AIR_ERROR_NO_T rv = AIR_E_OK;

    if(HAL_PHY_LPBK_FAR_END == lpbk_type)
    {
        /* Backup page */
        if ((rv = hal_mdio_readC22ByPort(unit, port, MII_PAGE_SELECT, &page)) != AIR_E_OK)
        {
            return rv;
        }

        /* Swtich to page 1 */
        reg_data = 1;
        if ((rv = hal_mdio_writeC22ByPort(unit, port, MII_PAGE_SELECT, reg_data)) != AIR_E_OK)
        {
            return rv;
        }

        /* Read data from ExtReg1A */
        if ((rv = hal_mdio_readC22ByPort(unit, port, MII_RESV2, &reg_data)) != AIR_E_OK)
        {
            return rv;
        }

        if (reg_data & LPBK_FAR_END)
        {
            *ptr_enable = TRUE;
        }
        else
        {
            *ptr_enable = FALSE;
        }

        /* Restore page*/
        if ((rv = hal_mdio_writeC22ByPort(unit, port, MII_PAGE_SELECT, page)) != AIR_E_OK)
        {
            return rv;
        }
    }
    else
    {
        if ((rv = hal_mdio_readC22ByPort(unit, port, MII_BMCR, &reg_data)) != AIR_E_OK)
        {
            return rv;
        }

        if (reg_data & BMCR_LOOPBACK)
        {
            *ptr_enable = TRUE;
        }
        else
        {
            *ptr_enable = FALSE;
        }
    }

    return rv;
}

/* FUNCTION NAME:   hal_an8804_phy_setSmartSpeedDown
 * PURPOSE:
 *      This API is used to gst port smart speed down.
 * INPUT:
 *      unit            --  Device unit number
 *      port            --  Port number
 *      ssd_mode        --  smart speed down mode
 *
 * OUTPUT:
 *
 * RETURN:
 *      AIR_E_OK
 *      AIR_E_BAD_PARAMETER
 *
 * NOTES:
 *      None
 */
AIR_ERROR_NO_T
hal_an8804_phy_setSmartSpeedDown(
    const UI32_T                unit,
    const UI32_T                port,
    const HAL_PHY_SSD_MODE_T    ssd_mode)
{
    UI16_T  reg_data = 0, page = 0, data = 0;
    AIR_ERROR_NO_T rv = AIR_E_OK;

    if ((HAL_PHY_SSD_MODE_1T == ssd_mode) || (HAL_PHY_SSD_MODE_5T < ssd_mode))
    {
        return AIR_E_BAD_PARAMETER;
    }

    /* Backup page */
    if ((rv = hal_mdio_readC22ByPort(unit, port, MII_PAGE_SELECT, &page)) != AIR_E_OK)
    {
        return rv;
    }

    /* Switch to page 1*/
    reg_data = 1;
    if ((rv = hal_mdio_writeC22ByPort(unit, port, MII_PAGE_SELECT, reg_data)) != AIR_E_OK)
    {
        return rv;
    }
    if ((rv = hal_mdio_readC22ByPort(unit, port, 0x14, &data)) != AIR_E_OK)
    {
        return rv;
    }

    data &= ~BITS(2,3);
    if(HAL_PHY_SSD_MODE_DISABLE != ssd_mode)
    {
        data |= BIT(4);
        data |= (ssd_mode - HAL_PHY_SSD_MODE_2T) << 2;
    }
    else
    {
        data &= ~BIT(4);
    }

    /* Switch to page 1*/
    reg_data = 1;
    if ((rv = hal_mdio_writeC22ByPort(unit, port, MII_PAGE_SELECT, reg_data)) != AIR_E_OK)
    {
        return rv;
    }

    if ((rv = hal_mdio_writeC22ByPort(unit, port, 0x14, data)) != AIR_E_OK)
    {
        return rv;
    }

    /* Restore page */
    if ((rv = hal_mdio_writeC22ByPort(unit, port, MII_PAGE_SELECT, page)) != AIR_E_OK)
    {
        return rv;
    }

    return rv;
}

/* FUNCTION NAME:   hal_an8804_phy_getSmartSpeedDown
 * PURPOSE:
 *      This API is used to get port power save.
 * INPUT:
 *      unit            --  Device unit number
 *      port            --  Port number
 *
 * OUTPUT:
 *      ptr_ssd_mode    --  smart speed down mode
 * RETURN:
 *      AIR_E_OK
 *      AIR_E_BAD_PARAMETER
 *
 * NOTES:
 *      None
 */
AIR_ERROR_NO_T
hal_an8804_phy_getSmartSpeedDown(
    const UI32_T        unit,
    const UI32_T        port,
    HAL_PHY_SSD_MODE_T  *ptr_ssd_mode)
{
    UI16_T  reg_data = 0, page = 0, data = 0;
    AIR_ERROR_NO_T rv = AIR_E_OK;

    /* Backup page */
    if ((rv = hal_mdio_readC22ByPort(unit, port, MII_PAGE_SELECT, &page)) != AIR_E_OK)
    {
        return rv;
    }

    /* Switch to page 1*/
    reg_data = 1;
    if ((rv = hal_mdio_writeC22ByPort(unit, port, MII_PAGE_SELECT, reg_data)) != AIR_E_OK)
    {
        return rv;
    }
    if ((rv = hal_mdio_readC22ByPort(unit, port, 0x14, &data)) != AIR_E_OK)
    {
        return rv;
    }

    reg_data = BITS_OFF_R(data, 4, 1);
    if (reg_data)
    {
        *ptr_ssd_mode = (BITS_OFF_R(data, 2, 2) + HAL_PHY_SSD_MODE_2T);
    }
    else
    {
        *ptr_ssd_mode = HAL_PHY_SSD_MODE_DISABLE;
    }

    /* Restore page */
    if ((rv = hal_mdio_writeC22ByPort(unit, port, MII_PAGE_SELECT, page)) != AIR_E_OK)
    {
        return rv;
    }

    return rv;
}

/* FUNCTION NAME:   hal_an8804_phy_setLedOnCtrl
 * PURPOSE:
 *      This API is used to set control of port LED.
 * INPUT:
 *      unit            --  Device unit number
 *      port            --  Port number
 *      led_id          --  LED ID
 *      enable          --  FALSE:Disable
 *                          TRUE: Enable
 *
 * OUTPUT:
 *      None
 * RETURN:
 *      AIR_E_OK
 * NOTES:
 *      None
 */
AIR_ERROR_NO_T
hal_an8804_phy_setLedOnCtrl(
    const UI32_T    unit,
    const UI32_T    port,
    const UI32_T    led_id,
    const BOOL_T    enable)
{
    AIR_ERROR_NO_T  rv = AIR_E_OK;
    return rv;
}


/* FUNCTION NAME:   hal_an8804_phy_getLedOnCtrl
 * PURPOSE:
 *      This API is used to get port LED control setting.
 * INPUT:
 *      unit            --  Device unit number
 *      port            --  Port number
 *      led_id          --  LED ID
 * OUTPUT:
 *      ptr_enable      --  FALSE:Disable
 *                          TRUE: Enable
 * RETURN:
 *      AIR_E_OK
 * NOTES:
 *      None
 */
AIR_ERROR_NO_T
hal_an8804_phy_getLedOnCtrl(
    const UI32_T    unit,
    const UI32_T    port,
    const UI32_T    led_id,
    BOOL_T          *ptr_enable)
{
    UI16_T          reg_data = 0, reg_addr = 0;
    AIR_ERROR_NO_T  rv = AIR_E_OK;

    if (led_id == 0)
    {
        reg_addr = AN8804_LED_0_ON_MASK;
    }
    else if (led_id == 1)
    {
        reg_addr = AN8804_LED_1_ON_MASK;
    }
    else if (led_id == 2)
    {
        reg_addr = AN8804_LED_2_ON_MASK;
    }
    else
    {
        reg_addr = AN8804_LED_3_ON_MASK;
    }

    rv = hal_mdio_readC45ByPort(unit, port, MMD_DEV_VSPEC2, reg_addr, &reg_data);
    if (AIR_E_OK == rv)
    {
        if (reg_data & AN8804_LED_LINK_FORCE_ON)
        {
            *ptr_enable = TRUE;
        }
        else
        {
            *ptr_enable = FALSE;
        }
    }
    return rv;
}

/* FUNCTION NAME:   hal_an8804_phy_testTxCompliance
 * PURPOSE:
 *      This API is used to set the Tx compliance mode.
 * INPUT:
 *      unit            --  Device unit number
 *      port            --  Port number
 *      mode            --  BIST mode
 * OUTPUT:
 *      None
 * RETURN:
 *      AIR_E_OK
 * NOTES:
 *      None
 */
AIR_ERROR_NO_T
hal_an8804_phy_testTxCompliance(
    const UI32_T                        unit,
    const UI32_T                        port,
    const HAL_PHY_TX_COMPLIANCE_MODE_T  mode)
{
    AIR_ERROR_NO_T ret = AIR_E_OK;
    ret = hal_cmn_phy_testTxCompliance(unit, port, mode);
    return ret;
}

/* FUNCTION NAME:   hal_an8804_phy_setPhyLedCtrlMode
 * PURPOSE:
 *      This API is used to set phy led control mode.
 * INPUT:
 *      unit            --  Device unit number
 *      port            --  Port number
 *      led_id          --  LED Id
 *      ctrl_mode       --  LED control mode enumeration type
 * OUTPUT:
 *      None
 * RETURN:
 *      AIR_E_OK
 *      AIR_E_BAD_PARAMETER
 * NOTES:
 *      None
 */
AIR_ERROR_NO_T
hal_an8804_phy_setPhyLedCtrlMode(
    const UI32_T                    unit,
    const UI32_T                    port,
    const UI32_T                    led_id,
    const HAL_PHY_LED_CTRL_MODE_T   ctrl_mode)
{
    UI32_T          src_ctrl = 0, phy_port = 0;
    UI32_T          reg_data = 0;
    AIR_ERROR_NO_T  rv = AIR_E_OK;
    AIR_CFG_VALUE_T cfg;

    HAL_AIR_PORT_TO_PHY_PORT(unit, port, phy_port);
    
    osal_memset(&cfg, 0, sizeof(AIR_CFG_VALUE_T));
    hal_cfg_getValue(unit, AIR_CFG_TYPE_PHY_LED_TYPE, &cfg);
    if (LED_TYPE_PARALLEL == cfg.value)
    {
        cfg.param0 = port;
        cfg.param1 = led_id;
        cfg.value = HAL_PEARL_PERIF_GPIO_PIN_COUNT;
        hal_cfg_getValue(unit, AIR_CFG_TYPE_GPIO_LED_MAP, &cfg);
        if (cfg.value >= HAL_PEARL_PERIF_GPIO_PIN_COUNT)
        {
            return AIR_E_OK;
        }

        src_ctrl = 1 << cfg.value;
        rv |= aml_readReg(unit, AN8804_RG_FORCE_GPIO0_EN, &reg_data, sizeof(UI32_T));
        if (HAL_PHY_LED_CTRL_MODE_PHY == ctrl_mode)
        {
            reg_data &= ~src_ctrl;
        }
        else if (HAL_PHY_LED_CTRL_MODE_FORCE == ctrl_mode)
        {
            reg_data |= src_ctrl;
        }
        else
        {
            return AIR_E_BAD_PARAMETER;
        }
        rv |= aml_writeReg(unit, AN8804_RG_FORCE_GPIO0_EN, &reg_data, sizeof(UI32_T));
        return rv;
    }

    if (0 == led_id)
    {
        src_ctrl = 0x1 << (phy_port * 3);
    }
    else if (1 == led_id)
    {
        src_ctrl = 0x2 << (phy_port * 3);
    }
    else if (2 == led_id)
    {
        src_ctrl = 0x4 << (phy_port * 3);
    }
    else
    {
        return AIR_E_BAD_PARAMETER;
    }
    aml_readReg(unit, SLED_SRC_SEL_REG, &reg_data, sizeof(UI32_T));

    if (HAL_PHY_LED_CTRL_MODE_PHY == ctrl_mode)
    {
        reg_data &= ~src_ctrl;
    }
    else if (HAL_PHY_LED_CTRL_MODE_FORCE == ctrl_mode)
    {
        reg_data |= src_ctrl;
    }
    else
    {
        return AIR_E_BAD_PARAMETER;
    }
    aml_writeReg(unit, SLED_SRC_SEL_REG, &reg_data, sizeof(UI32_T));
    return rv;
}


/* FUNCTION NAME:   hal_an8804_phy_getPhyLedCtrlMode
 * PURPOSE:
 *      This API is used to get phy led control mode.
 * INPUT:
 *      unit            --  Device unit number
 *      port            --  Port number
 *      led_id          --  LED Id
 * OUTPUT:
 *      ptr_ctrl_mode   --  LED control mode
 * RETURN:
 *      AIR_E_OK
 * NOTES:
 *      None
 */
AIR_ERROR_NO_T
hal_an8804_phy_getPhyLedCtrlMode(
    const UI32_T                unit,
    const UI32_T                port,
    const UI32_T                led_id,
    HAL_PHY_LED_CTRL_MODE_T     *ptr_ctrl_mode)
{
    UI32_T          src_ctrl = 0, phy_port = 0;
    UI32_T          reg_data = 0;
    AIR_ERROR_NO_T  rv = AIR_E_OK;
    AIR_CFG_VALUE_T cfg;

    HAL_AIR_PORT_TO_PHY_PORT(unit, port, phy_port);
    
    osal_memset(&cfg, 0, sizeof(AIR_CFG_VALUE_T));
    hal_cfg_getValue(unit, AIR_CFG_TYPE_PHY_LED_TYPE, &cfg);
    if (LED_TYPE_PARALLEL == cfg.value)
    {
        cfg.param0 = port;
        cfg.param1 = led_id;
        cfg.value = HAL_PEARL_PERIF_GPIO_PIN_COUNT;
        hal_cfg_getValue(unit, AIR_CFG_TYPE_GPIO_LED_MAP, &cfg);
        if (cfg.value >= HAL_PEARL_PERIF_GPIO_PIN_COUNT)
        {
            return AIR_E_ENTRY_NOT_FOUND;
        }

        src_ctrl = 1 << cfg.value;
        rv = aml_readReg(unit, AN8804_RG_FORCE_GPIO0_EN, &reg_data, sizeof(UI32_T));
        if (reg_data & src_ctrl)
        {
            *ptr_ctrl_mode = HAL_PHY_LED_CTRL_MODE_FORCE;
        }
        else
        {
            *ptr_ctrl_mode = HAL_PHY_LED_CTRL_MODE_PHY;
        }
        return rv;
    }

    if (0 == led_id)
    {
        src_ctrl = 0x1 << (phy_port * 3);
    }
    else if (1 == led_id)
    {
        src_ctrl = 0x2 << (phy_port * 3);
    }
    else if (2 == led_id)
    {
        src_ctrl = 0x4 << (phy_port * 3);
    }
    else
    {
        return AIR_E_BAD_PARAMETER;
    }
    aml_readReg(unit, SLED_SRC_SEL_REG, &reg_data, sizeof(UI32_T));

    if (reg_data & src_ctrl)
    {
        *ptr_ctrl_mode = HAL_PHY_LED_CTRL_MODE_FORCE;
    }
    else
    {
        *ptr_ctrl_mode = HAL_PHY_LED_CTRL_MODE_PHY;
    }
    return rv;
}


/* FUNCTION NAME:   hal_an8804_phy_setPhyLedForceState
 * PURPOSE:
 *      This API is used to set phy led force state.
 * INPUT:
 *      unit            --  Device unit number
 *      port            --  Port number
 *      led_id          --  LED Id
 *      state           --  LED force state enumeration type
 * OUTPUT:
 *      None
 * RETURN:
 *      AIR_E_OK
 *      AIR_E_BAD_PARAMETER
 * NOTES:
 *      None
 */
AIR_ERROR_NO_T
hal_an8804_phy_setPhyLedForceState(
    const UI32_T                unit,
    const UI32_T                port,
    const UI32_T                led_id,
    const HAL_PHY_LED_STATE_T   state)
{
    UI32_T          reg_data = 0, flash_id = 0, reg_addr = 0 , shift = 0, gpio_pin = 0;
    AIR_ERROR_NO_T  rv = AIR_E_OK;
    AIR_CFG_VALUE_T cfg;
    
    osal_memset(&cfg, 0, sizeof(AIR_CFG_VALUE_T));
    hal_cfg_getValue(unit, AIR_CFG_TYPE_PHY_LED_TYPE, &cfg);
    if (LED_TYPE_PARALLEL == cfg.value)
    {
        cfg.param0 = port;
        cfg.param1 = led_id;
        cfg.value = HAL_PEARL_PERIF_GPIO_PIN_COUNT;
        hal_cfg_getValue(unit, AIR_CFG_TYPE_GPIO_LED_MAP, &cfg);
        if (cfg.value >= HAL_PEARL_PERIF_GPIO_PIN_COUNT)
        {
            return AIR_E_OK;
        }
        
        if (HAL_PHY_LED_STATE_FORCE_PATT == state)
        {
            if (0 == led_id)
            {
                flash_id = FLASH_MAP_ID_PATT_0 & 0x0f;
            }
            else
            {
                return AIR_E_BAD_PARAMETER;
            }
        }
        else if (HAL_PHY_LED_STATE_OFF == state)
        {
            flash_id = FLASH_MAP_ID_OFF & 0x0f;
        }
        else if (HAL_PHY_LED_STATE_ON == state)
        {
            flash_id = FLASH_MAP_ID_ON & 0x0f;
        }
        else
        {
            return AIR_E_BAD_PARAMETER;
        }

        gpio_pin = cfg.value;
        if(gpio_pin >= 8)
        {
            reg_addr = GPIO_FLASH_MAP_CFG1_REG;
        }
        else
        {
            reg_addr = GPIO_FLASH_MAP_CFG0_REG;
        }
        shift = (gpio_pin & 0x07) * 4;
        rv |= aml_readReg(unit, reg_addr, &reg_data, sizeof(UI32_T));
        reg_data = (reg_data & ~(0x0f << shift)) | (flash_id << shift);
        rv |= aml_writeReg(unit, reg_addr, &reg_data, sizeof(UI32_T));
        return rv;
    }

    if (HAL_PHY_LED_STATE_FORCE_PATT == state)
    {
        if (0 == led_id)
        {
            flash_id = FLASH_MAP_ID_PATT_0;
        }
        else
        {
            flash_id = FLASH_MAP_ID_PATT_1;
        }
    }
    else if (HAL_PHY_LED_STATE_OFF == state)
    {
        flash_id = FLASH_MAP_ID_OFF;
    }
    else if (HAL_PHY_LED_STATE_ON == state)
    {
        flash_id = FLASH_MAP_ID_ON;
    }
    else
    {
        return AIR_E_BAD_PARAMETER;
    }
    /* set flash map */
    aml_readReg(unit, GPIO_FLASH_MAP_CFG1_REG, &reg_data, sizeof(UI32_T));
    if (0 == led_id)
    {
        reg_data = (reg_data & 0xff00ffff) | (flash_id << 16);
    }
    else if (1 == led_id)
    {
        reg_data = (reg_data & 0x00ffffff) | (flash_id << 24);
    }
    else
    {
        return AIR_E_BAD_PARAMETER;
    }
    aml_writeReg(unit, GPIO_FLASH_MAP_CFG1_REG, &reg_data, sizeof(UI32_T));

    return rv;
}


/* FUNCTION NAME:   hal_an8804_phy_getPhyLedForceState
 * PURPOSE:
 *      This API is used to get phy led force state.
 * INPUT:
 *      unit            --  Device unit number
 *      port            --  Port number
 *      led_id          --  LED Id
 * OUTPUT:
 *      ptr_state       --  LED force state enumeration type
 * RETURN:
 *      AIR_E_OK
 * NOTES:
 *      None
 */
AIR_ERROR_NO_T
hal_an8804_phy_getPhyLedForceState(
    const UI32_T            unit,
    const UI32_T            port,
    const UI32_T            led_id,
    HAL_PHY_LED_STATE_T     *ptr_state)
{
    UI32_T          reg_data = 0, reg_addr = 0, shift = 0, gpio_pin = 0;
    AIR_ERROR_NO_T  rv = AIR_E_OK;
    AIR_CFG_VALUE_T cfg;
    
    osal_memset(&cfg, 0, sizeof(AIR_CFG_VALUE_T));
    hal_cfg_getValue(unit, AIR_CFG_TYPE_PHY_LED_TYPE, &cfg);
    if (LED_TYPE_PARALLEL == cfg.value)
    {
        cfg.param0 = port;
        cfg.param1 = led_id;
        cfg.value = HAL_PEARL_PERIF_GPIO_PIN_COUNT;
        hal_cfg_getValue(unit, AIR_CFG_TYPE_GPIO_LED_MAP, &cfg);
        if (cfg.value >= HAL_PEARL_PERIF_GPIO_PIN_COUNT)
        {
            return AIR_E_ENTRY_NOT_FOUND;
        }

        gpio_pin = cfg.value;
        if(gpio_pin >= 8)
        {
            reg_addr = GPIO_FLASH_MAP_CFG1_REG;
        }
        else
        {
            reg_addr = GPIO_FLASH_MAP_CFG0_REG;
        }
        shift = (gpio_pin & 0x07) * 4;
        rv |= aml_readReg(unit, reg_addr, &reg_data, sizeof(UI32_T));
        reg_data = (reg_data >> shift) & 0x0000000f;

        if ((FLASH_MAP_ID_PATT_0 & 0x0f) == reg_data)
        {
            *ptr_state = HAL_PHY_LED_STATE_FORCE_PATT;
        }
        else if ((FLASH_MAP_ID_OFF & 0x0f) == reg_data)
        {
            *ptr_state = HAL_PHY_LED_STATE_OFF;
        }
        else if ((FLASH_MAP_ID_ON & 0x0f) == reg_data)
        {
            *ptr_state = HAL_PHY_LED_STATE_ON;
        }
        else
        {
            return AIR_E_NOT_SUPPORT;
        }
        return rv;
    }

    aml_readReg(unit, GPIO_FLASH_MAP_CFG1_REG, &reg_data, sizeof(UI32_T));
    if (0 == led_id)
    {
        reg_data = (reg_data >> 16) & 0x000000ff;
    }
    else if (1 == led_id)
    {
        reg_data = (reg_data >> 24);
    }
    else
    {
        return AIR_E_BAD_PARAMETER;
    }
    if (FLASH_MAP_ID_PATT_0 == reg_data || FLASH_MAP_ID_PATT_1 == reg_data)
    {
        *ptr_state = HAL_PHY_LED_STATE_FORCE_PATT;
    }
    else if (FLASH_MAP_ID_OFF == reg_data)
    {
        *ptr_state = HAL_PHY_LED_STATE_OFF;
    }
    else if (FLASH_MAP_ID_ON == reg_data)
    {
        *ptr_state = HAL_PHY_LED_STATE_ON;
    }
    else
    {
        return AIR_E_NOT_SUPPORT;
    }
    return rv;
}


/* FUNCTION NAME:   hal_an8804_phy_setPhyLedForcePattCfg
 * PURPOSE:
 *      This API is used to set phy led force pattern.
 * INPUT:
 *      unit            --  Device unit number
 *      port            --  Port number
 *      led_id          --  LED Id
 *      pattern         --  LED force pattern enumeration type
 * OUTPUT:
 *      None
 * RETURN:
 *      AIR_E_OK
 *      AIR_E_BAD_PARAMETER
 * NOTES:
 *      None
 */
AIR_ERROR_NO_T
hal_an8804_phy_setPhyLedForcePattCfg(
    const UI32_T                unit,
    const UI32_T                port,
    const UI32_T                led_id,
    const HAL_PHY_LED_PATT_T    pattern)
{
    UI32_T          reg_data = 0, wg_period = 0;
    UI32_T          wg_cycle = 0;
    AIR_ERROR_NO_T  rv = AIR_E_OK;

    if (HAL_PHY_LED_PATT_HZ_HALF == pattern)
    {
        wg_period = PATTERN_HZ_HALF;
        wg_cycle = WAVE_GEN_CYCLE_MAX;
    }
    else if (HAL_PHY_LED_PATT_HZ_ONE == pattern)
    {
        wg_period = PATTERN_HZ_ONE;
        wg_cycle = WAVE_GEN_CYCLE_MAX;
    }
    else if (HAL_PHY_LED_PATT_HZ_TWO == pattern)
    {
        wg_period = PATTERN_HZ_TWO;
        wg_cycle = WAVE_GEN_CYCLE_MAX;
    }
    else if (HAL_PHY_LED_PATT_HZ_ZERO == pattern)
    {
        wg_period = PATTERN_HZ_ZERO;
        wg_cycle = WAVE_GEN_CYCLE_MIN;
    }
    else
    {
        return AIR_E_BAD_PARAMETER;
    }

    /* set wave-gen pattern */
    aml_readReg(unit, GPIO_FLASH_PRD_SET3_REG, &reg_data, sizeof(UI32_T));
    if (0 == led_id)
    {
        reg_data = (reg_data & 0xffff0000) | wg_period;
    }
    else if (1 == led_id)
    {
        reg_data = (reg_data & 0x0000ffff) | (wg_period << 16);
    }
    else
    {
        return AIR_E_BAD_PARAMETER;
    }
    aml_writeReg(unit, GPIO_FLASH_PRD_SET3_REG, &reg_data, sizeof(UI32_T));

    /* set wave-gen cycle */
    aml_readReg(unit, CYCLE_CFG_VALUE1_REG, &reg_data, sizeof(UI32_T));
    if (0 == led_id)
    {
        reg_data = (reg_data & 0xff00ffff) | (wg_cycle << 16);
    }
    else if (1 == led_id)
    {
        reg_data = (reg_data & 0x00ffffff) | (wg_cycle << 24);
    }
    else
    {
        return AIR_E_BAD_PARAMETER;
    }
    aml_writeReg(unit, CYCLE_CFG_VALUE1_REG, &reg_data, sizeof(UI32_T));
    return rv;
}


/* FUNCTION NAME:   hal_an8804_phy_getPhyLedForcePattCfg
 * PURPOSE:
 *      This API is used to get phy led force pattern.
 * INPUT:
 *      unit            --  Device unit number
 *      port            --  Port number
 *      led_id          --  LED Id
 * OUTPUT:
 *      ptr_pattern     --  LED force pattern enumeration type
 * RETURN:
 *      AIR_E_OK
 * NOTES:
 *      None
 */
AIR_ERROR_NO_T
hal_an8804_phy_getPhyLedForcePattCfg(
    const UI32_T            unit,
    const UI32_T            port,
    const UI32_T            led_id,
    HAL_PHY_LED_PATT_T      *ptr_pattern)
{
    UI32_T                  reg_data = 0;
    AIR_ERROR_NO_T          rv = AIR_E_OK;

    /* get wave-gen pattern */
    aml_readReg(unit, GPIO_FLASH_PRD_SET3_REG, &reg_data, sizeof(UI32_T));
    if (0 == led_id)
    {
        reg_data = (reg_data & 0x0000ffff);
    }
    else if (1 == led_id)
    {
        reg_data = (reg_data >> 16);
    }
    else
    {
        return AIR_E_BAD_PARAMETER;
    }

    if (PATTERN_HZ_HALF == reg_data)
    {
        *ptr_pattern = HAL_PHY_LED_PATT_HZ_HALF;
    }
    else if (PATTERN_HZ_ONE == reg_data)
    {
        *ptr_pattern = HAL_PHY_LED_PATT_HZ_ONE;
    }
    else if (PATTERN_HZ_TWO == reg_data)
    {
        *ptr_pattern = HAL_PHY_LED_PATT_HZ_TWO;
    }
    else if (PATTERN_HZ_ZERO == reg_data)
    {
        *ptr_pattern = HAL_PHY_LED_PATT_HZ_ZERO;
    }
    else
    {
        return AIR_E_BAD_PARAMETER;
    }
    return rv;
}


/* FUNCTION NAME: hal_an8804_phy_triggerCableTest
 * PURPOSE:
 *      Trigger cable status.
 *
 * INPUT:
 *      unit            --  Device ID
 *      port            --  Select port number
 *      test_pair       --  Select test pair
 *                          HAL_PHY_CABLE_TEST_PAIR_A
 *                          HAL_PHY_CABLE_TEST_PAIR_B
 *                          HAL_PHY_CABLE_TEST_PAIR_C
 *                          HAL_PHY_CABLE_TEST_PAIR_D
 *                          HAL_PHY_CABLE_TEST_PAIR_ALL
 *
 * OUTPUT:
 *      ptr_test_rslt   --  Cable diagnostic information
 *                          HAL_PHY_CABLE_TEST_RSLT_T
 *
 * RETURN:
 *      AIR_E_OK
 *      AIR_E_NOT_SUPPORT
 *      AIR_E_BAD_PARAMETER
 *
 * NOTES:
 *      Support cable diagnostic in speed 1G only.
 */
AIR_ERROR_NO_T
hal_an8804_phy_triggerCableTest(
    const UI32_T                unit,
    const UI32_T                port,
    HAL_PHY_CABLE_TEST_PAIR_T  test_pair,
    HAL_PHY_CABLE_TEST_RSLT_T  *ptr_test_rslt)
{
    AIR_ERROR_NO_T ret = AIR_E_NOT_SUPPORT;
    ret = hal_cmn_phy_triggerCableTest(unit, port, test_pair, ptr_test_rslt);
    return ret;
}

/* FUNCTION NAME: hal_an8804_phy_getCableTestRawData
 * PURPOSE:
 *      Get cable ec training 4 pair raw date.
 *
 * INPUT:
 *      unit                --  Device ID
 *      port                --  Select port number
 *      test_pair           --  Select test pair
 *
 * OUTPUT:
 *      pptr_raw_data_all   --  Cable diagnostic raw information
 *
 *
 * RETURN:
 *      AIR_E_OK
 *      AIR_E_NOT_SUPPORT
 *      AIR_E_BAD_PARAMETER
 *
 * NOTES:
 *      Support cable diagnostic dump pair information.
 */
AIR_ERROR_NO_T
hal_an8804_phy_getCableTestRawData(
    const UI32_T                unit,
    const UI32_T                port,
    UI32_T                      **pptr_raw_data_all)
{
    AIR_ERROR_NO_T ret = AIR_E_NOT_SUPPORT;
    ret = hal_cmn_phy_getCableTestRawData(unit, port, pptr_raw_data_all);
    return ret;
}

/* FUNCTION NAME: hal_an8804_phy_setPhyLedGlbCfg
 * PURPOSE:
 *      Set LED global configuration.
 * INPUT:
 *      unit                     -- Device ID
 *      port                     -- Port ID
 *      ptr_glb_cfg              -- Global configuration
 *                                  HAL_PHY_LED_GLB_CFG_T
 * OUTPUT:
 *      None
 * RETURN:
 *      AIR_E_OK                 -- Operation success.
 *      AIR_E_BAD_PARAMETER      -- Parameter is wrong.
 * NOTES:
 *      None
 */
AIR_ERROR_NO_T
hal_an8804_phy_setPhyLedGlbCfg(
    const UI32_T            unit,
    const UI32_T            port,
    HAL_PHY_LED_GLB_CFG_T   *ptr_glb_cfg)
{
    AIR_ERROR_NO_T ret = AIR_E_OK;
    ret = hal_cmn_phy_setPhyLedGlbCfg(unit, port, ptr_glb_cfg);
    return ret;
}

/* FUNCTION NAME: hal_an8804_phy_getPhyLedGlbCfg
 * PURPOSE:
 *      Get LED global configuration.
 * INPUT:
 *      unit                     -- Device ID
 *      port                     -- Port ID
 * OUTPUT:
 *      ptr_glb_cfg              -- Global configuration
 *                                  HAL_PHY_LED_GLB_CFG_T
 * RETURN:
 *      AIR_E_OK                 -- Operation success.
 *      AIR_E_BAD_PARAMETER      -- Parameter is wrong.
 * NOTES:
 *      None
 */
AIR_ERROR_NO_T
hal_an8804_phy_getPhyLedGlbCfg(
    const UI32_T            unit,
    const UI32_T            port,
    HAL_PHY_LED_GLB_CFG_T   *ptr_glb_cfg)
{
    AIR_ERROR_NO_T ret = AIR_E_OK;
    ret = hal_cmn_phy_getPhyLedGlbCfg(unit, port, ptr_glb_cfg);
    return ret;
}

/* FUNCTION NAME: hal_an8804_phy_setPhyLedBlkEvent
 * PURPOSE:
 *      Set LED blinking event combination.
 * INPUT:
 *      unit                     -- Device ID
 *      port                     -- Port ID
 *      led_id                   -- LED ID
 *      evt_flags                -- Blinking event combination
 *                                  Refer to HAL_PHY_LED_BLK_EVT_FLAGS_XXX
 * OUTPUT:
 *      None
 * RETURN:
 *      AIR_E_OK                 -- Operation success.
 *      AIR_E_BAD_PARAMETER      -- Parameter is wrong.
 * NOTES:
 *      None
 */
AIR_ERROR_NO_T
hal_an8804_phy_setPhyLedBlkEvent(
    const UI32_T    unit,
    const UI32_T    port,
    const UI32_T    led_id,
    const UI32_T    evt_flags)
{
    AIR_ERROR_NO_T ret = AIR_E_OK;
    ret = hal_cmn_phy_setPhyLedBlkEvent(unit, port, led_id, evt_flags);
    return ret;
}

/* FUNCTION NAME: hal_an8804_phy_getPhyLedBlkEvent
 * PURPOSE:
 *      Get LED blinking event combination.
 * INPUT:
 *      unit                     -- Device ID
 *      port                     -- Port ID
 *      led_id                   -- LED ID
 * OUTPUT:
 *      ptr_evt_flags            -- Blinking event combination
 *                                  Refer to HAL_PHY_LED_BLK_EVT_FLAGS_XXX
 * RETURN:
 *      AIR_E_OK                 -- Operation success.
 *      AIR_E_BAD_PARAMETER      -- Parameter is wrong.
 * NOTES:
 *      None
 */
AIR_ERROR_NO_T
hal_an8804_phy_getPhyLedBlkEvent(
    const UI32_T    unit,
    const UI32_T    port,
    const UI32_T    led_id,
    UI32_T          *ptr_evt_flags)
{
    AIR_ERROR_NO_T ret = AIR_E_OK;
    ret = hal_cmn_phy_getPhyLedBlkEvent(unit, port, led_id, ptr_evt_flags);
    return ret;
}

/* FUNCTION NAME: hal_an8804_phy_setPhyLedDuration
 * PURPOSE:
 *      Set LED duration
 * INPUT:
 *      unit                     -- Device ID
 *      port                     -- Port ID
 *      mode                     -- Duration mode
 *                                  HAL_PHY_LED_BLK_CTRL_MODE_T
 *      time                     -- Duration time, unit: ms
 * OUTPUT:
 *      None
 * RETURN:
 *      AIR_E_OK                 -- Operation success.
 *      AIR_E_BAD_PARAMETER      -- Parameter is wrong.
 * NOTES:
 *      None
 */
AIR_ERROR_NO_T
hal_an8804_phy_setPhyLedDuration(
    const UI32_T                        unit,
    const UI32_T                        port,
    const HAL_PHY_LED_BLK_CTRL_MODE_T   mode,
    const UI32_T                        time)
{
    AIR_ERROR_NO_T ret = AIR_E_OK;
    ret = hal_cmn_phy_setPhyLedDuration(unit, port, mode, time);
    return ret;
}

/* FUNCTION NAME: hal_an8804_phy_getPhyLedDuration
 * PURPOSE:
 *      Get LED duration
 * INPUT:
 *      unit                     -- Device ID
 *      port                     -- Port ID
 *      mode                     -- Duration mode
 *                                  HAL_PHY_LED_BLK_CTRL_MODE_T
 * OUTPUT:
 *      ptr_time                 -- Duration time, unit: ms
 * RETURN:
 *      AIR_E_OK                 -- Operation success.
 *      AIR_E_BAD_PARAMETER      -- Parameter is wrong.
 * NOTES:
 *      None
 */
AIR_ERROR_NO_T
hal_an8804_phy_getPhyLedDuration(
    const UI32_T                        unit,
    const UI32_T                        port,
    const HAL_PHY_LED_BLK_CTRL_MODE_T   mode,
    UI32_T                              *ptr_time)
{
    AIR_ERROR_NO_T ret = AIR_E_OK;
    ret = hal_cmn_phy_getPhyLedDuration(unit, port, mode, ptr_time);
    return ret;
}

/* FUNCTION NAME: hal_en8804_phy_setPhyOpMode
 * PURPOSE:
 *      Set Phy operation mode.
 *
 * INPUT:
 *      unit                     -- Device ID
 *      port                     -- Port ID
 *      mode                     -- Phy operation mode
 *                                  AIR_PORT_OP_MODE_T
 * OUTPUT:
 *      None
 * RETURN:
 *      AIR_E_OK                 -- Operation success.
 *      AIR_E_BAD_PARAMETER      -- Parameter is wrong.
 *      AIR_E_NOT_SUPPORT        -- Feature is not supported.
 * NOTES:
 *      None
 */
AIR_ERROR_NO_T
hal_en8804_phy_setPhyOpMode(
    const UI32_T                        unit,
    const UI32_T                        port,
    const HAL_PHY_OP_MODE_T             mode)
{
    AIR_ERROR_NO_T rv = AIR_E_OK;
    rv = _hal_en8804_phy_setPhyOpMode(unit, port, mode);
    return rv;
}

/* FUNCTION NAME: hal_en8804_phy_getPhyOpMode
 * PURPOSE:
 *      Get Phy operation mode.
 * INPUT:
 *      unit                     -- Device ID
 *      port                     -- Port ID
 *
 * OUTPUT:
 *      ptr_mode                 -- Phy operation mode enumeration type
 *                                  AIR_PORT_OP_MODE_T
 * RETURN:
 *      AIR_E_OK                 -- Operation success.
 *      AIR_E_BAD_PARAMETER      -- Parameter is wrong.
 *      AIR_E_NOT_SUPPORT        -- Feature is not supported.
 * NOTES:
 *      None
 */
AIR_ERROR_NO_T
hal_en8804_phy_getPhyOpMode(
    const UI32_T                        unit,
    const UI32_T                        port,
    HAL_PHY_OP_MODE_T                   *ptr_mode)
{
    AIR_ERROR_NO_T  rv = AIR_E_OK;
    UI16_T          reg_data = 0;

    rv = hal_mdio_readC45ByPort(unit, port, MMD_DEV_VSPEC1, 0x0c6, &reg_data);
    if (AIR_E_OK == rv)
    {
        switch(reg_data)
        {
            case EN8804_LONG_REACH:
                *ptr_mode = HAL_PHY_OP_MODE_LONG_REACH;
                break;
            case EN8804_1G_LOW_POWER:
            case EN8804_10M_100M_LOW_POWER:
                *ptr_mode = HAL_PHY_OP_MODE_NORMAL;
                break;
            default:
                *ptr_mode = HAL_PHY_OP_MODE_NORMAL;
                break;
        }
    }
    return rv;
}

const HAL_PHY_DRIVER_T
_ext_an8804_phy_func_vec =
{
    /* note: if function not support, fill "NULL". */
    hal_an8804_phy_init,
    hal_an8804_phy_setAdminState,
    hal_an8804_phy_getAdminState,
    hal_an8804_phy_setAutoNego,
    hal_an8804_phy_getAutoNego,
    hal_an8804_phy_setLocalAdvAbility,
    hal_an8804_phy_getLocalAdvAbility,
    hal_an8804_phy_getRemoteAdvAbility,
    hal_an8804_phy_setSpeed,
    hal_an8804_phy_getSpeed,
    hal_an8804_phy_setDuplex,
    hal_an8804_phy_getDuplex,
    hal_an8804_phy_getLinkStatus,
    hal_an8804_phy_setLoopBack,
    hal_an8804_phy_getLoopBack,
    hal_an8804_phy_setSmartSpeedDown,
    hal_an8804_phy_getSmartSpeedDown,
    hal_an8804_phy_setLedOnCtrl,
    hal_an8804_phy_getLedOnCtrl,
    hal_an8804_phy_testTxCompliance,
    NULL,
    NULL,
    NULL,
    NULL,
    hal_an8804_phy_setPhyLedCtrlMode,
    hal_an8804_phy_getPhyLedCtrlMode,
    hal_an8804_phy_setPhyLedForceState,
    hal_an8804_phy_getPhyLedForceState,
    hal_an8804_phy_setPhyLedForcePattCfg,
    hal_an8804_phy_getPhyLedForcePattCfg,
    hal_an8804_phy_triggerCableTest,
    hal_an8804_phy_getCableTestRawData,
    hal_an8804_phy_setPhyLedGlbCfg,
    hal_an8804_phy_getPhyLedGlbCfg,
    hal_an8804_phy_setPhyLedBlkEvent,
    hal_an8804_phy_getPhyLedBlkEvent,
    hal_an8804_phy_setPhyLedDuration,
    hal_an8804_phy_getPhyLedDuration,
    hal_en8804_phy_setPhyOpMode,
    hal_en8804_phy_getPhyOpMode,
    NULL,
};

AIR_ERROR_NO_T
hal_an8804_phy_getDriver(
    HAL_PHY_DRIVER_T **pptr_hal_driver)
{
    (*pptr_hal_driver) = (HAL_PHY_DRIVER_T *)&_ext_an8804_phy_func_vec;

    return (AIR_E_OK);
}

