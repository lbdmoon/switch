/* FILE NAME: air_ver.h
 * PURPOSE:
 *      Define the version for AIR SDK.
 *
 * NOTES:
 *      None
 */

#ifndef AIR_VER_H
#define AIR_VER_H

/* INCLUDE FILE DECLARATIONS
 */

/* NAMING CONSTANT DECLARATIONS
 */
#define AIR_VER_SDK    "v2.8.6a"

/* MACRO FUNCTION DECLARATIONS
 */

/* DATA TYPE DECLARATIONS
 */

/* EXPORTED SUBPROGRAM SPECIFICATIONS
 */

#endif  /* AIR_VER_H */

