/*******************************************************************************
*  Copyright Statement:
*  --------------------
*  This software is protected by Copyright and the information contained
*  herein is confidential. The software may not be copied and the information
*  contained herein may not be used or disclosed except with the written
*  permission of Airoha Technology Corp. (C) 2021
*
*  BY OPENING THIS FILE, BUYER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
*  THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("AIROHA SOFTWARE")
*  RECEIVED FROM AIROHA AND/OR ITS REPRESENTATIVES ARE PROVIDED TO BUYER ON
*  AN "AS-IS" BASIS ONLY. AIROHA EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
*  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
*  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
*  NEITHER DOES AIROHA PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
*  SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
*  SUPPLIED WITH THE AIROHA SOFTWARE, AND BUYER AGREES TO LOOK ONLY TO SUCH
*  THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. AIROHA SHALL ALSO
*  NOT BE RESPONSIBLE FOR ANY AIROHA SOFTWARE RELEASES MADE TO BUYER'S
*  SPECIFICATION OR TO CONFORM TO A PARTICULAR STANDARD OR OPEN FORUM.
*
*  BUYER'S SOLE AND EXCLUSIVE REMEDY AND AIROHA'S ENTIRE AND CUMULATIVE
*  LIABILITY WITH RESPECT TO THE AIROHA SOFTWARE RELEASED HEREUNDER WILL BE,
*  AT AIROHA'S OPTION, TO REVISE OR REPLACE THE AIROHA SOFTWARE AT ISSUE,
*  OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY BUYER TO
*  AIROHA FOR SUCH AIROHA SOFTWARE AT ISSUE.
*
*  THE TRANSACTION CONTEMPLATED HEREUNDER SHALL BE CONSTRUED IN ACCORDANCE
*  WITH THE LAWS OF THE STATE OF CALIFORNIA, USA, EXCLUDING ITS CONFLICT OF
*  LAWS PRINCIPLES.  ANY DISPUTES, CONTROVERSIES OR CLAIMS ARISING THEREOF AND
*  RELATED THERETO SHALL BE SETTLED BY ARBITRATION IN SAN FRANCISCO, CA, UNDER
*  THE RULES OF THE INTERNATIONAL CHAMBER OF COMMERCE (ICC).
*
*******************************************************************************/

/* FILE NAME:   air_cfg.h
 * PURPOSE:
 *      Customer configuration on AIR SDK.
 * NOTES:
 */

#ifndef AIR_CFG_H
#define AIR_CFG_H

/* INCLUDE FILE DECLARATIONS
 */

#include <air_types.h>
#include <air_error.h>


/* NAMING CONSTANT DECLARATIONS
 */

/* MACRO FUNCTION DECLARATIONS
 */
#ifdef AIR_EN_USERDEFINED_CHIPNUM
#define AIR_CFG_MAXIMUM_CHIPS_PER_SYSTEM    (AIR_EN_USERDEFINED_CHIPNUM)
#else
#define AIR_CFG_MAXIMUM_CHIPS_PER_SYSTEM    (4)
#endif

/* DATA TYPE DECLARATIONS
 */
typedef enum
{
    AIR_CFG_TYPE_IFMON_ENABLE,                      /* used to enable or disable IFMON thread
                                                     * param0: ignore
                                                     * param1: ignore
                                                     * value : 0 - disable, 1 - enable
                                                     */
    AIR_CFG_TYPE_IFMON_THREAD_PRI,                  /* used to customize IFMON thread priority
                                                     * param0: ignore
                                                     * param1: ignore
                                                     * value : priority
                                                     */
    AIR_CFG_TYPE_IFMON_THREAD_STACK,                /* used to customize IFMON thread stack size
                                                     * param0: ignore
                                                     * param1: ignore
                                                     * value : stack size, unit is byte
                                                     */
    AIR_CFG_TYPE_PHY_ACCESS_TYPE,                   /* used to configure PHY access type
                                                     * param0: port
                                                     * param1: ignore
                                                     * value : 0 - direct mdio, 1: i2c indirect mdio
                                                     */
    AIR_CFG_TYPE_PHY_I2C_ADDRESS,                   /* used to configure I2C slave address of I2C type PHY
                                                     * param0: port
                                                     * param1: ignore
                                                     * value : i2c address for indirect access mdio
                                                     *         Only valid if AIR_CFG_TYPE_PHY_ACCESS_TYPE is
                                                     *         i2c indirect mdio
                                                     */
    AIR_CFG_TYPE_PHY_ADDRESS,                       /* used to configure PHY address
                                                     * param0: port
                                                     * param1: ignore
                                                     * value : phy address 0-31
                                                     */
    AIR_CFG_TYPE_PHY_LED_BEHAVIOR,                  /* used to configure PHY LED behavior
                                                     * param0: port
                                                     * param1: led id
                                                     * value : bit(0, 1, 2) wrt LED on for (1000M, 100M, 10M) link up
                                                     *         bit(3, 4) wrt LED on for (full, half) duplex
                                                     *         bit(5, 6) wrt LED blink for 1000M (TX, RX) activity
                                                     *         bit(7, 8) wrt LED blink for 100M (TX, RX) activity
                                                     *         bit(9, 10) wrt LED blink for 10M (TX, RX) activity
                                                     *         bit11 for LED high active
                                                     *         bit12 wrt LED on for 2500M link up
                                                     *         bit(13, 14) wrt LED on for 2500M (TX, RX) activity
                                                     */
    AIR_CFG_TYPE_PHY_LED_TYPE,                      /* used to configure PHY LED type
                                                     * param0: ignore
                                                     * param1: ignore
                                                     * value : 0 - serial LED, 1 - parallel LED
                                                     */
    AIR_CFG_TYPE_PHY_LED_COUNT,                     /* used to configure per-port PHY LED count
                                                     * param0: ignore
                                                     * param1: ignore
                                                     * value : LED count, valid: 1-2
                                                     */
    AIR_CFG_TYPE_SERDES_POLARITY_REVERSE,           /* used to configure serdes polarity reverse
                                                     * param0: port
                                                     * param1: ignore
                                                     * value : 0 - no reverse, 1 - tx reverse,
                                                     *         2 - rx reverse, 3 - tx_rx reverse
                                                     */
    AIR_CFG_TYPE_MAX_ENTRY_CNT_MAC_BASED_VLAN,      /* used to configure shared table entry count for MAC based VLAN
                                                     * param0: ignore
                                                     * param1: ignore
                                                     * value : max entry number
                                                     */
    AIR_CFG_TYPE_MAX_ENTRY_CNT_IPV4_BASED_VLAN,     /* used to configure shared table entry count for
                                                     * IPv4 subnet based VLAN
                                                     * param0: ignore
                                                     * param1: ignore
                                                     * value : max entry number
                                                     */
    AIR_CFG_TYPE_MAX_ENTRY_CNT_SERVICE_VLAN,        /* used to configure shared table entry count for service VLAN
                                                     * param0: ignore
                                                     * param1: ignore
                                                     * value : max entry number
                                                     */
    AIR_CFG_TYPE_FORCE_DEVICE_ID,                   /* used to force device id
                                                     * param0: ignore
                                                     * param1: ignore
                                                     * value : device id
                                                     */
    AIR_CFG_TYPE_MDIO_CLOCK,                        /* used to configure MDIO(SMI) interface clock
                                                     * param0: ignore
                                                     * param1: ignore
                                                     * value : 0 - 700KHz, 1 - 2.8MHz, 2 - 5.6MHz, 3 - 11.2MHz
                                                     */
    AIR_CFG_TYPE_SIF_LOCAL_CLOCK,                   /* used to configure local SIF(I2C) interface clock
                                                     * param0: local sif channel
                                                     * param1: ignore
                                                     * value : 0 - 100kHz, 1 - 400kHz, 2 - 1MHz
                                                     */
    AIR_CFG_TYPE_SIF_REMOTE_SLAVE_ID,               /* used to configure remote SIF(I2C) slave address
                                                     * prarm0: local sif channel
                                                     * param1: device index
                                                     * value : slave id
                                                     */
    AIR_CFG_TYPE_SIF_REMOTE_CLOCK,                  /* used to configure remote SIF(I2C) interface clock
                                                     * param0: local sif channel
                                                     * param1: slave id
                                                     * value : 0 - 100kHz, 1 - 400kHz, 2 - 1MHz
                                                     */
    AIR_CFG_TYPE_PERIF_FORCE_GPIO_PIN,              /* used to configure peripheral pin as GPIO
                                                     * param0: pin number
                                                     * param1: ignore
                                                     * value : 0 - disable, 1 - enable
                                                     */
    AIR_CFG_TYPE_COMBO_PORT_LED_TYPE,               /* used to configure combo port LED type
                                                     * param0: port
                                                     * param1: ignore
                                                     * value : 0 - serial LED, 1 - parallel LED
                                                     */
    AIR_CFG_TYPE_SERDES_SYSTEM_SIDE_OP_MODE,        /* used to configure serdes system side operation mode
                                                     * param0: port
                                                     * param1: ignore
                                                     * value : 0 - Speed changed by line side link speed,
                                                     *         1 - Speed fixed at serdes maximum speed
                                                     */
    AIR_CFG_TYPE_SERDES_PORT_OPTION,                /* used to configure serdes port operation options
                                                     * param0: port
                                                     * param1: ignore
                                                     * value : 0 - default, 1 - force rate-adaption,
                                                     *         2 - skip serdes driver
                                                     *         default -
                                                     *              all air port API's behavior will depend on
                                                     *              serdes mode setting and configure on both
                                                     *              serdes and MAC
                                                     *         force rate-adaption -
                                                     *              used for the peer serdes not support
                                                     *              automatical rate adaption
                                                     *         skip serdes driver -
                                                     *              all air port API's behavior will bypass serdes
                                                     *              and only configure on MAC
                                                     *
                                                     */
    AIR_CFG_TYPE_GPIO_LED_MAP,                      /* used to configure gpio mapping to port led
                                                     * param0: port number
                                                     * param1: ignore
                                                     * value : gpio number
                                                     */
    AIR_CFG_TYPE_GPIO_LED_CFG,                      /* used to configure GPIO LED behavior
                                                     * param0: gpio id
                                                     * param1: ignore
                                                     * value :  0: PORT0_LED0,  1: PORT0_LED1,  2: PORT0_LED2,  3: PORT0_LED3
                                                     *          4: PORT1_LED0,  5: PORT1_LED1,  6: PORT1_LED2,  7: PORT1_LED3
                                                     *          8: PORT2_LED0,  9: PORT2_LED1, 10: PORT2_LED2, 11: PORT2_LED3
                                                     *         12: PORT3_LED0, 13: PORT3_LED1, 14: PORT3_LED2, 15: PORT3_LED3
                                                     *         16: PORT4_LED0, 17: PORT4_LED1, 18: PORT4_LED2, 19: PORT4_LED3
                                                     */
    AIR_CFG_TYPE_GPIO_INVERSE,                      /* used to configure GPIO inverse
                                                     * param0: gpio number
                                                     * param1: ignore
                                                     * value : 0 - not inverse, 1 - inverse
                                                     */
    AIR_CFG_TYPE_LAST
}AIR_CFG_TYPE_T;

typedef struct AIR_CFG_VALUE_S
{
    UI32_T  param0;             /*(Optional) The optional parameter which is available
                                 * when the AIR_CFG_TYPE_T needs the first arguments*/
    UI32_T  param1;             /*(Optional) The optional parameter which is available
                                 * when the AIR_CFG_TYPE_T needs the second arguments*/
    I32_T   value;

}AIR_CFG_VALUE_T;

typedef AIR_ERROR_NO_T
    (*AIR_CFG_GET_FUNC_T)(
    const UI32_T            unit,
    const AIR_CFG_TYPE_T    cfg_type,
    AIR_CFG_VALUE_T         *ptr_cfg_value);

/* EXPORTED SUBPROGRAM SPECIFICATIONS
 */
/* FUNCTION NAME:   air_cfg_getValue
 * PURPOSE:
 *      This API is used to get the value of customized configuration.
 * INPUT:
 *      unit                 -- Device ID
 *      cfg_type             -- The type of config
 *                              AIR_CFG_TYPE_T
 * OUTPUT:
 *      ptr_value            -- The value of config
 *                              AIR_CFG_VALUE_T
 * RETURN:
 *      AIR_E_OK             -- Operation success.
 *      AIR_E_BAD_PARAMETER  -- Parameter is wrong.
 * NOTES:
 *      None
 */
AIR_ERROR_NO_T
air_cfg_getValue(
    const UI32_T unit,
    const AIR_CFG_TYPE_T cfg_type,
    AIR_CFG_VALUE_T *ptr_value);

/* FUNCTION NAME:   air_cfg_register
 * PURPOSE:
 *      The function is to register user configuration callback to SDK.
 *
 * INPUT:
 *      unit                 -- Device ID
 *      ptr_cfg_callback     -- Pointer to user configuration callback
 *                              AIR_CFG_GET_FUNC_T
 *
 * OUTPUT:
 *      None
 *
 * RETURN:
 *      AIR_E_OK             -- Operation success.
 *      AIR_E_BAD_PARAMETER  -- Parameter is wrong.
 *
 * NOTES:
 *      1. During SDK initializtion, it will call registered user
 *         configuration callback to get configuration and apply them.
 *         If there is no registered user configuration callback or
 *         can not get specified user configuration callback, SDK will
 *         apply default setting.
 *      2. This function should be called before calling air_init
 */
AIR_ERROR_NO_T
air_cfg_register(
    const UI32_T            unit,
    AIR_CFG_GET_FUNC_T      ptr_cfg_callback);

#endif  /* AIR_CFG_H */
