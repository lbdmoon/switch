/*******************************************************************************
*  Copyright Statement:
*  --------------------
*  This software is protected by Copyright and the information contained
*  herein is confidential. The software may not be copied and the information
*  contained herein may not be used or disclosed except with the written
*  permission of Airoha Technology Corp. (C) 2021
*
*  BY OPENING THIS FILE, BUYER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
*  THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("AIROHA SOFTWARE")
*  RECEIVED FROM AIROHA AND/OR ITS REPRESENTATIVES ARE PROVIDED TO BUYER ON
*  AN "AS-IS" BASIS ONLY. AIROHA EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
*  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
*  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
*  NEITHER DOES AIROHA PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
*  SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
*  SUPPLIED WITH THE AIROHA SOFTWARE, AND BUYER AGREES TO LOOK ONLY TO SUCH
*  THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. AIROHA SHALL ALSO
*  NOT BE RESPONSIBLE FOR ANY AIROHA SOFTWARE RELEASES MADE TO BUYER'S
*  SPECIFICATION OR TO CONFORM TO A PARTICULAR STANDARD OR OPEN FORUM.
*
*  BUYER'S SOLE AND EXCLUSIVE REMEDY AND AIROHA'S ENTIRE AND CUMULATIVE
*  LIABILITY WITH RESPECT TO THE AIROHA SOFTWARE RELEASED HEREUNDER WILL BE,
*  AT AIROHA'S OPTION, TO REVISE OR REPLACE THE AIROHA SOFTWARE AT ISSUE,
*  OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY BUYER TO
*  AIROHA FOR SUCH AIROHA SOFTWARE AT ISSUE.
*
*  THE TRANSACTION CONTEMPLATED HEREUNDER SHALL BE CONSTRUED IN ACCORDANCE
*  WITH THE LAWS OF THE STATE OF CALIFORNIA, USA, EXCLUDING ITS CONFLICT OF
*  LAWS PRINCIPLES.  ANY DISPUTES, CONTROVERSIES OR CLAIMS ARISING THEREOF AND
*  RELATED THERETO SHALL BE SETTLED BY ARBITRATION IN SAN FRANCISCO, CA, UNDER
*  THE RULES OF THE INTERNATIONAL CHAMBER OF COMMERCE (ICC).
*
*******************************************************************************/

/* FILE NAME: air_swc.h
* PURPOSE:
 *      Define the swtich function in AIR SDK.
*
* NOTES:
*       None
*/

#ifndef AIR_SWC_H
#define AIR_SWC_H

/* INCLUDE FILE DECLARATIONS
*/
#include <air_port.h>

/* NAMING CONSTANT DECLARATIONS
*/
#define AIR_SWC_CABLE_MAX_PAIR          (AIR_PORT_CABLE_MAX_PAIR)

/* MACRO FUNCTION DECLARATIONS
*/

/* DATA TYPE DECLARATIONS
*/
typedef enum
{
    AIR_SWC_MGMT_FRAME_TYPE_IGMP,
    AIR_SWC_MGMT_FRAME_TYPE_PPPOE,
    AIR_SWC_MGMT_FRAME_TYPE_ARP,
    AIR_SWC_MGMT_FRAME_TYPE_PAE,
    AIR_SWC_MGMT_FRAME_TYPE_DHCP,
    AIR_SWC_MGMT_FRAME_TYPE_BPDU,
    AIR_SWC_MGMT_FRAME_TYPE_TTL_0,
    AIR_SWC_MGMT_FRAME_TYPE_MLD,
    AIR_SWC_MGMT_FRAME_TYPE_REV_01,
    AIR_SWC_MGMT_FRAME_TYPE_REV_02,
    AIR_SWC_MGMT_FRAME_TYPE_REV_03,
    AIR_SWC_MGMT_FRAME_TYPE_REV_0E,
    AIR_SWC_MGMT_FRAME_TYPE_REV_10,
    AIR_SWC_MGMT_FRAME_TYPE_REV_20,
    AIR_SWC_MGMT_FRAME_TYPE_REV_21,
    AIR_SWC_MGMT_FRAME_TYPE_REV_UN,
    AIR_SWC_MGMT_FRAME_TYPE_LAST
} AIR_SWC_MGMT_FRAME_TYPE_T;

typedef enum
{
    AIR_SWC_MGMT_FWD_MODE_SYS_SETTING,
    AIR_SWC_MGMT_FWD_MODE_SYS_SETTING_INCLUDE_CPU,
    AIR_SWC_MGMT_FWD_MODE_SYS_SETTING_EXCLUDE_CPU,
    AIR_SWC_MGMT_FWD_MODE_CPU_ONLY,
    AIR_SWC_MGMT_FWD_MODE_DROP,
    AIR_SWC_MGMT_FWD_MODE_LAST
} AIR_SWC_MGMT_FWD_MODE_T;

typedef enum
{
    AIR_SWC_CABLE_STATUS_OPEN = AIR_PORT_CABLE_STATUS_OPEN,
    AIR_SWC_CABLE_STATUS_SHORT,
    AIR_SWC_CABLE_STATUS_NORMAL,
    AIR_SWC_CABLE_STATUS_LAST
} AIR_SWC_CABLE_STATUS_T;

typedef enum
{
    AIR_SWC_CABLE_TEST_PAIR_A = AIR_PORT_CABLE_TEST_PAIR_A,
    AIR_SWC_CABLE_TEST_PAIR_B,
    AIR_SWC_CABLE_TEST_PAIR_C,
    AIR_SWC_CABLE_TEST_PAIR_D,
    AIR_SWC_CABLE_TEST_PAIR_ALL,
    AIR_SWC_CABLE_TEST_PAIR_LAST
} AIR_SWC_CABLE_TEST_PAIR_T;

typedef struct AIR_SWC_MGMT_FRAME_CFG_S
{
#define AIR_SWC_MGMT_FRAME_CFG_FLAGS_ENABLE     (1U << 0)
#define AIR_SWC_MGMT_FRAME_CFG_FLAGS_PRI_HIGH   (1U << 1)
    UI32_T                      flags;
    AIR_SWC_MGMT_FRAME_TYPE_T   frame_type;
    AIR_SWC_MGMT_FWD_MODE_T     forward_mode;
} AIR_SWC_MGMT_FRAME_CFG_T;

typedef struct AIR_SWC_CABLE_TEST_RSLT_S
{
    AIR_SWC_CABLE_STATUS_T  status[AIR_SWC_CABLE_MAX_PAIR];

    /* Cable length = length * 0.1 m */
    UI32_T                  length[AIR_SWC_CABLE_MAX_PAIR];
} AIR_SWC_CABLE_TEST_RSLT_T;

typedef enum
{
    AIR_SWC_JUMBO_SIZE_1518 = 0,
    AIR_SWC_JUMBO_SIZE_1536,
    AIR_SWC_JUMBO_SIZE_1552,
    AIR_SWC_JUMBO_SIZE_2048,
    AIR_SWC_JUMBO_SIZE_3072,
    AIR_SWC_JUMBO_SIZE_4096,
    AIR_SWC_JUMBO_SIZE_5120,
    AIR_SWC_JUMBO_SIZE_6144,
    AIR_SWC_JUMBO_SIZE_7168,
    AIR_SWC_JUMBO_SIZE_8192,
    AIR_SWC_JUMBO_SIZE_9216,
    AIR_SWC_JUMBO_SIZE_12288,
    AIR_SWC_JUMBO_SIZE_15360,
    AIR_SWC_JUMBO_SIZE_LAST
} AIR_SWC_JUMBO_SIZE_T;

typedef enum
{
    AIR_SWC_PROPERTY_ENABLE_MAC_AUTO_FLUSH,
    AIR_SWC_PROPERTY_ENABLE_L1_RATE_CTRL,
    AIR_SWC_PROPERTY_MAC_AUTO_FLUSH = AIR_SWC_PROPERTY_ENABLE_MAC_AUTO_FLUSH,
    AIR_SWC_PROPERTY_L1_RATE_CTRL = AIR_SWC_PROPERTY_ENABLE_L1_RATE_CTRL,
    AIR_SWC_PROPERTY_ACL_RATE_CTRL_MGMT_FRAME_INCLUDE,
    AIR_SWC_PROPERTY_STORM_CTRL_MGMT_FRAME_INCLUDE,
    AIR_SWC_PROPERTY_LAST
} AIR_SWC_PROPERTY_T;
/* Definition of Loop Detection */
typedef enum{
    AIR_SWC_LPDET_MODE_DISABLE,
    AIR_SWC_LPDET_MODE_ENABLE,
    AIR_SWC_LPDET_MODE_LAST
} AIR_SWC_LPDET_MODE_T;

/* EXPORTED SUBPROGRAM SPECIFICATIONS
*/

/* FUNCTION NAME:   air_swc_setMgmtFrameCfg
 * PURPOSE:
 *      Set management frame config.
 * INPUT:
 *      unit                     -- Device ID
 *      ptr_cfg                  -- Management frame configuration
 *                                  AIR_SWC_MGMT_FRAME_CFG_T
 * OUTPUT:
 *      None
 * RETURN:
 *      AIR_E_OK                 -- Operation Success.
 *      AIR_E_BAD_PARAMETER      -- Parameter is wrong.
 * NOTES:
 *      REV_xx = 01-80-C2-00-00-xx of destination mac
 *      REV_UN = others
 */
AIR_ERROR_NO_T
air_swc_setMgmtFrameCfg(
    const UI32_T                unit,
    AIR_SWC_MGMT_FRAME_CFG_T    *ptr_cfg);

/* FUNCTION NAME:   air_swc_getMgmtFrameCfg
 * PURPOSE:
 *      Get management frame config.
 * INPUT:
 *      unit                     -- Device ID
 * OUTPUT:
 *      ptr_cfg                  -- Management frame configuration
 *                                  AIR_SWC_MGMT_FRAME_CFG_T
 * RETURN:
 *      AIR_E_OK                 -- Operation Success.
 *      AIR_E_BAD_PARAMETER      -- Parameter is wrong.
 * NOTES:
 *      REV_xx = 01-80-C2-00-00-xx of destination mac
 *      REV_UN = others
 */
AIR_ERROR_NO_T
air_swc_getMgmtFrameCfg(
    const UI32_T                unit,
    AIR_SWC_MGMT_FRAME_CFG_T    *ptr_cfg);

/* FUNCTION NAME:   air_swc_triggerCableTest
 * PURPOSE:
 *      Get cable status.
 * INPUT:
 *      unit                     -- Device ID
 *      port                     -- Port ID
 *      test_pair                -- Tested cable pair
 *                                  AIR_SWC_CABLE_TEST_PAIR_T
 * OUTPUT:
 *      ptr_test_rslt            -- Cable diagnostic information
 *                                  AIR_SWC_CABLE_TEST_RSLT_T
 * RETURN:
 *      AIR_E_OK                 -- Operation Success.
 *      AIR_E_NOT_SUPPORT        -- Feature is not supported.
 *      AIR_E_BAD_PARAMETER      -- Parameter is wrong.
 * NOTES:
 *      Support cable diagnostic in speed 1G only.
 *
 *      This API will be obsoleted in later release, please use
 *      new APIs designed on air_port.h instead.
 */
AIR_ERROR_NO_T
air_swc_triggerCableTest(
    const UI32_T                unit,
    const UI32_T                port,
    AIR_SWC_CABLE_TEST_PAIR_T   test_pair,
    AIR_SWC_CABLE_TEST_RSLT_T   *ptr_test_rslt);

/* FUNCTION NAME:   air_swc_setSystemMac
 * PURPOSE:
 *      Set the system MAC address.
 * INPUT:
 *      unit                     -- Device ID
 *      mac                      -- System MAC address
 * OUTPUT:
 *      None
 * RETURN:
 *      AIR_E_OK                 -- Operation Success.
 *      AIR_E_BAD_PARAMETER      -- Parameter is wrong.
 * NOTES:
 *      It's unique and specified for pause frame.
 */
AIR_ERROR_NO_T
air_swc_setSystemMac(
    const UI32_T    unit,
    const AIR_MAC_T mac);

/* FUNCTION NAME:   air_swc_getSystemMac
 * PURPOSE:
 *      Get the system MAC address.
 * INPUT:
 *      unit                     -- Device ID
 * OUTPUT:
 *      mac                      -- System MAC address
 * RETURN:
 *      AIR_E_OK                 -- Operation Success.
 *      AIR_E_BAD_PARAMETER      -- Parameter is wrong.
 * NOTES:
 *      It's unique and specified for pause frame.
 */
AIR_ERROR_NO_T
air_swc_getSystemMac(
    const UI32_T unit,
    AIR_MAC_T    mac);

/* FUNCTION NAME:   air_swc_setJumboSize
 * PURPOSE:
 *      Set accepting jumbo frmes with specificied size.
 * INPUT:
 *      unit                     -- Device ID
 *      jumbo_size               -- Maximun jumbo packet size
 *                                  AIR_SWC_JUMBO_SIZE_T
 * OUTPUT:
 *      None
 * RETURN:
 *      AIR_E_OK                 -- Operation Success.
 *      AIR_E_BAD_PARAMETER      -- Parameter is wrong.
 * NOTES:
 */
AIR_ERROR_NO_T
air_swc_setJumboSize(
    const UI32_T                unit,
    const AIR_SWC_JUMBO_SIZE_T  jumbo_size);

/* FUNCTION NAME:   air_swc_getJumboSize
 * PURPOSE:
 *      Get accepting jumbo frmes with specificied size.
 * INPUT:
 *      unit                     -- Device ID
 * OUTPUT:
 *      ptr_jumbo_size           -- Maximun jumbo packet size
 *                                  AIR_SWC_JUMBO_SIZE_T
 * RETURN:
 *      AIR_E_OK                 -- Operation Success.
 *      AIR_E_BAD_PARAMETER      -- Parameter is wrong.
 * NOTES:
 *      None
 */
AIR_ERROR_NO_T
air_swc_getJumboSize(
    const UI32_T            unit,
    AIR_SWC_JUMBO_SIZE_T    *ptr_jumbo_size);

/* FUNCTION NAME:   air_swc_setProperty
 * PURPOSE:
 *      Set switch property.
 * INPUT:
 *      unit                     -- Device ID
 *      property                 -- Select switch property
 *                                  AIR_SWC_PROPERTY_T
 *      param0                   -- 1: Enable
 *                                  0: Disable
 *      param1                   -- Reserved
 * OUTPUT:
 *      None
 * RETURN:
 *      AIR_E_OK                 -- Operation Success.
 *      AIR_E_BAD_PARAMETER      -- Parameter is wrong.
 * NOTES:
 *      None
 */
AIR_ERROR_NO_T
air_swc_setProperty(
    const UI32_T                unit,
    const AIR_SWC_PROPERTY_T    property,
    const UI32_T                param0,
    const UI32_T                param1);

/* FUNCTION NAME:   air_swc_getProperty
 * PURPOSE:
 *      Get switch property.
 * INPUT:
 *      unit                     -- Device ID
 *      property                 -- Select switch property
 *                                  AIR_SWC_PROPERTY_T
 * OUTPUT:
 *      ptr_param0               -- 1: Enable
 *                                  0: Disable
 *      ptr_param1               -- Reserved
 * RETURN:
 *      AIR_E_OK                 -- Operation Success.
 *      AIR_E_BAD_PARAMETER      -- Parameter is wrong.
 * NOTES:
 *      None
 */
AIR_ERROR_NO_T
air_swc_getProperty(
    const UI32_T                unit,
    const AIR_SWC_PROPERTY_T    property,
    UI32_T                      *ptr_param0,
    UI32_T                      *ptr_param1);

/* FUNCTION NAME:   air_swc_setLoopDetect
 * PURPOSE:
 *      Set loop detection mode.
 * INPUT:
 *      unit                     -- Device ID
 *      port                     -- Port ID
 *      mode                     -- Loop detection mode
 *                                  AIR_SWC_LPDET_MODE_T
 * OUTPUT:
 *      None
 * RETURN:
 *      AIR_E_OK                 -- Operation Success.
 *      AIR_E_BAD_PARAMETER      -- Parameter is wrong.
 * NOTES:
 *      None
 */
AIR_ERROR_NO_T
air_swc_setLoopDetect(
    const UI32_T                unit,
    const UI32_T                port,
    const AIR_SWC_LPDET_MODE_T  mode);

/* FUNCTION NAME:   air_swc_getLoopDetect
 * PURPOSE:
 *      Get loop detection mode.
 * INPUT:
 *      unit                     -- Device ID
 *      port                     -- Port ID
 * OUTPUT:
 *      ptr_mode                 -- Loop detection mode
 *                                  AIR_SWC_LPDET_MODE_T
 * RETURN:
 *      AIR_E_OK                 -- Operation Success.
 *      AIR_E_BAD_PARAMETER      -- Parameter is wrong.
 * NOTES:
 *      None
 */
AIR_ERROR_NO_T
air_swc_getLoopDetect(
    const UI32_T            unit,
    const UI32_T            port,
    AIR_SWC_LPDET_MODE_T    *ptr_mode);

/* FUNCTION NAME:   air_swc_setLoopDetectFrame
 * PURPOSE:
 *      Set source mac or ether type.
 * INPUT:
 *      unit                     -- Device ID
 *      ether_type               -- Ether type of loop detect frame
 *      smac                     -- Source MAC address of loop detect
 *                                  frame
 * OUTPUT:
 *      None
 * RETURN:
 *      AIR_E_OK                 -- Operation Success.
 *      AIR_E_BAD_PARAMETER      -- Parameter is wrong.
 * NOTES:
 *      Use current setting if type = 0
 *      Use current setting if smac = 0
 */
AIR_ERROR_NO_T
air_swc_setLoopDetectFrame(
    const UI32_T    unit,
    const UI32_T    ether_type,
    AIR_MAC_T       smac);

/* FUNCTION NAME:   air_swc_getLoopDetectFrame
 * PURPOSE:
 *      Get source mac, ether type.
 * INPUT:
 *      unit                     -- Device ID
 * OUTPUT:
 *      ptr_ether_type           -- Ether type of loop detect frame
 *      smac                     -- Source MAC address of loop detect
 *                                  frame
 * RETURN:
 *      AIR_E_OK                 -- Operation Success.
 *      AIR_E_BAD_PARAMETER      -- Parameter is wrong.
 * NOTES:
 *      None
 */
AIR_ERROR_NO_T
air_swc_getLoopDetectFrame(
    const UI32_T    unit,
    UI32_T          *ptr_ether_type,
    AIR_MAC_T       smac);

/* FUNCTION NAME:   air_swc_getLoopDetectStatus
 * PURPOSE:
 *      Get the current loop detection status.
 * INPUT:
 *      unit                     -- Device ID
 *      port_bitmap              -- Port bitmap
 * OUTPUT:
 *      ld_status_bitmap         -- Loop status bitmap
 *                                  TRUE  : loop occur
 *                                  FALSE : loop not occur
 * RETURN:
 *      AIR_E_OK                 -- Operation Success.
 *      AIR_E_BAD_PARAMETER      -- Parameter is wrong.
 *      AIR_E_OTHERS             -- Other errors.
 * NOTES:
 *      None
 */
AIR_ERROR_NO_T
air_swc_getLoopDetectStatus(
    const UI32_T                unit,
    const AIR_PORT_BITMAP_T     port_bitmap,
    AIR_PORT_BITMAP_T           ld_status_bitmap);

/* FUNCTION NAME:   air_swc_clearLoopDetectStatus
 * PURPOSE:
 *      Clear the current loop detection status of specific port
 *      bitmap.
 * INPUT:
 *      unit                     -- Device ID
 *      port_bitmap              -- Port bitmap
 * OUTPUT:
 *      None
 * RETURN:
 *      AIR_E_OK                 -- Operation Success.
 *      AIR_E_BAD_PARAMETER      -- Parameter is wrong.
 *      AIR_E_OTHERS             -- Other errors.
 * NOTES:
 *      None
 */
AIR_ERROR_NO_T
air_swc_clearLoopDetectStatus(
    const UI32_T                unit,
    const AIR_PORT_BITMAP_T     port_bitmap);

#endif /* End of AIR_SWC_H */
