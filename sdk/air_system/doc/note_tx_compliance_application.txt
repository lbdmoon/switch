/*
 *  Note    : note_tx_compliance_test
 *  Chip    : EN8851
 *  SDK     : AIR_SDK_v2.7.0
 *  Update  : 2022-10-03
 */

(1) Overview
    Ethernet device needs to have TX compliance testing. SDK provides built-in
    test commands to support configuration for different TX compliance testing.
    The purpose of this documenet is to describe how to use SDK diag shell to
    perform this testing.

(2) TX Compliance Command Syntax
    Below is the TX compliance command syntax on SDK diag shell:

    "diag test tx-compliance [ unit=<UINT> ] portlist=<UINTLIST> type=<TYPE>"

    - unit :
        Device unit number.
    - portlist :
        A string to describe the port list. The number after ',' means to add
        another port, after '-' means to add consecutive ports.
        e.g. 1,3,5-10,12
    - type :
        - 1000m-tm1
        - 1000m-tm2
        - 1000m-tm3
        - 1000m-tm4
        - 100m-pair-a
        - 100m-pair-b
        - 10m-sine
        - 10m-random
        - 10m-nlp

(3) TX Compliance Test Example
    Below is an example to test TX compliance with type 1000M-TM1 on port 1.

    sdk# diag test tx-compliance portlist=1 type=1000m-tm1

(4) Notices
    TX compliance configuration may modify the PHY setting. It is suggested
    to reboot the device every time at the end of the TX compliance test,
    even if changing the type only.
