#include <air_init.h>
#include <customer_ref.h>
#include <osal/osali.h>
#include <stdio.h>
#include <string.h>
#include <osal/osal.h>
#include <sdk_ref.h>
#ifdef SKU_2150
#include <sfp_led.h>
#endif

AIR_ERROR_NO_T
sdk_ref_initApp(
    const UI32_T unit)
{
    UI32_T rc = AIR_E_OK;

#ifdef SKU_2150
    rc = sfp_led_init(unit);
    if (AIR_E_OK != rc)
    {
        osal_printf("Failed to initialize SFP LED!\n");
    }
#endif

    return rc;
}

void
sdk_ref_write(
    const void *ptr_buf,
    UI32_T len)
{
    C8_T            buf[OSAL_PRN_BUF_SZ];
    /* length not include '\0', it need add 1*/
    snprintf(buf, len+1, "%s", (const char *)ptr_buf);
    printf("%s", buf);
}

AIR_ERROR_NO_T sdk_ref_init(void)
{
    AIR_ERROR_NO_T rc = AIR_E_OK;
    AIR_INIT_PARAM_T init_cmn;

    printf("Initializing CmnModule...");
    memset(&init_cmn, 0, sizeof(init_cmn));
    init_cmn.dsh_write_func = sdk_ref_write;
    init_cmn.debug_write_func = sdk_ref_write;
    rc = air_init_initCmnModule(&init_cmn);
    if (AIR_E_OK != rc)
    {
        osal_printf("Failed to initialize common modules!\n");
        return rc;
    }
    osal_printf("Done!\n");

    /* register customer configuration callback for customized SDK setting */
    rc = customer_ref_registerConfig(0);
    if (AIR_E_OK != rc)
    {
        osal_printf("Failed to register customer configuration callback!\n");
        return rc;
    }

    /*air init*/
    osal_printf("Air init low level...\n");
    rc = air_init_initLowLevel(0);
    if (AIR_E_OK != rc)
    {
        osal_printf("Failed to initialize low level modules!\n");
        return rc;
    }

    /* initialize port mapping */
    rc = customer_ref_initPortMap(0);
    if (AIR_E_OK != rc)
    {
        osal_printf("Failed to initialize SDK port mapping!\n");
        return rc;
    }

    osal_printf("Air init task resource...\n");
    rc = air_init_initTaskRsrc(0);
    if (AIR_E_OK != rc)
    {
        osal_printf("Failed to initialize SDK task resources!\n");
        return rc;
    }

    /* SDK module initialization */
    osal_printf("SDK module initialization...\n");
    rc = air_init_initModule(0);
    if (AIR_E_OK != rc)
    {
        osal_printf("Failed to initialize SDK modules!\n");
        return rc;
    }

    osal_printf("Air init task...\n");
    rc = air_init_initTask(0);
    if (AIR_E_OK != rc)
    {
        osal_printf("Failed to initialize SDK tasks!\n");
        return rc;
    }

    osal_printf("Air init apps...\n");
    rc = sdk_ref_initApp(0);

    return rc;
}
