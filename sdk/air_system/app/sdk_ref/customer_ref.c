#include <air_cfg.h>
#include <air_init.h>
#include <osal/osal.h>


typedef struct SDK_CFG_S
{
    UI32_T                      unit;
    AIR_CFG_TYPE_T              cfg_type;
    UI32_T                      param0;
    UI32_T                      param1;
    UI32_T                      value;
} SDK_CFG_T;


const static SDK_CFG_T _customer_ref_cfg[] =
{
/*
 *  unit    AIR_CFG_TYPE_XXX                    param0  param1  value
 *  ----    ----------------                    ------  ------  -----
 */
    {0, AIR_CFG_TYPE_FORCE_DEVICE_ID,           0,      0,   0x8855},
    {0, AIR_CFG_TYPE_IFMON_ENABLE,              0,      0,    FALSE},
    {0, AIR_CFG_TYPE_IFMON_THREAD_PRI,          0,      0,        5},
    {0, AIR_CFG_TYPE_IFMON_THREAD_STACK,        0,      0,      606},
    {0, AIR_CFG_TYPE_PHY_ADDRESS,               1,      0,        0},
    {0, AIR_CFG_TYPE_PHY_ADDRESS,               2,      0,        1},
    {0, AIR_CFG_TYPE_PHY_ADDRESS,               3,      0,        2},
    {0, AIR_CFG_TYPE_PHY_ADDRESS,               4,      0,        3},
    {0, AIR_CFG_TYPE_PHY_ADDRESS,               5,      0,        4},
#ifdef SKU_2149
    {0, AIR_CFG_TYPE_PHY_ADDRESS,               6,      0,     0x19},   //AN8801SB phy address
#endif
#if defined(SKU_2151) || defined(SKU_2150)
    {0, AIR_CFG_TYPE_PHY_ADDRESS,               6,      0,        6},   //fake phy address
#endif
    {0, AIR_CFG_TYPE_PHY_LED_BEHAVIOR,          1,      0,    0x7FF},
    {0, AIR_CFG_TYPE_PHY_LED_BEHAVIOR,          2,      0,    0x7FF},
    {0, AIR_CFG_TYPE_PHY_LED_BEHAVIOR,          3,      0,    0x7FF},
    {0, AIR_CFG_TYPE_PHY_LED_BEHAVIOR,          4,      0,    0x7FF},
    {0, AIR_CFG_TYPE_PHY_LED_BEHAVIOR,          5,      0,    0x7FF},
#ifdef SKU_2149
    {0, AIR_CFG_TYPE_PHY_LED_BEHAVIOR,          6,      0,   0x87FF},
#endif
#ifdef SKU_2150
    {0, AIR_CFG_TYPE_PHY_LED_BEHAVIOR,          6,      0,   0x7FFF},
#endif
    {0, AIR_CFG_TYPE_PHY_LED_COUNT,             0,      0,        1},
#if defined(SKU_2151) || defined(SKU_2150)
    {0, AIR_CFG_TYPE_PHY_LED_TYPE,              0,      0,        1},
#else
    {0, AIR_CFG_TYPE_PHY_LED_TYPE,              0,      0,        0},
#endif
#if defined(SKU_2151) || defined(SKU_2150)
    //{0, AIR_CFG_TYPE_GPIO_LED_MAP,              1,      0,        0},
    {0, AIR_CFG_TYPE_GPIO_LED_MAP,              2,      0,        5},
    {0, AIR_CFG_TYPE_GPIO_LED_MAP,              3,      0,        4},
    {0, AIR_CFG_TYPE_GPIO_LED_MAP,              4,      0,        3},
    {0, AIR_CFG_TYPE_GPIO_LED_MAP,              5,      0,        2},
    //{0, AIR_CFG_TYPE_GPIO_LED_CFG,              0,      0,        0},
    {0, AIR_CFG_TYPE_GPIO_LED_CFG,              2,      0,       16},
    {0, AIR_CFG_TYPE_GPIO_LED_CFG,              3,      0,       12},
    {0, AIR_CFG_TYPE_GPIO_LED_CFG,              4,      0,        8},
    {0, AIR_CFG_TYPE_GPIO_LED_CFG,              5,      0,        4},
    {0, AIR_CFG_TYPE_GPIO_INVERSE,              0,      0,        1},
    {0, AIR_CFG_TYPE_GPIO_INVERSE,              2,      0,        1},
    {0, AIR_CFG_TYPE_GPIO_INVERSE,              3,      0,        1},
    {0, AIR_CFG_TYPE_GPIO_INVERSE,              5,      0,        1},
#endif
    {0, AIR_CFG_TYPE_SERDES_POLARITY_REVERSE,   6,      0,        1},
};

const static AIR_INIT_PORT_MAP_T _ref_board_port_map[] =
{
/*
 * port    AIR_INIT_PORT_SPEED_T    AIR_INIT_PORT_TYPE_T    ...
 * ----    ---------------------    --------------------
 */
    { 0,  AIR_INIT_PORT_TYPE_CPU, AIR_INIT_PORT_SPEED_1000M},
    { 1,  AIR_INIT_PORT_TYPE_BASET, AIR_INIT_PORT_SPEED_1000M,  .baset_port={0}},
    { 2,  AIR_INIT_PORT_TYPE_BASET, AIR_INIT_PORT_SPEED_1000M,  .baset_port={1}},
    { 3,  AIR_INIT_PORT_TYPE_BASET, AIR_INIT_PORT_SPEED_1000M,  .baset_port={2}},
    { 4,  AIR_INIT_PORT_TYPE_BASET, AIR_INIT_PORT_SPEED_1000M,  .baset_port={3}},
    { 5,  AIR_INIT_PORT_TYPE_BASET, AIR_INIT_PORT_SPEED_1000M,  .baset_port={4}},
#ifdef SKU_2149
    { 6,  AIR_INIT_PORT_TYPE_XSGMII, AIR_INIT_PORT_SPEED_1000M, .xsgmii_port={4, 0, 0}}, //sgmii AN
#endif
#ifdef SKU_2150
    { 6,  AIR_INIT_PORT_TYPE_XSGMII, AIR_INIT_PORT_SPEED_1000M, .xsgmii_port={0, 0, 0}}, //1000base-x
#endif
#ifdef SKU_2151
    { 6,  AIR_INIT_PORT_TYPE_XSGMII, AIR_INIT_PORT_SPEED_5000M, .xsgmii_port={4, 0, 0}}, //5G baseR
#endif
};


static AIR_ERROR_NO_T
_ref_getConfigValue(
    const UI32_T unit,
    const AIR_CFG_TYPE_T type,
    AIR_CFG_VALUE_T *ptr_value)
{
    AIR_ERROR_NO_T rc = AIR_E_ENTRY_NOT_FOUND;
    const SDK_CFG_T *ptr_cfg;
    UI32_T i;
    UI32_T cfg_num;

    cfg_num = sizeof(_customer_ref_cfg)/sizeof(SDK_CFG_T);
    ptr_cfg = _customer_ref_cfg;

    for (i = 0; i < cfg_num; i++)
    {
        if ((ptr_cfg[i].unit == unit)
            && (ptr_cfg[i].cfg_type == type)
            && (ptr_cfg[i].param0 == ptr_value->param0)
            && (ptr_cfg[i].param1 == ptr_value->param1))
        {
            rc = AIR_E_OK;
            ptr_value->value = ptr_cfg[i].value;
            break;
        }
    }

    return  rc;
}

AIR_ERROR_NO_T customer_ref_registerConfig(UI32_T unit)
{
    AIR_ERROR_NO_T rc;
    rc = air_cfg_register(unit, _ref_getConfigValue);
    if (AIR_E_OK != rc)
    {
        osal_printf("Error: Failed to register customer configuration callback for customized SDK setting!\n");
    }
    else
    {
        osal_printf("Register customer configuration callback for customized SDK setting...\n");
    }
    return rc;
}

AIR_ERROR_NO_T customer_ref_initPortMap(UI32_T unit)
{
    AIR_ERROR_NO_T rc;
    rc = air_init_initSdkPortMap(unit, sizeof(_ref_board_port_map) / sizeof(AIR_INIT_PORT_MAP_T), _ref_board_port_map);
    if (AIR_E_OK != rc)
    {
        osal_printf("Error: Failed to initialize port map setting!\n");
    }
    else
    {
        osal_printf("Initialize port map setting...\n");
    }
    return rc;
}
