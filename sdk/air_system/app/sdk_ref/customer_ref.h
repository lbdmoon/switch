#ifndef CUSTOMER_REF_H
#define CUSTOMER_REF_H

AIR_ERROR_NO_T customer_ref_registerConfig(UI32_T unit);
AIR_ERROR_NO_T customer_ref_initPortMap(UI32_T unit);

#endif
