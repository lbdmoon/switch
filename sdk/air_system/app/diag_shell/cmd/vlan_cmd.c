/*******************************************************************************
*  Copyright Statement:
*  --------------------
*  This software is protected by Copyright and the information contained
*  herein is confidential. The software may not be copied and the information
*  contained herein may not be used or disclosed except with the written
*  permission of Airoha Technology Corp. (C) 2021
*
*  BY OPENING THIS FILE, BUYER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
*  THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("AIROHA SOFTWARE")
*  RECEIVED FROM AIROHA AND/OR ITS REPRESENTATIVES ARE PROVIDED TO BUYER ON
*  AN "AS-IS" BASIS ONLY. AIROHA EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
*  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
*  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
*  NEITHER DOES AIROHA PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
*  SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
*  SUPPLIED WITH THE AIROHA SOFTWARE, AND BUYER AGREES TO LOOK ONLY TO SUCH
*  THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. AIROHA SHALL ALSO
*  NOT BE RESPONSIBLE FOR ANY AIROHA SOFTWARE RELEASES MADE TO BUYER'S
*  SPECIFICATION OR TO CONFORM TO A PARTICULAR STANDARD OR OPEN FORUM.
*
*  BUYER'S SOLE AND EXCLUSIVE REMEDY AND AIROHA'S ENTIRE AND CUMULATIVE
*  LIABILITY WITH RESPECT TO THE AIROHA SOFTWARE RELEASED HEREUNDER WILL BE,
*  AT AIROHA'S OPTION, TO REVISE OR REPLACE THE AIROHA SOFTWARE AT ISSUE,
*  OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY BUYER TO
*  AIROHA FOR SUCH AIROHA SOFTWARE AT ISSUE.
*
*  THE TRANSACTION CONTEMPLATED HEREUNDER SHALL BE CONSTRUED IN ACCORDANCE
*  WITH THE LAWS OF THE STATE OF CALIFORNIA, USA, EXCLUDING ITS CONFLICT OF
*  LAWS PRINCIPLES.  ANY DISPUTES, CONTROVERSIES OR CLAIMS ARISING THEREOF AND
*  RELATED THERETO SHALL BE SETTLED BY ARBITRATION IN SAN FRANCISCO, CA, UNDER
*  THE RULES OF THE INTERNATIONAL CHAMBER OF COMMERCE (ICC).
*
*******************************************************************************/

#include <air_vlan.h>
#include <air_port.h>
#include <osal/osal.h>
#include <cmlib/cmlib_port.h>
#include <cmlib/cmlib_bitmap.h>
#include <parser/dsh_parser.h>
#include <parser/dsh_util.h>
#include <cmd/vlan_cmd.h>
#include <cmd/port_cmd.h>
#include <hal/common/hal.h>

/* NAMING CONSTANT DECLARATIONS
 */
#define VLAN_CMD_VLAN_ENTRY_NUM   (4096)
#define VLAN_CMD_VLAN_START_NUM   (1)

/* MACRO FUNCTION DECLARATIONS
 */
#define VLAN_CMD_PRINT_VLAN_TBL_INDEX() do                                                                                                                \
{                                                                                                                                                         \
    osal_printf("\n");                                                                                                                                    \
    osal_printf("unit  vid   member-port       ivl  fid  copy-pri  egs-tag-con  egs-tag-ctl-en  untagged-port     tagged-port     swap-port     stack-port     \n");    \
    osal_printf("----------------------------------------------------------------------------------------------------------------------------------------------\n");    \
} while(0)

#define VLAN_CMD_PRINT_VLAN_TBL_CONTENT(__unit__, __vid__, __ptr_entry__) do                                           \
{                                                                                                                      \
    UI32_T port = 0, str_len = 0;                                                                                      \
    C8_T *ptr_str = NULL;                                                                                              \
    AIR_PORT_BITMAP_T pbm = {0}, untagged_pbm = {0}, tagged_pbm = {0}, stack_pbm = {0}, swap_pbm = {0};                \
    ptr_str = osal_alloc(AIR_MAX_LEN_OF_PORT_STR, "cmd");                                                              \
    if (NULL == ptr_str)                                                                                               \
    {                                                                                                                  \
        osal_printf("***Error***, allocate memory fail\n");                                                            \
        return AIR_E_NO_MEMORY;                                                                                        \
    }                                                                                                                  \
    osal_memset(ptr_str, 0, AIR_MAX_LEN_OF_PORT_STR);                                                                  \
    AIR_PORT_BITMAP_COPY((pbm), ((__ptr_entry__)->port_bitmap));                                                      \
    osal_printf("%-4d  ", (__unit__));                                                                                 \
    osal_printf("%-4d  ", (__vid__));                                                                                  \
    CMD_STRING_SIZE_PORTLIST(ptr_str, AIR_MAX_LEN_OF_PORT_STR, pbm);                                                   \
    str_len = osal_strlen(ptr_str);                                                                                    \
    if (str_len > AIR_MAX_LEN_OF_PORT_PRINT)                                                                           \
    {                                                                                                                  \
        osal_printf("......            ");                                                                             \
    }                                                                                                                  \
    else                                                                                                               \
    {                                                                                                                  \
        osal_printf("%-16s  ", ptr_str);                                                                               \
    }                                                                                                                  \
    osal_printf("%-3s  ", ((__ptr_entry__)->flags & AIR_VLAN_ENTRY_FLAGS_EN_IVL) ? "en" : "dis");                                                        \
    osal_printf("%-3d  ", (__ptr_entry__)->fid);                                                                       \
    osal_printf("%-3s", ((__ptr_entry__)->flags & AIR_VLAN_ENTRY_FLAGS_EN_CP_PRI) ? "en" : "dis");                                                     \
    if (FALSE != ((__ptr_entry__)->flags & AIR_VLAN_ENTRY_FLAGS_EN_CP_PRI))                                                                             \
    {                                                                                                                  \
        osal_printf("       ");                                                                                        \
    }                                                                                                                  \
    else                                                                                                               \
    {                                                                                                                  \
        osal_printf(" -> %-1d  ", (__ptr_entry__)->user_pri);                                                          \
    }                                                                                                                  \
    if (FALSE != ((__ptr_entry__)->flags & AIR_VLAN_ENTRY_FLAGS_EN_EG_CON))                                                                               \
    {                                                                                                                  \
        osal_printf("en           ");                                                                                  \
        osal_printf("x               x                 x                 x");                                          \
    }                                                                                                                  \
    else                                                                                                               \
    {                                                                                                                  \
        osal_printf("dis          ");                                                                                  \
        if (FALSE != ((__ptr_entry__)->flags & AIR_VLAN_ENTRY_FLAGS_EN_ETAG_CTRL))                                                                     \
        {                                                                                                              \
            osal_printf("en              ");                                                                           \
            AIR_PORT_FOREACH(pbm, port)                                                                                \
            {                                                                                                          \
                switch ((__ptr_entry__)->egtag_ctl[port])                                                              \
                {                                                                                                      \
                    case AIR_VLAN_PORT_EGS_TAG_CTRL_TYPE_UNTAGGED: AIR_PORT_ADD(untagged_pbm, port); break;             \
                    case AIR_VLAN_PORT_EGS_TAG_CTRL_TYPE_SWAP:     AIR_PORT_ADD(swap_pbm, port);     break;             \
                    case AIR_VLAN_PORT_EGS_TAG_CTRL_TYPE_TAGGED:   AIR_PORT_ADD(tagged_pbm, port);   break;             \
                    case AIR_VLAN_PORT_EGS_TAG_CTRL_TYPE_STACK:    AIR_PORT_ADD(stack_pbm, port);    break;             \
                    default: continue; break;                                                                          \
                }                                                                                                      \
            }                                                                                                          \
            CMD_STRING_SIZE_PORTLIST(ptr_str, AIR_MAX_LEN_OF_PORT_STR, untagged_pbm);                                  \
            str_len = osal_strlen(ptr_str);                                                                            \
            if (str_len > AIR_MAX_LEN_OF_PORT_PRINT)                                                                   \
            {                                                                                                          \
                osal_printf("......            ");                                                                     \
            }                                                                                                          \
            else                                                                                                       \
            {                                                                                                          \
                osal_printf("%-16s  ", ptr_str);                                                                       \
            }                                                                                                          \
            CMD_STRING_SIZE_PORTLIST(ptr_str, AIR_MAX_LEN_OF_PORT_STR, tagged_pbm);                                    \
            str_len = osal_strlen(ptr_str);                                                                            \
            if (str_len > AIR_MAX_LEN_OF_PORT_PRINT)                                                                   \
            {                                                                                                          \
                osal_printf("......            ");                                                                     \
            }                                                                                                          \
            else                                                                                                       \
            {                                                                                                          \
                osal_printf("%-16s  ", ptr_str);                                                                       \
            }                                                                                                          \
            CMD_STRING_SIZE_PORTLIST(ptr_str, AIR_MAX_LEN_OF_PORT_STR, swap_pbm);                                      \
            str_len = osal_strlen(ptr_str);                                                                            \
            if (str_len > AIR_MAX_LEN_OF_PORT_PRINT)                                                                   \
            {                                                                                                          \
                osal_printf("......            ");                                                                     \
            }                                                                                                          \
            else                                                                                                       \
            {                                                                                                          \
                osal_printf("%-16s  ", ptr_str);                                                                       \
            }                                                                                                          \
            CMD_STRING_SIZE_PORTLIST(ptr_str, AIR_MAX_LEN_OF_PORT_STR, stack_pbm);                                     \
            str_len = osal_strlen(ptr_str);                                                                            \
            if (str_len > AIR_MAX_LEN_OF_PORT_PRINT)                                                                   \
            {                                                                                                          \
                osal_printf("......            ");                                                                     \
            }                                                                                                          \
            else                                                                                                       \
            {                                                                                                          \
                osal_printf("%-16s  ", ptr_str);                                                                       \
            }                                                                                                          \
        }                                                                                                              \
        else                                                                                                           \
        {                                                                                                              \
            osal_printf("dis             x                 x                 x");                                      \
        }                                                                                                              \
    }                                                                                                                  \
    osal_printf("\n");                                                                                                 \
    osal_free(ptr_str);                                                                                                \
}while(0)

#define VLAN_CMD_PRINT_VLAN_ENTRY(__unit__, __vid__, __ptr_entry__) do                                                 \
{                                                                                                                      \
    UI32_T port = 0;                                                                                                   \
    AIR_PORT_BITMAP_T pbm = {0}, untagged_pbm = {0}, tagged_pbm = {0}, stack_pbm = {0}, swap_pbm = {0};                \
    C8_T *ptr_str = NULL;                                                                                              \
    ptr_str = osal_alloc(AIR_MAX_LEN_OF_PORT_STR, "cmd");                                                              \
    if (NULL == ptr_str)                                                                                               \
    {                                                                                                                  \
        osal_printf("***Error***, allocate memory fail\n");                                                            \
        return AIR_E_NO_MEMORY;                                                                                        \
    }                                                                                                                  \
    osal_memset(ptr_str, 0, AIR_MAX_LEN_OF_PORT_STR);                                                                  \
    AIR_PORT_BITMAP_COPY((pbm), ((__ptr_entry__)->port_bitmap));                                                      \
    osal_printf("VLAN %-5d:\n", (__vid__));                                                                            \
    CMD_STRING_SIZE_PORTLIST(ptr_str, AIR_MAX_LEN_OF_PORT_STR, pbm);                                                   \
    osal_printf(" - member port = %s\n", ptr_str);                                                                     \
    osal_printf(" - fid = %d\n", (__ptr_entry__)->fid);                                                                \
    osal_printf(" - IVL = %s\n", ((__ptr_entry__)->flags & AIR_VLAN_ENTRY_FLAGS_EN_IVL) ? "enable" : "disable");                                         \
    osal_printf(" - copy-pri = %s\n", ((__ptr_entry__)->flags & AIR_VLAN_ENTRY_FLAGS_EN_CP_PRI) ? "enable" : "disable");                               \
    if (FALSE == ((__ptr_entry__)->flags & AIR_VLAN_ENTRY_FLAGS_EN_CP_PRI) )                                                                            \
    {                                                                                                                  \
        osal_printf(" - user-pri = %d\n", (__ptr_entry__)->user_pri);                                                  \
    }                                                                                                                  \
    osal_printf(" - egs-tag-con = %s\n", ((__ptr_entry__)->flags & AIR_VLAN_ENTRY_FLAGS_EN_EG_CON) ? "enable" : "disable");                              \
    if (FALSE == ((__ptr_entry__)->flags & AIR_VLAN_ENTRY_FLAGS_EN_EG_CON))                                                                              \
    {                                                                                                                  \
        osal_printf(" - egs-tag-ctl-en = %s\n", ((__ptr_entry__)->flags & AIR_VLAN_ENTRY_FLAGS_EN_ETAG_CTRL) ? "enable" : "disable");                 \
        if (FALSE != ((__ptr_entry__)->flags & AIR_VLAN_ENTRY_FLAGS_EN_ETAG_CTRL))                                                                     \
        {                                                                                                              \
            AIR_PORT_FOREACH(pbm, port)                                                                                \
            {                                                                                                          \
                switch ((__ptr_entry__)->egtag_ctl[port])                                                              \
                {                                                                                                      \
                    case AIR_VLAN_PORT_EGS_TAG_CTRL_TYPE_UNTAGGED: AIR_PORT_ADD(untagged_pbm, port); break;             \
                    case AIR_VLAN_PORT_EGS_TAG_CTRL_TYPE_SWAP:     AIR_PORT_ADD(swap_pbm, port);     break;             \
                    case AIR_VLAN_PORT_EGS_TAG_CTRL_TYPE_TAGGED:   AIR_PORT_ADD(tagged_pbm, port);   break;             \
                    case AIR_VLAN_PORT_EGS_TAG_CTRL_TYPE_STACK:    AIR_PORT_ADD(stack_pbm, port);    break;             \
                    default: continue; break;                                                                          \
                }                                                                                                      \
            }                                                                                                          \
            CMD_STRING_SIZE_PORTLIST(ptr_str, AIR_MAX_LEN_OF_PORT_STR, untagged_pbm);                                  \
            osal_printf(" - egs-untagged = %s\n", ptr_str);                                                            \
            CMD_STRING_SIZE_PORTLIST(ptr_str, AIR_MAX_LEN_OF_PORT_STR, tagged_pbm);                                    \
            osal_printf(" - egs-tagged = %s\n", ptr_str);                                                              \
            CMD_STRING_SIZE_PORTLIST(ptr_str, AIR_MAX_LEN_OF_PORT_STR, swap_pbm);                                      \
            osal_printf(" - egs-swap = %s\n", ptr_str);                                                                \
            CMD_STRING_SIZE_PORTLIST(ptr_str, AIR_MAX_LEN_OF_PORT_STR, stack_pbm);                                     \
            osal_printf(" - egs-stack = %s\n", ptr_str);                                                               \
        }                                                                                                              \
    }                                                                                                                  \
    osal_printf(" - stag = %d\n", (__ptr_entry__)->stag);                                                              \
    osal_free(ptr_str);                                                                                                \
} while(0)

/* DATA TYPE DECLARATIONS
 */

/* LOCAL SUBPROGRAM BODIES
 */


/***********************************
 * Command
 ***********************************/
static AIR_ERROR_NO_T
_vlan_cmd_init(
    const C8_T    *tokens[],
    UI32_T        token_idx)
{
    AIR_ERROR_NO_T rc = AIR_E_OK;
    C8_T str[DSH_CMD_MAX_LENGTH] = {0};
    UI32_T unit = 0, vid = 0, fid = 0, port = 0, pri = 0, stag = 0;
    AIR_PORT_BITMAP_T pbm = {0}, tag_pbm = {0};
    BOOL_T ivl = FALSE, copy_pri = FALSE, egs_tag_con = FALSE, egs_tag_ctl_en = FALSE;
    BOOL_T fid_valid = FALSE, pbm_valid = FALSE, ivl_valid = FALSE, copy_pri_valid = FALSE, user_pri_valid = FALSE;
    BOOL_T egs_tag_con_valid = FALSE, egs_tag_ctl_en_valid = FALSE, egs_tag_ctl_valid = FALSE, stag_valid = FALSE;
    AIR_VLAN_PORT_EGS_TAG_CTRL_TYPE_T egs_tag_ctl = AIR_VLAN_PORT_EGS_TAG_CTRL_TYPE_LAST;
    AIR_VLAN_ENTRY_T entry = {0};

    /* parse and get */
    DSH_CHECK_GET_UNIT(tokens, token_idx, &unit, 2);
    DSH_CHECK_PARAM(dsh_getUint(tokens, token_idx, "vid", &vid), token_idx, 2);
    if (AIR_E_OK == dsh_checkString(tokens[token_idx], "fid"))
    {
        fid_valid = TRUE;
        DSH_CHECK_PARAM(dsh_getUint(tokens, token_idx, "fid", &fid), token_idx, 2);
    }
    if (AIR_E_OK == dsh_checkString(tokens[token_idx], "member"))
    {
        DSH_CHECK_PARAM(dsh_getPortBitmap(tokens, token_idx, "member", unit, &pbm), token_idx, 2);
        pbm_valid = TRUE;

    }
    if (AIR_E_OK == dsh_checkString(tokens[token_idx], "ivl"))
    {
        ivl_valid = TRUE;
        DSH_CHECK_PARAM(dsh_getString(tokens, token_idx, "ivl", str), token_idx, 2);
        if (AIR_E_OK == dsh_checkString(str, "enable"))
        {
            ivl = TRUE;
        }
        else if (AIR_E_OK == dsh_checkString(str, "disable"))
        {
            ivl = FALSE;
        }
        else
        {
            return DSH_E_SYNTAX_ERR;
        }
    }
    if (AIR_E_OK == dsh_checkString(tokens[token_idx], "copy-pri"))
    {
        copy_pri_valid = TRUE;
        DSH_CHECK_PARAM(dsh_getString(tokens, token_idx, "copy-pri", str), token_idx, 2);
        if (AIR_E_OK == dsh_checkString(str, "enable"))
        {
            copy_pri = TRUE;
        }
        else if (AIR_E_OK == dsh_checkString(str, "disable"))
        {
            copy_pri = FALSE;
        }
        else
        {
            return DSH_E_SYNTAX_ERR;
        }
    }
    if (AIR_E_OK == dsh_checkString(tokens[token_idx], "user-pri"))
    {
        user_pri_valid = TRUE;
        DSH_CHECK_PARAM(dsh_getUint(tokens, token_idx, "user-pri", &pri), token_idx, 2);
    }
    if (AIR_E_OK == dsh_checkString(tokens[token_idx], "egs-tag-con"))
    {
        egs_tag_con_valid = TRUE;
        DSH_CHECK_PARAM(dsh_getString(tokens, token_idx, "egs-tag-con", str), token_idx, 2);
        if (AIR_E_OK == dsh_checkString(str, "enable"))
        {
            egs_tag_con = TRUE;
        }
        else if (AIR_E_OK == dsh_checkString(str, "disable"))
        {
            egs_tag_con = FALSE;
        }
        else
        {
            return DSH_E_SYNTAX_ERR;
        }
    }
    if (AIR_E_OK == dsh_checkString(tokens[token_idx], "egs-tag-ctl-en"))
    {
        egs_tag_ctl_en_valid = TRUE;
        DSH_CHECK_PARAM(dsh_getString(tokens, token_idx, "egs-tag-ctl-en", str), token_idx, 2);
        if (AIR_E_OK == dsh_checkString(str, "enable"))
        {
            egs_tag_ctl_en = TRUE;
        }
        else if (AIR_E_OK == dsh_checkString(str, "disable"))
        {
            egs_tag_ctl_en = FALSE;
        }
        else
        {
            return DSH_E_SYNTAX_ERR;
        }
    }
    if (AIR_E_OK == dsh_checkString(tokens[token_idx], "portlist"))
    {
        DSH_CHECK_PARAM(dsh_getPortBitmap(tokens, token_idx, "portlist", unit, &tag_pbm), token_idx, 2);
        DSH_CHECK_PARAM(dsh_getString(tokens, token_idx, "egs-tag-ctl", str), token_idx, 2);
        egs_tag_ctl_valid = TRUE;

        if (AIR_E_OK == dsh_checkString(str, "untagged"))
        {
            egs_tag_ctl = AIR_VLAN_PORT_EGS_TAG_CTRL_TYPE_UNTAGGED;
        }
        else if (AIR_E_OK == dsh_checkString(str, "tagged"))
        {
            egs_tag_ctl = AIR_VLAN_PORT_EGS_TAG_CTRL_TYPE_TAGGED;
        }
        else if (AIR_E_OK == dsh_checkString(str, "swap"))
        {
            egs_tag_ctl = AIR_VLAN_PORT_EGS_TAG_CTRL_TYPE_SWAP;
        }
        else if (AIR_E_OK == dsh_checkString(str, "stack"))
        {
            egs_tag_ctl = AIR_VLAN_PORT_EGS_TAG_CTRL_TYPE_STACK;
        }
        else
        {
            return DSH_E_SYNTAX_ERR;
        }
    }
    if (AIR_E_OK == dsh_checkString(tokens[token_idx], "stag"))
    {
        stag_valid = TRUE;
        DSH_CHECK_PARAM(dsh_getUint(tokens, token_idx, "stag", &stag), token_idx, 2);
    }
    DSH_CHECK_LAST_TOKEN(tokens[token_idx]);

    /* check parameter */
    if (vid >= VLAN_CMD_VLAN_ENTRY_NUM)
    {
        osal_printf("***Error***, illegal vid\n");
        return AIR_E_BAD_PARAMETER;
    }

    /* set parameter */
    if (TRUE == fid_valid)
    {
        entry.fid = fid;
    }
    if (TRUE == pbm_valid)
    {
        AIR_PORT_BITMAP_COPY(entry.port_bitmap, pbm);
    }
    if (TRUE == ivl_valid)
    {
        if(ivl)
        {
            entry.flags |= AIR_VLAN_ENTRY_FLAGS_EN_IVL;
        }
        else
        {
            entry.flags &= ~(AIR_VLAN_ENTRY_FLAGS_EN_IVL);
        }
    }
    if (TRUE == copy_pri_valid)
    {
        if(copy_pri)
        {
            entry.flags |= AIR_VLAN_ENTRY_FLAGS_EN_CP_PRI;
        }
        else
        {
            entry.flags &= ~(AIR_VLAN_ENTRY_FLAGS_EN_CP_PRI);
        }
    }
    if (TRUE == user_pri_valid)
    {
        entry.user_pri= pri;
    }
    if (TRUE == egs_tag_con_valid)
    {
        if(egs_tag_con)
        {
            entry.flags |= AIR_VLAN_ENTRY_FLAGS_EN_EG_CON;
        }
        else
        {
            entry.flags &= ~(AIR_VLAN_ENTRY_FLAGS_EN_EG_CON);
        }
    }
    if (TRUE == egs_tag_ctl_en_valid)
    {
        if(egs_tag_ctl_en)
        {
            entry.flags |= AIR_VLAN_ENTRY_FLAGS_EN_ETAG_CTRL;
        }
        else
        {
            entry.flags &= ~(AIR_VLAN_ENTRY_FLAGS_EN_ETAG_CTRL);
        }
    }
    if (TRUE == egs_tag_ctl_valid)
    {
        AIR_PORT_FOREACH(tag_pbm, port)
        {
            entry.egtag_ctl[port] = egs_tag_ctl;
        }
    }
    if (TRUE == stag_valid)
    {
        entry.stag = stag;
    }

    rc = air_vlan_createVlan(unit, vid, &entry);
    if (AIR_E_OK != rc)
    {
        osal_printf("***Error***, vlan init error\n");
    }
    return rc;
}

static AIR_ERROR_NO_T
_vlan_cmd_create(
    const C8_T    *tokens[],
    UI32_T        token_idx)
{
    AIR_ERROR_NO_T rc = AIR_E_OK;
    UI32_T unit = 0, vid = 0;

    /* parse and get */
    DSH_CHECK_GET_UNIT(tokens, token_idx, &unit, 2);
    DSH_CHECK_PARAM(dsh_getUint(tokens, token_idx, "vid", &vid), token_idx, 2);
    DSH_CHECK_LAST_TOKEN(tokens[token_idx]);

    /* check parameter*/
    if (vid >= VLAN_CMD_VLAN_ENTRY_NUM)
    {
        osal_printf("***Error***, illegal vid\n");
        return AIR_E_BAD_PARAMETER;
    }

    rc = air_vlan_createVlan(unit, vid, NULL);
    if (AIR_E_OK != rc)
    {
        osal_printf("***Error***, create vlan error\n");
    }

    return rc;
}

static AIR_ERROR_NO_T
_vlan_cmd_destroy(
    const C8_T    *tokens[],
    UI32_T        token_idx)
{
    AIR_ERROR_NO_T rc = AIR_E_OK;
    UI32_T unit = 0, vid = 0;

    /* parse and get */
    DSH_CHECK_GET_UNIT(tokens, token_idx, &unit, 2);
    DSH_CHECK_PARAM(dsh_getUint(tokens, token_idx, "vid", &vid), token_idx, 2);
    DSH_CHECK_LAST_TOKEN(tokens[token_idx]);

    /* check parameter*/
    if (vid >= VLAN_CMD_VLAN_ENTRY_NUM)
    {
        osal_printf("***Error***, illegal vid\n");
        return AIR_E_BAD_PARAMETER;
    }

    rc = air_vlan_destroyVlan(unit, vid);
    if (rc != AIR_E_OK)
    {
        osal_printf("***Error***, destroy vlan error\n");
    }

    return rc;
}

static AIR_ERROR_NO_T
_vlan_cmd_set(
    const C8_T    *tokens[],
    UI32_T        token_idx)
{
    AIR_ERROR_NO_T rc = AIR_E_OK;
    C8_T str[DSH_CMD_MAX_LENGTH] = {0};
    UI32_T unit = 0, vid = 0, fid = 0, port = 0, pri = 0, stag = 0;
    AIR_PORT_BITMAP_T pbm = {0};
    BOOL_T copy_pri = FALSE, egs_tag_con = FALSE, egs_tag_ctl_en = FALSE;
    BOOL_T fid_valid = FALSE, pbm_valid = FALSE,  copy_pri_valid = FALSE, user_pri_valid = FALSE;
    BOOL_T egs_tag_con_valid = FALSE, egs_tag_ctl_en_valid = FALSE, egs_tag_ctl_valid = FALSE, stag_valid = FALSE;
    AIR_VLAN_PORT_EGS_TAG_CTRL_TYPE_T egs_tag_ctl = AIR_VLAN_PORT_EGS_TAG_CTRL_TYPE_LAST;

    /* parse and get */
    DSH_CHECK_GET_UNIT(tokens, token_idx, &unit, 2);
    DSH_CHECK_PARAM(dsh_getUint(tokens, token_idx, "vid", &vid), token_idx, 2);
    if (AIR_E_OK == dsh_checkString(tokens[token_idx], "fid"))
    {
        fid_valid = TRUE;
        DSH_CHECK_PARAM(dsh_getUint(tokens, token_idx, "fid", &fid), token_idx, 2);
    }
    if (AIR_E_OK == dsh_checkString(tokens[token_idx], "member"))
    {
        DSH_CHECK_PARAM(dsh_getPortBitmap(tokens, token_idx, "member", unit, &pbm), token_idx, 2);
        pbm_valid = TRUE;
    }

    if (AIR_E_OK == dsh_checkString(tokens[token_idx], "copy-pri"))
    {
        copy_pri_valid = TRUE;
        DSH_CHECK_PARAM(dsh_getString(tokens, token_idx, "copy-pri", str), token_idx, 2);
        if (AIR_E_OK == dsh_checkString(str, "enable"))
        {
            copy_pri = TRUE;
        }
        else if (AIR_E_OK == dsh_checkString(str, "disable"))
        {
            copy_pri = FALSE;
        }
        else
        {
            return DSH_E_SYNTAX_ERR;
        }
    }
    if (AIR_E_OK == dsh_checkString(tokens[token_idx], "user-pri"))
    {
        user_pri_valid = TRUE;
        DSH_CHECK_PARAM(dsh_getUint(tokens, token_idx, "user-pri", &pri), token_idx, 2);
    }
    if (AIR_E_OK == dsh_checkString(tokens[token_idx], "egs-tag-con"))
    {
        egs_tag_con_valid = TRUE;
        DSH_CHECK_PARAM(dsh_getString(tokens, token_idx, "egs-tag-con", str), token_idx, 2);
        if (AIR_E_OK == dsh_checkString(str, "enable"))
        {
            egs_tag_con = TRUE;
        }
        else if (AIR_E_OK == dsh_checkString(str, "disable"))
        {
            egs_tag_con = FALSE;
        }
        else
        {
            return DSH_E_SYNTAX_ERR;
        }
    }
    if (AIR_E_OK == dsh_checkString(tokens[token_idx], "egs-tag-ctl-en"))
    {
        egs_tag_ctl_en_valid = TRUE;
        DSH_CHECK_PARAM(dsh_getString(tokens, token_idx, "egs-tag-ctl-en", str), token_idx, 2);
        if (AIR_E_OK == dsh_checkString(str, "enable"))
        {
            egs_tag_ctl_en = TRUE;
        }
        else if (AIR_E_OK == dsh_checkString(str, "disable"))
        {
            egs_tag_ctl_en = FALSE;
        }
        else
        {
            return DSH_E_SYNTAX_ERR;
        }
    }
    if (AIR_E_OK == dsh_checkString(tokens[token_idx], "port"))
    {
        egs_tag_ctl_valid = TRUE;
        DSH_CHECK_PARAM(dsh_getUint(tokens, token_idx, "port", &port), token_idx, 2);
        DSH_CHECK_PARAM(dsh_getString(tokens, token_idx, "egs-tag-ctl", str), token_idx, 2);
        if (AIR_E_OK == dsh_checkString(str, "untagged"))
        {
            egs_tag_ctl = AIR_VLAN_PORT_EGS_TAG_CTRL_TYPE_UNTAGGED;
        }
        else if (AIR_E_OK == dsh_checkString(str, "tagged"))
        {
            egs_tag_ctl = AIR_VLAN_PORT_EGS_TAG_CTRL_TYPE_TAGGED;
        }
        else if (AIR_E_OK == dsh_checkString(str, "swap"))
        {
            egs_tag_ctl = AIR_VLAN_PORT_EGS_TAG_CTRL_TYPE_SWAP;
        }
        else if (AIR_E_OK == dsh_checkString(str, "stack"))
        {
            egs_tag_ctl = AIR_VLAN_PORT_EGS_TAG_CTRL_TYPE_STACK;
        }
        else
        {
            return DSH_E_SYNTAX_ERR;
        }
    }
    if (AIR_E_OK == dsh_checkString(tokens[token_idx], "stag"))
    {
        stag_valid = TRUE;
        DSH_CHECK_PARAM(dsh_getUint(tokens, token_idx, "stag", &stag), token_idx, 2);
    }
    DSH_CHECK_LAST_TOKEN(tokens[token_idx]);

    /* check parameter*/
    if (vid >= VLAN_CMD_VLAN_ENTRY_NUM)
    {
        osal_printf("***Error***, illegal vid\n");
        return AIR_E_BAD_PARAMETER;
    }

    if (fid_valid)
    {
        rc = air_vlan_setFid(unit, vid, fid);
        if (AIR_E_OK != rc)
        {
            osal_printf("***Error***, set vlan fid error\n");
        }
    }

    if (pbm_valid)
    {
        rc = air_vlan_setMember(unit, vid, pbm);
        if (AIR_E_OK != rc)
        {
            osal_printf("***Error***, set vlan member error\n");
        }
    }

    if (copy_pri_valid)
    {
        rc = air_vlan_setPriorityCopy(unit, vid, copy_pri);
        if (AIR_E_OK != rc)
        {
            osal_printf("***Error***, set vlan copy-pri error\n");
        }
    }

    if (user_pri_valid)
    {
        rc = air_vlan_setUserPriority(unit, vid, pri);
        if (AIR_E_OK != rc)
        {
            osal_printf("***Error***, set vlan user-pri error\n");
        }
    }

    if (egs_tag_con_valid)
    {
        rc = air_vlan_setIngressTagKeeping(unit, vid, egs_tag_con);
        if (AIR_E_OK != rc)
        {
            osal_printf("***Error***, set vlan egs-tag-con error\n");
        }
    }

    if (egs_tag_ctl_en_valid)
    {
        rc = air_vlan_setVlanEgressTagCtrl(unit, vid, egs_tag_ctl_en);
        if (AIR_E_OK != rc)
        {
            osal_printf("***Error***, set vlan egs-tag-ctl-en error\n");
        }
    }

    if (egs_tag_ctl_valid)
    {
        rc = air_vlan_setPortEgressTagCtrl(unit, vid, port, egs_tag_ctl);
        if (AIR_E_OK != rc)
        {
            osal_printf("***Error***, set vlan egs-tag-ctl error\n");
        }
    }

    if (stag_valid)
    {
        rc = air_vlan_setVlanStag(unit, vid, stag);
        if (AIR_E_OK != rc)
        {
            osal_printf("***Error***, set vlan stag error\n");
        }
    }

    return rc;
}

static AIR_ERROR_NO_T
_vlan_cmd_show(
    const C8_T    *tokens[],
    UI32_T        token_idx)
{
    AIR_ERROR_NO_T rc = AIR_E_OK;
    UI32_T unit = 0,  vid = 0;
    AIR_VLAN_ENTRY_T *ptr_entry = NULL;

    /* parse and get */
    DSH_CHECK_GET_UNIT(tokens, token_idx, &unit, 2);
    DSH_CHECK_PARAM(dsh_getUint(tokens, token_idx, "vid", &vid), token_idx, 2);

    /* check parameter*/
    if (vid >= VLAN_CMD_VLAN_ENTRY_NUM)
    {
        osal_printf("***Error***, illegal vid\n");
        return AIR_E_BAD_PARAMETER;
    }

    ptr_entry = osal_alloc(sizeof(AIR_VLAN_ENTRY_T), "cmd");
    if (NULL == ptr_entry)
    {
        osal_printf("***Error***, allocate memory fail\n");
        return AIR_E_NO_MEMORY;
    }
    osal_memset(ptr_entry, 0, sizeof(AIR_VLAN_ENTRY_T));

    rc = air_vlan_getVlan(unit, vid, ptr_entry);
    if (AIR_E_OK == rc)
    {
        VLAN_CMD_PRINT_VLAN_ENTRY(unit, vid, ptr_entry);
    }
    else if (AIR_E_ENTRY_NOT_FOUND == rc)
    {
        osal_printf("VLAN %d not found\n", vid);
    }
    else
    {
        osal_printf("***Error***, show VLAN %d fail\n", vid);
    }

    osal_free(ptr_entry);
    return rc;
}

static AIR_ERROR_NO_T
_vlan_cmd_dump(
    const C8_T    *tokens[],
    UI32_T        token_idx)
{
    AIR_ERROR_NO_T rc = AIR_E_OK;
    BOOL_T index_enable = TRUE;
    UI32_T unit = 0, vid = 0, count = 0, wdog_count = 0;
    AIR_VLAN_ENTRY_T *ptr_entry = NULL;

    DSH_CHECK_GET_UNIT(tokens, token_idx, &unit, 2);
    DSH_CHECK_LAST_TOKEN(tokens[token_idx]);

    ptr_entry = osal_alloc(sizeof(AIR_VLAN_ENTRY_T), "cmd");
    if (NULL == ptr_entry)
    {
        osal_printf("***Error***, allocate memory fail\n");
        return AIR_E_NO_MEMORY;
    }
    osal_memset(ptr_entry, 0, sizeof(AIR_VLAN_ENTRY_T));

    for (vid = VLAN_CMD_VLAN_START_NUM; vid < VLAN_CMD_VLAN_ENTRY_NUM; vid++)
    {
        rc = air_vlan_getVlan(unit, vid, ptr_entry);
        if (wdog_count >= AIR_VLAN_WDOG_KICK_NUM)
        {
            osal_wdog_kick();
            wdog_count -= AIR_VLAN_WDOG_KICK_NUM;
        }
        if (AIR_E_OK == rc)
        {
            if (TRUE == index_enable)
            {
                VLAN_CMD_PRINT_VLAN_TBL_INDEX();
                index_enable = FALSE;
            }
            VLAN_CMD_PRINT_VLAN_TBL_CONTENT(unit, vid, ptr_entry);
            osal_memset(ptr_entry, 0, sizeof(AIR_VLAN_ENTRY_T));
            count++;
            wdog_count += count;
        }
        else
        {
            wdog_count++;
            continue;
        }
    }

    if (0 == count)
    {
        osal_printf("VLAN table is empty\n");
    }
    else
    {
        osal_printf("\nTotal valid VLAN %s: %d\n", (count > 1) ? "entries" : "entry", count);
    }

    osal_free(ptr_entry);
    return AIR_E_OK;
}

static AIR_ERROR_NO_T
_vlan_cmd_set_port(
    const C8_T    *tokens[],
    UI32_T        token_idx)
{
    AIR_ERROR_NO_T rc = AIR_E_OK;
    C8_T str[DSH_CMD_MAX_LENGTH] = {0};
    UI32_T unit = 0, port = 0, pvid = 0, psvid = 0;
    AIR_VLAN_ACCEPT_FRAME_TYPE_T accept_frm = AIR_VLAN_ACCEPT_FRAME_TYPE_LAST;
    AIR_VLAN_PORT_ATTR_T port_vlan_attr = AIR_VLAN_PORT_ATTR_LAST;
    AIR_IGR_PORT_EGS_TAG_ATTR_T igs_port_egs_tag_attr = AIR_IGR_PORT_EGS_TAG_ATTR_LAST;
    AIR_VLAN_PORT_EGS_TAG_CTRL_TYPE_T port_egs_tag_attr = AIR_VLAN_PORT_EGS_TAG_CTRL_TYPE_LAST;
    BOOL_T pvid_valid = FALSE, psvid_valid = FALSE, accept_frm_valid = FALSE;
    BOOL_T port_vlan_attr_valid = FALSE, igs_port_egs_tag_attr_valid = FALSE, port_egs_tag_attr_valid = FALSE;

    /* parse and get */
    DSH_CHECK_GET_UNIT(tokens, token_idx, &unit, 2);
    DSH_CHECK_PARAM(dsh_getUint(tokens, token_idx, "port", &port), token_idx, 2);
    if (AIR_E_OK == dsh_checkString(tokens[token_idx], "pvid"))
    {
        pvid_valid = TRUE;
        DSH_CHECK_PARAM(dsh_getUint(tokens, token_idx, "pvid", &pvid), token_idx, 2);
    }
    if (AIR_E_OK == dsh_checkString(tokens[token_idx], "psvid"))
    {
        psvid_valid = TRUE;
        DSH_CHECK_PARAM(dsh_getUint(tokens, token_idx, "psvid", &psvid), token_idx, 2);
    }
    if (AIR_E_OK == dsh_checkString(tokens[token_idx], "accept-frame"))
    {
        accept_frm_valid = TRUE;
        DSH_CHECK_PARAM(dsh_getString(tokens, token_idx, "accept-frame", str), token_idx, 2);
        if (AIR_E_OK == dsh_checkString(str, "all"))
        {
            accept_frm = AIR_VLAN_ACCEPT_FRAME_TYPE_ALL;
        }
        else if (AIR_E_OK == dsh_checkString(str, "tagged"))
        {
            accept_frm = AIR_VLAN_ACCEPT_FRAME_TYPE_TAG_ONLY;
        }
        else if (AIR_E_OK == dsh_checkString(str, "untagged"))
        {
            accept_frm = AIR_VLAN_ACCEPT_FRAME_TYPE_UNTAG_ONLY;
        }
        else
        {
            return DSH_E_SYNTAX_ERR;
        }
    }
    if (AIR_E_OK == dsh_checkString(tokens[token_idx], "vlan-attr"))
    {
        port_vlan_attr_valid = TRUE;
        DSH_CHECK_PARAM(dsh_getString(tokens, token_idx, "vlan-attr", str), token_idx, 2);
        if (AIR_E_OK == dsh_checkString(str, "user"))
        {
            port_vlan_attr = AIR_VLAN_PORT_ATTR_USER_PORT;
        }
        else if (AIR_E_OK == dsh_checkString(str, "transparent"))
        {
            port_vlan_attr = AIR_VLAN_PORT_ATTR_TRANSPARENT_PORT;
        }
        else if (AIR_E_OK == dsh_checkString(str, "stack"))
        {
            port_vlan_attr = AIR_VLAN_PORT_ATTR_STACK_PORT;
        }
        else
        {
            return DSH_E_SYNTAX_ERR;
        }
    }
    if (AIR_E_OK == dsh_checkString(tokens[token_idx], "igs-port-egs-tag-attr"))
    {
        igs_port_egs_tag_attr_valid = TRUE;
        DSH_CHECK_PARAM(dsh_getString(tokens, token_idx, "igs-port-egs-tag-attr", str), token_idx, 2);
        if (AIR_E_OK == dsh_checkString(str, "disable"))
        {
            igs_port_egs_tag_attr = AIR_IGR_PORT_EGS_TAG_ATTR_DISABLE;
        }
        else if (AIR_E_OK == dsh_checkString(str, "untagged"))
        {
            igs_port_egs_tag_attr = AIR_IGR_PORT_EGS_TAG_ATTR_UNTAGGED;
        }
        else if (AIR_E_OK == dsh_checkString(str, "tagged"))
        {
            igs_port_egs_tag_attr = AIR_IGR_PORT_EGS_TAG_ATTR_TAGGED;
        }
        else if (AIR_E_OK == dsh_checkString(str, "swap"))
        {
            igs_port_egs_tag_attr = AIR_IGR_PORT_EGS_TAG_ATTR_SWAP;
        }
        else if (AIR_E_OK == dsh_checkString(str, "stack"))
        {
            igs_port_egs_tag_attr = AIR_IGR_PORT_EGS_TAG_ATTR_STACK;
        }
        else
        {
            return DSH_E_SYNTAX_ERR;
        }
    }
    if (AIR_E_OK == dsh_checkString(tokens[token_idx], "egs-tag-attr"))
    {
        port_egs_tag_attr_valid = TRUE;
        DSH_CHECK_PARAM(dsh_getString(tokens, token_idx, "egs-tag-attr", str), token_idx, 2);
        if (AIR_E_OK == dsh_checkString(str, "untagged"))
        {
            port_egs_tag_attr = AIR_VLAN_PORT_EGS_TAG_CTRL_TYPE_UNTAGGED;
        }
        else if (AIR_E_OK == dsh_checkString(str, "tagged"))
        {
            port_egs_tag_attr = AIR_VLAN_PORT_EGS_TAG_CTRL_TYPE_TAGGED;
        }
        else if (AIR_E_OK == dsh_checkString(str, "swap"))
        {
            port_egs_tag_attr = AIR_VLAN_PORT_EGS_TAG_CTRL_TYPE_SWAP;
        }
        else if (AIR_E_OK == dsh_checkString(str, "stack"))
        {
            port_egs_tag_attr = AIR_VLAN_PORT_EGS_TAG_CTRL_TYPE_STACK;
        }
        else
        {
            return DSH_E_SYNTAX_ERR;
        }
    }
    DSH_CHECK_LAST_TOKEN(tokens[token_idx]);

    if (pvid_valid)
    {
        rc = air_vlan_setPortCvid(unit, port, pvid);
        if (AIR_E_OK != rc)
        {
            osal_printf("***Error***, set vlan port pvid error\n");
        }
    }

    if (psvid_valid)
    {
        rc = air_vlan_setPortSvid(unit, port, psvid);
        if (AIR_E_OK != rc)
        {
            osal_printf("***Error***, set vlan port psvid error\n");
        }
    }

    if (accept_frm_valid)
    {
        rc = air_vlan_setPortAcceptFrameType(unit, port, accept_frm);
        if (AIR_E_OK != rc)
        {
            osal_printf("***Error***, set vlan port accept frame type error\n");
        }
    }

    if (port_vlan_attr_valid)
    {
        rc = air_vlan_setPortAttr(unit, port, port_vlan_attr);
        if (AIR_E_OK != rc)
        {
            osal_printf("***Error***, set vlan port vlan attribute error\n");
        }
    }

    if (igs_port_egs_tag_attr_valid)
    {
        rc = air_vlan_setIngressPortTagAttr(unit, port, igs_port_egs_tag_attr);
        if (AIR_E_OK != rc)
        {
            osal_printf("***Error***, set vlan port ingress port egress tag ctl error\n");
        }
    }

    if (port_egs_tag_attr_valid)
    {
        rc = air_vlan_setPortEgressTagAttr(unit, port, port_egs_tag_attr);
        if (AIR_E_OK != rc)
        {
            osal_printf("***Error***, set vlan port egress tag ctl error\n");
        }
    }

    return rc;
}

static AIR_ERROR_NO_T
_vlan_cmd_show_port(
    const C8_T    *tokens[],
    UI32_T        token_idx)
{
    AIR_ERROR_NO_T rc = AIR_E_OK;
    UI32_T unit = 0, port = 0;
    UI16_T pvid = 0, psvid = 0;
    AIR_VLAN_ACCEPT_FRAME_TYPE_T accept_frm = AIR_VLAN_ACCEPT_FRAME_TYPE_LAST;
    AIR_VLAN_PORT_ATTR_T port_vlan_attr = AIR_VLAN_PORT_ATTR_LAST;
    AIR_IGR_PORT_EGS_TAG_ATTR_T igs_port_egs_tag_attr = AIR_IGR_PORT_EGS_TAG_ATTR_LAST;
    AIR_VLAN_PORT_EGS_TAG_CTRL_TYPE_T port_egs_tag_attr = AIR_VLAN_PORT_EGS_TAG_CTRL_TYPE_LAST;

    /* parse and get */
    DSH_CHECK_GET_UNIT(tokens, token_idx, &unit, 2);
    DSH_CHECK_PARAM(dsh_getUint(tokens, token_idx, "port", &port), token_idx, 2);

    rc = air_vlan_getPortCvid(unit, port, &pvid);
    if (rc != AIR_E_OK)
    {
        osal_printf("***Error***, get vlan port pvid error\n");
        return AIR_E_BAD_PARAMETER;
    }

    rc = air_vlan_getPortSvid(unit, port, &psvid);
    if (rc != AIR_E_OK)
    {
        osal_printf("***Error***, get vlan port psvid error\n");
        return AIR_E_BAD_PARAMETER;
    }

    rc = air_vlan_getPortAcceptFrameType(unit, port, &accept_frm);
    if (rc != AIR_E_OK)
    {
        osal_printf("***Error***, get vlan port accept frame type error\n");
        return AIR_E_BAD_PARAMETER;
    }

    rc = air_vlan_getPortAttr(unit, port, &port_vlan_attr);
    if (rc != AIR_E_OK)
    {
        osal_printf("***Error***, get vlan port vlan attribute error\n");
        return AIR_E_BAD_PARAMETER;
    }

     rc = air_vlan_getIngressPortTagAttr(unit, port, &igs_port_egs_tag_attr);
    if (rc != AIR_E_OK)
    {
        osal_printf("***Error***, get vlan port ingress port egress tag ctl error\n");
        return AIR_E_BAD_PARAMETER;
    }

    rc = air_vlan_getPortEgressTagAttr(unit, port, &port_egs_tag_attr);
    if (rc != AIR_E_OK)
    {
        osal_printf("***Error***, get vlan port egress tag ctl error\n");
        return AIR_E_BAD_PARAMETER;
    }

    osal_printf("Port %-5d:\n", port);
    osal_printf(" - pvid = %d\n", pvid);
    osal_printf(" - psvid = %d\n", psvid);
    osal_printf(" - accept-frame = ");
    if (AIR_VLAN_ACCEPT_FRAME_TYPE_ALL == accept_frm)
    {
        osal_printf("all\n");
    }
    else if (AIR_VLAN_ACCEPT_FRAME_TYPE_TAG_ONLY == accept_frm)
    {
        osal_printf("tagged\n");
    }
    else if (AIR_VLAN_ACCEPT_FRAME_TYPE_UNTAG_ONLY == accept_frm)
    {
        osal_printf("untagged\n");
    }
    else
    {
        osal_printf("unknown\n");
    }

    osal_printf(" - vlan-attr = ");
    if (AIR_VLAN_PORT_ATTR_USER_PORT == port_vlan_attr)
    {
        osal_printf("user\n");
    }
    else if (AIR_VLAN_PORT_ATTR_TRANSPARENT_PORT == port_vlan_attr)
    {
        osal_printf("transparent\n");
    }
    else if (AIR_VLAN_PORT_ATTR_STACK_PORT == port_vlan_attr)
    {
        osal_printf("stack\n");
    }
    else
    {
        osal_printf("unknown\n");
    }

    osal_printf(" - igs-port-egs-tag-attr = ");
    if (AIR_IGR_PORT_EGS_TAG_ATTR_DISABLE == igs_port_egs_tag_attr)
    {
        osal_printf("disable\n");
    }
    else if (AIR_IGR_PORT_EGS_TAG_ATTR_UNTAGGED == igs_port_egs_tag_attr)
    {
        osal_printf("untagged\n");
    }
    else if (AIR_IGR_PORT_EGS_TAG_ATTR_TAGGED == igs_port_egs_tag_attr)
    {
        osal_printf("tagged\n");
    }
    else if (AIR_IGR_PORT_EGS_TAG_ATTR_SWAP == igs_port_egs_tag_attr)
    {
        osal_printf("swap\n");
    }
    else if (AIR_IGR_PORT_EGS_TAG_ATTR_STACK == igs_port_egs_tag_attr)
    {
        osal_printf("stack\n");
    }
    else
    {
        osal_printf("unknown\n");
    }

    osal_printf(" - egs-tag-attr = ");
    if (AIR_VLAN_PORT_EGS_TAG_CTRL_TYPE_UNTAGGED == port_egs_tag_attr)
    {
        osal_printf("untagged\n");
    }
    else if (AIR_VLAN_PORT_EGS_TAG_CTRL_TYPE_TAGGED == port_egs_tag_attr)
    {
        osal_printf("tagged\n");
    }
    else if (AIR_VLAN_PORT_EGS_TAG_CTRL_TYPE_SWAP == port_egs_tag_attr)
    {
        osal_printf("swap\n");
    }
    else if (AIR_VLAN_PORT_EGS_TAG_CTRL_TYPE_STACK == port_egs_tag_attr)
    {
        osal_printf("stack\n");
    }
    else
    {
        osal_printf("unknown\n");
    }

    return rc;
}

static AIR_ERROR_NO_T
_vlan_cmd_addMember(
    const C8_T    *tokens[],
    UI32_T        token_idx)
{
    AIR_ERROR_NO_T rc = AIR_E_OK;
    UI32_T unit = 0, vid = 0, port = 0;
    AIR_PORT_BITMAP_T pbm;

    /* parse and get */
    DSH_CHECK_GET_UNIT(tokens, token_idx, &unit, 2);
    DSH_CHECK_PARAM(dsh_getUint(tokens, token_idx, "vid", &vid), token_idx, 2);
    DSH_CHECK_PARAM(dsh_getPortBitmap(tokens, token_idx, "portlist", unit, &pbm), token_idx, 2);
    DSH_CHECK_LAST_TOKEN(tokens[token_idx]);

    /* check parameter*/
    if ((vid >= VLAN_CMD_VLAN_ENTRY_NUM) || (0 == vid))
    {
        osal_printf("***Error***, illegal vid\n");
        return AIR_E_BAD_PARAMETER;
    }

    AIR_PORT_FOREACH(pbm, port)
    {
        rc = air_vlan_addMemberPort(unit, vid, port);
        if (AIR_E_OK != rc)
        {
            osal_printf("***Error***, add vlan member error\n");
        }
    }

    return rc;
}

static AIR_ERROR_NO_T
_vlan_cmd_delMember(
    const C8_T    *tokens[],
    UI32_T        token_idx)
{
    AIR_ERROR_NO_T rc = AIR_E_OK;
    UI32_T unit = 0, vid = 0, port = 0;
    AIR_PORT_BITMAP_T pbm;

    /* parse and get */
    DSH_CHECK_GET_UNIT(tokens, token_idx, &unit, 2);
    DSH_CHECK_PARAM(dsh_getUint(tokens, token_idx, "vid", &vid), token_idx, 2);
    DSH_CHECK_PARAM(dsh_getPortBitmap(tokens, token_idx, "portlist", unit, &pbm), token_idx, 2);
    DSH_CHECK_LAST_TOKEN(tokens[token_idx]);

    /* check parameter*/
    if ((vid >= VLAN_CMD_VLAN_ENTRY_NUM) || (0 == vid))
    {
        osal_printf("***Error***, illegal vid\n");
        return AIR_E_BAD_PARAMETER;
    }

    AIR_PORT_FOREACH(pbm, port)
    {
        rc = air_vlan_delMemberPort(unit, vid, port);
        if (AIR_E_OK != rc)
        {
            osal_printf("***Error***, del vlan member error\n");
        }
    }

    return rc;
}

static AIR_ERROR_NO_T
_vlan_cmd_addMacBased(
    const C8_T    *tokens[],
    UI32_T        token_idx)
{
    AIR_ERROR_NO_T              rc = AIR_E_OK;
    UI32_T                      unit = 0;
    UI32_T                      idx = 0;
    UI32_T                      vid = 0;
    UI32_T                      pri = 0;
    AIR_MAC_T                   mac;
    AIR_MAC_T                   mask;
    AIR_VLAN_MAC_BASED_ENTRY_T  entry;

    /* cmd: vlan add mac-based [ unit=<UINT> ] idx=<UINT>
     *      mac=<MACADDR> mac-mask=<MACADDR> vid=<UINT> pri=<UINT> */

    osal_memset(&entry, 0, sizeof(AIR_VLAN_MAC_BASED_ENTRY_T));

    /* parse and get */
    DSH_CHECK_GET_UNIT(tokens, token_idx, &unit, 2);
    DSH_CHECK_PARAM(dsh_getUint(tokens, token_idx, "idx", &idx), token_idx, 2);
    DSH_CHECK_PARAM(dsh_getMacAddr(tokens, token_idx, "mac", &mac), token_idx, 2);
    DSH_CHECK_PARAM(dsh_getMacAddr(tokens, token_idx, "mac-mask", &mask), token_idx, 2);
    DSH_CHECK_PARAM(dsh_getUint(tokens, token_idx, "vid", &vid), token_idx, 2);
    DSH_CHECK_PARAM(dsh_getUint(tokens, token_idx, "pri", &pri), token_idx, 2);
    DSH_CHECK_LAST_TOKEN(tokens[token_idx]);

    osal_memcpy(entry.mac, mac, sizeof(AIR_MAC_T));
    osal_memcpy(entry.mac_mask, mask, sizeof(AIR_MAC_T));
    entry.vid = vid;
    entry.pri = pri;

    rc = air_vlan_addMacBasedVlan(unit, idx, &entry);
    if(AIR_E_OK != rc)
    {
        osal_printf("***Error***, add mac based error\n");
    }

    return rc;
}

static AIR_ERROR_NO_T
_vlan_cmd_getMacBased(
    const C8_T    *tokens[],
    UI32_T        token_idx)
{
    AIR_ERROR_NO_T              rc = AIR_E_OK;
    UI32_T                      unit = 0;
    UI32_T                      idx = 0;
    AIR_VLAN_MAC_BASED_ENTRY_T  entry;

    /* cmd: vlan get mac-based [ unit=<UNIT> ] idx=<UINT> */

    osal_memset(&entry, 0, sizeof(AIR_VLAN_MAC_BASED_ENTRY_T));

    /* parse and get */
    DSH_CHECK_GET_UNIT(tokens, token_idx, &unit, 2);
    DSH_CHECK_PARAM(dsh_getUint(tokens, token_idx, "idx", &idx), token_idx, 2);
    DSH_CHECK_LAST_TOKEN(tokens[token_idx]);

    rc = air_vlan_getMacBasedVlan(unit, idx, &entry);
    if(AIR_E_OK != rc)
    {
        osal_printf("***Error***, show mac based error\n");
        return rc;
    }

    osal_printf("idx:       %u\n", idx);
    osal_printf("mac:       ");
    dsh_showMacAddr((const AIR_MAC_T*)&entry.mac);
    osal_printf("\n");
    osal_printf("mac-mask:  ");
    dsh_showMacAddr((const AIR_MAC_T*)&entry.mac_mask);
    osal_printf("\n");
    osal_printf("vid:       %u\n", entry.vid);
    osal_printf("pri:       %u\n", entry.pri);

    return rc;
}

static AIR_ERROR_NO_T
_vlan_cmd_delMacBased(
    const C8_T    *tokens[],
    UI32_T        token_idx)
{
    AIR_ERROR_NO_T  rc = AIR_E_OK;
    UI32_T          unit = 0;
    UI32_T          idx = 0;

    /* cmd: vlan del mac-based [ unit=<UNIT> ] idx=<UINT> */

    /* parse and get */
    DSH_CHECK_GET_UNIT(tokens, token_idx, &unit, 2);
    DSH_CHECK_PARAM(dsh_getUint(tokens, token_idx, "idx", &idx), token_idx, 2);
    DSH_CHECK_LAST_TOKEN(tokens[token_idx]);

    rc = air_vlan_delMacBasedVlan(unit, idx);
    if(AIR_E_OK != rc)
    {
        osal_printf("***Error***, delete mac based error\n");
    }

    return rc;
}

static AIR_ERROR_NO_T
_vlan_cmd_addSubnetBased(
    const C8_T    *tokens[],
    UI32_T        token_idx)
{
    AIR_ERROR_NO_T                  rc = AIR_E_OK;
    UI32_T                          unit = 0;
    UI32_T                          idx = 0;
    UI32_T                          vid = 0;
    UI32_T                          pri = 0;
    AIR_IP_ADDR_T                   ipAddr;
    AIR_IP_ADDR_T                   ipMask;
    AIR_VLAN_SUBNET_BASED_ENTRY_T   entry;

    /* cmd: vlan add subnet-based [ unit=<UINT> ] idx=<UINT>
     *      ip=<IPADDR> ip-mask=<IPADDR> vid=<UINT> pri=<UINT>
     */

    osal_memset(&entry, 0, sizeof(AIR_VLAN_SUBNET_BASED_ENTRY_T));

    /* parse and get */
    DSH_CHECK_GET_UNIT(tokens, token_idx, &unit, 2);
    DSH_CHECK_PARAM(dsh_getUint(tokens, token_idx, "idx", &idx), token_idx, 2);
    if(AIR_E_OK == (rc = dsh_checkString(tokens[token_idx], "ip")))
    {
        DSH_CHECK_IP_ADDR(dsh_getIpv4Addr(tokens, token_idx, "ip", &ipAddr.ip_addr.ipv4_addr),
                          dsh_getIpv6Addr(tokens, token_idx, "ip", &ipAddr.ip_addr.ipv6_addr),
                          ipAddr.ipv4, token_idx, 2);
    }
    else
    {
        return rc;
    }
    if(AIR_E_OK == (rc = dsh_checkString(tokens[token_idx], "ip-mask")))
    {
        DSH_CHECK_IP_ADDR(dsh_getIpv4Addr(tokens, token_idx, "ip-mask", &ipMask.ip_addr.ipv4_addr),
                          dsh_getIpv6Addr(tokens, token_idx, "ip-mask", &ipMask.ip_addr.ipv6_addr),
                          ipMask.ipv4, token_idx, 2);
    }
    else
    {
        return rc;
    }
    DSH_CHECK_PARAM(dsh_getUint(tokens, token_idx, "vid", &vid), token_idx, 2);
    DSH_CHECK_PARAM(dsh_getUint(tokens, token_idx, "pri", &pri), token_idx, 2);
    DSH_CHECK_LAST_TOKEN(tokens[token_idx]);

    osal_memcpy(&entry.ip_addr, &ipAddr, sizeof(AIR_IP_ADDR_T));
    osal_memcpy(&entry.ip_mask, &ipMask, sizeof(AIR_IP_ADDR_T));
    entry.vid = vid;
    entry.pri = pri;

    rc = air_vlan_addSubnetBasedVlan(unit, idx, &entry);
    if(AIR_E_OK != rc)
    {
        osal_printf("***Error***, add subnet based error\n");
    }

    return rc;
}

static AIR_ERROR_NO_T
_vlan_cmd_getSubnetBased(
    const C8_T    *tokens[],
    UI32_T        token_idx)
{
    AIR_ERROR_NO_T                  rc = AIR_E_OK;
    UI32_T                          unit = 0;
    UI32_T                          idx = 0;
    AIR_VLAN_SUBNET_BASED_ENTRY_T   entry;

    /* cmd: vlan get subnet-based [ unit=<UINT> ] idx=<UINT>
     */

    osal_memset(&entry, 0, sizeof(AIR_VLAN_SUBNET_BASED_ENTRY_T));

    /* parse and get */
    DSH_CHECK_GET_UNIT(tokens, token_idx, &unit, 2);
    DSH_CHECK_PARAM(dsh_getUint(tokens, token_idx, "idx", &idx), token_idx, 2);
    DSH_CHECK_LAST_TOKEN(tokens[token_idx]);

    rc = air_vlan_getSubnetBasedVlan(unit, idx, &entry);
    if(AIR_E_OK != rc)
    {
        osal_printf("***Error***, show subnet based error\n");
        return rc;
    }

    osal_printf("idx:       %u\n", idx);
    osal_printf("ip:        ");
    dsh_showIpAddr(&entry.ip_addr);
    osal_printf("\n");
    osal_printf("ip-mask:   ");
    dsh_showIpAddr(&entry.ip_mask);
    osal_printf("\n");
    osal_printf("vid:       %u\n", entry.vid);
    osal_printf("pri:       %u\n", entry.pri);

    return rc;
}

static AIR_ERROR_NO_T
_vlan_cmd_delSubnetBased(
    const C8_T    *tokens[],
    UI32_T        token_idx)
{
    AIR_ERROR_NO_T  rc = AIR_E_OK;
    UI32_T          unit = 0;
    UI32_T          idx = 0;

    /* cmd: vlan del subnet-based [ unit=<UINT> ] idx=<UINT>
     */

    /* parse and get */
    DSH_CHECK_GET_UNIT(tokens, token_idx, &unit, 2);
    DSH_CHECK_PARAM(dsh_getUint(tokens, token_idx, "idx", &idx), token_idx, 2);
    DSH_CHECK_LAST_TOKEN(tokens[token_idx]);

    rc = air_vlan_delSubnetBasedVlan(unit, idx);
    if(AIR_E_OK != rc)
    {
        osal_printf("***Error***, del subnet based error\n");
    }

    return rc;
}

static AIR_ERROR_NO_T
_vlan_cmd_addProtocolBased(
    const C8_T    *tokens[],
    UI32_T        token_idx)
{
    AIR_ERROR_NO_T                          rc = AIR_E_OK;
    UI32_T                                  unit = 0;
    UI32_T                                  idx = 0;
    UI32_T                                  group_id = 0;
    UI32_T                                  type_data[2];
    C8_T                                    frame_type_str[DSH_CMD_MAX_LENGTH];
    AIR_VLAN_PROTOCOL_BASED_FRAME_TYPE_T    frame_type;
    AIR_VLAN_PROTOCOL_BASED_ENTRY_T         entry;

    /* cmd: vlan add protocol-based [ unit=<UINT> ] idx=<UINT>
     *      frame-type={ ethernet | rfc1042 | llc | snap } type-data=<HEX> group-id=<UINT>
     */

    osal_memset(&entry, 0, sizeof(AIR_VLAN_PROTOCOL_BASED_ENTRY_T));

    /* parse and get */
    DSH_CHECK_GET_UNIT(tokens, token_idx, &unit, 2);
    DSH_CHECK_PARAM(dsh_getUint(tokens, token_idx, "idx", &idx), token_idx, 2);
    DSH_CHECK_PARAM(dsh_getString(tokens, token_idx, "frame-type", frame_type_str), token_idx, 2);

    if(AIR_E_OK == dsh_checkString(frame_type_str, "ethernet"))
    {
        frame_type = AIR_VLAN_PROTOCOL_BASED_FRAME_TYPE_ETHERNET;
        DSH_CHECK_PARAM(dsh_getHex(tokens, token_idx, "ethertype", type_data, sizeof(UI32_T)), token_idx, 2);
    }
    else if(AIR_E_OK == dsh_checkString(frame_type_str, "rfc1042"))
    {
        frame_type = AIR_VLAN_PROTOCOL_BASED_FRAME_TYPE_RFC1042;
        DSH_CHECK_PARAM(dsh_getHex(tokens, token_idx, "ethertype", type_data, sizeof(UI32_T)), token_idx, 2);
    }
    else if(AIR_E_OK == dsh_checkString(frame_type_str, "llc-other"))
    {
        frame_type = AIR_VLAN_PROTOCOL_BASED_FRAME_TYPE_LLC_OTHER;
        DSH_CHECK_PARAM(dsh_getHex(tokens, token_idx, "sap", type_data, sizeof(UI32_T)), token_idx, 2);
    }
    else if(AIR_E_OK == dsh_checkString(frame_type_str, "snap-other"))
    {
        frame_type = AIR_VLAN_PROTOCOL_BASED_FRAME_TYPE_SNAP_OTHER;
        DSH_CHECK_PARAM(dsh_getHex(tokens, token_idx, "pid", type_data, sizeof(UI32_T)*2), token_idx, 2);
    }
    else
    {
        return DSH_E_SYNTAX_ERR;
    }

    DSH_CHECK_PARAM(dsh_getUint(tokens, token_idx, "group-id", &group_id), token_idx, 2);
    DSH_CHECK_LAST_TOKEN(tokens[token_idx]);

    entry.frame_type = frame_type;
    osal_memcpy(&entry.type_data, type_data, sizeof(UI32_T)*2);
    entry.group_id = group_id;

    rc = air_vlan_addProtocolBasedVlan(unit, idx, &entry);
    if(AIR_E_OK != rc)
    {
        osal_printf("***Error***, add protocol based error\n");
    }

    return rc;
}

static AIR_ERROR_NO_T
_vlan_cmd_getProtocolBased(
    const C8_T    *tokens[],
    UI32_T        token_idx)
{
    AIR_ERROR_NO_T                      rc = AIR_E_OK;
    UI32_T                              unit = 0;
    UI32_T                              idx = 0;
    AIR_VLAN_PROTOCOL_BASED_ENTRY_T     entry;

    /* cmd: vlan get protocol-based [ unit=<UINT> ] idx=<UINT>
     */

    osal_memset(&entry, 0, sizeof(AIR_VLAN_PROTOCOL_BASED_ENTRY_T));

    /* parse and get */
    DSH_CHECK_GET_UNIT(tokens, token_idx, &unit, 2);
    DSH_CHECK_PARAM(dsh_getUint(tokens, token_idx, "idx", &idx), token_idx, 2);
    DSH_CHECK_LAST_TOKEN(tokens[token_idx]);

    rc = air_vlan_getProtocolBasedVlan(unit, idx, &entry);
    if(AIR_E_OK != rc)
    {
        osal_printf("***Error***, show protocol based error\n");
        return rc;
    }

    osal_printf("idx:           %u\n", idx);
    osal_printf("frame-type:    %s\n",
        ((AIR_VLAN_PROTOCOL_BASED_FRAME_TYPE_ETHERNET == entry.frame_type)? "ethernet" :
        (AIR_VLAN_PROTOCOL_BASED_FRAME_TYPE_RFC1042 == entry.frame_type)?   "rfc1042" :
        (AIR_VLAN_PROTOCOL_BASED_FRAME_TYPE_LLC_OTHER == entry.frame_type)? "llc-other" :
        (AIR_VLAN_PROTOCOL_BASED_FRAME_TYPE_SNAP_OTHER == entry.frame_type)?"snap-other" : ""));
    if(AIR_VLAN_PROTOCOL_BASED_FRAME_TYPE_ETHERNET == entry.frame_type)
    {
        osal_printf("ethertype:     0x%04x\n", entry.type_data.eth_type);
    }
    else if(AIR_VLAN_PROTOCOL_BASED_FRAME_TYPE_RFC1042 == entry.frame_type)
    {
        osal_printf("ethertype:     0x%04x\n", entry.type_data.rfc1042_eth_type);
    }
    else if(AIR_VLAN_PROTOCOL_BASED_FRAME_TYPE_LLC_OTHER == entry.frame_type)
    {
        osal_printf("sap:           0x%04x\n", entry.type_data.llc_dsap_ssap);
    }
    else if(entry.frame_type == AIR_VLAN_PROTOCOL_BASED_FRAME_TYPE_SNAP_OTHER)
    {
        osal_printf("pid:           0x%02x%08x\n", entry.type_data.snap_pid[1], entry.type_data.snap_pid[0]);
    }
    else
    {
        osal_printf("***Error***, unknown frame type\n");
    }
    osal_printf("group-id:      %u\n", entry.group_id);

    return rc;
}

static AIR_ERROR_NO_T
_vlan_cmd_delProtocolBased(
    const C8_T    *tokens[],
    UI32_T        token_idx)
{
    AIR_ERROR_NO_T  rc = AIR_E_OK;
    UI32_T          unit = 0;
    UI32_T          idx = 0;

    /* cmd: vlan del protocol-based [ unit=<UINT> ] idx=<UINT>
     */

    /* parse and get */
    DSH_CHECK_GET_UNIT(tokens, token_idx, &unit, 2);
    DSH_CHECK_PARAM(dsh_getUint(tokens, token_idx, "idx", &idx), token_idx, 2);
    DSH_CHECK_LAST_TOKEN(tokens[token_idx]);

    rc = air_vlan_delProtocolBasedVlan(unit, idx);
    if(AIR_E_OK != rc)
    {
        osal_printf("***Error***, delete protocol based error\n");
    }

    return rc;
}

static AIR_ERROR_NO_T
_vlan_cmd_setProtocolBasedAttr(
    const C8_T    *tokens[],
    UI32_T        token_idx)
{
    AIR_ERROR_NO_T      rc = AIR_E_OK;
    UI32_T              unit = 0;
    UI32_T              group_id = 0;
    UI32_T              vid = 0;
    UI32_T              pri = 0;
    UI32_T              port = 0;
    AIR_PORT_BITMAP_T   pbm = {0};

    /* cmd: vlan set protocol-based-attr [ unit=<UINT> ]
     *      portlist=<UINTLIST> group-id=<UINT> vid=<UINT> pri=<UINT>
     */

    /* parse and get */
    DSH_CHECK_GET_UNIT(tokens, token_idx, &unit, 2);
    DSH_CHECK_PARAM(dsh_getPortBitmap(tokens, token_idx, "portlist", unit, &pbm), token_idx, 2);
    DSH_CHECK_PARAM(dsh_getUint(tokens, token_idx, "group-id", &group_id), token_idx, 2);
    DSH_CHECK_PARAM(dsh_getUint(tokens, token_idx, "vid", &vid), token_idx, 2);
    DSH_CHECK_PARAM(dsh_getUint(tokens, token_idx, "pri", &pri), token_idx, 2);
    DSH_CHECK_LAST_TOKEN(tokens[token_idx]);

    AIR_PORT_FOREACH(pbm, port)
    {
        rc = air_vlan_setProtocolBasedVlanPortAttr(unit, port, group_id, vid, pri);
        if(AIR_E_OK != rc)
        {
            osal_printf("***Error***, set protocol based attr error\n");
            return rc;
        }
    }

    return rc;
}

static AIR_ERROR_NO_T
_vlan_cmd_getProtocolBasedAttr(
    const C8_T    *tokens[],
    UI32_T        token_idx)
{
    AIR_ERROR_NO_T      rc = AIR_E_OK;
    UI32_T              unit = 0;
    UI32_T              group_id = 0;
    UI16_T              vid = 0;
    UI16_T              pri = 0;
    UI32_T              port = 0;
    AIR_PORT_BITMAP_T   pbm = {0};

    /* cmd: vlan get protocol-based-attr [ unit=<UINT> ] portlist=<UINTLIST>
     */

    /* parse and get */
    DSH_CHECK_GET_UNIT(tokens, token_idx, &unit, 2);
    DSH_CHECK_PARAM(dsh_getPortBitmap(tokens, token_idx, "portlist", unit, &pbm), token_idx, 2);
    DSH_CHECK_PARAM(dsh_getUint(tokens, token_idx, "group-id", &group_id), token_idx, 2);
    DSH_CHECK_LAST_TOKEN(tokens[token_idx]);

    osal_printf("%10s %8s %5s %5s\n","unit/port", "group-id", "vid", "pri");
    AIR_PORT_FOREACH(pbm, port)
    {
        rc = air_vlan_getProtocolBasedVlanPortAttr(unit, port, group_id, &vid, &pri);
        if(AIR_E_OK != rc)
        {
            osal_printf("***Error***,show protocol based attr error\n");
            return rc;
        }
        osal_printf("%4u/%2u %11u %5u %5u\n", unit, port, group_id, vid, pri);
    }

    return rc;
}

static AIR_ERROR_NO_T
_vlan_cmd_setIngressVlanFilterMode(
    const C8_T    *tokens[],
    UI32_T        token_idx)
{
    AIR_ERROR_NO_T      rc = AIR_E_OK;
    UI32_T              unit = 0;
    UI32_T              port = 0;
    BOOL_T              mode = 0;
    AIR_PORT_BITMAP_T   pbm = {0};
    C8_T                st[DSH_CMD_MAX_LENGTH] = {0};

    /* cmd: vlan set ingress-filter [ unit=<UINT> ] portlist=<UINTLIST> mode={ enable | disable }
     */

    /* parse and get */
    DSH_CHECK_GET_UNIT(tokens, token_idx, &unit, 2);
    DSH_CHECK_PARAM(dsh_getPortBitmap(tokens, token_idx, "portlist", unit, &pbm), token_idx, 2);
    DSH_CHECK_PARAM(dsh_getString(tokens, token_idx, "mode", st), token_idx, 2);
    DSH_CHECK_LAST_TOKEN(tokens[token_idx]);

    if(AIR_E_OK == dsh_checkString(st, "enable"))
    {
        mode = TRUE;
    }
    else if(AIR_E_OK != dsh_checkString(st, "disable"))
    {
        mode = FALSE;
    }

    AIR_PORT_FOREACH(pbm, port)
    {
        rc = air_vlan_setIngressVlanFilter(unit, port, mode);
        if(AIR_E_OK != rc)
        {
            osal_printf("***Error***, set port(%u) VLAN ingress filter mode error(%d)\n", port, rc);
            return rc;
        }
    }

    return rc;
}

static AIR_ERROR_NO_T
_vlan_cmd_getIngressVlanFilterMode(
    const C8_T    *tokens[],
    UI32_T        token_idx)
{
    AIR_ERROR_NO_T      rc = AIR_E_OK;
    UI32_T              unit = 0;
    UI32_T              port = 0;
    BOOL_T              state = 0;
    AIR_PORT_BITMAP_T   pbm = {0};

    /* cmd: vlan show ingress-filter [ unit=<UINT> ] portlist=<UINTLIST>
     */

    /* parse and get */
    DSH_CHECK_GET_UNIT(tokens, token_idx, &unit, 2);
    DSH_CHECK_PARAM(dsh_getPortBitmap(tokens, token_idx, "portlist", unit, &pbm), token_idx, 2);
    DSH_CHECK_LAST_TOKEN(tokens[token_idx]);

    osal_printf("unit %u\n", unit);
    AIR_PORT_FOREACH(pbm, port)
    {
        osal_printf(" - Port = %u\n - Status = ", port);
        rc = air_vlan_getIngressVlanFilter(unit, port, &state);
        if(AIR_E_OK == rc)
        {
            osal_printf("%s\n", (TRUE == state) ? "Enable" : "Disable");
        }
        else if(AIR_E_NOT_SUPPORT == rc)
        {
            osal_printf("Not Support\n");
        }
        else
        {
            osal_printf("***Error***, show port(%u) VLAN ingress filter mode error(%d)\n", port, rc);
            return rc;
        }
    }

    return rc;
}

/* -------------------------------------------------------------- callback */
const static DSH_VEC_T  _vlan_cmd_vec[] =
{
    {
        "init", 1, _vlan_cmd_init,
        "vlan init [ unit=<UINT> ] vid=<UINT> [ fid=<UINT> ] [ member=<UINTLIST>] [ ivl={ enable | disable } ] "
        "[ copy-pri={ enable | disable } ] [ user-pri=<UINT> ] [ egs-tag-con={ enable | disable } ] "
        "[ egs-tag-ctl-en={ enable | disable } ] [ portlist=<UINTLIST> egs-tag-ctl={ untagged | tagged | swap | stack } [ stag=<UINT> ] ]\n"
    },
    {
        "create", 1, _vlan_cmd_create,
        "vlan create [ unit=<UINT> ] vid=<UINT>\n"
    },
    {
        "destroy", 1, _vlan_cmd_destroy,
        "vlan destroy [ unit=<UINT> ] vid=<UINT>\n"
    },
    {
        "set vlan-attr", 2, _vlan_cmd_set,
        "vlan set vlan-attr [ unit=<UINT> ] vid=<UINT> [ fid=<UINT> ] [ member=<UINTLIST> ] "
        "[ copy-pri={ enable | disable } ] [ user-pri=<UINT> ] [ egs-tag-con={ enable | disable } ] "
        "[ egs-tag-ctl-en={ enable | disable } ] [ port=<UINT> egs-tag-ctl={ untagged | tagged | swap | stack } [ stag=<UINT> ] ]\n"
    },
    {
        "show vlan-attr", 2, _vlan_cmd_show,
        "vlan show vlan-attr [ unit=<UINT> ] vid=<UINT>\n"
    },
    {
        "dump", 1, _vlan_cmd_dump,
        "vlan dump [ unit=<UINT> ]\n"
    },
    {
        "port set", 2, _vlan_cmd_set_port,
        "vlan port set [ unit=<UINT> ] port=<UINT> [ pvid=<UINT> ] [ psvid=<UINT> ] [ accept-frame={ all | tagged | untagged } ] "
        "[ vlan-attr={ user | transparent | stack }] "
        "[ igs-port-egs-tag-attr={ disable | untagged | tagged | swap | stack } ] "
        "[ egs-tag-attr={ untagged | tagged | swap | stack } ]\n"
    },
    {
        "port show", 2, _vlan_cmd_show_port,
        "vlan port show [ unit=<UINT> ] port=<UINT>\n"
    },
    {
        "add member", 2, _vlan_cmd_addMember,
        "vlan add member [ unit=<UINT> ] vid=<UINT> portlist=<UINTLIST>\n"
    },
    {
        "del member", 2, _vlan_cmd_delMember,
        "vlan del member [ unit=<UINT> ] vid=<UINT> portlist=<UINTLIST>\n"
    },
    {
        "add mac-based", 2, _vlan_cmd_addMacBased,
        "vlan add mac-based [ unit=<UINT> ] idx=<UINT> mac=<MACADDR> mac-mask=<MACADDR> vid=<UINT> pri=<UINT>\n"
    },
    {
        "show mac-based", 2, _vlan_cmd_getMacBased,
        "vlan show mac-based [ unit=<UINT> ] idx=<UINT>\n"
    },
    {
        "del mac-based", 2, _vlan_cmd_delMacBased,
        "vlan del mac-based [ unit=<UINT> ] idx=<UINT>\n"
    },
    {
        "add subnet-based", 2, _vlan_cmd_addSubnetBased,
        "vlan add subnet-based [ unit=<UINT> ] idx=<UINT> ip=<IPADDR> ip-mask=<IPADDR> vid=<UINT> pri=<UINT>\n"
    },
    {
        "show subnet-based", 2, _vlan_cmd_getSubnetBased,
        "vlan show subnet-based [ unit=<UINT> ] idx=<UINT>\n"
    },
    {
        "del subnet-based", 2, _vlan_cmd_delSubnetBased,
        "vlan del subnet-based [ unit=<UINT> ] idx=<UINT>\n"
    },
    {
        "add protocol-based", 2, _vlan_cmd_addProtocolBased,
        "vlan add protocol-based [ unit=<UINT> ] idx=<UINT> frame-type={ ethernet ethertype=<HEX> | rfc1042 ethertype=<HEX> | llc-other sap=<HEX> | snap-other pid=<HEX> } group-id=<UINT>\n"
    },
    {
        "show protocol-based", 2, _vlan_cmd_getProtocolBased,
        "vlan show protocol-based [ unit=<UINT> ] idx=<UINT>\n"
    },
    {
        "del protocol-based", 2, _vlan_cmd_delProtocolBased,
        "vlan del protocol-based [ unit=<UINT> ] idx=<UINT>\n"
    },
    {
        "set protocol-based-attr", 2, _vlan_cmd_setProtocolBasedAttr,
        "vlan set protocol-based-attr [ unit=<UINT> ] portlist=<UINTLIST> group-id=<UINT> vid=<UINT> pri=<UINT>\n"
    },
    {
        "show protocol-based-attr", 2, _vlan_cmd_getProtocolBasedAttr,
        "vlan show protocol-based-attr [ unit=<UINT> ] portlist=<UINTLIST> group-id=<UINT>\n"
    },
    {
        "set ingress-filter", 2, _vlan_cmd_setIngressVlanFilterMode,
        "vlan set ingress-filter [ unit=<UINT> ] portlist=<UINTLIST> mode={ enable | disable }\n"
    },
    {
        "show ingress-filter", 2, _vlan_cmd_getIngressVlanFilterMode,
        "vlan show ingress-filter [ unit=<UINT> ] portlist=<UINTLIST>\n"
    },
};

AIR_ERROR_NO_T
vlan_cmd_dispatcher(
    const C8_T                  *tokens[],
    UI32_T                      token_idx)
{
    int rv=0;
    rv = (dsh_dispatcher(tokens, token_idx, _vlan_cmd_vec, sizeof(_vlan_cmd_vec)/sizeof(DSH_VEC_T)));
    return  rv;
}

AIR_ERROR_NO_T
vlan_cmd_usager()
{
    int rv=0;
    rv = (dsh_usager(_vlan_cmd_vec, sizeof(_vlan_cmd_vec)/sizeof(DSH_VEC_T)));
    return rv;
}

