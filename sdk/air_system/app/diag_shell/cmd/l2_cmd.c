/*******************************************************************************
*  Copyright Statement:
*  --------------------
*  This software is protected by Copyright and the information contained
*  herein is confidential. The software may not be copied and the information
*  contained herein may not be used or disclosed except with the written
*  permission of Airoha Technology Corp. (C) 2021
*
*  BY OPENING THIS FILE, BUYER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
*  THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("AIROHA SOFTWARE")
*  RECEIVED FROM AIROHA AND/OR ITS REPRESENTATIVES ARE PROVIDED TO BUYER ON
*  AN "AS-IS" BASIS ONLY. AIROHA EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
*  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
*  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
*  NEITHER DOES AIROHA PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
*  SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
*  SUPPLIED WITH THE AIROHA SOFTWARE, AND BUYER AGREES TO LOOK ONLY TO SUCH
*  THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. AIROHA SHALL ALSO
*  NOT BE RESPONSIBLE FOR ANY AIROHA SOFTWARE RELEASES MADE TO BUYER'S
*  SPECIFICATION OR TO CONFORM TO A PARTICULAR STANDARD OR OPEN FORUM.
*
*  BUYER'S SOLE AND EXCLUSIVE REMEDY AND AIROHA'S ENTIRE AND CUMULATIVE
*  LIABILITY WITH RESPECT TO THE AIROHA SOFTWARE RELEASED HEREUNDER WILL BE,
*  AT AIROHA'S OPTION, TO REVISE OR REPLACE THE AIROHA SOFTWARE AT ISSUE,
*  OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY BUYER TO
*  AIROHA FOR SUCH AIROHA SOFTWARE AT ISSUE.
*
*  THE TRANSACTION CONTEMPLATED HEREUNDER SHALL BE CONSTRUED IN ACCORDANCE
*  WITH THE LAWS OF THE STATE OF CALIFORNIA, USA, EXCLUDING ITS CONFLICT OF
*  LAWS PRINCIPLES.  ANY DISPUTES, CONTROVERSIES OR CLAIMS ARISING THEREOF AND
*  RELATED THERETO SHALL BE SETTLED BY ARBITRATION IN SAN FRANCISCO, CA, UNDER
*  THE RULES OF THE INTERNATIONAL CHAMBER OF COMMERCE (ICC).
*
*******************************************************************************/

#include <air_error.h>
#include <air_types.h>
#include <air_l2.h>
#include <air_port.h>
#include <air_sec.h>
#include <osal/osal.h>
#include <cmlib/cmlib_port.h>
#include <cmlib/cmlib_bitmap.h>
#include <parser/dsh_parser.h>
#include <parser/dsh_util.h>
#include <cmd/port_cmd.h>
#include <cmd/l2_cmd.h>

/* NAMING CONSTANT DECLARATIONS
 */
#define L2_CMD_INVALID_ENTRY        (0xFFFFFFFF)
#define L2_WDOG_KICK_NUM            (100)

/* MACRO FUNCTION DECLARATIONS
 */
#define L2_CMD_PRINT_UCAST_MAC_TBL_INDEX() do                                                           \
{                                                                                                       \
    osal_printf("\n");                                                                                  \
    osal_printf(" unit  mac                ivl  vid   fid    802.1x     age-time  forward     port\n"); \
    osal_printf("---------------------------------------------------------------------------------\n"); \
} while(0)

#define L2_CMD_PRINT_UCAST_ADDR(__unit__, __count__, __ptr_mt__) do                                 \
{                                                                                                   \
    AIR_ERROR_NO_T          rc = AIR_E_OK;                                                          \
    UI32_T                  i;                                                                      \
    AIR_SEC_8021X_MODE_T    base;                                                                   \
    UI32_T                  real_time;                                                              \
    if (AIR_E_OK != rc)                                                                             \
    {                                                                                               \
        osal_printf("error\n");                                                                     \
        break;                                                                                      \
    }                                                                                               \
    rc = air_sec_get8021xGlobalMode(unit, &base);                                                   \
    if (AIR_E_OK != rc)                                                                             \
    {                                                                                               \
        osal_printf("error\n");                                                                     \
        break;                                                                                      \
    }                                                                                               \
    for (i = 0; i < (__count__); i++)                                                               \
    {                                                                                               \
        osal_printf(" %-4d  ", (__unit__));                                                         \
        osal_printf("%02x-%02x-%02x-%02x-%02x-%02x  ",                                              \
                (__ptr_mt__)[i].mac[0], (__ptr_mt__)[i].mac[1], (__ptr_mt__)[i].mac[2],             \
                (__ptr_mt__)[i].mac[3], (__ptr_mt__)[i].mac[4], (__ptr_mt__)[i].mac[5]);            \
        if (((__ptr_mt__)[i].flags) & AIR_L2_MAC_ENTRY_FLAGS_IVL)                                   \
        {                                                                                           \
            osal_printf("%-3s  ", "IVL");                                                           \
            osal_printf("%-4d  ", (__ptr_mt__)[i].cvid);                                            \
            osal_printf("%-5s  ", "-----");                                                         \
        }                                                                                           \
        else                                                                                        \
        {                                                                                           \
            osal_printf("%-3s  ", "SVL");                                                           \
            osal_printf("%-4s  ", "----");                                                          \
            osal_printf("%-5d  ", (__ptr_mt__)[i].fid);                                             \
        }                                                                                           \
        if (AIR_SEC_8021X_MODE_MAC == base)                                                         \
        {                                                                                           \
            if (((__ptr_mt__)[i].flags) & AIR_L2_MAC_ENTRY_FLAGS_UNAUTH)                            \
            {                                                                                       \
                osal_printf("%-6s  ", "unauth");                                                    \
            }                                                                                       \
            else                                                                                    \
            {                                                                                       \
                osal_printf("%-6s  ", "auth");                                                      \
            }                                                                                       \
        }                                                                                           \
        else                                                                                        \
        {                                                                                           \
            osal_printf("------  ");                                                                \
        }                                                                                           \
        if (((__ptr_mt__)[i].flags) & AIR_L2_MAC_ENTRY_FLAGS_STATIC)                                \
        {                                                                                           \
            osal_printf("%11s  ", "static");                                                        \
        }                                                                                           \
        else                                                                                        \
        {                                                                                           \
            real_time = (__ptr_mt__)[i].timer;                                                      \
            osal_printf("%7d sec  ", (UI32_T)real_time);                                            \
        }                                                                                           \
        osal_printf("%-11s ", _air_mac_address_forward_control_string[(__ptr_mt__)[i].sa_fwd]);     \
        CMD_PRINT_PORTLIST((__ptr_mt__)[i].port_bitmap);                                            \
        osal_printf("\n");                                                                          \
    }                                                                                               \
}while(0)

/* DATA TYPE DECLARATIONS
*/

/* GLOBAL VARIABLE DECLARATIONS
*/

/* LOCAL SUBPROGRAM BODIES
*/
/* STATIC VARIABLE DECLARATIONS
 */
const static C8_T *_air_forward_type_string [] =
{
"Broadcast",
"Unknown Multicast",
"Unknown Unicast",
"Unknown IP Multicast"
};
const static C8_T *_air_forward_action_string [] =
{
"Drop",
"Flooding",
"Forward to Pbm"
};

const static C8_T *_air_mac_address_forward_control_string [] =
{
"Default",
"CPU include",
"CPU exclude",
"CPU only",
"Drop"
};

static void
_port_cmd_printPortList(
    const C8_T                 *ptr_str,
    const AIR_PORT_BITMAP_T    pbm)
{
    UI32_T cnt, port, first, span = 0;

    CMLIB_PORT_BITMAP_COUNT(pbm, cnt);
    osal_printf("%s", ptr_str);
    if (0 == cnt)
    {
        osal_printf("NULL\n");
    }
    else
    {
        first = 1;
        CMLIB_BITMAP_BIT_FOREACH(pbm, port, AIR_PORT_BITMAP_SIZE)
        {
            osal_printf("%s%d", first ? "" : ",", port);
            first = 0;

            for (span = 1; (++port < AIR_PORT_NUM) && CMLIB_BITMAP_BIT_CHK(pbm, port); span++);

            if (span > 1)
            {
                osal_printf("-%d", port - 1);
            }
        }
        osal_printf("\n");
    }
}

static AIR_ERROR_NO_T
_l2_cmd_addMacTable(
    const C8_T                  *tokens[],
    UI32_T                      token_idx)
{
    AIR_ERROR_NO_T      rc = AIR_E_OK;
    UI32_T              unit = 0;
    AIR_MAC_T           mac;
    AIR_PORT_BITMAP_T   bitmap = {0};
    UI32_T              vid = 0;
    UI32_T              fid = 0;
    UI32_T              timer = AIR_L2_MAC_DEF_AGE_OUT_TIME;
    BOOL_T              is_ivl = FALSE;
    BOOL_T              is_static = FALSE;
    BOOL_T              is_unauth = FALSE;
    C8_T                str[DSH_CMD_MAX_LENGTH]={0};
    AIR_L2_FWD_CTRL_T   forward_control = AIR_L2_FWD_CTRL_DEFAULT;
    AIR_MAC_ENTRY_T     mt;

    /* cmd: l2 add mac-table [ unit=<UINT> ] [ static ] [ unauth ] mac=<MACADDR>
     *                       portlist=<UINTLIST> { vid=<UINT> | fid=<UNIT> }
     *                       [ src-mac-forward={ default | cpu-exclude | cpu-include | cpu-only | drop } ]
     *                       [ timer=<UINT> ]
     */

    DSH_CHECK_GET_UNIT(tokens, token_idx, &unit, 2);
    if (AIR_E_OK == dsh_checkString(tokens[token_idx], "static"))
    {
        token_idx++;
        is_static = TRUE;
    }
    if (AIR_E_OK == dsh_checkString(tokens[token_idx], "unauth"))
    {
        token_idx++;
        is_unauth = TRUE;
    }
    DSH_CHECK_PARAM(dsh_getMacAddr(tokens, token_idx, "mac", &mac), token_idx, 2);
    DSH_CHECK_PARAM(dsh_getPortBitmap(tokens, token_idx, "portlist", unit, &bitmap), token_idx, 2);
    if (AIR_E_OK == dsh_checkString(tokens[token_idx], "vid"))
    {
        DSH_CHECK_PARAM(dsh_getUint(tokens, token_idx, "vid", &vid), token_idx, 2);
        is_ivl = TRUE;
    }
    else if (AIR_E_OK == dsh_checkString(tokens[token_idx], "fid"))
    {
        DSH_CHECK_PARAM(dsh_getUint(tokens, token_idx, "fid", &fid), token_idx, 2);
        is_ivl = FALSE;
    }
    else
    {
        return DSH_E_SYNTAX_ERR;
    }

    DSH_CHECK_OPT(dsh_getString(tokens, token_idx,  "src-mac-forward", str), token_idx, 2);
    if (AIR_E_OK == dsh_checkString(str, "default"))
    {
        forward_control = AIR_L2_FWD_CTRL_DEFAULT;
    }
    else if (AIR_E_OK == dsh_checkString(str, "cpu-exclude"))
    {
        forward_control = AIR_L2_FWD_CTRL_CPU_EXCLUDE;
    }
    else if (AIR_E_OK == dsh_checkString(str, "cpu-include"))
    {
        forward_control = AIR_L2_FWD_CTRL_CPU_INCLUDE;
    }
    else if (AIR_E_OK == dsh_checkString(str, "cpu-only"))
    {
        forward_control = AIR_L2_FWD_CTRL_CPU_ONLY;
    }
    else if (AIR_E_OK == dsh_checkString(str, "drop"))
    {
        forward_control = AIR_L2_FWD_CTRL_DROP;
    }
    else
    {
        forward_control = AIR_L2_FWD_CTRL_DEFAULT;
    }

    if (AIR_E_OK == dsh_checkString(tokens[token_idx], "timer"))
    {
        DSH_CHECK_PARAM(dsh_getUint(tokens, token_idx, "timer", &timer), token_idx, 2);
    }
    DSH_CHECK_LAST_TOKEN(tokens[token_idx]);

    osal_memset(&mt, 0, sizeof(AIR_MAC_ENTRY_T));

    osal_memcpy(mt.mac, mac, sizeof(AIR_MAC_T));
    mt.flags = 0;
    if (TRUE == is_ivl)
    {
        mt.cvid = (UI16_T)vid;
        mt.flags |= AIR_L2_MAC_ENTRY_FLAGS_IVL;
    }
    else
    {
        mt.fid = (UI16_T)fid;
    }

    if (TRUE == is_static)
    {
        mt.flags |= AIR_L2_MAC_ENTRY_FLAGS_STATIC;
    }

    if (TRUE == is_unauth)
    {
        mt.flags |= AIR_L2_MAC_ENTRY_FLAGS_UNAUTH;
    }

    AIR_PORT_BITMAP_COPY(mt.port_bitmap, bitmap);

    /* sa_fwd field settings */
    mt.sa_fwd = forward_control;
    mt.timer = timer;

    rc = air_l2_addMacAddr(unit, &mt);

    if (AIR_E_TABLE_FULL == rc)
    {
        osal_printf("***Error***, hash bucket is full\n");
    }
    else if (AIR_E_OK != rc)
    {
        osal_printf("***Error***, add address fail\n");
    }

    return (rc);
}

static AIR_ERROR_NO_T
_l2_cmd_delMacTable(
    const C8_T                  *tokens[],
    UI32_T                      token_idx)
{
    AIR_ERROR_NO_T      rc = AIR_E_OK;
    UI32_T              unit = 0;
    AIR_MAC_T           mac;
    UI32_T              vid = 0;
    UI32_T              fid = 0;
    BOOL_T              is_ivl = FALSE;
    AIR_MAC_ENTRY_T     mt;

    /* cmd: l2 del mac-table [ unit=<UINT> ] mac=<MACADDR> { vid=<UINT> | fid=<UNIT> }
     */

    DSH_CHECK_GET_UNIT(tokens, token_idx, &unit, 2);
    DSH_CHECK_PARAM(dsh_getMacAddr(tokens, token_idx, "mac", &mac), token_idx, 2);
    if (AIR_E_OK == dsh_checkString(tokens[token_idx], "vid"))
    {
        DSH_CHECK_PARAM(dsh_getUint(tokens, token_idx, "vid", &vid), token_idx, 2);
        is_ivl = TRUE;
    }
    else if (AIR_E_OK == dsh_checkString(tokens[token_idx], "fid"))
    {
        DSH_CHECK_PARAM(dsh_getUint(tokens, token_idx, "fid", &fid), token_idx, 2);
        is_ivl = FALSE;
    }
    else
    {
        return DSH_E_SYNTAX_ERR;
    }
    DSH_CHECK_LAST_TOKEN(tokens[token_idx]);

    osal_memset(&mt, 0, sizeof(AIR_MAC_ENTRY_T));

    osal_memcpy(mt.mac, mac, sizeof(AIR_MAC_T));
    mt.flags = 0;
    if (TRUE == is_ivl)
    {
        mt.cvid = (UI16_T)vid;
        mt.flags |= AIR_L2_MAC_ENTRY_FLAGS_IVL;
    }
    else
    {
        mt.fid = (UI16_T)fid;
    }

    rc = air_l2_delMacAddr(unit, &mt);
    if (AIR_E_OK != rc)
    {
        osal_printf("***Error***, delete mac address fail\n");
    }

    return (rc);
}

static AIR_ERROR_NO_T
_l2_cmd_clrMacTable(
    const C8_T                  *tokens[],
    UI32_T                      token_idx)
{
    AIR_ERROR_NO_T      rc = AIR_E_OK;
    UI32_T              unit = 0;

     /* cmd: l2 clear mac-table [ unit=<UINT> ]
     */

    DSH_CHECK_GET_UNIT(tokens, token_idx, &unit, 2);
    DSH_CHECK_LAST_TOKEN(tokens[token_idx]);

    rc = air_l2_clearMacAddr(unit);

    if (AIR_E_OK != rc)
    {
        osal_printf("***Error***, clear L2 MAC table fail\n");
    }

    return (rc);
}

static AIR_ERROR_NO_T
_l2_cmd_showMacTable(
    const C8_T                  *tokens[],
    UI32_T                      token_idx)
{
    AIR_ERROR_NO_T      rc = AIR_E_OK;
    UI32_T              unit = 0;
    AIR_MAC_T           mac;
    UI32_T              vid = 0;
    UI32_T              fid = 0;
    BOOL_T              is_ivl = FALSE;
    AIR_MAC_ENTRY_T     *ptr_mt;
    UI8_T               count;

    /* cmd: l2 show mac-table [ unit=<UINT> ] mac=<MACADDR> { vid=<UINT> | fid=<UNIT> }
     */

    DSH_CHECK_GET_UNIT(tokens, token_idx, &unit, 2);
    DSH_CHECK_PARAM(dsh_getMacAddr(tokens, token_idx, "mac", &mac), token_idx, 2);
    if (AIR_E_OK == dsh_checkString(tokens[token_idx], "vid"))
    {
        DSH_CHECK_PARAM(dsh_getUint(tokens, token_idx, "vid", &vid), token_idx, 2);
        is_ivl = TRUE;
    }
    else if (AIR_E_OK == dsh_checkString(tokens[token_idx], "fid"))
    {
        DSH_CHECK_PARAM(dsh_getUint(tokens, token_idx, "fid", &fid), token_idx, 2);
        is_ivl = FALSE;
    }
    else
    {
        return DSH_E_SYNTAX_ERR;
    }
    DSH_CHECK_LAST_TOKEN(tokens[token_idx]);

    ptr_mt = osal_alloc(sizeof(AIR_MAC_ENTRY_T), "cmd");
    if (NULL == ptr_mt)
    {
        osal_printf("***Error***, allocate memory fail\n");
        return AIR_E_NO_MEMORY;
    }
    osal_memset(ptr_mt, 0, sizeof(AIR_MAC_ENTRY_T));

    osal_memcpy(ptr_mt ->mac, mac, sizeof(AIR_MAC_T));
    if (TRUE == is_ivl)
    {
        ptr_mt ->cvid = (UI16_T)vid;
        ptr_mt ->flags |= AIR_L2_MAC_ENTRY_FLAGS_IVL;
    }
    else
    {
        ptr_mt ->fid = (UI16_T)fid;
    }

    rc = air_l2_getMacAddr(unit, &count, ptr_mt);

    if (AIR_E_ENTRY_NOT_FOUND == rc)
    {
        osal_printf("Target MAC entry does not exist\n");
        osal_free(ptr_mt);
        return (rc);
    }
    else if (AIR_E_OK != rc)
    {
        osal_printf("***Error***, show mac table fail\n");
        osal_free(ptr_mt);
        return (rc);
    }

    /* Print MAC entry */
    L2_CMD_PRINT_UCAST_MAC_TBL_INDEX();
    L2_CMD_PRINT_UCAST_ADDR(unit, 1, ptr_mt);

    osal_free(ptr_mt);
    return (rc);
}

static AIR_ERROR_NO_T
_l2_cmd_dumpMacTable(
    const C8_T                  *tokens[],
    UI32_T                      token_idx)
{
    AIR_ERROR_NO_T      rc = AIR_E_OK;
    UI32_T              unit = 0;
    AIR_MAC_ENTRY_T     *ptr_mt;
    UI8_T               count;
    UI32_T              bucket_size;
    UI32_T              total_count;
    UI32_T              wdog_count;

    /* cmd: l2 dump mac-table [ unit=<UINT> ]
     */

    DSH_CHECK_GET_UNIT(tokens, token_idx, &unit, 2);
    DSH_CHECK_LAST_TOKEN(tokens[token_idx]);

    rc = air_l2_getMacBucketSize(unit, &bucket_size);
    if (AIR_E_OK != rc)
    {
        osal_printf("***Error***, get max. set number fail\n");
        return (rc);
    }

    ptr_mt = osal_alloc(sizeof(AIR_MAC_ENTRY_T) * bucket_size, "cmd");
    if (NULL == ptr_mt)
    {
        osal_printf("***Error***, allocate memory fail\n");
        return AIR_E_NO_MEMORY;
    }
    osal_memset(ptr_mt, 0, sizeof(AIR_MAC_ENTRY_T) * bucket_size);

    /* Get 1st MAC entry */
    total_count = 0;
    wdog_count = 0;
    rc = air_l2_getMacAddr(unit, &count, ptr_mt);
    if (AIR_E_ENTRY_NOT_FOUND == rc)
    {
        osal_printf("MAC table is empty\n");
        osal_free(ptr_mt);
        return (rc);
    }
    else if (AIR_E_OK != rc)
    {
        osal_printf("***Error***, dump mac table fail\n");
        osal_free(ptr_mt);
        return (rc);
    }
    total_count += count;
    wdog_count += count;

    /* Print 1st MAC entry */
    L2_CMD_PRINT_UCAST_MAC_TBL_INDEX();
    L2_CMD_PRINT_UCAST_ADDR(unit, count, ptr_mt);

    while(1)
    {
        /* Get the other MAC entries */
        osal_memset(ptr_mt, 0, sizeof(AIR_MAC_ENTRY_T) * bucket_size);
        rc = air_l2_getNextMacAddr(unit, &count, ptr_mt);
        if (AIR_E_ENTRY_NOT_FOUND == rc)
        {
            break;
        }
        else if (AIR_E_OK != rc)
        {
            osal_printf("***Error***, dump mac table fail\n");
            break;
        }
        total_count += count;
        wdog_count += count;

        /* Print the other MAC entries */
        L2_CMD_PRINT_UCAST_ADDR(unit, count, ptr_mt);
        if (wdog_count >= L2_WDOG_KICK_NUM)
        {
            osal_wdog_kick();
            wdog_count -= L2_WDOG_KICK_NUM;
        }
    }

    osal_printf("\nFound %d MAC %s.\n", total_count, (total_count>1)?"entries":"entry");
    osal_free(ptr_mt);
    return (rc);
}

static AIR_ERROR_NO_T
_l2_cmd_flushMacTable(
    const C8_T                  *tokens[],
    UI32_T                      token_idx)
{
    AIR_ERROR_NO_T          rc = AIR_E_OK;
    C8_T                    str[DSH_CMD_MAX_LENGTH]={0};
    UI32_T                  unit = 0, value = 0;
    AIR_L2_MAC_FLUSH_TYPE_T type = AIR_L2_MAC_FLUSH_TYPE_LAST;

    /* cmd: l2 flush mac-table [ unit=<UINT> ] type={ vid | fid | port } value=<UINT>
     */

    DSH_CHECK_GET_UNIT(tokens, token_idx, &unit, 2);
    DSH_CHECK_PARAM(dsh_getString(tokens, token_idx, "type", str), token_idx, 2);
    if (AIR_E_OK == dsh_checkString(str, "vid"))
    {
        type = AIR_L2_MAC_FLUSH_TYPE_VID;
    }
    else if (AIR_E_OK == dsh_checkString(str, "fid"))
    {
        type = AIR_L2_MAC_FLUSH_TYPE_FID;
    }
    else if (AIR_E_OK == dsh_checkString(str, "port"))
    {
        type = AIR_L2_MAC_FLUSH_TYPE_PORT;
    }
    else
    {
        return AIR_E_BAD_PARAMETER;
    }
    DSH_CHECK_PARAM(dsh_getUint(tokens, token_idx, "value", &value), token_idx, 2);
    DSH_CHECK_LAST_TOKEN(tokens[token_idx]);

    rc = air_l2_flushMacAddr(unit, type, value);
    return (rc);
}

static AIR_ERROR_NO_T
_l2_cmd_setMacAddrAgeOut(
    const C8_T                  *tokens[],
    UI32_T                      token_idx)
{
    AIR_ERROR_NO_T      rc = AIR_E_OK;
    UI32_T              unit = 0;
    UI32_T              age_time;

    /* cmd: l2 set mac-age-time [ unit=<UINT> ] age-time=<UNIT>
     */

    DSH_CHECK_GET_UNIT(tokens, token_idx, &unit, 2);
    DSH_CHECK_PARAM(dsh_getUint(tokens, token_idx, "age-time", &age_time), token_idx, 2);
    DSH_CHECK_LAST_TOKEN(tokens[token_idx]);

    rc = air_l2_setMacAddrAgeOut(unit, age_time);

    if (AIR_E_OK != rc)
    {
        osal_printf("***Error***, set mac-age-time fail\n");
    }

    return (rc);
}

static void
_l2_cmd_showMacAddrAgeOutInfo(
    const UI32_T unit,
    const UI32_T age_time)
{
    osal_printf("\n");
    osal_printf("mac-age-time:\n");
    osal_printf(" - dev unit  = %d\n", unit);
    osal_printf(" - real time = %d sec.\n", age_time);
    osal_printf("\n");
}

static AIR_ERROR_NO_T
_l2_cmd_showMacAddrAgeOut(
    const C8_T                  *tokens[],
    UI32_T                      token_idx)
{
    AIR_ERROR_NO_T      rc = AIR_E_OK;
    UI32_T              unit = 0;
    UI32_T              age_time;

    /* cmd: l2 show mac-age-time [ unit=<UINT> ]
     */

    DSH_CHECK_GET_UNIT(tokens, token_idx, &unit, 2);
    DSH_CHECK_LAST_TOKEN(tokens[token_idx]);

    rc = air_l2_getMacAddrAgeOut(unit, &age_time);

    if (AIR_E_OK != rc)
    {
        osal_printf("***Error***, show mac-age-time fail\n");
    }
    else
    {
        _l2_cmd_showMacAddrAgeOutInfo(unit, age_time);
    }

    return (rc);
}

static AIR_ERROR_NO_T
_l2_cmd_setMacAddrAgeOutMode(
    const C8_T                  *tokens[],
    UI32_T                      token_idx)
{
    AIR_ERROR_NO_T      rc = AIR_E_OK;
    UI32_T              unit = 0;
    AIR_PORT_BITMAP_T   bitmap = {0};
    UI32_T              port;
    BOOL_T              mode;
    C8_T                str[DSH_CMD_MAX_LENGTH];

    /* cmd: l2 set mac-age-port [ unit=<UINT> ] portlist=<UINTLIST> mode={ disable | enable }
     */

    DSH_CHECK_GET_UNIT(tokens, token_idx, &unit, 2);
    DSH_CHECK_PARAM(dsh_getPortBitmap(tokens, token_idx, "portlist", unit, &bitmap), token_idx, 2);
    DSH_CHECK_PARAM(dsh_getString(tokens, token_idx, "mode", str), token_idx, 2);
    if (AIR_E_OK == dsh_checkString(str, "disable"))
    {
        mode = FALSE;
    }
    else if (AIR_E_OK == dsh_checkString(str, "enable"))
    {
        mode = TRUE;
    }
    else
    {
        return DSH_E_SYNTAX_ERR;
    }
    DSH_CHECK_LAST_TOKEN(tokens[token_idx]);

    AIR_PORT_FOREACH(bitmap, port)
    {
        rc = air_l2_setMacAddrAgeOutMode(unit, port, mode);
        if (AIR_E_OK != rc)
        {
            osal_printf("***Error***, set mac-age-port fail\n");
            break;
        }
    }

    return (rc);
}

static void
_l2_cmd_printMacAddrAgeOutModePortlist(
    const UI32_T                unit,
    const AIR_PORT_BITMAP_T    cmd_bitmap,
    const AIR_PORT_BITMAP_T    val_bitmap)
{
    UI32_T  port;
    osal_printf("\n");
    osal_printf("mac-age-time:\n");
    osal_printf(" - unit      = %d\n", unit);
    osal_printf(" Port  Mode\n");
    osal_printf("==============\n");
    AIR_PORT_FOREACH(cmd_bitmap, port)
    {
        osal_printf(" %-4d  ", port);
        if (AIR_PORT_CHK(val_bitmap, port))
        {
            osal_printf("%-7s  ", "enable");
        }
        else
        {
            osal_printf("%-7s  ", "disable");
        }
        osal_printf("\n");
    }
    osal_printf("\n");
}

static AIR_ERROR_NO_T
_l2_cmd_showMacAddrAgeOutMode(
    const C8_T                  *tokens[],
    UI32_T                      token_idx)
{
    AIR_ERROR_NO_T              rc = AIR_E_OK;
    UI32_T                      unit = 0;
    AIR_PORT_BITMAP_T           cmd_bitmap = {0};
    AIR_PORT_BITMAP_T           val_bitmap = {0};
    UI32_T                      port;
    BOOL_T                      mode;

    /* cmd: l2 show mac-age-port [ unit=<UINT> ] portlist=<UINTLIST>
     */

    DSH_CHECK_GET_UNIT(tokens, token_idx, &unit, 2);
    DSH_CHECK_PARAM(dsh_getPortBitmap(tokens, token_idx, "portlist", unit, &cmd_bitmap), token_idx, 2);
    DSH_CHECK_LAST_TOKEN(tokens[token_idx]);

    AIR_PORT_FOREACH(cmd_bitmap, port)
    {
        rc = air_l2_getMacAddrAgeOutMode(unit, port, &mode);
        if (AIR_E_OK != rc)
        {
            osal_printf("***Error***, show mac-age-port fail\n");
            return (rc);
        }
        if (TRUE == mode)
        {
            AIR_PORT_ADD(val_bitmap, port);
        }
    }
    _l2_cmd_printMacAddrAgeOutModePortlist(unit, cmd_bitmap, val_bitmap);

    return (rc);
}

static AIR_ERROR_NO_T
_l2_cmd_setForwardMode(
    const C8_T          *tokens[],
    UI32_T              token_idx)
{
    AIR_ERROR_NO_T rc = AIR_E_OK;
    UI32_T unit=0;
    C8_T str[DSH_CMD_MAX_LENGTH] = {0};
    AIR_FORWARD_TYPE_T forwardType;
    AIR_FORWARD_ACTION_T forwardAction;
    AIR_PORT_BITMAP_T pbm = {0};

    /* paser tokens */
    DSH_CHECK_GET_UNIT(tokens, token_idx, &unit, 2);
    DSH_CHECK_PARAM(dsh_getString(tokens, token_idx, "type", str), token_idx, 2);
    if (AIR_E_OK == dsh_checkString(str, "bcast"))
    {
        forwardType = AIR_FORWARD_TYPE_BCST;
    }
    else if (AIR_E_OK == dsh_checkString(str, "unknown-unicast"))
    {
        forwardType = AIR_FORWARD_TYPE_UCST;
    }
    else if (AIR_E_OK == dsh_checkString(str, "unknown-mcast"))
    {
        forwardType = AIR_FORWARD_TYPE_MCST;
    }
    else if (AIR_E_OK == dsh_checkString(str, "unknown-ipmcast"))
    {
        forwardType = AIR_FORWARD_TYPE_UIPMCST;
    }
    else
    {
        return DSH_E_SYNTAX_ERR;
    }
    DSH_CHECK_PARAM(dsh_getString(tokens, token_idx, "action", str), token_idx, 2);
    if (AIR_E_OK == dsh_checkString(str, "drop"))
    {
        forwardAction = AIR_FORWARD_ACTION_DROP;
    }
    else if (AIR_E_OK == dsh_checkString(str, "flooding"))
    {
        forwardAction = AIR_FORWARD_ACTION_FLOODING;
    }
    else if (AIR_E_OK == dsh_checkString(str, "forward-pbm"))
    {
        forwardAction = AIR_FORWARD_ACTION_TO_PBM;
        /* if action = forward-pbm, paser the portlist */
        DSH_CHECK_OPT(dsh_getPortBitmap(tokens, token_idx, "portlist", unit, &pbm), token_idx, 2);
    }
    else
    {
        return DSH_E_SYNTAX_ERR;
    }
    DSH_CHECK_LAST_TOKEN(tokens[token_idx]);

    rc = air_l2_setForwardMode(unit, forwardType, forwardAction, pbm);

    if (AIR_E_OK != rc)
    {
        osal_printf("***Error***, set forward mode error\n");
        return rc;
    }

    return rc;
}

static AIR_ERROR_NO_T
_l2_cmd_getForwardMode(
    const C8_T          *tokens[],
    UI32_T              token_idx)
{
    AIR_ERROR_NO_T rc = AIR_E_OK;
    UI32_T unit=0;
    C8_T str[DSH_CMD_MAX_LENGTH] = {0};
    AIR_FORWARD_TYPE_T forwardType;
    AIR_FORWARD_ACTION_T forwardAction;
    AIR_PORT_BITMAP_T pbm = {0};

    /* paser tokens */
    DSH_CHECK_GET_UNIT(tokens, token_idx, &unit, 2);
    DSH_CHECK_PARAM(dsh_getString(tokens, token_idx, "type", str), token_idx, 2);
    if (AIR_E_OK == dsh_checkString(str, "bcast"))
    {
        forwardType = AIR_FORWARD_TYPE_BCST;
    }
    else if (AIR_E_OK == dsh_checkString(str, "unknown-unicast"))
    {
        forwardType = AIR_FORWARD_TYPE_UCST;
    }
    else if (AIR_E_OK == dsh_checkString(str, "unknown-mcast"))
    {
        forwardType = AIR_FORWARD_TYPE_MCST;
    }
    else if (AIR_E_OK == dsh_checkString(str, "unknown-ipmcast"))
    {
        forwardType = AIR_FORWARD_TYPE_UIPMCST;
    }
    else
    {
        return DSH_E_SYNTAX_ERR;
    }
    DSH_CHECK_LAST_TOKEN(tokens[token_idx]);


    rc = air_l2_getForwardMode(unit, forwardType, &forwardAction, pbm);
    if (AIR_E_OK != rc)
    {
        osal_printf("***Error***, get forward mode error\n");
        return rc;
    }

    osal_printf("\n");
    osal_printf("Forward Mode Statue\n");
    osal_printf("Unit     : %d\n", unit);
    osal_printf("Type     : %s\n", _air_forward_type_string[forwardType]);
    osal_printf("Action   : %s\n", _air_forward_action_string[forwardAction]);
    if ( AIR_FORWARD_ACTION_TO_PBM == forwardAction)
    {
        _port_cmd_printPortList("Current forward portlist = ", pbm);
    }
    osal_printf("=================\n");

    return rc;
}

/* -------------------------------------------------------------- callback */
const static DSH_VEC_T  _l2_cmd_vec[] =
{
    {
        "add mac-table", 2, _l2_cmd_addMacTable,
        "l2 add mac-table [ unit=<UINT> ] [ static ] [ unauth ] mac=<MACADDR>\n"
        "                 portlist=<UINTLIST> { vid=<UINT> | fid=<UINT> }\n"
        "                 [ src-mac-forward={ default | cpu-exclude | cpu-include | cpu-only | drop } ]\n"
        "                 [ timer=<UINT> ]\n"
    },
    {
        "del mac-table", 2, _l2_cmd_delMacTable,
        "l2 del mac-table [ unit=<UINT> ] mac=<MACADDR> { vid=<UINT> | fid=<UINT> }\n"
    },
    {
        "clear mac-table", 2, _l2_cmd_clrMacTable,
        "l2 clear mac-table [ unit=<UINT> ]\n"
    },
    {
        "show mac-table", 2, _l2_cmd_showMacTable,
        "l2 show mac-table [ unit=<UINT> ] mac=<MACADDR> { vid=<UINT> | fid=<UINT> }\n"
    },
    {
        "dump mac-table", 2, _l2_cmd_dumpMacTable,
        "l2 dump mac-table [ unit=<UINT> ]\n"
    },
    {
        "flush mac-table", 2, _l2_cmd_flushMacTable,
        "l2 flush mac-table [ unit=<UINT> ] type={ vid | fid | port } value=<UINT>\n"
    },
    {
        "set mac-age-time", 2, _l2_cmd_setMacAddrAgeOut,
        "l2 set mac-age-time [ unit=<UINT> ] age-time=<UINT>\n"
    },
    {
        "show mac-age-time", 2, _l2_cmd_showMacAddrAgeOut,
        "l2 show mac-age-time [ unit=<UINT> ]\n"
    },
    {
        "set mac-age-port", 2, _l2_cmd_setMacAddrAgeOutMode,
        "l2 set mac-age-port [ unit=<UINT> ] portlist=<UINTLIST> mode={ enable | disable }\n"
    },
    {
        "show mac-age-port", 2, _l2_cmd_showMacAddrAgeOutMode,
        "l2 show mac-age-port [ unit=<UINT> ] portlist=<UINTLIST>\n"
    },
    {
        "set fwd-mode", 2, _l2_cmd_setForwardMode,
        "l2 set fwd-mode [ unit=<UINT> ] type={ bcast | unknown-unicast | unknown-mcast | unknown-ipmcast } "
        "action={ drop | flooding | forward-pbm portlist=<UINTLIST> }\n"
        "Note: Portlist can be set only action is forward-pbm \n"
    },
    {
        "show fwd-mode", 2, _l2_cmd_getForwardMode,
        "l2 show fwd-mode [ unit=<UINT> ] type={ bcast | unknown-unicast | unknown-mcast | unknown-ipmcast }\n"
    },
};

AIR_ERROR_NO_T
l2_cmd_dispatcher(
    const C8_T                  *tokens[],
    UI32_T                      token_idx)
{
    return (dsh_dispatcher(tokens, token_idx, _l2_cmd_vec, sizeof(_l2_cmd_vec)/sizeof(DSH_VEC_T)));
}

AIR_ERROR_NO_T
l2_cmd_usager()
{
    return (dsh_usager(_l2_cmd_vec, sizeof(_l2_cmd_vec)/sizeof(DSH_VEC_T)));
}

