/*******************************************************************************
*  Copyright Statement:
*  --------------------
*  This software is protected by Copyright and the information contained
*  herein is confidential. The software may not be copied and the information
*  contained herein may not be used or disclosed except with the written
*  permission of Airoha Technology Corp. (C) 2021
*
*  BY OPENING THIS FILE, BUYER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
*  THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("AIROHA SOFTWARE")
*  RECEIVED FROM AIROHA AND/OR ITS REPRESENTATIVES ARE PROVIDED TO BUYER ON
*  AN "AS-IS" BASIS ONLY. AIROHA EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
*  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
*  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
*  NEITHER DOES AIROHA PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
*  SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
*  SUPPLIED WITH THE AIROHA SOFTWARE, AND BUYER AGREES TO LOOK ONLY TO SUCH
*  THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. AIROHA SHALL ALSO
*  NOT BE RESPONSIBLE FOR ANY AIROHA SOFTWARE RELEASES MADE TO BUYER'S
*  SPECIFICATION OR TO CONFORM TO A PARTICULAR STANDARD OR OPEN FORUM.
*
*  BUYER'S SOLE AND EXCLUSIVE REMEDY AND AIROHA'S ENTIRE AND CUMULATIVE
*  LIABILITY WITH RESPECT TO THE AIROHA SOFTWARE RELEASED HEREUNDER WILL BE,
*  AT AIROHA'S OPTION, TO REVISE OR REPLACE THE AIROHA SOFTWARE AT ISSUE,
*  OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY BUYER TO
*  AIROHA FOR SUCH AIROHA SOFTWARE AT ISSUE.
*
*  THE TRANSACTION CONTEMPLATED HEREUNDER SHALL BE CONSTRUED IN ACCORDANCE
*  WITH THE LAWS OF THE STATE OF CALIFORNIA, USA, EXCLUDING ITS CONFLICT OF
*  LAWS PRINCIPLES.  ANY DISPUTES, CONTROVERSIES OR CLAIMS ARISING THEREOF AND
*  RELATED THERETO SHALL BE SETTLED BY ARBITRATION IN SAN FRANCISCO, CA, UNDER
*  THE RULES OF THE INTERNATIONAL CHAMBER OF COMMERCE (ICC).
*
*******************************************************************************/

#include <air_port.h>
#include <air_svlan.h>
#include <air_vlan.h>
#include <osal/osal_lib.h>
#include <parser/dsh_parser.h>
#include <parser/dsh_util.h>
#include <cmd/svlan_cmd.h>

/* MACRO FUNCTION DECLARATIONS
 */
#define AIR_CHECK_RANGE(__value__, __min__, __max__, __str__) do             \
    {                                                                        \
        if ( ((__value__) > (__max__))  ||                                   \
             ((__value__) < (__min__)) )                                     \
        {                                                                     \
            osal_printf("***Error***, invalid %s=%u, range=%u-%u\n",         \
             __str__, __value__, (UI32_T)__min__, (UI32_T)__max__);        \
            return  AIR_E_BAD_PARAMETER;                                   \
        }                                                                   \
    } while (0)

AIR_ERROR_NO_T _svlan_cmd_setReceivedTPID(
        const C8_T  *tokens[],
        UI32_T      token_idx)
{
    AIR_ERROR_NO_T rc = AIR_E_OK;
    UI32_T unit = 0;
    UI32_T portid;
    UI32_T tpid;
    AIR_PORT_BITMAP_T pbm = {0};

    /* cmd: svlan set rcv-tpid [ unit=<UINT> ] portlist=<UINTLIST> tpid=<HEX>
     */

    /* parse and get */
    DSH_CHECK_GET_UNIT(tokens, token_idx, &unit, 2);
    DSH_CHECK_PARAM(dsh_getPortBitmap(tokens, token_idx, "portlist", unit, &pbm), token_idx, 2);
    DSH_CHECK_OPT(dsh_getHex(tokens, token_idx, "tpid", &tpid, sizeof(UI32_T)), token_idx, 2);
    DSH_CHECK_LAST_TOKEN(tokens[token_idx]);

    AIR_PORT_FOREACH(pbm, portid)
    {
        /* Call api */
        rc = air_svlan_setIngressTpid(unit, portid, tpid);
        if (AIR_E_OK != rc)
        {
            osal_printf("***Error***, set PortID %d Received TPID  %x fail\n", portid, tpid);
            break;
        }
    }

    return rc;
}

AIR_ERROR_NO_T _svlan_cmd_showReceivedTPID(
        const C8_T  *tokens[],
        UI32_T      token_idx)
{
    AIR_ERROR_NO_T rc = AIR_E_OK;
    UI32_T unit = 0;
    UI32_T portid;
    UI32_T tpid;
    AIR_PORT_BITMAP_T pbm = {0};

    /* cmd line: svlan show rcv-tpid [ unit=<UINT> ] port=<UINT>
     */

    /* parse and get */
    DSH_CHECK_GET_UNIT(tokens, token_idx, &unit, 2);
    DSH_CHECK_PARAM(dsh_getPortBitmap(tokens, token_idx, "portlist", unit, &pbm), token_idx, 2);
    DSH_CHECK_LAST_TOKEN(tokens[token_idx]);

    osal_printf("%10s %10s\n", "unit/port", "rcv-tpid");
    AIR_PORT_FOREACH(pbm, portid)
    {
        osal_printf("%5d/%2d", unit, portid);
        /* Call api */
        rc = air_svlan_getIngressTpid(unit, portid, &tpid);
        if (AIR_E_OK == rc)
        {
            osal_printf(" %12x\n", tpid);
        }
        else
        {
            osal_printf("***Error***, get PortID %d Received TPID fail\n", portid);
            break;
        }
    }

    return rc;
}

AIR_ERROR_NO_T _svlan_cmd_setInnerTPID(
        const C8_T  *tokens[],
        UI32_T      token_idx)
{
    AIR_ERROR_NO_T rc = AIR_E_OK;
    UI32_T unit = 0;
    UI32_T portid;
    UI32_T tpid;
    AIR_PORT_BITMAP_T pbm = {0};

    /* cmd line: svlan set inner-tpid [ unit=<UINT> ] portlist=<UINTLIST> tpid=<HEX>
     */

    /* parse and get */
    DSH_CHECK_GET_UNIT(tokens, token_idx, &unit, 2);
    DSH_CHECK_PARAM(dsh_getPortBitmap(tokens, token_idx, "portlist", unit, &pbm), token_idx, 2);
    DSH_CHECK_OPT(dsh_getHex(tokens, token_idx, "tpid", &tpid, sizeof(UI32_T)), token_idx, 2);
    DSH_CHECK_LAST_TOKEN(tokens[token_idx]);

    AIR_PORT_FOREACH(pbm, portid)
    {
        /* Call api */
        rc = air_svlan_setEgressInnerTpid(unit, portid, tpid);
        if (AIR_E_OK != rc)
        {
            osal_printf("***Error***, set PortID %d Innter TPID  %x fail\n",portid, tpid);
            break;
        }
    }

    return rc;
}

AIR_ERROR_NO_T _svlan_cmd_showInnerTPID(
        const C8_T  *tokens[],
        UI32_T      token_idx)
{
    AIR_ERROR_NO_T rc = AIR_E_OK;
    UI32_T unit = 0;
    UI32_T portid;
    UI32_T tpid;
    AIR_PORT_BITMAP_T pbm = {0};

    /* cmd line: svlan show inner-tpid [ unit=<UINT> ] port=<UINT>
     */

    /* parse and get */
    DSH_CHECK_GET_UNIT(tokens, token_idx, &unit, 2);
    DSH_CHECK_PARAM(dsh_getPortBitmap(tokens, token_idx, "portlist", unit, &pbm), token_idx, 2);
    DSH_CHECK_LAST_TOKEN(tokens[token_idx]);

    osal_printf("%10s %12s\n", "unit/port", "inner-tpid");
    AIR_PORT_FOREACH(pbm, portid)
    {
        osal_printf("%5d/%2d", unit, portid);
        /* Call api */
        rc = air_svlan_getEgressInnerTpid(unit, portid, &tpid);
        if (AIR_E_OK == rc)
        {
            osal_printf(" %14d\n", tpid);
        }
        else
        {
            osal_printf("***Error***, get PortID %d Inner TPID fail\n", portid);
            break;
        }
    }

    return rc;
}

AIR_ERROR_NO_T _svlan_cmd_setOuterTPID(
        const C8_T  *tokens[],
        UI32_T      token_idx)
{
    AIR_ERROR_NO_T rc = AIR_E_OK;
    UI32_T unit = 0;
    UI32_T portid;
    UI32_T tpid;
    AIR_PORT_BITMAP_T pbm = {0};

    /* cmd line: svlan set outer-tpid [ unit=<UINT> ] portlist=<UINTLIST> tpid=<HEX>
     */

    /* parse and get */
    DSH_CHECK_GET_UNIT(tokens, token_idx, &unit, 2);
    DSH_CHECK_PARAM(dsh_getPortBitmap(tokens, token_idx, "portlist", unit, &pbm), token_idx, 2);
    DSH_CHECK_OPT(dsh_getHex(tokens, token_idx, "tpid", &tpid, sizeof(UI32_T)), token_idx, 2);
    DSH_CHECK_LAST_TOKEN(tokens[token_idx]);

    AIR_PORT_FOREACH(pbm, portid)
    {
        /* Call api */
        rc = air_svlan_setEgressOuterTpid(unit, portid, tpid);
        if (AIR_E_OK != rc)
        {
            osal_printf("***Error***, set PortID %d Outer TPID  %x fail\n",portid, tpid);
            break;
        }
    }

    return rc;
}

AIR_ERROR_NO_T _svlan_cmd_showOuterTPID(
        const C8_T  *tokens[],
        UI32_T      token_idx)
{
    AIR_ERROR_NO_T rc = AIR_E_OK;
    UI32_T unit = 0;
    UI32_T portid;
    UI32_T tpid;
    AIR_PORT_BITMAP_T pbm = {0};

    /* parse and get */
    DSH_CHECK_GET_UNIT(tokens, token_idx, &unit, 2);
    DSH_CHECK_PARAM(dsh_getPortBitmap(tokens, token_idx, "portlist", unit, &pbm), token_idx, 2);
    DSH_CHECK_LAST_TOKEN(tokens[token_idx]);

    osal_printf("%10s %12s\n", "unit/port", "outer-tpid");
    AIR_PORT_FOREACH(pbm, portid)
    {
        osal_printf("%5d/%2d", unit, portid);
        /* Call api */
        rc = air_svlan_getEgressOuterTpid(unit, portid, &tpid);
        if (AIR_E_OK == rc)
        {
            osal_printf(" %14d\n", tpid);
        }
        else
        {
            osal_printf("***Error***, get PortID %d Outer TPID fail\n", portid);
            break;
        }
    }
    return rc;
}

AIR_ERROR_NO_T _svlan_cmd_setServicePort(
        const C8_T  *tokens[],
        UI32_T      token_idx)
{
    AIR_ERROR_NO_T rc = AIR_E_OK;
    UI32_T unit = 0;
    C8_T str[DSH_CMD_MAX_LENGTH] = {0};
    UI32_T portid;
    BOOL_T mode = 0;
    AIR_PORT_BITMAP_T pbm = {0};

    /* parse and get */
    DSH_CHECK_GET_UNIT(tokens, token_idx, &unit, 2);
    DSH_CHECK_PARAM(dsh_getPortBitmap(tokens, token_idx, "portlist", unit, &pbm), token_idx, 2);
    DSH_CHECK_PARAM(dsh_getString(tokens, token_idx, "mode", str), token_idx, 2);
    if (AIR_E_OK == dsh_checkString(str, "enable"))
    {
        mode = TRUE;
    }
    else if(AIR_E_OK == dsh_checkString(str, "disable"))
    {
        mode = FALSE;
    }
    DSH_CHECK_LAST_TOKEN(tokens[token_idx]);

    AIR_PORT_FOREACH(pbm, portid)
    {
        /* Call api */
        rc = air_svlan_setServicePort(unit, portid, mode);
        if (AIR_E_OK != rc)
        {
            osal_printf("***Error***, set svlan Service Port %d fail\n", portid);
            break;
        }
    }
    return rc;
}

AIR_ERROR_NO_T _svlan_cmd_showServicePort(
        const C8_T  *tokens[],
        UI32_T      token_idx)
{
    AIR_ERROR_NO_T rc = AIR_E_OK;
    UI32_T unit = 0;
    UI32_T portid;
    BOOL_T enable;
    AIR_PORT_BITMAP_T pbm = {0};

    /* cmd line: svlan show service-port [ unit=<UINT> ] port=<UINT>
     */

    /* parse and get */
    DSH_CHECK_GET_UNIT(tokens, token_idx, &unit, 2);
    DSH_CHECK_PARAM(dsh_getPortBitmap(tokens, token_idx, "portlist", unit, &pbm), token_idx, 2);
    DSH_CHECK_LAST_TOKEN(tokens[token_idx]);

    osal_printf("%10s %14s\n", "unit/port", "service-port");
    AIR_PORT_FOREACH(pbm, portid)
    {
        osal_printf("%5d/%2d", unit, portid);
        /* Call api */
        rc = air_svlan_getServicePort(unit, portid, &enable);

        if (AIR_E_OK == rc)
        {
            osal_printf(" %16s\n", enable ? "enable" : "disable");
        }
        else
        {
            osal_printf("***Error***, set svlan Service Port %d fail\n",portid);
            break;
        }
    }

    return rc;
}

AIR_ERROR_NO_T _svlan_cmd_setCustomPort(
        const C8_T  *tokens[],
        UI32_T      token_idx)
{
    AIR_ERROR_NO_T rc = AIR_E_OK;
    UI32_T unit = 0;
    C8_T str[DSH_CMD_MAX_LENGTH] = {0};
    UI32_T portid;
    BOOL_T mode = FALSE;
    AIR_PORT_BITMAP_T pbm = {0};

    /* cmd line: svlan set custom-port [ unit=<UINT> ] portlist=<UINTLIST> mode={ enable | disable }
     */

    /* parse and get */
    DSH_CHECK_GET_UNIT(tokens, token_idx, &unit, 2);
    DSH_CHECK_PARAM(dsh_getPortBitmap(tokens, token_idx, "portlist", unit, &pbm), token_idx, 2);
    DSH_CHECK_PARAM(dsh_getString(tokens, token_idx, "mode", str), token_idx, 2);
    if (AIR_E_OK == dsh_checkString(str, "enable"))
    {
        mode = TRUE;
    }
    else if(AIR_E_OK == dsh_checkString(str, "disable"))
    {
        mode = FALSE;
    }
    DSH_CHECK_LAST_TOKEN(tokens[token_idx]);

    AIR_PORT_FOREACH(pbm, portid)
    {
        /* Call api */
        rc = air_svlan_setCustomPort(unit, portid, mode);
        if (AIR_E_OK != rc)
        {
            osal_printf("***Error***, set svlan Custom Port %d fail\n",portid);
            break;
        }
    }
    return rc;
}

AIR_ERROR_NO_T _svlan_cmd_showCustomPort(
        const C8_T  *tokens[],
        UI32_T      token_idx)
{
    AIR_ERROR_NO_T rc = AIR_E_OK;
    UI32_T unit = 0;
    UI32_T portid;
    BOOL_T enable;
    AIR_PORT_BITMAP_T pbm = {0};

    /* cmd line: svlan show custom-port [ unit=<UINT> ] portlist=<UINTLIST>
     */

    /* parse and get */
    DSH_CHECK_GET_UNIT(tokens, token_idx, &unit, 2);
    DSH_CHECK_PARAM(dsh_getPortBitmap(tokens, token_idx, "portlist", unit, &pbm), token_idx, 2);
    DSH_CHECK_LAST_TOKEN(tokens[token_idx]);

    osal_printf("%10s %13s\n", "unit/port", "custom-port");
    AIR_PORT_FOREACH(pbm, portid)
    {
        osal_printf("%5d/%2d", unit, portid);
        /* Call api */
        rc = air_svlan_getCustomPort(unit, portid, &enable);

        if (AIR_E_OK == rc)
        {
            osal_printf(" %15s\n", enable ? "enable" : "disable");
        }
        else
        {
            osal_printf("***Error***, set svlan Custom Port %d fail\n",portid);
            break;
        }
    }

    return rc;
}

static void
_show_svlanEntry(
    const UI32_T unit,
    const UI32_T idx,
    const AIR_SVLAN_ENTRY_T *field)
{
    osal_printf("Unit : %u\n", unit);
    osal_printf("Svlan Idx : %u\n", idx);
    osal_printf("Svlan Field :\n");

    osal_printf("  -CVLAN ID : %u\n", field->c_vid);
    osal_printf("  -CVLAN priority : %u\n", field->c_pri);
    osal_printf("  -EtherType : 0x%x\n", field->eth_type);
    osal_printf("  -SVLAN ID : %u\n", field->s_vid);
    osal_printf("  -SVLAN priority : %u\n", field->s_pri);

    osal_printf("\n");
}

AIR_ERROR_NO_T _svlan_cmd_addSvlan(
        const C8_T    *tokens[],
        UI32_T        token_idx)
{
    AIR_ERROR_NO_T rc = AIR_E_OK;
    UI32_T unit = 0, svlanIdx;
    UI32_T cvid = 0, cvlanPri = 0, svid = 0, svlanPri = 0;
    UI32_T etherType = 0;
    AIR_SVLAN_ENTRY_T svlanEntry;

    osal_memset(&svlanEntry, 0, sizeof(AIR_SVLAN_ENTRY_T));
    /* parse and get */
    DSH_CHECK_GET_UNIT(tokens, token_idx, &unit, 2);
    DSH_CHECK_PARAM(dsh_getUint(tokens, token_idx, "idx", &svlanIdx), token_idx, 2);
    DSH_CHECK_PARAM(dsh_getUint(tokens, token_idx, "cvid", &cvid), token_idx, 2);
    svlanEntry.c_vid = cvid;
    svlanEntry.flags |= AIR_SVLAN_ENTRY_FLAGS_CHK_CVID ;
    /* Optinal field */
    DSH_CHECK_OPT(dsh_getUint(tokens, token_idx, "cpri", &cvlanPri), token_idx, 2);
    AIR_CHECK_RANGE(cvlanPri, 0, 7, "customPri");
    if(cvlanPri)
    {
        svlanEntry.c_pri = cvlanPri;
        svlanEntry.flags |= AIR_SVLAN_ENTRY_FLAGS_CHK_CPRI;
    }
    DSH_CHECK_OPT(dsh_getHex(tokens, token_idx, "ethertype", &etherType, sizeof(UI32_T)), token_idx, 2);
    if(etherType)
    {
        svlanEntry.eth_type= etherType;
        svlanEntry.flags |= AIR_SVLAN_ENTRY_FLAGS_CHK_ETH_TYPE;
    }
    DSH_CHECK_PARAM(dsh_getUint(tokens, token_idx, "svid", &svid), token_idx, 2);
    svlanEntry.s_vid = svid;
    DSH_CHECK_PARAM(dsh_getUint(tokens, token_idx, "spri", &svlanPri), token_idx, 2);
    AIR_CHECK_RANGE(svlanPri, 0, 7, "servicePri");
    svlanEntry.s_pri = svlanPri;
    DSH_CHECK_LAST_TOKEN(tokens[token_idx]);

    /* Call api */
    rc = air_svlan_addSvlan(unit, svlanIdx, &svlanEntry);
    if (AIR_E_ENTRY_EXISTS == rc)
    {
        osal_printf("***Error***, svlan idx %u already exists\n", svlanIdx);
    }
    else if (AIR_E_OK != rc)
    {
        osal_printf("***Error***, set svlan idx %u error\n", svlanIdx);
    }

    return rc;
}

AIR_ERROR_NO_T _svlan_cmd_showSvlan(
        const C8_T    *tokens[],
        UI32_T        token_idx)
{
    AIR_ERROR_NO_T rc = AIR_E_OK;
    UI32_T unit = 0, svlanIdx;
    AIR_SVLAN_ENTRY_T svlanEntry;

    osal_memset(&svlanEntry, 0, sizeof(AIR_SVLAN_ENTRY_T));
    /* parse and get */
    DSH_CHECK_GET_UNIT(tokens, token_idx, &unit, 2);
    DSH_CHECK_PARAM(dsh_getUint(tokens, token_idx, "idx", &svlanIdx), token_idx, 2);
    DSH_CHECK_LAST_TOKEN(tokens[token_idx]);

    /* Call api */
    rc = air_svlan_getSvlan(unit, svlanIdx, &svlanEntry);
    if (AIR_E_ENTRY_NOT_FOUND == rc)
    {
        osal_printf("***Error***, svlan idx %u not found\n", svlanIdx);
    }
    else if (AIR_E_OK != rc)
    {
        osal_printf("***Error***, get svlan idx %u error\n", svlanIdx);
    }
    else
    {
       _show_svlanEntry(unit, svlanIdx, &svlanEntry);
    }

    return rc;
}

AIR_ERROR_NO_T _svlan_cmd_delSvlan(
        const C8_T    *tokens[],
        UI32_T        token_idx)
{
    AIR_ERROR_NO_T rc = AIR_E_OK;
    UI32_T unit = 0, svlanIdx;

    /* parse and get */
    DSH_CHECK_GET_UNIT(tokens, token_idx, &unit, 2);
    DSH_CHECK_PARAM(dsh_getUint(tokens, token_idx, "idx", &svlanIdx), token_idx, 2);
    DSH_CHECK_LAST_TOKEN(tokens[token_idx]);

    /* Call api */
    rc = air_svlan_delSvlan(unit, svlanIdx);
    if (AIR_E_ENTRY_NOT_FOUND == rc)
    {
        osal_printf("***Error***, svlan idx %u not found\n", svlanIdx);
    }
    else if (AIR_E_OK != rc)
    {
        osal_printf("***Error***, del svlan idx %u error\n", svlanIdx);
    }

    return rc;
}
/* -------------------------------------------------------------- callback */
const static DSH_VEC_T  _svlan_cmd_vec[] =
{
    {
        "add svlan", 2, _svlan_cmd_addSvlan,
        "svlan add svlan [ unit=<UINT> ] idx=<UINT> cvid=<UINT> [ cpri=<UINT> ] "
        "[ ethertype=<HEX> ] svid=<UINT> spri=<UINT>\n"
    },
    {
        "show svlan", 2, _svlan_cmd_showSvlan,
        "svlan show svlan [ unit=<UINT> ] idx=<UINT> \n"
    },
    {
        "del svlan", 2, _svlan_cmd_delSvlan,
        "svlan del svlan [ unit=<UINT> ] idx=<UINT> \n"
    },
    {
        "set custom-port", 2, _svlan_cmd_setCustomPort,
        "svlan set custom-port [ unit=<UINT> ] portlist=<UINTLIST> mode={ enable | disable } \n"
    },
    {
        "show custom-port", 2, _svlan_cmd_showCustomPort,
        "svlan show custom-port [ unit=<UINT> ] portlist=<UINTLIST> \n"
    },
    {
        "set service-port", 2, _svlan_cmd_setServicePort,
        "svlan set service-port [ unit=<UINT> ] portlist=<UINTLIST> mode={ enable | disable } \n"
    },
    {
        "show service-port", 2, _svlan_cmd_showServicePort,
        "svlan show service-port [ unit=<UINT> ] portlist=<UINTLIST> \n"
    },
    {
        "set outer-tpid", 2, _svlan_cmd_setOuterTPID,
        "svlan set outer-tpid [ unit=<UINT> ] portlist=<UINTLIST> tpid=<HEX> \n"
    },
    {
        "show outer-tpid", 2, _svlan_cmd_showOuterTPID,
        "svlan show outer-tpid [ unit=<UINT> ] portlist=<UINTLIST> \n"
    },
    {
        "set inner-tpid", 2, _svlan_cmd_setInnerTPID,
        "svlan set inner-tpid [ unit=<UINT> ] portlist=<UINTLIST> tpid=<HEX> \n"
    },
    {
        "show inner-tpid", 2, _svlan_cmd_showInnerTPID,
        "svlan show inner-tpid [ unit=<UINT> ] portlist=<UINTLIST> \n"
    },
    {
        "set rcv-tpid", 2, _svlan_cmd_setReceivedTPID,
        "svlan set rcv-tpid [ unit=<UINT> ] portlist=<UINTLIST> tpid=<HEX> \n"
    },
    {
        "show rcv-tpid", 2, _svlan_cmd_showReceivedTPID,
        "svlan show rcv-tpid [ unit=<UINT> ] portlist=<UINTLIST> \n"
    },
};

AIR_ERROR_NO_T
svlan_cmd_dispatcher(
    const C8_T                  *tokens[],
    UI32_T                      token_idx)
{
    return (dsh_dispatcher(tokens, token_idx, _svlan_cmd_vec, sizeof(_svlan_cmd_vec)/sizeof(DSH_VEC_T)));
}

AIR_ERROR_NO_T
svlan_cmd_usager()
{
    return (dsh_usager(_svlan_cmd_vec, sizeof(_svlan_cmd_vec)/sizeof(DSH_VEC_T)));
}
