/*******************************************************************************
*  Copyright Statement:
*  --------------------
*  This software is protected by Copyright and the information contained
*  herein is confidential. The software may not be copied and the information
*  contained herein may not be used or disclosed except with the written
*  permission of Airoha Technology Corp. (C) 2021
*
*  BY OPENING THIS FILE, BUYER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
*  THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("AIROHA SOFTWARE")
*  RECEIVED FROM AIROHA AND/OR ITS REPRESENTATIVES ARE PROVIDED TO BUYER ON
*  AN "AS-IS" BASIS ONLY. AIROHA EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
*  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
*  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
*  NEITHER DOES AIROHA PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
*  SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
*  SUPPLIED WITH THE AIROHA SOFTWARE, AND BUYER AGREES TO LOOK ONLY TO SUCH
*  THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. AIROHA SHALL ALSO
*  NOT BE RESPONSIBLE FOR ANY AIROHA SOFTWARE RELEASES MADE TO BUYER'S
*  SPECIFICATION OR TO CONFORM TO A PARTICULAR STANDARD OR OPEN FORUM.
*
*  BUYER'S SOLE AND EXCLUSIVE REMEDY AND AIROHA'S ENTIRE AND CUMULATIVE
*  LIABILITY WITH RESPECT TO THE AIROHA SOFTWARE RELEASED HEREUNDER WILL BE,
*  AT AIROHA'S OPTION, TO REVISE OR REPLACE THE AIROHA SOFTWARE AT ISSUE,
*  OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY BUYER TO
*  AIROHA FOR SUCH AIROHA SOFTWARE AT ISSUE.
*
*  THE TRANSACTION CONTEMPLATED HEREUNDER SHALL BE CONSTRUED IN ACCORDANCE
*  WITH THE LAWS OF THE STATE OF CALIFORNIA, USA, EXCLUDING ITS CONFLICT OF
*  LAWS PRINCIPLES.  ANY DISPUTES, CONTROVERSIES OR CLAIMS ARISING THEREOF AND
*  RELATED THERETO SHALL BE SETTLED BY ARBITRATION IN SAN FRANCISCO, CA, UNDER
*  THE RULES OF THE INTERNATIONAL CHAMBER OF COMMERCE (ICC).
*
*******************************************************************************/
#include <air_error.h>
#include <air_types.h>
#include <air_qos.h>
#include <air_port.h>
#include <osal/osal.h>
#include <osal/osal_lib.h>
#include <cmlib/cmlib_bitmap.h>
#include <cmlib/cmlib_port.h>
#include <cmlib/cmlib_bit.h>
#include <parser/dsh_parser.h>
#include <parser/dsh_util.h>
#include <hal/common/hal.h>
#include <cmd/swc_cmd.h>

/* NAMING CONSTANT DECLARATIONS
 */

/* MACRO FUNCTION DECLARATIONS
 */

/* DATA TYPE DECLARATIONS
 */

/* STATIC VARIABLE DECLARATIONS
 */
const static C8_T *_swd_frame_type[] =
{
    "IGMP",
    "PPPoE",
    "ARP",
    "PAE",
    "DHCP",
    "BPDU",
    "TTL_0",
    "MLD",
    "REV_01",
    "REV_02",
    "REV_03",
    "REV_0E",
    "REV_10",
    "REV_20",
    "REV_21",
    "REV_UN"
};

const static C8_T *_swd_fwd_ctrl[AIR_SWC_MGMT_FRAME_TYPE_LAST] =
{
    "DEFAULT",
    "CPU_INCLUDE",
    "CPU_EXCLUDE",
    "CPU_ONLY",
    "DROP"
};

const static C8_T *_swd_enable[AIR_SWC_MGMT_FRAME_TYPE_LAST] =
{
    "DISABLE",
    "ENABLE",
};

const static C8_T *_swd_property[] =
{
    "mac-auto-flush",
    "l1-rate-ctrl",
    "acl-rate-ctrl-mgmt-frame",
    "storm-ctrl-mgmt-frame"
};

/* LOCAL SUBPROGRAM BODIES
 */
static AIR_ERROR_NO_T
_swc_cmd_setMgmtFrame(
    const C8_T          *tokens[],
    UI32_T              token_idx)
{
    AIR_ERROR_NO_T rc = AIR_E_OK;
    UI32_T unit = 0;
    C8_T mode_str[DSH_CMD_MAX_LENGTH] = {0};
    C8_T type_str[DSH_CMD_MAX_LENGTH] = {0};
    C8_T pri_str[DSH_CMD_MAX_LENGTH] = {0};
    C8_T pwd_str[DSH_CMD_MAX_LENGTH] = {0};
    UI32_T mng_type = 0;
    AIR_SWC_MGMT_FRAME_CFG_T frame_cfg;
    UI32_T star_idx = 0;

    /*
     * Command format
     * swc set mgmt-frame [ unit=<UNIT> ]
     * type={ igmp | pppoe | arp | pae | dhcp | ttl-0 | bpdu | mld |
     * rev-01 | rev-02 | rev-03 | rev-0e | rev-10 | rev-20 | rev-21 | rev-un }
     * {[state={ enable | disable }] [pri-high={ enable | disable }]\n"
     * [forward={ default | cpu-exclude | cpu-include | cpu-only | drop }]}\n"
     * Note: rev-xx = 01-80-C2-00-00-xx of destination mac, rev-un = others
     */

    /* paser tokens */
    DSH_CHECK_GET_UNIT(tokens, token_idx, &unit, 2);
    DSH_CHECK_PARAM(dsh_getString(tokens, token_idx, "type", type_str), token_idx, 2);
    star_idx = (token_idx);
    for( ; ; )
    {
        if(NULL == tokens[token_idx])
        {
            break;
        }
        if(AIR_E_OK == dsh_checkString(tokens[token_idx], "mode"))
        {
            DSH_CHECK_PARAM(dsh_getString(tokens, token_idx, "mode", mode_str), token_idx, 2);

        }
        else if(AIR_E_OK == dsh_checkString(tokens[token_idx], "pri-high"))
        {
            DSH_CHECK_PARAM(dsh_getString(tokens, token_idx, "pri-high", pri_str), token_idx, 2);
        }
        else if(AIR_E_OK == dsh_checkString(tokens[token_idx], "forward"))
        {
            DSH_CHECK_PARAM(dsh_getString(tokens, token_idx, "forward", pwd_str), token_idx, 2);
        }
        else
        {
            return AIR_E_BAD_PARAMETER;
        }
    }
    if(star_idx == token_idx)
    {
        osal_printf("***Error***, set no management frame config\n");
        return AIR_E_BAD_PARAMETER;
    }

    if(AIR_E_OK == dsh_checkString(type_str, "igmp"))
    {
        mng_type = AIR_SWC_MGMT_FRAME_TYPE_IGMP;
    }
    else if(AIR_E_OK == dsh_checkString(type_str, "pppoe"))
    {
        mng_type = AIR_SWC_MGMT_FRAME_TYPE_PPPOE;
    }
    else if(AIR_E_OK == dsh_checkString(type_str, "arp"))
    {
        mng_type = AIR_SWC_MGMT_FRAME_TYPE_ARP;
    }
    else if(AIR_E_OK == dsh_checkString(type_str, "pae"))
    {
        mng_type = AIR_SWC_MGMT_FRAME_TYPE_PAE;
    }
    else if(AIR_E_OK == dsh_checkString(type_str, "dhcp"))
    {
        mng_type = AIR_SWC_MGMT_FRAME_TYPE_DHCP;
    }
    else if(AIR_E_OK == dsh_checkString(type_str, "ttl-0"))
    {
        mng_type = AIR_SWC_MGMT_FRAME_TYPE_TTL_0;
    }
    else if(AIR_E_OK == dsh_checkString(type_str, "bpdu"))
    {
        mng_type = AIR_SWC_MGMT_FRAME_TYPE_BPDU;
    }
    else if(AIR_E_OK == dsh_checkString(type_str, "mld"))
    {
        mng_type = AIR_SWC_MGMT_FRAME_TYPE_MLD;
    }
    else if(AIR_E_OK == dsh_checkString(type_str, "rev-01"))
    {
        mng_type = AIR_SWC_MGMT_FRAME_TYPE_REV_01;
    }
    else if(AIR_E_OK == dsh_checkString(type_str, "rev-02"))
    {
        mng_type = AIR_SWC_MGMT_FRAME_TYPE_REV_02;
    }
    else if(AIR_E_OK == dsh_checkString(type_str, "rev-03"))
    {
        mng_type = AIR_SWC_MGMT_FRAME_TYPE_REV_03;
    }
    else if(AIR_E_OK == dsh_checkString(type_str, "rev-0e"))
    {
        mng_type = AIR_SWC_MGMT_FRAME_TYPE_REV_0E;
    }
    else if(AIR_E_OK == dsh_checkString(type_str, "rev-10"))
    {
        mng_type = AIR_SWC_MGMT_FRAME_TYPE_REV_10;
    }
    else if(AIR_E_OK == dsh_checkString(type_str, "rev-20"))
    {
        mng_type = AIR_SWC_MGMT_FRAME_TYPE_REV_20;
    }
    else if(AIR_E_OK == dsh_checkString(type_str, "rev-21"))
    {
        mng_type = AIR_SWC_MGMT_FRAME_TYPE_REV_21;
    }
    else if(AIR_E_OK == dsh_checkString(type_str, "rev-un"))
    {
        mng_type = AIR_SWC_MGMT_FRAME_TYPE_REV_UN;
    }
    else
    {
        return AIR_E_BAD_PARAMETER;
    }
    osal_memset(&frame_cfg, 0, sizeof(AIR_SWC_MGMT_FRAME_CFG_T));
    frame_cfg.frame_type = mng_type;
    air_swc_getMgmtFrameCfg(unit, &frame_cfg);

    if (AIR_E_OK == dsh_checkString(mode_str, "enable"))
    {
        frame_cfg.flags |= AIR_SWC_MGMT_FRAME_CFG_FLAGS_ENABLE;
    }
    else if (AIR_E_OK == dsh_checkString(mode_str, "disable"))
    {
        frame_cfg.flags &= ~AIR_SWC_MGMT_FRAME_CFG_FLAGS_ENABLE;
    }

    if (AIR_E_OK == dsh_checkString(pri_str, "enable"))
    {
        frame_cfg.flags |= AIR_SWC_MGMT_FRAME_CFG_FLAGS_PRI_HIGH;
    }
    else if (AIR_E_OK == dsh_checkString(pri_str, "disable"))
    {
        frame_cfg.flags &= ~AIR_SWC_MGMT_FRAME_CFG_FLAGS_PRI_HIGH;
    }

    if(AIR_E_OK == dsh_checkString(pwd_str, "default"))
    {
        frame_cfg.forward_mode = AIR_SWC_MGMT_FWD_MODE_SYS_SETTING;
    }
    else if(AIR_E_OK == dsh_checkString(pwd_str, "cpu-exclude"))
    {
        frame_cfg.forward_mode = AIR_SWC_MGMT_FWD_MODE_SYS_SETTING_EXCLUDE_CPU;
    }
    else if(AIR_E_OK == dsh_checkString(pwd_str, "cpu-include"))
    {
        frame_cfg.forward_mode = AIR_SWC_MGMT_FWD_MODE_SYS_SETTING_INCLUDE_CPU;
    }
    else if(AIR_E_OK == dsh_checkString(pwd_str, "cpu-only"))
    {
        frame_cfg.forward_mode = AIR_SWC_MGMT_FWD_MODE_CPU_ONLY;
    }
    else if(AIR_E_OK == dsh_checkString(pwd_str, "drop"))
    {
        frame_cfg.forward_mode = AIR_SWC_MGMT_FWD_MODE_DROP;
    }

    rc = air_swc_setMgmtFrameCfg(unit, &frame_cfg);
    if(AIR_E_OK != rc)
    {
        osal_printf("***Error***, set mng-frm-cfg error\n");
    }

    return rc;
}

static AIR_ERROR_NO_T
_swc_cmd_showMgmtFrame(
    const C8_T          *tokens[],
    UI32_T              token_idx)
{
    AIR_ERROR_NO_T rc = AIR_E_OK;
    UI32_T unit = 0, mng_type = 0;
    AIR_SWC_MGMT_FRAME_CFG_T frame_cfg;
    UI32_T enable = 0, pri_high = 0;

    /*
     * Command format
     * swc show mgmt-frame [ unit=<UNIT> ]
     */

    /* paser tokens */
    DSH_CHECK_GET_UNIT(tokens, token_idx, &unit, 2);
    DSH_CHECK_LAST_TOKEN(tokens[token_idx]);

    osal_printf("Unit %d\n", unit);
    osal_printf("Frame  Management  Pri-high  Forward Control\n");
    osal_printf("--------------------------------------------\n");

    osal_memset(&frame_cfg, 0, sizeof(AIR_SWC_MGMT_FRAME_CFG_T));
    for(mng_type = AIR_SWC_MGMT_FRAME_TYPE_IGMP; mng_type < AIR_SWC_MGMT_FRAME_TYPE_LAST; mng_type++)
    {
        frame_cfg.frame_type = mng_type;
        rc = air_swc_getMgmtFrameCfg(unit, &frame_cfg);
        if(AIR_E_OK == rc)
        {
            if (frame_cfg.flags & AIR_SWC_MGMT_FRAME_CFG_FLAGS_ENABLE)
            {
                enable = 1;
            }
            else
            {
                enable = 0;
            }

            if (frame_cfg.flags & AIR_SWC_MGMT_FRAME_CFG_FLAGS_PRI_HIGH)
            {
                pri_high = 1;
            }
            else
            {
                pri_high = 0;
            }
            osal_printf("%6s %10s %9s %12s\n", _swd_frame_type[mng_type], _swd_enable[enable], _swd_enable[pri_high], _swd_fwd_ctrl[frame_cfg.forward_mode]);
        }
        else
        {
            osal_printf("***Error***, show mng-frm-cfg error\n");
        }
    }
    osal_printf("\n");

    return rc;
}

static AIR_ERROR_NO_T
_swc_cmd_triggerCableTest(
    const C8_T          *tokens[],
    UI32_T              token_idx)
{
    AIR_ERROR_NO_T rc = AIR_E_OK;
    UI32_T unit = 0, port = 0, pair = 0;
    AIR_PORT_BITMAP_T pbm = {0};
    C8_T str[DSH_CMD_MAX_LENGTH] = {0};
    AIR_SWC_CABLE_TEST_RSLT_T cable;
    UI8_T i = 0;
    C8_T str_pair[4][7] =
    {
        "Pair A",
        "Pair B",
        "Pair C",
        "Pair D"
    };

    /*
     * Command format
     * swc trigger cable-test [ unit=<UINT> ] portlist=<UINTLIST> pair={ a | b | c | d | all }
     */

    /* paser tokens */
    DSH_CHECK_GET_UNIT(tokens, token_idx, &unit, 2);
    DSH_CHECK_PARAM(dsh_getPortBitmap(tokens, token_idx, "portlist", unit, &pbm), token_idx, 2);
    DSH_CHECK_PARAM(dsh_getString(tokens, token_idx, "pair", str), token_idx, 2);
    DSH_CHECK_LAST_TOKEN(tokens[token_idx]);

    osal_memset(&cable, 0, sizeof(AIR_SWC_CABLE_TEST_RSLT_T));
    if(AIR_E_OK == dsh_checkString(str, "a"))
    {
        pair = AIR_SWC_CABLE_TEST_PAIR_A;
    }
    else if(AIR_E_OK == dsh_checkString(str, "b"))
    {
        pair = AIR_SWC_CABLE_TEST_PAIR_B;
    }
    else if(AIR_E_OK == dsh_checkString(str, "c"))
    {
        pair = AIR_SWC_CABLE_TEST_PAIR_C;
    }
    else if(AIR_E_OK == dsh_checkString(str, "d"))
    {
        pair = AIR_SWC_CABLE_TEST_PAIR_D;
    }
    else if(AIR_E_OK == dsh_checkString(str, "all"))
    {
        pair = AIR_SWC_CABLE_TEST_PAIR_ALL;
    }
    else
    {
        return AIR_E_BAD_PARAMETER;
    }

    osal_printf("unit %u", unit);
    AIR_PORT_FOREACH(pbm, port)
    {
        osal_printf(" - Port = %u\n", port);
        rc = air_swc_triggerCableTest(unit, port, pair, &cable);
        if(AIR_E_OK == rc)
        {
            if (AIR_SWC_CABLE_TEST_PAIR_ALL == pair)
            {
                for(i = 0; i < AIR_SWC_CABLE_MAX_PAIR; i++)
                {
                    osal_printf(" - %s cable status = ", str_pair[i]);
                    if(AIR_SWC_CABLE_STATUS_OPEN == cable.status[i])
                    {
                        osal_printf("open\n");
                    }
                    else if(AIR_SWC_CABLE_STATUS_SHORT == cable.status[i])
                    {
                        osal_printf("short\n");
                    }
                    else if(AIR_SWC_CABLE_STATUS_NORMAL == cable.status[i])
                    {
                        osal_printf("normal\n");
                    }
                    osal_printf(" - %s length = %d.%d m\n", str_pair[i], (cable.length[i] / 10), (cable.length[i] % 10));
                }
            }
            else
            {
                osal_printf(" - %s cable status = ", str_pair[pair]);
                if(AIR_SWC_CABLE_STATUS_OPEN == cable.status[pair])
                {
                    osal_printf("open\n");
                }
                else if(AIR_SWC_CABLE_STATUS_SHORT == cable.status[pair])
                {
                    osal_printf("short\n");
                }
                else if(AIR_SWC_CABLE_STATUS_NORMAL == cable.status[pair])
                {
                    osal_printf("normal\n");
                }
                osal_printf(" - %s length = %d.%d m\n", str_pair[pair], (cable.length[pair] / 10), (cable.length[pair] % 10));
            }
        }
        else if(AIR_E_NOT_SUPPORT == rc)
        {
            osal_printf("***Error***, only support cable test for 1G speed\n");
        }
        else if(AIR_E_TIMEOUT == rc)
        {
            osal_printf("***Error***, trigger cable-test time out\n");
        }
        else
        {
            osal_printf("***Error***, trigger cable-test error\n");
        }
        osal_printf("\n\n");
    }

    return rc;
}

static AIR_ERROR_NO_T
_swc_cmd_setSysMac(
    const C8_T          *tokens[],
    UI32_T              token_idx)
{
    AIR_ERROR_NO_T rc = AIR_E_OK;
    UI32_T unit = 0;
    AIR_MAC_T mac;

    /*
     * Command format
     * swc set sys-mac [ unit=<UINT> ] mac=<MACADDR>
     */

    /* paser tokens */
    DSH_CHECK_GET_UNIT(tokens, token_idx, &unit, 2);
    DSH_CHECK_PARAM(dsh_getMacAddr(tokens, token_idx, "mac", &mac), token_idx, 2);
    DSH_CHECK_LAST_TOKEN(tokens[token_idx]);

    rc = air_swc_setSystemMac(unit, mac);
    if(AIR_E_OK != rc)
    {
        osal_printf("***Error***, set sys-mac error\n");
    }

    return rc;
}

static AIR_ERROR_NO_T
_swc_cmd_getSysMac(
    const C8_T          *tokens[],
    UI32_T              token_idx)
{
    AIR_ERROR_NO_T rc = AIR_E_OK;
    UI32_T unit = 0;
    AIR_MAC_T mac;

    /*
     * Command format
     * swc show sys-mac [ unit=<UINT> ]
     */

    /* paser tokens */
    DSH_CHECK_GET_UNIT(tokens, token_idx, &unit, 2);
    DSH_CHECK_LAST_TOKEN(tokens[token_idx]);

    rc = air_swc_getSystemMac(unit, mac);
    if(AIR_E_OK != rc)
    {
        osal_printf("***Error***, show sys-mac error\n");
    }
    else
    {
        osal_printf("System MAC address : %02X-%02X-%02X-%02X-%02X-%02X\n",
                mac[0], mac[1], mac[2], mac[3], mac[4], mac[5]);
    }

    return rc;
}

static AIR_ERROR_NO_T
_swc_cmd_setJumbo(
    const C8_T          *tokens[],
    UI32_T              token_idx)
{

    AIR_ERROR_NO_T rc = AIR_E_OK;
    UI32_T unit = 0;
    C8_T str[DSH_CMD_MAX_LENGTH] = {0};
    AIR_SWC_JUMBO_SIZE_T jlen = AIR_SWC_JUMBO_SIZE_LAST;

    UI32_T i = 0;
    C8_T str_jumbo[AIR_SWC_JUMBO_SIZE_LAST][6] = {
        "1518", "1536", "1552", "2048", "3072", "4096", "5120",
        "6144", "7168", "8192", "9216", "12288", "15360"};
    /*
     * Command format
     * swc set jumbo [ unit=<UINT> ]
     * jumbo-len={ 1518 | 1536 | 1552 | 2048 | 3072 | 4096 | 5120 | 6144 | 7168 | 8192 | 9216 | 12288 | 15360 }
     */

    /* paser tokens */
    DSH_CHECK_GET_UNIT(tokens, token_idx, &unit, 2);
    DSH_CHECK_PARAM(dsh_getString(tokens, token_idx, "jumbo-len", str), token_idx, 2);

    for(i=0; i<=AIR_SWC_JUMBO_SIZE_LAST; i++)
    {
        if(AIR_E_OK == dsh_checkString(str, str_jumbo[i])){
            jlen = i;
            break;
        }
    }
    if(AIR_SWC_JUMBO_SIZE_LAST  == i)
    {
        return AIR_E_BAD_PARAMETER;
    }

    DSH_CHECK_LAST_TOKEN(tokens[token_idx]);

    rc = air_swc_setJumboSize(unit, jlen);
    if(AIR_E_OK != rc)
    {
        osal_printf("***Error***, set jumbo error\n");
    }

    return rc;
}

static AIR_ERROR_NO_T
_swc_cmd_showJumbo(
    const C8_T          *tokens[],
    UI32_T              token_idx)
{
    AIR_ERROR_NO_T rc = AIR_E_OK;
    UI32_T unit = 0;
    AIR_SWC_JUMBO_SIZE_T jlen = AIR_SWC_JUMBO_SIZE_LAST;


    C8_T str_jumbo[AIR_SWC_JUMBO_SIZE_LAST][6] = {
        "1518", "1536", "1552", "2048", "3072", "4096", "5120",
        "6144", "7168", "8192", "9216", "12288", "15360"};
    /*
     * Command format
     * swc show jumbo [ unit=<UINT> ]
     */

    /* paser tokens */
    DSH_CHECK_GET_UNIT(tokens, token_idx, &unit, 2);
    DSH_CHECK_LAST_TOKEN(tokens[token_idx]);

    osal_printf("unit %u\n", unit);
    rc = air_swc_getJumboSize(unit, &jlen);
    if(AIR_E_OK == rc)
    {
        osal_printf(" - Jumbo len = %s\n", str_jumbo[jlen]);
    }
    else
    {
        osal_printf("***Error***, show jumbo error\n");
    }

    return rc;
}

static AIR_ERROR_NO_T
_swc_cmd_setProperty(
    const C8_T          *tokens[],
    UI32_T              token_idx)
{
    AIR_ERROR_NO_T rc = AIR_E_OK;
    UI32_T unit = 0;
    C8_T state_str[DSH_CMD_MAX_LENGTH] = {0};
    C8_T type_str[DSH_CMD_MAX_LENGTH] = {0};
    UI32_T enable = 0, param = 0;
    AIR_SWC_PROPERTY_T type = AIR_SWC_PROPERTY_LAST;

    /*
     * Command format
     * swc set property [ unit=<UNIT> ]
     * type={ mac-auto-flush | l1-rate-ctrl | acl-rate-ctrl-mgmt-frame | storm-ctrl-mgmt-frame } state={ enable | disable }\n"
     */

    /* paser tokens */
    DSH_CHECK_GET_UNIT(tokens, token_idx, &unit, 2);
    DSH_CHECK_PARAM(dsh_getString(tokens, token_idx, "type", type_str), token_idx, 2);
    DSH_CHECK_PARAM(dsh_getString(tokens, token_idx, "state", state_str), token_idx, 2);
    DSH_CHECK_LAST_TOKEN(tokens[token_idx]);

    if(AIR_E_OK == dsh_checkString(type_str, "mac-auto-flush"))
    {
        type = AIR_SWC_PROPERTY_ENABLE_MAC_AUTO_FLUSH;
    }
    else if(AIR_E_OK == dsh_checkString(type_str, "l1-rate-ctrl"))
    {
        type = AIR_SWC_PROPERTY_ENABLE_L1_RATE_CTRL;
    }
    else if(AIR_E_OK == dsh_checkString(type_str, "acl-rate-ctrl-mgmt-frame"))
    {
        type = AIR_SWC_PROPERTY_ACL_RATE_CTRL_MGMT_FRAME_INCLUDE;
    }
    else if(AIR_E_OK == dsh_checkString(type_str, "storm-ctrl-mgmt-frame"))
    {
        type = AIR_SWC_PROPERTY_STORM_CTRL_MGMT_FRAME_INCLUDE;
    }
    else
    {
        return AIR_E_BAD_PARAMETER;
    }

    if (AIR_E_OK == dsh_checkString(state_str, "enable"))
    {
        enable = 1;
    }
    else if (AIR_E_OK == dsh_checkString(state_str, "disable"))
    {
        enable = 0;
    }

    rc = air_swc_setProperty(unit, type, enable, param);
    if(AIR_E_OK != rc)
    {
        osal_printf("***Error***, set switch property error\n");
    }

    return rc;
}

static AIR_ERROR_NO_T
_swc_cmd_showProperty(
    const C8_T          *tokens[],
    UI32_T              token_idx)
{
    AIR_ERROR_NO_T rc = AIR_E_OK;
    UI32_T unit = 0;
    UI32_T enable = 0, param = 0;
    AIR_SWC_PROPERTY_T type = AIR_SWC_PROPERTY_LAST;

    /*
     * Command format
     * swc show property [ unit=<UNIT> ]
     */

    DSH_CHECK_GET_UNIT(tokens, token_idx, &unit, 2);
    DSH_CHECK_LAST_TOKEN(tokens[token_idx]);
    osal_printf("                Property      State \n");
    osal_printf("------------------------------------\n");

    for(type = AIR_SWC_PROPERTY_ENABLE_MAC_AUTO_FLUSH; type < AIR_SWC_PROPERTY_LAST; type++)
    {
        rc = air_swc_getProperty(unit, type, &enable, &param);
        if(AIR_E_OK == rc)
        {
            osal_printf("%24s %10s\n", _swd_property[type], _swd_enable[enable]);
        }
        else
        {
            osal_printf("***Error***, show switch property error\n");
        }
    }
    osal_printf("\n");

    return rc;
}

static AIR_ERROR_NO_T
_swc_cmd_setLoopDetect(
    const C8_T          *tokens[],
    UI32_T              token_idx)
{
    AIR_ERROR_NO_T rc = AIR_E_OK;
    UI32_T unit = 0, port = 0;
    AIR_PORT_BITMAP_T pbm = {0};
    C8_T st[DSH_CMD_MAX_LENGTH] = {0};
    AIR_SWC_LPDET_MODE_T mode = AIR_SWC_LPDET_MODE_DISABLE;

    /*
     * Command format
     * swc set lp-det [ unit=<UINT> ] portlist=<UINTLIST> mode={ enable | disable }
     */

    /* paser tokens */
    DSH_CHECK_GET_UNIT(tokens, token_idx, &unit, 2);
    DSH_CHECK_PARAM(dsh_getPortBitmap(tokens, token_idx, "portlist", unit, &pbm), token_idx, 2);
    DSH_CHECK_PARAM(dsh_getString(tokens, token_idx, "mode", st), token_idx, 2);
    DSH_CHECK_LAST_TOKEN(tokens[token_idx]);

    if (AIR_E_OK == dsh_checkString(st, "enable"))
    {
        mode = AIR_SWC_LPDET_MODE_ENABLE;
    }
    else if(AIR_E_OK != dsh_checkString(st, "disable"))
    {
        return AIR_E_BAD_PARAMETER;
    }

    AIR_PORT_FOREACH(pbm, port)
    {
        rc = air_swc_setLoopDetect(unit, port, mode);
        if (AIR_E_OK != rc)
        {
            osal_printf("***Error***, set port=%u loop detect error\n", port);
            break;
        }
    }

    return rc;
}

static AIR_ERROR_NO_T
_swc_cmd_showLoopDetect(
    const C8_T          *tokens[],
    UI32_T              token_idx)
{
    AIR_ERROR_NO_T rc = AIR_E_OK;
    UI32_T unit = 0, port = 0;
    AIR_PORT_BITMAP_T pbm = {0};
    AIR_SWC_LPDET_MODE_T state = AIR_SWC_LPDET_MODE_LAST;

    /*
     * Command format
     * swc show lp-det [ unit=<UINT> ] portlist=<UINTLIST>
     */

    /* paser tokens */
    DSH_CHECK_GET_UNIT(tokens, token_idx, &unit, 2);
    DSH_CHECK_PARAM(dsh_getPortBitmap(tokens, token_idx, "portlist", unit, &pbm), token_idx, 2);
    DSH_CHECK_LAST_TOKEN(tokens[token_idx]);

    osal_printf("unit %u\n", unit);
    AIR_PORT_FOREACH(pbm, port)
    {
        osal_printf(" - Port = %u\n", port);
        rc = air_swc_getLoopDetect(unit, port, &state);
        if (AIR_E_OK == rc)
        {
            osal_printf(" - Loop detect status = %s\n",
                    (AIR_SWC_LPDET_MODE_ENABLE == state)?"Enable":"Disable");
        }
        else
        {
            osal_printf("***Error***, show port=%u loop detect error\n", port);
            break;
        }
        osal_printf("\n");
    }

    return rc;
}

static AIR_ERROR_NO_T
_swc_cmd_setLoopDetectFrame(
    const C8_T          *tokens[],
    UI32_T              token_idx)
{
    AIR_ERROR_NO_T rc = AIR_E_OK;
    UI32_T unit = 0;
    AIR_MAC_T smac;
    UI32_T type = 0;
    BOOL_T change = FALSE;

    /*
     * Command format
     * swc set lp-det-frm [ unit=<UINT> ] [ smac=<MACADDR> ] [ type=<HEX> ]
     */

    osal_memset(smac, 0, sizeof(AIR_MAC_T));
    /* paser tokens */
    DSH_CHECK_GET_UNIT(tokens, token_idx, &unit, 2);
    if (AIR_E_OK == dsh_getMacAddr(tokens, token_idx, "smac", &smac))
    {
        change = TRUE;
        token_idx += 2;
    }
    if (AIR_E_OK == dsh_getHex(tokens, token_idx, "ethertype", &type, sizeof(UI32_T)))
    {
        change = TRUE;
        token_idx += 2;
    }
    DSH_CHECK_LAST_TOKEN(tokens[token_idx]);

    if (FALSE == change)
    {
        osal_printf("***Error***, no option to set loop detect\n");
        return AIR_E_BAD_PARAMETER;
    }

    rc = air_swc_setLoopDetectFrame(unit, type, smac);
    if (AIR_E_OK != rc)
    {
        osal_printf("***Error***, set loop detect error\n");
    }

    return rc;
}

static AIR_ERROR_NO_T
_swc_cmd_showLoopDetectFrame(
    const C8_T          *tokens[],
    UI32_T              token_idx)
{
    AIR_ERROR_NO_T rc = AIR_E_OK;
    UI32_T unit = 0;
    AIR_MAC_T smac;
    UI32_T type = 0;

    /*
     * Command format
     * swc show lp-det-frm [ unit=<UINT> ]
     */

    /* paser tokens */
    DSH_CHECK_GET_UNIT(tokens, token_idx, &unit, 2);
    DSH_CHECK_LAST_TOKEN(tokens[token_idx]);

    osal_memset(smac, 0, sizeof(AIR_MAC_T));
    osal_printf("unit %u\n", unit);
    rc = air_swc_getLoopDetectFrame(unit, &type, smac);
    if (AIR_E_OK == rc)
    {
        osal_printf(" - Loop frame ethertype = 0x%4X\n", type);
        osal_printf(" - Loop frame SMAC = %02X-%02X-%02X-%02X-%02X-%02X\n", smac[0], smac[1], smac[2], smac[3], smac[4], smac[5]);
    }
    else
    {
        osal_printf("***Error***, show loop detect error\n");
    }
    osal_printf("\n");

    return rc;
}

static AIR_ERROR_NO_T
_swc_cmd_showLoopDetectStatus(
    const C8_T          *tokens[],
    UI32_T              token_idx)
{
    AIR_ERROR_NO_T rc = AIR_E_OK;
    UI32_T unit = 0, port;
    BOOL_T all = TRUE;
    AIR_PORT_BITMAP_T port_bitmap = {0};
    AIR_PORT_BITMAP_T lp_status_bitmap = {0};

    /*
     * Command format
     * swc show lp-status [ unit=<UINT> ] [ portlist=<UINTLIST> ]
     */

    /* paser tokens */
    DSH_CHECK_GET_UNIT(tokens, token_idx, &unit, 2);
    if(AIR_E_OK == dsh_getPortBitmap(tokens, token_idx, "portlist", unit, &port_bitmap))
    {
        all = FALSE;
        token_idx += 2;
    }
    DSH_CHECK_LAST_TOKEN(tokens[token_idx]);

    if(TRUE == all)
    {
        CMLIB_BITMAP_SET(port_bitmap, HAL_PORT_BMP_ETH(unit), AIR_PORT_BITMAP_SIZE);
    }

    rc = air_swc_getLoopDetectStatus(unit, port_bitmap, lp_status_bitmap);
    if (AIR_E_OK == rc)
    {
        osal_printf("Current loop occurs status: \n");
        osal_printf("%10s %10s\n", "unit/port", "loop-status");

        AIR_PORT_FOREACH(port_bitmap, port)
        {
            osal_printf("%4d/%2d", unit, port);

            if (AIR_PORT_CHK(lp_status_bitmap, port))
            {
                osal_printf(" %14s", "occur");
            }
            else
            {
                osal_printf(" %14s", "not occur");
            }
            osal_printf("\n");
        }
    }
    else
    {
        osal_printf("***Error***, show loop status error\n");
    }

    return AIR_E_OK;
}

static AIR_ERROR_NO_T
_swc_cmd_clrLoopDetectStatus(
    const C8_T          *tokens[],
    UI32_T              token_idx)
{
    AIR_ERROR_NO_T rc = AIR_E_OK;
    AIR_PORT_BITMAP_T port_bitmap = {0};
    UI32_T unit = 0;
    BOOL_T all = TRUE;

    /*
     * Command format
     * swc clear lp-status [ unit=<UINT> ] [ portlist=<UINTLIST> ]
     */

    /* paser tokens */
    DSH_CHECK_GET_UNIT(tokens, token_idx, &unit, 2);
    if(AIR_E_OK == dsh_getPortBitmap(tokens, token_idx, "portlist", unit, &port_bitmap))
    {
        all = FALSE;
        token_idx += 2;
    }
    DSH_CHECK_LAST_TOKEN(tokens[token_idx]);

    if(TRUE == all)
    {
        CMLIB_BITMAP_SET(port_bitmap, HAL_PORT_BMP_ETH(unit), AIR_PORT_BITMAP_SIZE);
    }

    rc = air_swc_clearLoopDetectStatus(unit, port_bitmap);
    if (AIR_E_OK != rc)
    {
        osal_printf("***Error***, clear loop status error\n");
    }

    return rc;
}

const static DSH_VEC_T _swc_cmd_vec[] =
{
    {
        "set mgmt-frame", 2, _swc_cmd_setMgmtFrame,
        "swc set mgmt-frame [ unit=<UINT> ] \n"
        "type={ igmp | pppoe | arp | pae | dhcp | ttl-0 | bpdu | mld |\n"
        "rev-01 | rev-02 | rev-03 | rev-0e | rev-10 | rev-20 | rev-21 | rev-un }\n"
        "{ [ mode={ enable | disable } ] [ pri-high={ enable | disable } ]\n"
        "[ forward={ default | cpu-exclude | cpu-include | cpu-only | drop } ] }\n"
        "Note: rev-xx = 01-80-C2-00-00-xx of destination mac, rev-un = others\n"
    },
    {
        "show mgmt-frame", 2, _swc_cmd_showMgmtFrame,
        "swc show mgmt-frame [ unit=<UINT> ]\n"
    },
    {
        "set jumbo", 2, _swc_cmd_setJumbo,
        "swc set jumbo [ unit=<UINT> ]\n"
        "jumbo-len={ 1518 | 1536 | 1552 | 2048 | 3072 | 4096 | 5120 | 6144 | 7168 | 8192 | 9216 | 12288 | 15360 }\n"
    },
    {
        "show jumbo", 2, _swc_cmd_showJumbo,
        "swc show jumbo [ unit=<UINT> ]\n"
    },
    {
        "trigger cable-test", 2, _swc_cmd_triggerCableTest,
        "swc trigger cable-test [ unit=<UINT> ] portlist=<UINTLIST> pair={ a | b | c | d | all }\n"
    },
    {
        "set sys-mac", 2, _swc_cmd_setSysMac,
        "swc set sys-mac [ unit=<UINT> ] mac=<MACADDR>\n"
    },
    {
        "show sys-mac", 2, _swc_cmd_getSysMac,
        "swc show sys-mac [ unit=<UINT> ]\n"
    },
    {
        "set property", 2, _swc_cmd_setProperty,
        "swc set property [ unit=<UINT> ] \n"
        "type={ mac-auto-flush | l1-rate-ctrl | acl-rate-ctrl-mgmt-frame | storm-ctrl-mgmt-frame } state={ enable | disable }\n"
    },
    {
        "show property", 2, _swc_cmd_showProperty,
        "swc show property [ unit=<UINT> ]\n"
    },
    {
        "set lp-det", 2, _swc_cmd_setLoopDetect,
        "swc set lp-det [ unit=<UINT> ] portlist=<UINTLIST> mode={ enable | disable }\n"
    },
    {
        "show lp-det", 2, _swc_cmd_showLoopDetect,
        "swc show lp-det [ unit=<UINT> ] portlist=<UINTLIST>\n"
    },
    {
        "set lp-det-frame", 2, _swc_cmd_setLoopDetectFrame,
        "swc set lp-det-frame [ unit=<UINT> ] [ smac=<MACADDR> ] [ ethertype=<HEX> ]\n"
    },
    {
        "show lp-det-frame", 2, _swc_cmd_showLoopDetectFrame,
        "swc show lp-det-frame [ unit=<UINT> ]\n"
    },
    {
        "show lp-status", 2, _swc_cmd_showLoopDetectStatus,
        "swc show lp-status [ unit=<UINT> ] [ portlist=<UINTLIST> ]\n"
    },
    {
        "clear lp-status", 2, _swc_cmd_clrLoopDetectStatus,
        "swc clear lp-status [ unit=<UINT> ] [ portlist=<UINTLIST> ]\n"
    },
 };

AIR_ERROR_NO_T
swc_cmd_dispatcher(
    const C8_T      *tokens[],
    UI32_T          token_idx)
{
    return (dsh_dispatcher(tokens, token_idx, _swc_cmd_vec, sizeof(_swc_cmd_vec)/sizeof(DSH_VEC_T)));
}

AIR_ERROR_NO_T
swc_cmd_usager()
{
    return (dsh_usager(_swc_cmd_vec, sizeof(_swc_cmd_vec)/sizeof(DSH_VEC_T)));
}
