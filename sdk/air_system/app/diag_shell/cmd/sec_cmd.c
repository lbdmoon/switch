/*******************************************************************************
*  Copyright Statement:
*  --------------------
*  This software is protected by Copyright and the information contained
*  herein is confidential. The software may not be copied and the information
*  contained herein may not be used or disclosed except with the written
*  permission of Airoha Technology Corp. (C) 2021
*
*  BY OPENING THIS FILE, BUYER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
*  THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("AIROHA SOFTWARE")
*  RECEIVED FROM AIROHA AND/OR ITS REPRESENTATIVES ARE PROVIDED TO BUYER ON
*  AN "AS-IS" BASIS ONLY. AIROHA EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
*  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
*  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
*  NEITHER DOES AIROHA PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
*  SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
*  SUPPLIED WITH THE AIROHA SOFTWARE, AND BUYER AGREES TO LOOK ONLY TO SUCH
*  THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. AIROHA SHALL ALSO
*  NOT BE RESPONSIBLE FOR ANY AIROHA SOFTWARE RELEASES MADE TO BUYER'S
*  SPECIFICATION OR TO CONFORM TO A PARTICULAR STANDARD OR OPEN FORUM.
*
*  BUYER'S SOLE AND EXCLUSIVE REMEDY AND AIROHA'S ENTIRE AND CUMULATIVE
*  LIABILITY WITH RESPECT TO THE AIROHA SOFTWARE RELEASED HEREUNDER WILL BE,
*  AT AIROHA'S OPTION, TO REVISE OR REPLACE THE AIROHA SOFTWARE AT ISSUE,
*  OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY BUYER TO
*  AIROHA FOR SUCH AIROHA SOFTWARE AT ISSUE.
*
*  THE TRANSACTION CONTEMPLATED HEREUNDER SHALL BE CONSTRUED IN ACCORDANCE
*  WITH THE LAWS OF THE STATE OF CALIFORNIA, USA, EXCLUDING ITS CONFLICT OF
*  LAWS PRINCIPLES.  ANY DISPUTES, CONTROVERSIES OR CLAIMS ARISING THEREOF AND
*  RELATED THERETO SHALL BE SETTLED BY ARBITRATION IN SAN FRANCISCO, CA, UNDER
*  THE RULES OF THE INTERNATIONAL CHAMBER OF COMMERCE (ICC).
*
*******************************************************************************/

#include <air_error.h>
#include <air_types.h>
#include <air_sec.h>
#include <air_port.h>
#include <osal/osal.h>
#include <cmlib/cmlib_port.h>
#include <cmlib/cmlib_bitmap.h>
#include <parser/dsh_parser.h>
#include <parser/dsh_util.h>
#include <cmd/sec_cmd.h>
#include <cmd/port_cmd.h>
#include <hal/common/hal.h>
/* NAMING CONSTANT DECLARATIONS
 */

/* MACRO FUNCTION DECLARATIONS
 */
#define _SEC_CHECK_FLAG_STRING(__cmd__, __str__, __cond1__, __cond2__, __struc__, __flag__) do  \
{                                                                                               \
    osal_memset((__str__), 0, DSH_CMD_MAX_LENGTH);                                              \
    DSH_CHECK_PARAM(dsh_getString(tokens, token_idx, (__cmd__), (__str__)), token_idx, 2);      \
    if (AIR_E_OK == dsh_checkString((__str__), (__cond1__)))                                    \
    {                                                                                           \
        (__struc__).flags |= (__flag__);                                                        \
    }                                                                                           \
    else if (AIR_E_OK != dsh_checkString((__str__), (__cond2__)))                               \
    {                                                                                           \
        return DSH_E_SYNTAX_ERR;                                                                \
    }                                                                                           \
}while(0)

#define _SEC_CHECK_FLAG_ENABLE(__cmd__, __str__, __struc__, __flag__)                   \
    _SEC_CHECK_FLAG_STRING(__cmd__, __str__, "enable", "disable", __struc__, __flag__)

/* DATA TYPE DECLARATIONS
*/

/* GLOBAL VARIABLE DECLARATIONS
*/

/* LOCAL SUBPROGRAM BODIES
*/
static AIR_ERROR_NO_T
_sec_cmd_set8021x(
    const C8_T                  *tokens[],
    UI32_T                      token_idx)
{
    AIR_ERROR_NO_T              rc = AIR_E_OK;
    UI32_T                      unit = 0;
    AIR_SEC_8021X_MODE_T        base;
    C8_T                        str[DSH_CMD_MAX_LENGTH];

    /* cmd: sec set 8021x [ unit=<UINT> ] base={ mac | port }
     */

    DSH_CHECK_GET_UNIT(tokens, token_idx, &unit, 2);
    DSH_CHECK_PARAM(dsh_getString(tokens, token_idx, "base", str), token_idx, 2);
    if (AIR_E_OK == dsh_checkString(str, "mac"))
    {
        base = AIR_SEC_8021X_MODE_MAC;
    }
    else if (AIR_E_OK == dsh_checkString(str, "port"))
    {
        base = AIR_SEC_8021X_MODE_PORT;
    }
    else
    {
        return DSH_E_SYNTAX_ERR;
    }
    DSH_CHECK_LAST_TOKEN(tokens[token_idx]);

    rc = air_sec_set8021xGlobalMode(unit, base);
    if (AIR_E_OK != rc)
    {
        osal_printf("***Error***, set 8021x fail\n");
    }

    return (rc);
}

static AIR_ERROR_NO_T
_sec_cmd_show8021x(
    const C8_T                  *tokens[],
    UI32_T                      token_idx)
{
    AIR_ERROR_NO_T              rc = AIR_E_OK;
    UI32_T                      unit = 0;
    AIR_SEC_8021X_MODE_T        base;

    /* cmd: sec show 8021x [ unit=<UINT> ]
     */

    DSH_CHECK_GET_UNIT(tokens, token_idx, &unit, 2);
    DSH_CHECK_LAST_TOKEN(tokens[token_idx]);

    rc = air_sec_get8021xGlobalMode(unit, &base);
    if (AIR_E_OK != rc)
    {
        osal_printf("***Error***, show 8021x fail\n");
        return (rc);
    }
    osal_printf("802.1X authentication base : ");
    if (AIR_SEC_8021X_MODE_MAC == base)
    {
        osal_printf("MAC based\n");
    }
    else
    {
        osal_printf("Port based\n");
    }

    return (rc);
}

static AIR_ERROR_NO_T
_sec_cmd_set8021xConfig(
    const C8_T                  *tokens[],
    UI32_T                      token_idx)
{
    AIR_ERROR_NO_T              rc = AIR_E_OK;
    UI32_T                      unit = 0;
    C8_T                        str[DSH_CMD_MAX_LENGTH];
    AIR_SEC_8021X_CFG_T         config;

    /* cmd: sec set 8021x config [ unit=<UINT> ]
     *                              pb-pm={ allow | deny }
     *                              auth={ fwd | drop | cpu }
     *                              unauth={ fwd | drop | cpu }
     * Note: pb-pm means Portbase-Port-Move
     */

    osal_memset(&config, 0, sizeof(AIR_SEC_8021X_CFG_T));

    DSH_CHECK_GET_UNIT(tokens, token_idx, &unit, 2);

    _SEC_CHECK_FLAG_STRING("pb-pm", str, "allow", "deny", config,
        AIR_SEC_8021X_CFG_FLAGS_PB_PORT_MOVE);

    DSH_CHECK_PARAM(dsh_getString(tokens, token_idx, "auth", str), token_idx, 2);
    if (AIR_E_OK == dsh_checkString(str, "fwd"))
    {
        config.auth_fwd = AIR_SEC_8021X_FWD_MAC;
    }
    else if (AIR_E_OK == dsh_checkString(str, "drop"))
    {
        config.auth_fwd = AIR_SEC_8021X_FWD_DROP;
    }
    else if (AIR_E_OK == dsh_checkString(str, "cpu"))
    {
        config.auth_fwd = AIR_SEC_8021X_FWD_CPU;
    }
    else
    {
        return DSH_E_SYNTAX_ERR;
    }

    DSH_CHECK_PARAM(dsh_getString(tokens, token_idx, "unauth", str), token_idx, 2);
    if (AIR_E_OK == dsh_checkString(str, "fwd"))
    {
        config.unauth_fwd = AIR_SEC_8021X_FWD_MAC;
    }
    else if (AIR_E_OK == dsh_checkString(str, "drop"))
    {
        config.unauth_fwd = AIR_SEC_8021X_FWD_DROP;
    }
    else if (AIR_E_OK == dsh_checkString(str, "cpu"))
    {
        config.unauth_fwd = AIR_SEC_8021X_FWD_CPU;
    }
    else
    {
        return DSH_E_SYNTAX_ERR;
    }

    DSH_CHECK_LAST_TOKEN(tokens[token_idx]);

    rc = air_sec_set8021xGlobalCfg(unit, &config);
    if (AIR_E_OK != rc)
    {
        osal_printf("***Error***, set 8021x fail\n");
    }

    return (rc);
}

static AIR_ERROR_NO_T
_sec_cmd_show8021xConfig(
    const C8_T                  *tokens[],
    UI32_T                      token_idx)
{
    AIR_ERROR_NO_T              rc = AIR_E_OK;
    UI32_T                      unit = 0;
    AIR_SEC_8021X_CFG_T         config;
    AIR_SEC_8021X_FWD_T         *ptr_fwd;
    UI8_T                       i;

    /* cmd: sec show 8021x config [ unit=<UINT> ]
     */

    DSH_CHECK_GET_UNIT(tokens, token_idx, &unit, 2);
    DSH_CHECK_LAST_TOKEN(tokens[token_idx]);

    rc = air_sec_get8021xGlobalCfg(unit, &config);
    if (AIR_E_OK != rc)
    {
        osal_printf("***Error***, show 8021x fail\n");
        return (rc);
    }
    osal_printf("802.1X authenticated configurations: \n");
    osal_printf(" %-20s: ", "Port based port move");
    osal_printf("%s\n", (config.flags & AIR_SEC_8021X_CFG_FLAGS_PB_PORT_MOVE)?"Allowed":"Denied");
    for (i = 0; i < 2; i++)
    {
        if (!(i % 2))
        {
            osal_printf(" %-20s: ", "Authenticated");
            ptr_fwd = &config.auth_fwd;
        }
        else
        {
            osal_printf(" %-20s: ", "Unauthenticated");
            ptr_fwd = &config.unauth_fwd;
        }
        switch(*ptr_fwd)
        {
            case AIR_SEC_8021X_FWD_MAC:
                osal_printf("Forwarding according to MAC table.\n");
                break;
            case AIR_SEC_8021X_FWD_DROP:
                osal_printf("Drop.\n");
                break;
            case AIR_SEC_8021X_FWD_CPU:
                osal_printf("Trap to CPU.\n");
                break;
            default:
                osal_printf("Unknowned option.\n");
                break;
        }
    }
    return (rc);
}

static AIR_ERROR_NO_T
_sec_cmd_set8021xMac(
    const C8_T                  *tokens[],
    UI32_T                      token_idx)
{
    AIR_ERROR_NO_T              rc = AIR_E_OK;
    UI32_T                      unit = 0;
    C8_T                        str[DSH_CMD_MAX_LENGTH];
    AIR_SEC_8021X_MAC_CFG_T     mac_config;

    /* cmd: sec set 8021x mac [ unit=<UINT> ]
     *                        auth-port-move={ allow | deny }
     *                        unauth-port-move={ allow | deny }
     */

    osal_memset(&mac_config, 0, sizeof(AIR_SEC_8021X_MAC_CFG_T));

    DSH_CHECK_GET_UNIT(tokens, token_idx, &unit, 2);

    _SEC_CHECK_FLAG_STRING("auth-port-move", str, "allow", "deny", mac_config,
        AIR_SEC_8021X_MAC_CFG_FLAGS_PORT_MOVE_AUTH);

    _SEC_CHECK_FLAG_STRING("unauth-port-move", str, "allow", "deny", mac_config,
        AIR_SEC_8021X_MAC_CFG_FLAGS_PORT_MOVE_UNAUTH);

    DSH_CHECK_LAST_TOKEN(tokens[token_idx]);

    rc = air_sec_set8021xMacBasedCfg(unit, &mac_config);
    if (AIR_E_OK != rc)
    {
        osal_printf("***Error***, set 8021x mac fail\n");
    }

    return (rc);
}

static AIR_ERROR_NO_T
_sec_cmd_show8021xMac(
    const C8_T                  *tokens[],
    UI32_T                      token_idx)
{
    AIR_ERROR_NO_T              rc = AIR_E_OK;
    UI32_T                      unit = 0;
    AIR_SEC_8021X_MAC_CFG_T     mac_config;

    /* cmd: sec show 8021x mac [ unit=<UINT> ]
     */

    DSH_CHECK_GET_UNIT(tokens, token_idx, &unit, 2);
    DSH_CHECK_LAST_TOKEN(tokens[token_idx]);

    rc = air_sec_get8021xMacBasedCfg(unit, &mac_config);
    if (AIR_E_OK != rc)
    {
        osal_printf("***Error***, show 8021x mac fail\n");
        return (rc);
    }
    osal_printf("802.1X MAC-based authenticated configurations: \n");
    osal_printf(" %-30s: ","Authenticated MAC port move");
    osal_printf("%s\n", (mac_config.flags & AIR_SEC_8021X_MAC_CFG_FLAGS_PORT_MOVE_AUTH)?"Allowed":"Denied");
    osal_printf(" %-30s: ","Unauthenticated MAC port move");
    osal_printf("%s\n", (mac_config.flags & AIR_SEC_8021X_MAC_CFG_FLAGS_PORT_MOVE_UNAUTH)?"Allowed":"Denied");
    osal_printf("\n");

    return (rc);
}

static AIR_ERROR_NO_T
_sec_cmd_set8021xPort(
    const C8_T                  *tokens[],
    UI32_T                      token_idx)
{
    AIR_ERROR_NO_T              rc = AIR_E_OK;
    UI32_T                      unit = 0;
    AIR_PORT_BITMAP_T           bitmap = {0};
    UI32_T                      port;
    C8_T                        str[DSH_CMD_MAX_LENGTH];
    AIR_SEC_8021X_PORT_CFG_T    port_config;

    /* cmd: sec set 8021x port [ unit=<UINT> ] portlist=<UINTLIST> rx-auth={ auth | unauth }
     */

    osal_memset(&port_config, 0, sizeof(AIR_SEC_8021X_PORT_CFG_T));
    DSH_CHECK_GET_UNIT(tokens, token_idx, &unit, 2);
    DSH_CHECK_PARAM(dsh_getPortBitmap(tokens, token_idx, "portlist", unit, &bitmap), token_idx, 2);
    DSH_CHECK_PARAM(dsh_getString(tokens, token_idx, "rx-auth", str), token_idx, 2);
    if (AIR_E_OK == dsh_checkString(str, "unauth"))
    {
        port_config.rx_auth = AIR_SEC_8021X_AUTH_UNAUTH;
    }
    else if (AIR_E_OK == dsh_checkString(str, "auth"))
    {
        port_config.rx_auth = AIR_SEC_8021X_AUTH_AUTH;
    }
    else
    {
        return DSH_E_SYNTAX_ERR;
    }
    DSH_CHECK_LAST_TOKEN(tokens[token_idx]);

    AIR_PORT_FOREACH(bitmap, port)
    {
        rc = air_sec_set8021xPortBasedCfg(unit, port, &port_config);
        if (AIR_E_OK != rc)
        {
            osal_printf("***Error***, set 8021x port fail\n");
            break;
        }
    }
    return (rc);
}

static AIR_ERROR_NO_T
_sec_cmd_show8021xPort(
    const C8_T                  *tokens[],
    UI32_T                      token_idx)
{
    AIR_ERROR_NO_T              rc = AIR_E_OK;
    UI32_T                      unit = 0;
    AIR_PORT_BITMAP_T           bitmap = {0};
    UI32_T                      port;
    AIR_SEC_8021X_PORT_CFG_T    port_config;

    /* cmd: sec show 8021x port [ unit=<UINT> ] portlist=<UINTLIST>
     */

    DSH_CHECK_GET_UNIT(tokens, token_idx, &unit, 2);
    DSH_CHECK_PARAM(dsh_getPortBitmap(tokens, token_idx, "portlist", unit, &bitmap), token_idx, 2);
    DSH_CHECK_LAST_TOKEN(tokens[token_idx]);

    osal_printf("802.1X Port-based authenticated configurations: \n");
    osal_printf(" Port  rx-auth\n");
    osal_printf("===============\n");

    AIR_PORT_FOREACH(bitmap, port)
    {
        rc = air_sec_get8021xPortBasedCfg(unit, port, &port_config);
        if (AIR_E_OK != rc)
        {
            osal_printf("***Error***, show 8021x port fail\n");
            return (rc);
        }
        osal_printf(" %-4d ", port);
        osal_printf(" %-7s ", (AIR_SEC_8021X_AUTH_AUTH == port_config.rx_auth)?"authed":"unauthed");
        osal_printf("\n");
    }

    return (rc);
}

static AIR_ERROR_NO_T
_sec_cmd_setPortSecCfg(
    const C8_T                  *tokens[],
    UI32_T                      token_idx)
{
    AIR_ERROR_NO_T              rc = AIR_E_OK;
    UI32_T                      unit = 0;
    C8_T                        str[DSH_CMD_MAX_LENGTH];
    AIR_SEC_MAC_LIMIT_CFG_T     config;

    /* cmd: sec set port-security config [ unit=<UINT> ] mng-frm={ include | exclude }
     *                                   port-move-drop={ disable | enable }
     *                                   sa-full-drop={ disable | enable }
     *                                   port-move-tocpu={ disable | enable }
     *                                   sa-full-tocpu={ disable | enable }
     */

    osal_memset(&config, 0, sizeof(AIR_SEC_MAC_LIMIT_CFG_T));

    DSH_CHECK_GET_UNIT(tokens, token_idx, &unit, 2);
    _SEC_CHECK_FLAG_STRING("mng-frm", str, "exclude", "include", config,
        AIR_SEC_MAC_LIMIT_CFG_FLAGS_EXCLUDE_MGMT);
    _SEC_CHECK_FLAG_ENABLE("port-move-drop", str, config,
        AIR_SEC_MAC_LIMIT_CFG_FLAGS_DROP_PORT_MOVE);
    _SEC_CHECK_FLAG_ENABLE("sa-full-drop", str, config,
        AIR_SEC_MAC_LIMIT_CFG_FLAGS_DROP_SA_FULL);
    _SEC_CHECK_FLAG_ENABLE("port-move-tocpu", str, config,
        AIR_SEC_MAC_LIMIT_CFG_FLAGS_TOCPU_PORT_MOVE);
    _SEC_CHECK_FLAG_ENABLE("sa-full-tocpu", str, config,
        AIR_SEC_MAC_LIMIT_CFG_FLAGS_TOCPU_SA_FULL);
    DSH_CHECK_LAST_TOKEN(tokens[token_idx]);

    rc = air_sec_setMacLimitGlobalCfg(unit, &config);
    if (AIR_E_OK != rc)
    {
        osal_printf("***Error***, set port-security config fail\n");
    }

    return (rc);
}

static AIR_ERROR_NO_T
_sec_cmd_showPortSecCfg(
    const C8_T                  *tokens[],
    UI32_T                      token_idx)
{
    AIR_ERROR_NO_T              rc = AIR_E_OK;
    UI32_T                      unit = 0;
    AIR_SEC_MAC_LIMIT_CFG_T     config;

    /* cmd: sec show port-security config [ unit=<UINT> ]
     */

    DSH_CHECK_GET_UNIT(tokens, token_idx, &unit, 2);
    DSH_CHECK_LAST_TOKEN(tokens[token_idx]);

    rc = air_sec_getMacLimitGlobalCfg(unit, &config);
    if (AIR_E_OK != rc)
    {
        osal_printf("***Error***, show port-security config fail\n");
        return (rc);
    }

    osal_printf("Port Security Configurations: \n");
    osal_printf(" %-33s : %s\n", "Management frame",
            (config.flags & AIR_SEC_MAC_LIMIT_CFG_FLAGS_EXCLUDE_MGMT)?"excluded":"included");
    osal_printf(" %-33s : %s\n", "Drop port-move error frame",
            (config.flags & AIR_SEC_MAC_LIMIT_CFG_FLAGS_DROP_PORT_MOVE)?"enabled":"disabled");
    osal_printf(" %-33s : %s\n", "Drop SA full error frame",
            (config.flags & AIR_SEC_MAC_LIMIT_CFG_FLAGS_DROP_SA_FULL)?"enabled":"disabled");
    osal_printf(" %-33s : %s\n", "Trap port move error frame to CPU",
            (config.flags & AIR_SEC_MAC_LIMIT_CFG_FLAGS_TOCPU_PORT_MOVE)?"enabled":"disabled");
    osal_printf(" %-33s : %s\n", "Trap SA full error frame to CPU",
            (config.flags & AIR_SEC_MAC_LIMIT_CFG_FLAGS_TOCPU_SA_FULL)?"enabled":"disabled");
    osal_printf("\n");
    return (rc);
}

static AIR_ERROR_NO_T
_sec_cmd_setPortSecPort(
    const C8_T                  *tokens[],
    UI32_T                      token_idx)
{
    AIR_ERROR_NO_T                  rc = AIR_E_OK;
    UI32_T                          unit = 0;
    AIR_PORT_BITMAP_T               bitmap = {0};
    UI32_T                          port;
    C8_T                            str[DSH_CMD_MAX_LENGTH];
    AIR_SEC_MAC_LIMIT_PORT_CFG_T    port_config;

    /*  cmd:sec set port-security port [ unit=<UINT> ]  portlist=<UINTLIST>
     *                                  sa-learn={ disable | enable }
     *                                  sa-limit-mode={ disable | enable }
     *                                  sa-limit-count=<UINT>
     */

    osal_memset(&port_config, 0, sizeof(AIR_SEC_MAC_LIMIT_PORT_CFG_T));
    DSH_CHECK_GET_UNIT(tokens, token_idx, &unit, 2);
    DSH_CHECK_PARAM(dsh_getPortBitmap(tokens, token_idx, "portlist", unit, &bitmap), token_idx, 2);

    _SEC_CHECK_FLAG_ENABLE("sa-learn", str, port_config,
        AIR_SEC_MAC_LIMIT_PORT_CFG_FLAGS_SA_LRN);

    _SEC_CHECK_FLAG_ENABLE("sa-limit-mode", str, port_config,
        AIR_SEC_MAC_LIMIT_PORT_CFG_FLAGS_SA_LMT);

    DSH_CHECK_PARAM(dsh_getUint(tokens, token_idx, "sa-limit-count", &(port_config.sa_lmt_cnt)), token_idx, 2);
    DSH_CHECK_LAST_TOKEN(tokens[token_idx]);

    AIR_PORT_FOREACH(bitmap, port)
    {
        rc = air_sec_setMacLimitPortCfg(unit, port, &port_config);
        if (AIR_E_OK != rc)
        {
            osal_printf("***Error***, set port-security port fail\n");
            break;
        }
    }
    return (rc);
}

static AIR_ERROR_NO_T
_sec_cmd_showPortSecPort(
    const C8_T                  *tokens[],
    UI32_T                      token_idx)
{
    AIR_ERROR_NO_T                  rc = AIR_E_OK;
    UI32_T                          unit = 0;
    AIR_PORT_BITMAP_T               bitmap = {0};
    UI32_T                          port;
    AIR_SEC_MAC_LIMIT_PORT_CFG_T    port_config;

    /* cmd: sec show port-security port [ unit=<UINT> ] portlist=<UINTLIST>
     */

    DSH_CHECK_GET_UNIT(tokens, token_idx, &unit, 2);
    DSH_CHECK_PARAM(dsh_getPortBitmap(tokens, token_idx, "portlist", unit, &bitmap), token_idx, 2);
    DSH_CHECK_LAST_TOKEN(tokens[token_idx]);

    osal_printf(" unit  port  sa-learn  sa-limit-mode sa-limit-count\n");
    osal_printf("===================================================\n");
    AIR_PORT_FOREACH(bitmap, port)
    {
        rc = air_sec_getMacLimitPortCfg(unit, port, &port_config);
        if (AIR_E_OK != rc)
        {
            osal_printf("***Error***, show port-security port fail\n");
            return (rc);
        }

        osal_printf(" %-4d  ", unit);
        osal_printf("%-4d  ", port);
        osal_printf("%-8s  ",
                (port_config.flags & AIR_SEC_MAC_LIMIT_PORT_CFG_FLAGS_SA_LRN)?"enabled":"disabled");
        osal_printf("%-13s  ",
                (port_config.flags & AIR_SEC_MAC_LIMIT_PORT_CFG_FLAGS_SA_LMT)?"enabled":"disabled");
        osal_printf("%-14d  ", port_config.sa_lmt_cnt);
        osal_printf("\n");
    }
    return (rc);
}

static AIR_ERROR_NO_T
_sec_cmd_setPortSecMode(
    const C8_T                  *tokens[],
    UI32_T                      token_idx)
{
    AIR_ERROR_NO_T              rc = AIR_E_OK;
    UI32_T                      unit = 0;
    C8_T                        str[DSH_CMD_MAX_LENGTH];
    BOOL_T                      mode;

    /* cmd: sec set port-security [ unit=<UINT> ] mode={ disable | enable }
     */

    DSH_CHECK_GET_UNIT(tokens, token_idx, &unit, 2);
    DSH_CHECK_PARAM(dsh_getString(tokens, token_idx, "mode", str), token_idx, 2);
    if (AIR_E_OK == dsh_checkString(str, "enable"))
    {
        mode = TRUE;
    }
    else if (AIR_E_OK == dsh_checkString(str, "disable"))
    {
        mode = FALSE;
    }
    else
    {
        return DSH_E_SYNTAX_ERR;
    }
    DSH_CHECK_LAST_TOKEN(tokens[token_idx]);

    rc = air_sec_setMacLimitGlobalMode(unit, mode);
    if (AIR_E_OK != rc)
    {
        osal_printf("***Error***, set port-security fail\n");
    }

    return (rc);
}

static AIR_ERROR_NO_T
_sec_cmd_showPortSecMode(
    const C8_T                  *tokens[],
    UI32_T                      token_idx)
{
    AIR_ERROR_NO_T              rc = AIR_E_OK;
    UI32_T                      unit = 0;
    BOOL_T                      mode;

    /* cmd: sec show port-security [ unit=<UINT> ]
     */

    DSH_CHECK_GET_UNIT(tokens, token_idx, &unit, 2);
    DSH_CHECK_LAST_TOKEN(tokens[token_idx]);

    rc = air_sec_getMacLimitGlobalMode(unit, &mode);
    if (AIR_E_OK != rc)
    {
        osal_printf("***Error***, show port-security fail\n");
        return (rc);
    }

    osal_printf("Port Security Mode: %s\n", (mode & TRUE)?"enabled":"disabled");
    return (rc);
}

static AIR_ERROR_NO_T
_sec_cmd_stormCtlSet(
    const C8_T                *tokens[],
    UI32_T                    token_idx)
{
    /* Command format
    * "sec set stormctl-enable [ unit=<UINT> ] portlist =<UINTLIST> mode = {true|false} type = {mc|uc|bc}"
    */
    AIR_ERROR_NO_T rc = AIR_E_OK;

    UI32_T unit=0;
    AIR_PORT_BITMAP_T portlist = {0};
    UI32_T port;
    C8_T mode[DSH_CMD_MAX_LENGTH] = {0};
    C8_T type[DSH_CMD_MAX_LENGTH] = {0};

    AIR_SEC_STORM_TYPE_T type_t;
    BOOL_T en_t;

    /* parse and get */
    DSH_CHECK_GET_UNIT(tokens, token_idx, &unit, 2);

    DSH_CHECK_PARAM(dsh_getPortBitmap(tokens, token_idx, "portlist", unit, &portlist), token_idx, 2);
    DSH_CHECK_PARAM(dsh_getString(tokens, token_idx, "mode", mode), token_idx, 2);
    DSH_CHECK_PARAM(dsh_getString(tokens, token_idx, "type", type), token_idx, 2);

    if (AIR_E_OK == dsh_checkString(mode , "enable"))
    {
        en_t = TRUE;
    }
    else if (AIR_E_OK == dsh_checkString(mode , "disable"))
    {
        en_t = FALSE;
    }
    else
    {
        return DSH_E_SYNTAX_ERR;
    }

    if (AIR_E_OK == dsh_checkString(type, "bc"))
    {
        type_t = AIR_SEC_STORM_TYPE_BC;
    }
    else if (AIR_E_OK == dsh_checkString(type, "mc"))
    {
        type_t = AIR_SEC_STORM_TYPE_UMC;
    }
    else if (AIR_E_OK == dsh_checkString(type, "uc"))
    {
        type_t = AIR_SEC_STORM_TYPE_UUC;
    }
    else
    {
        return DSH_E_SYNTAX_ERR;
    }

    DSH_CHECK_LAST_TOKEN(tokens[token_idx]);

    /*Send to driver*/
    AIR_PORT_FOREACH(portlist, port)
    {
        rc = air_sec_setPortStormCtrl(unit, port, type_t, en_t);
        if (AIR_E_OK != rc)
        {
            osal_printf("ERROR, set Rate Stormctl cfg port %u error\n", port);
        }
        else
        {
            osal_printf("***Succ***, Set port %u Stormctl success \n", port);
        }
    }
    return rc;
}

static AIR_ERROR_NO_T
_sec_cmd_stormCtlCfgSet(
    const C8_T                *tokens[],
    UI32_T                    token_idx)
{
    /* Command format
    * "sec set stormctl-cfg [ unit=<UINT> ] portlist=<UINTLIST> type={mc|uc|bc} mode={pps|kbps} rate=<UINT>",
    */
    AIR_ERROR_NO_T rc = AIR_E_OK;

    UI32_T unit=0;
    AIR_PORT_BITMAP_T portlist;
    UI32_T port;

    C8_T    type[DSH_CMD_MAX_LENGTH] = {0};
    C8_T    mode[DSH_CMD_MAX_LENGTH] = {0};

    AIR_SEC_STORM_TYPE_T type_t;
    AIR_SEC_STORM_RATE_MODE_T mode_t;
    UI32_T  rate;

    /* parse and get */
    DSH_CHECK_GET_UNIT(tokens, token_idx, &unit, 2);

    DSH_CHECK_PARAM(dsh_getPortBitmap(tokens, token_idx, "portlist", unit, &portlist), token_idx, 2);
    DSH_CHECK_PARAM(dsh_getString(tokens, token_idx, "type", type), token_idx, 2);
    DSH_CHECK_PARAM(dsh_getString(tokens, token_idx, "mode", mode), token_idx, 2);
    DSH_CHECK_PARAM(dsh_getUint(tokens, token_idx, "rate", &rate), token_idx, 2);
    DSH_CHECK_LAST_TOKEN(tokens[token_idx]);

    if (AIR_E_OK == dsh_checkString(type, "bc"))
    {
        type_t = AIR_SEC_STORM_TYPE_BC;
    }
    else if (AIR_E_OK == dsh_checkString(type, "mc"))
    {
        type_t = AIR_SEC_STORM_TYPE_UMC;
    }
    else if (AIR_E_OK == dsh_checkString(type, "uc"))
    {
        type_t = AIR_SEC_STORM_TYPE_UUC;
    }
    else
    {
        return DSH_E_SYNTAX_ERR;
    }

    if(AIR_E_OK == dsh_checkString(mode , "kbps"))
    {
        mode_t = AIR_SEC_STORM_RATE_MODE_BPS;
    }
    else if(AIR_E_OK  == dsh_checkString(mode , "pps"))
    {
        mode_t = AIR_SEC_STORM_RATE_MODE_PPS;
    }
    else
    {
        return DSH_E_SYNTAX_ERR;
    }

    /*Send to driver*/
    AIR_PORT_FOREACH(portlist, port)
    {
        rc = air_sec_setPortStormCtrlRate(unit, port, type_t, mode_t, rate);
        if (AIR_E_OK != rc)
        {
            osal_printf("***Error***, set Rate Stormctl cfg error\n");
            break;
        }
        else
        {
            osal_printf("***Succ***, Set groupid %u port %u Stormctl success \n", type_t, port);
        }
    }
    return rc;
}

static AIR_ERROR_NO_T
_sec_cmd_stormCtlManageSet(
    const C8_T                *tokens[],
    UI32_T                    token_idx)
{
    /* Command format
    *  "sec set stormctl-exmng-frm [ unit=<UINT> ] type = {include | exclude}"
    *  "mode 0:disable 1:enable ",
    */
    AIR_ERROR_NO_T rc = AIR_E_OK;
    UI32_T unit=0;
    AIR_SEC_STORM_CTRL_MODE_T mng_t;
    C8_T    type[DSH_CMD_MAX_LENGTH] = {0};

    /* parse and get */
    DSH_CHECK_GET_UNIT(tokens, token_idx, &unit, 2);
    DSH_CHECK_PARAM(dsh_getString(tokens, token_idx, "type", type), token_idx, 2);
    DSH_CHECK_LAST_TOKEN(tokens[token_idx]);

    /*Send to driver*/
    if (AIR_E_OK == dsh_checkString(type, "include"))
    {
        mng_t = AIR_SEC_STORM_CTRL_MODE_MGMT_FRAME_INCLUDE;
    }
    else if (AIR_E_OK == dsh_checkString(type, "exclude"))
    {
        mng_t = AIR_SEC_STORM_CTRL_MODE_MGMT_FRAME_EXCLUDE;
    }
    else
    {
        return DSH_E_SYNTAX_ERR;
    }

    rc = air_sec_setStormCtrlMgmtMode(unit, mng_t);
    if (AIR_E_OK != rc)
    {
        osal_printf("***Error***,set stormctl manage mode %s value failed \n", (mng_t & TRUE)?"exclude":"include");
    }
    else
    {
        osal_printf("Success.\n");
    }
    return rc;
}

static AIR_ERROR_NO_T
_sec_cmd_stormCtlGet(
    const C8_T                *tokens[],
    UI32_T                    token_idx)
{
    /* Command format
    * "sec get stormctl-enable [ unit=<UINT> ] portlist =<UINTLIST> "
    * "bc group1 /mc group2 /uc group3",
    */
    AIR_ERROR_NO_T rc = AIR_E_OK;
    UI32_T unit=0;
    AIR_PORT_BITMAP_T portlist = {0};
    UI32_T port;
    BOOL_T en_t;

    AIR_PORT_BITMAP_T bc_grp = {0};
    AIR_PORT_BITMAP_T mc_grp = {0};
    AIR_PORT_BITMAP_T uc_grp = {0};

    /* parse and get */
    DSH_CHECK_GET_UNIT(tokens, token_idx, &unit, 2);

    DSH_CHECK_PARAM(dsh_getPortBitmap(tokens, token_idx, "portlist", unit, &portlist), token_idx, 2);

    DSH_CHECK_LAST_TOKEN(tokens[token_idx]);

    AIR_PORT_BITMAP_CLEAR(bc_grp);
    AIR_PORT_BITMAP_CLEAR(mc_grp);
    AIR_PORT_BITMAP_CLEAR(uc_grp);

    /*Send to driver*/
    AIR_PORT_FOREACH(portlist, port)
    {
        rc = air_sec_getPortStormCtrl(unit, port, AIR_SEC_STORM_TYPE_BC, &en_t);
        if (AIR_E_OK == rc)
        {
            if (en_t)
            {
                CMLIB_BITMAP_BIT_ADD(bc_grp, port);
            }
        }
        rc = air_sec_getPortStormCtrl(unit, port, AIR_SEC_STORM_TYPE_UMC, &en_t);
        if (AIR_E_OK == rc)
        {
            if (en_t)
            {
                CMLIB_BITMAP_BIT_ADD(mc_grp, port);
            }
        }
        rc = air_sec_getPortStormCtrl(unit, port, AIR_SEC_STORM_TYPE_UUC, &en_t);
        if (AIR_E_OK == rc)
        {
            if (en_t)
            {
                CMLIB_BITMAP_BIT_ADD(uc_grp, port);
            }
        }
    }
    /*Print info here*/
    osal_printf("\nbc enable port is:\n");
    CMLIB_PORT_BITMAP_COUNT(bc_grp, port);
    if (0 == port)
    {
        osal_printf("None");
    }
    else
    {
        CMLIB_BITMAP_BIT_FOREACH(bc_grp, port, AIR_PORT_BITMAP_SIZE)
        {
            osal_printf("%u\t", port);
        }
    }
    osal_printf("\nmc enable port is:\n");
    CMLIB_PORT_BITMAP_COUNT(mc_grp, port);
    if (0 == port)
    {
        osal_printf("None");
    }
    else
    {
        CMLIB_BITMAP_BIT_FOREACH(mc_grp, port, AIR_PORT_BITMAP_SIZE)
        {
            osal_printf("%u\t", port);
        }
    }
    osal_printf("\nuc enable port is:\n");
    CMLIB_PORT_BITMAP_COUNT(uc_grp, port);
    if (0 == port)
    {
        osal_printf("None");
    }
    else
    {
        CMLIB_BITMAP_BIT_FOREACH(uc_grp, port, AIR_PORT_BITMAP_SIZE)
        {
            osal_printf("%u\t", port);
        }
    }
    return rc;
}

static AIR_ERROR_NO_T
_sec_cmd_stormCtlCfgGet(
    const C8_T                *tokens[],
    UI32_T                    token_idx)
{
    /* Command format
    * "sec get stormctl-cfg [ unit=<UINT> ] portlist =<UINTLIST> "
    * "return all port settting",
    */
    AIR_ERROR_NO_T rc = AIR_E_OK;
    UI32_T unit=0;
    AIR_PORT_BITMAP_T portlist;
    UI32_T port;

    AIR_SEC_STORM_RATE_MODE_T mode_t;
    UI32_T  rate;
    AIR_SEC_STORM_TYPE_T rate_unit;
    C8_T *str_type[]={
        "Broadcast",
        "Unknown Unicast",
        "Unknown Multicast",
    };

    /* parse and get */
    DSH_CHECK_GET_UNIT(tokens, token_idx, &unit, 2);
    DSH_CHECK_PARAM(dsh_getPortBitmap(tokens, token_idx, "portlist", unit, &portlist), token_idx, 2);
    DSH_CHECK_LAST_TOKEN(tokens[token_idx]);

    /*Send to driver*/
    AIR_PORT_FOREACH(portlist, port)
    {
        osal_printf("Port %u config setting is: \n", port);
        for(rate_unit = AIR_SEC_STORM_TYPE_BC; rate_unit < AIR_SEC_STORM_TYPE_LAST; rate_unit++)
        {
            rc = air_sec_getPortStormCtrlRate(unit, port, rate_unit, &mode_t, &rate);
            if (AIR_E_OK != rc)
            {
                osal_printf("***Error***, get %s Rate Stormctl cfg error\n", str_type[rate_unit]);
                break;
            }
            else
            {
                osal_printf("%s:\n\trate ---- %07d\tmode ---- %s\t\n",
                        str_type[rate_unit], rate, mode_t?"kbps":"pps");
            }
        }
    }

    return rc;
}

static AIR_ERROR_NO_T
_sec_cmd_stormCtlManageGet(
    const C8_T                *tokens[],
    UI32_T                    token_idx)
{
    /* Command format
    * "sec get stormctl-exmng-frm [ unit=<UINT> ]"
    * "return mode = <UINT> mode 0:include 1:exclude ",
    */
    AIR_ERROR_NO_T rc = AIR_E_OK;
    UI32_T unit=0;
    AIR_SEC_STORM_CTRL_MODE_T mng_t;

    /* parse and get */
    DSH_CHECK_GET_UNIT(tokens, token_idx, &unit, 2);
    DSH_CHECK_LAST_TOKEN(tokens[token_idx]);

    /*Send to driver*/
    rc = air_sec_getStormCtrlMgmtMode(unit, &mng_t);
    if (AIR_E_OK != rc)
    {
        osal_printf("***Error***, get stormctl manage mode failed , rc is %u\n", rc);
    }
    else
    {
        osal_printf("stormctl management mode %s \n", mng_t ? "exclude": "include");
    }
    return rc;
}

/* -------------------------------------------------------------- callback */
const static DSH_VEC_T  _sec_cmd_vec[] =
{
    {
        "set 8021x config", 3, _sec_cmd_set8021xConfig,
        "sec set 8021x config [ unit=<UINT> ] pb-pm={ allow | deny }\n"
        "                     auth={ fwd | drop | cpu }\n"
        "                     unauth={ fwd | drop | cpu }\n"
        "Note: pb-pm means Portbase-Port-Move\n"
    },
    {
        "show 8021x config", 3, _sec_cmd_show8021xConfig,
        "sec show 8021x config [ unit=<UINT> ]\n"
    },
    {
        "set 8021x mac", 3, _sec_cmd_set8021xMac,
        "sec set 8021x mac [ unit=<UINT> ]\n"
        "                  auth-port-move={ allow | deny }\n"
        "                  unauth-port-move={ allow | deny }\n"
    },
    {
        "show 8021x mac", 3, _sec_cmd_show8021xMac,
        "sec show 8021x mac [ unit=<UINT> ]\n"
    },
    {
        "set 8021x port", 3, _sec_cmd_set8021xPort,
        "sec set 8021x port [ unit=<UINT> ] portlist=<UINTLIST> rx-auth={ auth | unauth }\n"
    },
    {
        "show 8021x port", 3, _sec_cmd_show8021xPort,
        "sec show 8021x port [ unit=<UINT> ] portlist=<UINTLIST>\n"
    },
    {
        "set 8021x", 2, _sec_cmd_set8021x,
        "sec set 8021x [ unit=<UINT> ] base={ mac | port }\n"
    },
    {
        "show 8021x", 2, _sec_cmd_show8021x,
        "sec show 8021x [ unit=<UINT> ]\n"
    },
    {
        "set port-security config", 3, _sec_cmd_setPortSecCfg,
        "sec set port-security config [ unit=<UINT> ] mng-frm={ include | exclude }\n"
        "                             port-move-drop={ enable | disable }\n"
        "                             sa-full-drop={ enable | disable }\n"
        "                             port-move-tocpu={ enable | disable }\n"
        "                             sa-full-tocpu={ enable | disable }\n"
    },
    {
        "show port-security config", 3, _sec_cmd_showPortSecCfg,
        "sec show port-security config [ unit=<UINT> ]\n"
    },
    {
        "set port-security port", 3, _sec_cmd_setPortSecPort,
        "sec set port-security port [ unit=<UINT> ]  portlist=<UINTLIST>\n"
        "                           sa-learn={ enable | disable }\n"
        "                           sa-limit-mode={ enable | disable }\n"
        "                           sa-limit-count=<UINT>\n"
    },
    {
        "show port-security port", 3, _sec_cmd_showPortSecPort,
        "sec show port-security port [ unit=<UINT> ] portlist=<UINTLIST>\n"
    },
    {
        "set port-security", 2, _sec_cmd_setPortSecMode,
        "sec set port-security [ unit=<UINT> ] mode={ enable | disable }\n"
    },
    {
        "show port-security", 2, _sec_cmd_showPortSecMode,
        "sec show port-security [ unit=<UINT> ]\n"
    },
    {
        "set stormctl-enable", 2, _sec_cmd_stormCtlSet,
        "sec set stormctl-enable [ unit=<UINT> ] portlist=<UINTLIST> mode={ enable | disable } type={ mc | uc | bc }\n",
    },
    {
        "set stormctl-cfg", 2, _sec_cmd_stormCtlCfgSet,
        "sec set stormctl-cfg [ unit=<UINT> ] portlist=<UINTLIST> type={ mc | uc | bc } mode={ pps | kbps } rate=<UINT>\n"
        "Note: EN8851 rate=0~2500K\n"
    },
    {
        "set stormctl-exmng-frm", 2, _sec_cmd_stormCtlManageSet,
        "sec set stormctl-exmng-frm [ unit=<UINT> ] type={ include | exclude }\n",
    },
    {
        "get stormctl-enable", 2, _sec_cmd_stormCtlGet,
        "sec get stormctl-enable [ unit=<UINT> ] portlist=<UINTLIST>\n",
    },
    {
        "get stormctl-cfg", 2, _sec_cmd_stormCtlCfgGet,
        "sec get stormctl-cfg [ unit=<UINT> ] portlist=<UINTLIST>\n",
    },
    {
        "get stormctl-exmng-frm", 2, _sec_cmd_stormCtlManageGet,
        "sec get stormctl-exmng-frm [ unit=<UINT> ] \n",
    },
};

AIR_ERROR_NO_T
sec_cmd_dispatcher(
    const C8_T                  *tokens[],
    UI32_T                      token_idx)
{
    return (dsh_dispatcher(tokens, token_idx, _sec_cmd_vec, sizeof(_sec_cmd_vec)/sizeof(DSH_VEC_T)));
}

AIR_ERROR_NO_T
sec_cmd_usager()
{
    return (dsh_usager(_sec_cmd_vec, sizeof(_sec_cmd_vec)/sizeof(DSH_VEC_T)));
}

