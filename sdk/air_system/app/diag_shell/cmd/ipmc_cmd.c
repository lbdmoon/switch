/*******************************************************************************
*  Copyright Statement:
*  --------------------
*  This software is protected by Copyright and the information contained
*  herein is confidential. The software may not be copied and the information
*  contained herein may not be used or disclosed except with the written
*  permission of Airoha Technology Corp. (C) 2021
*
*  BY OPENING THIS FILE, BUYER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
*  THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("AIROHA SOFTWARE")
*  RECEIVED FROM AIROHA AND/OR ITS REPRESENTATIVES ARE PROVIDED TO BUYER ON
*  AN "AS-IS" BASIS ONLY. AIROHA EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
*  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
*  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
*  NEITHER DOES AIROHA PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
*  SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
*  SUPPLIED WITH THE AIROHA SOFTWARE, AND BUYER AGREES TO LOOK ONLY TO SUCH
*  THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. AIROHA SHALL ALSO
*  NOT BE RESPONSIBLE FOR ANY AIROHA SOFTWARE RELEASES MADE TO BUYER'S
*  SPECIFICATION OR TO CONFORM TO A PARTICULAR STANDARD OR OPEN FORUM.
*
*  BUYER'S SOLE AND EXCLUSIVE REMEDY AND AIROHA'S ENTIRE AND CUMULATIVE
*  LIABILITY WITH RESPECT TO THE AIROHA SOFTWARE RELEASED HEREUNDER WILL BE,
*  AT AIROHA'S OPTION, TO REVISE OR REPLACE THE AIROHA SOFTWARE AT ISSUE,
*  OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY BUYER TO
*  AIROHA FOR SUCH AIROHA SOFTWARE AT ISSUE.
*
*  THE TRANSACTION CONTEMPLATED HEREUNDER SHALL BE CONSTRUED IN ACCORDANCE
*  WITH THE LAWS OF THE STATE OF CALIFORNIA, USA, EXCLUDING ITS CONFLICT OF
*  LAWS PRINCIPLES.  ANY DISPUTES, CONTROVERSIES OR CLAIMS ARISING THEREOF AND
*  RELATED THERETO SHALL BE SETTLED BY ARBITRATION IN SAN FRANCISCO, CA, UNDER
*  THE RULES OF THE INTERNATIONAL CHAMBER OF COMMERCE (ICC).
*
*******************************************************************************/

#include <air_error.h>
#include <air_types.h>
#include <air_cfg.h>
#include <air_ipmc.h>
#include <air_port.h>
#include <osal/osal.h>
#include <cmlib/cmlib_port.h>
#include <cmlib/cmlib_util.h>
#include <hal/common/hal.h>
#include <parser/dsh_parser.h>
#include <parser/dsh_util.h>
#include <cmd/ipmc_cmd.h>
#include <cmd/port_cmd.h>

static void
_ipmc_cmd_getMcastStr(
    AIR_IP_ADDR_T   *group_addr,
    AIR_IP_ADDR_T   *source_addr,
    C8_T            *dip_str,
    C8_T            *sip_str)
{
    osal_memset(dip_str, 0, CMLIB_UTIL_IP_ADDR_STR_SIZE);
    osal_memset(sip_str, 0, CMLIB_UTIL_IP_ADDR_STR_SIZE);
    cmlib_util_getIpAddrStr(group_addr, dip_str);
    cmlib_util_getIpAddrStr(source_addr, sip_str);
    return;
}

static void
_ipmc_cmd_printMcastStr(
    C8_T            *str_op,
    AIR_IPMC_TYPE_T type,
    UI32_T          vid,
    AIR_IP_ADDR_T   *group_addr,
    AIR_IP_ADDR_T   *source_addr,
    AIR_PORT_BITMAP_T portmap,
    AIR_ERROR_NO_T rc)
{
    C8_T                dip_str[CMLIB_UTIL_IP_ADDR_STR_SIZE];
    C8_T                sip_str[CMLIB_UTIL_IP_ADDR_STR_SIZE];
    C8_T                pbmp_str[CMLIB_UTIL_IP_ADDR_STR_SIZE];
    _ipmc_cmd_getMcastStr(group_addr, source_addr, dip_str, sip_str);
    CMD_STRING_PORTLIST(pbmp_str, portmap);
    if (AIR_E_OK == rc)
    {
        if (osal_strncmp(str_op, "Delete", osal_strlen(str_op)) == 0)
        {
            if (type == AIR_IPMC_TYPE_GRP_SRC)
            {
                osal_printf("%s vid %d group %s source %s OK\n",
                            str_op, vid, dip_str, sip_str);
            }
            else
            {
                osal_printf("%s vid %d group %s OK\n",
                            str_op, vid, dip_str);
            }
        }
        else
        {
            if (type == AIR_IPMC_TYPE_GRP_SRC)
            {
                osal_printf("%s vid %d group %s source %s portlist %s OK\n",
                            str_op, vid, dip_str, sip_str, pbmp_str);
            }
            else
            {
                osal_printf("%s vid %d group %s portlist %s OK\n",
                            str_op, vid, dip_str, pbmp_str);
            }
        }

    }
    else if(AIR_E_ENTRY_NOT_FOUND == rc)
    {
        if (type == AIR_IPMC_TYPE_GRP_SRC)
        {
            osal_printf("***Error***, Can not find vid %d group %s source %s.",
                        vid, dip_str, sip_str);
        }
        else
        {
            osal_printf("***Error***, Can not find vid %d group %s.\n",
                        vid, dip_str);
        }
    }
    else if(AIR_E_ENTRY_EXISTS == rc)
    {
        if (AIR_IPMC_TYPE_GRP_SRC == type)
        {
            osal_printf("***Error***, vid %d group %s source %s exists.\n", vid, dip_str, sip_str);
        }
        else
        {
            osal_printf("***Error***, vid %d group %s exists.\n", vid, dip_str);
        }
    }
    else
    {
        if (AIR_IPMC_TYPE_GRP_SRC == type)
        {
            osal_printf("***Error***, %s vid %d group %s source %s portlist %s Fail.\n", str_op, vid, dip_str, sip_str, pbmp_str);
        }
        else
        {
            osal_printf("***Error***, %s vid %d group %s portlist %s Fail.\n", str_op, vid, dip_str, pbmp_str);
        }
    }
    return;
}

static AIR_ERROR_NO_T
_ipmc_cmd_addMcastAddr(
    const C8_T  *tokens[],
    UI32_T      token_idx)
{
    AIR_ERROR_NO_T     rc = AIR_E_OK;
    UI32_T             unit = 0, vid = 0;
    AIR_PORT_BITMAP_T  pbm;
    AIR_IPMC_ENTRY_T    entry;
    AIR_IP_ADDR_T       group_addr, source_addr;
    C8_T                dip_str[CMLIB_UTIL_IP_ADDR_STR_SIZE];
    C8_T                sip_str[CMLIB_UTIL_IP_ADDR_STR_SIZE];

    osal_memset(&entry, 0, sizeof(AIR_IPMC_ENTRY_T));
    osal_memset(&group_addr, 0, sizeof(AIR_IP_ADDR_T));
    osal_memset(&source_addr, 0, sizeof(AIR_IP_ADDR_T));
    osal_memset(dip_str, 0, CMLIB_UTIL_IP_ADDR_STR_SIZE);
    osal_memset(sip_str, 0, CMLIB_UTIL_IP_ADDR_STR_SIZE);
    AIR_PORT_BITMAP_CLEAR(pbm);
    DSH_CHECK_GET_UNIT(tokens, token_idx, &unit, 2);
    DSH_CHECK_PARAM(dsh_getUint(tokens, token_idx, "vid", &vid), token_idx, 2);
    if (AIR_E_OK == dsh_checkString(tokens[token_idx], "gaddr"))
    {
        DSH_CHECK_IP_ADDR(dsh_getIpv4Addr(tokens, token_idx, "gaddr", &group_addr.ip_addr.ipv4_addr),
                          dsh_getIpv6Addr(tokens, token_idx, "gaddr", &group_addr.ip_addr.ipv6_addr),
                          group_addr.ipv4, token_idx, 2);
        if (!AIR_L3_IP_IS_MULTICAST(&group_addr))
        {
            osal_printf("***Error***, Input gaddr shall be a mulitcast address.\n");
            return AIR_E_BAD_PARAMETER;
        }
    }
    if (AIR_E_OK == dsh_checkString(tokens[token_idx], "saddr"))
    {
        DSH_CHECK_IP_ADDR(dsh_getIpv4Addr(tokens, token_idx, "saddr", &source_addr.ip_addr.ipv4_addr),
                          dsh_getIpv6Addr(tokens, token_idx, "saddr", &source_addr.ip_addr.ipv6_addr),
                          source_addr.ipv4, token_idx, 2);
        if (AIR_L3_IP_IS_MULTICAST(&source_addr))
        {
            osal_printf("***Error***, Input saddr shall not be a mulitcast address.\n");
            return AIR_E_BAD_PARAMETER;
        }
    }

    DSH_CHECK_PARAM(dsh_getPortBitmap(tokens, token_idx, "portlist", unit, &pbm), token_idx, 2);
    DSH_CHECK_LAST_TOKEN(tokens[token_idx]);

    AIR_PORT_BITMAP_COPY(entry.port_bitmap,pbm);

    entry.vid = vid;
    osal_memcpy(&entry.group_addr, &group_addr, sizeof(AIR_IP_ADDR_T));
    osal_memcpy(&entry.source_addr, &source_addr, sizeof(AIR_IP_ADDR_T));

    if (CMLIB_UTIL_IP_ADDR_IS_ZERO(entry.source_addr))
    {
        entry.type = AIR_IPMC_TYPE_GRP;
    }
    else
    {
        entry.type = AIR_IPMC_TYPE_GRP_SRC;
    }

    if((entry.type== AIR_VER_IGMP3_MLD2) &&
       (group_addr.ipv4 != source_addr.ipv4))
    {
        osal_printf("***Error***, IP family of group and source are not the same.\n");
        return AIR_E_BAD_PARAMETER;
    }

    rc = air_ipmc_addMcastAddr(unit, &entry);
    if (AIR_E_OK != rc)
    {
        _ipmc_cmd_printMcastStr("Add", entry.type, entry.vid, &entry.group_addr, &entry.source_addr, entry.port_bitmap, rc);
    }

    return rc;
}

static AIR_ERROR_NO_T
_ipmc_cmd_getMcastAddr(
    const C8_T  *tokens[],
    UI32_T      token_idx)
{
    AIR_ERROR_NO_T  rc = AIR_E_OK;
    UI32_T          unit = 0, vid = 0;
    AIR_IPMC_ENTRY_T    entry;
    AIR_IP_ADDR_T       group_addr, source_addr;
    C8_T                dip_str[CMLIB_UTIL_IP_ADDR_STR_SIZE];
    C8_T                sip_str[CMLIB_UTIL_IP_ADDR_STR_SIZE];

    osal_memset(&entry, 0, sizeof(AIR_IPMC_ENTRY_T));
    osal_memset(&group_addr, 0, sizeof(AIR_IP_ADDR_T));
    osal_memset(&source_addr, 0, sizeof(AIR_IP_ADDR_T));
    osal_memset(dip_str, 0, CMLIB_UTIL_IP_ADDR_STR_SIZE);
    osal_memset(sip_str, 0, CMLIB_UTIL_IP_ADDR_STR_SIZE);

    DSH_CHECK_GET_UNIT(tokens, token_idx, &unit, 2);
    DSH_CHECK_PARAM(dsh_getUint(tokens, token_idx, "vid", &vid), token_idx, 2);
    if (AIR_E_OK == dsh_checkString(tokens[token_idx], "gaddr"))
    {
        DSH_CHECK_IP_ADDR(dsh_getIpv4Addr(tokens, token_idx, "gaddr", &group_addr.ip_addr.ipv4_addr),
                          dsh_getIpv6Addr(tokens, token_idx, "gaddr", &group_addr.ip_addr.ipv6_addr),
                          group_addr.ipv4, token_idx, 2);
        if (!AIR_L3_IP_IS_MULTICAST(&group_addr))
        {
            osal_printf("***Error***, Input gaddr shall be a mulitcast address.\n");
            return AIR_E_BAD_PARAMETER;
        }
    }
    if (AIR_E_OK == dsh_checkString(tokens[token_idx], "saddr"))
    {
        DSH_CHECK_IP_ADDR(dsh_getIpv4Addr(tokens, token_idx, "saddr", &source_addr.ip_addr.ipv4_addr),
                          dsh_getIpv6Addr(tokens, token_idx, "saddr", &source_addr.ip_addr.ipv6_addr),
                          source_addr.ipv4, token_idx, 2);
        if (AIR_L3_IP_IS_MULTICAST(&source_addr))
        {
            osal_printf("***Error***, Input saddr shall not be a mulitcast address.\n");
            return AIR_E_BAD_PARAMETER;
        }
    }
    DSH_CHECK_LAST_TOKEN(tokens[token_idx]);

    entry.vid = vid;
    osal_memcpy(&entry.group_addr, &group_addr, sizeof(AIR_IP_ADDR_T));
    osal_memcpy(&entry.source_addr, &source_addr, sizeof(AIR_IP_ADDR_T));

    if (CMLIB_UTIL_IP_ADDR_IS_ZERO(entry.source_addr))
    {
        entry.type = AIR_IPMC_TYPE_GRP;
    }
    else
    {
        entry.type = AIR_IPMC_TYPE_GRP_SRC;
    }

    if((entry.type == AIR_IPMC_TYPE_GRP_SRC) &&
       (group_addr.ipv4 != source_addr.ipv4))
    {
        osal_printf("***Error***, IP family of group and source are not the same.\n");
        return AIR_E_BAD_PARAMETER;
    }

    rc = air_ipmc_getMcastAddr(unit, &entry);
    if(rc == AIR_E_OK)
    {
        _ipmc_cmd_printMcastStr("Get", entry.type, entry.vid, &entry.group_addr, &entry.source_addr, entry.port_bitmap, rc);
    }
    else if(rc == AIR_E_ENTRY_NOT_FOUND)
    {
        _ipmc_cmd_printMcastStr("", entry.type, entry.vid, &entry.group_addr, &entry.source_addr, entry.port_bitmap, rc);
    }
    else
    {
        osal_printf("***Error***, Get Fail.\n");
    }

    return rc;
}

static AIR_ERROR_NO_T
_ipmc_cmd_delMcastAddr(
    const C8_T  *tokens[],
    UI32_T      token_idx)
{
    AIR_ERROR_NO_T  rc = AIR_E_OK;
    UI32_T          unit = 0, vid = 0;
    AIR_IPMC_ENTRY_T    entry;
    AIR_IP_ADDR_T       group_addr, source_addr;

    osal_memset(&entry, 0, sizeof(AIR_IPMC_ENTRY_T));
    osal_memset(&group_addr, 0, sizeof(AIR_IP_ADDR_T));
    osal_memset(&source_addr, 0, sizeof(AIR_IP_ADDR_T));

    DSH_CHECK_GET_UNIT(tokens, token_idx, &unit, 2);
    DSH_CHECK_PARAM(dsh_getUint(tokens, token_idx, "vid", &vid), token_idx, 2);
    if (AIR_E_OK == dsh_checkString(tokens[token_idx], "gaddr"))
    {
        DSH_CHECK_IP_ADDR(dsh_getIpv4Addr(tokens, token_idx, "gaddr", &group_addr.ip_addr.ipv4_addr),
                          dsh_getIpv6Addr(tokens, token_idx, "gaddr", &group_addr.ip_addr.ipv6_addr),
                          group_addr.ipv4, token_idx, 2);
        if (!AIR_L3_IP_IS_MULTICAST(&group_addr))
        {
            osal_printf("***Error***, Input gaddr shall be a mulitcast address.\n");
            return AIR_E_BAD_PARAMETER;
        }
    }
    if (AIR_E_OK == dsh_checkString(tokens[token_idx], "saddr"))
    {
        DSH_CHECK_IP_ADDR(dsh_getIpv4Addr(tokens, token_idx, "saddr", &source_addr.ip_addr.ipv4_addr),
                          dsh_getIpv6Addr(tokens, token_idx, "saddr", &source_addr.ip_addr.ipv6_addr),
                          source_addr.ipv4, token_idx, 2);
        if (AIR_L3_IP_IS_MULTICAST(&source_addr))
        {
            osal_printf("***Error***, Input saddr shall not be a mulitcast address.\n");
            return AIR_E_BAD_PARAMETER;
        }
    }
    DSH_CHECK_LAST_TOKEN(tokens[token_idx]);

    entry.vid = vid;
    osal_memcpy(&entry.group_addr, &group_addr, sizeof(AIR_IP_ADDR_T));
    osal_memcpy(&entry.source_addr, &source_addr, sizeof(AIR_IP_ADDR_T));

    if (CMLIB_UTIL_IP_ADDR_IS_ZERO(entry.source_addr))
    {
        entry.type = AIR_IPMC_TYPE_GRP;
    }
    else
    {
        entry.type = AIR_IPMC_TYPE_GRP_SRC;
    }
    if((entry.type == AIR_IPMC_TYPE_GRP_SRC) &&
       (group_addr.ipv4 != source_addr.ipv4))
    {
        osal_printf("***Error***, IP family of group and source are not the same.\n");
        return AIR_E_BAD_PARAMETER;
    }

    rc = air_ipmc_delMcastAddr(unit,&entry);
    if (AIR_E_OK != rc)
    {
        _ipmc_cmd_printMcastStr("Delete", entry.type, entry.vid, &entry.group_addr, &entry.source_addr, entry.port_bitmap, rc);
    }

    return rc;
}

static AIR_ERROR_NO_T
_ipmc_cmd_delAllMcastAddr(
    const C8_T  *tokens[],
    UI32_T      token_idx)
{
    AIR_ERROR_NO_T  rc = AIR_E_OK;
    UI32_T          unit = 0;

    DSH_CHECK_GET_UNIT(tokens, token_idx, &unit, 2);
    DSH_CHECK_LAST_TOKEN(tokens[token_idx]);

    rc = air_ipmc_delAllMcastAddr(unit);
    if(AIR_E_OK != rc)
    {
        osal_printf("***Error***, Delete all Fail.\n");
    }

    return rc;

}


static AIR_ERROR_NO_T
_ipmc_cmd_addMcastMember(
    const C8_T  *tokens[],
    UI32_T      token_idx)
{
    AIR_ERROR_NO_T    rc = AIR_E_OK;
    UI32_T             unit = 0, vid = 0;
    AIR_IP_ADDR_T     group_addr, source_addr;
    AIR_PORT_BITMAP_T pbm;
    AIR_IPMC_ENTRY_T    entry;

    osal_memset(&entry, 0, sizeof(AIR_IPMC_ENTRY_T));
    osal_memset(&group_addr, 0, sizeof(AIR_IP_ADDR_T));
    osal_memset(&source_addr, 0, sizeof(AIR_IP_ADDR_T));
    AIR_PORT_BITMAP_CLEAR(pbm);
    DSH_CHECK_GET_UNIT(tokens, token_idx, &unit, 2);
    DSH_CHECK_PARAM(dsh_getUint(tokens, token_idx, "vid", &vid), token_idx, 2);
    if (AIR_E_OK == dsh_checkString(tokens[token_idx], "gaddr"))
    {
        DSH_CHECK_IP_ADDR(dsh_getIpv4Addr(tokens, token_idx, "gaddr", &group_addr.ip_addr.ipv4_addr),
                          dsh_getIpv6Addr(tokens, token_idx, "gaddr", &group_addr.ip_addr.ipv6_addr),
                          group_addr.ipv4, token_idx, 2);
        if (!AIR_L3_IP_IS_MULTICAST(&group_addr))
        {
            osal_printf("***Error***, Input gaddr shall be a mulitcast address.\n");
            return AIR_E_BAD_PARAMETER;
        }
    }
    if (AIR_E_OK == dsh_checkString(tokens[token_idx], "saddr"))
    {
        DSH_CHECK_IP_ADDR(dsh_getIpv4Addr(tokens, token_idx, "saddr", &source_addr.ip_addr.ipv4_addr),
                          dsh_getIpv6Addr(tokens, token_idx, "saddr", &source_addr.ip_addr.ipv6_addr),
                          source_addr.ipv4, token_idx, 2);
        if (AIR_L3_IP_IS_MULTICAST(&source_addr))
        {
            osal_printf("***Error***, Input saddr shall not be a mulitcast address.\n");
            return AIR_E_BAD_PARAMETER;
        }
    }

    DSH_CHECK_PARAM(dsh_getPortBitmap(tokens, token_idx, "portlist", unit, &pbm), token_idx, 2);
    DSH_CHECK_LAST_TOKEN(tokens[token_idx]);

    AIR_PORT_BITMAP_COPY(entry.port_bitmap,pbm);

    entry.vid = vid;
    osal_memcpy(&entry.group_addr, &group_addr, sizeof(AIR_IP_ADDR_T));
    osal_memcpy(&entry.source_addr, &source_addr, sizeof(AIR_IP_ADDR_T));
    if (CMLIB_UTIL_IP_ADDR_IS_ZERO(entry.source_addr))
    {
        entry.type = AIR_IPMC_TYPE_GRP;
    }
    else
    {
        entry.type = AIR_IPMC_TYPE_GRP_SRC;
    }

    if((entry.type == AIR_IPMC_TYPE_GRP_SRC) &&
       (group_addr.ipv4 != source_addr.ipv4))
    {
        osal_printf("***Error***, IP family of group and source are not the same.\n");
        return AIR_E_BAD_PARAMETER;
    }

    rc = air_ipmc_addMcastMember(unit, &entry);
    if (AIR_E_OK != rc)
    {
        _ipmc_cmd_printMcastStr("Add", entry.type, entry.vid, &entry.group_addr, &entry.source_addr, entry.port_bitmap, rc);
    }

    return rc;
}

static AIR_ERROR_NO_T
_ipmc_cmd_delMcastMember(
    const C8_T  *tokens[],
    UI32_T      token_idx)
{
    AIR_ERROR_NO_T    rc = AIR_E_OK;
    UI32_T            unit = 0, vid = 0;
    AIR_PORT_BITMAP_T pbm;
    AIR_IPMC_ENTRY_T    entry;
    AIR_IP_ADDR_T       group_addr, source_addr;

    osal_memset(&entry, 0, sizeof(AIR_IPMC_ENTRY_T));
    osal_memset(&group_addr, 0, sizeof(AIR_IP_ADDR_T));
    osal_memset(&source_addr, 0, sizeof(AIR_IP_ADDR_T));
    AIR_PORT_BITMAP_CLEAR(pbm);
    DSH_CHECK_GET_UNIT(tokens, token_idx, &unit, 2);
    DSH_CHECK_PARAM(dsh_getUint(tokens, token_idx, "vid", &vid), token_idx, 2);
    if (AIR_E_OK == dsh_checkString(tokens[token_idx], "gaddr"))
    {
        DSH_CHECK_IP_ADDR(dsh_getIpv4Addr(tokens, token_idx, "gaddr", &group_addr.ip_addr.ipv4_addr),
                          dsh_getIpv6Addr(tokens, token_idx, "gaddr", &group_addr.ip_addr.ipv6_addr),
                          group_addr.ipv4, token_idx, 2);
        if (!AIR_L3_IP_IS_MULTICAST(&group_addr))
        {
            osal_printf("***Error***, Input gaddr shall be a mulitcast address.\n");
            return AIR_E_BAD_PARAMETER;
        }
    }
    if (AIR_E_OK == dsh_checkString(tokens[token_idx], "saddr"))
    {
        DSH_CHECK_IP_ADDR(dsh_getIpv4Addr(tokens, token_idx, "saddr", &source_addr.ip_addr.ipv4_addr),
                          dsh_getIpv6Addr(tokens, token_idx, "saddr", &source_addr.ip_addr.ipv6_addr),
                          source_addr.ipv4, token_idx, 2);
        if (AIR_L3_IP_IS_MULTICAST(&source_addr))
        {
            osal_printf("***Error***, Input saddr shall not be a mulitcast address.\n");
            return AIR_E_BAD_PARAMETER;
        }
    }

    DSH_CHECK_PARAM(dsh_getPortBitmap(tokens, token_idx, "portlist", unit, &pbm), token_idx, 2);
    DSH_CHECK_LAST_TOKEN(tokens[token_idx]);

    AIR_PORT_BITMAP_COPY(entry.port_bitmap,pbm);

    entry.vid = vid;
    osal_memcpy(&entry.group_addr, &group_addr, sizeof(AIR_IP_ADDR_T));
    osal_memcpy(&entry.source_addr, &source_addr, sizeof(AIR_IP_ADDR_T));

    if (CMLIB_UTIL_IP_ADDR_IS_ZERO(entry.source_addr))
    {
        entry.type = AIR_IPMC_TYPE_GRP;
    }
    else
    {
        entry.type = AIR_IPMC_TYPE_GRP_SRC;
    }

    if((entry.type == AIR_IPMC_TYPE_GRP_SRC) &&
       (group_addr.ipv4 != source_addr.ipv4))
    {
        osal_printf("***Error***, IP family of group and source are not the same.\n");
        return AIR_E_BAD_PARAMETER;
    }

    rc = air_ipmc_delMcastMember(unit, &entry);
    if (AIR_E_OK != rc)
    {
        _ipmc_cmd_printMcastStr("Remove", entry.type, entry.vid, &entry.group_addr, &entry.source_addr, entry.port_bitmap, rc);
    }

    return rc;
}

static AIR_ERROR_NO_T
_ipmc_cmd_getMcastMemberCnt(
    const C8_T  *tokens[],
    UI32_T      token_idx)
{
    AIR_ERROR_NO_T  rc = AIR_E_OK;
    UI32_T          count = 0, unit=0, vid=0;
    AIR_IPMC_ENTRY_T    entry;
    AIR_IP_ADDR_T       group_addr, source_addr;
    C8_T                dip_str[CMLIB_UTIL_IP_ADDR_STR_SIZE];
    C8_T                sip_str[CMLIB_UTIL_IP_ADDR_STR_SIZE];

    osal_memset(&entry, 0, sizeof(AIR_IPMC_ENTRY_T));
    osal_memset(&group_addr, 0, sizeof(AIR_IP_ADDR_T));
    osal_memset(&source_addr, 0, sizeof(AIR_IP_ADDR_T));

    DSH_CHECK_GET_UNIT(tokens, token_idx, &unit, 2);
    DSH_CHECK_PARAM(dsh_getUint(tokens, token_idx, "vid", &vid), token_idx, 2);
    if (AIR_E_OK == dsh_checkString(tokens[token_idx], "gaddr"))
    {
        DSH_CHECK_IP_ADDR(dsh_getIpv4Addr(tokens, token_idx, "gaddr", &group_addr.ip_addr.ipv4_addr),
                          dsh_getIpv6Addr(tokens, token_idx, "gaddr", &group_addr.ip_addr.ipv6_addr),
                          group_addr.ipv4, token_idx, 2);
        if (!AIR_L3_IP_IS_MULTICAST(&group_addr))
        {
            osal_printf("***Error***, Input gaddr shall be a mulitcast address.\n");
            return AIR_E_BAD_PARAMETER;
        }
    }
    if (AIR_E_OK == dsh_checkString(tokens[token_idx], "saddr"))
    {
        DSH_CHECK_IP_ADDR(dsh_getIpv4Addr(tokens, token_idx, "saddr", &source_addr.ip_addr.ipv4_addr),
                          dsh_getIpv6Addr(tokens, token_idx, "saddr", &source_addr.ip_addr.ipv6_addr),
                          source_addr.ipv4, token_idx, 2);
        if (AIR_L3_IP_IS_MULTICAST(&source_addr))
        {
            osal_printf("***Error***, Input saddr shall not be a mulitcast address.\n");
            return AIR_E_BAD_PARAMETER;
        }
    }
    DSH_CHECK_LAST_TOKEN(tokens[token_idx]);

    entry.vid = vid;
    osal_memcpy(&entry.group_addr, &group_addr, sizeof(AIR_IP_ADDR_T));
    osal_memcpy(&entry.source_addr, &source_addr, sizeof(AIR_IP_ADDR_T));
    _ipmc_cmd_getMcastStr(&group_addr, &source_addr, dip_str, sip_str);

    if (CMLIB_UTIL_IP_ADDR_IS_ZERO(entry.source_addr))
    {
        entry.type = AIR_IPMC_TYPE_GRP;
    }
    else
    {
        entry.type = AIR_IPMC_TYPE_GRP_SRC;
    }

    if((AIR_IPMC_TYPE_GRP_SRC == entry.type) &&
       (group_addr.ipv4 != source_addr.ipv4))
    {
        osal_printf("***Error***, IP family of group and source are not the same.\n");
        return AIR_E_BAD_PARAMETER;
    }

    rc = air_ipmc_getMcastMemberCnt(unit, &entry, &count);
    if (AIR_E_OK == rc)
    {
        if (entry.type == AIR_IPMC_TYPE_GRP_SRC)
        {
            osal_printf("Get vid %d group %s source %s member count : %d.",
                        entry.vid, dip_str, sip_str, count);
        }
        else
        {
            osal_printf("Get vid %d group %s member count : %d.\n",
                        entry.vid, dip_str, count);
        }
    }
    else if(AIR_E_ENTRY_NOT_FOUND == rc)
    {
        if (entry.type == AIR_IPMC_TYPE_GRP_SRC)
        {
            osal_printf("***Error***, Can not find vid %d group %s source %s.",
                        entry.vid, dip_str, sip_str);
        }
        else
        {
            osal_printf("***Error***, Can not find vid %d group %s.\n",
                        entry.vid, dip_str);
        }
    }
    else
    {
        if (entry.type == AIR_IPMC_TYPE_GRP_SRC)
        {
            osal_printf("***Error***, Get vid %d group %s source %s member count Fail.",
                        entry.vid, dip_str, sip_str, count);
        }
        else
        {
            osal_printf("***Error***, Get vid %d group %s member count Fail.\n",
                        entry.vid, dip_str, count);
        }
    }

    return rc;
}

static AIR_ERROR_NO_T
_ipmc_cmd_getMcastAddrFirst(
    const C8_T  *tokens[],
    UI32_T      token_idx)
{
    AIR_ERROR_NO_T  rc = AIR_E_OK;
    UI32_T          unit = 0, i = 0;
    UI32_T          set_num;
    C8_T                search_type_str[DSH_CMD_MAX_LENGTH];
    AIR_IPMC_MATCH_TYPE_T   match_type;
    UI32_T                  count = 0;
    AIR_IPMC_ENTRY_T        *ptr_entry;
    AIR_IPMC_ENTRY_T        *ptr_entry_tmp;

    osal_memset(search_type_str, 0, sizeof(search_type_str));

    DSH_CHECK_GET_UNIT(tokens, token_idx, &unit, 2);
    DSH_CHECK_PARAM(dsh_getString(tokens, token_idx, "search-type", search_type_str), token_idx, 2);

    if (AIR_E_OK == dsh_checkString(search_type_str, "dip4"))
    {
        match_type = AIR_IPMC_MATCH_TYPE_IPV4_GRP;
    }
    else if (AIR_E_OK == dsh_checkString(search_type_str, "dip4sip4"))
    {
        match_type = AIR_IPMC_MATCH_TYPE_IPV4_GRP_SRC;
    }
    else if (AIR_E_OK == dsh_checkString(search_type_str, "dip6"))
    {
        match_type = AIR_IPMC_MATCH_TYPE_IPV6_GRP;
    }
    else if (AIR_E_OK == dsh_checkString(search_type_str, "dip6sip6"))
    {
        match_type = AIR_IPMC_MATCH_TYPE_IPV6_GRP_SRC;
    }
    else
    {
        return DSH_E_SYNTAX_ERR;
    }
    DSH_CHECK_LAST_TOKEN(tokens[token_idx]);

    rc = air_ipmc_getMcastBucketSize(unit, &set_num);
    if (AIR_E_OK != rc)
    {
        osal_printf("***Error***, get bucket size fail\n");
        return rc;
    }

    ptr_entry = osal_alloc(sizeof(AIR_IPMC_ENTRY_T) * set_num, "cmd");
    if (NULL == ptr_entry)
    {
        osal_printf("***Error***, allocate memory fail\n");
        return AIR_E_NO_MEMORY;
    }
    osal_memset(ptr_entry, 0, sizeof(AIR_IPMC_ENTRY_T) * set_num);

    rc = air_ipmc_getFirstMcastAddr(unit, match_type, &count, ptr_entry);
    if (AIR_E_OK != rc)
    {
        osal_printf("***Error***, get first multicast fail\n");
        return rc;
    }

    for (i = 0, ptr_entry_tmp = ptr_entry; i < count; i++, ptr_entry_tmp++)
    {
        _ipmc_cmd_printMcastStr("Get", ptr_entry_tmp->type, ptr_entry_tmp->vid, &ptr_entry_tmp->group_addr, &ptr_entry_tmp->source_addr, ptr_entry_tmp->port_bitmap, rc);
    }

    osal_free(ptr_entry);
    return rc;
}

static AIR_ERROR_NO_T
_ipmc_cmd_getMcastAddrNext(
    const C8_T  *tokens[],
    UI32_T      token_idx)
{
    AIR_ERROR_NO_T  rc = AIR_E_OK;
    UI32_T          unit = 0, i = 0;
    UI32_T          set_num;
    C8_T                search_type_str[DSH_CMD_MAX_LENGTH];
    AIR_IPMC_MATCH_TYPE_T   match_type;
    UI32_T                  count = 0;
    AIR_IPMC_ENTRY_T        *ptr_entry;
    AIR_IPMC_ENTRY_T        *ptr_entry_tmp;

    osal_memset(search_type_str, 0, sizeof(search_type_str));

    DSH_CHECK_GET_UNIT(tokens, token_idx, &unit, 2);
    DSH_CHECK_PARAM(dsh_getString(tokens, token_idx, "search-type", search_type_str), token_idx, 2);

    if (AIR_E_OK == dsh_checkString(search_type_str, "dip4"))
    {
        match_type = AIR_IPMC_MATCH_TYPE_IPV4_GRP;
    }
    else if (AIR_E_OK == dsh_checkString(search_type_str, "dip4sip4"))
    {
        match_type = AIR_IPMC_MATCH_TYPE_IPV4_GRP_SRC;
    }
    else if (AIR_E_OK == dsh_checkString(search_type_str, "dip6"))
    {
        match_type = AIR_IPMC_MATCH_TYPE_IPV6_GRP;
    }
    else if (AIR_E_OK == dsh_checkString(search_type_str, "dip6sip6"))
    {
        match_type = AIR_IPMC_MATCH_TYPE_IPV6_GRP_SRC;
    }
    else
    {
        return DSH_E_SYNTAX_ERR;
    }
    DSH_CHECK_LAST_TOKEN(tokens[token_idx]);

    rc = air_ipmc_getMcastBucketSize(unit, &set_num);
    if (AIR_E_OK != rc)
    {
        osal_printf("***Error***, get bucket size fail\n");
        return rc;
    }

    ptr_entry = osal_alloc(sizeof(AIR_IPMC_ENTRY_T) * set_num, "cmd");
    if (NULL == ptr_entry)
    {
        osal_printf("***Error***, allocate memory fail\n");
        return AIR_E_NO_MEMORY;
    }
    osal_memset(ptr_entry, 0, sizeof(AIR_IPMC_ENTRY_T) * set_num);

    rc = air_ipmc_getNextMcastAddr(unit, match_type, &count, ptr_entry);
    if (AIR_E_OK != rc)
    {
        osal_printf("***Error***, get next multicast fail\n");
        return rc;
    }

    for (i = 0, ptr_entry_tmp = ptr_entry; i < count; i++, ptr_entry_tmp++)
    {
        _ipmc_cmd_printMcastStr("Get", ptr_entry_tmp->type, ptr_entry_tmp->vid, &ptr_entry_tmp->group_addr, &ptr_entry_tmp->source_addr, ptr_entry_tmp->port_bitmap, rc);
    }

    osal_free(ptr_entry);
    return rc;
}

static AIR_ERROR_NO_T
_ipmc_cmd_setMcastLookupType(
    const C8_T  *tokens[],
    UI32_T      token_idx)
{
    AIR_ERROR_NO_T     rc = AIR_E_OK;
    UI32_T             unit = 0;
    C8_T               type_str[DSH_CMD_MAX_LENGTH] = {0};
    AIR_IPMC_TYPE_T    type = AIR_IPMC_TYPE_LAST;

    DSH_CHECK_GET_UNIT(tokens, token_idx, &unit, 2);
    DSH_CHECK_PARAM(dsh_getString(tokens, token_idx, "type", type_str), token_idx, 2);
    if (AIR_E_OK == dsh_checkString(type_str, "group-only"))
    {
        type = AIR_IPMC_TYPE_GRP;
    }
    else if (AIR_E_OK == dsh_checkString(type_str, "group-source"))
    {
        type = AIR_IPMC_TYPE_GRP_SRC;
    }
    else
    {
        return AIR_E_BAD_PARAMETER;
    }
    DSH_CHECK_LAST_TOKEN(tokens[token_idx]);

    rc = air_ipmc_setMcastLookupType(unit, type);
    if(AIR_E_OK != rc)
    {
        osal_printf("***Error***, set ipmc lookup type fail.\n");
        return rc;
    }

    return rc;
}


static AIR_ERROR_NO_T
_ipmc_cmd_getMcastLookupType(
    const C8_T  *tokens[],
    UI32_T      token_idx)
{
    AIR_ERROR_NO_T     rc = AIR_E_OK;
    UI32_T             unit = 0;
    AIR_IPMC_TYPE_T    type = AIR_IPMC_TYPE_LAST;

    DSH_CHECK_GET_UNIT(tokens, token_idx, &unit, 2);
    DSH_CHECK_LAST_TOKEN(tokens[token_idx]);

    rc = air_ipmc_getMcastLookupType(unit, &type);
    if(rc == AIR_E_OK)
    {
        osal_printf("\n");
        osal_printf("IPMC Loopkup Type is %s.\n",
                    (type == AIR_IPMC_TYPE_GRP) ? "group-only" : "group-source");
    }
    else
    {
        osal_printf("\n");
        osal_printf("***Error***, Get IPMC Loopkup Type FAIL.");
    }

    return rc;
}

static AIR_ERROR_NO_T
_ipmc_cmd_setPortIpmcMode(
    const C8_T  *tokens[],
    UI32_T      token_idx)
{
    AIR_ERROR_NO_T     rc = AIR_E_OK;
    UI32_T             unit = 0, mode = 0, port = 0;
    C8_T               str[DSH_CMD_MAX_LENGTH];
    AIR_PORT_BITMAP_T  pbm;
    C8_T               pbmp_str[CMLIB_UTIL_IP_ADDR_STR_SIZE];

    osal_memset(str, 0, sizeof(str));
    osal_memset(pbm, 0, sizeof(AIR_PORT_BITMAP_T));

    DSH_CHECK_GET_UNIT(tokens, token_idx, &unit, 2);
    DSH_CHECK_PARAM(dsh_getPortBitmap(tokens, token_idx, "portlist", unit, &pbm), token_idx, 2);

    AIR_PORT_FOREACH(pbm, port)
    {
        if (!AIR_PORT_CHK(HAL_PORT_BMP(unit), port))
        {
            osal_printf("***Error***, member portlist invalid\n");
            return AIR_E_BAD_PARAMETER;
        }
    }

    DSH_CHECK_PARAM(dsh_getString(tokens, token_idx, "mode", str), token_idx, 2);
    if (AIR_E_OK == dsh_checkString(str, "enable"))
    {
        mode = TRUE;
    }
    else if (AIR_E_OK == dsh_checkString(str, "disable"))
    {
        mode = FALSE;
    }
    else
    {
        return DSH_E_SYNTAX_ERR;
    }
    DSH_CHECK_LAST_TOKEN(tokens[token_idx]);

    CMD_STRING_PORTLIST(pbmp_str, pbm);
    AIR_PORT_FOREACH(pbm, port)
    {
        rc = air_ipmc_setPortIpmcMode(unit, port, mode);
        if(AIR_E_OK != rc)
        {
            osal_printf("***Error***, set port=%u multicast data forward %s mode %s fail.\n",
                        port, (mode ? "enable" : "disable"));
            break;
        }
    }

    return rc;
}


static AIR_ERROR_NO_T
_ipmc_cmd_getPortIpmcMode(
    const C8_T  *tokens[],
    UI32_T      token_idx)
{
    AIR_ERROR_NO_T     rc = AIR_E_OK;
    UI32_T             unit = 0, port = 0;
    C8_T               str[DSH_CMD_MAX_LENGTH];
    C8_T               pbmp_str[CMLIB_UTIL_IP_ADDR_STR_SIZE];
    BOOL_T             mode = FALSE;
    AIR_PORT_BITMAP_T  port_bitmap_enable, port_bitmap_disable;

    osal_memset(str, 0, sizeof(str));
    AIR_PORT_BITMAP_CLEAR(port_bitmap_enable);
    AIR_PORT_BITMAP_CLEAR(port_bitmap_disable);

    DSH_CHECK_GET_UNIT(tokens, token_idx, &unit, 2);
    DSH_CHECK_LAST_TOKEN(tokens[token_idx]);

    AIR_PORT_FOREACH(HAL_PORT_BMP(unit), port)
    {
        rc = air_ipmc_getPortIpmcMode(unit, port, &mode);
        if(AIR_E_OK != rc)
        {
            osal_printf("***Error***, get port=%u multicast data forward mode fail.\n", port);
            return rc;
        }

        if(TRUE == mode)
        {
            AIR_PORT_ADD(port_bitmap_enable, port);
        }
        else
        {
            AIR_PORT_ADD(port_bitmap_disable, port);
        }
    }

    osal_printf("Multicast Data Forward Mode :\n");
    CMD_STRING_PORTLIST(pbmp_str, port_bitmap_enable);
    osal_printf("  Enable  portlist:  %s.\n", pbmp_str);
    CMD_STRING_PORTLIST(pbmp_str, port_bitmap_disable);
    osal_printf("  Disable portlist:  %s.\n", pbmp_str);

    return rc;
}


const static DSH_VEC_T  _ipmc_cmd_vec[] =
{
    {
        "set ipmc-lookup-type", 2, _ipmc_cmd_setMcastLookupType,
        "ipmc set ipmc-lookup-type [ unit=<UINT> ] type={ group-only | group-source }\n"
    },
    {
        "show ipmc-lookup-type", 2, _ipmc_cmd_getMcastLookupType,
        "ipmc show ipmc-lookup-type [ unit=<UINT> ]\n"
    },
    {
        "add mcast-addr", 2, _ipmc_cmd_addMcastAddr,
        "ipmc add mcast-addr [ unit=<UINT> ] vid=<UINT> gaddr=<IPADDR> [ saddr=<IPADDR> ] portlist=<UINTLIST>\n"
    },
    {
        "show mcast-addr", 2, _ipmc_cmd_getMcastAddr,
        "ipmc show mcast-addr [ unit=<UINT> ] vid=<UINT> gaddr=<IPADDR> [ saddr=<IPADDR> ]\n"
    },
    {
        "del mcast-addr", 2, _ipmc_cmd_delMcastAddr,
        "ipmc del mcast-addr [ unit=<UINT> ] vid=<UINT> gaddr=<IPADDR> [ saddr=<IPADDR> ]\n"
    },
    {
        "del mcast-all", 2, _ipmc_cmd_delAllMcastAddr,
        "ipmc del mcast-all [ unit=<UINT> ]\n"
    },
    {
        "add mcast-member", 2, _ipmc_cmd_addMcastMember,
        "ipmc add mcast-member [ unit=<UINT> ] vid=<UINT> gaddr=<IPADDR> [ saddr=<IPADDR> ] portlist=<UINTLIST>\n"
    },
    {
        "del mcast-member", 2, _ipmc_cmd_delMcastMember,
        "ipmc del mcast-member [ unit=<UINT> ] vid=<UINT> gaddr=<IPADDR> [ saddr=<IPADDR> ] portlist=<UINTLIST>\n"
    },
    {
        "show mcast-member-cnt", 2, _ipmc_cmd_getMcastMemberCnt,
        "ipmc show mcast-member-cnt [ unit=<UINT> ] vid=<UINT> gaddr=<IPADDR> [ saddr=<IPADDR> ]\n"
    },
    {
        "show mcast-addr-first", 2, _ipmc_cmd_getMcastAddrFirst,
        "ipmc show mcast-addr-first [ unit=<UINT> ] search-type={ dip4 | dip4sip4 | dip6 | dip6sip6 }\n"
    },
    {
        "show mcast-addr-next", 2, _ipmc_cmd_getMcastAddrNext,
        "ipmc show mcast-addr-next [ unit=<UINT> ] search-type={ dip4 | dip4sip4 | dip6 | dip6sip6 }\n"
    },
    {
        "set ipmc-mode", 2, _ipmc_cmd_setPortIpmcMode,
        "ipmc set ipmc-mode [ unit=<UINT> ] portlist=<UINTLIST> mode={ enable | disable }\n"
    },
    {
        "show ipmc-mode", 2, _ipmc_cmd_getPortIpmcMode,
        "ipmc show ipmc-mode [ unit=<UINT> ]\n"
    },
};

AIR_ERROR_NO_T
ipmc_cmd_dispatcher(
    const C8_T                  *tokens[],
    UI32_T                      token_idx)
{
    return (dsh_dispatcher(tokens, token_idx, _ipmc_cmd_vec, sizeof(_ipmc_cmd_vec)/sizeof(DSH_VEC_T)));
}

AIR_ERROR_NO_T
ipmc_cmd_usager()
{
    return (dsh_usager(_ipmc_cmd_vec, sizeof(_ipmc_cmd_vec)/sizeof(DSH_VEC_T)));
}

