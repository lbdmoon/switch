/*******************************************************************************
*  Copyright Statement:
*  --------------------
*  This software is protected by Copyright and the information contained
*  herein is confidential. The software may not be copied and the information
*  contained herein may not be used or disclosed except with the written
*  permission of Airoha Technology Corp. (C) 2021
*
*  BY OPENING THIS FILE, BUYER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
*  THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("AIROHA SOFTWARE")
*  RECEIVED FROM AIROHA AND/OR ITS REPRESENTATIVES ARE PROVIDED TO BUYER ON
*  AN "AS-IS" BASIS ONLY. AIROHA EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
*  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
*  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
*  NEITHER DOES AIROHA PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
*  SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
*  SUPPLIED WITH THE AIROHA SOFTWARE, AND BUYER AGREES TO LOOK ONLY TO SUCH
*  THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. AIROHA SHALL ALSO
*  NOT BE RESPONSIBLE FOR ANY AIROHA SOFTWARE RELEASES MADE TO BUYER'S
*  SPECIFICATION OR TO CONFORM TO A PARTICULAR STANDARD OR OPEN FORUM.
*
*  BUYER'S SOLE AND EXCLUSIVE REMEDY AND AIROHA'S ENTIRE AND CUMULATIVE
*  LIABILITY WITH RESPECT TO THE AIROHA SOFTWARE RELEASED HEREUNDER WILL BE,
*  AT AIROHA'S OPTION, TO REVISE OR REPLACE THE AIROHA SOFTWARE AT ISSUE,
*  OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY BUYER TO
*  AIROHA FOR SUCH AIROHA SOFTWARE AT ISSUE.
*
*  THE TRANSACTION CONTEMPLATED HEREUNDER SHALL BE CONSTRUED IN ACCORDANCE
*  WITH THE LAWS OF THE STATE OF CALIFORNIA, USA, EXCLUDING ITS CONFLICT OF
*  LAWS PRINCIPLES.  ANY DISPUTES, CONTROVERSIES OR CLAIMS ARISING THEREOF AND
*  RELATED THERETO SHALL BE SETTLED BY ARBITRATION IN SAN FRANCISCO, CA, UNDER
*  THE RULES OF THE INTERNATIONAL CHAMBER OF COMMERCE (ICC).
*
*******************************************************************************/
#include <air_error.h>
#include <air_types.h>
#include <air_port.h>
#include <osal/osal.h>
#include <parser/dsh_parser.h>
#include <parser/dsh_util.h>
#include <hal/common/hal_mdio.h>
#include <hal/common/hal_phy.h>
#include <cmd/phy_cmd.h>
#include <cmlib/cmlib_bit.h>

/* NAMING CONSTANT DECLARATIONS
 */

/* MACRO FUNCTION DECLARATIONS
 */

/* DATA TYPE DECLARATIONS
 */

/* LOCAL SUBPROGRAM BODIES
 */

static AIR_ERROR_NO_T
_phy_cmd_mdioWriteC22(
    const C8_T    *tokens[],
    UI32_T        token_idx)
{
    /* Command format
     * phy set c22 [ unit=<UINT> ] [ mdio-bus=<UINT> ] phy-addr=<UINT> reg-addr=<UINT> reg-data=<HEX>
     */
    AIR_ERROR_NO_T rc = AIR_E_OK;
    UI32_T unit = 0, mdio_bus = 0, phy_addr=0, reg_addr=0, reg_data=0;
    /* parse and get */
    DSH_CHECK_GET_UNIT(tokens, token_idx, &unit, 2);
    if (AIR_E_OK == dsh_checkString(tokens[token_idx], "mdio-bus"))
    {
        DSH_CHECK_PARAM(dsh_getUint(tokens, token_idx, "mdio-bus", &mdio_bus), token_idx, 2);
    }
    DSH_CHECK_PARAM(dsh_getUint(tokens, token_idx, "phy-addr", &phy_addr), token_idx, 2);
    DSH_CHECK_PARAM(dsh_getUint(tokens, token_idx, "reg-addr", &reg_addr), token_idx, 2);
    DSH_CHECK_PARAM(dsh_getHex(tokens, token_idx, "reg-data", &reg_data, sizeof(UI32_T)), token_idx, 2);

    rc = hal_mdio_writeC22(unit, (UI16_T)mdio_bus, (UI16_T)phy_addr, (UI16_T)reg_addr, (UI16_T)reg_data);
    if(AIR_E_OK != rc)
    {
        osal_printf("***Error***, phy set c22 error\n");
    }

    return rc;
}

static AIR_ERROR_NO_T
_phy_cmd_mdioReadC22(
    const C8_T    *tokens[],
    UI32_T        token_idx)
{
    /* Command format
     * phy show c22 [ unit=<UINT> ] [ mdio-bus=<UINT> ] phy-addr=<UINT> reg-addr=<UINT>
     */
    AIR_ERROR_NO_T rc = AIR_E_OK;
    UI32_T unit = 0, mdio_bus = 0, phy_addr=0, reg_addr=0;
    UI16_T reg_data=0;

    /* parse and get */
    DSH_CHECK_GET_UNIT(tokens, token_idx, &unit, 2);
    if (AIR_E_OK == dsh_checkString(tokens[token_idx], "mdio-bus"))
    {
        DSH_CHECK_PARAM(dsh_getUint(tokens, token_idx, "mdio-bus", &mdio_bus), token_idx, 2);
    }
    DSH_CHECK_PARAM(dsh_getUint(tokens, token_idx, "phy-addr", &phy_addr), token_idx, 2);
    DSH_CHECK_PARAM(dsh_getUint(tokens, token_idx, "reg-addr", &reg_addr), token_idx, 2);

    rc = hal_mdio_readC22(unit, (UI16_T)mdio_bus, (UI16_T)phy_addr, (UI16_T)reg_addr, &reg_data);
    if(AIR_E_OK != rc)
    {
        osal_printf("***Error***, phy show c22 error\n");
    }
    else
    {
        osal_printf("\nunit=%2u    mdio-bus=%2u    phy-addr=%2u    reg-addr=%2u    reg-data=0x%04x\n", unit, mdio_bus, phy_addr, reg_addr, reg_data);
    }

    return rc;
}

static AIR_ERROR_NO_T
_phy_cmd_mdioWriteC22ByPort(
    const C8_T    *tokens[],
    UI32_T        token_idx)
{
    /* Command format
     * phy set port-c22 [ unit=<UINT> ] portlist=<UINTLIST> reg-addr=<UINT> reg-data=<HEX>
     */
    AIR_ERROR_NO_T rc = AIR_E_OK;
    UI32_T unit = 0, port=0, reg_addr=0, reg_data=0;
    AIR_PORT_BITMAP_T port_bitmap;

    /* parse and get */
    DSH_CHECK_GET_UNIT(tokens, token_idx, &unit, 2);
    DSH_CHECK_PARAM(dsh_getPortBitmap(tokens, token_idx, "portlist", unit, &port_bitmap), token_idx, 2);
    DSH_CHECK_PARAM(dsh_getUint(tokens, token_idx, "reg-addr", &reg_addr), token_idx, 2);
    DSH_CHECK_PARAM(dsh_getHex(tokens, token_idx, "reg-data", &reg_data, sizeof(UI32_T)), token_idx, 2);

    AIR_PORT_FOREACH(port_bitmap, port)
    {
        if(port == HAL_CPU_PORT(unit))
        {
            continue;
        }
        rc = hal_mdio_writeC22ByPort(unit, port, (UI16_T)reg_addr, (UI16_T)reg_data);
        if(AIR_E_OK != rc)
        {
            osal_printf("***Error***, phy set port-c22 error\n");
            break;
        }
    }

    return rc;
}

static AIR_ERROR_NO_T
_phy_cmd_mdioReadC22ByPort(
    const C8_T    *tokens[],
    UI32_T        token_idx)
{
    /* Command format
     * phy show port-c22 [ unit=<UINT> ] portlist=<UINTLIST> reg-addr=<UINT>
     */
    AIR_ERROR_NO_T rc = AIR_E_OK;
    UI32_T unit = 0, port=0, reg_addr=0;
    UI16_T reg_data=0;
    AIR_PORT_BITMAP_T port_bitmap;

    /* parse and get */
    DSH_CHECK_GET_UNIT(tokens, token_idx, &unit, 2);
    DSH_CHECK_PARAM(dsh_getPortBitmap(tokens, token_idx, "portlist", unit, &port_bitmap), token_idx, 2);
    DSH_CHECK_PARAM(dsh_getUint(tokens, token_idx, "reg-addr", &reg_addr), token_idx, 2);

    AIR_PORT_FOREACH(port_bitmap, port)
    {
        if(port == HAL_CPU_PORT(unit))
        {
            continue;
        }
        rc = hal_mdio_readC22ByPort(unit, port, (UI16_T)reg_addr, &reg_data);
        if(AIR_E_OK != rc)
        {
            osal_printf("***Error***, phy show port-c22 error\n");
            break;
        }
        else
        {
            osal_printf("\nunit=%2u    port=%2u    reg-addr=%2u    reg-data=0x%04x\n", unit, port, reg_addr, reg_data);
        }
    }

    return rc;
}

static AIR_ERROR_NO_T
_phy_cmd_mdioWriteC45(
    const C8_T    *tokens[],
    UI32_T        token_idx)
{
    /* Command format
     * phy set c45 [ unit=<UINT> ] [ mdio-bus=<UINT> ] phy-addr=<UINT>
     * dev-type=<UINT> reg-addr=<UINT> reg-data=<HEX>
     */
    AIR_ERROR_NO_T rc = AIR_E_OK;
    UI32_T unit = 0, mdio_bus = 0, phy_addr=0, dev_type=0, reg_addr=0, reg_data=0;

    /* parse and get */
    DSH_CHECK_GET_UNIT(tokens, token_idx, &unit, 2);
    if (AIR_E_OK == dsh_checkString(tokens[token_idx], "mdio-bus"))
    {
        DSH_CHECK_PARAM(dsh_getUint(tokens, token_idx, "mdio-bus", &mdio_bus), token_idx, 2);
    }
    DSH_CHECK_PARAM(dsh_getUint(tokens, token_idx, "phy-addr", &phy_addr), token_idx, 2);
    DSH_CHECK_PARAM(dsh_getUint(tokens, token_idx, "dev-type", &dev_type), token_idx, 2);
    DSH_CHECK_PARAM(dsh_getUint(tokens, token_idx, "reg-addr", &reg_addr), token_idx, 2);
    DSH_CHECK_PARAM(dsh_getHex(tokens, token_idx, "reg-data", &reg_data, sizeof(UI32_T)), token_idx, 2);

    rc = hal_mdio_writeC45(unit, (UI16_T)mdio_bus, (UI16_T)phy_addr, (UI16_T)dev_type, (UI16_T)reg_addr, (UI16_T)reg_data);
    if(AIR_E_OK != rc)
    {
        osal_printf("***Error***, phy set c45 error\n");
    }

    return rc;
}

static AIR_ERROR_NO_T
_phy_cmd_mdioReadC45(
    const C8_T    *tokens[],
    UI32_T        token_idx)
{
    /* Command format
     * phy show c45 [ unit=<UINT> ] [ mdio-bus=<UINT> ] phy-addr=<UINT> dev-type=<UINT> reg-addr=<UINT>
     */
    AIR_ERROR_NO_T rc = AIR_E_OK;
    UI32_T unit = 0, mdio_bus = 0, phy_addr=0, dev_type=0, reg_addr=0;
    UI16_T reg_data=0;
    /* parse and get */
    DSH_CHECK_GET_UNIT(tokens, token_idx, &unit, 2);
    if (AIR_E_OK == dsh_checkString(tokens[token_idx], "mdio-bus"))
    {
        DSH_CHECK_PARAM(dsh_getUint(tokens, token_idx, "mdio-bus", &mdio_bus), token_idx, 2);
    }
    DSH_CHECK_PARAM(dsh_getUint(tokens, token_idx, "phy-addr", &phy_addr), token_idx, 2);
    DSH_CHECK_PARAM(dsh_getUint(tokens, token_idx, "dev-type", &dev_type), token_idx, 2);
    DSH_CHECK_PARAM(dsh_getUint(tokens, token_idx, "reg-addr", &reg_addr), token_idx, 2);

    rc = hal_mdio_readC45(unit, (UI16_T)mdio_bus, (UI16_T)phy_addr, (UI16_T)dev_type, (UI16_T)reg_addr, &reg_data);
    if(AIR_E_OK != rc)
    {
        osal_printf("***Error***, phy set c45 error\n");
    }
    else
    {
        osal_printf("\nunit=%2u    mdio-bus=%2u    phy-addr=%2u    dev-type=%2u    reg-addr=%2u    reg-data=0x%04x\n", unit, mdio_bus, phy_addr, dev_type, reg_addr, reg_data);
    }

    return rc;
}

static AIR_ERROR_NO_T
_phy_cmd_mdioWriteC45ByPort(
    const C8_T    *tokens[],
    UI32_T        token_idx)
{
    /* Command format
     * phy set port-c45 [ unit=<UINT> ] portlist=<UINTLIST>
     * dev-type=<UINT> reg-addr=<UINT> reg-data=<HEX>
     */
    AIR_ERROR_NO_T rc = AIR_E_OK;
    UI32_T unit = 0, port=0, dev_type=0, reg_addr=0, reg_data=0;
    AIR_PORT_BITMAP_T port_bitmap;

    /* parse and get */
    DSH_CHECK_GET_UNIT(tokens, token_idx, &unit, 2);
    DSH_CHECK_PARAM(dsh_getPortBitmap(tokens, token_idx, "portlist", unit, &port_bitmap), token_idx, 2);
    DSH_CHECK_PARAM(dsh_getUint(tokens, token_idx, "dev-type", &dev_type), token_idx, 2);
    DSH_CHECK_PARAM(dsh_getUint(tokens, token_idx, "reg-addr", &reg_addr), token_idx, 2);
    DSH_CHECK_PARAM(dsh_getHex(tokens, token_idx, "reg-data", &reg_data, sizeof(UI32_T)), token_idx, 2);

    AIR_PORT_FOREACH(port_bitmap, port)
    {
        if(port == HAL_CPU_PORT(unit))
        {
            continue;
        }
        rc = hal_mdio_writeC45ByPort(unit, port, dev_type, (UI16_T)reg_addr, (UI16_T)reg_data);
        if(AIR_E_OK != rc)
        {
            osal_printf("***Error***, phy set port-c22 error\n");
            break;
        }
    }

    return rc;
}

static AIR_ERROR_NO_T
_phy_cmd_mdioReadC45ByPort(
    const C8_T    *tokens[],
    UI32_T        token_idx)
{
    /* Command format
     * phy show port-c45 [ unit=<UINT> ] portlist=<UINTLIST> dev-type=<UINT> reg-addr=<UINT>
     */
    AIR_ERROR_NO_T rc = AIR_E_OK;
    UI32_T unit = 0, port=0, dev_type=0, reg_addr=0;
    UI16_T reg_data=0;
    AIR_PORT_BITMAP_T port_bitmap;

    /* parse and get */
    DSH_CHECK_GET_UNIT(tokens, token_idx, &unit, 2);
    DSH_CHECK_PARAM(dsh_getPortBitmap(tokens, token_idx, "portlist", unit, &port_bitmap), token_idx, 2);
    DSH_CHECK_PARAM(dsh_getUint(tokens, token_idx, "dev-type", &dev_type), token_idx, 2);
    DSH_CHECK_PARAM(dsh_getUint(tokens, token_idx, "reg-addr", &reg_addr), token_idx, 2);

    AIR_PORT_FOREACH(port_bitmap, port)
    {
        if(port == HAL_CPU_PORT(unit))
        {
            continue;
        }
        rc = hal_mdio_readC45ByPort(unit, port, (UI16_T)dev_type, (UI16_T)reg_addr, &reg_data);
        if(AIR_E_OK != rc)
        {
            osal_printf("***Error***, phy show port-c22 error\n");
            break;
        }
        else
        {
            osal_printf("\nunit=%2u    port=%2u    dev-type=%2u    reg-addr=%2u    reg-data=0x%04x\n", unit, port, dev_type, reg_addr, reg_data);
        }
    }

    return rc;
}

static AIR_ERROR_NO_T
_phy_cmd_showInfo(
    const C8_T    *tokens[],
    UI32_T        token_idx)
{
    /* Command format
     * phy show info [ unit=<UINT> ] portlist=<UINTLIST>
     */
    UI32_T unit = 0, port=0;
    AIR_PORT_BITMAP_T port_bitmap;
    UI16_T reg_id_msb = 0, reg_id_lsb = 0;
    C8_T buffer[11] = {};

    /* parse and get */
    DSH_CHECK_GET_UNIT(tokens, token_idx, &unit, 2);
    DSH_CHECK_PARAM(dsh_getPortBitmap(tokens, token_idx, "portlist", unit, &port_bitmap), token_idx, 2);

    HAL_CHECK_UNIT(unit);

    osal_printf("%10s %7s %10s %10s %10s %8s %10s %10s\n",
                "unit/port", "type", "i2c-addr", "phy-addr", "phy-id", "rev-id", "internal", "external");

    AIR_PORT_FOREACH(port_bitmap, port)
    {
        if (!HAL_IS_ETH_PORT_VALID(unit, port))
        {
            continue;
        }

        osal_printf("%4d/%2d", unit, port);

        /* access type */
        switch (HAL_PHY_PORT_DEV_ACCESS_TYPE(unit, port))
        {
            case HAL_PHY_ACCESS_TYPE_MDIO:
                osal_printf("%11s", "mdio");
                break;
            case HAL_PHY_ACCESS_TYPE_I2C_MDIO:
                osal_printf("%11s", "i2c");
                break;
            default:
                osal_printf("%11s", "---");
                break;
        }

        /* i2c/phy addr */
        osal_snprintf(buffer, sizeof(buffer), "0x%x", HAL_PHY_PORT_DEV_I2C_ADDR(unit, port));
        osal_printf("%11s", buffer);
        osal_printf("%11d", HAL_PHY_PORT_DEV_PHY_ADDR(unit, port));

        /* phy id, revision id */
        if ((AIR_E_OK == hal_mdio_readC22ByPort(unit, port, MII_PHYSID1, &reg_id_msb))
            && (AIR_E_OK == hal_mdio_readC22ByPort(unit, port, MII_PHYSID2, &reg_id_lsb)))
        {
            osal_snprintf(buffer, sizeof(buffer), "0x%x", (reg_id_msb << 16) | (reg_id_lsb & BITS_RANGE(MII_PHY_LSB_ID_OFFT, MII_PHY_LSB_ID_LENG)));
            osal_printf("%11s", buffer);
            osal_printf("%9d", HAL_PHY_PORT_DEV_REVISION_ID(unit, port));
        }
        else
        {
            osal_printf("%11s", "---");
            osal_printf("%9s", "---");
        }

        /* internal/external driver */
        if (NULL != PTR_HAL_PHY_PORT_INT_DRIVER(unit, port))
        {
            osal_printf("%11s", "Y");
        }
        else
        {
            osal_printf("%11s", "N");
        }

        if (NULL != PTR_HAL_PHY_PORT_EXT_DRIVER(unit, port))
        {
            osal_printf("%11s", "Y");
        }
        else
        {
            osal_printf("%11s", "N");
        }
        osal_printf("\n");
    }

    return AIR_E_OK;
}

#ifdef AIR_EN_I2C_PHY
static AIR_ERROR_NO_T
_phy_cmd_mdioWriteRegByI2c(
    const C8_T    *tokens[],
    UI32_T        token_idx)
{
    /* Command format
     * phy set port-i2c-reg [ unit=<UINT> ] portlist=<UINTLIST> reg-addr=<UINT> reg-data=<HEX>
     */
    AIR_ERROR_NO_T rc = AIR_E_OK;
    UI32_T unit = 0, port = 0, reg_addr = 0, reg_data = 0;
    AIR_PORT_BITMAP_T port_bitmap;

    /* parse and get */
    DSH_CHECK_GET_UNIT(tokens, token_idx, &unit, 2);
    DSH_CHECK_PARAM(dsh_getPortBitmap(tokens, token_idx, "portlist", unit, &port_bitmap), token_idx, 2);
    DSH_CHECK_PARAM(dsh_getUint(tokens, token_idx, "reg-addr", &reg_addr), token_idx, 2);
    DSH_CHECK_PARAM(dsh_getHex(tokens, token_idx, "reg-data", &reg_data, sizeof(UI32_T)), token_idx, 2);

    AIR_PORT_FOREACH(port_bitmap, port)
    {
        if (port == HAL_CPU_PORT(unit))
        {
            continue;
        }

        if (HAL_PHY_ACCESS_TYPE_MDIO == HAL_PHY_PORT_DEV_ACCESS_TYPE(unit, port))
        {
            osal_printf("***Error***, port %u access type is not i2c\n", port);
            rc = AIR_E_BAD_PARAMETER;
            break;
        }

        rc = hal_mdio_writeRegByI2c(unit, port, reg_addr, reg_data);
        if(AIR_E_OK != rc)
        {
            osal_printf("***Error***, phy set port-i2c-reg error\n");
            break;
        }
    }

    return rc;
}

static AIR_ERROR_NO_T
_phy_cmd_mdioReadRegByI2c(
    const C8_T    *tokens[],
    UI32_T        token_idx)
{
    /* Command format
     * phy show port-i2c-reg [ unit=<UINT> ] portlist=<UINTLIST> reg-addr=<UINT>
     */
    AIR_ERROR_NO_T rc = AIR_E_OK;
    UI32_T unit = 0, port = 0, reg_addr = 0, reg_data = 0;
    AIR_PORT_BITMAP_T port_bitmap;

    /* parse and get */
    DSH_CHECK_GET_UNIT(tokens, token_idx, &unit, 2);
    DSH_CHECK_PARAM(dsh_getPortBitmap(tokens, token_idx, "portlist", unit, &port_bitmap), token_idx, 2);
    DSH_CHECK_PARAM(dsh_getUint(tokens, token_idx, "reg-addr", &reg_addr), token_idx, 2);

    AIR_PORT_FOREACH(port_bitmap, port)
    {
        if (port == HAL_CPU_PORT(unit))
        {
            continue;
        }

        if (HAL_PHY_ACCESS_TYPE_MDIO == HAL_PHY_PORT_DEV_ACCESS_TYPE(unit, port))
        {
            osal_printf("***Error***, port %u access type is not i2c\n", port);
            rc = AIR_E_BAD_PARAMETER;
            break;
        }

        rc = hal_mdio_readRegByI2c(unit, port, reg_addr, &reg_data);
        if(AIR_E_OK != rc)
        {
            osal_printf("***Error***, phy show port-i2c-reg\n");
            break;
        }
        else
        {
            osal_printf("\nunit=%2u    port=%2u    reg-addr=0x%08x    reg-data=0x%08x\n", unit, port, reg_addr, reg_data);
        }
    }

    return rc;
}

static AIR_ERROR_NO_T
_phy_cmd_mdioWriteC22ByI2c(
    const C8_T    *tokens[],
    UI32_T        token_idx)
{
    /* Command format
     * phy set i2c-c22 [ unit=<UINT> ] [ i2c-bus=<UINT> ] i2c-addr=<UINT> phy-addr=<UINT> reg-addr=<UINT> reg-data=<HEX>
     */
    AIR_ERROR_NO_T rc = AIR_E_OK;
    UI32_T unit = 0, i2c_bus = 0, i2c_addr = 0, phy_addr = 0, reg_addr = 0, reg_data = 0;
    /* parse and get */
    DSH_CHECK_GET_UNIT(tokens, token_idx, &unit, 2);
    if (AIR_E_OK == dsh_checkString(tokens[token_idx], "i2c-bus"))
    {
        DSH_CHECK_PARAM(dsh_getUint(tokens, token_idx, "i2c-bus", &i2c_bus), token_idx, 2);
    }
    DSH_CHECK_PARAM(dsh_getUint(tokens, token_idx, "i2c-addr", &i2c_addr), token_idx, 2);
    DSH_CHECK_PARAM(dsh_getUint(tokens, token_idx, "phy-addr", &phy_addr), token_idx, 2);
    DSH_CHECK_PARAM(dsh_getUint(tokens, token_idx, "reg-addr", &reg_addr), token_idx, 2);
    DSH_CHECK_PARAM(dsh_getHex(tokens, token_idx, "reg-data", &reg_data, sizeof(UI32_T)), token_idx, 2);

    rc = hal_mdio_writeC22ByI2c(unit, (UI16_T)i2c_bus, (UI16_T)i2c_addr, (UI16_T)phy_addr, (UI16_T)reg_addr, (UI16_T)reg_data);
    if(AIR_E_OK != rc)
    {
        osal_printf("***Error***, phy set i2c-c22 error\n");
    }

    return rc;
}

static AIR_ERROR_NO_T
_phy_cmd_mdioReadC22ByI2c(
    const C8_T    *tokens[],
    UI32_T        token_idx)
{
    /* Command format
     * phy show i2c-c22 [ unit=<UINT> ] [ i2c-bus=<UINT> ] i2c-addr=<UINT> phy-addr=<UINT> reg-addr=<UINT>
     */
    AIR_ERROR_NO_T rc = AIR_E_OK;
    UI32_T unit = 0, i2c_bus = 0, i2c_addr = 0, phy_addr = 0, reg_addr = 0;
    UI16_T reg_data = 0;

    /* parse and get */
    DSH_CHECK_GET_UNIT(tokens, token_idx, &unit, 2);
    if (AIR_E_OK == dsh_checkString(tokens[token_idx], "i2c-bus"))
    {
        DSH_CHECK_PARAM(dsh_getUint(tokens, token_idx, "i2c-bus", &i2c_bus), token_idx, 2);
    }
    DSH_CHECK_PARAM(dsh_getUint(tokens, token_idx, "i2c-addr", &i2c_addr), token_idx, 2);
    DSH_CHECK_PARAM(dsh_getUint(tokens, token_idx, "phy-addr", &phy_addr), token_idx, 2);
    DSH_CHECK_PARAM(dsh_getUint(tokens, token_idx, "reg-addr", &reg_addr), token_idx, 2);

    rc = hal_mdio_readC22ByI2c(unit, (UI16_T)i2c_bus, (UI16_T)i2c_addr, (UI16_T)phy_addr, (UI16_T)reg_addr, &reg_data);
    if(AIR_E_OK != rc)
    {
        osal_printf("***Error***, phy show i2c-c22 error\n");
    }
    else
    {
        osal_printf("\nunit=%2u    i2c-bus=%2u    i2c_addr=0x%02X    phy-addr=%2u    reg-addr=%2u    reg-data=0x%04x\n", unit, i2c_bus, i2c_addr, phy_addr, reg_addr, reg_data);
    }

    return rc;
}

static AIR_ERROR_NO_T
_phy_cmd_mdioWriteC45ByI2c(
    const C8_T    *tokens[],
    UI32_T        token_idx)
{
    /* Command format
     * phy set i2c-c45 [ unit=<UINT> ] [ i2c-bus=<UINT> ] i2c-addr=<UINT>
     * phy-addr=<UINT> dev-type=<UINT> reg-addr=<UINT> reg-data=<HEX>
     */
    AIR_ERROR_NO_T rc = AIR_E_OK;
    UI32_T unit = 0, i2c_bus = 0, i2c_addr = 0, phy_addr = 0, dev_type = 0, reg_addr = 0, reg_data = 0;

    /* parse and get */
    DSH_CHECK_GET_UNIT(tokens, token_idx, &unit, 2);
    if (AIR_E_OK == dsh_checkString(tokens[token_idx], "i2c-bus"))
    {
        DSH_CHECK_PARAM(dsh_getUint(tokens, token_idx, "i2c-bus", &i2c_bus), token_idx, 2);
    }
    DSH_CHECK_PARAM(dsh_getUint(tokens, token_idx, "i2c-addr", &i2c_addr), token_idx, 2);
    DSH_CHECK_PARAM(dsh_getUint(tokens, token_idx, "phy-addr", &phy_addr), token_idx, 2);
    DSH_CHECK_PARAM(dsh_getUint(tokens, token_idx, "dev-type", &dev_type), token_idx, 2);
    DSH_CHECK_PARAM(dsh_getUint(tokens, token_idx, "reg-addr", &reg_addr), token_idx, 2);
    DSH_CHECK_PARAM(dsh_getHex(tokens, token_idx, "reg-data", &reg_data, sizeof(UI32_T)), token_idx, 2);

    rc = hal_mdio_writeC45ByI2c(unit, (UI16_T)i2c_bus, (UI16_T)i2c_addr, (UI16_T)phy_addr, (UI16_T)dev_type, (UI16_T)reg_addr, (UI16_T)reg_data);
    if(AIR_E_OK != rc)
    {
        osal_printf("***Error***, phy set i2c-c45 error\n");
    }

    return rc;
}

static AIR_ERROR_NO_T
_phy_cmd_mdioReadC45ByI2c(
    const C8_T    *tokens[],
    UI32_T        token_idx)
{
    /* Command format
     * phy show i2c-c45 [ unit=<UINT> ] [ i2c-bus=<UINT> ] i2c-addr=<UINT>
     * phy-addr=<UINT> dev-type=<UINT> reg-addr=<UINT>
     */
    AIR_ERROR_NO_T rc = AIR_E_OK;
    UI32_T unit = 0, i2c_bus = 0, i2c_addr = 0, phy_addr = 0, dev_type = 0, reg_addr = 0;
    UI16_T reg_data = 0;
    /* parse and get */
    DSH_CHECK_GET_UNIT(tokens, token_idx, &unit, 2);
    if (AIR_E_OK == dsh_checkString(tokens[token_idx], "i2c-bus"))
    {
        DSH_CHECK_PARAM(dsh_getUint(tokens, token_idx, "i2c-bus", &i2c_bus), token_idx, 2);
    }
    DSH_CHECK_PARAM(dsh_getUint(tokens, token_idx, "i2c-addr", &i2c_addr), token_idx, 2);
    DSH_CHECK_PARAM(dsh_getUint(tokens, token_idx, "phy-addr", &phy_addr), token_idx, 2);
    DSH_CHECK_PARAM(dsh_getUint(tokens, token_idx, "dev-type", &dev_type), token_idx, 2);
    DSH_CHECK_PARAM(dsh_getUint(tokens, token_idx, "reg-addr", &reg_addr), token_idx, 2);

    rc = hal_mdio_readC45ByI2c(unit, (UI16_T)i2c_bus, (UI16_T)i2c_addr, (UI16_T)phy_addr, (UI16_T)dev_type, (UI16_T)reg_addr, &reg_data);
    if(AIR_E_OK != rc)
    {
        osal_printf("***Error***, phy show i2c-c45\n");
    }
    else
    {
        osal_printf("\nunit=%2u    i2c-bus=%2u    i2c_addr=0x%02X    phy-addr=%2u    dev-type=%2u    reg-addr=%2u    reg-data=0x%04x\n", unit, i2c_bus, i2c_addr, phy_addr, dev_type, reg_addr, reg_data);
    }

    return rc;
}
#endif
static AIR_ERROR_NO_T
_phy_cmd_dumpPara(
    const C8_T    *tokens[],
    UI32_T        token_idx)
{
    /* Command format
     * phy dump parameter [ unit=<UINT> ] portlist=<UINTLIST>
     */
    UI32_T unit = 0, port=0;
    AIR_PORT_BITMAP_T port_bitmap;
    AIR_ERROR_NO_T rc = AIR_E_OK;

    /* parse and get */
    DSH_CHECK_GET_UNIT(tokens, token_idx, &unit, 2);
    DSH_CHECK_PARAM(dsh_getPortBitmap(tokens, token_idx, "portlist", unit, &port_bitmap), token_idx, 2);

    AIR_PORT_FOREACH(port_bitmap, port)
    {
        rc = hal_phy_dumpPhyPara(unit, port);
        if(AIR_E_OK != rc)
        {
            osal_printf("***Error***, phy dump parameter\n");
        }
    }

    return rc;
}

/* -------------------------------------------------------------- callback */
const static DSH_VEC_T  _phy_cmd_vec[] =
{
    {
        "set c22", 2, _phy_cmd_mdioWriteC22,
        "phy set c22 [ unit=<UINT> ] [ mdio-bus=<UINT> ] phy-addr=<UINT> reg-addr=<UINT> reg-data=<HEX>\n"
    },
    {
        "show c22", 2, _phy_cmd_mdioReadC22,
        "phy show c22 [ unit=<UINT> ] [ mdio-bus=<UINT> ] phy-addr=<UINT> reg-addr=<UINT>\n"
    },
    {
        "set port-c22", 2, _phy_cmd_mdioWriteC22ByPort,
        "phy set port-c22 [ unit=<UINT> ] portlist=<UINTLIST> reg-addr=<UINT> reg-data=<HEX>\n"
    },
    {
        "show port-c22", 2, _phy_cmd_mdioReadC22ByPort,
        "phy show port-c22 [ unit=<UINT> ] portlist=<UINTLIST> reg-addr=<UINT>\n"
    },
    {
        "set c45", 2, _phy_cmd_mdioWriteC45,
        "phy set c45 [ unit=<UINT> ] [ mdio-bus=<UINT> ] phy-addr=<UINT> dev-type=<UINT> reg-addr=<UINT> reg-data=<HEX>\n"
    },
    {
        "show c45", 2, _phy_cmd_mdioReadC45,
        "phy show c45 [ unit=<UINT> ] [ mdio-bus=<UINT> ] phy-addr=<UINT> dev-type=<UINT> reg-addr=<UINT>\n"
    },
    {
        "set port-c45", 2, _phy_cmd_mdioWriteC45ByPort,
        "phy set port-c45 [ unit=<UINT> ] portlist=<UINTLIST> dev-type=<UINT> reg-addr=<UINT> reg-data=<HEX>\n"
    },
    {
        "show port-c45", 2, _phy_cmd_mdioReadC45ByPort,
        "phy show port-c45 [ unit=<UINT> ] portlist=<UINTLIST> dev-type=<UINT> reg-addr=<UINT>\n"
    },
    {
        "show info", 2, _phy_cmd_showInfo,
        "phy show info [ unit=<UINT> ] portlist=<UINTLIST>\n"
    },
#ifdef AIR_EN_I2C_PHY
    {
        "set port-i2c-reg", 2, _phy_cmd_mdioWriteRegByI2c,
        "phy set port-i2c-reg [ unit=<UINT> ] portlist=<UINTLIST> reg-addr=<UINT> reg-data=<HEX>\n"
    },
    {
        "show port-i2c-reg", 2, _phy_cmd_mdioReadRegByI2c,
        "phy show port-i2c-reg [ unit=<UINT> ] portlist=<UINTLIST> reg-addr=<UINT>\n"
    },
    {
        "set i2c-c22", 2, _phy_cmd_mdioWriteC22ByI2c,
        "phy set i2c-c22 [ unit=<UINT> ] [ i2c-bus=<UINT> ] i2c-addr=<UINT> phy-addr=<UINT> reg-addr=<UINT> reg-data=<HEX>\n"
    },
    {
        "show i2c-c22", 2, _phy_cmd_mdioReadC22ByI2c,
        "phy show i2c-c22 [ unit=<UINT> ] [ i2c-bus=<UINT> ] i2c-addr=<UINT> phy-addr=<UINT> reg-addr=<UINT>\n"
    },
    {
        "set i2c-c45", 2, _phy_cmd_mdioWriteC45ByI2c,
        "phy set i2c-c45 [ unit=<UINT> ] [ i2c-bus=<UINT> ] i2c-addr=<UINT> phy-addr=<UINT> dev-type=<UINT> reg-addr=<UINT> reg-data=<HEX>\n"
    },
    {
        "show i2c-c45", 2, _phy_cmd_mdioReadC45ByI2c,
        "phy show i2c-c45 [ unit=<UINT> ] [ i2c-bus=<UINT> ] i2c-addr=<UINT> phy-addr=<UINT> dev-type=<UINT> reg-addr=<UINT>\n"
    },
#endif
    {
        "dump parameter", 2, _phy_cmd_dumpPara,
        "phy dump parameter [ unit=<UINT> ] portlist=<UINTLIST>\n"
    },
};

AIR_ERROR_NO_T
phy_cmd_dispatcher(
    const C8_T                  *tokens[],
    UI32_T                      token_idx)
{
    int rv=0;
    rv = (dsh_dispatcher(tokens, token_idx, _phy_cmd_vec, sizeof(_phy_cmd_vec)/sizeof(DSH_VEC_T)));
    return  rv;
}

AIR_ERROR_NO_T
phy_cmd_usager()
{
    int rv=0;
    rv = (dsh_usager(_phy_cmd_vec, sizeof(_phy_cmd_vec)/sizeof(DSH_VEC_T)));
    return rv;
}
