include         $(AIR_SDK)/make/make_prologue.mk

# Subdirectories, in random order
dir             := $(d)/parser
include         $(dir)/make.mk

dir             := $(d)/cmd
include         $(dir)/make.mk
# End subdirectories
# Local rules

include         $(AIR_SDK)/make/make_epilogue.mk

