
software_branch=`git branch -av | grep "^*" | awk -F ' ' '{print $2}'`
echo "$software_branch" | grep "^(" 2>&1 > /dev/null
result=$?
#echo "$result"
if [ "$result" == "0" ] ; then
       software_branch=`git branch -av | grep "^*" | awk -F '(' '{print $2}' | awk -F ')' '{print $1}'`
       software_hash=`git branch -av | grep "^*" | awk -F ')' '{print $2}' | awk -F ' ' '{print $1}'`
       #echo "Branch is $software_branch"
       #echo "HASH is $software_hash"
else
       software_branch=`git branch -av | grep "^*" | awk -F ' ' '{print $2}'`
       software_hash=`git branch -av | grep "^*" | awk -F ' ' '{print $3}'`
       #echo "Branch is $software_branch"
       #echo "HASH is $software_hash"
fi

software_time=`date "+%Y-%m-%d %H:%M:%S" -d "$(git log -n 1 --pretty=format:%ai)"`
software_time_code=`date "+%Y%m%d.%H%M%S" -d "$(git log -n 1 --pretty=format:%ai)"`
compile_time=`date "+%Y-%m-%d %H:%M:%S"`
compile_time_code=`date "+%Y%m%d.%H%M%S"`
user=`whoami`
host=`hostname`

version="$software_branch $software_hash $software_time"
cmplenv="$user@$host $compile_time"
echo "$version"
echo "$cmplenv"

ROOT_DIR=`pwd`
CROSS_COMPILE_PATH=$ROOT_DIR/toolchain/nds32le-elf-mculib-v3j/bin/
MODULES="2149 2150 2151"

echo "$CROSS_COMPILE_PATH"
echo "$MODULES"

function compile ()
{
        local DIR=$1
	local MODULE=$2

        pushd $DIR
	make clean all CROSS_COMPILE_PATH=$CROSS_COMPILE_PATH SKU=$MODULE SW_CD=$software_time_code.$compile_time_code USRCFLAGS="-DMODULE_BOARD=\"$MODULE\" -DVERCODE=\"$version\" -DCMPLENV=\"$cmplenv\""
        RESULT=$?
        popd

        if [ $RESULT == "0" ] ; then
                STATUS=success
        else
                STATUS=failure
        fi
	echo -e "\e[40;32mCompile $DIR $STATUS\e[0m"
}

APP_DIR=freertos/project
APP_DEBUG_OUT=$APP_DIR/airRTOSSystem*
APP_IMAGE_OUT=$APP_DIR/airRTOSSystem*.crc.bin

OUTPUT_DIR=output
OUTPUT_DEBUG_DIR=$OUTPUT_DIR/debug
OUTPUT_IMAGE_DIR=$OUTPUT_DIR/image

rm -rfv  $OUTPUT_DIR $OUTPUT_DEBUG_DIR $OUTPUT_IMAGE_DIR
mkdir -p $OUTPUT_DIR $OUTPUT_DEBUG_DIR $OUTPUT_IMAGE_DIR

for i in $MODULES
do
	compile $APP_DIR $i
	if [ $RESULT != "0" ] ; then
		exit -1
	else
		APP_SUCCESS=0
	fi

	if [ $APP_SUCCESS == "0" ] ; then
		echo -e "\e[40;32mApp success output file is:\e[0m"
		ls -l $APP_DEBUG_OUT
		#APP_BIN=$PWD/`ls $APP_DEBUG_OUT.bin`
		cp -vf $APP_DEBUG_OUT $OUTPUT_DEBUG_DIR
		cp -vf $APP_IMAGE_OUT $OUTPUT_IMAGE_DIR
	fi
done

pushd $OUTPUT_IMAGE_DIR
md5sum * > md5.sum
popd

pushd $OUTPUT_DEBUG_DIR
md5sum * > md5.sum
popd

echo -e "\e[40;32mFinal All App success output file is:\e[0m"
ls -l $OUTPUT_IMAGE_DIR
ls -l $OUTPUT_DEBUG_DIR


